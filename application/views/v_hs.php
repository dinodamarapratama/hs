<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>MIS APP</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url() ?>assets/login/img/logo-biomedika.ico">
    <link rel="manifest" href="<?= base_url() ?>manifest.json" />
    <!-- Google Fonts
============================================ -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet"> -->
    <!-- <link href="<?php echo base_url() ?>assets/admin/css/material-icon-font.css" rel="stylesheet"> -->
    <link href="<?php echo base_url() ?>assets/css/material-icon-font.css" rel="stylesheet">
    <!-- Bootstrap CSS
============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/css/bootstrap.min.css">
    <!-- Bootstrap CSS
============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/css/font-awesome.min.css">
    <!-- owl.carousel CSS
============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/css/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/css/owl.transitions.css">
    <!-- meanmenu CSS
============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/css/meanmenu/meanmenu.min.css">
    <!-- animate CSS
============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/css/animate.css">
    <!-- normalize CSS
============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/css/normalize.css">
    <!-- mCustomScrollbar CSS
============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- jvectormap CSS
============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/css/jvectormap/jquery-jvectormap-2.0.3.css">
    <!-- notika icon CSS
============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/css/notika-custom-icon.css">
    <!-- wave CSS
============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/css/wave/waves.min.css">
    <!-- main CSS
============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/css/main.css">
    <!-- style CSS
============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/style.css">
    <!-- responsive CSS
============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/css/responsive.css">
    <!-- modernizr JS
============================================ -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" integrity="sha512-3n19xznO0ubPpSwYCRRBgHh63DrV+bdZfHK52b1esvId4GsfwStQNPJFjeQos2h3JwCmZl0/LgLxSKMAI55hgw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- Data Table JS
============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/css/jquery.dataTables.min.css">



    <!-- OLD -->

    <link href="<?php echo base_url() ?>assets/admin/css/roboto-font.css" rel="stylesheet">
    <!-- Waves Effect Css -->
    <link href="<?php echo base_url() ?>assets/admin/plugins/node-waves/waves.css" rel="stylesheet" />
    <!-- Animation Css -->
    <link href="<?php echo base_url() ?>assets/admin/plugins/animate-css/animate.css" rel="stylesheet" />
    <!-- Multi Select Css -->
    <link href="<?php echo base_url() ?>assets/admin/plugins/multi-select/css/multi-select.css" rel="stylesheet">
    <!-- Bootstrap Select Css -->
    <link href="<?php echo base_url() ?>assets/admin/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <!-- JQuery DataTable Css -->
    <!-- <link href="<?php echo base_url() ?>assets/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet"> -->
    <!-- Colorpicker Css -->
    <link href="<?php echo base_url() ?>assets/admin/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" />
    <!--Jquery UI-->
    <link href="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui/css/jquery-ui-autocomplete.min.css" rel="stylesheet">
    <!-- Dropzone Css -->
    <link href="<?php echo base_url() ?>assets/admin/plugins/dropzone/dropzone.css" rel="stylesheet">
    <!-- Multi Select Css -->
    <link href="<?php echo base_url() ?>assets/admin/plugins/multi-select/css/multi-select.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/admin/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/admin/plugins/select2/select2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/css/wave/button.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/admin/css/chosen/chosen.css" />
    <script src="<?php echo base_url() ?>assets/admin/plugins/jquery/jquery.min.js"></script>

    <style type="text/css">
  #rcorners {
  border-radius: 20px;
  background: #7fd77f;
  padding: 15px; 
  width: 10px;
  height: 10px; 
  } 

  #rcorners2 {
  border-radius: 20px;
  background: #f02424d6;
  padding: 15px; 
  width: 10px;
  height: 10px; 
  } 

  #rcorners3 {
  border-radius: 20px;
  background: #e1e144;
  padding: 15px; 
  width: 10px;
  height: 10px; 
  } 

    </style>


</head>

<body style="height: 100%;background-color:white; width:100%">
    <div class="container" style="width: 100%">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="data-table-list">

                    <!-- Start Master Users-->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="hs">

                            <div class="header">
                                <div align="center">
                                    <h2>
                                        Data Home Service <div id="dateNow"></div>
                                    </h2>

                                    <select id="myBranch" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);" class="form-control" style="max-width:200px">
                                        <option value="">Pilih Branch</option>
                                        <?php foreach ($branch as $key => $p) {
                                            echo '<option ' . ($selected_branch == $p->branch_id ? 'selected' : '') . ' value="'  . base_url('homeservice/index/' . $p->branch_id) . '">' . $p->branch_name . '</option>';
                                        }
                                        ?>
                                    </select>
                                    <input type="hidden" id="myDate" style="max-width:200px" name="date" class="form-control" onchange="changeDate(this)" value="<?php echo date("Y-m-d"); ?>">
									<input type="date" id="myDate2" style="max-width:200px" name="date2" class="form-control" onchange="changeDate(this)" value="<?php echo date("Y-m-d"); ?>">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <div class="col-md-3 collapse" id="collapseExample">
                                    <form action="#" id="form_masterusers">
                                        <input type="hidden" readonly name="id_user" class="form-control">

                                        <label class="form-label">Date</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="date" name="date" id="inputdate" class="form-control" onchange="changeInputDate(this)" value="<?php echo date("Y-m-d"); ?>">
                                            </div>
                                        </div>

                                        <label class="form-label">Pid</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" name="pid" class="form-control">
                                            </div>
                                        </div>

                                        <label class="form-label">Time</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <select name="time_id" class="form-control time-select">
                                                    <?php foreach ($hs_time as $key => $p) {
                                                        echo '<option value="' . $p->time_id . '">' . $p->time_name . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <input type="hidden" value="Diproses" name="status">

                                        <label class="form-label">Nama</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" name="nama" class="form-control">
                                            </div>
                                        </div>

                                        <label class="form-label">Alamat</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" name="alamat" class="form-control">
                                            </div>
                                        </div>

                                        <label class="form-label">No Telp</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="number" name="no_telp" class="form-control">
                                            </div>
                                        </div>

                                        <label class="form-label">Dokter</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" name="dokter" class="form-control">
                                            </div>
                                        </div>

                                        <label class="form-label">Jumlah Pasien</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="number" name="jumlah_pasien" class="form-control">
                                            </div>
                                        </div>

                                        <label class="form-label">Paket</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" name="paket" class="form-control">
                                            </div>
                                        </div>

                                        <label class="form-label">Payment</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <select name="pay_id" class="form-control">
                                                    <?php foreach ($hs_payment as $key => $p) {
                                                        echo '<option value="' . $p->pay_id . '">' . $p->pay_name . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <label class="form-label">Petugas</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <select name="petugas_id" class="form-control">
                                                    <?php foreach ($hs_initial_petugas as $key => $p) {
                                                        echo '<option value="' . $p->petugas_id . '">' . $p->name . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <label class="form-label">Petugas HS</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <select name="ptgshs_id" class="form-control">
                                                    <?php foreach ($hs_initial_petugas_hs as $key => $p) {
                                                        echo '<option value="' . $p->ptgshs_id . '">' . $p->name . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <label class="form-label">TTD Pasien</label>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" name="ttd_pasien" class="form-control">
                                            </div>
                                        </div>

                                        <button class="btn btn-primary btn-sm hidden" type="reset">Reset</button>
                                    </form>
                                    <input type="button" onclick="saveUsers()" value="Save">
                                </div>

                                <div class="col">
                                    <div class="table-responsive">
                                        <div id="tableDiv" style="margin-top: 40px">
                                            Table will gentare here.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="modalHs" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-lg" style="background-color:white;">
                                <br>
                                <h4 align="center" class="modal-title center">Detail Home Service</h4>
                                <div class="modal-body" style="padding:20px">
                                <button type="button" class="close" data-dismiss="modal" onclick="resetFormUsers();"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                        <tr>
                                            <td>Pid</td>
                                            <td id="data_pid"></td>
                                        </tr>
                                        <tr>
                                            <td>Time</td>
                                            <td id="time_name"></td>
                                        </tr>
                                        <tr>
                                            <td>Time Selesai</td>
                                            <td id="time_selesai"></td>
                                        </tr>
                                        <tr>
                                            <td>Nama</td>
                                            <td id="nama"></td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td id="alamat"></td>
                                        </tr>
                                        <tr>
                                            <td>Nomor Telpon</td>
                                            <td id="no_telp"></td>
                                        </tr>
                                        <tr>
                                            <td>Dokter</td>
                                            <td id="dokter"></td>
                                        </tr>
                                        <tr>
                                            <td>Jumlah Pasien</td>
                                            <td id="jumlah_pasien"></td>
                                        </tr>
                                        <tr>
                                            <td>Paket</td>
                                            <td id="paket"></td>
                                        </tr>
                                        <tr>
                                            <td>Payment</td>
                                            <td id="pay_name"></td>
                                        </tr>
                                        <tr>
                                            <td>Petugas</td>
                                            <td id="nama_petugas"></td>
                                        </tr>
                                        <tr>
                                            <td>Petugas HS
                                            </td>
                                            <td id="nama_petugas_hs"></td>
                                        </tr>
                                        <tr>
                                            <td>TTD Pasien
                                            </td>
                                            <td id="ttd_pasien"></td>
                                        </tr>
                                    </table>
                                    <div id="diproses">
                                        <form action="#" id="form_masterhs">
                                        <label class="form-label">Waktu Selesai</label><br>
                                        <input type="checkbox" name="time_selesai" value="<?php echo date('Y-m-d H:i:s') ?>"> <br><br>
                                        <center><button type="button" class="btn btn-primary" onclick="saveSelesai();">Simpan</button></center>
                                        <button type="button" class="close" data-dismiss="modal" onclick="resetFormUsers();"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                        </form>
                                        <center><button type="button" class="btn btn-danger" onclick="cancel();">Batalkan</button></center>
                                    </div>
                                </div>
                            </div>
                        </div>


           <div class="col-md-12 inline" style="text-align: center;margin: 40px">

            <span id="rcorners">Selesai</span>
            <span id="rcorners3">Proses</span>
            <span >Batal = Hilang</span>


            </div>

                        <!-- End Master Users -->



                        <!-- Start Master Department-->
                        <div role="tabpanel" class="tab-pane fade in" id="masterdepartment">
                            <div class="header">
                                <h2>
                                    Data Master Department
                                    <button type="button" class="pull-right btn btn-primary" onclick="tambahDepartment();">Tambah</button>
                                </h2>
                            </div>
                            <div class="table-responsive">
                                <table id="table_masterdepartment" class="table table-bordered table-striped table-hover dataTable js-exportable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>

                            <div class="modal fade" id="modalDepartment" tabindex="-1" role="dialog">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Tambah Master Department</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form action="#" id="form_masterdepartment">
                                                <input type="hidden" readonly name="id_department" class="form-control">

                                                <label class="form-label">Name</label>
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="name" class="form-control">
                                                    </div>
                                                </div>

                                                <button class="btn btn-primary btn-sm hidden" type="reset">Reset</button>
                                            </form>
                                        </div>

                                        <div class="modal-footer">
                                            <input class="btn btn-primary btn-sm" type="button" onclick="saveDepartment()" value="Simpan">
                                            <input class="btn btn-danger btn-sm" type="button" data-dismiss="modal" value="Batal">
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- End Master Department -->

                         
                         


                    </div>
                </div>
            </div>
        </div>

<style>

 
/*The comment icon*/
.icon_comment:before {
    width: 16px;
    height: 16px;
    margin: 0 0 5px 0;
    padding: 2px;
    background-color: #8cab72;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    font-size: 12px;
    color: #fff;
}
 
/*The close icon*/
.icon_close:before {
    font-size: 16px;
    color: #999;
}
 
/*The close icon hover*/
.icon_close:hover:before {
    color: #777;
}
 
/*Notification alert box*/
.rrpowered_noti {
    width: 245px;
    margin: 50px;
    padding: 10px;
    position: absolute;
    top: 0;
    z-index: 99;
    background-color: #fff;
    line-height: 1.3em;
    color: #666;
    font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif;
    font-size: 12px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    -webkit-box-shadow: 0px 0px 5px 1px rgba(50, 50, 50, 0.35);
    -moz-box-shadow: 0px 0px 5px 1px rgba(50, 50, 50, 0.35);
    box-shadow: 0px 0px 5px 1px rgba(50, 50, 50, 0.35);
}
 
/*Notification alert box hover*/
.rrpowered_noti:hover {
    -webkit-box-shadow: 0px 0px 5px 1px rgba(50, 50, 50, 0.50);
    -moz-box-shadow: 0px 0px 5px 1px rgba(50, 50, 50, 0.50);
    box-shadow: 0px 0px 5px 1px rgba(50, 50, 50, 0.50);
    cursor: pointer;
}
 
/*Clearfix for clearing floats*/
.noti_clearfix:before, .noti_clearfix:after {
    content: '';
    display: table;
}
 
/*Clearfix for clearing floats*/
.noti_clearfix:after {
    clear: both;
    content: ".";
    display: block;
    font-size: 0;
    height: 0; 
    line-height: 0;
    visibility: hidden;
}
 
/*Float left*/ 
.noti_left {
    float: left;
}
 
/*Float right*/
.noti_right {
    float: right;
}
 
/*Thumbnail image*/
.rrpowered_noti_image {
    width: 30px;
    height: 30px;
    background-image: url('../images/thumb.jpg');
}
 
/*Notification alert box content*/
.rrpowered_noti_content p {
    width: 150px; 
    margin: 0;
    padding: 0 5px 10px 5px;
}
 
/*Ago text*/
.rrpowered_noti_time {
    color: #999;
}
 
/*Notification alert box close button*/
.rrpowered_noti_close {
    width: 16px;
    height: 16px;
}
</style>
<div class="rrpowered_notification"></div>
<script>
var timer = 10000, timeoutId;
var notiAlert = $(".rrpowered_noti");
var alerttext="";


/*The function to show notification alert box*/
function showAlert(text) {
	$(".rrpowered_notification").html('<div class="rrpowered_noti noti_clearfix"><div class="rrpowered_noti_image noti_left"></div><div class="rrpowered_noti_content noti_left"> <p><b>'+text+'</b> </p><span class="rrpowered_noti_time"><i class="icon_comment"></i> </span></div><div class="icon_close noti_right" id="closex">X</div></div>');
}

/*The function to start the timeout timer*/
function startTimer() {
	timeoutId = setTimeout(function() {
		$(".rrpowered_noti").hide();
	}, timer);
}

/*The function to stop the timeout timer*/
function stopTimer() {
	clearTimeout(timeoutId);
}

/*Call the stopTimer function on mouse enter and call the startTimer function on mouse leave*/
$(".rrpowered_noti").mouseenter(stopTimer).mouseleave(startTimer);

/*Close the notification alert box when close button is clicked*/
$(".rrpowered_notification").click(function() {
	$(".rrpowered_noti").hide();
});
</script>




        <br>
        <br>
            
            
       



<script type="text/javascript">
Date.prototype.addHours = function(h) {
    this.setTime(this.getTime() + (h*60*60*1000));
    return this;
}
Number.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
}

$(document).ready(function() {
resetFormUsers();
getDataUsers();
prepareTable(document.getElementById("myDate").value);
});



// START USERS

$("#table_masterusers");
var hs_data
var curr_id

getAvailableTime(document.getElementById("myDate").value);
	
function formatDate(date) {
  var monthNames = [
    "Januari", "Februari", "Maret",
    "April", "Mei", "Juni", "July",
    "Agustus", "September", "Oktober",
    "November", "Desember"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}

	function getYMD() {
		var today = new Date().addHours(9);
var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
		return date;
	}
	
	function setDMY() {
		var today = new Date().addHours(9);
		var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+str_pad(today.getDate());
		//console.log(date);
		return date;
	}
	function str_pad(n) {
		return String("00" + n).slice(-2);
	}

function prepareTable(date, is_alert = false) {
var url = '';
if($("#dateNow").html()!=formatDate(new Date().addHours(9))){
	$("#dateNow").html(formatDate(new Date().addHours(9)));
	url = "<?php echo base_url('homeservice/get_hs_timeline/') ?>" + getYMD();
	document.getElementById("myDate").value = getYMD();
	document.getElementById("myDate2").value = setDMY();
	is_alert=true;
}
else{
	document.getElementById("myDate").value = document.getElementById("myDate2").value;
	url = "<?php echo base_url('homeservice/get_hs_timeline/') ?>" + date;
}
$.ajax({
url: url,
dataType: "json",
method: "post",
data: {
       branch_id: <?php echo empty($this->uri->segment(3)) ? '""' : $this->uri->segment(3); ?>
},
success: function(result) {
var time = result.hs_time
var petugas = result.hs_initial_petugas_hs
var data = result.data
data = data ? data : []

data = Object.keys(data).map(key => {
    return data[key];
})

hs_data = data

alerttext = '';

var table_body = "<table align='center' class='table table-bordered table-striped table-hover dataTable js-exportable'  style='table-layout: fixed' width='100%' cellspacing='0'>"
table_body += `                      <thead>
<tr>`
table_body += `<center><th style='word-wrap:break-word;background-color:#b5aeae;color:white;'>Waktu / Petugas</th></center>`
time.forEach(element => {
table_body += `<th style='word-wrap:break-word;background-color:#b5aeae;color:white;text-align: center;'>${element.time_name.substring(0,5)}</th>`
});
table_body += '</tr>'

petugas.forEach(element => {
table_body += '<tr>'
table_body += `<td style='word-wrap:break-word;background-color:#00c292;color:white;'>${element.abbr_hs}${(element.name!=null && element.name!=''?' - '+element.name.substring(0,6)+'..':'')}</td>`
time.forEach(element2 => {
var found = false;
// data.forEach(element3 => {
// 	var explode = element3.multi_time_id.split(",");
// 	explode.forEach(multi_time => {
// 		if (element3.ptgshs_id == element.ptgshs_id && multi_time == element2.time_id && element3.status != 'Cancel') {
// 			found = true;
// 		}
// 	});
	
// if (element3.ptgshs_id == element.ptgshs_id && element3.time_id == element2.time_id && element3.status != 'Cancel') {
// var background = '';
// var selesai = '';
// switch (element3.status) {
// case 'Diproses':
// background = '#e1e144';
// break;
// case 'Cancel':
// background = '#f02424d6';
// break;
// case 'Selesai':
// background = '#7fd77f';
// selesai = element3.time_selesai;
// break;

// default:
// break;
// }




// if(element3.catatan!='')
// 	alerttext+=element3.nama+' : '+element3.catatan+'<br />';



// table_body += `

// <td colspan='${element3.colspan}' onclick='showHs(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p>${element3.nama}</p> <p>${element3.alamat}</p> <p>Catatan : ${element3.catatan}</p><br> ${selesai.substring(10,16)}</td>
// `
// found = true
// }
// });



data.forEach(element3 => {
    var explode = element3.multi_time_id.split(",");
    console.log("testing")
    console.log(element3);
    explode.forEach(multi_time => {
        // console.log(multi_time);
        if (element3.ptgshs_id == element.ptgshs_id && multi_time == element2.time_id && element3.deleted == 0 ) {
            found = true;
        }
    });

    if (element3.ptgshs_id == element.ptgshs_id && element3.time_id == element2.time_id && element3.deleted == 0) {
        console.log('masuk');
        var background = '';
        var selesai = '';
        switch (element3.status) {
            case 'Diproses':
                background = '#e1e144';
                break;
            case 'Cancel':
                background = '#f02424d6';
                break;
            case 'Booked':
                background = '#FFFDD0';
                break;
            case 'Selesai':
                background = '#7fd77f';
                selesai = element3.time_selesai;
                break;

            default:
                break;
        }

        if (element3.catatan != '')
            alerttext += element3.nama + ' : ' + element3.catatan + '<br />';

            if(element3.status == 'Booked'){
                table_body += `<td colspan='${element3.colspan}' onclick='showHs(${element3.id})'  style='word-wrap:break-word;background-color:${background}'><p style="font-size:10px">Booked</p></td>`
            }else{
                table_body += `<td colspan='${element3.colspan}' onclick='showHs(${element3.id})'  style='word-wrap:break-word;background-color:${background}'><p>${element3.nama}</p> <p>${element3.alamat}</p> <p>Catatan : ${element3.catatan}</p> <br> ${selesai.substring(10,16)}</td>`
            }
        found = true
    }
});



if(alerttext!='' && is_alert){
	showAlert('Catatan : <br />'+alerttext);
	startTimer();
}





if (!found) {
table_body += `
<td style='word-wrap:break-word;'></td>
`
}
});
table_body += '</tr>'
});

table_body += '</thead>'
table_body += '</table>'
console.log(table_body);
$('#tableDiv').html(table_body);
},
error: function(jqXHR, textStatus, errorThrown) {
location.reload(true)
console.log(jqXHR, textStatus, errorThrown);
}
});
}

function saveSelesai() {
url = "<?php echo base_url('admin/homeservice/setSelesai') ?>";

var fd = new FormData();
var other_data = $('#form_masterhs').serializeArray();
$.each(other_data, function(key, input) {
fd.append(input.name, input.value);
});
fd.append('status', 'Selesai')
fd.append('id', curr_id)

$.ajax({
url: url,
type: "POST",
data: fd,
dataType: 'json',
processData: false,
contentType: false,
success: function(result) {
swal({
title: 'Home Servie Data',
text: result.message,
type: 'success',
});
prepareTable(document.getElementById("myDate").value);
$("#modalHs").modal('hide');
},
error: function(jqXHR, textStatus, errorThrown) {
location.reload(true)
console.log(jqXHR, textStatus, errorThrown)
}
});
}

function showHs(data) {
console.log(hs_data, data)
hs_data.forEach(element => {
if (element.id == data) {
curr_id = element.id
Object.keys(element).forEach(function(k) {
$("#" + k).html(element[k]);

});
$("#data_pid").html(element.pid);
$("#time_selesai").html(element.time_name_selesai);
$("#modalHs").modal('show');

if (element.status != 'Diproses') {
$("#diproses").hide()
} else {

$("#diproses").show()
}
}
});
}


function getDataUsers() {
url = "<?php echo base_url('admin/homeservice/get_hs') ?>";
$('#table_masterusers').DataTable({
"order": [
[0, "desc"]
],
"ajax": url,
"columns": [{
"data": "no"
}, {
"data": "date"
}, {
"data": "pid"
}, {
"data": "time_name"
}, {
"data": "nama"
}, {
"data": "alamat"
}, {
"data": "no_telp"
}, {
"data": "dokter"
}, {
"data": "jumlah_pasien"
}, {
"data": "paket"
}, {
"data": "pay_name"
}, {
"data": "nama_petugas"
}, {
"data": "nama_petugas_hs"
}, {
"data": "ttd_pasien"
}, {
"data": "branch"
}, {
"data": "status"
}, {
"data": "creator"
}, {
"data": "created_date"
},
{
"width": "8%",
"mRender": function(index, type, data) {
html = '';
html += '<a href="#" onclick="hapusUsers(' + data.id + ');"><i class="notika-icon notika-trash"></i></a>';
return html;
}
},
],
"oLanguage": {
"sSearch": "Pencarian :",
"sZeroRecords": "Data tidak ditemukan.",
"sLengthMenu": "Tampilkan _MENU_ data",
"sEmptyTable": "Data tidak ditemukan.",
"sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data.",
"infoFiltered": "(dari total _MAX_ data)"
}
});
}

function resetFormUsers() {

$("#modalUsers .modal-title").text('Modal Data');
$('#form_masterusers').find('button[type=reset]').click();
$("#form_masterusers input").attr('disabled', false);
$("#form_masterusers select").attr('disabled', false);
$("#form_masterusers input[name='id_user']").val("");
$("#modalUsers .modal-footer button").show();
$("#modalUsers .modal-footer button.action").hide();
}

function reloadDataUsers() {
$("#modalUsers").modal('hide');
var table = $('#table_masterusers').DataTable();
table.ajax.reload();
}

function changeDate(value) {
document.getElementById("inputdate").value = value.value;
prepareTable(value.value,true);
}

function changeInputDate(value) {
getAvailableTime(value.value);
}

function getAvailableTime(value) {
url = "<?php echo base_url('homeservice/get_available_time') ?>";
$.ajax({
url: url,
dataType: "json",
method: "post",
data: {
date: value
},
success: function(result) {
var data = result.data
var $el = $(".time-select");
$el.empty(); // remove old options
$.each(data, function(key, value) {
$el.append($("<option></option>")
.attr("value", value.time_id).text(value.time_name.substring(0,5)));
});
},
error: function(jqXHR, textStatus, errorThrown) {
location.reload(true)
console.log(jqXHR, textStatus, errorThrown);
}
});
}

function tambahUsers() {
$("#modalUsers .modal-title").text('Tambah Data');
resetFormUsers();
$("#modalUsers").modal('show');
}

function editUsers(id) {
resetFormUsers();
url = "<?php echo base_url('admin/homeservice/edit_hs') ?>";
$.ajax({
url: url,
dataType: "json",
method: "post",
data: {
id: id
},
success: function(result) {
$("#modalUsers .modal-title").text('Edit Data');
$.each(result.data, function(index, value) {
$("input[name='" + index + "']").val(value);
$("select[name='" + index + "']").val(value);
$("input[type=date][name='" + index + "']").val(value);
})
$("#modalUsers").modal('show');
},
error: function(jqXHR, textStatus, errorThrown) {
location.reload(true)
console.log(jqXHR, textStatus, errorThrown);
}
});
}

function saveUsers() {
url = "<?php echo base_url('admin/homeservice/save_hs') ?>";

var fd = new FormData();
var other_data = $('#form_masterusers').serializeArray();
$.each(other_data, function(key, input) {
fd.append(input.name, input.value);
});

$.ajax({
url: url,
type: "POST",
data: fd,
dataType: 'json',
processData: false,
contentType: false,
success: function(result) {
swal({
title: 'Home Servie Data',
text: result.message,
type: 'success',
});
resetFormUsers();
prepareTable(document.getElementById("myDate").value);
},
error: function(jqXHR, textStatus, errorThrown) {
location.reload(true)
console.log(jqXHR, textStatus, errorThrown)
}
});
};

function cancel() {
var konfirmasi = confirm("Apakah anda yakin cancel ?");
if (konfirmasi == true) {
url = "<?php echo base_url('admin/homeservice/delete_hs') ?>";
$.ajax({
url: url,
dataType: "json",
method: "post",
data: {
id: curr_id
},
success: function(result) {
swal({
title: 'Home Data',
text: result.message,
type: 'success',
}, function() {
prepareTable(document.getElementById("myDate").value);
$("#modalHs").modal('hide');
});
},
error: function(jqXHR, textStatus, errorThrown) {
location.reload(true)
console.log(jqXHR, textStatus, errorThrown);
}
});
}
}

function hapusUsers(id) {
resetFormUsers();
var konfirmasi = confirm("Apakah anda yakin cancel ?");
if (konfirmasi == true) {
url = "<?php echo base_url('admin/homeservice/delete_hs') ?>";
$.ajax({
url: url,
dataType: "json",
method: "post",
data: {
id: id
},
success: function(result) {
swal({
title: 'Home Data',
text: result.message,
type: 'success',
}, function() {
reloadDataUsers();
});
},
error: function(jqXHR, textStatus, errorThrown) {
       location.reload(true);
console.log(jqXHR, textStatus, errorThrown);
}
});
}
}

function refreshSelectUsers() {
var select1 = $("#form_masterusers #id_user");

select1.html("");

$.ajax({
method: "post",
url: base_url + "admin/master_users/get_users",
success: function(resp) {
var listUsers = [];
try {
var json = JSON.parse(resp);
listUsers = json.data;
} catch (x) {
console.log("error parse to json", resp, x);
}
$.each(listUsers, function(index, users) {
select1.append($("<option/>").attr({
value: users.id_user
}).html(users.username));
});
}
});
}
	
setInterval(function(){ prepareTable(document.getElementById("myDate").value); }, 10000);


// END USERS



// START DEPARTMENT


// END DEPARTMENT
</script>



      <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
        <!-- bootstrap JS
============================================ -->
        <script src="<?php echo base_url() ?>assets/admin/js/bootstrap.min.js"></script>
        <!-- wow JS
============================================ -->
        <script src="<?php echo base_url() ?>assets/admin/js/wow.min.js"></script>
        <!-- price-slider JS
============================================ -->
        <script src="<?php echo base_url() ?>assets/admin/js/jquery-price-slider.js"></script>
        <!-- owl.carousel JS
============================================ -->
        <script src="<?php echo base_url() ?>assets/admin/js/owl.carousel.min.js"></script>
        <!-- scrollUp JS
============================================ -->
        <script src="<?php echo base_url() ?>assets/admin/js/jquery.scrollUp.min.js"></script>
        <!-- meanmenu JS
============================================ -->
        <script src="<?php echo base_url() ?>assets/admin/js/meanmenu/jquery.meanmenu.js"></script>
        <!-- counterup JS
============================================ -->
        <script src="<?php echo base_url() ?>assets/admin/js/counterup/jquery.counterup.min.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/js/counterup/waypoints.min.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/js/counterup/counterup-active.js"></script>
        <!-- mCustomScrollbar JS
============================================ -->
        <script src="<?php echo base_url() ?>assets/admin/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
        <!-- sparkline JS
============================================ -->
        <script src="<?php echo base_url() ?>assets/admin/js/sparkline/jquery.sparkline.min.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/js/sparkline/sparkline-active.js"></script>
        <!-- sparkline JS
============================================ -->
        <script src="<?php echo base_url() ?>assets/admin/js/flot/jquery.flot.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/js/flot/jquery.flot.resize.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/js/flot/curvedLines.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/js/flot/flot-active.js"></script>
        <!-- knob JS
============================================ -->
        <script src="<?php echo base_url() ?>assets/admin/js/knob/jquery.knob.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/js/knob/jquery.appear.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/js/knob/knob-active.js"></script>
        <!--  wave JS
============================================ -->
        <script src="<?php echo base_url() ?>assets/admin/js/wave/waves.min.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/js/wave/wave-active.js"></script>
        <!--  todo JS
============================================ -->
        <script src="<?php echo base_url() ?>assets/admin/js/todo/jquery.todo.js"></script>
        <!-- plugins JS
============================================ -->
        <script src="<?php echo base_url() ?>assets/admin/js/plugins.js"></script>
        <!--  Chat JS
============================================ -->
        <script src="<?php echo base_url() ?>assets/admin/js/chat/moment.min.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/js/chat/jquery.chat.js"></script>

        <!-- <script src="<?= base_url() ?>assets/admin/js/chosen/chosen.jquery.js"></script> -->
        <!-- <script src="<?= base_url() ?>assets/admin/js/bootstrap-select/bootstrap-select.js"></script> -->
        <!-- <script src='<?= base_url() ?>assets/admin/js/summernote/summernote-updated.min.js'></script> -->
        <!-- <script src='<?= base_url() ?>assets/admin/js/summernote/summernote-active.js'></script> -->
        <!-- main JS
============================================ -->
        <script src="<?php echo base_url() ?>assets/admin/js/main.js"></script>





        <!-- MIS OLD-->
        <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui/js/jquery-ui.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui/js/jquery-ui-autocomplete.min.js"></script>
        <!-- Slimscroll Plugin Js -->
        <script src="<?php echo base_url() ?>assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
        <!-- Waves Effect Plugin Js -->
        <script src="<?php echo base_url() ?>assets/admin/plugins/node-waves/waves.js"></script>
        <!-- Jquery CountTo Plugin Js -->
        <script src="<?php echo base_url() ?>assets/admin/plugins/jquery-countto/jquery.countTo.js"></script>
        <!-- Flot Charts Plugin Js -->
        <script src="<?php echo base_url() ?>assets/admin/plugins/flot-charts/jquery.flot.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/plugins/flot-charts/jquery.flot.resize.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/plugins/flot-charts/jquery.flot.pie.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/plugins/flot-charts/jquery.flot.categories.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/plugins/flot-charts/jquery.flot.time.js"></script>
        <!-- Custom Js -->
        <script src="<?php echo base_url() ?>assets/admin/js/admin.js"></script>
        <!-- Demo Js -->
        <script src="<?php echo base_url() ?>assets/admin/js/demo.js"></script>
        <!-- Jquery DataTable Plugin Js -->
        <!-- Data Table JS
		============================================ -->
        <script src="<?php echo base_url() ?>assets/admin/js/data-table/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/js/data-table/data-table-act.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/plugins/ckeditor/ckeditor.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/plugins/sweetalert/sweetalert.min.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/js/pagination/scripts-received.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/js/pagination/scripts-sent.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/js/pagination/scripts-archived.js"></script>
        <script src="<?php echo base_url() ?>assets/admin/js/pagination/scripts-draft.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/admin/plugins/momentjs/moment.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/admin/plugins/select2/select2.min.js"></script>
        <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
