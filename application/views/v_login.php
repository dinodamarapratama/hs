<!-- <div id="content_wrapper"> /mengakomodasi login noreload-->
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title>MIS APP | Login</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--- Favicon --->
    <link rel="icon" href="<?php echo base_url()?>assets/login/img/logo-biomedika.ico" type="image/x-icon"/>

    <!-- Site Icons -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/login/img/logo-biomedika.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="<?php echo base_url()?>assets/login/img/logo-biomedika.ico" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/biomedika_theme/css/bootstrap.min.css?v=001" />
    <!-- Site CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/biomedika_theme/css/style.css?v=001" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/biomedika_theme/css/login.css?v=001" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/biomedika_theme/css/toaster.css" rel="stylesheet">
    <link rel="manifest" href="./manifest.json" />


    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <style>
        .form-control::-webkit-input-placeholder { color: #999; } /* WebKit, Blink, Edge */
        .form-control:-moz-placeholder { color: #999; }  /* Mozilla Firefox 4 to 18 */
        .form-control::-moz-placeholder { color: #999; }  /* Mozilla Firefox 19+ */
        .form-control:-ms-input-placeholder { color: #999; }  /* Internet Explorer 10-11 */
        .form-control::-ms-input-placeholder { color: #999; }  /* Microsoft Edge */
    </style>


    <script type="text/javascript">
   function myFunction() {
            var x = document.getElementById("password");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }
  function isGood(password) {
      var password_strength = document.getElementById("password-text");

      //TextBox left blank.
      if (password.length == 0) {
        password_strength.innerHTML = "";
        return;
      }

      //Regular Expressions.
      var regex = new Array();
      regex.push("[A-Z]"); //Uppercase Alphabet.
      regex.push("[a-z]"); //Lowercase Alphabet.
      regex.push("[0-9]"); //Digit.
      //regex.push("[$@$!%*#?&]"); //Special Character.

      var passed = 0;

      //Validate for each Regular Expression.
      for (var i = 0; i < regex.length; i++) {
        if (new RegExp(regex[i]).test(password)) {
          passed++;
        }
      }

      //Display status.
      var strength = "";
      switch (passed) {
       
        case 1:
          strength = "<small class='progress-bar bg-danger' style='width: 40%'>Weak</small>";
          break;
        case 2:
          strength = "<small class='progress-bar bg-warning' style='width: 60%'>Medium</small>";
          break;
        case 3:
          strength = "<small class='progress-bar bg-success' style='width: 100%'>Strong</small>";
          break;

      }
      password_strength.innerHTML = strength;

    }
</script>
    
</head>

<body id="about" class="inner_page" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

    <!-- LOADER -->
     <!--<div id="preloader">
        <div class="loader">
            <img class="b-lazy" data-src="<?php echo base_url() ?>assets/biomedika_theme/img/loader.gif" alt="#" />
        </div>
    </div>-->
    <!-- end loader -->

    <!-- section -->

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-12 padding_0 hide-mobile bg-cream height-full">
                <div class="valign-center">
                    <div class="logo-title b-lazy"><img class="b-lazy" data-src="<?php echo base_url() ?>assets/biomedika_theme/img/login-logo.png" width="300px" height="90px"></div>
                    <br><br><br>
                    <div class="side-title">
                        <div></div>
                        <div>WELCOME TO</div>
                        <div>MIS APP</div>
                        <div><a href="https://www.instagram.com/biomedika.id/" target="_blank"><img class="b-lazy" data-src="<?php echo base_url() ?>assets/biomedika_theme/img/icon-1.png" width="25px" height="25px"></a>
                            <a href="https://biomedika.co.id" target="_blank"><img class="b-lazy" data-src="<?php echo base_url() ?>assets/biomedika_theme/img/icon-2.png" width="25px" height="25px"></a>
                            <a href="mailto:contact@biomeidka.co.id"><img class="b-lazy" data-src="<?php echo base_url() ?>assets/biomedika_theme/img/icon-3.png" width="30px" height="25px"></a></div>
                    </div>
                    <br><br>
                    <div class="icon-gelas"><img class="b-lazy" data-src="<?php echo base_url() ?>assets/biomedika_theme/img/image1.png" width="55px" height="90px"></div>
                </div>
            </div>




            <div class="col-lg-6 col-md-12 white_fonts layout_padding padding_left_right height-full">



                <div class="valign-center">
                    <div class="box-body"> 
                        <div class="box-login">
                             <!-- Page content -->
                <?php 
                if ($this->session->flashdata('sukses')) {
                ?>
                <div class="alert alert-success alert-dismissible" style="padding: 20px; margin-left: 10px; margin-right: 10px;">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" style="margin-right: 10px;">&times;</a>
                <strong><i class="fa fa-check"></i>   Success!</strong> <?= $this->session->flashdata('sukses'); ?>.
                </div>
                <?php
                }
                ?>


                <?php 
                if ($this->session->flashdata('valid_password')) {
                ?>
                <div class="alert alert-danger alert-dismissible" style="padding: 20px; margin-left: 10px; margin-right: 10px;">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" style="margin-right: 10px;">&times;</a>
                <strong><i class="fa fa-check"></i>   Error!</strong> <?= $this->session->flashdata('valid_password'); ?>.
                </div>
                <?php
                }
                ?>
                            <h3 class="small_heading"><br>USER LOGIN</h3>
                            <h4>Welcome Back</h4>
                            <!-- input-group.// -->
                            <form method="POST" action="#" onsubmit="return false;" id="myForm">
                               
                                <div class="row">
                                    <div class="col-md-12 mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"> <i class="fa fa-user text-success"></i> </span>
                                            </div>
                                            <input name="username" class="form-control" placeholder="NIP/USERNAME/NAME" type="text" autocomplete="off">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="col-md-12  mb-2">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"> <i class="fa fa-lock text-success"></i> </span>
                                            </div>
                                            <input class="form-control" placeholder="******" type="password" name="password" minlength="6"  id="myInput" autocomplete="off">
                                            <a class="icon-mata" onclick="myFunction()"><img class="b-lazy" data-src="<?php echo base_url() ?>assets/biomedika_theme/img/image3.png" width="20px" height="10px"></a>
                                        </div>
                                    </div>
                                    <div class="col-md-12 remember-me" style="text-align: left;">
                                        <label><input type="checkbox"> Remember me</label>
                                    </div>
                                    <?=$recaptcha?>
                                   
                                    <br><br>
                                    <br><br>
                                    <div class="col-md-12">
                                        <div class="submit-button text-center">
                                            <button class="btn btn-common" id="submit" type="submit" onclick="login();">LOGIN</button>
                                            <div id="msgSubmit" class="h3 text-center hidden"></div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                            </form>
                            <br><br> <br><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="modal-pwd" tabindex="-1" role="dialog">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Ganti Password</h4>


</div>
<div class="modal-body">
<form action="<?=base_url()?>welcome/changeLogin" method="POST" enctype="multipart/form-data">

<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

<div class="row">
<div class="col-lg-6">
<label class="form-label">NIP</label>
<div class="form-group form-float">
<div class="form-line">
<input type="text" name="NIP" autocomplete="off" class="form-control" required>
</div>
</div>
</div>

<div class="col-lg-6">
<label class="form-label">Password Baru</label>
<div class="form-group form-float">
<div class="form-line">

 <input class="form-control" placeholder="************************" type="password" name="password_new" id="password" onkeyup="isGood(this.value)" autocomplete="off" minlength="6">
  <a class="icon-mata" style="position: absolute;
    z-index: 9;
    right: 20px;
    top: 40px;" onclick="myFunction()"><img class="b-lazy" data-src="<?php echo base_url() ?>assets/biomedika_theme/img/image3.png" width="20px" height="10px"></a>
<small class="help-block" id="password-text"></small>
<label style="color:red">Minimal 6 digit *</label><br>
<label style="color:red">Huruf depan wajib kapital *</label><br>
<label style="color:red">Kombinasi huruf dan angka *</label><br>
</div>
</div>
</div>
</div>
<center>
<button class="btn btn-danger" type="reset">Reset</button>
<button type="submit" class="btn btn-success"> Simpan</button>
<button style="color:white" type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
</center>
</form>
</div>
<div class="modal-footer">
</div>
</div>
</div>

    <!-- end section -->

    <style>

    </style>
    <!-- ALL JS FILES -->
    <script src="<?php echo base_url() ?>assets/biomedika_theme/js/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>assets/biomedika_theme/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>assets/biomedika_theme/js/custom.js"></script>
    <script src="<?php echo base_url()?>assets/biomedika_theme/js/toaster.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/blazy/1.8.2/blazy.min.js" integrity="sha512-Yrd3VqXNBUzyCQWVBlL65mTdE1snypc9E3XnGJba0zJmxweyJAqDNp6XSARxxAO6hWdwMpKQOIGE5uvGdG0+Yw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    
    <!-- end:: Page -->
    <script type="text/javascript">
        $(document).ready(function(){

            var bLazy = new Blazy({
        // Options
   		 });

            var notifUrl = cekNotifUrl();
            if (notifUrl.length > 0) {
                /*set session storage*/
                alert('set notif');
                sessionStorage.history = notifUrl;
            }
        });

        function cekNotifUrl(){
            var params = '<?= $this->session->userdata("notif_url") ?>';
            return params;
        }

        function myFunction() {
            var x = document.getElementById("myInput");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }

        function login(){

         
            var recaptcha = document.forms["myForm"]["g-recaptcha-response"];
            recaptcha.required = true;
            recaptcha.oninvalid = function(e) {
            alertGagal('Mohon isi captha atau checklist im not a robot !');
            window.stop();
            
            }

            $('#submit').html('<i class="fa fa-spinner fa-spin"></i>  '); //please wait

            var username = $('input[name="username"]').val();
            var password = $('input[name="password"]').val();

            if (password == '123456') {

                $("#modal-pwd").modal('show');

            }

            else {

            if (username.length == 0 || password.length == 0) {
              alertGagal('Username dan password harus diisi !');
              
            }else{
              var login_url = '<?= base_url().'welcome/login'?>';
              $.post(login_url,{
                'username' : username,
                'password' : password,
                <?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo
                $this->security->get_csrf_hash(); ?>',
              },function(data){
                var rsp = $.parseJSON(data);
                if (rsp.code == 200) {
                  alertSukses(rsp.msg);
                  document.getElementById("myForm").reset()
                  /*redirect*/
                  setTimeout(function(){
                     
                     if (sessionStorage.history) {
                        rsp.redirect = sessionStorage.history;
                     }
                     /*if want to use not redirect page*/
                     // window.history.pushState(null, 'MIS APP', rsp.redirect);
                     // $('#content_wrapper').html('');
                     // $('#content_wrapper').load(rsp.redirect).trigger('create');
                     /**/
                     location.href = rsp.redirect;
                  },2100);

                }else{
                  alertGagal(rsp.msg);
                }
               
              });

            }
        }

            setTimeout(function(){
               $('#submit').html('login');
            },9000);

        }

        function alertConfig(){
          toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "rtl": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": 500,
            "hideDuration": 1000,
            "timeOut": 2000,
            "extendedTimeOut": 1000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          }
        }

        function alertSukses(text_pesan=''){
          alertConfig();
          toastr["success"](text_pesan);
        }

        function alertInfo(text_pesan=''){
          alertConfig();
          toastr["info"](text_pesan);
        }

        function alertGagal(text_pesan=''){
          alertConfig();
          toastr["error"](text_pesan);
        }

        function alertPeringatan(text_pesan=''){
          alertConfig();
          toastr["warning"](text_pesan);
        }

        <?php 
            if (isset($_GET['pesan'])) {
                if ($_GET['pesan'] == "logout") {
                    ?>
                    textMsg = 'Session anda telah berakhir. Terimakasih.'
                    alertInfo(textMsg);
                <?php
                }
                if ($_GET['pesan'] == "belumlogin") {
                    ?>
                    alertPeringatan('Silahkan login terlebih dahulu.');
                <?php
                }
            }
        ?>
    </script>
</body>

</html>
<!-- </div> /utk mengakomodasi login noreload-->