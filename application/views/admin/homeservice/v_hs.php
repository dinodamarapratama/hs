<?php
	$notification = $this->input->get('notif');
	if (!empty($notification)) {
		$id_user_temp = $this->session->userdata('id_user');
		$id_user = $this->session->userdata('id_user');
		$id_bagian = $this->session->userdata('id_bagian');
		$id_position = $this->session->userdata('id_position');
        $status = $this->session->userdata('STATUS');
	}else{
		$id_user_temp = $this->session->userdata('id_user');
		$id_user = $this->session->userdata('id_user');
		$id_bagian = $this->session->userdata('id_bagian');
		$id_position = $this->session->userdata('id_position');
        $status = $this->session->userdata('STATUS');
	}
?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/jquery-easyui/themes/metro/metro-green/easyui.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/jquery-easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/jquery-easyui/jquery-easyui-texteditor/texteditor.css">

<link href="<?php echo base_url() ?>assets/admin/plugins/select2/select2.min.css" rel="stylesheet">
<?php
if(isset($easy)){
?>
<script type="text/javascript" src="<?php echo base_url();?>assets/jquery-easyui/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/jquery-easyui/jquery.easyui.min.js"></script>

<!--<script type="text/javascript" src="http://www.jeasyui.com/easyui/plugins/jquery.messager.js"></script>-->


<script type="text/javascript" src="<?php echo base_url();?>assets/jquery-easyui/plugins/jquery.messager.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/jquery-easyui/datagrid-filter.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery-easyui/jquery-easyui-texteditor/jquery.texteditor.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/jquery-easyui/jquery.media.js"></script>

<script type="text/javascript">
var $eui = $.noConflict(true);
$eui('#tt').tabs({
	border:false
});
</script>

<?php
}
?>

<style type="text/css">

.modal-dialog {
    pointer-events: auto; 
}

#rcorners {
border-radius: 20px;
background: #7fd77f;
padding: 15px;
width: 10px;
height: 10px;
}

#rcorners2 {
border-radius: 20px;
background: #f02424d6;
padding: 15px;
width: 10px;
height: 10px;
}

#rcorners3 {
border-radius: 20px;
background: #e1e144;
padding: 15px;
width: 10px;
height: 10px;
}

.form-style-9{
/* max-width: 450px; */
background: #FAFAFA;
padding: 30px;
/*margin: 30px auto;*/
box-shadow: 1px 1px 25px rgba(0, 0, 0, 0.35);
border-radius: 10px;
border: 6px solid #008000;
}
.form-style-9 ul{
padding:0;
margin:0;
list-style:none;
}
.form-style-9 ul li{
display: block;
margin-bottom: 10px;
min-height: 35px;
}
.form-style-9 ul li  .field-style{
box-sizing: border-box;
-webkit-box-sizing: border-box;
-moz-box-sizing: border-box;
padding: 8px;
outline: none;
border: 1px solid #008000;
-webkit-transition: all 0.30s ease-in-out;
-moz-transition: all 0.30s ease-in-out;
-ms-transition: all 0.30s ease-in-out;
-o-transition: all 0.30s ease-in-out;

}.form-style-9 ul li  .field-style:focus{
box-shadow: 0 0 5px #B0CFE0;
border:1px solid #B0CFE0;
}
.form-style-9 ul li .field-split{
width: 49%;
}
.form-style-9 ul li .field-full{
width: 100%;
}
.form-style-9 ul li input.align-left{
float:left;
}
.form-style-9 ul li input.align-right{
float:right;
}
.form-style-9 ul li textarea{
width: 100%;
height: 100px;
}
.form-style-9 ul li input[type="button"],
.form-style-9 ul li input[type="submit"] {
-moz-box-shadow: inset 0px 1px 0px 0px #3985B1;
-webkit-box-shadow: inset 0px 1px 0px 0px #3985B1;
box-shadow: inset 0px 1px 0px 0px #3985B1;
background-color: #216288;
border: 1px solid #17445E;
display: inline-block;
cursor: pointer;
color: #FFFFFF;
padding: 8px 18px;
text-decoration: none;
font: 12px Arial, Helvetica, sans-serif;
}
.form-style-9 ul li input[type="button"]:hover,
.form-style-9 ul li input[type="submit"]:hover {
background: linear-gradient(to bottom, #2D77A2 5%, #337DA8 100%);
background-color: #28739E;
}

#invoice{
    padding: 30px;
}

.invoice {
    position: relative;
    background-color: #FFF;
    padding: 15px
}

.invoice header {
    padding: 10px 0;
    margin-bottom: 20px;
    border-bottom: 1px solid #3989c6
}

.invoice .company-details {
    text-align: right
}

.invoice .company-details .name {
    margin-top: 0;
    margin-bottom: 0
}

.invoice .contacts {
    margin-bottom: 20px
}

.invoice .invoice-to {
    text-align: left
}

.invoice .invoice-to .to {
    margin-top: 0;
    margin-bottom: 0
}

.invoice .invoice-details {
    text-align: right
}

.invoice .invoice-details .invoice-id {
    margin-top: 0;
    color: #3989c6
}

.invoice main {
    padding-bottom: 50px
}

.invoice main .thanks {
    margin-top: -100px;
    font-size: 2em;
    margin-bottom: 50px
}

.invoice main .notices {
    padding-left: 6px;
    border-left: 6px solid #00c292
}

.invoice main .notices .notice {
    font-size: 1.2em
}

.invoice table {
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
    margin-bottom: 20px
}

.invoice table td,.invoice table th {
    padding: 15px;
    background: #eee;
    border-bottom: 1px solid #fff
}

.invoice table th {
    white-space: nowrap;
    font-weight: 400;
    font-size: 16px
}

.invoice table td h3 {
    margin: 0;
    font-weight: 400;
    color: #3989c6;
    font-size: 1.2em
}

.invoice table .qty,.invoice table .total,.invoice table .unit {
    text-align: right;
    font-size: 1.2em
}

.invoice table .no {
    color: #fff;
    font-size: 1.6em;
    background: #3989c6
}

.invoice table .unit {
    background: #ddd
}

.invoice table .total {
    background: #3989c6;
    color: #fff
}

.invoice table tbody tr:last-child td {
    border: none
}

.invoice table tfoot td {
    background: 0 0;
    border-bottom: none;
    white-space: nowrap;
    text-align: right;
    padding: 10px 20px;
    font-size: 1.2em;
    border-top: 1px solid #aaa
}

.invoice table tfoot tr:first-child td {
    border-top: none
}

.invoice table tfoot tr:last-child td {
    color: #00c292;
    font-size: 1.4em;
    border-top: 1px solid #00c292
}

.invoice table tfoot tr td:first-child {
    border: none
}

.invoice footer {
    width: 100%;
    text-align: center;
    color: #777;
    border-top: 1px solid #aaa;
    padding: 8px 0
}

@media print {
    .invoice {
        font-size: 11px!important;
        overflow: hidden!important
    }

    .invoice footer {
        position: absolute;
        bottom: 10px;
        page-break-after: always
    }

    .invoice>div:last-child {
        page-break-before: always
    }
}


#overlay {
  position: absolute;
  top:0px;
  left:0px;
  width: 100%;
  height: 100%;
  display: none;
  background: black;
  opacity: .4;
  z-index: 9999;
}

/* spinner  */
.sk-chase {
  width: 40px;
  height: 40px;
  position: relative;
  animation: sk-chase 2.5s infinite linear both;
}

.sk-chase-dot {
  width: 100%;
  height: 100%;
  position: absolute;
  left: 0;
  top: 0; 
  animation: sk-chase-dot 2.0s infinite ease-in-out both; 
}

.sk-chase-dot:before {
  content: '';
  display: block;
  width: 25%;
  height: 25%;
  background-color: #fff;
  border-radius: 100%;
  animation: sk-chase-dot-before 2.0s infinite ease-in-out both; 
}

.sk-chase-dot:nth-child(1) { animation-delay: -1.1s; }
.sk-chase-dot:nth-child(2) { animation-delay: -1.0s; }
.sk-chase-dot:nth-child(3) { animation-delay: -0.9s; }
.sk-chase-dot:nth-child(4) { animation-delay: -0.8s; }
.sk-chase-dot:nth-child(5) { animation-delay: -0.7s; }
.sk-chase-dot:nth-child(6) { animation-delay: -0.6s; }
.sk-chase-dot:nth-child(1):before { animation-delay: -1.1s; }
.sk-chase-dot:nth-child(2):before { animation-delay: -1.0s; }
.sk-chase-dot:nth-child(3):before { animation-delay: -0.9s; }
.sk-chase-dot:nth-child(4):before { animation-delay: -0.8s; }
.sk-chase-dot:nth-child(5):before { animation-delay: -0.7s; }
.sk-chase-dot:nth-child(6):before { animation-delay: -0.6s; }

@keyframes sk-chase {
  100% { transform: rotate(360deg); } 
}

@keyframes sk-chase-dot {
  80%, 100% { transform: rotate(360deg); } 
}

@keyframes sk-chase-dot-before {
  50% {
    transform: scale(0.4); 
  } 100%, 0% {
    transform: scale(1.0); 
  } 
}

</style>


<!-- START HOME SERVICE -->
<!-- main-content opened -->
<div class="main-content horizontal-content">

<div id="overlay">
    <div class="w-100 d-flex justify-content-center align-items-center">
		 <div class="sk-chase">
  			<div class="sk-chase-dot"></div>
  			<div class="sk-chase-dot"></div>
  			<div class="sk-chase-dot"></div>
  			<div class="sk-chase-dot"></div>
  			<div class="sk-chase-dot"></div>
  			<div class="sk-chase-dot"></div>
		</div>
    </div>
</div>



<div title="HS" style="padding:20px;">


    <div id="toolbar1">
       
        <span> Branch :
        <select id="nyBranch" onchange="changeDate(document.getElementById('myDate').value,'b')" class="" style="max-width:200px"> <!-- this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value); -->
        <option value="">Pilih Branch</option>
        <?php foreach ($branch as $key => $p) {
        // echo '<option ' . ($selected_branch == $p->branch_id ? 'selected' : '') . ' value="'  . base_url('admin/homeservice/index/' . $p->branch_id) . '">' . $p->branch_name . '</option>';
        if ($p->branch_id == $branch_id){
            echo '<option value="'  . base_url('admin/homeservice/index/' . $p->branch_id) . '" selected=selected>' . $p->branch_name . '</option>';
        }else{
            echo '<option value="'  . base_url('admin/homeservice/index/' . $p->branch_id) . '">' . $p->branch_name . '</option>';
        }
        }
        ?>
        </select>
        </span>

      
        <span>Date :
        <input type="date" id="myDate" style="max-width:200px" name="date" class="changedate" onchange="changeDate(this,'d')" value="<?php echo date("Y-m-d"); ?>" required></span>
        
        <!-- <a data-toggle="modal" data-target="#tambahModal" id="addschedule" class="easyui-linkbutton" plain="true"><i class="fa fa-plus-circle text-success"></i> Tambah</a> -->
        <a data-toggle="modal" data-target="#exportModal" class="easyui-linkbutton" plain="true"><i class="fa fa-file-pdf-o text-success"></i> Export</a>
        <a data-toggle="modal" data-target="#lokasi" class="easyui-linkbutton" plain="true"><i class="fa fa-search-location text-success"></i>  Info Lokasi Petugas</a>
         <a ><i class="far fa-window-close"></i> Batal = Pindah ke data cancel</a>
        <span id="rcorners">Selesai</span>
        <span id="rcorners3">Proses</span>
    </div>

    <div style="margin:20px 0;"></div>

    <!-- stay -->
<div style="">
            <div id="tab" class="btn-group" data-toggle="buttons-radio">
              <a href="#prices2" class="btn btn-large" style="background-color:#e1e144" onclick='prepareTable(document.getElementById("myDate").value,false,false,false,"proses")'>Proses</a>
              <a href="#features2" class="btn btn-large" style="background-color:#7fd77f" onclick='prepareTable(document.getElementById("myDate").value,false,false,false,"selesai")'>Selesai</a>
              <a href="#requests2" class="btn btn-large" style="background-color:#f02424d6; color: #fff" onclick='prepareTable(document.getElementById("myDate").value,false,false,false,"cancel")'>Cancel</a>
              <a href="#requests2" class="btn btn-large" style="background-color:#1d9bf0; color: #fff" onclick='prepareTable(document.getElementById("myDate").value,false,false,false,"non_petugas")'>Booking Jauh/Non Petugas</a>
              <a href="#requests2" class="btn btn-large" style="background-color:#FFFDD0; color: #000" onclick='prepareTable(document.getElementById("myDate").value,false,false,false,"")'>Booked</a>
            </div>
</div>

<div class="easyui-layout" style="width:100%;height:1000px;">
  <div data-options="region:'center'" style="width:100%;" title="Home Service" toolbar="#toolbar1">
    <div id="tableDiv" style=""> Table will gentare here. </div>
    <div class="panel-heading active">
      <h4 class="panel-title">
        <!-- <a data-toggle="collapse" href="#collapse2">Lihat data booking = Untuk pasien yang memesan di tanggal/bulan yang jauh atau Non Petugas HS </a> -->
      </h4>
    </div>
    <div id="collapse2" class="panel-collapse collapse in">
      <ul class="list-group">
        <div id="booking-list">
          <li class="list-group-item">Tidak ada data</li>
        </div>
      </ul>
    </div>
    <div class="panel-heading active">
      <h4 class="panel-title">
        <a data-toggle="collapse" href="#collapse1">Lihat data cancel</a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse in">
      <ul class="list-group">
        <div id="cancel-list">
          <li class="list-group-item">Tidak ada data</li>
        </div>
      </ul>
    </div>
  </div>
</div>
</div>

</div>

<!-- Tambah Modal -->
<div class="modal fade" id="tambahModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Home Service</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#" id="form_masterhs" name="form_masterhs" class="form-style-9">
                    <ul>
                        <input type="hidden"  name="id" class="form-control">
						<input type="hidden"  name="id_multi" class="form-control">
                        <li>
                            <label>Cabang * (Wajib di pilih)</label><br>
                            <select  name="branch_id" class="field-style align-none" required>
                                <?php 
                                    foreach ($master_branch as $key => $b) {
                                        if ($b->branch_id == $branch_id){
                                            echo '<option value="' . $b->branch_id . '" selected=selected>' . $b->branch_name . '</option>';
                                        }else{
                                            echo '<option value="' . $b->branch_id . '">' . $b->branch_name . '</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </li>

                        <li>
                            <label>Tanggal Booking dan Jam Booking * (Wajib di pilih)</label><br>
                            
                            
                            
                            
                            <input type="date" name="date" class="field-style align-left text-uppercase changedate" placeholder="yyyy-mm-dd"  id="inputdate" onchange="changeDate(this)" value="<?php echo date("Y-m-d"); ?>"/ required>&nbsp;&nbsp;&nbsp;
                            
                            <!-- <input type="hidden" name="time_id_select" id="time_id_select" />
                            <input type="text" name="time_id_select2" id="time_id_select2"class="field-style time-select" placeholder="" readonly/>
                            sampai
                            <input type="hidden" name="time_id_sampai" id="time_id_select" />
                            <input type="text" name="time_id_sampai2" id="time_id_sampai2" class="field-style  time-select time-sampai" placeholder="" readonly/> -->
                            <select name="time_id" id="time_id_select" class="field-style align-right time-select" required>
                                <?php 
                                foreach ($hs_time as $key => $p) {
                                    echo '<option value="' . $p->time_id . '">' . $p->time_name . '</option>';
                                }
                                ?>
                            </select>
                            sampai
                            <select name="time_id_sampai" class="field-style align-right time-select time-sampai" required>
                                <?php 
                                    foreach ($hs_time as $key => $p) {
                                        echo '<option value="' . $p->time_id . '">' . $p->time_name . '</option>';
                                    }
                                ?>
                             </select>
                        </li>

                        

                         <li>
                            <label>Petugas HS * (Jika kosong laporan dengan BM)</label>
                            <select name="ptgshs_id" id="data_ptgshs_id" nama="data_ptgshs_id" class="field-style field-full align-none" onchange="getAvailableTime()" required>
                                <!-- <?php 
                                    foreach ($hs_initial_petugas_hs as $key => $p) {
                                        echo '<option value="' . $p->ptgshs_id . '">' . $p->abbr_hs . ' - ' . $p->name . '</option>';
                                    }
                                ?> -->
                            </select>
                        </li>

                        <!-- stay ganti petugas -->

                        <p>
                            <button class="btn btn-primary" id="gantiPetugas_btn" type="button" onclick="gantiPetugas()" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                Ganti Petugas
                            </button>
                        </p>
                        <div class="collapse" id="collapseExample">
                            <div class="card card-body">

                                <label class="form-label">Petugas HS Pengganti</label>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <select name="penggantiHS" class="form-control" id="select-usrhs" style="width: 100%">
                                        <?php foreach ($users as $key => $u) { ?>
                                            <option value="<?php echo $u->id_user ?>"><?php echo $u->name ?> <?php echo $u->last_name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>




                        
                        <li style="padding-bottom: : 20px">
                            <input type="text" name="pid" id="pid" class="field-style field-full align-none" placeholder="PID" />
                        </li>

                        

                        <li>
                            <input type="text" name="nama" id="nama" class="field-style field-split align-left text-uppercase" placeholder="Nama *">
                            <input type="text" name="no_telp" id="no_telp" class="field-style field-split align-right" placeholder="No Telp/Hp *">
                        </li>

                        <li>
                            <textarea name="alamat" id="input_alamat" class="field-style" placeholder="Alamat *" required></textarea>
                        </li>

                        <li>
                            <input type="text" name="dokter" class="field-style field-split align-left" placeholder="Dokter">
                            <input type="number" name="jumlah_pasien" id="jumlah_pasien" class="field-style field-split align-right" placeholder="Jumlah Pasien *" required>
                        </li>

                        <li>
                            <input type="text" name="pemeriksaan" class="field-style field-full align-none" placeholder="Pemeriksaan" />
                        </li>

                        <li>
                            <label>Petugas Penerima (Otomatis terisi tidak bisa di ganti)</label>
                            <input type="text" name="petugas_id" value="<?php echo $this->session->userdata('name');?>" class="field-style field-full align-none"  readonly>
                            <!-- <select name="petugas_id" class="field-style field-split align-left" id="select-pn" style="width: 100%" required="true">
                            <?php 
                                foreach ($hs_initial_petugas as $key => $p) {
                                    echo '<option value="' . $p->petugas_id . '" class="form-control">' . $p->name . '</option>';
                                }
                            ?>
                            </select>-->
                        </li>

                        <li>
                            <textarea name="catatan" id="catatanarea" class="field-style" placeholder="Catatan"></textarea>
                        </li>

                        <label class="form-label">Pembayaran</label>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <?php 
                                foreach ($hs_payment as $key => $p) { 
                                ?>
                                    <?php echo '<input type="radio" name="pay_id" value="' . $p->pay_id . '" checked>&nbsp;' . $p->pay_name . ' &nbsp;'; ?>
                                <?php 
                                } 
                                ?>
                            </div>
                        </div>
                        <li>
                            <center>
                                <button type="button" class="btn btn-success btn-with-icon btn-block btn-compose" onclick="validateForm()">Simpan</button>
                                <button class="btn btn-success btn-with-icon btn-block btn-compose" type="reset">Reset</button>
                            </center>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Tambah Modal -->

<!-- Export Modal -->
<div class="modal fade" id="exportModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Export Data Home Service</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="formExport" method="POST" action="<?php echo base_url('admin/homeservice/exportv2') ?>">
          <label for="inputCabang">Cabang</label>
          <div class="form-group">
            <select class="form-control" name="cabang" id="inputCabang" onchange="$('#cabang_name').val($('#inputCabang option:selected').html())" required style="width:100%;">
              <option value="">Pilih cabang</option>
              <?php
              foreach ($branch as $branch) {
                echo "<option value='$branch->branch_id'>$branch->branch_name</option>";
              }
              ?>
            </select>
          </div>
          <input id="cabang_name" type="hidden" name="cabang_name" value="">
          <div class="form-group">
            <label for="inputDari">Dari</label>
            <input type="date" name="dari" class="form-control" id="inputDari" placeholder="Dari" required>
          </div>
          <div class="form-group">
            <label for="inputSampai">Sampai</label>
            <input type="date" name="sampai" class="form-control" id="inputSampai" placeholder="Sampai" required>
          </div>
          <button type="submit" class="btn btn-success" id="buttonPilih">Export</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Export Modal -->


<!-- Modal -->
<div id="lokasi" class="modal fade" role="dialog">
<div class="modal-dialog modal-lg">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title" style="color:white">Info Lokasi Petugas Home Service</h4>
</div>
<div class="modal-body">
<div class="table-responsive">
<table id="table_lokasi" class="table table-bordered table-striped table-hover dataTable js-exportable" width="100%" cellspacing="0">
<thead>
<tr>
<th>Jalur Name</th>
<th>Lokasi</th>
</tr>
</thead>
</table>
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>

</div>
</div>

</div>




<div class="modal fade" id="modalHs" tabindex="-1" role="dialog" style="padding: 50px">
<div class="modal-dialog modal-lg" style="background-color:white;">
<br>
<h4 align="center" class="modal-title center">Detail Home Service</h4>
<div class="modal-body" >
<button type="button" class="close" data-dismiss="modal" onclick="resetFormhs();"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
</div>

<div id="invoice">

<!-- <div class="toolbar hidden-print">
<div class="text-right">
<button id="printInvoice" class="btn btn-info"><i class="fa fa-print"></i> Print</button>
<button class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Export as PDF</button> 
</div>-->
<hr>
</div>
<div class="invoice overflow-auto">
<div style="min-width: 600px">

<main>
<div class="row contacts">
<div class="col invoice-to">
<div>
<span style="text-align: left">Nama Pasien : <span style="text-align: left;font-weight: bold;" id="data_nama"></span>
<span class="pull-right">Petugas HS : <span style=";font-weight: bold;" id="nama_petugas_hs"></span></span><br>
<span style=";">Alamat : <span style="text-align: left;font-weight: bold;" id="data_alamat"></span></span><br>
<span class="">No Telp : <span style="text-align: left;font-weight: bold;" id="data_no_telp"></span></span>

</div>
</div>


</div>
<table border="0" cellspacing="0" cellpadding="0">
<thead>
<tr>
<th >Jam Pemesanan</th>
<th >Dokter</th>
<th >Jumlah Pasien</th>
<th >Pemeriksaan</th>
<th >Jam Selesai</th>
</tr>
</thead>
<tbody>
<tr>
<td id="time_name"></td>
<td id="dokter"></td>
<td id="jumlah_pasien"></td>
<td id="pemeriksaan"></td>
<td id="time_selesai"></td>
</tr>
</tbody>
<tfoot>
<tr>
<td colspan="2"></td>
<td colspan="2">Pembayaran</td>
<td id="pay_name"></td>
</tr>

</tfoot>
</table>

<div class="notices">
<div>Petugas Penerima :</div>
<div class="notice" id="nama_petugas"></div>
</div>
</main>
<footer>
<div id="diproses">
<form action="#" id="form_masterhs">
<label class="form-label">Waktu Selesai</label><br>
<input type="checkbox" name="time_selesai" value="<?php echo date('Y-m-d H:i:s') ?>">
<center>
<button type="button" class="btn btn-primary" onclick="saveSelesai();">Simpan</button>
<button type="button" class="btn btn-warning" onclick="edit();">Edit</button>
</center>
<button type="button" class="close" data-dismiss="modal" onclick="resetFormhs();"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
</form>
<center><button type="button" class="btn btn-danger" onclick="cancel();">Batalkan</button></center>
</div>

<div id="booking_status">
<center><button type="button" class="btn btn-danger" onclick="cancel_booking();">Batalkan Booking</button></center>
</div>


</footer>



</div>
<!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
<div></div>
</div>
</div>

<script type="text/javascript">
$('#printInvoice').click(function(){
Popup($('.invoice')[0].outerHTML);
function Popup(data)
{
window.print();
return true;
}
});
</script>


</div>
</div>
</div>
</div>
</div>
</div>

<!-- END HOME SERVICE -->

<style>
/*The comment icon*/
.icon_comment:before {
    width: 16px;
    height: 16px;
    margin: 0 0 5px 0;
    padding: 2px;
    background-color: #8cab72;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    font-size: 12px;
    color: #fff;
}
 
/*The close icon*/
.icon_close:before {
    font-size: 16px;
    color: #999;
}
 
/*The close icon hover*/
.icon_close:hover:before {
    color: #777;
}
 
/*Notification alert box*/
.rrpowered_noti {
    width: 245px;
    margin: 50px;
    padding: 10px;
    position: absolute;
    top: 250;
    z-index: 99;
    background-color: #fff;
    line-height: 1.3em;
    color: #666;
    font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif;
    font-size: 12px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    -webkit-box-shadow: 0px 0px 5px 1px rgba(50, 50, 50, 0.35);
    -moz-box-shadow: 0px 0px 5px 1px rgba(50, 50, 50, 0.35);
    box-shadow: 0px 0px 5px 1px rgba(50, 50, 50, 0.35);
}
 
/*Notification alert box hover*/
.rrpowered_noti:hover {
    -webkit-box-shadow: 0px 0px 5px 1px rgba(50, 50, 50, 0.50);
    -moz-box-shadow: 0px 0px 5px 1px rgba(50, 50, 50, 0.50);
    box-shadow: 0px 0px 5px 1px rgba(50, 50, 50, 0.50);
    cursor: pointer;
}
 
/*Clearfix for clearing floats*/
.noti_clearfix:before, .noti_clearfix:after {
    content: '';
    display: table;
}
 
/*Clearfix for clearing floats*/
.noti_clearfix:after {
    clear: both;
    content: ".";
    display: block;
    font-size: 0;
    height: 0; 
    line-height: 0;
    visibility: hidden;
}
 
/*Float left*/ 
.noti_left {
    float: left;
}
 
/*Float right*/
.noti_right {
    float: right;
}
 
/*Thumbnail image*/
.rrpowered_noti_image {
    width: 30px;
    height: 30px;
    background-image: url('../images/thumb.jpg');
}
 
/*Notification alert box content*/
.rrpowered_noti_content p {
    width: 150px; 
    margin: 0;
    padding: 0 5px 10px 5px;
}
 
/*Ago text*/
.rrpowered_noti_time {
    color: #999;
}
 
/*Notification alert box close button*/
.rrpowered_noti_close {
    width: 16px;
    height: 16px;
}
</style>

<div class="rrpowered_notification"></div>

<script>
var timer = 10000, timeoutId;
var notiAlert = $(".rrpowered_noti");
var alerttext="";


/*The function to show notification alert box*/
function showAlert(text) {
	$(".rrpowered_notification").html('<div class="rrpowered_noti noti_clearfix"><div class="rrpowered_noti_image noti_left"></div><div class="rrpowered_noti_content noti_left"> <p><b>'+text+'</b> </p><span class="rrpowered_noti_time"><i class="icon_comment"></i> </span></div><div class="icon_close noti_right" id="closex">X</div></div>');
}

/*The function to start the timeout timer*/
function startTimer() {
	timeoutId = setTimeout(function() {
		$(".rrpowered_noti").hide();
	}, timer);
}

/*The function to stop the timeout timer*/
function stopTimer() {
	clearTimeout(timeoutId);
}

/*Call the stopTimer function on mouse enter and call the startTimer function on mouse leave*/
$(".rrpowered_noti").mouseenter(stopTimer).mouseleave(startTimer);

/*Close the notification alert box when close button is clicked*/
$(".rrpowered_notification").click(function() {
	$(".rrpowered_noti").hide();
});
</script>



<script type="text/javascript">



var alerttext="";
// Stay
$(document).ready( function () {

var id_notif = '<?= isset($_GET["notif"]) ? $_GET["notif"] : "" ?>';
var date_notif = '<?= isset($_GET["date"]) ? $_GET["date"] : "" ?>';
var branch_id_notif = '<?= isset($_GET["branch_id"]) ? $_GET["branch_id"] : "" ?>';
var notif_type = '<?= isset($_GET["notif_type"]) ? $_GET["notif_type"] : "" ?>';

if (id_notif.length > 0) {
	/*get view data received*/
	// viewData(id_notif,date_notif,branch_id_notif);

    if (notif_type == 'bookinghs'){
        setTimeout(function() {
            viewData(id_notif,date_notif,branch_id_notif);
            changeDate(date_notif,'b')
        }, 1000);
        
        setTimeout(function() {
            editBookHS(id_notif);
        }, 2000);
    }else if (notif_type == 'homeservice'){
        setTimeout(function() {
            viewData(id_notif,date_notif,branch_id_notif);
        }, 1000);

        setTimeout(function() {
            showHsMulti(id_notif);
        }, 2000);

    }else{}

    
    window.history.pushState({}, "<?php echo base_url();?>", "homeservice");
}




var modal_add = $('#tambahModal');

$('#tambahModal #select-usrhs').select2({
dropdownParent: modal_add,
placeholder: 'Select User'
});
$('#tambahModal .select2-search__field').css({
width: '100%'
});


getLokasi();


// var hs = $('#collapseExample');

// $('#collapseExample #select-pn').select2({
// dropdownParent: hs,
// placeholder: 'Select User'
// });
// $('#collapseExample	 .select2-search__field').css({
// width: '200px'
// });

$('#addschedule').click(function(){
	getAvailableTime(document.getElementById("myDate").value);
});

});


function onModal() {
    document.getElementById("overlay").style.display = "flex";
}

function offModal() {
    document.getElementById("overlay").style.display = "none";
}










var modeGantiPetugas = false;

function gantiPetugas() {
    modeGantiPetugas = !modeGantiPetugas;
    if (modeGantiPetugas){
        $('#gantiPetugas_btn').html('Batal');
        $('#gantiPetugas_btn').removeClass('btn-primary').addClass('btn-danger');
    }else{
        $('#gantiPetugas_btn').html('Ganti Petugas');
        $('#gantiPetugas_btn').removeClass('btn-danger').addClass('btn-primary');
    }
    console.log(modeGantiPetugas);
}


$("#table_masterhs");
$("#table_lokasi");

function getLokasi()
{
url = "<?php echo base_url('admin/homeservice/get_lokasi') ?>";
$('#table_lokasi').DataTable({
scrollCollapse: true,
"ajax": url,
"columns": [
{ "data": "abbr_name"},
{ "data": "lokasi"},
],
"oLanguage": {
"sSearch": "Pencarian :",
"sZeroRecords": "Data tidak ditemukan.",
"sLengthMenu": "Tampilkan _MENU_ data",
"sEmptyTable": "Data tidak ditemukan.",
"sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data.",
"infoFiltered": "(dari total _MAX_ data)"
}
});
}

var hs_data
var curr_id
var ptgshs

getAvailableTime(document.getElementById("myDate").value);

function viewData(id,date,branch) {
	// alert(branch);
	//console.log(obj);
	if(id!=null){
		document.getElementById("nyBranch").selectedIndex = branch;
		prepareTable(date,false,false,id);
		//console.log('AAA');
		//console.log(hs_data);
		//showHsMulti(id);
	}
}

var currentType_hs


function prepareTable(date, baru, is_alert = false, notif_id=false,type) {
var branch = document.getElementById("nyBranch").selectedIndex;	
console.log("dddd",branch);
// alert(date);

// stay
var id_position = '<?= $id_position ?>';
var ekspedisi = false;

    if (['22'].includes(id_position)) {
        ekspedisi = true;
    }

// alert(type);
if (type != undefined){
    currentType_hs = type;
}else{
    currentType_hs = ""
}	

url = "<?php echo base_url('admin/homeservice/get_hs_timeline/') ?>" + date+'/'+branch;
$.ajax({
url: url,
dataType: "json",
method: "post",
data: {

},
success: function(result) {
var time = result.hs_time
console.log("yoga",time);
var petugas = result.hs_initial_petugas_hs
var branch_id = result.branch_id
var data = result.data
data = data ? data : []

data = Object.keys(data).map(key => {
    return data[key];
})

hs_data = data
console.log('BBBB');
console.log(petugas);
alerttext = '';

var table_body = "<table align='center' class='table table-bordered table-striped table-hover dataTable js-exportable'  style='table-layout: fixed' width='100%' cellspacing='0'>"
table_body += `                      <thead>
<tr>`
table_body += `<th style='word-wrap:break-word;background-color:#b5aeae;color:white;'>Waktu / Petugas</th>`
time.forEach(element => {
table_body += `<th style='word-wrap:break-word;background-color:#b5aeae;color:white;text-align: center;'>${element.time_name.substring(0,5)}</th>`
});
table_body += '</tr>'

petugas.forEach(element => {
table_body += '<tr>'
table_body += `<td style='word-wrap:break-word;background-color:#00c292;color:white;'>${element.abbr_hs}${(element.name!=null && element.name!=''?' - '+element.name.substring(0,6)+'..':'')}</td>`
time.forEach(element2 => {
var found = false;


if (currentType_hs == "proses"){
    data.forEach(element3 => {
    var explode = element3.multi_time_id.split(",");
    // console.log("testing")
    // console.log(element3.multi_time_id);
    explode.forEach(multi_time => {
        // console.log(multi_time);
        if (element3.ptgshs_id == element.ptgshs_id && multi_time == element2.time_id && element3.deleted == 0 && (element3.status == 'Cancel' || element3.status == 'Selesai' || element3.status == 'Diproses' || element3.status == 'Booked')) {
            found = true;
        }
    });

    if (element3.ptgshs_id == element.ptgshs_id && element3.time_id == element2.time_id && element3.deleted == 0 && (element3.status == 'Cancel' || element3.status == 'Selesai' || element3.status == 'Diproses' || element3.status == 'Booked')) {
        console.log('masuk');
        var background = '';
        var selesai = '';
        switch (element3.status) {
            case 'Diproses':
                background = '#e1e144';
                break;
            case 'Cancel':
                background = '#FFFDD0';
                break;
            case 'Booked':
                background = '#FFFDD0';
                break;
            case 'Selesai':
                background = '#FFFDD0';
                selesai = element3.time_selesai;
                break;
            default:
                break;
        }

        if (element3.catatan != '')
            alerttext += element3.nama + ' : ' + element3.catatan + '<br />';

            // stay
        if (ekspedisi){
            if(element3.status == 'Booked'){
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p style="font-size:10px">Booked</p></td>`
            }else{
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p><p>${element3.alamat}</p><br> ${selesai.substring(10,16)}</td>`
            }
        }else{
            if(element3.status == 'Booked'){
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p style="font-size:10px">Booked</p></td>`
            }else{
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p>${element3.nama}</p> <p>${element3.alamat}</p> <p>Catatan : ${element3.catatan}</p> <br> ${selesai.substring(10,16)}</td>`
            }
        }
        
        found = true
    }
});
}else if (currentType_hs == "selesai"){
    data.forEach(element3 => {
    var explode = element3.multi_time_id.split(",");
    // console.log("testing")
    // console.log(element3.multi_time_id);
    explode.forEach(multi_time => {
        // console.log(multi_time);
        if (element3.ptgshs_id == element.ptgshs_id && multi_time == element2.time_id && element3.deleted == 0 && (element3.status == 'Cancel' || element3.status == 'Selesai' || element3.status == 'Diproses' || element3.status == 'Booked')) {
            found = true;
        }
    });

    if (element3.ptgshs_id == element.ptgshs_id && element3.time_id == element2.time_id && element3.deleted == 0 && (element3.status == 'Cancel' || element3.status == 'Selesai' || element3.status == 'Diproses' || element3.status == 'Booked')) {
        console.log('masuk');
        var background = '';
        var selesai = '';
        switch (element3.status) {
            case 'Diproses':
                background = '#FFFDD0';
                break;
            case 'Cancel':
                background = '#FFFDD0';
                break;
            case 'Booked':
                background = '#FFFDD0';
                break;
            case 'Selesai':
                background = '#7fd77f';
                selesai = element3.time_selesai;
                break;
            default:
                break;
        }

        if (element3.catatan != '')
            alerttext += element3.nama + ' : ' + element3.catatan + '<br />';

        if (ekspedisi){
            if(element3.status == 'Booked'){
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p style="font-size:10px">Booked</p></td>`
            }else{
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p><p>${element3.alamat}</p><br> ${selesai.substring(10,16)}</td>`
            }
        }else{
            if(element3.status == 'Booked'){
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p style="font-size:10px">Booked</p></td>`
            }else{
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p>${element3.nama}</p> <p>${element3.alamat}</p> <p>Catatan : ${element3.catatan}</p> <br> ${selesai.substring(10,16)}</td>`
            }
        }
        found = true
    }
});
}else if (currentType_hs == "cancel"){
    data.forEach(element3 => {
    var explode = element3.multi_time_id.split(",");
    // console.log("testing")
    // console.log(element3.multi_time_id);
    explode.forEach(multi_time => {
        // console.log(multi_time);
        if (element3.ptgshs_id == element.ptgshs_id && multi_time == element2.time_id && element3.deleted == 0 && (element3.status == 'Cancel' || element3.status == 'Selesai' || element3.status == 'Diproses' || element3.status == 'Booked')) {
            found = true;
        }
    });

    if (element3.ptgshs_id == element.ptgshs_id && element3.time_id == element2.time_id && element3.deleted == 0 && (element3.status == 'Cancel' || element3.status == 'Selesai' || element3.status == 'Diproses' || element3.status == 'Booked')) {
        console.log('masuk');
        var background = '';
        var selesai = '';
        switch (element3.status) {
            case 'Diproses':
                background = '#FFFDD0';
                break;
            case 'Cancel':
                background = '#f02424d6';
                break;
            case 'Booked':
                background = '#FFFDD0';
                break;
            case 'Selesai':
                background = '#FFFDD0';
                selesai = element3.time_selesai;
                break;

            default:
                break;
        }

        if (element3.catatan != '')
            alerttext += element3.nama + ' : ' + element3.catatan + '<br />';

        if (ekspedisi){
            if(element3.status == 'Booked'){
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p style="font-size:10px">Booked</p></td>`
            }else{
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p><p>${element3.alamat}</p><br> ${selesai.substring(10,16)}</td>`
            }
        }else{
            if(element3.status == 'Booked'){
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p style="font-size:10px">Booked</p></td>`
            }else{
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p>${element3.nama}</p> <p>${element3.alamat}</p> <p>Catatan : ${element3.catatan}</p> <br> ${selesai.substring(10,16)}</td>`
                // table_body += `<td style='word-wrap:break-word;'><button onclick='bookhs(${element2.time_id},"${element.abbr_hs}","${element.name}","${element.ptgshs_id}")' type="button" class="btn btn-dark">Book Now</button></td>`
            }
        }
        found = true
    }
}); 
}else if (currentType_hs == "non_petugas"){
    data.forEach(element3 => {
    var explode = element3.multi_time_id.split(",");
    // console.log("testing")
    // console.log(element3.multi_time_id);
    explode.forEach(multi_time => {
        // console.log(multi_time);
        if (!element3.nama_petugas_hs && element3.ptgshs_id == element.ptgshs_id && multi_time == element2.time_id && element3.deleted == 0 && (element3.status == 'Cancel' || element3.status == 'Selesai' || element3.status == 'Diproses' || element3.status == 'Booked')) {
            found = true;
        }
    });

    if (element3.ptgshs_id == element.ptgshs_id && element3.time_id == element2.time_id && element3.deleted == 0 && (element3.status == 'Cancel' || element3.status == 'Selesai' || element3.status == 'Diproses' || element3.status == 'Booked')) {
        
        if(!element3.nama_petugas_hs){
        console.log('masuk');
        var background = '';
        var selesai = '';
        switch (element3.status) {
            case 'Diproses':
                background = '#1d9bf0';
                break;
            case 'Cancel':
                background = '#FFFDD0';
                break;
            case 'Booked':
                background = '#FFFDD0';
                break;
            case 'Selesai':
                background = '#FFFDD0';
                selesai = element3.time_selesai;
                break;

            default:
                break;
        }

        if (element3.catatan != '')
            alerttext += element3.nama + ' : ' + element3.catatan + '<br />';

        if (ekspedisi){
            if(element3.status == 'Booked'){
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p style="font-size:10px">Booked</p></td>`
            }else{
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p><p>${element3.alamat}</p><br> ${selesai.substring(10,16)}</td>`
            }
        }else{
            if(element3.status == 'Booked'){
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p style="font-size:10px">Booked</p></td>`
            }else{
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p>${element3.nama}</p> <p>${element3.alamat}</p> <p>Catatan : ${element3.catatan}</p> <br> ${selesai.substring(10,16)}</td>`
            }
        }

        found = true
    }else{

        console.log('masuk');
        var background = '';
        var selesai = '';
        switch (element3.status) {
            case 'Diproses':
                background = '#FFFDD0';
                break;
            case 'Cancel':
                background = '#FFFDD0';
                break;
            case 'Booked':
                background = '#FFFDD0';
                break;
            case 'Selesai':
                background = '#FFFDD0';
                selesai = element3.time_selesai;
                break;

            default:
                break;
        }

        if (element3.catatan != '')
            alerttext += element3.nama + ' : ' + element3.catatan + '<br />';

        if (ekspedisi){
            if(element3.status == 'Booked'){
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p style="font-size:10px">Booked</p></td>`
            }else{
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p><p>${element3.alamat}</p><br> ${selesai.substring(10,16)}</td>`
            }
        }else{
            if(element3.status == 'Booked'){
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p style="font-size:10px">Booked</p></td>`
            }else{
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p>${element3.nama}</p> <p>${element3.alamat}</p> <p>Catatan : ${element3.catatan}</p> <br> ${selesai.substring(10,16)}</td>`
            }
        }

        found = true

    }
    }
});
}else{

    data.forEach(element3 => {
    var explode = element3.multi_time_id.split(",");
    console.log("testing")
    console.log(element3);
    explode.forEach(multi_time => {
        // console.log(multi_time);
        if (element3.ptgshs_id == element.ptgshs_id && multi_time == element2.time_id && element3.deleted == 0 ) {
            found = true;
        }
    });

    if (element3.ptgshs_id == element.ptgshs_id && element3.time_id == element2.time_id && element3.deleted == 0) {
        console.log('masuk');
        var background = '';
        var selesai = '';
        switch (element3.status) {
            case 'Diproses':
                background = '#e1e144';
                break;
            case 'Cancel':
                background = '#f02424d6';
                break;
            case 'Booked':
                background = '#FFFDD0';
                break;
            case 'Selesai':
                background = '#7fd77f';
                selesai = element3.time_selesai;
                break;

            default:
                break;
        }

        if (element3.catatan != '')
            alerttext += element3.nama + ' : ' + element3.catatan + '<br />';

        if (ekspedisi){
            if(element3.status == 'Booked'){
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p style="font-size:10px">Booked</p></td>`
            }else{
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p><p>${element3.alamat}</p><br> ${selesai.substring(10,16)}</td>`
            }
        }else{
            if(element3.status == 'Booked'){
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p style="font-size:10px">Booked</p></td>`
            }else{
                table_body += `<td colspan='${element3.colspan}' onclick='showHsMulti(${element3.id})' style='word-wrap:break-word;background-color:${background}'><p>${element3.nama}</p> <p>${element3.alamat}</p> <p>Catatan : ${element3.catatan}</p> <br> ${selesai.substring(10,16)}</td>`
            }
        }
        found = true
    }
});

}


if (alerttext != '' && is_alert) {
    showAlert('Catatan : <br />' + alerttext);
    startTimer();
}

// stay
// console.log("oya",element)

if (!found) {
    table_body += `<td style='word-wrap:break-word;'><button onclick='bookhs(${element2.time_id},"${element.abbr_hs}","${element.name}","${element.ptgshs_id}")' type="button" class="btn btn-dark">Book Now</button></td>`
}
});
table_body += '</tr>'
});

table_body += '</thead>'
table_body += '</table>'
//console.log(table_body);
$('#tableDiv').html(table_body);

var cancel_body = '';
data.forEach(element => {
    console.log("padi", element)
    // if (element.status == 'Cancel' && element.branch_id == branch_id && element.deleted == "1") {
    if (element.status == 'Cancel' && element.branch_id == branch_id && element.deleted == "1") {
        cancel_body += ` <li class="list-group-item"  onclick='showHs(${element.id})'>${element.nama}</li>`
    }
});
$('#cancel-list').html(cancel_body);

// stay
var booking_body = '';
data.forEach(element => {

    // if (!element.ptgshs_id && element.branch_id == branch_id) {
    //     booking_body += ` <li class="list-group-item"  onclick='showHs(${element.id})'>${element.nama} - ${element.date}</li>`
    // }
    if (!element.nama_petugas_hs && element.branch_id == branch_id) {
        booking_body += ` <li class="list-group-item"  onclick='showHs(${element.id})'>${element.nama} - ${element.date}</li>`
    }
});
$('#booking-list').html(booking_body);

// stay
if(baru) {
var $el = $("#data_ptgshs_id");
$el.empty(); // remove old options
console.log("CCCC",petugas)
$.each(petugas, function(key, value) {
$el.append($("<option></option>")
.attr("value", value.ptgshs_id).text(value.abbr_hs +   (value.name!=null && value.name!=''?" - " +value.name:'')));
});
ptgshs = petugas

       getAvailableTime();
}

var notif_type = '<?= isset($_GET["notif_type"]) ? $_GET["notif_type"] : "" ?>';
// alert(notif_type);
// stay here
if(notif_id!=false && notif_type == 'homeservice'){
	// showHsMulti(notif_id);
}

},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'HOME SERVICE',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}
function arraysEqual(a, b) {
if (a === b) return true;
if (a == null || b == null) return false;
if (a.length != b.length) return false;

// If you don't care about the order of the elements inside
// the array, you should sort both arrays here.
// Please note that calling sort on an array will modify that array.
// you might want to clone your array first.

for (var i = 0; i < a.length; ++i) {
if (a[i] !== b[i]) return false;
}
return true;
}
function edit() {

url = "<?php echo base_url('admin/homeservice/edit_hs_multi') ?>";
$.ajax({
url: url,
dataType: "json",
method: "post",
data: {
id: curr_id
},
success: function(result) {
    console.log("takoyaki", result);
$.each(result.data, function(index, value) {

$("input[type=hidden][name='id_multi']").val(result.data.id_multi);
	
$("input[type=hidden][name='" + index + "']").val(value);
$("input[type=text][name='" + index + "']").val(value);
$("input[type=number][name='" + index + "']").val(value);
$("select[name='" + index + "']").val(value);
$("input[type=date][name='" + index + "']").val(value);
$("input[type=radio][name='" + index + "'][value = '" + value + "']").attr('checked', 'checked');
})
getTimePromise().then(function() {
var explode_time_id_multi = result.data.time_id_multi.split(",");
var explode_time_name_multi = result.data.time_name_multi.split(",");

$.each(explode_time_id_multi, function(index, value) {
	$(".time-select").append(new Option(explode_time_name_multi[index].substring(0, 5), explode_time_id_multi[index]));
})

var select = $('#time_id_select');
select.html(select.find('option').sort(function(x, y) {
	return $(x).text() > $(y).text() ? 1 : -1;
}));

var select2 = $("select[name='time_id_sampai']");
select2.html(select2.find('option').sort(function(x, y) {
	return $(x).text() > $(y).text() ? 1 : -1;
}));

$("#time_id_select").val(result.data.time_id);
$("select[name='time_id_sampai']").val(result.data.time_id_sampai);



});
$("textarea#input_alamat").val(result.data.alamat);
$("textarea#catatanarea").val(result.data.catatan);
$("#modalHs").modal('hide');
$("#tambahModal").modal("show");
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'HOME SERVICE',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}


function editBookHS(curr_id) {

url = "<?php echo base_url('admin/homeservice/edit_hs_multi') ?>";
$.ajax({
url: url,
dataType: "json",
method: "post",
data: {
id: curr_id
},
success: function(result) {
console.log("okonomiyaki");
setTimeout(function() {
// stay
    $.each(result.data, function(index, value) {
 console.log("takoyaki", index + " - " + value);
    
$("input[type=hidden][name='id_multi']").val(result.data.id_multi);	
$("input[type=hidden][name='" + index + "']").val(value);
$("input[type=text][name='" + index + "']").val(value);
$("input[type=number][name='" + index + "']").val(value);
$("select[name='" + index + "']").val(value);
$("input[type=date][name='" + index + "']").val(value);
$("input[type=radio][name='" + index + "'][value = '" + value + "']").attr('checked', 'checked');

})


$("input[type=text][name='nama']").val("");
$("#time_id_select").val(Number(result.data.time_id)).change();
$(`#time_id_select option[value='${result.data.time_id}']`).prop('selected', true)

// $("#time_id_select2").val(result.data.time_name);
// $("#time_id_sampai2").val(result.data.time_name);

}, 1000);




// setTimeout(function() {


// }, 1200);



getTimePromise().then(function() {
var explode_time_id_multi = result.data.time_id_multi.split(",");
var explode_time_name_multi = result.data.time_name_multi.split(",");

$.each(explode_time_id_multi, function(index, value) {
	$(".time-select").append(new Option(explode_time_name_multi[index].substring(0, 5), explode_time_id_multi[index]));
})

var select = $('#time_id_select');
select.html(select.find('option').sort(function(x, y) {
	return $(x).text() > $(y).text() ? 1 : -1;
}));

var select2 = $("select[name='time_id_sampai']");
select2.html(select2.find('option').sort(function(x, y) {
	return $(x).text() > $(y).text() ? 1 : -1;
}));

$("#time_id_select").val(result.data.time_id);
$("select[name='time_id_sampai']").val(result.data.time_id_sampai);



});

// $("textarea#input_alamat").val(result.data.alamat);
// $("textarea#catatanarea").val(result.data.catatan);
$("#modalHs").modal('hide');
$("#tambahModal").modal("show");
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'HOME SERVICE',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}

function saveSelesai() {
url = "<?php echo base_url('admin/homeservice/setSelesai') ?>";

var fd = new FormData();
var other_data = $('#form_masterhs').serializeArray();
$.each(other_data, function(key, input) {
fd.append(input.name, input.value);
});
fd.append('status', 'Selesai')
fd.append('id', curr_id)

$.ajax({
url: url,
type: "POST",
data: fd,
dataType: 'json',
processData: false,
contentType: false,
success: function(result) {
swal({
title: 'Home Service Data',
text: result.message,
type: 'success',
});
prepareTable(document.getElementById("myDate").value);
$("#modalHs").modal('hide');
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'Home Service Data',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown)
}
});
}

function showHs(data) {
resetFormhs();
//console.log(hs_data, data)
hs_data.forEach(element => {
if (element.id == data) {
curr_id = element.id
Object.keys(element).forEach(function(k) {
$("#" + k).html(element[k]);

});
$("#data_pid").html(element.pid);
$("#data_alamat").html(element.alamat);
// $("#time_selesai").html(element.time_selesai);
$("#modalHs").modal('show');

if (element.status != 'Diproses') {
$("#diproses").hide()
$("#booking_status").show()
} else {

$("#diproses").show()
$("#booking_status").hide()
}
}
});
}

function showHsMulti(data) {
resetFormhs();
//console.log(hs_data, data)
hs_data.forEach(element => {
if (element.id == data) {
curr_id = element.multi_id
Object.keys(element).forEach(function(k) {
$("#" + k).html(element[k]);

});
$("#data_nama").html(element.nama);
$("#data_pid").html(element.pid);
$("#data_alamat").html(element.alamat);
$("#data_no_telp").html(element.no_telp);


var explode = element.multi_time_id.split(",");

//console.log(explode.length);
if(explode.length>1)
	$(".invoice #time_name").html(element.time_name+' - '+element.time_name_selesai);
else
	$(".invoice #time_name").html(element.time_name);

$(".invoice #dokter").html(element.dokter);
$(".invoice #jumlah_pasien").html(element.jumlah_pasien);
$(".invoice #pemeriksaan").html(element.pemeriksaan);
$(".invoice #time_selesai").html(element.time_selesai);

// $("#time_selesai").html(element.time_selesai);
$("#modalHs").modal('show');

if (element.status != 'Diproses') {
$("#diproses").hide()
$("#booking_status").show()
} else {

$("#diproses").show()
$("#booking_status").hide()
}
}
});
}


function getDatahs() {
url = "<?php echo base_url('admin/homeservice/get_hs') ?>";
$('#table_masterhs').DataTable({
"order": [
[0, "desc"]
],
"ajax": url,
"columns": [{
"data": "no"
}, {
"data": "date"
}, {
"data": "pid"
}, {
"data": "time_name"
}, {
"data": "nama"
}, {
"data": "alamat"
}, {
"data": "no_telp"
}, {
"data": "dokter"
}, {
"data": "jumlah_pasien"
}, {
"data": "pemeriksaan"
}, {
"data": "pay_name"
}, {
"data": "nama_petugas"
}, {
"data": "nama_petugas_hs"
}, {
"data": "catatan"
}, {
"data": "ttd_pasien"
}, {
"data": "branch"
}, {
"data": "status"
}, {
"data": "creator"
}, {
"data": "created_date"
},
{
"width": "8%",
"mRender": function(index, type, data) {
html = '';
html += '<a href="#" onclick="hapushs(' + data.id + ');"><i class="notika-icon notika-trash"></i></a>';
return html;
}
},
],
"oLanguage": {
"sSearch": "Pencarian :",
"sZeroRecords": "Data tidak ditemukan.",
"sLengthMenu": "Tampilkan _MENU_ data",
"sEmptyTable": "Data tidak ditemukan.",
"sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data.",
"infoFiltered": "(dari total _MAX_ data)"
}
});
}

function resetFormhs() {

$("#modalhs .modal-title").text('Modal Data');
$('#form_masterhs').find('button[type=reset]').click();
$("#form_masterhs input").attr('disabled', false);
$("#form_masterhs select").attr('disabled', false);
$("#form_masterhs input[name='id_user']").val("");
$("input[type=hidden][name='id']").val("");
$("#modalhs .modal-footer button").show();
$("#modalhs .modal-footer button.action").hide();
document.getElementById("inputdate").value = document.getElementById("myDate").value;
getAvailableTime(document.getElementById("myDate").value);

}

function changeDate(value,x) {
// stay date 
// onModal();
// document.getElementById("inputdate").value = value;
if(x == 'b'){
    prepareTable(value, true, true, false, "");
}else if (x == 'd'){
    prepareTable(value.value, true, true, false, "");
}else{
    prepareTable(value.value, true, true, false, "");
}                           
 //$(".changedate").val(value.value);

}

function changeInputDate(value) {
getAvailableTime(value.value);
}

var getTimePromise = function(value) {
  return new Promise(function(resolve, reject) {

if (!value) {
value = $("#inputdate").val();
}
ptgshs_id = $("select[name='ptgshs_id']").val();
url = "<?php echo base_url('admin/homeservice/get_available_time') ?>";
$.ajax({
url: url,
dataType: "json",
method: "post",
data: {
date: value,
ptgshs_id: ptgshs_id
},
success: function(result) {
var data = result.data
// var $el = $(".time-select");
// $el.empty(); // remove old options
// $.each(data, function(key, value) {
// $el.append($("<option></option>")
// .attr("value", value.time_id).text(value.time_name.substring(0, 5)));
// });
// resolve(true);
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'HOME SERVICE',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
resolve(true);
}
});
  });
}

function getAvailableTime(value) {
if (!value) {
value = $("#inputdate").val();
}
ptgshs_id = $("select[name='ptgshs_id']").val();
url = "<?php echo base_url('admin/homeservice/get_available_time') ?>";
$.ajax({
url: url,
dataType: "json",
method: "post",
data: {
date: value,
ptgshs_id: ptgshs_id
},
success: function(result) {
// var data = result.data
// var $el = $(".time-select");
// $el.empty(); // remove old options
// $.each(data, function(key, value) {
// $el.append($("<option></option>")
// .attr("value", value.time_id).text(value.time_name.substring(0, 5)));
// });
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'HOME SERVICE',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}

$('#time_id_select').change(function(){
	$("select[name='time_id_sampai']").val(this.value);
});

function tambahhs() {
$("#modalhs .modal-title").text('Tambah Data');
resetFormhs();
$("#modalhs").modal('show');
}

function ediths(id) {
resetFormhs();
url = "<?php echo base_url('admin/homeservice/edit_hs') ?>";
$.ajax({
url: url,
dataType: "json",
method: "post",
data: {
id: id
},
success: function(result) {
$("#modalhs .modal-title").text('Edit Data');
$.each(result.data, function(index, value) {
$("input[name='" + index + "']").val(value);
$("select[name='" + index + "']").val(value);
$("input[type=date][name='" + index + "']").val(value);
$("textarea[name='" + index + "']").val(value);
})
$("#modalhs").modal('show');
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'HOME SERVICE',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}

function validateForm(){ 
    /*if( document.form_masteradmin_homeservice.data_ptgshs_id.value == "" ) {
            alert( "Petugas HS harus dipilih tidak boleh kosong, dan jika kosong harap laporkan dengan BM" );
            document.form_masteradmin_homeservice.data_ptgshs_id.focus() ;
            return false;
    }*/
    // if( document.form_masteradmin_homeservice.nama.value == "" ) {
    //         alert( "Silahkan masukkan nama" );
    //         document.form_masteradmin_homeservice.nama.focus() ;
    //         return false;
    // }
    // if( document.form_masteradmin_homeservice.nama.value == "" ) {
    //         alert( "Silahkan masukkan nama" );
    //         document.form_masteradmin_homeservice.nama.focus() ;
    //         return false;
    // }
    // if( document.form_masteradmin_homeservice.no_telp.value == "" ) {
    //         alert( "Silahkan masukkan nomor telepon" );
    //         document.form_masteradmin_homeservice.no_telp.focus() ;
    //         return false;
    // }
    // if( document.form_masteradmin_homeservice.input_alamat.value == "" ) {
    //         alert( "Silahkan masukkan alamat" );
    //         document.form_masteradmin_homeservice.input_alamat.focus() ;
    //         return false;
    // }
    // if( document.form_masteradmin_homeservice.jumlah_pasien.value == "" ) {
    //         alert( "Silahkan masukkan jumlah pasien" );
    //         document.form_masteradmin_homeservice.jumlah_pasien.focus() ;
    //         return false;
    // }
    // return( true );
    return savehs();
}; 

// stay
function savehs() {
    url = "<?php echo base_url('admin/homeservice/save_hs') ?>";
    var fd = new FormData();
    var other_data = $('#form_masterhs').serializeArray();
    // console.log(other_data);
    $.each(other_data, function(key, input) {
        fd.append(input.name, input.value);
    });
    fd.delete('penggantiHS');
    fd.append('status', 'Diproses');
	fd.append('abbr', $('#data_ptgshs_id option:selected').text());
    if (modeGantiPetugas){
        fd.append('petugas_pengganti_hs', $('#select-usrhs').val());
    }

    for (var pair of fd.entries()) {
    console.log(pair[0]+ ', ' + pair[1]); 
    }

    $.ajax({
        url: url,
        type: "POST",
        data: fd,
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function(result) {
            swal({
                title: 'Home Service Data',
                text: result.message,
                type: 'success',
            });
            resetFormhs();
            prepareTable(document.getElementById("myDate").value);
			$("#tambahModal").modal("hide");
			$("#modalHs").modal("hide");
			$(".modal-backdrop").remove();
            setTimeout(function() {
                location.reload()
            }, 1200);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            swal({
                title: 'Home Service Data',
                text: 'Error',
                type: 'error',
            });
            console.log(jqXHR, textStatus, errorThrown)
        }
    });
};


function bookhs(time_id, abbr_hs, name, ptgshs_id) {
    // alert(time_id + " " + abbr_hs + " " + name + " " + ptgshs_id);
    url = "<?php echo base_url('admin/homeservice/book_hs') ?>";
    var fd = new FormData();
    var other_data = $('#form_masterhs').serializeArray();
    // console.log("other_data",other_data);
    $.each(other_data, function(key, input) {
        fd.append(input.name, input.value);
    });

    // stay urgent
    // fd.append('id', $("input[name='id']").val());
    // fd.append('id_multi', $("input[name='id_multi']").val());
    // fd.append('branch_id', $("input[name='branch_id']").val());
    // fd.append('date', $("input[name='date']").val());
    // fd.append('time_id', $("input[name='time_id']").val());
    // fd.append('time_id_sampai', $("input[name='time_id_sampai']").val());
    // fd.append('ptgshs_id', $("input[name='ptgshs_id']").val());
    // fd.append('pid', $("input[name='pid']").val());
    // fd.append('nama', $("input[name='nama']").val());
    // fd.append('no_telp', $("input[name='no_telp']").val());
    // fd.append('alamat', $("input[name='alamat']").val());
    // fd.append('dokter', $("input[name='dokter']").val());
    // fd.append('jumlah_pasien', $("input[name='jumlah_pasien']").val());
    // fd.append('pemeriksaan', $("input[name='pemeriksaan']").val());
    // fd.append('petugas_id', $("input[name='petugas_id']").val());
    // fd.append('catatan', $("input[name='catatan']").val());
    // fd.append('pay_id', $("input[name='pay_id']").val());


    const myRnId = () => parseInt(Date.now() * Math.random());
    // alert(myRnId());

    fd.delete('date');
    fd.delete('branch_id');
    fd.delete('nama');
    fd.delete('time_id');
    fd.delete('time_id_sampai');
    fd.delete('ptgshs_id')

    fd.append('date', document.getElementById("myDate").value);
    fd.append('branch_id', document.getElementById("nyBranch").selectedIndex);
    fd.append('nama', myRnId());
    fd.append('time_id', time_id);
    fd.append('time_id_sampai', time_id);
    if (ptgshs_id){
        fd.append('ptgshs_id', ptgshs_id);
        fd.append('abbr', abbr_hs + " - " + name);
    }else{
        fd.append('ptgshs_id', '');
        fd.append('abbr', abbr_hs);
    }
    fd.append('status', 'Booked');
    fd.append('booked', 1);

    for (var pair of fd.entries()) {
    console.log(pair[0]+ ', ' + pair[1]); 
    }

    if (abbr_hs == '' || !document.getElementById("myDate").value || !document.getElementById("nyBranch").selectedIndex){
        alert("Terjadi kesalahan, mohon refresh browser!");
    }else{
        $.ajax({
        url: url,
        type: "POST",
        data: fd,
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function(result) {
            swal({
                title: 'Home Service Data',
                text: result.message,
                type: 'success',
            });
            resetFormhs();
            prepareTable(document.getElementById("myDate").value);
			$("#tambahModal").modal("hide");
			$("#modalHs").modal("hide");
			$(".modal-backdrop").remove();
            
            setTimeout(function() {
                location.reload()
            }, 1200);
           
        },
        error: function(jqXHR, textStatus, errorThrown) {
            swal({
                title: 'Home Service Data',
                text: 'Error',
                type: 'error',
            });
            console.log(jqXHR, textStatus, errorThrown)
        }
    });
    }

};

function cancel() {
var reason = prompt("Masukan alasan cancel");

if (reason) {
url = "<?php echo base_url('admin/homeservice/delete_hs') ?>";
$.ajax({
url: url,
dataType: "json",
method: "post",
data: {
id: curr_id,
reason: reason
},
success: function(result) {
swal({
title: 'Home Data',
text: result.message,
type: 'success',
}, function() {

getAvailableTime(document.getElementById("myDate").value);
prepareTable(document.getElementById("myDate").value);
$("#modalHs").modal('hide');
});
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'HOME SERVICE',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}
}

function cancel_booking() {
onModal();
url = "<?php echo base_url('admin/homeservice/delete_booking') ?>";
$.ajax({
url: url,
dataType: "json",
method: "post",
data: {
id: curr_id
},
success: function(result) {
swal({
title: 'Home Data',
text: result.message,
type: 'success',
}, function() {

getAvailableTime(document.getElementById("myDate").value);
prepareTable(document.getElementById("myDate").value);
$("#modalHs").modal('hide');
offModal();
// setTimeout(function() {
//     location.reload()
// }, 1200);
});
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'HOME SERVICE',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}

function hapushs(id) {
resetFormhs();
var konfirmasi = confirm("Apakah anda yakin cancel ?");
if (konfirmasi == true) {
url = "<?php echo base_url('admin/homeservice/delete_hs') ?>";
$.ajax({
url: url,
dataType: "json",
method: "post",
data: {
id: id
},
success: function(result) {
swal({
title: 'Home Data',
text: result.message,
type: 'success',
}, function() {});
getAvailableTime(document.getElementById("myDate").value);
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'HOME SERVICE',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}
}

function refreshSelecths() {
var select1 = $("#form_masterhs #id_user");

select1.html("");

$.ajax({
method: "post",
url: base_url + "admin/master_hs/get_hs",
success: function(resp) {
var lisths = [];
try {
var json = JSON.parse(resp);
lisths = json.data;
} catch (x) {
console.log("error parse to json", resp, x);
}
$.each(lisths, function(index, hs) {
select1.append($("<option/>").attr({
value: admin_homeservice.id_user
}).html(admin_homeservice.username));
});
}
});
}


$(document).ready(function() {
console.log("onready");
resetFormhs();


var url_string = window.location.href
var url = new URL(url_string);
var c = url.searchParams.get("date");
if(!c) {
c = '<?php echo date('Y-m-d'); ?>'
}
document.getElementById("myDate").value = c;
document.getElementById("inputdate").value = c;
prepareTable(document.getElementById("myDate").value, true);

getAvailableTime(document.getElementById("myDate").value);
});

setInterval(function(){ prepareTable(document.getElementById("myDate").value,false,false,false,currentType_hs); }, 10000);




</script>
