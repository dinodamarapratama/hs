<style type="text/css">
    

h1,
h3,
h4,
h5,
h6 {
margin-top: 0;
margin-bottom: .5rem;
}

p {
margin-top: 0;
margin-bottom: 1rem;
}

address {
font-style: normal;
line-height: inherit;
margin-bottom: 1rem;
}

ul {
margin-top: 0;
margin-bottom: 1rem;
}

ul ul {
margin-bottom: 0;
}

dfn {
font-style: italic;
}

strong {
font-weight: bolder;
}

a {
text-decoration: none;
color: #00c292;
background-color: transparent;
-webkit-text-decoration-skip: objects;
}

a:hover {
text-decoration: none;
color: #233dd2;
}

a:not([href]):not([tabindex]) {
text-decoration: none;
color: inherit;
}

a:not([href]):not([tabindex]):hover,
a:not([href]):not([tabindex]):focus {
text-decoration: none;
color: inherit;
}

a:not([href]):not([tabindex]):focus {
outline: 0;
}

code {
font-family: SFMono-Regular, Menlo, Monaco, Consolas, 'Liberation Mono', 'Courier New', monospace;
font-size: 1em;
}

img {
vertical-align: middle;
border-style: none;
}

caption {
padding-top: 1rem;
padding-bottom: 1rem;
caption-side: bottom;
text-align: left;
color: #8898aa;
}

label {
display: inline-block;
margin-bottom: .5rem;
}

button {
border-radius: 0;
}

button:focus {
outline: 1px dotted;
outline: 5px auto -webkit-focus-ring-color;
}



button {
text-transform: none;
}





textarea {
overflow: auto;
resize: vertical;
}

legend {
font-size: 1.5rem;
line-height: inherit;
display: block;
width: 100%;
max-width: 100%;
margin-bottom: .5rem;
padding: 0;
white-space: normal;
color: inherit;
}

progress {
vertical-align: baseline;
}

[type='number']::-webkit-inner-spin-button,
[type='number']::-webkit-outer-spin-button {
height: auto;
}

[type='search'] {
outline-offset: -2px;
-webkit-appearance: none;
}

[type='search']::-webkit-search-cancel-button,
[type='search']::-webkit-search-decoration {
-webkit-appearance: none;
}

::-webkit-file-upload-button {
font: inherit;
-webkit-appearance: button;
}

[hidden] {
display: none !important;
}

h1,
h3,
h4,
h5,
h6,
.h1,
.h3,
.h4,
.h5,
.h6 {
font-family: inherit;
font-weight: 600;
line-height: 1.5;
margin-bottom: .5rem;
color: #32325d;
}

h1,
.h1 {
font-size: 1.625rem;
}

h3,
.h3 {
font-size: 1.0625rem;
}

h4,
.h4 {
font-size: .9375rem;
}

h5,
.h5 {
font-size: .8125rem;
}

h6,
.h6 {
font-size: .625rem;
}

.display-2 {
font-size: 2.75rem;
font-weight: 600;
line-height: 1.5;
}

hr {
margin-top: 2rem;
margin-bottom: 2rem;
border: 0;
border-top: 1px solid rgba(0, 0, 0, .1);
}

code {
font-size: 87.5%;
word-break: break-word;
color: #f3a4b5;
}

a>code {
color: inherit;
}



.row {
display: flex;
margin-right: -15px;
margin-left: -15px;
flex-wrap: wrap;
}

.col-4,
.col-8,
.col,
.col-md-10,
.col-md-12,
.col-lg-3,
.col-lg-4,
.col-lg-6,
.col-lg-7,
.col-xl-4,
.col-xl-6,
.col-xl-8 {
position: relative;
width: 100%;
min-height: 1px;
padding-right: 15px;
padding-left: 15px;
}

.col {
max-width: 100%;
flex-basis: 0;
flex-grow: 1;
}

.col-4 {
max-width: 33.33333%;
flex: 0 0 33.33333%;
}

.col-8 {
max-width: 66.66667%;
flex: 0 0 66.66667%;
}

@media (min-width: 768px) {

.col-md-10 {
max-width: 83.33333%;
flex: 0 0 83.33333%;
}

.col-md-12 {
max-width: 100%;
flex: 0 0 100%;
}
}

@media (min-width: 992px) {

.col-lg-3 {
max-width: 25%;
flex: 0 0 25%;
}

.col-lg-4 {
max-width: 33.33333%;
flex: 0 0 33.33333%;
}

.col-lg-6 {
max-width: 50%;
flex: 0 0 50%;
}

.col-lg-7 {
max-width: 58.33333%;
flex: 0 0 58.33333%;
}

.order-lg-2 {
order: 2;
}
}

@media (min-width: 1200px) {

.col-xl-4 {
max-width: 33.33333%;
flex: 0 0 33.33333%;
}

.col-xl-6 {
max-width: 50%;
flex: 0 0 50%;
}

.col-xl-8 {
max-width: 66.66667%;
flex: 0 0 66.66667%;
}

.order-xl-1 {
order: 1;
}

.order-xl-2 {
order: 2;
}
}

.form-control {
font-size: 1rem;
line-height: 1.5;
display: block;
width: 100%;
height: calc(2.75rem + 2px);
padding: .625rem .75rem;
transition: all .2s cubic-bezier(.68, -.55, .265, 1.55);
color: #8898aa;
border: 1px solid #cad1d7;
border-radius: .375rem;
background-color: #fff;
background-clip: padding-box;
box-shadow: none;
}

@media screen and (prefers-reduced-motion: reduce) {
.form-control {
transition: none;
}
}

.form-control::-ms-expand {
border: 0;
background-color: transparent;
}

.form-control:focus {
color: #8898aa;
border-color: rgba(50, 151, 211, .25);
outline: 0;
background-color: #fff;
box-shadow: none, none;
}



.form-control::placeholder {
opacity: 1;
color: #adb5bd;
}

.form-control:disabled,
.form-control[readonly] {
opacity: 1;
background-color: #e9ecef;
}

textarea.form-control {
height: auto;
}

.form-group {
margin-bottom: 1.5rem;
}

.form-inline {
display: flex;
flex-flow: row wrap;
align-items: center;
}

@media (min-width: 576px) {
.form-inline label {
display: flex;
margin-bottom: 0;
align-items: center;
justify-content: center;
}

.form-inline .form-group {
display: flex;
margin-bottom: 0;
flex: 0 0 auto;
flex-flow: row wrap;
align-items: center;
}

.form-inline .form-control {
display: inline-block;
width: auto;
vertical-align: middle;
}

.dropdown {
position: relative;
}

.dropdown-menu {
font-size: 1rem;
position: absolute;
z-index: 1000;
top: 100%;
left: 0;
display: none;
float: left;
min-width: 10rem;
margin: .125rem 0 0;
padding: .5rem 0;
list-style: none;
text-align: left;
color: #525f7f;
border: 0 solid rgba(0, 0, 0, .15);
border-radius: .4375rem;
background-color: #fff;
background-clip: padding-box;
box-shadow: 0 50px 100px rgba(50, 50, 93, .1), 0 15px 35px rgba(50, 50, 93, .15), 0 5px 15px rgba(0, 0, 0, .1);
}

.dropdown-menu.show{
display: block;
opacity: 1 !important;
}

.dropdown-menu-right {
right: 0;
left: auto;
}

.dropdown-menu[x-placement^='top'],
.dropdown-menu[x-placement^='right'],
.dropdown-menu[x-placement^='bottom'],
.dropdown-menu[x-placement^='left'] {
right: auto;
bottom: auto;
}

.dropdown-divider {
overflow: hidden;
height: 0;
margin: .5rem 0;
border-top: 1px solid #e9ecef;
}

.dropdown-item {
font-weight: 400;
display: block;
clear: both;
width: 100%;
padding: .25rem 1.5rem;
text-align: inherit;
white-space: nowrap;
color: #212529;
border: 0;
background-color: transparent;
}

.dropdown-item:hover,
.dropdown-item:focus {
text-decoration: none;
color: #16181b;
background-color: #f6f9fc;
}

.dropdown-item:active {
text-decoration: none;
color: #fff;
background-color: #00c292;
}

.dropdown-item:disabled {
color: #8898aa;
background-color: transparent;
}

.dropdown-header {
font-size: .875rem;
display: block;
margin-bottom: 0;
padding: .5rem 1.5rem;
white-space: nowrap;
color: #8898aa;
}






.card {
position: relative;
display: flex;
flex-direction: column;
min-width: 0;
word-wrap: break-word;
border: 1px solid rgba(0, 0, 0, .05);
border-radius: .375rem;
background-color: #fff;
background-clip: border-box;
}

.card>hr {
margin-right: 0;
margin-left: 0;
}

.card-body {
padding: 1.5rem;
flex: 1 1 auto;
}

.card-header {
margin-bottom: 0;
padding: 1.25rem 1.5rem;
border-bottom: 1px solid rgba(0, 0, 0, .05);
background-color: #fff;
}

.card-header:first-child {
border-radius: calc(.375rem - 1px) calc(.375rem - 1px) 0 0;
}

@keyframes progress-bar-stripes {
from {
background-position: 1rem 0;
}

to {
background-position: 0 0;
}
}

.progress {
font-size: .75rem;
display: flex;
overflow: hidden;
height: 1rem;
border-radius: .375rem;
background-color: #e9ecef;
box-shadow: inset 0 .1rem .1rem rgba(0, 0, 0, .1);
}

.media {
display: flex;
align-items: flex-start;
}

.media-body {
flex: 1 1;
}

.bg-secondary {
background-color: #f7fafc !important;
}

a.bg-secondary:hover,
a.bg-secondary:focus,
button.bg-secondary:hover,
button.bg-secondary:focus {
background-color: #d2e3ee !important;
}

.bg-default {
background-color: #172b4d !important;
}

a.bg-default:hover,
a.bg-default:focus,
button.bg-default:hover,
button.bg-default:focus {
background-color: #0b1526 !important;
}

.bg-white {
background-color: #fff !important;
}

a.bg-white:hover,
a.bg-white:focus,
button.bg-white:hover,
button.bg-white:focus {
background-color: #e6e6e6 !important;
}

.bg-white {
background-color: #fff !important;
}

.border-0 {
border: 0 !important;
}

.rounded-circle {
border-radius: 50% !important;
}

.d-none {
display: none !important;
}

.d-flex {
display: flex !important;
}

@media (min-width: 768px) {

.d-md-flex {
display: flex !important;
}
}

@media (min-width: 992px) {

.d-lg-inline-block {
display: inline-block !important;
}

.d-lg-block {
display: block !important;
}
}

.justify-content-center {
justify-content: center !important;
}

.justify-content-between {
justify-content: space-between !important;
}

.align-items-center {
align-items: center !important;
}

@media (min-width: 1200px) {

.justify-content-xl-between {
justify-content: space-between !important;
}
}

.float-right {
float: right !important;
}

.shadow,
.card-profile-image img {
box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .15) !important;
}

.m-0 {
margin: 0 !important;
}

.mt-0 {
margin-top: 0 !important;
}

.mb-0 {
margin-bottom: 0 !important;
}

.mr-2 {
margin-right: .5rem !important;
}

.ml-2 {
margin-left: .5rem !important;
}

.mr-3 {
margin-right: 1rem !important;
}

.mt-4,
.my-4 {
margin-top: 1.5rem !important;
}

.mr-4 {
margin-right: 1.5rem !important;
}

.mb-4,
.my-4 {
margin-bottom: 1.5rem !important;
}

.mb-5 {
margin-bottom: 3rem !important;
}

.mt--7 {
margin-top: -6rem !important;
}

.pt-0 {
padding-top: 0 !important;
}

.pr-0 {
padding-right: 0 !important;
}

.pb-0 {
padding-bottom: 0 !important;
}

.pt-5 {
padding-top: 3rem !important;
}

.pt-8 {
padding-top: 8rem !important;
}

.pb-8 {
padding-bottom: 8rem !important;
}

.m-auto {
margin: auto !important;
}

@media (min-width: 768px) {

.mt-md-5 {
margin-top: 3rem !important;
}

.pt-md-4 {
padding-top: 1.5rem !important;
}

.pb-md-4 {
padding-bottom: 1.5rem !important;
}
}

@media (min-width: 992px) {

.pl-lg-4 {
padding-left: 1.5rem !important;
}

.pt-lg-8 {
padding-top: 8rem !important;
}

.ml-lg-auto {
margin-left: auto !important;
}
}

@media (min-width: 1200px) {

.mb-xl-0 {
margin-bottom: 0 !important;
}
}

.text-right {
text-align: right !important;
}

.text-center {
text-align: center !important;
}

.text-uppercase {
text-transform: uppercase !important;
}

.font-weight-light {
font-weight: 300 !important;
}

.font-weight-bold {
font-weight: 600 !important;
}

.text-white {
color: #fff !important;
}

.text-white {
color: #fff !important;
}

a.text-white:hover,
a.text-white:focus {
color: #e6e6e6 !important;
}

.text-muted {
color: #8898aa !important;
}

@media print {
*,
*::before,
*::after {
box-shadow: none !important;
text-shadow: none !important;
}



img {
page-break-inside: avoid;
}

p,
h3 {
orphans: 3;
widows: 3;
}

h3 {
page-break-after: avoid;
}

@ page {
size: a3;
}

body {
min-width: 992px !important;
}





figcaption,
main {
display: block;
}

main {
overflow: hidden;
}

.bg-white {
background-color: #fff !important;
}

a.bg-white:hover,
a.bg-white:focus,
button.bg-white:hover,
button.bg-white:focus {
background-color: #e6e6e6 !important;
}

.bg-gradient-default {
background: linear-gradient(87deg, #172b4d 0, #1a174d 100%) !important;
}

.bg-gradient-default {
background: linear-gradient(87deg, #172b4d 0, #1a174d 100%) !important;
}

@keyframes floating-lg {
0% {
transform: translateY(0px);
}

50% {
transform: translateY(15px);
}

100% {
transform: translateY(0px);
}
}

@keyframes floating {
0% {
transform: translateY(0px);
}

50% {
transform: translateY(10px);
}

100% {
transform: translateY(0px);
}
}

@keyframes floating-sm {
0% {
transform: translateY(0px);
}

50% {
transform: translateY(5px);
}

100% {
transform: translateY(0px);
}
}

.opacity-8 {
opacity: .8 !important;
}

.opacity-8 {
opacity: .9 !important;
}

.center {
left: 50%;
transform: translateX(-50%);
}

[class*='shadow'] {
transition: all .15s ease;
}

.font-weight-300 {
font-weight: 300 !important;
}

.text-sm {
font-size: .875rem !important;
}

.text-white {
color: #fff !important;
}

a.text-white:hover,
a.text-white:focus {
color: #e6e6e6 !important;
}

.avatar {
font-size: 1rem;
display: inline-flex;
width: 48px;
height: 48px;
color: #fff;
border-radius: 50%;
background-color: #adb5bd;
align-items: center;
justify-content: center;
}

.avatar img {
width: 100%;
border-radius: 50%;
}

.avatar-sm {
font-size: .875rem;
width: 36px;
height: 36px;
}



.card-profile-image {
position: relative;
}

.card-profile-image img {
position: absolute;
left: 50%;
max-width: 180px;
transition: all .15s ease;
transform: translate(-50%, -30%);
border-radius: .375rem;
}

.card-profile-image img:hover {
transform: translate(-50%, -33%);
}

.card-profile-stats {
padding: 1rem 0;
}

.card-profile-stats>div {
margin-right: 1rem;
padding: .875rem;
text-align: center;
}

.card-profile-stats>div:last-child {
margin-right: 0;
}

.card-profile-stats>div .heading {
font-size: 1.1rem;
font-weight: bold;
display: block;
}

.card-profile-stats>div .description {
font-size: .875rem;
color: #adb5bd;
}

.main-content {
position: relative;
}

.main-content .navbar-top {
position: absolute;
z-index: 1;
top: 0;
left: 0;
width: 100%;
padding-right: 0 !important;
padding-left: 0 !important;
background-color: transparent;
}



.dropdown {
display: inline-block;
}

.dropdown-menu {
min-width: 12rem;
}

.dropdown-menu .dropdown-item {
font-size: .875rem;
padding: .5rem 1rem;
}

.dropdown-menu .dropdown-item>i {
font-size: 1rem;
margin-right: 1rem;
vertical-align: -17%;
}

.dropdown-header {
font-size: .625rem;
font-weight: 700;
padding-right: 1rem;
padding-left: 1rem;
text-transform: uppercase;
color: #f6f9fc;
}

.dropdown-menu a.media>div:first-child {
line-height: 1;
}

.dropdown-menu a.media p {
color: #8898aa;
}

.dropdown-menu a.media:hover .heading,
.dropdown-menu a.media:hover p {
color: #172b4d !important;
}

.footer {
padding: 2.5rem 0;
background: #f7fafc;
}

.footer .nav .nav-item .nav-link {
color: #8898aa !important;
}

.footer .nav .nav-item .nav-link:hover {
color: #525f7f !important;
}

.footer .copyright {
font-size: .875rem;
}

.form-control-label {
font-size: .875rem;
font-weight: 600;
color: #525f7f;
}

.form-control {
font-size: .875rem;
}



.form-control:focus::placeholder {
color: #adb5bd;
}

textarea[resize='none'] {
resize: none !important;
}

textarea[resize='both'] {
resize: both !important;
}

textarea[resize='vertical'] {
resize: vertical !important;
}

textarea[resize='horizontal'] {
resize: horizontal !important;
}

.form-control-alternative {
transition: box-shadow .15s ease;
border: 0;
box-shadow: 0 1px 3px rgba(50, 50, 93, .15), 0 1px 0 rgba(0, 0, 0, .02);
}

.form-control-alternative:focus {
box-shadow: 0 4px 6px rgba(50, 50, 93, .11), 0 1px 3px rgba(0, 0, 0, .08);
}



.focused .form-control {
border-color: rgba(50, 151, 211, .25);
}

.header {
position: relative;
}



.focused .form-control {
border-color: rgba(50, 151, 211, .25);
}

.mask {
position: absolute;
top: 0;
left: 0;
width: 100%;
height: 100%;
transition: all .15s ease;
}

@media screen and (prefers-reduced-motion: reduce) {
.mask {
transition: none;
}
}


@ keyframes show-navbar-dropdown {
0% {
transition: visibility .25s, opacity .25s, transform .25s;
transform: translate(0, 10px) perspective(200px) rotateX(-2deg);
opacity: 0;
}

100% {
transform: translate(0, 0);
opacity: 1;
}
}

@keyframes hide-navbar-dropdown {
from {
opacity: 1;
}

to {
transform: translate(0, 10px);
opacity: 0;
}
}
}



@keyframes show-navbar-collapse {
0% {
transform: scale(.95);
transform-origin: 100% 0;
opacity: 0;
}

100% {
transform: scale(1);
opacity: 1;
}
}

@keyframes hide-navbar-collapse {
from {
transform: scale(1);
transform-origin: 100% 0;
opacity: 1;
}

to {
transform: scale(.95);
opacity: 0;
}
}

.progress {
overflow: hidden;
height: 8px;
margin-bottom: 1rem;
border-radius: .25rem;
background-color: #e9ecef;
box-shadow: inset 0 1px 2px rgba(0, 0, 0, .1);
}

p {
font-size: 1rem;
font-weight: 300;
line-height: 1.7;
}

.description {
font-size: .875rem;
}

.heading {
font-size: .95rem;
font-weight: 600;
letter-spacing: .025em;
text-transform: uppercase;
}

.heading-small {
font-size: .75rem;
padding-top: .25rem;
padding-bottom: .25rem;
letter-spacing: .04em;
text-transform: uppercase;
}

.display-2 span {
font-weight: 300;
display: block;
}



.stealthy {
  left: 0;
  margin: 0;
  max-height: 1px;
  max-width: 1px;
  opacity: 0;
  outline: none;
  overflow: hidden;
  pointer-events: none;
  position: absolute;
  top: 0;
  z-index: -1;
}

.progress-bar {
  border-radius: 5px;
}



</style>

<script type="text/javascript">
   function myFunction() {
            var x = document.getElementById("password");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }
  function isGood(password) {
      var password_strength = document.getElementById("password-text");

      //TextBox left blank.
      if (password.length == 0) {
        password_strength.innerHTML = "";
        return;
      }

      //Regular Expressions.
      var regex = new Array();
      regex.push("[A-Z]"); //Uppercase Alphabet.
      regex.push("[a-z]"); //Lowercase Alphabet.
      regex.push("[0-9]"); //Digit.
      //regex.push("[$@$!%*#?&]"); //Special Character.

      var passed = 0;

      //Validate for each Regular Expression.
      for (var i = 0; i < regex.length; i++) {
        if (new RegExp(regex[i]).test(password)) {
          passed++;
        }
      }

      //Display status.
      var strength = "";
      switch (passed) {
       
        case 1:
          strength = "<small class='progress-bar bg-danger' style='width: 40%'>Weak</small>";
          break;
        case 2:
          strength = "<small class='progress-bar bg-warning' style='width: 60%'>Medium</small>";
          break;
        case 3:
          strength = "<small class='progress-bar bg-success' style='width: 100%'>Strong</small>";
          break;

      }
      password_strength.innerHTML = strength;

    }
</script>



<!-- Page content -->
<?php 
	if ($this->session->flashdata('sukses')) {
?>
<div class="alert alert-success alert-dismissible" style="padding: 20px; margin-left: 10px; margin-right: 10px;">
  <a href="#" class="close" data-dismiss="alert" aria-label="close" style="margin-right: 10px;">&times;</a>
  <strong><i class="fa fa-check"></i>   Success!</strong> <?= $this->session->flashdata('sukses'); ?>.
</div>
<?php
	}
?>


<?php 
  if ($this->session->flashdata('valid_password')) {
?>
<div class="alert alert-danger alert-dismissible" style="padding: 20px; margin-left: 10px; margin-right: 10px;">
  <a href="#" class="close" data-dismiss="alert" aria-label="close" style="margin-right: 10px;">&times;</a>
  <strong><i class="fa fa-check"></i>   Error!</strong> <?= $this->session->flashdata('valid_password'); ?>.
</div>
<?php
  }
?>




<!-- main-content opened -->
<div class="main-content horizontal-content">

<div class="container mt--9">

<?php foreach ($users as $key => $u) { ?>
<form action="<?php echo base_url() ?>admin/profile/change" method="POST" enctype="multipart/form-data" autocomplete="off" id="users">
<input type="hidden" name="id_user" value="<?=$u->id_user;?>">
<div class="row">
<div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
<div class="card card-profile shadow">
<div class="row justify-content-center">
<div class="col-lg-4 order-lg-2">
<div class="card-profile-image">
<a href="#">
<img name="gambar" src="<?php echo base_url().'uploads/avatar/'.$u->image; ?>"  class="rounded-circle">
</a>
</div>
</div>
</div>

<div class="card-body pt-0 pt-md-4">

<div class="text-center">
<h3>
<?php foreach ($umur as $um) { ?>
<?php echo $u->name ?><span class="font-weight-light">, <?php echo $um->age ?> Tahun</span>
<?php } ?>
</h3>
<div class="h5 font-weight-300">
<i class="ni location_pin mr-2"></i><?php echo $u->TMTLAHIR ?>
</div>
<div class="h5 mt-4">
<i class="ni business_briefcase-24 mr-2"></i><?php echo $u->name_position ?> - <?php echo $u->dpt_name ?>
</div>
<div>
<i class="ni education_hat mr-2"></i><?php echo $u->bagian_name ?>
</div>
<div>
	
<h3>Sisa Cuti : <?php echo $u->CUTI ?> Hari</h3>
<?php foreach ($calijin as $ci) { ?>
<h3>Total Izin : <?php echo $ci->total_izin ?> Hari</h3>
<?php } ?>
		
</div>
<hr class="my-4">
<p><?php echo $u->ALAMAT ?>, <?php echo $u->PROPINSI ?> , <?php echo $u->KABUPATEN ?> , <?php echo $u->KODEPOS ?> , <?php echo $u->TELPONRUMAH ?> , <?php echo $u->TELPONHP ?></p>

<hr class="my-4">



<a style="color:white" class="btn btn-success btn-with-icon btn-block btn-compose waves-effect" href="<?php echo base_url()?><?php echo $u->lampiran_str ?>" target="_blank">Download STR</a>
<a style="color:white" class="btn btn-success btn-with-icon btn-block btn-compose waves-effect" href="<?php echo base_url()?><?php echo $u->lampiran_skk ?>" target="_blank">Download Surat Keterangan Kerja</a>


</div>
</div>
</div>
</div>

<div class="col-xl-8 order-xl-1">
<div class="card bg-secondary shadow">
<div class="card-header bg-white border-0">
<div class="row align-items-center">
<div class="col-8">
<h3 class="mb-0">Akun Saya</h3>
</div>
<div class="col-4 text-right">
<button type="submit" class="btn btn-success" id="btn_save">Simpan</button>

</div>
</div>
</div>
<div class="card-body">
<h6 class="heading-small text-muted mb-4">Informasi Pengguna</h6>

<div class="pl-lg-4">
<div class="row">
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >Nama Pengguna</label>
			<input type="text" name="username" class="form-control form-control-alternative" placeholder="Nama Pengguna" value="<?php echo $u->username ?>" autocomplete="off" onfocus="this.removeAttribute('readonly');">
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			<label class="form-control-label" >Ganti Password</label>
			<a style="color:white" type="button" data-toggle="modal" data-target="#modal<?=$u->id_user;?>" class="btn btn-success btn-with-icon btn-block btn-compose waves-effect" onclick="changePWD();">Klik disini</a>
		</div>
	</div>
</div>
<div class="row">
<div class="col-lg-6">
	<div class="form-group focused">
		<label class="form-control-label">Nama Depan</label>
		<input type="text" name="name"  class="form-control form-control-alternative" placeholder="Nama Depan" value="<?php echo $u->name ?>">
	</div>
</div>
<div class="col-lg-6">
	<div class="form-group focused">
		<label class="form-control-label" >Nama Belakang</label>
		<input type="text" name="last_name" class="form-control form-control-alternative" placeholder="Nama Belakang" value="<?php echo $u->last_name ?>">
	</div>
</div>
</div>

<div class="row">
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >Inisial</label>
			<input type="text" name="inisial"  class="form-control form-control-alternative" placeholder="Inisial" value="<?php echo $u->inisial ?>">
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >Nama Panggilan</label>
			<input type="text" name="call_name" class="form-control form-control-alternative" placeholder="Nama Panggilan" value="<?php echo $u->call_name ?>">
		</div>
	</div>
</div>


<div class="row">
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >Tanggal Lahir</label>
			<input type="date" name="TGLLAHIR"  class="form-control form-control-alternative" value="<?php echo $u->TGLLAHIR ?>" readonly>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group focused">
				<label class="form-control-label" >Tanggal Masuk</label>
				<input type="date" name="TGLMASUK" class="form-control form-control-alternative" value="<?php echo $u->TGLMASUK?>" readonly>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >NIK</label>
			<input type="text" class="form-control form-control-alternative" placeholder="NIP" value="<?php echo $u->NIP ?>"  readonly>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >Email</label>
			<input type="email" name="email" class="form-control form-control-alternative" placeholder="example@example.com" value="<?php echo $u->email ?>" >
		</div>
	</div>
</div>


<div class="row">
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >Cabang</label>
			<input type="text" class="form-control form-control-alternative" placeholder="Branch" value="<?php echo $u->branch_name ?>" readonly>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >Departemen</label>
			<input type="text" class="form-control form-control-alternative" placeholder="Department" value="<?php echo $u->dpt_name ?>" readonly>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >Jabatan</label>
			<input type="text" class="form-control form-control-alternative" placeholder="Position" value="<?php echo $u->name_position ?>" readonly>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >Bagian</label>
			<input type="text" class="form-control form-control-alternative" placeholder="Bagian" value="<?php echo $u->bagian_name ?>" readonly>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >Gol Darah</label>
			<input type="text" name="GOLDARAH"  class="form-control form-control-alternative" placeholder="Gol Darah" value="<?php echo $u->GOLDARAH ?>" >
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >Jumlah Anak</label>
			<input type="text" name="JMLANAK" class="form-control form-control-alternative" placeholder="Jumlah Anak" value="<?php echo $u->JMLANAK ?>">
		</div>
	</div>
</div>


<div class="row">
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >Status Kawin</label>
			<input type="text" name="STATUSKAWIN"  class="form-control form-control-alternative" placeholder="Status Kawin" value="<?php echo $u->STATUSKAWIN ?>" >
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >Pendidikan</label>
			<input type="text" name="PENDIDIKAN" class="form-control form-control-alternative" placeholder="Pendidikan" value="<?php echo $u->PENDIDIKAN ?>" >
		</div>
	</div>
</div>


<div class="row">
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >Tinggi</label>
			<input type="text" name="TINGGI"  class="form-control form-control-alternative" placeholder="Tinggi" value="<?php echo $u->TINGGI ?>" >
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >Berat</label>
			<input type="text" name="BERAT" class="form-control form-control-alternative" placeholder="Berat" value="<?php echo $u->BERAT?>">
		</div>
	</div>
</div>


<div class="row">
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >No BPJS Kesehatan</label>
			<input type="text" name="NOBPJSKES"  class="form-control form-control-alternative" placeholder="No BPJS Kesehatan" value="<?php echo $u->NOBPJSKES ?>" readonly>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >Finger ID</label>
			<input type="text" name="FINGERID" class="form-control form-control-alternative" placeholder="Finger ID" value="<?php echo $u->FINGERID ?>" readonly>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >No NPWP</label>
			<input type="text" name="NONPWP"  class="form-control form-control-alternative" placeholder="No NPWP" value="<?php echo $u->NONPWP ?>" readonly>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >No KTP</label>
			<input type="text" name="NOKTP" class="form-control form-control-alternative" placeholder="No KTP" value="<?php echo $u->NOKTP?>" readonly>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
		<div class="form-group focused">
			<label class="form-control-label">Photo</label>
			<input class="form-control form-control-alternative" name="gambar" type="file">
		</div>
	</div>
</div>
</div>


<hr class="my-4">
<!-- Address -->
<h6 class="heading-small text-muted mb-4">Alamat KTP</h6>

<div class="pl-lg-4">
<div class="row">
	<div class="col-md-12">
		<div class="form-group focused">
			<label class="form-control-label">Alamat</label>
			<input class="form-control form-control-alternative" placeholder="Alamat KTP" type="text" name="ALAMAT" value="<?php echo $u->ALAMAT?>" readonly>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-4">
		<div class="form-group focused">
			<label class="form-control-label" for="input-city">Propinsi</label>
            <input class="form-control form-control-alternative" placeholder="Propinsi" name="PROPINSI" type="text" value="<?php echo $u->PROPINSI?>" readonly>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="form-group focused">
			<label class="form-control-label" for="input-country">Kota Madya / Kabupaten</label>
			<input class="form-control form-control-alternative" placeholder="Kota Madya / Kabupaten" type="text" name="KABUPATEN" value="<?php echo $u->KABUPATEN?>" readonly>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="form-group focused">
			<label class="form-control-label" for="input-country">Kecamatan</label>
			<input class="form-control form-control-alternative" placeholder="Kecamatan" type="text" name="KECAMATAN" value="<?php echo $u->KECAMATAN?>" readonly>
		</div>
	</div>
	
</div>

<div class="row">
	<div class="col-lg-4">
		<div class="form-group focused">
			<label class="form-control-label" for="input-country">KEL/DESA</label>
			<input class="form-control form-control-alternative" placeholder="KEL/DESA" type="text" name="KELDESA" value="<?php echo $u->KELDESA?>" readonly>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="form-group focused">
			<label class="form-control-label" for="input-city">RT/RW</label>
			<input class="form-control form-control-alternative" placeholder="RT/RW" type="text" name="RTRW" value="<?php echo $u->RTRW?>" readonly>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="form-group">
			<label class="form-control-label" for="input-country">Kode POS</label>
			<input type="text" class="form-control form-control-alternative" placeholder="Kode Pos" name="KODEPOS" value="<?php echo $u->KODEPOS?>" readonly>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label">Telpon Rumah</label>
			<input type="text" name="TELPONRUMAH" class="form-control form-control-alternative" placeholder="Telpon Rumah" value="<?php echo $u->TELPONRUMAH?>" readonly>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label">No. HP</label>
			<input type="text" name="TELPONHP" class="form-control form-control-alternative" placeholder="No. HP" value="<?php echo $u->TELPONHP?>" readonly>
		</div>
	</div>


</div>
</div>



<hr class="my-4">
<!-- Address -->
<h6 class="heading-small text-muted mb-4">Alamat Domisili</h6>
<div class="pl-lg-4">
<div class="row">
	<div class="col-md-12">
		<div class="form-group focused">
			<label class="form-control-label">Alamat</label>
			<input class="form-control form-control-alternative" placeholder="Alamat Domisili" type="text" name="ALAMAT_DOMO" value="<?php echo $u->ALAMAT_DOMO ?>">
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-4">
		<div class="form-group focused">
			<label class="form-control-label" for="input-city">Propinsi</label>
			<select class="form-control form-control-alternative" name="PROPINSI_DOMO" id="propinsi_domo">
				<option>--Pilih--</option>
				<?php
					foreach ($propinsi as $key => $value) {
				?>
				<option value="<?= $value->id_propinsi ?>"><?= $value->nama_propinsi?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="form-group focused">
			<label class="form-control-label" for="input-country">Kota Madya / Kabupaten</label>
			<select class="form-control form-control-alternative" name="KABUPATEN_DOMO" id="kabupaten_domo" disabled="disabled">
				<option>--Pilih--</option>
			</select>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="form-group focused">
			<label class="form-control-label" for="input-country">Kecamatan</label>
			<select class="form-control form-control-alternative" name="KECAMATAN_DOMO" id="kecamatan_domo" disabled="disabled">
				<option>--Pilih--</option>
			</select>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-4">
		<div class="form-group focused">
			<label class="form-control-label" for="input-country">KEL/DESA</label>
			<select class="form-control form-control-alternative" name="KELDESA_DOMO" id="desa_domo" disabled="disabled">
				<option>--Pilih--</option>/
			</select>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="form-group focused">
			<label class="form-control-label" for="input-city">RT/RW</label>
			<input type="text" class="form-control form-control-alternative" placeholder="RT/RW" name="RTRW_DOMO" value="<?php echo $u->RTRW_DOMO?>">
		</div>
	</div>
	<div class="col-lg-4">
		<div class="form-group">
			<label class="form-control-label" for="input-country">Kode POS</label>
			<input type="text" class="form-control form-control-alternative" placeholder="Kode Pos" name="KODEPOS_DOMO" value="<?php echo $u->KODEPOS_DOMO ?>">
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >Telpon Rumah</label>
			<input type="text" name="TELPONRUMAH_DOMO"  class="form-control form-control-alternative" placeholder="Telpon Rumah" value="<?php echo $u->TELPONRUMAH_DOMO ?>">
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group focused">
			<label class="form-control-label" >No. HP</label>
			<input type="text" name="TELPONHP_DOMO" class="form-control form-control-alternative" placeholder="No. HP" value="<?php echo $u->TELPONHP_DOMO ?>">
		</div>
	</div>
</div>
</div>
<!-- alamat var -->
<?php 
	
	$kabupaten = $u->KABUPATEN;
	$kabupaten_domo = $u->KABUPATEN_DOMO;
	$propinsi = $u->PROPINSI;
	$propinsi_domo = $u->PROPINSI_DOMO;
	$kecamatan = $u->KECAMATAN;
	$kecamatan_domo = $u->KECAMATAN_DOMO;
	$kelurahan = $u->KELDESA;
	$kelurahan_domo = $u->KELDESA_DOMO;
} 

?>
</form>
<div class="modal fade" id="modal<?=$u->id_user?>" tabindex="-1" role="dialog">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Ganti Password</h4>


</div>
<div class="modal-body">
<form action="<?=base_url()?>admin/profile/changePWD" method="POST">
  <input type="hidden" name="id_user" value="<?=$u->id_user;?>">
<div class="row">
<div class="col-lg-6">
<label class="form-label">Password Lama</label>
<div class="form-group form-float">
<div class="form-line">
<input type="password" name="password_old" autocomplete="off" class="form-control" required>
</div>
</div>
</div>

<div class="col-lg-6">
<label class="form-label">Password Baru</label>
<div class="form-group form-float">
<div class="form-line">

 <input class="form-control" placeholder="************************" type="password" name="password_new" id="password" onkeyup="isGood(this.value)" autocomplete="off" minlength="6">
  <a class="icon-mata" style="position: absolute;
    z-index: 9;
    right: 20px;
    top: 40px;" onclick="myFunction()"><img class="b-lazy" data-src="<?php echo base_url() ?>assets/biomedika_theme/img/image3.png" width="20px" height="10px"></a>
<small class="help-block" id="password-text"></small>
<label style="color:red">Minimal 6 digit *</label><br>
<label style="color:red">Huruf depan wajib kapital *</label><br>
<label style="color:red">Kombinasi huruf dan angka *</label><br>
</div>
</div>
</div>
</div>
<center>
<button class="btn btn-danger" type="reset">Reset</button>
<button type="submit" class="btn btn-success"> Simpan</button>
<button style="color:white" type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
</center>
</form>
</div>
<div class="modal-footer">
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<style type="text/css">
	.select2-container--default .select2-selection--single{
		height: 45px;
	}
	.select2-container--default .select2-selection--single .select2-selection__rendered{
		line-height: 45px;
	}
	.select2-container--default .select2-selection--single .select2-selection__arrow b{
		margin-top: 45%;
	}
</style>

<script type="text/javascript">



	$(document).ready( function () {

    

		$('select').select2();

		$('#propinsi').change(function(){
			var id_propinsi = $(this).val();
			getKabupaten('#kabupaten',id_propinsi);
		});

		$('#kabupaten').change(function(){
			var id_kabupaten = $(this).val();
			getKecamatan('#kecamatan',id_kabupaten);
		});


		$('#kecamatan').change(function(){
			var id_desa = $(this).val();
			getKelurahan('#desa',id_desa);
		});

		$('#propinsi_domo').change(function(){
			var id_propinsi = $(this).val();
			getKabupaten('#kabupaten_domo',id_propinsi);
		});

		$('#kabupaten_domo').change(function(){
			var id_kabupaten = $(this).val();
			getKecamatan('#kecamatan_domo',id_kabupaten);
		});

		$('#kecamatan_domo').change(function(){
			var id_desa = $(this).val();
			getKelurahan('#desa_domo',id_desa);
		});

		/*init*/
		getKabupaten('#kabupaten','<?= $propinsi ?>');
		getKabupaten('#kabupaten_domo','<?= $propinsi ?>');

		getKecamatan('#kecamatan','<?= $kabupaten ?>');
		getKecamatan('#kecamatan_domo','<?= $kabupaten_domo ?>');

		getKelurahan('#desa','<?= $kecamatan ?>');
		getKelurahan('#desa_domo','<?= $kecamatan_domo ?>');

		/*set data on select*/
		$('#propinsi').val('<?= $propinsi ?>').trigger('change');
		$('#propinsi_domo').val('<?= $propinsi_domo ?>').trigger('change');
		$('#kabupaten').val('<?= $kabupaten ?>').trigger('change');
		$('#kabupaten_domo').val('<?= $kabupaten_domo ?>').trigger('change');
		$('#kecamatan').val('<?= $kecamatan ?>').trigger('change');
		$('#kecamatan_domo').val('<?= $kecamatan_domo ?>').trigger('change');
		$('#desa').val('<?= $kelurahan ?>').trigger('change');
		$('#desa_domo').val('<?= $kelurahan_domo ?>').trigger('change');
		/**/

	});



	function getKabupaten(selector,id_propinsi){
        var base_url = '<?= base_url() ?>';
        $.ajax({
        	async: false,
            method: "post",
            url: base_url + "admin/profile/getKabupaten",
            data: {
                id_propinsi: id_propinsi
            },
            success: function(resp) {
                var dataList = $.parseJSON(resp);
                $(selector).html('');
                $(selector).attr('disabled',false);
                $(selector).select2({
	                data: dataList
	                
	            });
            }
        });
	}

	function getKelurahan(selector,id_kecamatan){
		var base_url = '<?= base_url() ?>';
        $.ajax({
        	async: false,
            method: "post",
            url: base_url + "admin/profile/getKelurahan",
            data: {
                id_kecamatan: id_kecamatan
            },
            success: function(resp) {
                var dataList = $.parseJSON(resp);
                $(selector).html('');
                $(selector).attr('disabled',false);
                $(selector).select2({
	                data: dataList
	                
	            });
            }
        });
	}

	function getKecamatan(selector,id_kabkota){
		var base_url = '<?= base_url() ?>';
        $.ajax({
        	async: false,
            method: "post",
            url: base_url + "admin/profile/getKecamatan",
            data: {
                id_kabkota: id_kabkota
            },
            success: function(resp) {
                var dataList = $.parseJSON(resp);
                $(selector).html('');
                $(selector).attr('disabled',false);
                $(selector).select2({
	                data: dataList
	                
	            });
            }
        });
	}

</script>

