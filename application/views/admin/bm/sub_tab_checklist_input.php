<div class="tabcontent fade in" id="sub_tab_cheklist_input">
	<div id="">
		<div class="styled-form" style="margin-top: 30px; width: 50%;">
			<div class="alert alert-danger">
				<h4>Form Input Cheklist</h4>
				<p>* form akan tampil setelah data dilengkapi!</p>
			</div>
			
			<div id="filter_body">
				<div class="form-group form-float">
					<label class="form-label">Nama Alat</label>
					<div class="form-line">
						<select class="form-control" name="cfilter_id" id="cfilter_id" placeholder="*gunakan tab/enter">
							<option>--pilih--</option>
							<?php
								foreach ($list_mia as $key => $value) {
									echo "<option value='".$value->id."' sn='".$value->serial_number."'>".$value->alat_name."</option>";
								}
							?>
						</select>
					</div>
				</div>

				<div class="form-group form-float">
					<label class="form-label">Bulan</label>
					<div class="form-line">
						<select class="form-control" name="cfilter_bulan" id="cfilter_bulan" placeholder="*gunakan tab/enter">
							<option>--pilih--</option>
							<?php
								foreach ($list_bulan as $key => $value) {
									echo "<option value='".$key."'>".$value."</option>";
								}
							?>
						</select>
					</div>
				</div>

				<div class="form-group form-float">
					<label class="form-label">Tahun</label>
					<div class="form-line">
						<select class="form-control" name="cfilter_tahun" id="cfilter_tahun" placeholder="*gunakan tab/enter">
							<option>--pilih--</option>
							<?php
								foreach ($list_tahun as $key => $value) {
									echo "<option value='".$key."'>".$value."</option>";
								}
							?>
						</select>
					</div>
				</div>
			</div>
			

			<div class="form-group form-float">
				<div class="form-line">
					<a href="#" onclick="filterChecklist(); return false;" class="btn btn-default filter_btn"><i class="fa fa-check text-success"></i> Proses</a>
					<a href="#" onclick="resetChecklist(); return false;" class="btn btn-default filter_btn"><i class="fa fa-times text-danger"></i> Reset</a>

				</div>
				
			</div>

		</div>
	</div>

	<div id="form_checklist_input" style="display: none;" class="styled-form">
		<table id="head_table_form_checklist" class="table table-bordered" align="center">
			<tr>
				<th colspan="2" style="text-align: center !important; background-color: transparent; width: 200px;">
					<img class="b-lazy" data-src="<?= base_url('assets/logo-biomedika.ico') ?>">
					<h4>Bio Medika</h4>
					<p style="font-size: 10px;">Laboratorium Klinik Utama</p>
				</th>
				<th colspan="2" style="text-align: center !important; background-color: transparent;">
					<h2 id="ctitle_checklist">Jadwal & Cheklist Perawatan </h2> 
				</th>
				<th colspan="2" style="background-color: transparent; width: 250px; text-align: left;">
					<div class="col-md-12" style="margin-top: 15px;padding: 0; margin-left: 0;">
						<div class="col-sm-4">Cabang</div>
						<div class="col-sm-8" id="ccabang_checklist">: <?= $nama_cabang ?></div>
					</div>
					<div class="col-md-12" style="padding: 0; margin-left: 0;">
						<div class="col-sm-4">Bulan</div>
						<div class="col-sm-8" id="cbulan_checklist">:</div>
					</div>
					<div class="col-md-12" style="padding: 0; margin-left: 0;">
						<div class="col-sm-4">Tahun</div>
						<div class="col-sm-8" id="ctahun_checklist">:</div>
					</div>
					<div class="col-md-12" style="padding: 0; margin-left: 0;">
						<div class="col-sm-4">Serial Number</div>
						<div class="col-sm-8" id="cserial_checklist">:</div>
					</div>
				</th>
			</tr>
		</table>
		<div style="overflow-x: scroll;" class="vertical dragscroll">
			<h4 style="margin-top:25px; text-align:center;">Harian</h4>
			<table id="harian_table_form_checklist" class="table table-bordered table-striped" align="center">
				<tr>
					<th width="20" rowspan="2">No</th><th width="200" rowspan="2">Jenis Perawatan</th>
					<th colspan="31">Tanggal</th>
				</tr>
				<tr>
					<th>1</th><th>2</th>
					<th>3</th><th>4</th>
					<th>5</th><th>6</th>
					<th>7</th><th>8</th>
					<th>9</th><th>10</th>
					<th>11</th><th>12</th>
					<th>13</th><th>14</th>
					<th>15</th><th>16</th>
					<th>17</th><th>18</th>
					<th>19</th><th>20</th>
					<th>21</th><th>22</th>
					<th>23</th><th>24</th>
					<th>25</th><th>26</th>
					<th>27</th><th>28</th>
					<th>29</th><th>30</th>
					<th>31</th>
				</tr>
			</table>
		</div>
		
		<h4 style="margin-top:25px; text-align:center;">Mingguan</h4>
		<table id="mingguan_table_form_checklist" class="table table-bordered table-striped" align="center">
			<tr>
				<tr>
					<th width="20" rowspan="2">No</th><th width="200" rowspan="2">Jenis Perawatan</th>
					<th colspan="4">Tanggal</th>
				</tr>
			</tr>
			<tr>
				<th>Week 1</th>
				<th>Week 2</th>
				<th>Week 3</th>
				<th>Week 4</th>
			</tr>
		</table>

		<div style="overflow-x: scroll;" class="vertical dragscroll">
			<h4 style="margin-top:25px; text-align:center;">Bulanan</h4>
			<table id="bulanan_table_form_checklist" class="table table-bordered table-striped" align="center">
				<tr>
					<tr>
						<th width="20" rowspan="2">No</th><th width="200" rowspan="2">Jenis Perawatan</th>
						<th colspan="12">Tanggal</th>
					</tr>
				</tr>
				<tr>
					<th>Jan</th>
					<th>Feb</th>
					<th>Mar</th>
					<th>Apr</th>
					<th>Mei</th>
					<th>Jun</th>
					<th>Jul</th>
					<th>Agu</th>
					<th>Sep</th>
					<th>Okt</th>
					<th>Nov</th>
					<th>Des</th>
				</tr>
			</table>
		</div>

		<h4 style="margin-top:25px; text-align:center;">Triwulan</h4>
		<table id="3bulanan_table_form_checklist" class="table table-bordered table-striped" align="center">
			<tr>
				<tr>
					<th width="20" rowspan="2">No</th><th width="200" rowspan="2">Jenis Perawatan</th>
					<th colspan="4">Tanggal</th>
				</tr>
			</tr>
			<tr>
				<th>Triwulan 1</th>
				<th>Triwulan 2</th>
				<th>Triwulan 3</th>
				<th>Triwulan 4</th>
			</tr>
		</table>

		<h4 style="margin-top:25px; text-align:center;">Semester</h4>
		<table id="6bulanan_table_form_checklist" class="table table-bordered table-striped" align="center">
			<tr>
				<tr>
					<th width="20" rowspan="2">No</th><th width="200" rowspan="2">Jenis Perawatan</th>
					<th colspan="4">Tanggal</th>
				</tr>
			</tr>
			<tr>
				<th>Semester 1</th>
				<th>Semester 2</th>
			</tr>
		</table>

		<div style="overflow-x: scroll;" class="vertical dragscroll">
			<h4 style="margin-top:25px; text-align:center;">As Needed</h4>
			<table id="asneeded_table_form_checklist" class="table table-bordered table-striped" align="center">
				<tr>
					<th width="20" rowspan="2">No</th><th width="200" rowspan="2">Jenis Perawatan</th>
					<th colspan="31">Tanggal</th>
				</tr>
				<tr>
					<th>1</th><th>2</th>
					<th>3</th><th>4</th>
					<th>5</th><th>6</th>
					<th>7</th><th>8</th>
					<th>9</th><th>10</th>
					<th>11</th><th>12</th>
					<th>13</th><th>14</th>
					<th>15</th><th>16</th>
					<th>17</th><th>18</th>
					<th>19</th><th>20</th>
					<th>21</th><th>22</th>
					<th>23</th><th>24</th>
					<th>25</th><th>26</th>
					<th>27</th><th>28</th>
					<th>29</th><th>30</th>
					<th>31</th>
				</tr>
			</table>
		</div>
		
	</div>
</div>

<script type="text/javascript">
	$(document).ready( function () {
		var m = parseInt('<?= date("m") ?>');
		$('#cfilter_bulan').val(m).trigger('change');
		$('#cfilter_tahun').val('<?= date("Y") ?>').trigger('change');
	});
	

	function filterChecklist(){
		
		var bulan_caption = $('#cfilter_bulan :selected').text();
		var tahun_caption = $('#cfilter_tahun :selected').text();
		var alat_caption = $('#cfilter_id :selected').text();
		var sn_caption = $('#cfilter_id :selected').attr('sn');
		
		if (bulan_caption != '--pilih--' && tahun_caption != '--pilih--' && alat_caption != '--pilih--' && bulan_caption && tahun_caption && alat_caption) {

			$('#cbulan_checklist').html(': '+bulan_caption);
			$('#ctahun_checklist').html(': '+tahun_caption);
			$('#cserial_checklist').html(': '+sn_caption);
			$('#ctitle_checklist').html('Jadwal & Cheklist Perawatan '+alat_caption);
			$('.dom_harian, .dom_mingguan, .dom_bulanan, .dom_asneeded, .dom_3bulanan, .dom_6bulanan').remove();

			url = "<?php echo base_url('admin/intern_checklist/get_cheklist_form') ?>";
			$.ajax({
				url:url,
				dataType: "json",
				method: "post",
				data:{
					id_intern_alat:$('#cfilter_id').val(),
					bulan:$('#cfilter_bulan').val(),
					tahun:$('#cfilter_tahun').val()
				},
				success: function(result) {
					console.log(result);
					if (result.code == 400) {
						swal({
							title: 'Form Checklist Report',
							text: 'Tidak dapat memroses data, master checklist belum terdaftar!',
							type: 'error',
						});	
						$('#form_checklist').slideUp();
					}else{
						/*harian*/
						var DomData = '';
						var no = 1;
						$.each(result.data.harian,function(i,v){
							DomData += '<tr class="dom_harian">';
							DomData += '<td>'+(no++)+'</td>';
							$.each(result.checklist_name,function(c,n){
								if (c == i) {
									DomData += '<td>'+n+'</td>';
								}
							});
							$.each(v,function(ii,vv){
								if (vv.nilai==1) {
									// DomData += '<td><input ic_id="'+vv.ic_id+'" ic_nilai="'+vv.nilai+'" ic_tgl="'+vv.tgl+'" ic_in_id="'+vv.ic_in_id+'" onclick="update_ic_harian(this)" type="checkbox" value="0" checked /></td>';
									DomData += '<td><i class="fa fa-check text-success"></i></td>';
								}else{
									DomData += '<td><input ic_id="'+vv.ic_id+'" ic_nilai="'+vv.nilai+'" ic_tgl="'+vv.tgl+'" ic_in_id="'+vv.ic_in_id+'" onclick="update_ic(this)" type="checkbox" value="1" /></td>';
								}
								
							});
							DomData += '</tr>';
						});
						$('#harian_table_form_checklist').append(DomData);
						/**/

						/*mingguan*/
						var DomData = '';
						var no = 1;
						$.each(result.data.mingguan,function(i,v){
							DomData += '<tr class="dom_mingguan">';
							DomData += '<td>'+(no++)+'</td>';
							$.each(result.checklist_name,function(c,n){
								if (c == i) {
									DomData += '<td>'+n+'</td>';
								}
							});
							$.each(v,function(ii,vv){
								// console.log(vv.nilai);
								if (vv.nilai==1) {
									DomData += '<td><i class="fa fa-check text-success"></i></td>';
								}else{
									DomData += '<td><input ic_id="'+vv.ic_id+'" ic_nilai="'+vv.nilai+'" ic_tgl="'+vv.tgl+'" ic_in_id="'+vv.ic_in_id+'" onclick="update_ic(this)" type="checkbox" value="1" /></td>';
								}
								
							});
							DomData += '</tr>';
						});
						$('#mingguan_table_form_checklist').append(DomData);
						/**/

						/*bulanan*/
						var DomData = '';
						var no = 1;
						$.each(result.data.bulanan,function(i,v){
							DomData += '<tr class="dom_bulanan">';
							DomData += '<td>'+(no++)+'</td>';
							$.each(result.checklist_name,function(c,n){
								if (c == i) {
									DomData += '<td>'+n+'</td>';
								}
							});
							$.each(v,function(ii,vv){
								if (vv.nilai==1) {
									DomData += '<td><i class="fa fa-check text-success"></i></td>';
								}else{
									DomData += '<td><input ic_id="'+vv.ic_id+'" ic_nilai="'+vv.nilai+'" ic_tgl="'+vv.tgl+'" ic_in_id="'+vv.ic_in_id+'" onclick="update_ic(this)" type="checkbox" value="1" /></td>';
								}
								
							});
							DomData += '</tr>';
						});
						$('#bulanan_table_form_checklist').append(DomData);
						/**/

						/*as needed*/
						var DomData = '';
						var no = 1;
						$.each(result.data.as_needed,function(i,v){
							DomData += '<tr class="dom_asneeded">';
							DomData += '<td>'+(no++)+'</td>';
							$.each(result.checklist_name,function(c,n){
								if (c == i) {
									DomData += '<td>'+n+'</td>';
								}
							});
							$.each(v,function(ii,vv){
								if (vv.nilai==1) {
									DomData += '<td><i class="fa fa-check text-success"></i></td>';
								}else{
									DomData += '<td><input ic_id="'+vv.ic_id+'" ic_nilai="'+vv.nilai+'" ic_tgl="'+vv.tgl+'" ic_in_id="'+vv.ic_in_id+'" onclick="update_ic(this)" type="checkbox" value="1" /></td>';
								}
								
							});
							DomData += '</tr>';
						});
						$('#asneeded_table_form_checklist').append(DomData);
						/**/

						/*3 bulanan*/
						var DomData = '';
						var no = 1;
						$.each(result.data.tri_month,function(i,v){
							DomData += '<tr class="dom_3bulanan">';
							DomData += '<td>'+(no++)+'</td>';
							$.each(result.checklist_name,function(c,n){
								if (c == i) {
									DomData += '<td>'+n+'</td>';
								}
							});
							$.each(v,function(ii,vv){
								if (vv.nilai==1) {
									DomData += '<td><i class="fa fa-check text-success"></i></td>';
								}else{
									DomData += '<td><input ic_id="'+vv.ic_id+'" ic_nilai="'+vv.nilai+'" ic_tgl="'+vv.tgl+'" ic_in_id="'+vv.ic_in_id+'" onclick="update_ic(this)" type="checkbox" value="1" /></td>';
								}
								
							});
							DomData += '</tr>';
						});
						$('#3bulanan_table_form_checklist').append(DomData);
						/**/

						/*6 bulanan*/
						var DomData = '';
						var no = 1;
						$.each(result.data.six_month,function(i,v){
							DomData += '<tr class="dom_6bulanan">';
							DomData += '<td>'+(no++)+'</td>';
							$.each(result.checklist_name,function(c,n){
								if (c == i) {
									DomData += '<td>'+n+'</td>';
								}
							});
							$.each(v,function(ii,vv){
								if (vv.nilai==1) {
									DomData += '<td><i class="fa fa-check text-success"></i></td>';
								}else{
									DomData += '<td><input ic_id="'+vv.ic_id+'" ic_nilai="'+vv.nilai+'" ic_tgl="'+vv.tgl+'" ic_in_id="'+vv.ic_in_id+'" onclick="update_ic(this)" type="checkbox" value="1" /></td>';
								}
								
							});
							DomData += '</tr>';
						});
						$('#6bulanan_table_form_checklist').append(DomData);
						/**/
						
						$('#form_checklist_input').slideDown();	
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown) {
					
				}
			});

			
		}else{
			swal({
				title: 'Form Checklist Report',
				text: 'Tidak dapat memroses data, chek filter kembali!',
				type: 'error',
			});
			$('#form_checklist_input').slideUp();
		}
		
		
	}

	function resetChecklist(){
		$('#form_checklist_input').slideUp();
		$('#cfilter_id, #cfilter_bulan, #cfilter_tahun').val(0).trigger('change');	
	}

	function update_ic(x){
		var get_url = "<?php echo base_url('admin/intern_checklist/update_ic_input') ?>";
		var ic_id = $(x).attr('ic_id');
		var ic_nilai = parseInt($(x).attr('ic_nilai'));
		var ic_tgl = $(x).attr('ic_tgl');
		var ic_in_id = parseInt($(x).attr('ic_in_id'));
		var nilai = 0;
		if (ic_nilai==0) {
			nilai = 1; /*set to 1*/
		}

		update_ic_in({
			'get_url':get_url,
			'ic_id':ic_id,
			'ic_in_id':ic_in_id,
			'value':nilai,
			'tgl':ic_tgl
		});
		
	}


	function update_ic_in(parseData = {}){
		$.ajax({
			url:parseData.get_url,
			dataType: "json",
			method: "post",
			data:parseData,
			success: function(result) {
				filterChecklist(); /*refresh data*/
			},
			error: function(jqXHR, textStatus, errorThrown) {}
		});
	}
</script>
