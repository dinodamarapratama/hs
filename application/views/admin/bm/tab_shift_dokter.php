<div role="tabpanel" class="tab-pane fade in" id="appdokter_shift">
	<!-- jadwal dokter biomedika -->
	<div class="main-content horizontal-content">

<!-- container opened -->
<div class="container">
<div class="inbox-area">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
				<div class="inbox-left-sd card">
				<div class="compose-ml"> 
					<a class="btn btn-info" id="compose" data-toggle="modal" data-target="#form_input" onclick="initInputForm();"><i class="fa fa-plus"></i> Add Schedule</a> 
						<!-- <a class="btn btn-danger" title="semua data yg diset repeat weekly akan digenerate otomatis" onclick="doGenerateRepeat();"><i class="fa fa-refresh"></i> Generate Repeat Schedule</a>  -->
				</div>

				<div class="form-group form-float">
					<label class="form-label">Bulan</label>
					<div class="form-line">
						<select id="pick_bulan" name="pick_bulan" class="form-control">
						</select>
					</div>
				</div>

				<div class="form-group form-float">
					<label class="form-label">Tahun</label>
					<div class="form-line">
						<select id="pick_tahun" name="pick_tahun" class="form-control">
						<?php 
						for($i = 2020; $i<2026 ; $i++) {
						    echo '<option value"'.$i.'" '. (date('Y') == $i ? ' selected ' : ' ') .' >'.$i.'</option>';
						}
						?>
						</select>
					</div>
				</div>
				<div class="form-group form-float">
					<div class="form-line">
						<label class="form-label">Cabang</label>
						<select id="pick_branch" name="branch" class="form-control">
						</select>
					</div>
				</div>
				<div class="form-group form-float">
					<div class="form-line">
						<label class="form-label">Dokter</label>
						<select id="pick_dokter" name="dokter" class="form-control">
						</select>
					</div>
				</div>
				<div class="form-group form-float">
					<div class="form-line">
						<a class="btn btn-info" onclick="getShift(2); return false;"><i class="fa fa-search"></i> Show</a>
					</div>
				</div>

				<!-- <div class="form-group form-float">
					<div class="form-line">
						<label class="form-label">Switch View Mode</label>
						<div class="compose-ml"> 
							<a class="btn btn-warning" id="btn_list_view">List View</a>
							<a class="btn btn-warning" id="btn_calendar_view" disabled="true" >Calendar View</a> 
						</div>
					</div>
				</div> -->

				</div>
			</div>

			<!-- block content -->
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
				<div class="inbox-text-list sm-res-mg-t-30">
					<h1 align="center" id="caption_title_dokter">Jadwal Dokter Biomedika</h1>
					<h3 align="center" id="caption_title_calendar"> - </h3>
				    <div id="calendar_view"></div>
				    <div id="list_view" style="display: none;">
				    	<!-- list view -->
				    	<table id="tb_schedule" class="table table-bordered table-striped table-hover dataTable js-exportable">
							<thead>
							<tr>
								<th width="50px;">No</th>
								<th>Nama Dokter</th>
								<th>Cabang</th>
								<th>Tanggal Input</th>
								<th>Creator</th>
								<th>Aksi</th>
							</tr>
							</thead>
						</table>	
				    </div>
				</div>
			</div>

		</div>
	</div>
</div>
</div>
</div>


<!-- block modal form -->
<!-- modal input -->
<div class="modalInit" id="modal_input"></div>
<!-- modal view -->
<div class="modalInit" id="modal_view"></div>
<!-- modal edit -->
<div class="modalInit" id="modal_edit"></div>
<!--  -->

<!-- block js -->
<script type="text/javascript">
	/**
		initialize
	*/
	var data;
	var cal;
	
	$(document).ready(function(){
		getBulan();
		getBranch();
		getDokter();
		branchOption();
		// initInputForm();
		setTimeout(function(){
			getShift(2);	
		},2000);
		
		/**/
		

	});

	function getShift(type=1) {
    var month = $('#pick_bulan').val();
    var year = $('#pick_tahun').val();
    var branch = $('#pick_branch').val();
    var dokter = $('#pick_dokter').val();

    var cabang = $('#pick_branch').find(':selected').text();
    if (branch == 16) {
    	cabang = 'Semua Cabang';
    }else{
    	cabang = 'Cabang '+cabang;
    }
    var bulan = $('#pick_bulan').find(':selected').text();
    var tahun = year;
    if (dokter == 'all') {
    	cap_dokter = 'Semua Dokter';
    }else{
    	cap_dokter = $('#pick_dokter').find(":selected").text();
    }
    $('#caption_title_calendar').text(bulan+' '+tahun+' - '+cabang);
    $('#caption_title_dokter').text('Jadwal Dokter Biomedika ('+cap_dokter+')');

		// var d = new Date();
		// d.setMonth(Number(month)-1);
		// d.setYear(year);
		// cal.setDate(d);
		
		if (type == 2) {
			var url_list = "<?php echo base_url('admin/appdokter_shift/get_list') ?>";
			get_jadwal_list(url_list,month,year,branch,dokter);
			$('#list_view').show();
		}

		// if (type == 1) {
		// 	var url_cal = "<?php echo base_url('admin/appdokter_shift/get_cal') ?>";
		// 	get_calender(url_cal,month,year,branch,dokter);
		// }

	}

	function get_jadwal_list(url,month,year,branch,dokter){
		if ($.fn.dataTable.isDataTable("#tb_schedule")) {
		    $('#tb_schedule').DataTable().destroy();  
		}

		$('#tb_schedule').DataTable({
			"serverSide": true,
	        "processing": true,
	        "searching" : false,
			"ajax": {
				"url": url,
	            "dataSrc": 'data',
	            "type" : "POST",
	            "data" : {
					'month' : month,
					'year' : year,
					'branch' : branch,
					'dokter' : dokter
				}
			},
			"columns": [
				{ "data": 'id', "orderable": false, 
	               render: function (data, type, row, meta) {
	               return meta.row + meta.settings._iDisplayStart + 1;
	               }
	            },
				{ "data": "full_name" },
				{ "data": "branch_name" },
				{ "data": "tgl_input" },
				{ "data": "p_input" },
				{  "width": "18%", 
				"mRender": function(index, type, data) {
					cur_date = '<?= date("d-m-Y") ?>';
					html = '';
					html += '<a href="#" class="btn notika-btn btn-info btn-sm" title="detail" onclick="viewShift('+data.id+');"><i style="color: #fff; font-size:12px;" class="fa fa-search"></i> </a>';
					html += '<a href="#" class="btn notika-btn btn-warning btn-sm" title="ubah" onclick="editShift('+data.id+');"><i style="color: #fff; font-size:12px;" class="fa fa-edit"></i> </a>';
					if (data.lewat == 0) {
						html += '<a href="#" class="btn notika-btn btn-danger btn-sm" title="hapus data?" onclick="confirmDelShift('+data.id+');"><i style="color: #fff; font-size:12px;" class="fa fa-trash"></i> </a>';	
					}
					
					return html;
				}
			},
			],
			"oLanguage": {
			"sSearch": "Pencarian :",
			"sZeroRecords": "Data tidak ditemukan.",
			"sLengthMenu": "Tampilkan _MENU_ data",
			"sEmptyTable": "Data tidak ditemukan.",
			"sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data.",
			"infoFiltered": "(dari total _MAX_ data)"
			}
		});

		/*set*/
		$("#tb_schedule").css("font-size", 11);
		$("#tb_schedule").css("width", '100%');
	}

	function doGenerateRepeat(){
		var url = "<?php echo base_url('admin/appdokter_shift/getCountData') ?>";
		$.get(url,function(data){
			var rsp = $.parseJSON(data);
			// console.log(rsp);
			if (rsp.recordTotal == 0) {
				alertGagal('maaf belum ada jadwal yang bisa diotomatisasi.');return false;
			}else{
				var url = "<?php echo base_url('welcome/repeatWeeklyShiftDokter') ?>";
				$.get(url,function(data){
					alertSukses('generate otomatis jadwal berhasil.');
					getShift(2);
					return false;
				});
			}
		});
	}

	function clearInitModal(){
		$('.modalInit').html('');
	}

	function viewShift(idShift){
		clearInitModal();
		url = '<?= base_url("admin/appdokter_shift/initFormView/") ?>'+idShift;
		$.get(url,function(dom_html){
			$('#modal_view').html(dom_html);
		});
	}

	function editShift(idShift){
		clearInitModal();
		url = '<?= base_url("admin/appdokter_shift/initFormEdit/") ?>'+idShift;
		$.get(url,function(dom_html){
			$('#modal_edit').html(dom_html);
		});
		
	}

	function initInputForm(){
		clearInitModal();
		/**/
		url = '<?= base_url("admin/appdokter_shift/initForm") ?>';
		$.get(url,function(dom_html){
			$('#modal_input').html(dom_html);
		});
		
		/**/
		// initSelect();
	}

	function openInputForm(){

	}

	function get_calender(url,month,year,branch,dokter){

	}
	
	function initCalView(){

	}
	
	function initListView(){

	}


	function branchOption(){
		cur_branch = '<?= $this->session->userdata("branch_id") ?>';
		if (cur_branch != 16) {
			$('#pick_branch').attr('disabled',true);
		}else{
			/*pusat*/
			$('#pick_branch').attr('disabled',false);
		}
	}

	function getDokter(selector='#pick_dokter'){
		url = '<?= base_url("admin/appdokter_shift/getDokter") ?>';
		$.get(url, function(rslt){
			rsp = $.parseJSON(rslt);
			if (selector == '#pick_dokter') {
				optionDOM = '<option value="all" selected>-- Semua --</option>';
			}else{
				optionDOM = '<option value="-" selected>-- Pilih --</option>';
			}
			optionDOM = '<option value="all" selected>-- Semua --</option>';
			$.each(rsp.data,function(k,v){
				optionDOM += '<option value="'+v.id_pengguna+'">'+v.firstname+' '+v.lastname+'</option>';
			});
			$(selector).html(optionDOM);

		});
	}

	function getBulan(selector='#pick_bulan'){
		url = '<?= base_url("admin/appdokter_shift/getBulan") ?>';
		$.get(url, function(rslt){
			rsp = $.parseJSON(rslt);
			// console.log(rsp);
			optionDOM = '';
			currentMnth = '<?= date("m") ?>';
			$.each(rsp.data,function(k,v){
				// console.log(k+' : '+v);
				sel = '';
				if (currentMnth == k) {
					sel = 'selected';
				}
				optionDOM += '<option value="'+k+'" '+sel+'>'+v+'</option>';
			});
			$(selector).html(optionDOM);
		});
	}

	function getBranch(selector='#pick_branch'){
		url = '<?= base_url("admin/appdokter_shift/getBranch") ?>';
		$.get(url, function(rslt){
			rsp = $.parseJSON(rslt);
			optionDOM = '';
			if (selector=='#pick_branch') {
				optionDOM = '<option value="-" selected>-- Pilih --</option>';	
			}
			currentBch = '<?= $this->session->userdata("branch_id") ?>';
			$.each(rsp.data,function(k,v){
				sel = '';
				if (currentBch == v.branch_id) {
					sel = 'selected';
				}
				optionDOM += '<option value="'+v.branch_id+'" '+sel+'>'+v.branch_name+'</option>';
				
				// if (v.branch_id != 16) {
				// 	optionDOM1 += '<option value="'+v.branch_id+'" '+sel+'>'+v.branch_name+'</option>';
				// }
			});
			$(selector).html(optionDOM);
			// $('#shift_cabang').html(optionDOM1);
			
		});
	}

	function confirmDelShift(idShiftDel=''){
		var id_shift = $('#id_shift').val();
		var textMsg = 'Data pengisian akan dibatalkan!';
		if (parseInt(idShiftDel) > 0) {
			id_shift = idShiftDel;
			textMsg = 'Semua jadwal yg tersimpan berikut akan dihapus.'
		}
		if (id_shift.length == 0) {
			alertGagal('tidak ada data yang akan dihapus.'); return false;
		}else{
			swal({
			  title: 'Anda yakin?',
			  text: textMsg,
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#e83452',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Ya, batalkan!'
			},function(isConfirm){
				if (isConfirm) {
					deleteShift(id_shift);
				}
			});
		}
		

	}

	function deleteShift(idShiftDel){
		url = '<?= base_url("admin/appdokter_shift/deleteShift") ?>';
		id_shift = $('#id_shift').val();
		if (parseInt(idShiftDel) > 0) {
			id_shift = idShiftDel;
			textMsg = 'Semua jadwal yg tersimpan berikut akan dihapus.'
		}
		$.post(url,{
			'id_shift' : id_shift,
		},function(data){
			var rslt = $.parseJSON(data);
			if (rslt.code == 200) {
				/*get selector & set open form & date on week*/
				alertSukses('Data berhasil dihapus.');
				setTimeout(function(){
					$('#form_input').modal('hide');
					getShift(2);
				},1000);
			}
		});
	}
</script>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/biomedika_theme/css/toaster.css" rel="stylesheet">
<script src="<?php echo base_url()?>assets/biomedika_theme/js/toaster.js"></script>
<script type="text/javascript">
	function alertConfig(){
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": true,
      "rtl": false,
      "positionClass": "toast-bottom-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": 500,
      "hideDuration": 1000,
      "timeOut": 2000,
      "extendedTimeOut": 1000,
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut",
      "escapeHtml": false
    }
  }

  function alertSukses(text_pesan=''){
    alertConfig();
    toastr["success"](text_pesan);
  }

  function alertGagal(text_pesan=''){
    alertConfig();
    toastr["error"](text_pesan);
  }

  function alertWarning(text_pesan=''){
    alertConfig();
    toastr["warning"](text_pesan);
  }
</script>
<!--  -->