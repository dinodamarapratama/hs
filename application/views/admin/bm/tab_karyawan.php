<script src="<?php echo base_url()?>assets/admin/js/dragscroll.js"></script>
<style>
	#table_karyawan tbody td div {
		cursor: pointer;
	}
	.select2-container{
		width: 100% !important;
	}
	.select2-search__field {
	     display: block!important;
	}
	.scrolledTable{ overflow-y: auto; clear:both;height:550px; }
</style>

<div role="tabpanel" class="tab-pane fade in" id="kary">
	<div class="table-responsive" style="margin-top: 10px; height: 700px;">
		<table id="table_karyawan" class="table table-bordered table-striped table-hover dataTable js-exportable stripe row-border order-column nowrap" width="100%" cellspacing="0">
			<thead>
				<tr>
					<th>No</th>
					<th>Action</th>
					<th>Foto</th>
					<th>Nama Depan</th>
					<th>Nama Belakang</th>
					<th>Cabang</th>
					<th>Nama Pengguna</th>
					<th>Kata Kunci</th>
					<th>Nama Panggilan</th>
					<th>Inisial</th>
					<th>NIK</th>
					<th>Tempat Lahir</th>
					<th>Tanggal Lahir</th>
					<th>Jenis Kelamin</th>
					<th>No. Telpon</th>
					<th>No. Handphone</th>
					<th>Pendidikan</th>
					<th>Agama</th>
					<th>Gol Darah</th>
					<th>Tinggi Badan</th>
					<th>Berat Badan</th>
					<th>Alamat KTP</th>
					<th>Propinsi KTP</th>
					<th>Kota Madya / Kabupaten KTP</th>
					<th>Kecamatan KTP</th>
					<th>Kel/Desa KTP</th>
					<th>RT/RW KTP</th>
					<th>Kode Pos KTP</th>
					<th>Alamat Domisili</th>
					<th>Propinsi Domisili</th>
					<th>Kota Madya / Kabupaten Domisili</th>
					<th>Kecamatan Domisili</th>
					<th>Kel/Desa Domisili</th>
					<th>RT/RW Domisili</th>
					<th>Kode Pos Domisili</th>
					<th>Email</th>
					<th>No. KTP</th>
					<th>Perusahaan</th>
					<th>No. Npwp</th>
					<th>Tipe Pembayaran</th>
					<th>Bank</th>
					<th>Status Hubungan</th>
					<th>Status Karyawan</th>
					<th>Finger ID</th>
					<th>Jumlah Anak</th>
					<th>No. BPJS Tenagakerja</th>
					<th>Bumida</th>
					<th>No. BPJS Kesehatan</th>
					<th>Tgl Masuk</th>
					<th>Tgl Tetap</th>
					<th>Tgl Keluar</th>
					<th>Tgl Awal Kontrak</th>
					<th>Tgl Akhir Kontrak</th>
					<th>Cuti</th>
					<th>Masa Kerja</th> <!-- generate dari perhitungan -->
					<th>Lampiran STR</th>
					<th>Lampiran MCU</th>
				</tr>
			</thead>
		</table>
	</div>


	<div class="modal fade" id="modalKary" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Karyawan</h4>
				</div>
				<div style="padding: 15px;">
					<ul id="tab_menus" class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a id="toogleData_umum" href="#data_umum" aria-controls="settings" role="tab" data-toggle="tab">Data Umum</a></li>
						<li role="presentation"><a id="toogleLain_lain" href="#data_lain" aria-controls="settings" role="tab" data-toggle="tab">Lain-lain</a></li>
					</ul>
					<!-- form -->
					<form id="form_karyawan" name="form_karyawan" enctype="multipart/form-data">
					<input type="hidden" name="id_user">
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="data_umum">
						<br>
							<div class="form-content">
								<div class="row">
									<div class="col-md-6">
										<!-- profile umum -->
										<label class="form-label">Nama Depan</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="name" class="form-control">
											</div>
										</div>

										<label class="form-label">Username</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="username" class="form-control">
											</div>
										</div>

										<label class="form-label">Panggilan</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="call_name" class="form-control">
											</div>
										</div>

										<label class="form-label">Jenis Kelamin</label>
										<div class="form-group form-float">
											<div class="form-line">
												<select class="form-control" name="GENDER">
													<option>Pilih Jenis Kelamin</option>
													<option value="PRIA">PRIA</option>
													<option value="WANITA">WANITA</option>
												</select>
											</div>
										</div>

										<label class="form-label">Tanggal Lahir</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="TGLLAHIR" class="form-control datepickerx">
											</div>
										</div>

										<label class="form-label">No Hp</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" onkeypress="return hanyaAngka(event);" name="TELPONHP" class="form-control">
											</div>
										</div>

										<label class="form-label">Pendidikan</label>
										<div class="form-group form-float">
											<div class="form-line">
												<select class="form-control" name="PENDIDIKAN">
													<option value="SMA">SMA</option>
													<option value="SMK">SMK</option>
													<option value="D1">D1</option>
													<option value="D3">D3</option>
													<option value="S1">S1</option>
													<option value="S2">S2</option>
													<option value="S3">S3</option>
												</select>
											</div>
										</div>

										<label class="form-label">Agama</label>
										<div class="form-group form-float">
											<div class="form-line">
												<select class="form-control" name="AGAMA">
													<option value="ISLAM">ISLAM</option>
													<option value="KRISTEN">KATOLIK</option>
													<option value="PROTESTAN">PROTESTAN</option>
													<option value="HINDU">HINDU</option>
													<option value="BUDHA">BUDHA</option>
													<option value="KONG HU CU">KONG HU CU</option>
												</select>
											</div>
										</div>

										<label class="form-label">Gol Darah</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="GOLDARAH" class="form-control">
											</div>
										</div>


										<code>Alamat Asal/KTP</code><br><br>
										<label class="form-label">Alamat</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="ALAMAT" class="form-control">
											</div>
										</div>

										<label class="form-label">Propinsi</label>
										<div class="form-group form-float">
											<div class="form-line">
											<select name="PROPINSI" class="form-control" id="propinsi">
												<option>--Pilih--</option>
												<?php
												foreach ($propinsi as $key => $value) {
												?>
												<option value="<?= $value->id_propinsi ?>"><?= $value->nama_propinsi?></option>
												<?php } ?>
											</select>
											</div>
										</div>

										<label class="form-label">Kabupaten/Kodya</label>
										<div class="form-group form-float">
											<div class="form-line">
												<select class="form-control" name="KABUPATEN" id="kabupaten" disabled="disabled">
													<option>--Pilih--</option>
												</select>
											</div>
										</div>

										<label class="form-label">Kecamatan</label>
										<div class="form-group form-float">
											<div class="form-line">
												<select class="form-control" name="KECAMATAN" id="kecamatan" disabled="disabled">
													<option>--Pilih--</option>
												</select>
											</div>
										</div>

										<label class="form-label">Desa/Kelurahan</label>
										<div class="form-group form-float">
											<div class="form-line">
											<!-- <input type="text" name="KELDESA" class="form-control"> -->
												<select class="form-control" name="KELDESA" id="desa" disabled="disabled">
													<option>--Pilih--</option>
												</select>
											</div>
										</div>

										<label class="form-label">RT/RW</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="RTRW" class="form-control">
											</div>
										</div>

										<label class="form-label">Kode POS</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="KODEPOS" onkeypress="return hanyaAngka(event);" class="form-control" maxlength="5">
											</div>
										</div>

									</div>

									<div class="col-md-6">

										<label class="form-label">Nama Belakang</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="last_name" class="form-control" >
											</div>
										</div>

										<label class="form-label">Kata Kunci</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="password" class="form-control" >
											</div>
										</div>

										<label class="form-label">Inisial</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="inisial" class="form-control">
											</div>
										</div>

										<label class="form-label">Tempat Lahir</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="TMTLAHIR" class="form-control">
											</div>
										</div>

										<label class="form-label">No Telp</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" onkeypress="return hanyaAngka(event);" name="TELPONRUMAH" class="form-control">
											</div>
										</div>

										<label class="form-label">Email</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="email" class="form-control" >
											</div>
										</div>

										<label class="form-label">Tinggi Badan</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="TINGGI" class="form-control">
											</div>
										</div>

										<label class="form-label">Berat Badan</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="BERAT" class="form-control">
											</div>
										</div>


										<code>Alamat Domisili</code><br><br>
										<label class="form-label">Alamat</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="ALAMAT_DOMO" class="form-control">
											</div>
										</div>

										<label class="form-label">Propinsi</label>
										<div class="form-group form-float">
											<div class="form-line">
												<select name="PROPINSI_DOMO" class="form-control" id="propinsi_domo">
													<option>--Pilih--</option>
													<?php
													foreach ($propinsi as $key => $value) {
													?>
													<option value="<?= $value->id_propinsi ?>"><?= $value->nama_propinsi?></option>
													<?php } ?>
												</select>
											</div>
										</div>

										<label class="form-label">Kabupaten/Kodya</label>
										<div class="form-group form-float">
											<div class="form-line">
												<select class="form-control" name="KABUPATEN_DOMO" id="kabupaten_domo" disabled="disabled">
													<option>--Pilih--</option>
												</select>
											</div>
										</div>
										
										<label class="form-label">Kecamatan</label>
										<div class="form-group form-float">
											<div class="form-line">
												<select class="form-control" name="KECAMATAN_DOMO" id="kecamatan_domo" disabled="disabled">
													<option>--Pilih--</option>
												</select>
											</div>
										</div>

										<label class="form-label">Desa/Kelurahan</label>
										<div class="form-group form-float">
											<div class="form-line">
												<select class="form-control" name="KELDESA_DOMO" id="desa_domo" disabled="disabled">
													<option>--Pilih--</option>
												</select>
											</div>
										</div>

										<label class="form-label">RT/RW</label>
										<div class="form-group form-float">
											<div class="form-line">
											<input type="text" name="RTRW_DOMO" class="form-control">
											</div>
										</div>

										<label class="form-label">Kode POS</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="KODEPOS_DOMO" onkeypress="return hanyaAngka(event);" class="form-control" maxlength="5">
											</div>
										</div>


									</div>


								</div>
							</div>
						</div>


						<div role="tabpanel" class="tab-pane fade in" id="data_lain">
						<br>
							<div class="form-content">
								<div class="row">
									<div class="col-md-6">
										<label class="form-label">Tanggal Masuk</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="TGLMASUK" class="form-control datepickerx">
											</div>
										</div>

										<label class="form-label">Tanggal Tetap</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="TGLTETAP" class="form-control datepickerx">
											</div>
										</div>

										<label class="form-label">Bagian</label>
										<div class="form-group form-float">
											<div class="form-line">
												<select class="form-control" name="id_bagian">
												<option>--Pilih--</option>
												<?php
												foreach ($master_bagian as $key => $value) {
												echo "<option value='".$value->id."'>".$value->name."</option>";
												}
												?>
												</select>
											</div>
										</div>

										<label class="form-label">NIK</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="NIP" class="form-control" onkeypress="return hanyaAngka(event);" readonly="readonly">
											</div>
										</div>


										<label class="form-label">Finger ID</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="FINGERID" readonly="readonly" class="form-control" onkeypress="return hanyaAngka(event);" value="">
											</div>
										</div>

										<label class="form-label">No. KTP</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="NOKTP" class="form-control" onkeypress="return hanyaAngka(event);">
											</div>
										</div>

										<label class="form-label">Perusahaan</label>
										<div class="form-group form-float">
											<div class="form-line">
												<select class="form-control" name="PERUSAHAAN">
													<option>Pilih Perusahaan</option>
													<option value="BMJ">BMJ</option>
													<option value="BCM">BCM</option>
													<option value="BPD">BPD</option>
												</select>
											</div>
										</div>

										<label class="form-label">Bank</label>
										<div class="form-group form-float">
											<div class="form-line">
												<select class="form-control" name="BANK">
													<option>Pilih Bank</option>
													<option value="BCA">BCA</option>
												</select>
											</div>
										</div>


										<label class="form-label">Status Hubungan</label>
										<div class="form-group form-float">
											<div class="form-line">
												<select class="form-control" name="STATUSKAWIN">
													<option>Pilih Status</option>
													<option value="MENIKAH">MENIKAH</option>
													<option value="LAJANG">LAJANG</option>
													<option value="CERAI">CERAI</option>
												</select>
											</div>
										</div>


										<label class="form-label">No. BPJS Tenagakerja</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="NOJAMSOSTEK" class="form-control" onkeypress="return hanyaAngka(event);">
											</div>
										</div>

										<label class="form-label">Bumida</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" class="form-control" name="BUMIDA">
											</div>
										</div>

										<label class="form-label">Cuti</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="CUTI" class="form-control" onkeypress="return hanyaAngka(event);">
											</div>
										</div>

									</div>

									<div class="col-md-6">
										<label class="form-label">Awal Kontrak</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="AWALKONTRAK" class="form-control datepickerx">
											</div>
										</div>
										
										<label class="form-label">Akhir Kontrak</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="AKHIRKONTRAK" class="form-control datepickerx">
											</div>
										</div>

										<label class="form-label">Tanggal Keluar</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="TGLKELUAR" class="form-control datepickerx">
											</div>
										</div>

										<label class="form-label">Cabang</label>
										<div class="form-group form-float">
											<div class="form-line">
												<select class="form-control" name="branch_id">
												<?php
												foreach ($branch as $key => $value) {
												echo "<option value='".$value->branch_id."'>".$value->branch_name."</option>";
												}
												?>
												</select>
											</div>
										</div>

										<label class="form-label">Posisi</label>
										<div class="form-group form-float">
											<div class="form-line">
												<select class="form-control" name="id_position">
												<?php
												foreach ($master_position as $key => $value) {
												echo "<option value='".$value->id_position."'>".$value->name_position."</option>";
												}
												?>
												</select>
											</div>
										</div>

										<label class="form-label">Departement</label>
										<div class="form-group form-float">
											<div class="form-line">
												<select class="form-control" name="id_department">
													<option>--Pilih--</option>
													<?php
													foreach ($master_depart as $key => $value) {
													echo "<option value='".$value->id_department."'>".$value->name."</option>";
													}
													?>
												</select>
											</div>
										</div>



										<label class="form-label">No NPWP</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="NONPWP" class="form-control" onkeypress="return hanyaAngka(event);">
											</div>
										</div>

										<label class="form-label">Tipe Pembayaran</label>
										<div class="form-group form-float">
											<div class="form-line">
												<select class="form-control" name="TIPEBAYAR">
													<option>Pilih Pembayaran</option>
													<option value="CASH">CASH</option>
													<option value="TRANSFER">TRANSFER</option>
												</select>
											</div>
										</div>


										<label class="form-label">Jumlah Anak</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" name="JMLANAK" class="form-control" onkeypress="return hanyaAngka(event);">
											</div>
										</div>

										<label class="form-label">Status Karyawan</label>
										<div class="form-group form-float">
											<div class="form-line">
												<select name="STATUS" class="form-control">
													<option>PILIH STATUS</option>
													<option value="FREELANCE">FREELANCE</option>
													<option value="KONTRAK">KONTRAK</option>
													<option value="TETAP">TETAP</option>
													<option value="KELUAR">KELUAR</option>
												</select>
											</div>
										</div>

										<label class="form-label">No. BPJS Kesehatan</label>
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" class="form-control" name="NOBPJSKES" onkeypress="return hanyaAngka(event);">
											</div>
										</div>

									</div>

									<div style="margin-left: 20px; margin-right: 20px;" class="col-md-11">
										<label class="form-label">Foto</label>
										<div class="form-group form-float">
											<input type="file" id="foto" name="foto" style="padding: 0;height: 35px">
										</div>
									</div>
									
									<div style="margin-left: 20px; margin-right: 20px;" class="col-md-11">
										<label class="form-label">Lampiran STR</label>
										<div class="form-group form-float">
											<input type="file" name="lampiran_str" style="padding: 0;height: 35px">
											
										</div>
									</div>

									<div style="margin-left: 20px; margin-right: 20px;" class="col-md-11">
										<label class="form-label">Lampiran MCU</label>
										<div class="form-group form-float">
											<input type="file" name="lampiran_mcu" style="padding: 0;height: 35px">
										</div>
									</div>


								</div>
							</div>
						</div>
					</div>
					</form>
				</div>

				<div class="modal-footer" id="modal-footer1">
					<a href="#" id="btnCancelForm" onclick="closeModal(); return false;" class="btn btn-danger waves-effect"> tutup</a>
				</div>
				

			</div>
		</div>
	</div>


</div>

<script type="text/javascript">
	$(document).ready( function () {
		getDataKaryawan();
		//$('.dataTables_scrollBody').addClass("dragscroll");
		$('.scrolledTable').addClass("dragscroll");
	});
	
	function closeModal(){
		$('#modalKary').modal('hide');
	}

	function getDataKaryawan(filter={}){
		// $('#table_karyawan').DataTable().clear().destroy();
		var url = "<?php echo base_url('admin/hr/load_data/0/0/bm') ?>";
		var pathFoto = "<?php echo base_url('uploads/avatar/') ?>";
		$('#table_karyawan').DataTable({
			"serverSide": true,
	        //"processing": true,
			"ajax": {
				"url": url,
	            "dataSrc": 'data',
	            "type" : "POST",
	            "data" : filter
			},
			
			/*"scrollY": 500,
			"scrollX": true,*/
			"order":[[ 3, "asc" ]],
			"columns": [
				{ "data": "no","orderable":false },
				{  "width": "10%", 
				"mRender": function(index, type, data) {
					html = '';
					html += '<a href="#" class="btn notika-btn btn-info btn-sm" onclick="viewKary('+data.id_user+'); return false;"><i style="color: #fff; font-size:12px;" class="material-icons">description</i> </a>';
			
					return html;
					},
					"orderable":false
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						html = '<image src='+pathFoto+""+data.image+' class="img-responsive img-circle" id="viewFoto-'+data.id_user+'" >';
						return html;
					},
					"orderable":false
				},
				{ "width": "8%", 
					"mRender": function(index, type, data) {
						valData = data.name;
						return valData;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.last_name;
						return valData;
					}
				},
				{ "width": "8%", 
					"mRender": function(index, type, data) {
						valData = data.branch_name;
						return valData;
					}
				},
				
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.username;
						return valData;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.password;
						return valData;
					},
					"orderable":false
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.call_name;
						return valData;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.inisial;
						return valData;
					}
				},
				{"data": "NIP"},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.TMTLAHIR;
						html = '';
						html += '<div id="textXTMTLAHIR-'+data.id_user+'">'+ valData+'</div><input class="form-control" style="display:none;width:100px !important;" type="text" id="inputXTMTLAHIR-'+data.id_user+'" value="'+data.TMTLAHIR+'"  />';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.TGLLAHIR;
						html = '';
						html += '<div id="textXTGLLAHIR-'+data.id_user+'">'+ valData+'</div><input class="form-control datepicker" style="display:none;width:150px !important;" type="text" id="inputXTGLLAHIR-'+data.id_user+'" value="'+data.TGLLAHIR+'"  />';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.GENDER;
						html = '';
						html += '<div id="textXGENDER-'+data.id_user+'">'+ valData+'</div><select class="form-control" style="display:none;width:100px !important;" id="inputXGENDER-'+data.id_user+'"><option value="">-PILIH-</option><option value="PRIA">PRIA</option><option value="WANITA">WANITA</option></select>';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.TELPONRUMAH;
						html = '';
						html += '<div id="textXTELPONRUMAH-'+data.id_user+'">'+ valData+'</div><input class="form-control" style="display:none;width:100px !important;" type="text" onkeypress="return hanyaAngka(event);" id="inputXTELPONRUMAH-'+data.id_user+'" value="'+data.TELPONRUMAH+'"  />';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.TELPONHP;
						html = '';
						html += '<div id="textXTELPONHP-'+data.id_user+'">'+ valData+'</div><input class="form-control" style="display:none;width:100px !important;" type="text" onkeypress="return hanyaAngka(event);" id="inputXTELPONHP-'+data.id_user+'" value="'+data.TELPONHP+'"  />';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.PENDIDIKAN;
						return valData;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.AGAMA;
						return valData;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.GOLDARAH;
						return valData;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.TINGGI;
						return valData;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.BERAT;
						return valData;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.ALAMAT;
						html = '';
						html += '<div id="textXALAMAT-'+data.id_user+'">'+ valData+'</div><textarea class="form-control" style="display:none;width:100px !important;" id="inputXALAMAT-'+data.id_user+'">'+data.ALAMAT+'</textarea>';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.PROPINSI;
						html = '';
						html += '<div id="textXPROPINSI-'+data.id_user+'">'+ valData+'</div><select class="form-control" style="display:none;width:100px !important;" id="inputXPROPINSI-'+data.id_user+'"></select>';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.KABUPATEN;
						html = '';
						html += '<div id="textXKABUPATEN-'+data.id_user+'">'+ valData+'</div><select class="form-control" style="display:none;width:100px !important;" id="inputXKABUPATEN-'+data.id_user+'"></select>';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.KECAMATAN;
						html = '';
						html += '<div id="textXKECAMATAN-'+data.id_user+'">'+ valData+'</div><select class="form-control" style="display:none;width:100px !important;" id="inputXKECAMATAN-'+data.id_user+'"></select>';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.KELDESA;
						html = '';
						html += '<div id="textXKELDESA-'+data.id_user+'">'+ valData+'</div><select class="form-control" style="display:none;width:100px !important;" id="inputXKELDESA-'+data.id_user+'"></select>';
						return html;
					}
				},

				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.RTRW;
						html = '';
						html += '<div id="textXRTRW-'+data.id_user+'">'+ valData+'</div><input class="form-control" style="display:none;width:100px !important;" type="text" id="inputXRTRW-'+data.id_user+'" value="'+data.RTRW+'"  />';
						return html;
					}
				},

				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.KODEPOS;
						html = '';
						html += '<div id="textXKODEPOS-'+data.id_user+'">'+ valData+'</div><input class="form-control" style="display:none;width:100px !important;" type="text" id="inputXKODEPOS-'+data.id_user+'" value="'+data.KODEPOS+'"  />';
						return html;
					}
				},

				{ "width": "50", 
					"mRender": function(index, type, data) {
					valData = data.ALAMAT_DOMO;
					return valData;
					}
				},

				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.PROPINSI_DOMO;
						html = '';
						html += '<div id="textXPROPINSI_DOMO-'+data.id_user+'">'+ valData+'</div><select class="form-control" style="display:none;width:100px !important;" id="inputXPROPINSI_DOMO-'+data.id_user+'"></select>';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.KABUPATEN_DOMO;
						html = '';
						html += '<div id="textXKABUPATEN_DOMO-'+data.id_user+'">'+ valData+'</div><select class="form-control" style="display:none;width:100px !important;" id="inputXKABUPATEN_DOMO-'+data.id_user+'"></select>';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.KECAMATAN_DOMO;
						html = '';
						html += '<div id="textXKECAMATAN_DOMO-'+data.id_user+'">'+ valData+'</div><select class="form-control" style="display:none;width:100px !important;" id="inputXKECAMATAN_DOMO-'+data.id_user+'"></select>';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.KELDESA_DOMO;
						html = '';
						html += '<div id="textXKELDESA_DOMO-'+data.id_user+'">'+ valData+'</div><select class="form-control" style="display:none;width:100px !important;" id="inputXKELDESA_DOMO-'+data.id_user+'"></select>';
						return html;
					}
				},

				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.RTRW_DOMO;
						html = '';
						html += '<div id="textXRTRW_DOMO-'+data.id_user+'">'+ valData+'</div><input class="form-control" style="display:none;width:100px !important;" type="text" id="inputXRTRW_DOMO-'+data.id_user+'" value="'+data.RTRW_DOMO+'"  />';
						return html;
					}
				},

				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.KODEPOS_DOMO;
						html = '';
						html += '<div id="textXKODEPOS_DOMO-'+data.id_user+'">'+ valData+'</div><input class="form-control" style="display:none;width:100px !important;" type="text" id="inputXKODEPOS_DOMO-'+data.id_user+'" value="'+data.KODEPOS_DOMO+'"  />';
						return html;
					}
				},

				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.email;
						return valData;
					}
				},
				{ "width": "100", 
					"mRender": function(index, type, data) {
						valData = data.NOKTP;
						html = '';
						html += '<div id="textXNOKTP-'+data.id_user+'">'+ valData+'</div><input class="form-control" style="display:none;width:100px !important;" type="text" id="inputXNOKTP-'+data.id_user+'" value="'+data.NOKTP+'" onkeypress="return hanyaAngka(event);" />';
						return html;
					}
				},
				{ "width": "100", 
					"mRender": function(index, type, data) {
						valData = data.PERUSAHAAN;
						html = '';
						html += '<div id="textXPERUSAHAAN-'+data.id_user+'">'+ valData+'</div><select class="form-control" style="display:none;width:100px !important;" id="inputXPERUSAHAAN-'+data.id_user+'"><option>Pilih Perusahaan</option><option value="BMJ">BMJ</option><option value="BCM">BCM</option><option value="BPD">BPD</option></select>';
						return html;
					}
				},
				{ "width": "100", 
					"mRender": function(index, type, data) {
						valData = data.NONPWP;
						html = '';
						html += '<div id="textXNONPWP-'+data.id_user+'">'+ valData+'</div><input class="form-control" style="display:none;width:100px !important;" type="text" id="inputXNONPWP-'+data.id_user+'" value="'+data.NONPWP+'" onkeypress="return hanyaAngka(event);" />';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.TIPEBAYAR;
						html = '';
						html += '<div id="textXTIPEBAYAR-'+data.id_user+'">'+ valData+'</div><select class="form-control" style="display:none;width:100px !important;" id="inputXTIPEBAYAR-'+data.id_user+'"><option>Pilih Pembayaraan</option><option value="CASH">CASH</option><option value="TRANSFER">TRANSFER</option></select>';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.BANK;
						html = '';
						html += '<div id="textXBANK-'+data.id_user+'">'+ valData+'</div><select class="form-control" style="display:none;width:100px !important;" name="BANK" id="inputXBANK-'+data.id_user+'"><option>Pilih Bank</option><option value="BCA">BCA</option></select>';
						return html;
					}
				},
				
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.STATUSKAWIN;
						html = '';
						html += '<div id="textXSTATUSKAWIN-'+data.id_user+'">'+ valData+'</div><select class="form-control" style="display:none;width:100px !important;" id="inputXSTATUSKAWIN-'+data.id_user+'"><option>Pilih Status</option><option value="MENIKAH">MENIKAH</option><option value="LAJANG">LAJANG</option></select>';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.STATUS;
						html = '';
						html += '<div id="textXSTATUS-'+data.id_user+'">'+ valData+'</div><select class="form-control" style="display:none;width:100px !important;" id="inputXSTATUS-'+data.id_user+'"><option>Pilih Status</option><option value="FREELANCE">FREELANCE</option><option value="KONTRAK">KONTRAK</option><option value="TETAP">TETAP</option><option value="KELUAR">KELUAR</option></select>';
						return html;
					}
				},
				{"data" : "FINGERID"},
				{ "width": "50", 
					"mRender": function(index, type, data) {
					valData = data.JMLANAK;
					return valData;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.NOJAMSOSTEK;
						html = '';
						html += '<div id="textXNOJAMSOSTEK-'+data.id_user+'">'+ valData+'</div><input class="form-control" style="display:none;width:100px !important;" type="text" id="inputXNOJAMSOSTEK-'+data.id_user+'" value="'+data.NOJAMSOSTEK+'" onkeypress="return hanyaAngka(event);" />';
						return html;
					}
				},
				
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.BUMIDA;
						html = '';
						html += '<div id="textXBUMIDA-'+data.id_user+'">'+ valData+'</div><input class="form-control" style="display:none;width:100px !important;" type="text" id="inputXBUMIDA-'+data.id_user+'"/>';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.NOBPJSKES;
						html = '';
						html += '<div id="textXNOBPJSKES-'+data.id_user+'">'+ valData+'</div><input class="form-control" style="display:none;width:100px !important;" type="text" id="inputXNOBPJSKES-'+data.id_user+'" onkeypress="return hanyaAngka(event);" />';
						return html;
					}
				},
				
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.TGLMASUK;
						html = '';
						html += '<div id="textXTGLMASUK-'+data.id_user+'">'+ valData+'</div><input class="form-control datepicker" style="display:none;width:150px !important;" type="text" id="inputXTGLMASUK-'+data.id_user+'" value="'+data.TGLMASUK+'" />';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.TGLTETAP;
						html = '';
						html += '<div id="textXTGLTETAP-'+data.id_user+'">'+ valData+'</div><input class="form-control datepicker" style="display:none;width:150px !important;" type="text" id="inputXTGLTETAP-'+data.id_user+'" value="'+data.TGLTETAP+'" />';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.TGLKELUAR;
						html = '';
						html += '<div id="textXTGLKELUAR-'+data.id_user+'">'+ valData+'</div><input class="form-control datepicker" style="display:none;width:150px !important;" type="text" id="inputXTGLKELUAR-'+data.id_user+'" value="'+data.TGLKELUAR+'" />';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.AWALKONTRAK;
						html = '';
						html += '<div id="textXAWALKONTRAK-'+data.id_user+'">'+ valData+'</div><input class="form-control datepicker" style="display:none;width:150px !important;" type="text" id="inputXAWALKONTRAK-'+data.id_user+'" value="'+data.AWALKONTRAK+'" />';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.AKHIRKONTRAK;
						html = '';
						html += '<div id="textXAKHIRKONTRAK-'+data.id_user+'">'+ valData+'</div><input class="form-control datepicker" style="display:none;width:150px !important;" type="text" id="inputXAKHIRKONTRAK-'+data.id_user+'" value="'+data.AKHIRKONTRAK+'" />';
						return html;
					}
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						valData = data.CUTI;
						html = '';
						html += '<div id="textXcuti-'+data.id_user+'">'+ valData+'</div><input class="form-control" style="display:none;width:100px !important;" type="text" id="inputXcuti-'+data.id_user+'" value="'+data.CUTI+'" onkeypress="return hanyaAngka(event);" />';
						return html;
					}
				},
				{'data':"masa_kerja"},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						pathFile = '../'+data.lampiran_str;
						html = '';
						if (data.lampiran_str.length == 0) {
							html = '<p class="badge" style="background:#f2dede;">tidak ada lampiran</p>';
						}else{
							html += '<a class="btn btn-default" id="btn_lampiran_mcu" href="'+pathFile+'" target="_blank"><i class="fa fa-arrow-down"></i></a>';	
						}
						return html;
					},
					"orderable":false
				},
				{ "width": "50", 
					"mRender": function(index, type, data) {
						pathFile = '../'+data.lampiran_mcu;
						html = '';
						if (data.lampiran_mcu.length == 0) {
							html = '<p class="badge" style="background:#f2dede;">tidak ada lampiran</p>';
						}else{
							html += '<a class="btn btn-default" id="btn_lampiran_mcu" href="'+pathFile+'" target="_blank"><i class="fa fa-arrow-down"></i></a>';	
						}
						return html;
					},
					"orderable":false
				},
			],

			"oLanguage": {
			"sSearch": "Pencarian :",
			"sZeroRecords": "Data tidak ditemukan.",
			"sLengthMenu": "Tampilkan _MENU_ data",
			"sEmptyTable": "Data tidak ditemukan.",
			"sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data.",
			"infoFiltered": "(dari total _MAX_ data)"
			}
		});
		
		$('#table_karyawan_filter label input').attr('placeholder','Pencarian berdasarkan nama depan atau belakang!');
		
		
		$('#table_karyawan').wrap("<div class='scrolledTable'></div>");
	}

	function viewKary(id_user){
		url = "<?php echo base_url('admin/hr/load_data/') ?>"+id_user;
		$.ajax({
			url:url,
			dataType: "json",
			method: "post",
			data:{},
			success: function(result) {
				var tgl_date = ["TGLLAHIR", "TGLMASUK", "TGLTETAP", "AWALKONTRAK", "AKHIRKONTRAK", "TGLKELUAR"];
				$.each(result.data,function(k,v){
					$.each(v,function(name,vals){
						if (name=='image') {
							var pathFoto = "<?php echo base_url('uploads/avatar/') ?>"+vals;
							$('#vfoto').remove();
							$('input[name="foto"]').parent().append('<image id="vfoto" width="100px;" height="100px;" src="'+pathFoto+'">');
							$('input[name="foto"]').hide();	
						}
						else if(name=='lampiran_mcu' || name == 'lampiran_str'){
							var pathFile = vals;
							// console.log('yes : lampiran : '+pathFile);
							if (name == 'lampiran_mcu') {
								$('#btn_lampiran_mcu').remove();
								$('input[name="lampiran_mcu"]').parent().append('<a class="btn btn-default" id="btn_lampiran_mcu" href="../'+pathFile+'" target="_blank"><i class="fa fa-arrow-down"></i></a>');
								$('input[name="lampiran_mcu"]').hide();
							}
							
							if (name == 'lampiran_str') {
								$('#btn_lampiran_str').remove();
								$('input[name="lampiran_str"]').parent().append('<a class="btn btn-default" id="btn_lampiran_str" href="../'+pathFile+'" target="_blank"><i class="fa fa-arrow-down"></i></a>');
								$('input[name="lampiran_str"]').hide();		
							}
							
						}
						else if (checkValue(name,tgl_date)=='1') {
							if (vals!=null && vals!='') {
								$('input[name="'+name+'"]').attr('disabled',true).val(reformatDate(vals)).trigger('change');
							}else{
								$('input[name="'+name+'"]').attr('disabled',true);
							}
						}
						else{
							$('input[name="'+name+'"]').val(vals).attr('disabled',true).trigger('change');
							$('select[name="'+name+'"]').val(vals).attr('disabled',true).trigger('change');
							/**/
							var fkhusus = {
								'ID_KELDESA_DOMO' : 'KELDESA_DOMO',
								'ID_KELDESA' : 'KELDESA',
								'ID_KECAMATAN' : 'KECAMATAN',
								'ID_KECAMATAN_DOMO' : 'KECAMATAN_DOMO',
								'ID_KABUPATEN' : 'KABUPATEN',
								'ID_KABUPATEN_DOMO' : 'KABUPATEN_DOMO',
								'ID_PROPINSI' : 'PROPINSI',
								'ID_PROPINSI_DOMO' : 'PROPINSI_DOMO'
							};
							var ixf = (name in fkhusus);
							if (ixf == true) {

								if (fkhusus[name] == 'PROPINSI') {
									getPropinsi('select[name="'+fkhusus[name]+'"]');
									getKabupaten('select[name="KABUPATEN"]',vals);
								}

								if (fkhusus[name] == 'PROPINSI_DOMO') {
									getPropinsi('select[name="'+fkhusus[name]+'"]');
									getKabupaten('select[name="KABUPATEN_DOMO"]',vals);
								}

								if (fkhusus[name] == 'KABUPATEN') {
									getKecamatan('select[name="KECAMATAN"]',vals);
								}

								if (fkhusus[name] == 'KABUPATEN_DOMO') {
									getKecamatan('select[name="KECAMATAN_DOMO"]',vals);
								}

								if (fkhusus[name] == 'KECAMATAN') {
									getKelurahan('select[name="KELDESA"]',vals);
								}

								if (fkhusus[name] == 'KECAMATAN_DOMO') {
									getKelurahan('select[name="KELDESA_DOMO"]',vals);
								}

							}
						}
					});

					/*set value alamat*/
					$('select[name="PROPINSI"]').attr('disabled',true).val(v.ID_PROPINSI).trigger('change');
					$('select[name="PROPINSI_DOMO"]').attr('disabled',true).val(v.ID_PROPINSI_DOMO).trigger('change');
					$('select[name="KABUPATEN"]').attr('disabled',true).val(v.ID_KABUPATEN).trigger('change');
					$('select[name="KABUPATEN_DOMO"]').attr('disabled',true).val(v.ID_KABUPATEN_DOMO).trigger('change');
					$('select[name="KECAMATAN"]').attr('disabled',true).val(v.ID_KECAMATAN).trigger('change');
					$('select[name="KECAMATAN_DOMO"]').attr('disabled',true).val(v.ID_KECAMATAN_DOMO).trigger('change');
					
					$('#desa').attr('disabled',true).val(v.ID_KELDESA).trigger('change');
					$('#desa_domo').attr('disabled',true).val(v.ID_KELDESA_DOMO).trigger('change');

						
				});

				$('#modalKary').modal('show');
				$('#modalKary .modal-header').html('<h4 class="modal-title">Data Karyawan</h4>');
			},
			error: function(jqXHR, textStatus, errorThrown) {
				swal({
					title: 'View HR',
					text: 'Error',
					type: 'error',
				});
			}
		});
	}

	function reformatDate(dateStr)
	{
	  dArr = dateStr.split("-");  // ex input "2010-01-18"
	  return dArr[2]+ "/" +dArr[1]+ "/" +dArr[0]; //ex out: "18/01/10"
	}

	function checkValue(value,arr){
	  var status = '0';
	 
	  for(var i=0; i<arr.length; i++){
	    var name = arr[i];
	    if(name == value){
	      status = '1';
	      break;
	    }
	  }

	  return status;
	}

	function getPropinsi(selector,select2=1){
		var base_url = '<?= base_url() ?>';
		$.ajax({
			async: false,
			method: "get",
			url: base_url + "admin/profile/getPropinsi",
			data: {},
			success: function(resp) {
				var dataList = $.parseJSON(resp);
				if (select2) {
					$(selector).select2({
						data: dataList
					});	
				}else{
					htmlOpt = ''; 
					$.each(dataList,function(i,v){
						htmlOpt +='<option value="'+v.id+'">'+v.text+'</option>';
					});
					$(selector).append(htmlOpt);
				}
			}
		});
	}

	function getKabupaten(selector,id_propinsi,select2=1){
		var base_url = '<?= base_url() ?>';
		$.ajax({
			async: false,
			method: "post",
			url: base_url + "admin/profile/getKabupaten",
			data: {
			id_propinsi: id_propinsi
			},
			success: function(resp) {
				var dataList = $.parseJSON(resp);
				$(selector).html('');
				$(selector).attr('disabled',false);
				if (select2) {
					$(selector).select2({
						data: dataList
					});	
				}else{
					htmlOpt = ''; 
					$.each(dataList,function(i,v){
						htmlOpt +='<option value="'+v.id+'">'+v.text+'</option>';
					});
					$(selector).append(htmlOpt);
				}
			}
		});
	}

	function getKelurahan(selector,id_kecamatan,select2=1){
		var base_url = '<?= base_url() ?>';
		$.ajax({
			async: false,
			method: "post",
			url: base_url + "admin/profile/getKelurahan",
			data: {
			id_kecamatan: id_kecamatan
			},
			success: function(resp) {
				var dataList = $.parseJSON(resp);
				$(selector).html('');
				$(selector).attr('disabled',false);
				if (select2) {
					$(selector).select2({
						data: dataList
					});	
				}else{
					htmlOpt = ''; 
					$.each(dataList,function(i,v){
						htmlOpt +='<option value="'+v.id+'">'+v.text+'</option>';
					});
					$(selector).append(htmlOpt);
				}
			}
		});
	}

	function getKecamatan(selector,id_kabkota, select2=1){
		var base_url = '<?= base_url() ?>';
		$.ajax({
			async: false,
			method: "post",
			url: base_url + "admin/profile/getKecamatan",
			data: {
			id_kabkota: id_kabkota
			},
			success: function(resp) {
				var dataList = $.parseJSON(resp);
				$(selector).html('');
				$(selector).attr('disabled',false);
				if (select2) {
					$(selector).select2({
						data: dataList
					});	
				}else{
					htmlOpt = ''; 
					$.each(dataList,function(i,v){
						htmlOpt +='<option value="'+v.id+'">'+v.text+'</option>';
					});
					$(selector).append(htmlOpt);
				}
			}
		});
	}
</script>