
<!-- Start Master Cabang -->
<div role="tabpanel" class="tab-pane fade in" id="time">

<div class="header">
<center><button type="button" class="btn btn-success" onclick="tambahTime();">Add</button></center>
</div>
<div class="table-responsive">
<table id="table_time" class="table table-bordered table-striped table-hover dataTable js-exportable" width="100%" cellspacing="0">
<thead>
<tr>
<th>No</th>
<th>Time</th>
<th>Creator</th>
<th>Created Date</th>
<th>Action</th>
</tr>
</thead>
</table>
</div>

<div class="modal fade" id="modal_time" tabindex="-1" role="dialog">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Add Time</h4>
</div>
<div class="modal-body">
<form action="#" id="form_masterTime">
<input type="hidden" readonly name="time_id" class="form-control">


<label class="form-label">Time Name</label>
<div class="form-group form-float">
<div class="form-line">
<input type="time" class="form-control" name="time_name" />
</div>
</div>


<label class="form-label">Branch</label>
<div class="form-group form-float">
<div class="form-line">
<input type="text" value="<?php echo $this->session->userdata('branch_name'); ?>" class="form-control" readonly>
</div>
</div>


<button class="btn btn-primary btn-sm hidden" type="reset">Reset</button>
</form>
</div>
<div class="modal-footer">
<input type="button" onclick="saveTime(this)" value="Save" id="btn_save">
<input type="button" data-dismiss="modal" value="Cancel">
</div>
</div>
</div>
</div>
</div>




<!-- End Master Cabang -->






<script type="text/javascript">

$(document).ready( function () {
getDataTime();
resetFormTime();

});

    
$("#table_time");

function getDataTime()
{
url = "<?php echo base_url('admin/master_bm/get_time') ?>";
$('#table_time').DataTable({
scrollCollapse: true,
"order": [[ 0, "desc" ]],
"ajax": url,
"columns": [
{ "data": "no" },
{ "data": "time_name" },
{ "data": "creator_id" },
{ "data": "created_at" },
{  "width": "8%", 
"mRender": function(index, type, data) {
html = '';
html += '<a href="#" onclick="editTime('+data.time_id+');"><i class="notika-icon notika-draft"></i></a>';
html += '<a href="#" onclick="hapusTime('+data.time_id+');"><i class="notika-icon notika-trash"></i></a>';
return html;
}
},
],
"oLanguage": {
"sSearch": "Pencarian :",
"sZeroRecords": "Data tidak ditemukan.",
"sLengthMenu": "Tampilkan _MENU_ data",
"sEmptyTable": "Data tidak ditemukan.",
"sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data.",
"infoFiltered": "(dari total _MAX_ data)"
}
});
}


function resetFormTime()
{

$("#modal_time .modal-title").text('Modal Data');
$('#form_masterTime').find('button[type=reset]').click();
$("#form_masterTime input").attr('disabled', false);
$("#form_masterTime select").attr('disabled', false);
$("#form_masterTime input[name='time_id']").val("");
$("#modal_time .modal-footer button").show();
$("#modal_time .modal-footer button.action").hide();


}


function reloadDataTime()
{
$("#modal_time").modal('hide');
var table = $('#table_time').DataTable();
table.ajax.reload();
}

function tambahTime()
{
$("#modal_time .modal-title").text('Tambah Data');
resetFormTime();
$("#modal_time").modal('show');
}

function editTime(time_id)
{
resetFormTime();
url = "<?php echo base_url('admin/master_bm/edit_time') ?>";
$.ajax({
url:url,
dataType: "json",
method: "post",
data:{
time_id:time_id
},
success: function(result) {
$("#modal_time .modal-title").text('Edit Data');
$.each(result.data, function(index, value){
$("input[name='"+index+"']").val(value);
$("select[name='"+index+"']").val(value);
$("input[type=date][name='"+index+"']").val(value);
})
$("#modal_time").modal('show');
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'Time',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}

function saveTime()
{
url = "<?php echo base_url('admin/master_bm/save_time') ?>";

var fd = new FormData();
var other_data = $('#form_masterTime').serializeArray();
$.each(other_data, function(key, input) {
fd.append(input.name, input.value);
});

$.ajax({
url: url,
type: "POST",
data: fd,
dataType: 'json',
processData: false,
contentType: false,
success: function(result) {
swal({
title: 'Time',
text: result.message,
type: 'success',
});
reloadDataTime();
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'Time',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown)
}
});
};

function hapusTime(time_id)
{
resetFormTime();
var konfirmasi = confirm("Apakah anda yakin ?");
if (konfirmasi == true) {
url = "<?php echo base_url('admin/master_bm/delete_time') ?>";
$.ajax({
url:url,
dataType: "json",
method: "post",
data:{
time_id:time_id
},
success: function(result) {
swal({
title: 'Time',
text: result.message,
type: 'success',
}, function() {
reloadDataTime();
});
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'Time',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}
}

function refreshSelectTime(){
var select1 = $("#form_masterTime #time_id");

select1.html("");

$.ajax({
method: "post",
url: base_url + "admin/master_bm/get_time",
success: function (resp) {
var listTime = [];
try{
var json = JSON.parse(resp);
listTime = json.data;
}catch(x) {
console.log("error parse to json", resp, x);
}
$.each(listTime, function(index, unit) {
select1.append( $("<option/>").attr({value: hs_time.time_id}).html( hs_time.time_name ) );
});
}
});
}

function decodeHTMLEntities(text) {
var entities = {
'amp': '&',
'apos': '\'',
'#x27': '\'',
'#x2F': '/',
'#39': '\'',
'#47': '/',
'lt': '<',
'gt': '>',
'nbsp': ' ',
'quot': '"'
}

return text.replace(/&([^;]+);/gm, function(match, entity) {
return entities[entity] || match
})
}






</script>

