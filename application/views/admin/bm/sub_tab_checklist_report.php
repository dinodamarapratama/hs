<div class="tabcontent fade in" id="sub_tab_cheklist_report">
<!-- <div role="tabpanel" class="tab-pane fade in" id="checklist_report"> -->
		<div id="">
			<div class="styled-form" style="margin-top: 30px; width: 50%;">
				<h4 class="modal-title">Filter</h4><br>
				
				<div id="filter_body_report">
					<div class="form-group form-float">
						<label class="form-label">Nama Alat</label>
						<div class="form-line">
							<select class="form-control" name="filter_id" id="filter_id" placeholder="*gunakan tab/enter">
								<option>--pilih--</option>
								<?php
									foreach ($list_mia as $key => $value) {
										echo "<option value='".$value->id."' sn='".$value->serial_number."'>".$value->alat_name."</option>";
									}
								?>
							</select>
						</div>
					</div>

					<div class="form-group form-float">
						<label class="form-label">Bulan</label>
						<div class="form-line">
							<select class="form-control" name="filter_bulan" id="filter_bulan" placeholder="*gunakan tab/enter">
								<option>--pilih--</option>
								<?php
									foreach ($list_bulan as $key => $value) {
										echo "<option value='".$key."'>".$value."</option>";
									}
								?>
							</select>
						</div>
					</div>

					<div class="form-group form-float">
						<label class="form-label">Tahun</label>
						<div class="form-line">
							<select class="form-control" name="filter_tahun" id="filter_tahun" placeholder="*gunakan tab/enter">
								<option>--pilih--</option>
								<?php
									foreach ($list_tahun as $key => $value) {
										echo "<option value='".$key."'>".$value."</option>";
									}
								?>
							</select>
						</div>
					</div>
				</div>
				

				<div class="form-group form-float">
					<div class="form-line">
						<a href="#" onclick="filterReport(); return false;" class="btn btn-default filter_btn"><i class="fa fa-search text-info"></i> Tampilkan</a>
						<a href="#" onclick="resetReport(); return false;" class="btn btn-default filter_btn"><i class="fa fa-times text-danger"></i> Reset</a>

						<div class="pull-right" id="window_btn">
							<a href="#" onclick="minimize(); return false;" class="btn btn-default"><i class="fa fa-arrows-h"></i> Min</a>
							<a href="#" onclick="maximize(); return false;" class="btn btn-default"><i class="fa fa-arrows"></i> Max</a>
						</div>
					</div>
					
				</div>

			</div>
		</div>
		<div id="form_report" style="display: none;" class="styled-form">
			<table id="head_table_report" class="table table-bordered" align="center">
				<tr>
					<th colspan="2" style="text-align: center !important; background-color: transparent; width: 200px;">
						<img class="b-lazy" data-src="<?= base_url('assets/logo-biomedika.ico') ?>">
						<h4>Bio Medika</h4>
						<p style="font-size: 10px;">Laboratorium Klinik Utama</p>
					</th>
					<th colspan="2" style="text-align: center !important; background-color: transparent;">
						<h2 id="title_checklist">Jadwal & Cheklist Perawatan </h2> 
					</th>
					<th colspan="2" style="background-color: transparent; width: 250px; text-align: left;">
						<!-- FRM/OPT/048/ ; Rev.02<br> -->
						<div class="col-md-12" style="margin-top: 15px;padding: 0; margin-left: 0;">
							<div class="col-sm-4">Cabang</div>
							<div class="col-sm-8" id="cabang_checklist">: <?= $nama_cabang ?></div>
						</div>
						<div class="col-md-12" style="padding: 0; margin-left: 0;">
							<div class="col-sm-4">Bulan</div>
							<div class="col-sm-8" id="bulan_checklist">:</div>
						</div>
						<div class="col-md-12" style="padding: 0; margin-left: 0;">
							<div class="col-sm-4">Tahun</div>
							<div class="col-sm-8" id="tahun_checklist">:</div>
						</div>
						<div class="col-md-12" style="padding: 0; margin-left: 0;">
							<div class="col-sm-4">Serial Number</div>
							<div class="col-sm-8" id="serial_checklist">:</div>
						</div>
					</th>
				</tr>
			</table>
			<div style="overflow-x: scroll;" class="vertical dragscroll">
				<h4 style="margin-top:25px; text-align:center;">Harian</h4>
				<table id="harian_table_report" class="table table-bordered table-striped" align="center">
					<tr>
						<th width="20" rowspan="2">No</th><th width="200" rowspan="2">Jenis Perawatan</th>
						<th colspan="31">Tanggal</th>
					</tr>
					<tr>
						<th>1</th><th>2</th>
						<th>3</th><th>4</th>
						<th>5</th><th>6</th>
						<th>7</th><th>8</th>
						<th>9</th><th>10</th>
						<th>11</th><th>12</th>
						<th>13</th><th>14</th>
						<th>15</th><th>16</th>
						<th>17</th><th>18</th>
						<th>19</th><th>20</th>
						<th>21</th><th>22</th>
						<th>23</th><th>24</th>
						<th>25</th><th>26</th>
						<th>27</th><th>28</th>
						<th>29</th><th>30</th>
						<th>31</th>
					</tr>
				</table>
			</div>
			
			<h4 style="margin-top:25px; text-align:center;">Mingguan</h4>
			<table id="mingguan_table_report" class="table table-bordered table-striped" align="center">
				<tr>
					<tr>
						<th width="20" rowspan="2">No</th><th width="200" rowspan="2">Jenis Perawatan</th>
						<th colspan="4">Tanggal</th>
					</tr>
				</tr>
				<tr>
					<th>Week 1</th>
					<th>Week 2</th>
					<th>Week 3</th>
					<th>Week 4</th>
				</tr>
			</table>

			<div style="overflow-x: scroll;" class="vertical dragscroll">
				<h4 style="margin-top:25px; text-align:center;">Bulanan</h4>
				<table id="bulanan_table_report" class="table table-bordered table-striped" align="center">
					<tr>
						<tr>
							<th width="20" rowspan="2">No</th><th width="200" rowspan="2">Jenis Perawatan</th>
							<th colspan="12">Tanggal</th>
						</tr>
					</tr>
					<tr>
						<th>Jan</th>
						<th>Feb</th>
						<th>Mar</th>
						<th>Apr</th>
						<th>Mei</th>
						<th>Jun</th>
						<th>Jul</th>
						<th>Agu</th>
						<th>Sep</th>
						<th>Okt</th>
						<th>Nov</th>
						<th>Des</th>
					</tr>
				</table>
			</div>

			<h4 style="margin-top:25px; text-align:center;">Triwulan</h4>
			<table id="3bulanan_table_report" class="table table-bordered table-striped" align="center">
				<tr>
					<tr>
						<th width="20" rowspan="2">No</th><th width="200" rowspan="2">Jenis Perawatan</th>
						<th colspan="4">Tanggal</th>
					</tr>
				</tr>
				<tr>
					<th>Triwulan 1</th>
					<th>Triwulan 2</th>
					<th>Triwulan 3</th>
					<th>Triwulan 4</th>
				</tr>
			</table>

			<h4 style="margin-top:25px; text-align:center;">Semester</h4>
			<table id="6bulanan_table_report" class="table table-bordered table-striped" align="center">
				<tr>
					<tr>
						<th width="20" rowspan="2">No</th><th width="200" rowspan="2">Jenis Perawatan</th>
						<th colspan="4">Tanggal</th>
					</tr>
				</tr>
				<tr>
					<th>Semester 1</th>
					<th>Semester 2</th>
				</tr>
			</table>

			<div style="overflow-x: scroll;" class="vertical dragscroll">
				<h4 style="margin-top:25px; text-align:center;">As Needed</h4>
				<table id="asneeded_table_report" class="table table-bordered table-striped" align="center">
					<tr>
						<th width="20" rowspan="2">No</th><th width="200" rowspan="2">Jenis Perawatan</th>
						<th colspan="31">Tanggal</th>
					</tr>
					<tr>
						<th>1</th><th>2</th>
						<th>3</th><th>4</th>
						<th>5</th><th>6</th>
						<th>7</th><th>8</th>
						<th>9</th><th>10</th>
						<th>11</th><th>12</th>
						<th>13</th><th>14</th>
						<th>15</th><th>16</th>
						<th>17</th><th>18</th>
						<th>19</th><th>20</th>
						<th>21</th><th>22</th>
						<th>23</th><th>24</th>
						<th>25</th><th>26</th>
						<th>27</th><th>28</th>
						<th>29</th><th>30</th>
						<th>31</th>
					</tr>
				</table>
			</div>
			
		</div>
	
<!-- </div> -->
</div>
<script type="text/javascript">
	$(document).ready( function () {
		var m = parseInt('<?= date("m") ?>');
		$('#filter_bulan').val(m).trigger('change');
		$('#filter_tahun').val('<?= date("Y") ?>').trigger('change');
	});

	function maximize(){
		$('#filter_body_report').show(500);
		$('.filter_btn').show(500);
		setTimeout(function(){
			$('#window_btn').removeClass('pull-left');
			$('#window_btn').addClass('pull-right');
		},300);
		
	}

	function minimize(){
		$('#filter_body_report').hide(500);
		$('.filter_btn').hide(500);
		setTimeout(function(){
			$('#window_btn').removeClass('pull-right');
			$('#window_btn').addClass('pull-left');
		},500);
	}

	function filterReport(){
		
		var bulan_caption = $('#filter_bulan :selected').text();
		var tahun_caption = $('#filter_tahun :selected').text();
		var alat_caption = $('#filter_id :selected').text();
		var sn_caption = $('#filter_id :selected').attr('sn');
		
		if (bulan_caption != '--pilih--' && tahun_caption != '--pilih--' && alat_caption != '--pilih--' && bulan_caption && tahun_caption && alat_caption) {

			$('#bulan_checklist').html(': '+bulan_caption);
			$('#tahun_checklist').html(': '+tahun_caption);
			$('#serial_checklist').html(': '+sn_caption);
			$('#title_checklist').html('Jadwal & Cheklist Perawatan '+alat_caption);
			$('.dom_harian, .dom_mingguan, .dom_bulanan, .dom_asneeded, .dom_3bulanan, .dom_6bulanan').remove();

			url = "<?php echo base_url('admin/intern_checklist/get_cheklist_report') ?>";
			$.ajax({
				url:url,
				dataType: "json",
				method: "post",
				data:{
					id_intern_alat:$('#filter_id').val(),
					bulan:$('#filter_bulan').val(),
					tahun:$('#filter_tahun').val()
				},
				success: function(result) {
					/*harian*/
					var DomData = '';
					var no = 1;
					$.each(result.harian,function(i,v){
						DomData += '<tr class="dom_harian">';
						DomData += '<td>'+(no++)+'</td>';
						DomData += '<td>'+i+'</td>';
						$.each(v,function(ii,vv){
							DomData += '<td align="center">'+vv+'</td>';
						});
						DomData += '</tr>';
					});
					$('#harian_table_report').append(DomData);
					/**/

					/*mingguan*/
					var DomData = '';
					var no = 1;
					$.each(result.mingguan,function(i,v){
						DomData += '<tr class="dom_mingguan">';
						DomData += '<td>'+(no++)+'</td>';
						DomData += '<td>'+i+'</td>';
						$.each(v,function(ii,vv){
							DomData += '<td align="center">'+vv+'</td>';
						});
						DomData += '</tr>';
					});
					$('#mingguan_table_report').append(DomData);
					/**/

					/*bulanan*/
					var DomData = '';
					var no = 1;
					$.each(result.bulanan,function(i,v){
						DomData += '<tr class="dom_bulanan">';
						DomData += '<td>'+(no++)+'</td>';
						DomData += '<td>'+i+'</td>';
						$.each(v,function(ii,vv){
							DomData += '<td align="center">'+vv+'</td>';
						});
						DomData += '</tr>';
					});
					$('#bulanan_table_report').append(DomData);
					/**/

					/*as needed*/
					var DomData = '';
					var no = 1;
					$.each(result.as_needed,function(i,v){
						DomData += '<tr class="dom_asneeded">';
						DomData += '<td>'+(no++)+'</td>';
						DomData += '<td>'+i+'</td>';
						$.each(v,function(ii,vv){
							DomData += '<td align="center">'+vv+'</td>';
						});
						DomData += '</tr>';
					});
					$('#asneeded_table_report').append(DomData);
					/**/

					/*3 bulanan*/
					var DomData = '';
					var no = 1;
					$.each(result.tri_month,function(i,v){
						DomData += '<tr class="dom_3bulanan">';
						DomData += '<td>'+(no++)+'</td>';
						DomData += '<td>'+i+'</td>';
						$.each(v,function(ii,vv){
							DomData += '<td align="center">'+vv+'</td>';
						});
						DomData += '</tr>';
					});
					$('#3bulanan_table_report').append(DomData);
					/**/

					/*6 bulanan*/
					var DomData = '';
					var no = 1;
					$.each(result.six_month,function(i,v){
						DomData += '<tr class="dom_6bulanan">';
						DomData += '<td>'+(no++)+'</td>';
						DomData += '<td>'+i+'</td>';
						$.each(v,function(ii,vv){
							DomData += '<td align="center">'+vv+'</td>';
						});
						DomData += '</tr>';
					});
					$('#6bulanan_table_report').append(DomData);
					/**/
				},
				error: function(jqXHR, textStatus, errorThrown) {
				
				}
			});

			$('#form_report').slideDown();
		}else{
			swal({
				title: 'Checklist Report',
				text: 'Tidak dapat memroses data, chek filter kembali!',
				type: 'error',
			});
		}
		
		
	}

	function resetReport(){
		$('#form_report').slideUp();
		$('#filter_id, #filter_bulan, #filter_tahun').val(0).trigger('change');	
	}	
</script>