
<!-- Start Master Cabang -->
<div role="tabpanel" class="tab-pane fade in" id="fs">

<div class="header">
<center><button type="button" class="btn notika-btn btn-reco-mg btn-button-mg waves-effect" onclick="tambahFs();">Add</button></center>
</div>
<div class="table-responsive">
<table id="table_fs" class="table table-bordered table-striped table-hover dataTable js-exportable" width="100%" cellspacing="0">
<thead>
<tr>
<th>No</th>
<th>Start Time</th>
<th>End Time</th>
<th>Slot</th>
<th>Branch</th>
<th>Creator</th>
<th>Created Date</th>
<th>Action</th>
</tr>
</thead>
</table>
</div>

<div class="modal fade" id="modal_fs" tabindex="-1" role="dialog">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Add Fs</h4>
</div>
<div class="modal-body">
<form action="#" id="form_masterFs">
<input type="hidden" readonly name="fs_id" class="form-control">


<label class="form-label">Start Time</label>
<div class="form-group form-float">
<div class="form-line">
<input type="time" class="form-control" name="start_fs" />
</div>
</div>

<label class="form-label">End Time</label>
<div class="form-group form-float">
<div class="form-line">
<input type="time" class="form-control" name="end_fs" />
</div>
</div>

<label class="form-label">Slot</label>
<div class="form-group form-float">
<div class="form-line">
<input type="number" class="form-control" name="slot" />
</div>
</div>


<label class="form-label">Branch</label>
<div class="form-group form-float">
<div class="form-line">
<input type="text" value="<?php echo $this->session->userdata('branch_name'); ?>" class="form-control" readonly>
</div>
</div>


<button class="btn btn-primary btn-sm hidden" type="reset">Reset</button>
</form>
</div>
<div class="modal-footer">
<input type="button" onclick="saveFs(this)" value="Save" id="btn_save">
<input type="button" data-dismiss="modal" value="Cancel">
</div>
</div>
</div>
</div>
</div>




<!-- End Master Cabang -->






<script type="text/javascript">

$(document).ready( function () {
getDataFs();
resetFormFs();

});

    
$("#table_fs");

function getDataFs()
{
url = "<?php echo base_url('admin/master_bm/get_fs') ?>";
$('#table_fs').DataTable({
scrollCollapse: true,
"order": [[ 0, "desc" ]],
"ajax": url,
"columns": [
{ "data": "no" },
{ "data": "start_fs" },
{ "data": "end_fs" },
{ "data": "slot" },
{ "data": "branch_id" },
{ "data": "creator_id" },
{ "data": "created_date" },
{  "width": "8%", 
"mRender": function(index, type, data) {
html = '';
html += '<a href="#" onclick="editFs('+data.fs_id+');"><i class="notika-icon notika-draft"></i></a>';
html += '<a href="#" onclick="hapusFs('+data.fs_id+');"><i class="notika-icon notika-trash"></i></a>';
return html;
}
},
],
"oLanguage": {
"sSearch": "Pencarian :",
"sZeroRecords": "Data tidak ditemukan.",
"sLengthMenu": "Tampilkan _MENU_ data",
"sEmptyTable": "Data tidak ditemukan.",
"sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data.",
"infoFiltered": "(dari total _MAX_ data)"
}
});
}


function resetFormFs()
{

$("#modal_fs .modal-title").text('Modal Data');
$('#form_masterFs').find('button[type=reset]').click();
$("#form_masterFs input").attr('disabled', false);
$("#form_masterFs select").attr('disabled', false);
$("#form_masterFs input[name='ptgshs_id']").val("");
$("#modal_phs .modal-footer button").show();
$("#modal_phs .modal-footer button.action").hide();


}


function reloadDataFs()
{
$("#modal_fs").modal('hide');
var table = $('#table_fs').DataTable();
table.ajax.reload();
}

function tambahFs()
{
$("#modal_fs .modal-title").text('Tambah Data');
resetFormFs();
$("#modal_fs").modal('show');
}

function editFs(fs_id)
{
resetFormPhs();
url = "<?php echo base_url('admin/master_bm/edit_fs') ?>";
$.ajax({
url:url,
dataType: "json",
method: "post",
data:{
fs_id:fs_id
},
success: function(result) {
$("#modal_fs .modal-title").text('Edit Data');
$.each(result.data, function(index, value){
$("input[name='"+index+"']").val(value);
$("select[name='"+index+"']").val(value).trigger('change');
$("input[type=date][name='"+index+"']").val(value);
})
$("#modal_fs").modal('show');
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'FS',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}

function saveFs()
{
url = "<?php echo base_url('admin/master_bm/save_fs') ?>";

var fd = new FormData();
var other_data = $('#form_masterFs').serializeArray();
$.each(other_data, function(key, input) {
fd.append(input.name, input.value);
});

$.ajax({
url: url,
type: "POST",
data: fd,
dataType: 'json',
processData: false,
contentType: false,
success: function(result) {
swal({
title: 'FS',
text: result.message,
type: 'success',
});
reloadDataFs();
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'Fs',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown)
}
});
};

function hapusFs(fs_id)
{
resetFormPhs();
var konfirmasi = confirm("Apakah anda yakin ?");
if (konfirmasi == true) {
url = "<?php echo base_url('admin/master_bm/delete_fs') ?>";
$.ajax({
url:url,
dataType: "json",
method: "post",
data:{
fs_id:fs_id
},
success: function(result) {
swal({
title: 'Fs',
text: result.message,
type: 'success',
}, function() {
reloadDataFs();
});
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'Fs',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}
}



</script>

