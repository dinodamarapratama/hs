<style>
.tab {
  float: left;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
  width: 100%;
  height: 200px;
}

/* Style the buttons that are used to open the tab content */
.tab button {
  display: block;
  background-color: inherit;
  color: black;
  padding: 5%;/*22px 16px;*/
  width: 100%;
  border: none;
  outline: none;
  text-align: left;
  cursor: pointer;
  transition: 0.3s;
}
/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current "tab button" class */
.tab button.active {
  background-color: #ccc;
}
</style>

<!-- Start Master schedule -->
<div role="tabpanel" class="tab-pane fade in" id="schedule">
	<div class="tab-content" id="container-tab" style="height: 1000px; margin-top: 10px;width:100%">
		<div class="header">
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="tab">
					<button class="tablinks alat_test active" onclick="openTab(event, 'sub_tab_alat')">Master Alat Test</button>
					<button class="tablinks cheklist_alat " onclick="openTab(event, 'sub_tab_cheklist')">Master Cheklist Alat</button>
					<button class="tablinks checklist_input" onclick="openTab(event, 'sub_tab_cheklist_input')">Input Checklist</button>
					<button class="tablinks checklist_report" onclick="openTab(event, 'sub_tab_cheklist_report')">Checklist Report</button>
				</div>
			</div>
			<div class="col-md-10">
				<?php
					$this->load->view("admin/bm/sub_tab_alat");
					$this->load->view("admin/bm/sub_tab_checklist");
					$this->load->view("admin/bm/sub_tab_checklist_input");
					$this->load->view("admin/bm/sub_tab_checklist_report");
				?>

			</div>
		</div>
	</div>
</div>
<!-- End Master Abbr -->

<script type="text/javascript">
	$(document).ready( function () {
		var tabcontent;
		// Get all elements with class="tabcontent" and hide them
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
		    tabcontent[i].style.display = "none";
		}

		$('#sub_tab_alat').show();
	});

	function openTab(evt, tab_id) {
	  	// Declare all variables
	 	var i, tablinks;

	  	var tabcontent;
		// Get all elements with class="tabcontent" and hide them
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
		    tabcontent[i].style.display = "none";
		}

	  	// Get all elements with class="tablinks" and remove the class "active"
	  	tablinks = document.getElementsByClassName("tablinks");
	  	for (i = 0; i < tablinks.length; i++) {
	    	tablinks[i].className = tablinks[i].className.replace(" active", "");
	  	}

	  	// Show the current tab, and add an "active" class to the link that opened the tab
	  	evt.currentTarget.className += " active";

	  	$('#'+tab_id).show();
	  	$('#container-tab').css('height','1000px');
	  	
		if (tab_id == 'sub_tab_cheklist_input') {
			$('#container-tab').css('height','2500px');
		}
		if (tab_id == 'sub_tab_cheklist_report') {
			$('#container-tab').css('height','2500px');
		}
	}

	function get_list_alat(){
		var selector = $('#id_intern_alat');
		var url = "<?php echo base_url('admin/master_intern_alat/dt_alat') ?>";
		$.ajax({
			url: url,
			type: "GET",
			data: {},
			success: function(result) {
				// console.log(result);
				var list = $.parseJSON(result);
				var optList = '<option>--pilih--</option>';
				console.log(list.data);
				$.each(list.data,function(i,v){
					optList +='<option value="'+v.id+'">'+v.nama_alat+' ('+v.serial_number+')</option>';
				});
				selector.html(optList);
				// console.log(optList)
			},
			error: function(jqXHR, textStatus, errorThrown) {
				// swal({
				// 	title: 'List alat',
				// 	text: 'Error',
				// 	type: 'error',
				// });
			}
		});
	}

	
</script>