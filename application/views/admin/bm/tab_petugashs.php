
<!-- Start Master Cabang -->
<div role="tabpanel" class="tab-pane fade in" id="phs">

<div class="header">
<center><button type="button" class="btn btn-success" onclick="tambahPhs();">Add</button></center>
</div>
<div class="table-responsive">
<table id="table_phs" class="table table-bordered table-striped table-hover dataTable js-exportable" width="100%" cellspacing="0">
<thead>
<tr>
<th>No</th>
<th>Petugas HS</th>
<th>Jalur</th>
<th>Start Date</th>
<th>End Date</th>
<th>Branch</th>
<th>Creator</th>
<th>Created Date</th>
<th>Start Date Sort</th>
<th>End Date Sort</th>
<th>Action</th>
</tr>
</thead>
</table>
</div>

<div class="modal fade" id="modal_phs" tabindex="-1" role="dialog">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Add Phs</h4>
</div>
<div class="modal-body">
<form action="#" id="form_masterPhs">
<input type="hidden" readonly name="ptgshs_id" class="form-control">

<div class="form-group">
  <div class="row">
    <div class="col-md-2">
      <label class="hrzn-fm">Hari</label>
    </div>
    <!--  -->
    <div class="col-md-8">
      <div class="nk-int-st">
        <input type="text" id="selectedValues" name="date" class="date-values form-control" style="width:100%" readonly onchange="return hitungTgl();" />
      </div>
      <div style="width: 75%; z-index: 13000; position: absolute;" id="calMulti">
        <div id="parent" style="display:none;">
          <div class="row header-row">
            <div class="col-md-1 previousCal">
              <a href="#" id="previousCal" onclick="previousCal(); return false;">
                <i class="fa fa-arrow-left" aria-hidden="true"></i>
              </a>
            </div>
            <div class="card-header month-selected col-sm" id="monthAndYear"></div>
            <div class="col-md-4">
              <select class="form-control col-xs-6" name="month" id="monthCal" onchange="change()"></select>
            </div>
            <div class="col-md-5">
              <select class="form-control col-xs-6" name="year" id="yearCal" onchange="change()"></select>
            </div>
            <div class="col-md-1 nextCal">
              <a href="#" id="nextCal" onclick="nextCal(); return false;">
                <i class="fa fa-arrow-right" aria-hidden="true"></i>
              </a>
            </div>
          </div>
          <table id="calendar">
            <thead>
              <tr>
                <th>S</th>
                <th>M</th>
                <th>T</th>
                <th>W</th>
                <th>T</th>
                <th>F</th>
                <th>S</th>
              </tr>
            </thead>
            <tbody id="calendarBody"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
	
<!-- <label class="form-label">Start Date</label>
<div class="form-group form-float">
<div class="form-line">
<input type="date" class="form-control" name="start_date" />
</div>
</div>

<label class="form-label">End Date</label>
<div class="form-group form-float">
<div class="form-line">
<input type="date" class="form-control" name="end_date" />
</div>
</div> -->

<label class="form-label">Petugas HS</label>
<div class="form-group form-float">
<div class="form-line">
<select name="id_user" class="form-control" id="select-usrhs" style="width: 100%">

<?php foreach ($users as $key => $u) { ?>

<option value="<?php echo $u->id_user ?>"><?php echo $u->name ?> <?php echo $u->last_name ?></option>

<?php } ?>

</select>
</div>
</div>

<label class="form-label">Jalur HS</label>
<div class="form-group form-float">
<div class="form-line">
<select name="abbr_hs" class="form-control" id="select-abbr" style="width: 100%">

<?php foreach ($abbr as $key => $a) { ?>

<option value="<?php echo $a->abbr_name ?>"><?php echo $a->abbr_name ?></option>

<?php } ?>

</select>
</div>
</div>


<label class="form-label">Branch</label>
<div class="form-group form-float">
<div class="form-line">
<input type="text" value="<?php echo $this->session->userdata('branch_name'); ?>" class="form-control" readonly>
</div>
</div>


<button class="btn btn-danger btn-sm hidden" type="reset">Reset</button>
</form>
</div>
<div class="modal-footer">
<input type="button" onclick="savePhs(this)" value="Save" id="btn_save">
<input type="button" data-dismiss="modal" value="Cancel">
</div>
</div>
</div>
</div>
</div>




<!-- End Master Cabang -->



<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/multidate_styles.css"/>
<script src="<?php echo base_url() ?>assets/js/multidatespicker.js"></script>
<link href='<?php echo base_url() ?>assets/fullcalendar/main.css' rel='stylesheet' />
<script src='<?php echo base_url() ?>assets/fullcalendar/main.js'></script>
<script src='<?php echo base_url() ?>assets/fullcalendar/locales-all.js'></script>
<script src='<?php echo base_url() ?>assets/fullcalendar/theme-chooser.js'></script>



<script type="text/javascript">

$(document).ready( function () {
getDataPhs();
resetFormPhs();

var modal_add = $('#modal_phs');



$('#modal_phs #select-usrhs').select2({
dropdownParent: modal_add,
placeholder: 'Select User'
});
$('#modal_phs .select2-search__field').css({
width: '100%'
});


$('#modal_phs #select-abbr').select2({
dropdownParent: modal_add,
placeholder: 'Select Jalur HS'
});
$('#modal_phs .select2-search__field').css({
width: '100%'
});





});

    
$("#table_phs");

function viewData(x,id){
	editPhs(id);
}


function hitungTgl() {
    var selectDate = $('#selectedValues').val();
    var tmp_sel = selectDate.split(',');
	console.log(tmp_sel);
	tmp_sel.map((o, i) => (
		console.log(o.replace(/\s/g, ''))
	))
}

function getDataPhs()
{
url = "<?php echo base_url('admin/master_bm/get_phs') ?>";
$('#table_phs').DataTable({
scrollCollapse: true,
// "order": [[ 0, "desc" ]],
"ajax": url,
"columns": [
{ "data": "no" },
{ "data": "petugas_hs" },
{ "data": "abbr_hs" },
{ "data": "start_date","iDataSort": 8 },
{ "data": "end_date","iDataSort": 9 },
{ "data": "branch_id" },
{ "data": "creator_id" },
{ "data": "created_at" },
{ "data": "start_date_sort","visible": false },
{ "data": "end_date_sort","visible": false },


{  "width": "8%", 
"mRender": function(index, type, data) {
html = '';
html += '<a href="#" onclick="editPhs('+data.ptgshs_id+');"><i class="notika-icon notika-draft"></i></a>';
html += '<a href="#" onclick="hapusPhs('+data.ptgshs_id+');"><i class="notika-icon notika-trash"></i></a>';
return html;
}
},
],
"oLanguage": {
"sSearch": "Pencarian :",
"sZeroRecords": "Data tidak ditemukan.",
"sLengthMenu": "Tampilkan _MENU_ data",
"sEmptyTable": "Data tidak ditemukan.",
"sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data.",
"infoFiltered": "(dari total _MAX_ data)"
}
});
}


function resetFormPhs()
{

$("#modal_phs .modal-title").text('Modal Data');
$('#form_masterPhs').find('button[type=reset]').click();
$("#form_masterPhs input").attr('disabled', false);
$("#form_masterPhs select").attr('disabled', false);
$("#form_masterPhs input[name='ptgshs_id']").val("");
$("#modal_phs .modal-footer button").show();
$("#modal_phs .modal-footer button.action").hide();

$("#select-usrhs").val('').trigger('change');
$("#select-abbr").val('').trigger('change');

}


function reloadDataPhs()
{
$("#modal_phs").modal('hide');
var table = $('#table_phs').DataTable();
table.ajax.reload();
}

function tambahPhs()
{
$("#modal_phs .modal-title").text('Tambah Data');
resetFormPhs();
$("#modal_phs").modal('show');
}

function editPhs(ptgshs_id)
{
resetFormPhs();
url = "<?php echo base_url('admin/master_bm/edit_phs') ?>";
$.ajax({
url:url,
dataType: "json",
method: "post",
data:{
ptgshs_id:ptgshs_id
},
success: function(result) {
$("#modal_phs .modal-title").text('Edit Data');
$.each(result.data, function(index, value){
$("input[name='"+index+"']").val(value);
$("select[name='"+index+"']").val(value).trigger('change');
$("input[type=date][name='"+index+"']").val(value);
})
$("#modal_phs").modal('show');
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'Phs',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}

function savePhs()
{

url = "<?php echo base_url('admin/master_bm/save_phs') ?>";

var fd = new FormData();
var other_data = $('#form_masterPhs').serializeArray();
$.each(other_data, function(key, input) {
fd.append(input.name, input.value);
});

var id_notif = '<?= isset($_GET["notif"]) ? $_GET["notif"] : "" ?>';
fd.append('id_notif', id_notif);

		fd.delete('month');
		fd.delete('year');
		fd.delete('date');
		fd.delete('start_date');
		fd.delete('end_date');

		for (var pair of fd.entries()) {
			console.log(pair[0]+ ', ' + pair[1]); 
		};

	var selectDate = $('#selectedValues').val();
    var tmp_sel = selectDate.split(',');

	tmp_sel.map(o => {
		console.log("salt", o.replace(/\s/g, '').split("-").reverse().join("-"));
		fd.append('date', o.replace(/\s/g, '').split("-").reverse().join("-"));
		fd.append('start_date', o.replace(/\s/g, '').split("-").reverse().join("-"));
		fd.append('end_date', o.replace(/\s/g, '').split("-").reverse().join("-"));	

$.ajax({
url: url,
type: "POST",
data: fd,
dataType: 'json',
processData: false,
contentType: false,
success: function(result) {
if(result.status){
	swal({
	title: 'Phs',
	text: result.message,
	type: 'success',
	});
	reloadDataPhs();
	refreshSelectPhs();
}
else{
	swal({
	title: 'Phs',
	text: result.message,
	type: 'error',
	});
}

},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'Phs',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown)
}
});


	});

	for (var pair of fd.entries()) {
			console.log(pair[0]+ ', ' + pair[1]); 
	}




};

function hapusPhs(ptgshs_id)
{
resetFormPhs();
var konfirmasi = confirm("Apakah anda yakin ?");
if (konfirmasi == true) {
url = "<?php echo base_url('admin/master_bm/delete_phs') ?>";
$.ajax({
url:url,
dataType: "json",
method: "post",
data:{
ptgshs_id:ptgshs_id
},
success: function(result) {
swal({
title: 'Phs',
text: result.message,
type: 'success',
}, function() {
reloadDataPhs();
});
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'Phs',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}
}

function refreshSelectPhs(){
var select1 = $("#form_masterPHs #ptgshs_id");

select1.html("");

$.ajax({
method: "post",
url: base_url + "admin/master_bm/get_phs",
success: function (resp) {
var listPhs = [];
try{
var json = JSON.parse(resp);
listPHs = json.data;
}catch(x) {
console.log("error parse to json", resp, x);
}
$.each(listPhs, function(index, unit) {
select1.append( $("<option/>").attr({value: hs_initial_petugas_hs.ptgshs_id}).html( hs_initial_petugas_hs.ptgshs_id ) );
});
}
});
}

function decodeHTMLEntities(text) {
var entities = {
'amp': '&',
'apos': '\'',
'#x27': '\'',
'#x2F': '/',
'#39': '\'',
'#47': '/',
'lt': '<',
'gt': '>',
'nbsp': ' ',
'quot': '"'
}

return text.replace(/&([^;]+);/gm, function(match, entity) {
return entities[entity] || match
})
}






</script>

