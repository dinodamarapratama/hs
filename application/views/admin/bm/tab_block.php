

<!-- Start Master block -->
<div role="tabpanel" class="tab-pane fade in" id="block">

<div class="header">
<center><button type="button" class="btn btn-success" onclick="tambahblock();">Add</button></center>
</div>
<div class="table-responsive">
<table id="table_block" class="table table-bordered table-striped table-hover dataTable js-exportable" width="100%" cellspacing="0">
<thead>
<tr>
<th>No</th>
<th>Jalur</th>
<th>Petugas</th>
<th>Nama Pasien</th>
<th>Hari</th>
<th>Time</th>
<th>Branch</th>
<th>Creator</th>
<th>Created Date</th>
<th>Action</th>
</tr>
</thead>
</table>
</div>

<div class="modal fade" id="modal_block" tabindex="-1" role="dialog">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Add Block</h4>
</div>
<div class="modal-body">
<form action="#" id="form_masterblock">
<input type="hidden" readonly name="block_id" class="form-control">


<label class="form-label">Petugas</label>
<div class="form-group form-float">
<div class="form-line">
<select name="ptgs_id" class="form-control" id="select-usr" style="width: 100%">

<?php foreach ($hs_initial_petugas_hs as $key => $u) { ?>

<option value="<?php echo $u->ptgshs_id ?>"><?php echo $u->abbr_hs ?> - <?php echo $u->fullname ?></option>

<?php } ?>

</select>
</div>
</div>

<label class="form-label">Nama</label>
<div class="form-group form-float">
<div class="form-line">
<input type="text" name="nama"  class="form-control">
</div>
</div>

<label class="form-label">Hari</label>
<div class="form-group form-float">
<div class="form-line">
<select name="hari_id" class="form-control" id="select-hari" style="width: 100%">

<option value="1">Senin</option>
<option value="2">Selasa</option>
<option value="3">Rabu</option>
<option value="4">Kamis</option>
<option value="5">Jumat</option>
<option value="6">Sabtu</option>
<option value="7">Minggu</option>

</select>
</div>
</div>


<label class="form-label">Branch</label>
<div class="form-group form-float">
<div class="form-line">
<input type="text" value="<?php echo $this->session->userdata('branch_name'); ?>" class="form-control" readonly>
</div>
</div>


<label class="form-label">Time</label>
<div class="form-group form-float">
<div class="form-line">
<?php foreach ($time as $key => $t) { ?>
<input name="time_id[]" value="<?php echo $t->time_id ?>" type="checkbox" id="<?php echo $t->time_name ?>">
<label for="<?php echo $t->time_name ?>"><?php echo $t->time_name ?></label>

<?php } ?>
</div>
</div>
<br>


<button class="btn btn-danger btn-sm hidden" type="reset">Reset</button>
</form>
</div>
<div class="modal-footer">
<input type="button" onclick="saveblock(this)" value="Save" id="btn_save">
<input type="button" data-dismiss="modal" value="Cancel">
</div>
</div>
</div>
</div>
</div>




<!-- End Master block -->






<script type="text/javascript">

$(document).ready( function () {
getDatablock();
resetFormblock();

var modal_block = $('#modal_block');
var modal_hari = $('#modal_block');


$('#modal_block #select-usr').select2({
dropdownParent: modal_block,
placeholder: 'Select Petugas'
});
$('#modal_block .select2-search__field').css({
width: '100%'
});

$('#modal_block #select-hari').select2({
dropdownParent: modal_hari,
placeholder: 'Select Hari'
});
$('#modal_block .select2-search__field').css({
width: '100%'
});

});

    
$("#table_block");

function getDatablock()
{
url = "<?php echo base_url('admin/master_bm/get_block') ?>";
$('#table_block').DataTable({
scrollCollapse: true,
"order": [[ 0, "desc" ]],
"ajax": url,
"columns": [
{ "data": "no" },
{ "data": "ptgs_id" },
{ "data": "ptgs" },
{ "data": "nama" },
{
"className": "center",
"mRender": function(index, type, data) {
html = '';
if (data.hari_id == '1') {
html += 'Senin';
}
if (data.hari_id == '2') {
html += 'Selasa';
}
if (data.hari_id == '3') {
html += 'Rabu';
}
if (data.hari_id == '4') {
html += 'Kamis';
}
if (data.hari_id == '5') {
html += 'Jumat';
}
if (data.hari_id == '6') {
html += 'Sabtu';
}
if (data.hari_id == '7') {
html += 'Minggu';
}
return html;
}
},
{ "data": "time_name" },
{ "data": "branch_id" },
{ "data": "creator_id" },
{ "data": "created_at" },
{  "width": "8%", 
"mRender": function(index, type, data) {
html = '';
html += '<a href="#" onclick="editblock('+data.block_id+');"><i class="notika-icon notika-draft"></i></a>';
html += '<a href="#" onclick="hapusblock('+data.block_id+');"><i class="notika-icon notika-trash"></i></a>';
return html;
}
},
],
"oLanguage": {
"sSearch": "Pencarian :",
"sZeroRecords": "Data tidak ditemukan.",
"sLengthMenu": "Tampilkan _MENU_ data",
"sEmptyTable": "Data tidak ditemukan.",
"sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data.",
"infoFiltered": "(dari total _MAX_ data)"
}
});
}


function resetFormblock()
{

$("#modal_block .modal-title").text('Modal Data');
$('#form_masterblock').find('button[type=reset]').click();
$("#form_masterblock input").attr('disabled', false);
$("#form_masterblock select").attr('disabled', false);
$("#form_masterblock input[name='block_id']").val("");
$("#modal_block .modal-footer button").show();
$("#modal_block .modal-footer button.action").hide();


}


function reloadDatablock()
{
$("#modal_block").modal('hide');
var table = $('#table_block').DataTable();
table.ajax.reload();
}

function tambahblock()
{
$("#modal_block .modal-title").text('Tambah Data');
resetFormblock();
$("#modal_block").modal('show');
}

function editblock(block_id)
{
resetFormblock();
url = "<?php echo base_url('admin/master_bm/edit_block') ?>";
$.ajax({
url:url,
dataType: "json",
method: "post",
data:{
block_id:block_id
},
success: function(result) {
$("#modal_block .modal-title").text('Edit Data');
$.each(result.data, function(index, value){
$("input[name='"+index+"']").val(value);
$("select[name='"+index+"']").val(value).trigger('change');
$("input[type=date][name='"+index+"']").val(value);
$("input[type=checkbox][name='"+index+"']").val(value);
})
$("#modal_block").modal('show');
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'block',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}

function saveblock()
{
url = "<?php echo base_url('admin/master_bm/save_block') ?>";

var fd = new FormData();
var other_data = $('#form_masterblock').serializeArray();
$.each(other_data, function(key, input) {
fd.append(input.name, input.value);
});

$.ajax({
url: url,
type: "POST",
data: fd,
dataType: 'json',
processData: false,
contentType: false,
success: function(result) {
swal({
title: 'block',
text: result.message,
type: 'success',
});
reloadDatablock();
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'block',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown)
}
});
};

function hapusblock(block_id)
{
resetFormblock();
var konfirmasi = confirm("Apakah anda yakin ?");
if (konfirmasi == true) {
url = "<?php echo base_url('admin/master_bm/delete_block') ?>";
$.ajax({
url:url,
dataType: "json",
method: "post",
data:{
block_id:block_id
},
success: function(result) {
swal({
title: 'block',
text: result.message,
type: 'success',
}, function() {
reloadDatablock();
});
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'block',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}
}

function refreshSelectblock(){
var select1 = $("#form_masterblock #block_id");

select1.html("");

$.ajax({
method: "post",
url: base_url + "admin/master_bm/get_block",
success: function (resp) {
var listblock = [];
try{
var json = JSON.parse(resp);
listblock = json.data;
}catch(x) {
console.log("error parse to json", resp, x);
}
$.each(listblock, function(index, unit) {
select1.append( $("<option/>").attr({value: hs_block.block_id}).html( hs_block.block_name ) );
});
}
});
}

function decodeHTMLEntities(text) {
var entities = {
'amp': '&',
'apos': '\'',
'#x27': '\'',
'#x2F': '/',
'#39': '\'',
'#47': '/',
'lt': '<',
'gt': '>',
'nbsp': ' ',
'quot': '"'
}

return text.replace(/&([^;]+);/gm, function(match, entity) {
return entities[entity] || match
})
}






</script>

