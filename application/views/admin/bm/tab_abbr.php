
<!-- Start Master Abbr -->
<div role="tabpanel" class="tab-pane fade in" id="abbr">

<div class="header">
<center><button type="button" class="btn btn-success" onclick="tambahabbr();">Add</button></center>
</div>
<div class="table-responsive">
<table id="table_abbr" class="table table-bordered table-striped table-hover dataTable js-exportable" width="100%" cellspacing="0">
<thead>
<tr>
<th>No</th>
<th>Jalur Name</th>
<th>Lokasi</th>
<th>Branch</th>
<th>Creator</th>
<th>Created Date</th>
<th>Action</th>
</tr>
</thead>
</table>
</div>

<div class="modal fade" id="modal_abbr" tabindex="-1" role="dialog">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Add Jalur</h4>
</div>
<div class="modal-body">
<form action="#" id="form_masterabbr">
<input type="hidden" readonly name="abbr_id" class="form-control">


<label class="form-label">Jalur Name</label>
<div class="form-group form-float">
<div class="form-line">
<input type="text" class="form-control" name="abbr_name" />
</div>
</div>

<label class="form-label">Lokasi</label>
<div class="form-group form-float">
<div class="form-line">
<textarea name="lokasi" id="input_lokasi" class="form-control"></textarea>
</div>
</div>


<label class="form-label">Branch</label>
<div class="form-group form-float">
<div class="form-line">
<input type="text" value="<?php echo $this->session->userdata('branch_name'); ?>" class="form-control" readonly>
</div>
</div>


<button class="btn btn-danger btn-sm hidden" type="reset">Reset</button>
</form>
</div>
<div class="modal-footer">
<input type="button" onclick="saveabbr(this)" value="Save" id="btn_save">
<input type="button" data-dismiss="modal" value="Cancel">
</div>
</div>
</div>
</div>
</div>




<!-- End Master Abbr -->






<script type="text/javascript">

$(document).ready( function () {
getDataabbr();
resetFormabbr();



});

    
$("#table_abbr");

function getDataabbr()
{
url = "<?php echo base_url('admin/master_bm/get_abbr') ?>";
$('#table_abbr').DataTable({
scrollCollapse: true,
"order": [[ 0, "desc" ]],
"ajax": url,
"columns": [
{ "data": "no" },
{ "data": "abbr_name"},
{ "data": "lokasi"},
{ "data": "branch_id" },
{ "data": "creator_id" },
{ "data": "created_at" },
{  "width": "8%", 
"mRender": function(index, type, data) {
html = '';
html += '<a href="#" onclick="editabbr('+data.abbr_id+');"><i class="notika-icon notika-draft"></i></a>';
html += '<a href="#" onclick="hapusabbr('+data.abbr_id+');"><i class="notika-icon notika-trash"></i></a>';
return html;
}
},
],
"oLanguage": {
"sSearch": "Pencarian :",
"sZeroRecords": "Data tidak ditemukan.",
"sLengthMenu": "Tampilkan _MENU_ data",
"sEmptyTable": "Data tidak ditemukan.",
"sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data.",
"infoFiltered": "(dari total _MAX_ data)"
}
});
}


function resetFormabbr()
{

$("#modal_abbr .modal-title").text('Modal Data');
$('#form_masterabbr').find('button[type=reset]').click();
$("#form_masterabbr input").attr('disabled', false);
$("#form_masterabbr select").attr('disabled', false);
$("#form_masterabbr input[name='abbr_id']").val("");
$("#modal_abbr .modal-footer button").show();
$("#modal_abbr .modal-footer button.action").hide();


}


function reloadDataabbr()
{
$("#modal_abbr").modal('hide');
var table = $('#table_abbr').DataTable();
table.ajax.reload();
}

function tambahabbr()
{
$("#modal_abbr .modal-title").text('Tambah Data');
resetFormabbr();
$("#modal_abbr").modal('show');
}

function editabbr(abbr_id)
{
resetFormabbr();
url = "<?php echo base_url('admin/master_bm/edit_abbr') ?>";
$.ajax({
url:url,
dataType: "json",
method: "post",
data:{
abbr_id:abbr_id
},
success: function(result) {
$("#modal_abbr .modal-title").text('Edit Data');
$.each(result.data, function(index, value){
$("input[name='"+index+"']").val(value);
$("select[name='"+index+"']").val(value);
$("input[type=date][name='"+index+"']").val(value);
$("textarea#input_lokasi").val(result.data.lokasi);
})
$("#modal_abbr").modal('show');
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'abbr',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}

function saveabbr()
{
url = "<?php echo base_url('admin/master_bm/save_abbr') ?>";

var fd = new FormData();
var other_data = $('#form_masterabbr').serializeArray();
$.each(other_data, function(key, input) {
fd.append(input.name, input.value);
});

$.ajax({
url: url,
type: "POST",
data: fd,
dataType: 'json',
processData: false,
contentType: false,
success: function(result) {
swal({
title: 'abbr',
text: result.message,
type: 'success',
});
reloadDataabbr();
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'abbr',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown)
}
});
};

function hapusabbr(abbr_id)
{
resetFormabbr();
var konfirmasi = confirm("Apakah anda yakin ?");
if (konfirmasi == true) {
url = "<?php echo base_url('admin/master_bm/delete_abbr') ?>";
$.ajax({
url:url,
dataType: "json",
method: "post",
data:{
abbr_id:abbr_id
},
success: function(result) {
swal({
title: 'abbr',
text: result.message,
type: 'success',
}, function() {
reloadDataabbr();
});
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'abbr',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}
}

function refreshSelectabbr(){
var select1 = $("#form_masterabbr #abbr_id");

select1.html("");

$.ajax({
method: "post",
url: base_url + "admin/master_bm/get_abbr",
success: function (resp) {
var listabbr = [];
try{
var json = JSON.parse(resp);
listabbr = json.data;
}catch(x) {
console.log("error parse to json", resp, x);
}
$.each(listabbr, function(index, unit) {
select1.append( $("<option/>").attr({value: hs_abbr.abbr_id}).html( hs_abbr.abbr_name ) );
});
}
});
}

function decodeHTMLEntities(text) {
var entities = {
'amp': '&',
'apos': '\'',
'#x27': '\'',
'#x2F': '/',
'#39': '\'',
'#47': '/',
'lt': '<',
'gt': '>',
'nbsp': ' ',
'quot': '"'
}

return text.replace(/&([^;]+);/gm, function(match, entity) {
return entities[entity] || match
})
}






</script>

