<style type="text/css">
	.styled-form{
		box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.35);
		/*border-radius: 10px;*/
		border: 6px solid #dee3de;
		padding: 15px;
		margin-bottom: 120px;
	}
</style>
<div class="tabcontent fade in" id="sub_tab_cheklist">
	<!-- master checklis perawatan -->
	<div role="tabpanel" class="tab-pane fade in">
		<div class="header" style="margin-bottom: 10px; margin-left: 10px;">
			<br>
			<button type="button" class="btn notika-btn btn-primary btn-reco-mg btn-button-mg waves-effect" onclick="tambah1();"><i class="fa fa-plus-circle"></i> Tambah</button>
		</div>
		<div class="table-responsive"> 
			<div class="col-md-9">
				<table id="table_checklist" class="table table-bordered table-striped table-hover dataTable js-exportable">
					<thead>
					<tr>
						<th width="50px;">No</th>
						<th>Nama Alat</th>
						<th>Nama Chekclist</th>
						<th>Periode Check</th>
						<th>Keterangan</th>
						<th>Branch</th>
						<th>Creator</th>
						<th>Aksi</th>
					</tr>
					</thead>
				</table>	
			</div> 
			<div class="col-md-3">
				<div class="styled-form">
					<h4 class="modal-title">Filter</h4><br>
					<div class="form-group form-float">
						<label class="form-label">Nama Alat</label>
						<div class="form-line">
							<input type="text" class="form-control" id="alat_filter" placeholder="*gunakan tab/enter" onchange="filterTableB(1, this);">
						</div>
					</div>
					<div class="form-group form-float">
						<label class="form-label">Cheklist</label>
						<div class="form-line">
							<input type="text" class="form-control" id="checklist_filter" placeholder="*gunakan tab/enter" onchange="filterTableB(2, this);">
						</div>
					</div>
					<div class="form-group form-float">
						<label class="form-label">Periode</label>
						<div class="form-line">
							<input type="text" class="form-control" id="periode_filter" placeholder="*gunakan tab/enter" onchange="filterTableB(3, this);">
						</div>
					</div>

					<div class="form-group form-float">
						<div class="form-line">
							<a href="#" onclick="resetFilter(); return false;" class="btn btn-default"><i class="fa fa-times text-danger"></i> Reset</a>
						</div>
					</div>

				</div>
			</div>            
		</div>

		<!-- modal input data -->
		<div class="modal fade" id="modal1" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="modal-header">
					<h4 class="modal-title">Tambah Cheklist Perawatan</h4>
					</div>
					<div class="modal-body">
						<form action="#" id="form_checklist">
							
							<div class="form-group form-float">
								<label class="form-label">Alat Test Terdaftar</label>
								<div class="form-line">
									<select class="form-control" name="id_intern_alat" id="id_intern_alat">
									</select>
								</div>
							</div>
							<div class="form-group form-float">
								<label class="form-label">Periode</label>
								<div class="form-line">
									<select class="form-control" name="periode" id="periode">
										<option>--pilih--</option>
										<?php
											$periode = [
												'harian' => 'Harian',
												'mingguan' => 'Mingguan',
												'bulanan' => 'Bulanan',
												'tri_month' => '3 Bulanan',
												'six_month' => '6 Bulanan',
												'as_needed' => 'As needed'
											];
											foreach ($periode as $key => $value) {
												echo "<option value='".$key."'>".$value."</option>";
											}
										?>
									</select>
								</div>
							</div>
							<div class="form-group form-float">
								<label class="form-label">Nama Perawatan</label>
								<div class="form-line">
									<input type="text" class="form-control" name="checklist_name">
								</div>
							</div>
							<div class="form-group form-float">
								<label class="form-label">Keterangan</label>
								<div class="form-line">
									<textarea class="form-control" name="note" id="note"></textarea>
								</div>
							</div>
							<input type="hidden" name="ic_id" id="ic_id">
						</form>
					</div>
					<div class="modal-footer">
						<a href="#" class="btn btn-success waves-effect" onclick="save1()"> simpan</a>
						<a href="#" class="btn btn-danger waves-effect" data-dismiss="modal"> cancel</a>
					</div>

				</div>
			</div>
		</div>
	</div>
	<!--  -->
</div>
<!-- script cheklist perawatan -->
<script type="text/javascript">
	$(document).ready( function () {
		resetForm1();
		getData1();
		get_list_alat();
	});

	function filterTableB(column=0,x){
		var table = $('#table_checklist').DataTable();
		var value = $(x).val();
		value = '/'+value+'/i';

			table
		    .columns( column )
		    .search( $(x).val() )
            .draw();
	}

	function resetForm1()
	{
		$('input[name="ic_id"]').val('');
		$('input[name="checklist_name"]').val('').trigger('change');
		$('select[name="id_intern_alat"]').val('').trigger('change');
		$('select[name="periode"]').val('');
		$('textarea[name="note"]').val('');
		$('.error_field').remove();
	}

	function tambah1()
	{
		$("#modal1 .modal-title").text('Tambah Data');
		resetForm1();
		$("#modal1").modal('show');
	}



	function getData1()
	{
		url = "<?php echo base_url('admin/master_intern_alat/dt_checklist') ?>";
		$('#table_checklist').DataTable({
			"ajax": url,
			"order": [[ 0, "desc" ]],
			"columns": [
				{ "data": "no" },
				{ "data": "nama_alat" },
				{ "data": "nama_checklist" },
				{ "data": "periode" },
				{ "data": "note" },
				{ "data": "nama_branch" },
				{ "data": "creator" },
				{  "width": "8%", 
				"mRender": function(index, type, data) {
					html = '';
					html += '<a href="#" class="btn notika-btn btn-warning btn-sm" onclick="edit1('+data.id+');"><i style="color: #fff; font-size:12px;" class="material-icons">edit</i> </a>';
					html += '<a href="#" class="btn notika-btn btn-danger btn-sm" onclick="hapus1('+data.id+');"><i style="color: #fff; font-size:12px;" class="material-icons">delete</i> </a>';
					return html;
				}
			},
			],
			"oLanguage": {
			"sSearch": "Pencarian :",
			"sZeroRecords": "Data tidak ditemukan.",
			"sLengthMenu": "Tampilkan _MENU_ data",
			"sEmptyTable": "Data tidak ditemukan.",
			"sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data.",
			"infoFiltered": "(dari total _MAX_ data)"
			}
		});

		/*set*/
		$("#table_checklist").css("font-size", 11);
		$("#table_checklist").css("width", '100%');
	}

	function reloadData1(){
		$('#table_checklist').DataTable().destroy();
		$("#modal1").modal('hide');
		getData1();
		resetForm1();
		get_list_alat();
	}

	function edit1(id)
	{
		resetForm();
		url = "<?php echo base_url('admin/master_intern_alat/edit_checklist') ?>";
		$.ajax({
			url:url,
			dataType: "json",
			method: "post",
			data:{
				ic_id:id
			},
			success: function(result) {
				$("#modal1 .modal-title").text('Edit Data');
				$.each(result.data, function(index, value){
					$("input[name='ic_id']").val(value.id)
					$("select[name='id_intern_alat']").val(value.id_intern_alat).trigger('change');
					$("select[name='periode']").val(value.periode).trigger('change');
					
					$("input[name='checklist_name']").val(value.checklist_name);
					$("textarea[name='note']").val(value.note);
				})
				$("#modal1").modal('show');
			},
			error: function(jqXHR, textStatus, errorThrown) {
			swal({
				title: 'Master Cheklist Alat',
				text: 'Error',
				type: 'error',
			});
			}
		});
	}

	function save1()
	{
		var ic_id = $('input[name="ic_id"]').val();
		var url = "<?php echo base_url('admin/master_intern_alat/save_checklist') ?>";
		if (ic_id.length>0) {
			url += '/'+1;
		}

		$.ajax({
			url: url,
			type: "POST",
			data: $('#form_checklist').serialize(),
			success: function(result) {
				swal({
					title: 'Master Cheklist',
					text: result.message,
					type: 'success',
				}, function() {
					reloadData1();
				});
			},
			error: function(jqXHR, textStatus, errorThrown) {
			swal({
				title: 'Master Cheklist',
				text: 'Error',
				type: 'error',
			});
			}
		});
	};

	function hapus1(id)
	{
		var konfirmasi = confirm("Apakah anda yakin ?");
		if (konfirmasi == true) {
			url = "<?php echo base_url('admin/master_intern_alat/hapus_checklist') ?>";
			$.ajax({
				url:url,
				dataType: "json",
				method: "post",
				data:{
					ic_id:id
				},
				success: function(result) {
					swal({
						title: 'Master Chekclist',
						text: result.message,
						type: 'success',
					}, function() {
						reloadData1();
					});
				},
				error: function(jqXHR, textStatus, errorThrown) {
				swal({
					title: 'Master Chekclist',
					text: 'Error',
					type: 'error',
				});
				
				}
			});
		}
	}


</script>