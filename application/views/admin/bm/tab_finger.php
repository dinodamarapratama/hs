<div role="tabpanel" class="tab-pane fade in" id="finger" style="margin-top: 10px;">
	<!-- Start Master Finger -->
	<div class="header">
		<div class="col-md-3">
			<div class="form-group form-float">
				<label class="form-label">Filter Nama Depan</label>
				<div class="form-line">
					<select id="search_kar" name="search_kar" class="form-control">
					    <option value="">Semua</option>
					    </select>
				</div>
			</div>

			<div class="form-group form-float">
				<label class="form-label">Filter Nama Belakang</label>
				<div class="form-line">
					<select id="search_blkng" name="search_blkng" class="form-control">
					    <option value="">Semua</option>
					    </select>
				</div>
			</div>

			<form action="<?= base_url('admin/hr/excelAbsen')?>" method="post">
				<div class="form-group">
					<label for="startDate">Start Date</label>
					<input type="date" name="startDate" id="startDate" class="form-control" value="<?= date('Y-m-01') ?>" />
				</div>
				<div class="form-group">
					<label for="endDate">End Date</label>
					<input type="date" name="endDate" id="endDate" class="form-control" value="<?= date('Y-m-t') ?>" />
				</div>
				<center><button type="submit" class="btn btn-default btn-sm">Export Excel</button></center>
			</form>
		</div>
	</div>

	<div class="table-responsive">
		<table id="table_finger" class="table table-bordered table-striped table-hover dataTable js-exportable" width="100%" cellspacing="0">
			<thead>
				<tr>
					<th>No</th>
					<th>Finger ID</th>
					<th>Nama Karyawan</th>
					<th>Tanggal</th>
					<th>Jam Datang</th>
					<th>Jam Pulang</th>
					<th>Cabang</th>

				</tr>
			</thead>
		</table>
	</div>

	<!-- End Master Finger -->
</div>

<script type="text/javascript">

	$(document).ready( function () {
		$("#table_finger").css("font-size", 11);

		$('#search_kar').change(function(){
			var table = $('#table_finger').DataTable();
			var nama = '';
			if ($(this).find(':selected').text() != 'Semua') {
				nama = $(this).find(':selected').text();
			}

			table.column('nama:name').search(nama).draw();
		});

		$('#search_blkng').change(function(){
			var table = $('#table_finger').DataTable();
			var nama_belakang = '';
			if ($(this).find(':selected').text() != 'Semua') {
				nama_belakang = $(this).find(':selected').text();
			}

			table.column('nama_belakang:name').search(nama_belakang).draw();
		});

		$('#search_cab').change(function(){
			var table = $('#table_finger').DataTable();
			var Cabang = '';
			if ($(this).find(':selected').text() != 'Semua') {
				Cabang = $(this).find(':selected').text();
			}

			table.column('Cabang:name').search(Cabang).draw();
		});

		$('#startDate').change(function(){
			filterData();
		});

		$('#endDate').change(function(){
			filterData();
		});

		$('#tab_finger').click(function(){
			$('#table_finger').DataTable().clear();
			$('#table_finger').DataTable().destroy();

			var defFilter = {
				'cabang' : '<?= $this->session->userdata("branch_name") ?>',
				'tgl1' : $('#startDate').val(),
				'tgl2' : $('#endDate').val()
			}; 

			var is_branch = '<?= $this->session->userdata("branch_id") ?>';
			if (is_branch == 16) {
				/*remove branch from filter*/
				delete defFilter['cabang'];
			}
			
			get_option();
			getDataFinger(defFilter);
		});
	});

	function filterData(){
		var data_parse = {
			'name' : $('#search_kar').find(':selected').text(),
			'tgl1' : $('#startDate').val(),
			'tgl2' : $('#endDate').val(),
			'cabang' : '<?= $this->session->userdata("branch_name") ?>',
		};

		var is_branch = '<?= $this->session->userdata("branch_id") ?>';
		if (is_branch == 16) {
			/*remove branch from filter*/
			delete data_parse['cabang'];
		}
		
		$('#table_finger').DataTable().clear();
		$('#table_finger').DataTable().destroy();
		getDataFinger(data_parse);
	}

	function get_option(){
		/* nama depan & nama belakang*/
		var obj_depan = $('#search_kar');
		var obj_blkg = $('#search_blkng');

		var base_url = '<?= base_url() ?>';
		$.ajax({
			async: false,
			method: "post",
			url: base_url + "admin/master_bm/get_karyawan",
			data: {},
			success: function(resp) {
				var dataList = $.parseJSON(resp);
				$(obj_depan).html('');
				$(obj_blkg).html('');
				htmlOpt = htmlOpt1 = '<option value="">Semua</option>'; 
				$.each(dataList.data,function(i,v){
					htmlOpt +='<option value="'+v.name+'">'+v.name+'</option>';
					htmlOpt1 +='<option value="'+v.last_name+'">'+v.last_name+'</option>';
				});

				obj_depan.append(htmlOpt);
				obj_blkg.append(htmlOpt1);
			}
		});
	}

	function getDataFinger(data_parse={}){
		url = "<?php echo base_url('admin/hr/get_finger') ?>";
		$('#table_finger').DataTable( {
	        "scrollCollapse": true,
			"scrollY": "500px",
	        "ajax": {
	            "url": url,
	            "dataSrc": 'data',
	            "type" : "POST",
	            "data" : data_parse
	        },
	        "order": [[ 0, 'desc' ]],
	        "iDisplayLength": 10,
	        "columns": [
				{ "name": "no","data": "no" },
				{ "name": "FingerId","data": "FingerId"},
				{ "name": "nama","data": "nama"},
				{ "name": "Date","data": "Date" },
				{ "name": "jam_datang","data": "jam_datang" },
				{ "name": "jam_pulang","data": "jam_pulang" },
				{ "name": "Cabang","data": "Cabang" }
			],
			"oLanguage": {
				"sSearch": "Pencarian :",
				"sZeroRecords": "Data tidak ditemukan.",
				"sLengthMenu": "Tampilkan _MENU_ data",
				"sEmptyTable": "Data tidak ditemukan.",
				"sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data.",
				"infoFiltered": "(dari total _MAX_ data)"
			}
	    });
	}

</script>

