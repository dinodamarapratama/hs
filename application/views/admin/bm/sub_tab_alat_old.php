<style type="text/css">
	.styled-form{
		box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.35);
		/*border-radius: 10px;*/
		border: 6px solid #dee3de;
		padding: 15px;
		margin-bottom: 120px;
	}
</style>
<div class="tabcontent fade in" id="sub_tab_alat">
	<div>
		<div class="header" style="margin-bottom: 10px; margin-left: 10px;">
			<br>
			<button type="button" class="btn notika-btn btn-primary btn-reco-mg btn-button-mg waves-effect" onclick="tambah();"><i class="fa fa-plus-circle"></i> Tambah</button>
		</div>
		<div class="table-responsive">   
			       
			<div class="col-md-9">
				<table id="table_alat" class="table table-bordered table-striped table-hover dataTable js-exportable">
					<thead>
					<tr>
						<th width="50px;">No</th>
						<th>Nama Alat</th>
						<th>Serial Number</th>
						<th>Keterangan</th>
						<th>Branch</th>
						<th>Creator</th>
						<th>Aksi</th>
					</tr>
					</thead>
				</table>	
			</div> 
			<div class="col-md-3">
				<div class="styled-form">
					<h4 class="modal-title">Filter</h4><br>
					<div class="form-group form-float">
						<label class="form-label">Nama Alat</label>
						<div class="form-line">
							<input type="text" class="form-control" id="alat_filter" placeholder="*gunakan tab/enter" onchange="filterTableA(1, this);">
						</div>
					</div>
					<div class="form-group form-float">
						<label class="form-label">Serial Number</label>
						<div class="form-line">
							<input type="text" class="form-control" id="serial_filter" placeholder="*gunakan tab/enter" onchange="filterTableA(2, this);">
						</div>
					</div>
					<div class="form-group form-float">
						<div class="form-line">
							<a href="#" onclick="resetFilter(); return false;" class="btn btn-default"><i class="fa fa-times text-danger"></i> Reset</a>
						</div>
					</div>

				</div>
			</div>     
		</div>

		<!-- modal input data -->
		<div class="modal fade" id="modal" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="modal-header">
					<h4 class="modal-title">Tambah Intern Alat Test</h4>
					</div>
					<div class="modal-body">
						<form action="#" id="form_intern_alat">
							
							<div class="form-group form-float">
								<label class="form-label">Nama</label>
								<div class="form-line">
									<select class="form-control" name="id_alat" id="id_alat">
										<?php
											foreach ($list_alat as $key => $value) {
												echo "<option value='".$value->id_alat."'>".$value->alat_name."</option>";
											}
										?>
									</select>
								</div>
							</div>

							<div class="form-group form-float">
								<label class="form-label">Serial Number</label>
								<div class="form-line">
									<input type="text" name="serial_number" id="serial_number" class="form-control" />
								</div>
							</div>

							<div class="form-group form-float">
								<label class="form-label">Keterangan</label>
								<div class="form-line">
									<textarea class="form-control" name="keterangan" id="keterangan"></textarea>
								</div>
							</div>
							<input type="hidden" name="mia_id" id="mia_id">
						</form>
					</div>
					<div class="modal-footer">
						<a href="#" class="btn btn-success waves-effect" onclick="save()"> simpan</a>
						<a href="#" class="btn btn-danger waves-effect" data-dismiss="modal"> cancel</a>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- script Master Intern Alat -->
<script type="text/javascript">
	$(document).ready( function () {
		resetForm();
		getData();
	});

	function resetFilter(){
		$('#alat_filter, #checklist_filter, #periode_filter, #serial_number').val('').trigger('change');
	}

	function filterTableA(column=0,x){
		var table = $('#table_alat').DataTable();
		var value = $(x).val();
		value = '/'+value+'/i';
		table
		    .columns( column )
		    .search( $(x).val() )
            .draw();
	}

	function getData()
	{
		url = "<?php echo base_url('admin/master_intern_alat/dt_alat') ?>";
		$('#table_alat').DataTable({
			"ajax": url,
			"order": [[ 0, "desc" ]],
			"columns": [
				{ "data": "no" },
				{ "data": "nama_alat" },
				{ "data": "serial_number" },
				{ "data": "keterangan" },
				{ "data": "nama_branch" },
				{ "data": "creator" },
				{  "width": "8%", 
				"mRender": function(index, type, data) {
					html = '';
					html += '<a href="#" class="btn notika-btn btn-warning btn-sm" onclick="edit('+data.id+');"><i style="color: #fff; font-size:12px;" class="material-icons">edit</i> </a>';
					html += '<a href="#" class="btn notika-btn btn-danger btn-sm" onclick="hapus('+data.id+');"><i style="color: #fff; font-size:12px;" class="material-icons">delete</i> </a>';
					return html;
				}
			},
			],
			"oLanguage": {
			"sSearch": "Pencarian :",
			"sZeroRecords": "Data tidak ditemukan.",
			"sLengthMenu": "Tampilkan _MENU_ data",
			"sEmptyTable": "Data tidak ditemukan.",
			"sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data.",
			"infoFiltered": "(dari total _MAX_ data)"
			}
		});

		/*set*/
		$("#table_alat").css("font-size", 11);
		$("#table_alat").css("width", '100%');
	}

	function resetForm()
	{
		$('input[name="mia_id"]').val('');
		$('input[name="serial_number"]').val('');
		$('select[name="id_alat"]').val('').trigger('change');
		$('textarea[name="keterangan"]').val('');
		$('.error_field').remove();
	}

	function tambah()
	{
		$("#modal .modal-title").text('Tambah Data');
		resetForm();
		$("#modal").modal('show');
	}

	function reloadData(){
		$('#table_alat').DataTable().destroy();
		$("#modal").modal('hide');
		getData();
		resetForm();
	}

	function edit(id)
	{
		resetForm();
		url = "<?php echo base_url('admin/master_intern_alat/edit_data') ?>";
		$.ajax({
			url:url,
			dataType: "json",
			method: "post",
			data:{
				mia_id:id
			},
			success: function(result) {
				$("#modal .modal-title").text('Edit Data');
				$.each(result.data, function(index, value){
					$("input[name='mia_id']").val(value.id)
					$("input[name='serial_number']").val(value.serial_number)
					$("select[name='id_alat']").val(value.id_alat);
					$("textarea[name='keterangan']").val(value.keterangan);
				})
				$("#modal").modal('show');
			},
			error: function(jqXHR, textStatus, errorThrown) {
			swal({
				title: 'Master Intern Alat',
				text: 'Error',
				type: 'error',
			});
			}
		});
	}

	function save()
	{
		var mia_id = $('input[name="mia_id"]').val();
		var url = "<?php echo base_url('admin/master_intern_alat/save_data') ?>";
		if (mia_id.length>0) {
			url += '/'+1;
		}

		var alat = $('#id_alat').val();
		if (alat.length<1) {
			$('#id_alat').parent().append('<p class="error_field" style="color: #DC143C;">Tidak Boleh Kosong!</p>')
			
			return false;
		}
		$.ajax({
			url: url,
			type: "POST",
			data: $('#form_intern_alat').serialize(),
			success: function(result) {
				swal({
					title: 'Master Intern Alat',
					text: result.message,
					type: 'success',
				}, function() {
					reloadData();
					get_list_alat();
				});
			},
			error: function(jqXHR, textStatus, errorThrown) {
			swal({
				title: 'Master Intern Alat',
				text: 'Error',
				type: 'error',
			});
			}
		});
	};

	function hapus(id)
	{
		var konfirmasi = confirm("Apakah anda yakin ?");
		if (konfirmasi == true) {
			url = "<?php echo base_url('admin/master_intern_alat/hapus_data') ?>";
			$.ajax({
				url:url,
				dataType: "json",
				method: "post",
				data:{
					mia_id:id
				},
				success: function(result) {
					swal({
						title: 'Master Intern Alat',
						text: result.message,
						type: 'success',
					}, function() {
						reloadData();
						get_list_alat();
					});
				},
				error: function(jqXHR, textStatus, errorThrown) {
				swal({
					title: 'Master Intern Alat',
					text: 'Error',
					type: 'error',
				});
				
				}
			});
		}
	}

</script>