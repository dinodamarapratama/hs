<style type="text/css">
    


</style>



<div class="container mt--9">
	
	<div class="table-responsive">
		<table id="table_notification" class="table table-bordered table-striped table-hover dataTable js-exportable stripe row-border order-column nowrap" width="100%" cellspacing="0">
			<thead>
				<tr>
					<th>No</th>
					<th>Tanggal</th>
					<th>Nama</th>
					<th>Notification</th>
				</tr>
			</thead>
		</table>
	</div>

</div>
</div>
</div>
</div>
</div>
</div>
<style type="text/css">
	.select2-container--default .select2-selection--single{
		height: 45px;
	}
	.select2-container--default .select2-selection--single .select2-selection__rendered{
		line-height: 45px;
	}
	.select2-container--default .select2-selection--single .select2-selection__arrow b{
		margin-top: 45%;
	}
</style>
<script type="text/javascript">
	$(document).ready( function () {
		getDataNotif();	

	});
	function getDataNotif(){
		$('#table_notification').DataTable().clear().destroy();
		var url = "<?php echo base_url('admin/notification/load_data') ?>";
		$('#table_notification').DataTable({
			scrollCollapse: true,
			"scrollY": "700px",
			"serverSide": true,
			"processing": true,
			"ajax": {
				"url": url,
				"dataSrc": 'data',
				"type" : "POST",
				//"data" : filter
			},
			"scrollY": 700,
			"scrollX": true,
			//"DisplayLength": "All",
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"scrollCollapse": true,
			"order":[[ 1, "asc" ]],
			"fixedColumns":   {
				//leftColumns: 6,
				//rightColumns: 0
			},
			"columns": [
				{ "data": "no" },
				{  "width": "10%", 
				"mRender": function(index, type, data) {
				html = '';
				html += '<a href="javascript:void(0);" onclick="viewDetailNotif2(this)" data-id="' + data.id + '" data-notif="' + data.dailyreports_id + '" class="waves-effect waves-block">'
				html += '<div class="icon-circle bg-light-green">'
				html += '</div>'
				html += '<div class="menu-info">'
				html += '<p style="font-size:12px">' + data.time + '</p>'
				html += '</div>'
				html += '</a>';
				return html;
				}
				},
				//{'data':"time"},
				{ "width": "8%", 
					"mRender": function(index, type, data) {
						valData = data.name;
						return valData;
					}
				},
				{'data':"description"}
				
				
			],
		
			"oLanguage": {
			"sSearch": "Pencarian :",
			"sZeroRecords": "Data tidak ditemukan.",
			"sLengthMenu": "Tampilkan _MENU_ data",
			"sEmptyTable": "Data tidak ditemukan.",
			"sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data.",
			"infoFiltered": "(dari total _MAX_ data)"
			}
		});
		
		$('#table_karyawan_filter label input').attr('placeholder','Pencarian berdasarkan nama depan!');
	}

	function viewDetailNotif2(node) {
		var currentUrl = "<?= current_url() ?>";
		node = $(node);
		// console.log("node", node);
		var idNotif = node.attr("data-id");
		// console.log("id notif", idNotif);
		var notif = null;
		/* mencari di list notif */
		
		console.log(listNotif);
		url_notif = "<?php echo base_url('admin/notification/get_notification?id=') ?>" + idNotif;
		$.ajax({
			url: url_notif,
			type: "GET",
			dataType: 'json',
			/*data: {
				id: id
			},*/
			success: function(datax) {
				
				console.log(datax[0]);
				
				var nData = null;
				if(datax[0].data != undefined && datax[0].data != null) {
					var nData = datax[0].data;
				}
				/* jika notif tidak memiliki atribut data */
				if(nData == null) {
					console.log('nData is null');
					return;
				}
				var url = null;
				var id = null;
				
				if(nData.dailyreports_id != undefined && nData.dailyreports_id != null) {
					url = "dailyreports";
					id = nData.dailyreports_id;
				} else if(nData.dailyreport_id != undefined && nData.dailyreport_id != null) {
					url = "dailyreports";
					id = nData.dailyreport_id;
				} else if(nData.complaint_id != undefined && nData.complaint_id != null) {
					url = "complaint";
					id = nData.complaint_id;
				} else if(nData.generalreports_id != undefined && nData.generalreports_id != null) {
					url = "generalreports";
					id = nData.generalreports_id;
				} else if(nData.monthlyreports_id != undefined && nData.monthlyreports_id != null) {
					url = "monthlyreports";
					id = nData.monthlyreports_id;
				} else if(nData.techreports_id != undefined && nData.techreports_id != null) {
					url = "techreports";
					id = nData.techreports_id;
				} else if(nData.itreports_id != undefined && nData.itreports_id != null) {
					url = "itreports";
					id = nData.itreports_id;
				//} else if(nData.id_hasil != undefined && nData.id_hasil != null) {
				} 
				else if(nData.doc_id != undefined && nData.doc_id != null) {
					url = "doc";
					id = nData.doc_id;
				}
				else if(nData.qcreport_id != undefined && nData.qcreport_id != null) {
					url = "qc";
					id = nData.qcreport_id;
					document.location = '<?= base_url() ?>admin/' + url + '?notifqc=' + id;
					return;
				}else if(nData.id_qc_input != undefined && nData.id_qc_input != null) {
					url = "qc";
					
					if(nData.id_hasil != undefined && nData.id_hasil != null) {
						id = nData.id_hasil;
						node.attr("id-hasil",id)
					}
					else{
						id = nData.id;
						node.attr("id-hasil",id)
					}
					node.attr("id-qc-input",nData.id_qc_input);
					 ; 
					showQcChart(node);
					
					//console.log('format data notifikasi tidak dikenal', node.attr("id-hasil"), notif);
					
					urlSetReadUser = "<?php echo base_url('admin/qc/set_user_read') ?>";
					$.ajax({
					url: urlSetReadUser,
					type: "POST",
					dataType: 'json',
					data: {
					id: idNotif
					},
					success: function(data) {
						//document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id;

					},
					error: function(jqXHR, textStatus, errorThrown) {
					console.log(jqXHR, textStatus, errorThrown)
					}
					});
					
					return;
				}
				else if(nData.id_cuti != undefined && nData.id_cuti != null) {
					url = "reqcuti";
					id = nData.id_cuti+'_'+idNotif;					 
				}
				else if(nData.id_izin != undefined && nData.id_izin != null) {
					url = "reqizin";
					id = nData.id_izin+'_'+idNotif;					 
				}
				else if(nData.event_type != undefined && nData.event_type != null) {
					url = "announcement";
					id = nData.id;
					
					/*urlSetReadUser = "<?php echo base_url('admin/announcement/update_notification') ?>";
					$.ajax({
					url: urlSetReadUser,
					type: "POST",
					dataType: 'json',
					data: {
					id: id
					},
					success: function(data) {
						//document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id;

					},
					error: function(jqXHR, textStatus, errorThrown) {
					console.log(jqXHR, textStatus, errorThrown)
					}
					});*/
					
				}
				
				if(url == null) {
					console.log('format data notifikasi tidak dikenal', nData, notif);
					return;
				}
						
				if(currentUrl.indexOf(url) >= 0) {
					console.log('trigger click', url, id);
					$("#view_" + id).click();
					var tr = $('#view_' + id);
					if(tr.length > 0) {
						viewData(tr);
					} else {
						viewData(null, id);
					}
					return;
				}
				console.log('redirect to page', url);
				document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id;
				
				
				
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR, textStatus, errorThrown)
			}
		});
		
		
	}

</script>




<!-- start modal chart -->

    <div class="modal fade" id="modal_chartjs" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 90vw">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Chart</h4>
                    <br>
                    <h5 id="test-name"></h5>
                    <h5 id="lot-number"></h5>
                    <h5 id="branch-name"></h5>
                </div>
                <div class="modal-body" style="width:90%;margin-left: 25px;padding: 0px !important;">
                    
                </div>
                <div class="modal-footer">
                    <!-- <input class="btn btn-primary btn-sm" type="button" onclick="saveHasil(this)" value="save" id="btn_save"> -->
                    <!-- <input class="btn btn-danger btn-sm" type="button" data-dismiss="modal" value="Batal"> -->
                    <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- end modal chart -->

</section>


<script type="text/javascript">
    var base_url = "<?= base_url() ?>";
</script>

<script src="<?php echo base_url() ?>assets/js/qc_chart.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/chartjs/Chart.js"></script>

<style type="text/css">
    .table_chart_info tr td {
        padding: 5px;
    }
    .canvas1{
        /*background: linear-gradient(to bottom, blue 50px, yellow 50px, green, green, yellow, blue);*/
        background: linear-gradient(to bottom, red, yellow , lightgreen , lightgreen , yellow , red);
        background-repeat: no-repeat;
        background-position-y: 33px;
        background-position-x: 33px;
        background-size: auto 170px;
    }
    .dt-container::-webkit-scrollbar {
        height: 5px
    }
    .dt-container::-webkit-scrollbar-track {
        border-radius: 10px;
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    }
    .dt-container::-webkit-scrollbar-thumb {
        border-radius: 10px;
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5);
    }
</style>
