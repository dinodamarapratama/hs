<?php
	$getSessionUser = $this->session->userdata('id_user');
	$getUser = $this->db->get_where('users',['id_user' => $getSessionUser]);
	$user = $getUser->row();
	if ($user->last_login !== $this->session->userdata('last_login')){
		$this->session->sess_destroy();
		redirect('/welcome/?pesan=loginditempatlain', 'refresh');
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
	<meta http-equiv='cache-control' content='no-cache'> 
	<meta http-equiv='expires' content='0'> 
	<meta http-equiv='pragma' content='no-cache'> 
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="Description" content="">
	<meta name="Author" content="">
	<meta name="Keywords" content=""/>

	<!-- Title -->
	<title>MIS APP</title>
	<link rel="manifest" href="/manifest.json" />


	<!-- OLD -->
	<link href="<?php echo base_url() ?>assets/css/material-icon-font.css" rel="stylesheet">
	
	<link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/font-awesome.min.css">
	<!-- owl.carousel CSS
	============================================ -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/owl.theme.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/owl.transitions.css">
	<!-- meanmenu CSS
	============================================ -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/meanmenu/meanmenu.min.css">
	<!-- animate CSS
	============================================ -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/animate.css">
	<!-- normalize CSS
	============================================ -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/normalize.css">
	<!-- mCustomScrollbar CSS
	============================================ -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/scrollbar/jquery.mCustomScrollbar.min.css">
	<!-- jvectormap CSS
	============================================ -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/jvectormap/jquery-jvectormap-2.0.3.css">
	<!-- notika icon CSS
	============================================ -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/notika-custom-icon.css">
	<!-- wave CSS
	============================================ -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/wave/waves.min.css">
	<!-- main CSS
	============================================ -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/main.css">
	<!-- style CSS
	============================================ -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/admin/style.css">
	<!-- responsive CSS
	============================================ -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/responsive.css">
	<!-- modernizr JS
	============================================ -->
	<!-- Data Table JS
	============================================ -->
	<!--<link rel="stylesheet" href="<?php //echo base_url()?>assets/admin/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="<?php //echo base_url()?>assets/admin/css/fixedColumns.dataTables.min.css">-->

	<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.3.0/css/fixedColumns.dataTables.min.css">-->
	<!-- TOAST Calendar UI
	============================================ -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/admin/css/tui-calendar.css" />
	<!--<link rel="stylesheet" type="text/css" href="https://uicdn.toast.com/tui-calendar/latest/tui-calendar.css" />-->

	<link href="<?php echo base_url() ?>assets/admin/css/roboto-font.css" rel="stylesheet">
	<!-- Waves Effect Css -->
	<link href="<?php echo base_url() ?>assets/admin/plugins/node-waves/waves.css" rel="stylesheet" />
	<!-- Animation Css -->
	<link href="<?php echo base_url() ?>assets/admin/plugins/animate-css/animate.css" rel="stylesheet" />
	<!-- Multi Select Css -->
	<link href="<?php echo base_url() ?>assets/admin/plugins/multi-select/css/multi-select.css" rel="stylesheet">
	<!-- Bootstrap Select Css -->
	<link href="<?php echo base_url() ?>assets/admin/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
	
	<!-- Colorpicker Css -->
	<link href="<?php echo base_url() ?>assets/admin/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" />
	<!--Jquery UI-->
	<link href="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui/css/jquery-ui.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui/css/jquery-ui-autocomplete.min.css" rel="stylesheet">
	<!-- Dropzone Css -->
	<link href="<?php echo base_url() ?>assets/admin/plugins/dropzone/dropzone.css" rel="stylesheet">
	<!-- Multi Select Css -->
	<link href="<?php echo base_url() ?>assets/admin/plugins/multi-select/css/multi-select.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/admin/plugins/sweetalert/sweetalert.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/admin/plugins/select2/select2.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/css/wave/button.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/admin/css/chosen/chosen.css"/>

	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/css/bootstrap-datetimepicker.min.css"/>
	<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"/>-->
	<!-- OLD -->


	<!-- Start New Assets -->
	<!--- Favicon --->
	<link rel="icon" href="<?php echo base_url()?>assets/login/img/logo-biomedika.ico" type="image/x-icon"/>

	<!--- Icons css --->
	<link href="<?php echo base_url()?>assets/biomedika_theme/admin/css/icons.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/css/material-icon-font.css" rel="stylesheet">
	<!-- Owl-carousel css-->
	<link href="<?php echo base_url()?>assets/biomedika_theme/admin/plugins/owl-carousel/owl.carousel.css" rel="stylesheet"/>

	<!--- Right-sidemenu css --->
	<link href="<?php echo base_url()?>assets/biomedika_theme/admin/plugins/sidebar/sidebar.css" rel="stylesheet">

	<!--- Style css --->
	<link href="<?php echo base_url()?>assets/biomedika_theme/admin/css/style.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/biomedika_theme/admin/css/skin-modes.css" rel="stylesheet">

	<!--- Animations css --->
	<link href="<?php echo base_url()?>assets/biomedika_theme/admin/css/animate.css" rel="stylesheet">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" />
	<link href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap5.min.css" rel="stylesheet">

	<?php flush(); ?>

	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />

<!-- custom -->
<style type="text/css">
	.table_chart_info tr td {
		padding: 5px;
	}
	.canvas1{
		background: linear-gradient(to bottom, red, yellow , lightgreen , lightgreen , yellow , red);
		background-repeat: no-repeat;
		background-position-y: 33px;
		background-position-x: 33px;
		background-size: auto 170px;
	}
	.dt-container::-webkit-scrollbar {
		height: 5px
	}
	.dt-container::-webkit-scrollbar-track {
		border-radius: 10px;
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	}
	.dt-container::-webkit-scrollbar-thumb {
		border-radius: 10px;
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5);
	}
	
	.horizontalMenu > .horizontalMenu-list > li > a {
		padding: 3px 3px;
	}
	.horizontalMenu {
		font-size: 10px;
	}
	.main-header-right,.main-header-left{
		width: 50%;
	}
	.tab-content{
		width: 75%;
	}
	.pull-left {
		margin-left: 118%;
	}
	.custom-control {
		margin-left: 10px;
	}
	#view {
		overflow-x: hidden !important;
		overflow-y: auto !important;
	}
	.fade.in {
		opacity: 1 !important;
	}
	h5, .h5 {
		font-size: 0.89375rem;
	}
	#container_all{
		/* padding-top: 30px; */
	}
	table.dataTable.no-footer {
		 border-bottom: 0px; 
	}
	table.dataTable {
		 margin-top: 0px !important; 
		 margin-bottom: 0px !important; 
	}
	div.dataTables_wrapper div.dataTables_filter input {
		width: 100%;
	}
	.nav-pills .nav-link.active {
		background-color: #0ba360;
	}
	.ntd-ctn {
		top: -6px;
		left: 25px;
	}
</style>
<!-- End Assets -->


<!--- JQuery min js --->
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <script
  src="https://code.jquery.com/jquery-migrate-3.3.2.min.js"
  integrity="sha256-Ap4KLoCf1rXb52q+i3p0k2vjBsmownyBTE1EqlRiMwA="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/jquery-easyui/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/jquery-easyui/jquery.easyui.min.js"></script>
<!--<script type="text/javascript" src="http://www.jeasyui.com/easyui/plugins/jquery.messager.js"></script>-->
<script type="text/javascript" src="<?php echo base_url();?>assets/jquery-easyui/plugins/jquery.messager.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/jquery-easyui/datagrid-filter.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery-easyui/jquery-easyui-texteditor/jquery.texteditor.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/jquery-easyui/jquery.media.js"></script>


<script type="text/javascript">
var $eui = $.noConflict(true);
</script>

<style>
#loading{
position: fixed;
width: 100%;
height: 100vh;
background: #fff url('<?php echo base_url();?>assets/img/ajax-loader1.gif') no-repeat center center;
z-index: 9999;
}
.navbar-brand {
padding: 0px; 
padding-left: 0px;
}
ul.notika-menu-wrap li a {
color: black;
}
.navbar-nav>li>a {
color: black;
}
.ntd-ctn {
background: red;
color: white;
}
.nav>li>a:focus, .nav>li>a:hover {
background-color: white;
}
.nav-tabs.notika-menu-wrap {
padding-left: 23%;
}
.nav.navbar-nav.notika-top-nav {
padding-right: 150px; 
}
</style>


<style type="text/css">
@keyframes ldio-anotherLoader {
0% { transform: rotate(0deg) }
50% { transform: rotate(180deg) }
100% { transform: rotate(360deg) }
}
.ldio-anotherLoader div {
position: absolute;
animation: ldio-anotherLoader 1s linear infinite;
width: 132px;
height: 132px;
top: 34px;
left: 34px;
border-radius: 50%;
box-shadow: 0 1px 0 0 #00a850;
transform-origin: 66px 66.5px;
}
.splash_loader {
width: 200px;
height: 200px;
display: inline-block;
overflow: hidden;
background: transparent;
}
.ldio-anotherLoader {
width: 100%;
height: 100%;
position: relative;
transform: translateZ(0) scale(1);
backface-visibility: hidden;
transform-origin: 0 0; /* see note above */
}
.ldio-anotherLoader div { box-sizing: content-box; }
/* generated by https://loading.io/ */
</style>

 <style type="text/css">
        .float{
        position:fixed;
        width:60px;
        height:60px;
        bottom:100px;
        right:15px;
        background-color:#ececec;
        color:#FFF;
        border-radius:50px;
        text-align:center;
      font-size:30px;
        box-shadow: 2px 2px 3px #999;
      z-index:100;
    }
    
    .float2{
        position:fixed;
        width:60px;
        height:60px;
        bottom:170px;
        right:15px;
        background-color:#ececec;
        color:#FFF;
        border-radius:50px;
        text-align:center;
      font-size:30px;
        box-shadow: 2px 2px 3px #999;
      z-index:100;
    }
    
    .float3{
        position:fixed;
        width:60px;
        height:60px;
        bottom:240px;
        right:15px;
        background-color:#ececec;
        color:#FFF;
        border-radius:50px;
        text-align:center;
      font-size:30px;
        box-shadow: 2px 2px 3px #999;
      z-index:100;
    }

    .my-float{
        margin-top:16px;
    }
    
    .my-float2{
        margin-top:16px;
    }

    .select2-container--open{
		z-index:99999;
	}
	
	#ui-datepicker-div{
		z-index:99999 !important;
	}
    </style>

</head>

<body class="main-body  app">

<!-- Loader -->
<!--<div id="global-loader">
<img src="<?php echo base_url()?>assets/biomedika_theme/admin/img/loaders/loader-4.svg" data-src="<?php echo base_url()?>assets/biomedika_theme/admin/img/loaders/loader-4.svg" class="loader-img b-lazy" alt="Loader">
</div>-->
<!-- Loader -->

<!-- main-header opened -->
<div class="main-header nav nav-item hor-header">
<div class="container">
	<div class="main-header-left ">
		<a class="animated-arrow hor-toggle horizontal-navtoggle"><span></span></a><!-- sidebar-toggle-->
		<a class="header-brand" href="<?=base_url()?>">
			<img data-src="<?php echo base_url();?>assets/images/logo.jpg" class="b-lazy logo-white" width="130">
			<img data-src="<?php echo base_url();?>assets/images/logo.jpg"  class="b-lazy logo-default" width="130">
			<img data-src="<?php echo base_url()?>assets/login/img/logo-biomedika.ico" class="icon-white b-lazy">
			<img data-src="<?php echo base_url()?>assets/login/img/logo-biomedika.ico" class="icon-default b-lazy">
		</a>
		
	</div><!-- search -->
	<div class="main-header-right">
			<div class="nav nav-item  navbar-nav-right ml-auto">
				<div class="nav-item full-screen fullscreen-button">
					<a class="new nav-link full-screen-link" href="#"><i class="fe fe-maximize"></i></span></a>
				</div>
				
				<!-- <div class="dropdown  nav-item main-header-message ">
					<a class="new nav-link" href="#" ><i class="fe fe-mail"></i>
						<span class=" pulse-danger"></span>
						<div class="ntd-ctn">
							<span class="label-count"><span id="count_message">0</span></span>
						</div>
					</a>
					<div class="dropdown-menu">
						<div class="menu-header-content bg-primary-gradient text-left d-flex">
							<div class="">
								<h6 class="menu-header-title text-white mb-0 message-new">0 new My Annoucement</h6>
							</div>
							<div class="my-auto ml-auto">
								<a class="badge badge-pill badge-warning float-right" href="#" onclick="read_all_message()">Mark All Read</a>
							</div>
						</div>
						<div class="main-message-list chat-scroll" id="message_cont">
							
						</div>
						<div class="text-center dropdown-footer">
							<a href="<?php echo base_url().'admin/notification';?>">VIEW ALL</a>
						</div>
					</div>
				</div> -->
				<div class="dropdown nav-item main-header-message">
					<a class="new nav-link" href="#"><i class="fe fe-bell"></i>
						<span class=" pulse-danger"></span>
						<div class="ntd-ctn">
							<span class="label-count"><span id="count_notification">0</span></span>
						</div>
					</a>
					
					<div class="dropdown-menu">
						<div class="menu-header-content bg-primary-gradient text-left d-flex">
							<div class="">
								<h6 class="menu-header-title text-white mb-0 notification-new">0 new My Reports</h6>
							</div>
							<div class="my-auto ml-auto">
								<a class="badge badge-pill badge-warning float-right" href="#" onclick="read_all_notif()">Mark All Read</a>
							</div>
						</div>
						<div class="main-message-list Notification-scroll" style="max-height:320px; width:300px; overflow:scroll;" id="notif_cont">
							<a class="d-flex p-3 border-bottom" href="#">
							
						</div>
						<div class="text-center dropdown-footer">
							<a href="<?php echo base_url().'admin/notification';?>">VIEW ALL</a>
						</div>
					</div>
				</div>
				<!-- <div class="dropdown nav-item main-header-message">
					<a class="new nav-link" href="#"><i class="fe fe-flag"></i>
						<span class=" pulse"></span>
						<div class="ntd-ctn">
							<span class="label-count"><span id="count_reminder">0</span></span>
						</div>
					</a>
					
					<div class="dropdown-menu">
						<div class="menu-header-content bg-primary-gradient text-left d-flex">
							<div class="">
								<h6 class="menu-header-title text-white mb-0 reminder-new">0 new Reminders</h6>
							</div>
							<div class="my-auto ml-auto">
								<a class="badge badge-pill badge-warning float-right" href="#" onclick="read_all_reminder()">Mark All Read</a>
							</div>
						</div>
						<div class="main-message-list Notification-scroll" id="reminder_cont" style="overflow: scroll;">
							
						</div>
						<div class="dropdown-footer">
							<a href="#">VIEW ALL</a>
						</div>
					</div>
				</div> -->
				<!-- <div class="dropdown nav-item main-header-message">
					<a class="new nav-link" href="#"><i class="fe fe-user"></i>
						<span class=" pulse"></span>
						<div class="ntd-ctn">
							<span class="label-count"><span id="count_notif_cuti">0</span></span>
						</div>
					</a>
					
					<div class="dropdown-menu">
						<div class="menu-header-content bg-primary-gradient text-left d-flex">
							<div class="">
								<h6 class="menu-header-title text-white mb-0 cuti-new">0 new Notifications</h6>
							</div>
							<div class="my-auto ml-auto">
								<a class="badge badge-pill badge-warning float-right" href="#" onclick="read_all_cuti()">Mark All Read</a>
							</div>
						</div>
						<div class="main-message-list Notification-scroll" id="cuti_cont" style="overflow: scroll;">
							
						</div>
						<div class="dropdown-footer">
							<a href="<?php //echo base_url().'admin/notification';?>">VIEW ALL</a>
						</div>
					</div>
				</div> -->
				<!-- <div class="dropdown nav-item main-header-message">
					<a class="new nav-link" href="#"><i class="fe fe-file-text"></i>
						<span class=" pulse"></span>
						<div class="ntd-ctn">
							<span class="label-count"><span id="count_notif_doc">0</span></span>
						</div>
					</a>
					
					<div class="dropdown-menu">
						<div class="menu-header-content bg-primary-gradient text-left d-flex">
							<div class="">
								<h6 class="menu-header-title text-white mb-0 doc-new">0 new Notifications</h6>
							</div>
							<div class="my-auto ml-auto">
								<a class="badge badge-pill badge-warning float-right" href="#" onclick="read_all_doc()">Mark All Read</a>
							</div>
						</div>
						<div class="main-message-list Notification-scroll" id="doc_cont" style="overflow: scroll;">
							
						</div>
						<div class="dropdown-footer">
							<a href="<?php //echo base_url().'admin/notification';?>">VIEW ALL</a>
						</div>
					</div>
				</div> -->
				
				<div class="dropdown main-profile-menu nav nav-item nav-link">
					<a class="profile-user d-flex" href=""><img data-src="<?php echo base_url().'uploads/avatar/'.$this->session->userdata('image'); ?>" alt="user-img" class="rounded-circle mCS_img_loaded b-lazy"><span></span></a>
					<div class="dropdown-menu">
						<div class="main-header-profile header-img">
							<div class="main-img-user"><img alt="" data-src="<?php echo base_url().'uploads/avatar/'.$this->session->userdata('image'); ?>" class="b-lazy"></div>
							<center><h6><?php echo $this->session->userdata('name');?> <?php echo $this->session->userdata('last_name');?></h6><span><?php echo $this->session->userdata('position_name');?> - <?php echo $this->session->userdata('bagian_name');?> (<?php echo $this->session->userdata('branch_name');?>)</span></center>
						</div>
						<!-- <a class="dropdown-item" href="#" onclick="loadPage('<?php echo base_url()?>admin/profile');"><i class="far fa-user"></i> My Profile</a>
						<a class="dropdown-item" href="#" onclick="loadPage('<?php echo base_url()?>admin/notification');"><i class="far fa-bell"></i> My Messages</a>
						<a class="dropdown-item" href="#" onclick="loadPage('<?php echo base_url()?>admin/reqcuti');"><i class="fa fa-users"></i> Permohonan Cuti</a>
						<a class="dropdown-item" href="#" onclick="loadPage('<?php echo base_url()?>admin/reqizin');"><i class="fas fa-user-injured"></i> Permohonan Izin</a>
						<a class="dropdown-item" href="#" onclick="loadPage('<?php echo base_url()?>admin/overtime');"><i class="fas fa-file-text"></i> SP Lembur</a>
						<a class="dropdown-item" href="#" onclick="loadPage('<?php echo base_url()?>admin/pindah_cabang');"><i class="fa fa-location-arrow"></i> Pindah Cabang</a> -->
						<a class="dropdown-item" href="#" onclick="loadPage('<?php echo base_url()?>admin/dashboard/logout');"><i class="fas fa-sign-out-alt"></i> Log Out</a>
					</div>
				</div>
				<div class="dropdown main-header-message right-toggle">
					<a class="nav-link pr-0" data-toggle="sidebar-right" data-target=".sidebar-right">
						<i class="ion ion-md-menu tx-20 bg-transparent"></i>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- main-header closed -->

<!--Horizontal-main -->
<div class="sticky">
<div class="horizontal-main hor-menu clearfix side-header">
	<div class="horizontal-mainwrapper container clearfix">
		<!--Nav-->
		<nav class="horizontalMenu clearfix">
			
			<?php 
			if(isset($sidebar_menu) ){
			echo $sidebar_menu;
			}
			else{
			require('static_menu.php'); 
			}

			?>

		
			<!-- <li aria-haspopup="true">
			<a href="https://linktr.ee/ISO27K" target="_blank">
			<i class="las la-sign-in-alt"></i> Form IT 
			</a>
			</li> -->
		
		</nav>
		<!--Nav-->
	</div>
</div>
</div>
<!--Horizontal-main -->

<div class="main-content horizontal-content">

<!-- container opened -->
<!-- <div class="container"> -->
	<div id="container_all">
