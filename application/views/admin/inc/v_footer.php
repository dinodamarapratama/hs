  
		</div>
	<!--</div> -->
	<!-- /container -->
</div>
<!-- /main-content -->

<!--Sidebar-right-->
<div class="sidebar sidebar-right sidebar-animate">
	<div class="panel panel-primary card mb-0">
		<div class="panel-body tabs-menu-body p-0 border-0">
			<ul class="Date-time">
				<li class="time">
					<h1 class="animated "></h1>
					<p class="animated "></p>
				</li>
			</ul>
			</div>
	</div>
</div>
<!--/Sidebar-right-->


<!-- Footer opened -->
		<div class="main-footer ht-40" style="z-index: 99;">
			<div class="container-fluid pd-t-0-f ht-100p">
				<span>Version 101.0.0 - Copyright © 2021 Laboratorium Klinik Bio Medika. All rights reserved.</span>
			</div>
		</div>
		<!-- Footer closed -->
		

		<!--- Back-to-top --->
		<a href="#top" id="back-to-top"><i class="las la-angle-double-up"></i></a>
		
		
		<!--- Datepicker js --->
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/plugins/jquery-ui/ui/widgets/datepicker.js"></script>

		<!--- Bootstrap Bundle js --->
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

		<!--- Ionicons js --->
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/plugins/ionicons/ionicons.js"></script>

		<!--- Chart bundle min js --->
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/plugins/chart.js/Chart.bundle.min.js"></script>
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/plugins/chart.js/excanvas.js"></script>
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/plugins/chart.js/utils.js"></script>

		<!--- Index js --->
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/js/index.js"></script>

		<!--- JQuery sparkline js --->
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/plugins/jquery-sparkline/jquery.sparkline.min.js"></script>

		<!--- Internal Sampledata js --->
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/js/chart.flot.sampledata.js"></script>

		<!--- Rating js --->
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/plugins/rating/jquery.rating-stars.js"></script>
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/plugins/rating/jquery.barrating.js"></script>

		<!--- Horizontalmenu js --->
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/plugins/horizontal-menu/horizontal-menu.js"></script>

		<!--- Eva-icons js --->
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/js/eva-icons.min.js"></script>

		<!--- Moment js --->
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/plugins/moment/moment.js"></script>

		<!--- Perfect-scrollbar js --->
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/plugins/perfect-scrollbar/p-scroll.js"></script>

		<!--- Sticky js --->
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/js/sticky.js"></script>

		<!--- Right-sidebar js --->
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/plugins/sidebar/sidebar.js"></script>
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/plugins/sidebar/sidebar-custom.js"></script>

		<!--- Scripts js --->
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/js/script.js"></script>

		<!--- Custom js --->
		<script src="<?php echo base_url()?>assets/biomedika_theme/admin/js/custom.js"></script>
		<!-- End Footer area-->
		<!-- jquery ============================================ -->
		<!-- bootstrap JS ============================================ -->
		<script src="<?php echo base_url()?>assets/admin/js/bootstrap.min.js"></script>
		<!-- wow JS ============================================ -->
		<script src="<?php echo base_url()?>assets/admin/js/wow.min.js"></script>
		<!-- price-slider JS ============================================ -->
		<script src="<?php echo base_url()?>assets/admin/js/jquery-price-slider.js"></script>
		<!-- owl.carousel JS ============================================ -->
		<script src="<?php echo base_url()?>assets/admin/js/owl.carousel.min.js"></script>
		<!-- scrollUp JS ============================================ -->
		<script src="<?php echo base_url()?>assets/admin/js/jquery.scrollUp.min.js"></script>
		<!-- meanmenu JS ============================================ -->
		<script src="<?php echo base_url()?>assets/admin/js/meanmenu/jquery.meanmenu.js"></script>
		<script src="<?php echo base_url()?>assets/admin/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
		<!-- sparkline JS ============================================ -->
		<script src="<?php echo base_url()?>assets/admin/js/sparkline/jquery.sparkline.min.js"></script>
		<script src="<?php echo base_url()?>assets/admin/js/sparkline/sparkline-active.js"></script>
		<!-- sparkline JS ============================================ -->
		<script src="<?php echo base_url()?>assets/admin/js/flot/jquery.flot.js"></script>
		<script src="<?php echo base_url()?>assets/admin/js/flot/jquery.flot.resize.js"></script>
		<script src="<?php echo base_url()?>assets/admin/js/flot/curvedLines.js"></script>
		<script src="<?php echo base_url()?>assets/admin/js/flot/flot-active.js"></script>
		<!-- knob JS ============================================ -->
		<script src="<?php echo base_url()?>assets/admin/js/knob/jquery.knob.js"></script>
		<script src="<?php echo base_url()?>assets/admin/js/knob/jquery.appear.js"></script>
		<script src="<?php echo base_url()?>assets/admin/js/knob/knob-active.js"></script>
		<!--  wave JS ============================================ -->
		<script src="<?php echo base_url()?>assets/admin/js/wave/waves.min.js"></script>
		<script src="<?php echo base_url()?>assets/admin/js/wave/wave-active.js"></script>
		<!--  todo JS ============================================ -->
		<script src="<?php echo base_url()?>assets/admin/js/todo/jquery.todo.js"></script>
		<!-- plugins JS ============================================ -->
		<script src="<?php echo base_url()?>assets/admin/js/plugins.js"></script>
		<!--  Chat JS ============================================ -->
		<script src="<?php echo base_url()?>assets/admin/js/chat/moment.min.js"></script>
		<script src="<?php echo base_url()?>assets/admin/js/chat/jquery.chat.js"></script>
		<script src='<?= base_url() ?>assets/admin/js/icheck/icheck.min.js'></script>
		<script src='<?= base_url() ?>assets/admin/js/icheck/icheck-active.js'></script>
		<script src="<?= base_url() ?>assets/admin/js/tui-code-snippet.js"></script>
		<script src="<?= base_url() ?>assets/admin/js/tui-dom.js"></script>
		<script src="<?= base_url() ?>assets/admin/js/tui-time-picker.min.js"></script>
		<script src="<?= base_url() ?>assets/admin/js/tui-date-picker.min.js"></script>
		<script src="<?= base_url() ?>assets/admin/js/tui-calendar.js"></script>
		<!-- main JS
		============================================ -->
		<script src="<?php echo base_url()?>assets/admin/js/main.js"></script>
		<!-- MIS OLD-->
		<script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui/js/jquery-ui.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui/js/jquery-ui-autocomplete.min.js"></script>
		<!-- Custom Js -->
		<script src="<?php echo base_url() ?>assets/admin/js/admin.js"></script>
		<!-- Jquery DataTable Plugin Js -->
		<!-- Data Table JS ============================================ -->
		<script src="<?= base_url() ?>assets/admin/js/data-table/jquery.dataTables.min.js"></script>
		<script src="<?= base_url() ?>assets/admin/js/data-table/dataTables.fixedColumns.min.js"></script>
		<script src="<?= base_url() ?>assets/admin/js/data-table/dataTables.editor.min.js"></script>
		<script src="<?= base_url() ?>assets/admin/js/data-table/moment.min.js"></script>
		<script src="<?= base_url() ?>assets/admin/js/data-table/datetime-moment.js"></script>
		<!-- <script src="<?php echo base_url() ?>assets/admin/js/data-table/data-table-act.js"></script> -->
		<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
		<script src="<?php echo base_url() ?>assets/admin/plugins/sweetalert/sweetalert.min.js"></script>
		<script type="text/javascript" src="<?= base_url() ?>assets/admin/plugins/select2/select2.min.js"></script>
		<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/blazy/1.8.2/blazy.min.js" integrity="sha512-Yrd3VqXNBUzyCQWVBlL65mTdE1snypc9E3XnGJba0zJmxweyJAqDNp6XSARxxAO6hWdwMpKQOIGE5uvGdG0+Yw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/UpUp/1.1.0/upup.min.js" integrity="sha512-gvjSoow5DPFvXsUlYkus34t20Gjs/4Cyl0HB2OJg5m3D2TgqETOGO9fAFq6fD58H6VgcxgbO3Yxty42zlVr1HA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
		
		<script type="text/javascript">
		var bLazy = new Blazy({
        // Options
   		 });

		var OneSignal = window.OneSignal || [];
		OneSignal.push(function() {
			if(!OneSignal.isPushNotificationsSupported()) {
				// return;
				console.log('push notif not supported');
			} else {
				console.log('push notif is supported');
			}
			OneSignal.init({
				// appId: "1d187d53-37dd-4631-a552-3ef3e9ec0d3f", // rahmat
				appId: "0116bef6-e210-4035-8956-387256c63ce6", // dino
				allowLocalhostAsSecureOrigin: true,
				// autoResubscribe: true,
				// requiresUserPrivacyConsent: true,
				welcomeNotification: {
					"title": "Selamat Bergabung dan berlangganan notifikasi",
					"message": "Welcome To MIS !",
					// "url": "" /* Leave commented for the notification to not open a window on Chrome and Firefox (on Safari, it opens to your webpage) */
				}
			});
			OneSignal.showNativePrompt();
			OneSignal.isPushNotificationsEnabled(function(isEnabled) {
				console.log('notif enabled?', isEnabled);
				if(!isEnabled) {
					window.OneSignal.showSlidedownPrompt();
				} else {
					console.log('notif is already enabled');
					registerPlayerId();
				}
			});
			OneSignal.on("subscriptionChange", function(isSubscribed) {
				/* If the user's subscription state changes during the page's session, update the button text */
				// updateMangeWebPushSubscriptionButton(buttonSelector);
				console.log('subscription change', isSubscribed);
			});
			var registerPlayerId = function() {
				OneSignal.getUserId(function(id) {
					$.ajax({
						url: '<?= base_url() ?>admin/onesignal/save',
						method: 'post',
						data: {
							id: id
						},
						success: function(resp) {
							console.log('store onesingal id response', resp);
						},
						error: function() {
							console.log('store onesignal error');
						}
					});
				});
			}
		});

		function loadPage(href='',target=''){
			var no_reload_menu = [
				/*'complaint','generalreports','dailyreports','itreports',
				'homeservice','monthlyreports','maintainext','intern_checklist','limbah','booking','profile','reqizin','reqcuti','notification','report_daily','report_complaint','master_users','doc','rooms','master_qc','master_menu','master_inventaris','master_bm','master_users','pemeriksaan','hr','ijin','logistik','techreports','qc','dashboard','announcement','drugs','mcu','clockin','suhu','overtime'*/
			];
			
			window.history.pushState(null, 'MIS APP', href+'?'+target);
			var nav = 'reload';
			$.each(no_reload_menu,function(i,v){
				if (href.includes(v)) {
					nav = 'no_reload';
				}
			});

			if (nav == 'no_reload' && !href.includes("logout")) {
				$('#container_all').load(href+'/index/js?'+target);
			}else{
				location.href = href;
			}

			/*set local storage for url history*/
			if (!href.includes('logout')) {
				var url_history = href;
				sessionStorage.history = url_history;
			}
			/**/
		}

		$(document).ready(function(){



			/*catch on reloaded page*/
			/*set local storage for url history*/
			var url_history = '<?= current_url() ?>';
			var params = '<?= $_SERVER['QUERY_STRING'] ?>';
			var fullUrl = url_history;
			if (params.length > 0) {
				fullUrl += '?'+params;
			}
			
			if (!url_history.includes('logout')) {
				sessionStorage.history = fullUrl;
			}
			/**/

			//fix bug double bootstrap load
			$('.nav-tabs .nav-link').click(function(e) {
				$(".nav-tabs .nav-link").removeClass('active');
			});
			
			// console.log('href : ',window.location.href);
			console.log('init js');
			$('.horizontalMenu-list li a, .sub-menu li a').click(function(e) {
				
				var flag = $(this).attr("data-toggle");
				if (flag !== 'tab') {
					$('.horizontalMenu-list li, .sub-menu li').removeClass('active');
					$(this).parent().addClass('active');	
					
					if ($(this).parents('div').prop('id') == 'navbarSupportedContent') {
						$('.custom-menu-content div').removeClass('active');	
					}
				}else{
					$('.custom-menu-content div').removeClass('active');
				}

				var no_reload_menu = [
					/*'complaint','generalreports','dailyreports','itreports',
					'homeservice','monthlyreports','maintainext','intern_checklist','limbah','booking','profile','reqizin','reqcuti','notification','report_daily','report_complaint','master_users','doc','rooms','master_qc','master_menu','master_inventaris','master_bm','master_users','pemeriksaan','hr','ijin','logistik','techreports','qc','dashboard','announcement','drugs','mcu','clockin','suhu','overtime'*/
				];
				e.preventDefault();
				var href = $(this).attr('href');
				if(href.includes("http")){
					//console.log(href);
					window.history.pushState(null, 'MIS APP', href);
					var nav = 'reload';
					$.each(no_reload_menu,function(i,v){
						if (href.includes(v)) {
							nav = 'no_reload';
						}
					});

					if (nav == 'no_reload' && !href.includes("logout")) {
						$('#container_all').load(href+'/index/js');
					}else{
						location.href = href;
					}

					/*set local storage for url history*/
					if (!href.includes('logout')) {
						var url_history = href;
						sessionStorage.history = url_history;
					}
					/**/
				}
				
			});


			

			
			$(document).on('click','#compose',function(e){
			//$('#compose').click(function(e) {
				$('select.search_user').select2({
				  ajax: {
					method: 'post',
					url: '<?= base_url() ?>admin/master_data/many_user',
					processResults: function(resp) {
						var res = JSON.parse(resp);
						var items = [];

						for(var i = 0, i2 = res.length; i < i2; i++) {
							items.push({
								id: res[i].id_user,
								text: res[i].name + ' '  +res[i].last_name
							});
						}
						return {
							results: items
						};
					}
				  }
				});	
				
				
				$('#add').on('hidden.bs.modal', function() {
					user = $("input[name='search_user']").val();
					if(user !== '') {
						save(true);
					}
				});
				
			});
			
			$(document).on('click','.nav-tabs .nav-link',function(e){
			//$('.nav-tabs .nav-link').click(function(e) {
				$(".nav-tabs .nav-link").removeClass('active');
			});
			
			
			
			
			/*var selectUser = $('select.search_user');
			var modal = selectUser.closest('div#add');
			$('select.search_user').select2({
				dropdownParent: modal,
				ajax: {
					method: 'post',
					url: '<?= base_url() ?>admin/master_data/many_user',
					processResults: function(resp) {
						console.log('receiver resp', resp);
						var res = JSON.parse(resp);
						var items = [];
						for(var i = 0, i2 = res.length; i < i2; i++) {
							items.push({
								id: res[i].id_user,
								text: res[i].name
							});
						}
						return {
							results: items
						};
					}
				}
			});*/

			$('select.search_user').select2({
			  ajax: {
				method: 'post',
				url: '<?= base_url() ?>admin/master_data/many_user',
				processResults: function(resp) {
					// console.log('receiver resp', resp);
					var res = JSON.parse(resp);
					var items = [];

					for(var i = 0, i2 = res.length; i < i2; i++) {
						items.push({
							id: res[i].id_user,
							text: res[i].name + ' '  +res[i].last_name
						});
					}
					return {
						results: items
					};
				}
			  }
			});
			
			// $('select.search_user').on("select2:selecting", function(e) { 
			// 	 var x = $('input[name="<?php echo isset($model) ? $model : "" ?>[to_id]"]');
			// 	 x = x[0];
			// 	 var terms = x.value.split(/,\s*/);
		 //         terms.push(e.params.args.data.id);
				 
			// 	 terms = terms.filter(function (el) {
			// 	  return (el != null && el != "");
			// 	});
				 
		 //         x.id = terms.join(", ");
		 //         $('input[name="<?php echo isset($model) ? $model : "" ?>[to_id]"]').val(x.id);
			// });
			// $('select.search_user').on("select2:unselecting", function(e) { 
			// 	 var x = $('input[name="<?php echo isset($model) ? $model : "" ?>[to_id]"]');
			// 	 x = x[0];
		 //         var terms = x.id.split(/,\s*/);
			// 	 var ff = e.params.args.data.id;
			// 	 //console.log(ff);
			// 	 if(ff != undefined)
			// 	 {
			// 		for(var i in terms){
			// 			if(terms[i]==ff){
			// 				terms.splice(i,1);
			// 				break;
			// 			}
			// 		}
			// 	 }
				 
			// 	terms = terms.filter(function (el) {
			// 	  return (el != null && el != "");
			// 	});
				 
		 //         x.id = terms.join(", ");
		 //         $('input[name="<?php echo isset($model) ? $model : "" ?>[to_id]"]').val(x.id);
			// });
		});
		// autocomplate = "<?php echo base_url('admin/homeservice/get_data_autocomplete') ?>";
		// $("input[name='search_user']").autocomplete({
		//     source: function(request, response) {
		//         request.term = request.term.split(/,\s*/).pop();
		//         $.ajax({
		//             url: autocomplate,
		//             method: "GET",
		//             dataType: "json",
		//             data: {
		//                 autocomplete: 'search_user',
		//                 search: {
		//                     'name': request.term
		//                 }
		//             },
		//             success: function(data) {
		//                 console.log('search user', data);
		//                 response(data);
		//             },
		//             error: function(xhr, ajaxOptions, thrownError) {
		//                 console.log(xhr.status);
		//                 console.log(thrownError);
		//             }
		//         });
		//     },
		//     focus: function(event, ui) {
		//         return false;
		//     },
		//     select: function(event, ui) {
		//         var terms = this.value.split(/,\s*/);
		//         terms.pop();
		//         terms.push(ui.item.value);
		//         terms.push("");
		//         this.value = terms.join(", ");
		//         var terms = this.id.split(/,\s*/);
		//         terms.pop();
		//         terms.push(ui.item.id);
		//         terms.push("");
		//         this.id = terms.join(", ");
		//         $('input[name="<?php echo isset($model) ? $model : '
		//             ' ?>[to_id]"]').val(this.id);
		//         $('input[name="to_id"]').val(this.id);
		//         return false;
		//     },
		//     minLength: 1
		// });

		var listNotif = [];
		var listNotifCuti = [];
		var listNotifDoc = [];
		var listNotifAnn = [];

		var listNotifadd = [];

		function loadNotificationAdd() {
			url = "<?php echo base_url('admin/homeservice/get_notificationAdd') ?>";
			$.ajax({
				url: url,
				type: "POST",
				dataType: 'json',
				success: function(data) {
					console.log("hindia",data )
					//$("#notification").html("");
					$("#notif_cont_add").html("");
					$("#count_unread_add").html("");
					$("#count_notification_add").html(data['count']);
					count = "";
					if(data['count'] > 0) {
						count = "";
						count += '<span style="color: white" class="badge bg-red"> ' + data['count'] + ' Unread</span>'
					}
					// $("#count_unread").append(count);
					var countAnnouncement = 0;
					var countDailyreport = 0;
					var countComplaint = 0;
					var countGeneralreports = 0;
					var countMonthlyreports = 0;
					var countTechreports = 0;
					var countItreports = 0;
					var countClockin = 0;
					var countGeneralhousekeepingreports=0;
					listNotifadd = [];
					
					var countNotRead = data['noread'];
					
					$.each(data, function(index, value) {
						if(index !== 'count') {
							if(value['id']!=undefined && value.data['notif_type'] == 'bookinghs'){
							// if(value['id']!=undefined){
								listNotifadd.push(value);
								var notif = "";
								
								//console.log(value);
								var isread = false;
								if(value["read"]=="0") 
								{
									isread = true;
								}
								
								//notif += '<li id="notif_' + value['id'] + '">'
									// notif += '<a href="javascript:viewDetailNotif(this);" data-id="'+value['id']+'" data-notif="'+ value['dailyreports_id'] +'" onclick="goToNotification(this)" class="waves-effect waves-block">'
								
								/*notif += '<a href="javascript:void(0);" onclick="viewDetailNotif(this)" data-id="' + value['id'] + '" data-notif="' + value['dailyreports_id'] + '" class="waves-effect waves-block" style="border-radius:5px; padding:3px;">'
								notif += '<div class="icon-circle bg-light-green">'
								notif += '</div>'
								notif += '<div class="menu-info">'
								notif += '<h2 style="font-size:12px;">' + value["description"] + '</h2>'
								notif += '<p style="font-size:12px">' + value['time'] + '</p>'
								notif += '</div>'
								notif += '</a>'*/
								
								notif += '<a href="#" style="color:black;'+((isread)?"font-weight:bold;font-style: italic;":"")+'" onclick="viewDetailNotifAdd(this)" data-id="' + value['id'] + '" data-notif="' + value['dailyreports_id'] + '" class="p-3 d-flex border-bottom">'
								if(value["photo_profile"]!=null)
								{	
									notif += '	<div class="drop-img cover-image" data-image-src="<?php echo base_url()?>uploads/avatar/' + value["photo_profile"] + '" style="background: url(\'<?php echo base_url()?>uploads/avatar/' + value["photo_profile"] + '\') center center;">'
								}
								else
									notif += '	<div class="drop-img cover-image" data-image-src="<?php echo base_url()?>assets/biomedika_theme/admin/img/faces/5.jpg">'
								
								notif += '		<span class="avatar-status bg-teal"></span>'
								notif += '	</div>'
								notif += '	<div class="wd-90p">'
								notif += '		<div class="d-flex">'
								notif += '			<h5 class="mb-1 name">' + value["creator_name"] + '</h5>'
								notif += '			<p class="time mb-0 text-right ml-auto float-right">' + value['time'] + '</p>'
								notif += '		</div>'
								notif += '		<p class="mb-0 desc">' + value["description"] + '</p>'
								notif += '	</div>'
								notif += '</a>'
								
								
								//notif += '</li>'
								//$("#notification").append(notif);
								$("#notif_cont_add").append(notif);
								if(value.data != undefined && value.data != null) {
									var d = value.data;
									// console.log('notif',d.dailyreports_id, d.dailyreport_id, d.complaint_id);
									if(d.dailyreports_id != undefined && d.dailyreports_id != null) {
										countDailyreport++;
									} else if(d.dailyreport_id != undefined && d.dailyreport_id != null) {
										countDailyreport++;
									} else if(d.complaint_id != undefined && d.complaint_id != null) {
										countComplaint++;
									} else if(d.generalreports_id != undefined && d.generalreports_id != null) {
										countGeneralreports++;
									} else if(d.monthlyreports_id != undefined && d.monthlyreports_id != null) {
										countMonthlyreports++;
									} else if(d.techreports_id != undefined && d.techreports_id != null) {
										countTechreports++;
									} else if(d.itreports_id != undefined && d.itreports_id != null) {
										countItreports++;
									} else if(d.id != undefined && d.id != null) {
										countAnnouncement++;
									}
										else if(d.clockin_id != undefined && d.clockin_id != null) {
										countClockin++;
									}	
									else if(d.generalhousekeepingreports_id != undefined && d.generalhousekeepingreports_id != null) {
										countGeneralhousekeepingreports++;
									}	
								}
							}
						}
					});

					console.log(countNotRead);
					if(countNotRead > 0 && countNotRead!='undefined')
					{
						$("#count_notification_add").html(countNotRead);
						$(".notification-new_add").html(countNotRead+" new My Reports");
						$(".main-header-notification>a>span").removeClass('pulse').addClass("pulse-danger");
					} else {
						countNotRead = 0;
						$("#count_notification_add").html(countNotRead);
						$(".notification-new_add").html(countNotRead+" new My Reports");
						$(".main-header-notification>a>span").removeClass('pulse-danger').addClass("pulse");
					}
					
					if(countDailyreport > 0) {
						$("#count_unread_add").html('<span style="color: white" class="badge bg-red">' + countDailyreport + '</span>');
					} else {
						$("#count_unread_add").hide();
					}
					if(countComplaint > 0) {
						// $("#count_unread_complaint").html('<span style="color: white" class="badge bg-red">' + countComplaint + '</span>');
						$("#count_unread_complaint").html(countComplaint);
					} else {
						$("#count_unread_complaint").hide();
					}
					if(countGeneralreports > 0) {
						// $("#count_unread_complaint").html('<span style="color: white" class="badge bg-red">' + countComplaint + '</span>');
						$("#count_unread_generalreports").html(countGeneralreports);
					} else {
						$("#count_unread_generalreports").hide();
					}
					if(countMonthlyreports > 0) {
						// $("#count_unread_complaint").html('<span style="color: white" class="badge bg-red">' + countComplaint + '</span>');
						$("#count_unread_monthlyreports").html(countMonthlyreports);
					} else {
						$("#count_unread_monthlyreports").hide();
					}
					if(countTechreports > 0) {
						// $("#count_unread_complaint").html('<span style="color: white" class="badge bg-red">' + countComplaint + '</span>');
						$("#count_unread_techreports").html(countTechreports);
					} else {
						$("#count_unread_techreports").hide();
					}
					if(countItreports > 0) {
						// $("#count_unread_complaint").html('<span style="color: white" class="badge bg-red">' + countComplaint + '</span>');
						$("#count_unread_itreports").html(countItreports);
					} else {
						$("#count_unread_itreports").hide();
					}
					if(countAnnouncement > 0) {
						// $("#count_unread_complaint").html('<span style="color: white" class="badge bg-red">' + countComplaint + '</span>');
						$("#count_unread_announcements").html(countAnnouncement);
					} else {
						$("#count_unread_announcements").hide();
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(jqXHR, textStatus, errorThrown)
				}
			});
		}

		
		function loadNotification() {
			url = "<?php echo base_url('admin/homeservice/get_notification') ?>";
			$.ajax({
				url: url,
				type: "POST",
				dataType: 'json',
				success: function(data) {
					//$("#notification").html("");
					$("#notif_cont").html("");
					$("#count_unread").html("");
					$("#count_notification").html(data['count']);
					count = "";
					if(data['count'] > 0) {
						count = "";
						count += '<span style="color: white" class="badge bg-red"> ' + data['count'] + ' Unread</span>'
					}
					// $("#count_unread").append(count);
					var countAnnouncement = 0;
					var countDailyreport = 0;
					var countComplaint = 0;
					var countGeneralreports = 0;
					var countMonthlyreports = 0;
					var countTechreports = 0;
					var countItreports = 0;
					var countClockin = 0;
					var countGeneralhousekeepingreports=0;
					listNotif = [];
					
					var countNotRead = data['noread'];
					
					$.each(data, function(index, value) {
						if(index !== 'count') {
							// if(value['id']!=undefined && value.data['notif_type'] != 'bookinghs'){
							if(value['id']!=undefined){
								listNotif.push(value);
								var notif = "";
								
								//console.log(value);
								var isread = false;
								if(value["read"]=="0") 
								{
									isread = true;
								}
								
								//notif += '<li id="notif_' + value['id'] + '">'
									// notif += '<a href="javascript:viewDetailNotif(this);" data-id="'+value['id']+'" data-notif="'+ value['dailyreports_id'] +'" onclick="goToNotification(this)" class="waves-effect waves-block">'
								
								/*notif += '<a href="javascript:void(0);" onclick="viewDetailNotif(this)" data-id="' + value['id'] + '" data-notif="' + value['dailyreports_id'] + '" class="waves-effect waves-block" style="border-radius:5px; padding:3px;">'
								notif += '<div class="icon-circle bg-light-green">'
								notif += '</div>'
								notif += '<div class="menu-info">'
								notif += '<h2 style="font-size:12px;">' + value["description"] + '</h2>'
								notif += '<p style="font-size:12px">' + value['time'] + '</p>'
								notif += '</div>'
								notif += '</a>'*/
								
								notif += '<a href="#" style="color:black;'+((isread)?"font-weight:bold;font-style: italic;":"")+'" onclick="viewDetailNotif(this)" data-id="' + value['id'] + '" data-notif="' + value['dailyreports_id'] + '" class="p-3 d-flex border-bottom">'
								if(value["photo_profile"]!=null)
								{	
									notif += '	<div class="drop-img cover-image" data-image-src="<?php echo base_url()?>uploads/avatar/' + value["photo_profile"] + '" style="background: url(\'<?php echo base_url()?>uploads/avatar/' + value["photo_profile"] + '\') center center;">'
								}
								else
									notif += '	<div class="drop-img cover-image" data-image-src="<?php echo base_url()?>assets/biomedika_theme/admin/img/faces/5.jpg">'
								
								notif += '		<span class="avatar-status bg-teal"></span>'
								notif += '	</div>'
								notif += '	<div class="wd-90p">'
								notif += '		<div class="d-flex">'
								notif += '			<h5 class="mb-1 name">' + value["creator_name"] + '</h5>'
								notif += '			<p class="time mb-0 text-right ml-auto float-right">' + value['time'] + '</p>'
								notif += '		</div>'
								notif += '		<p class="mb-0 desc">' + value["description"] + '</p>'
								notif += '	</div>'
								notif += '</a>'
								
								
								//notif += '</li>'
								//$("#notification").append(notif);
								$("#notif_cont").append(notif);
								if(value.data != undefined && value.data != null) {
									var d = value.data;
									// console.log('notif',d.dailyreports_id, d.dailyreport_id, d.complaint_id);
									if(d.dailyreports_id != undefined && d.dailyreports_id != null) {
										countDailyreport++;
									} else if(d.dailyreport_id != undefined && d.dailyreport_id != null) {
										countDailyreport++;
									} else if(d.complaint_id != undefined && d.complaint_id != null) {
										countComplaint++;
									} else if(d.generalreports_id != undefined && d.generalreports_id != null) {
										countGeneralreports++;
									} else if(d.monthlyreports_id != undefined && d.monthlyreports_id != null) {
										countMonthlyreports++;
									} else if(d.techreports_id != undefined && d.techreports_id != null) {
										countTechreports++;
									} else if(d.itreports_id != undefined && d.itreports_id != null) {
										countItreports++;
									} else if(d.id != undefined && d.id != null) {
										countAnnouncement++;
									}
									  else if(d.clockin_id != undefined && d.clockin_id != null) {
										countClockin++;
									}
									else if(d.generalhousekeepingreports_id != undefined && d.generalhousekeepingreports_id != null) {
										countGeneralhousekeepingreports++;
									}	
								}
							}
						}
					});
					console.log(countNotRead);
					if(countNotRead > 0 && countNotRead!='undefined')
					{
						$("#count_notification").html(countNotRead);
						$(".notification-new").html(countNotRead+" new My Reports");
						$(".main-header-notification>a>span").removeClass('pulse').addClass("pulse-danger");
					} else {
						countNotRead = 0;
						$("#count_notification").html(countNotRead);
						$(".notification-new").html(countNotRead+" new My Reports");
						$(".main-header-notification>a>span").removeClass('pulse-danger').addClass("pulse");
					}
					
					if(countDailyreport > 0) {
						$("#count_unread").html('<span style="color: white" class="badge bg-red">' + countDailyreport + '</span>');
					} else {
						$("#count_unread").hide();
					}
					if(countComplaint > 0) {
						// $("#count_unread_complaint").html('<span style="color: white" class="badge bg-red">' + countComplaint + '</span>');
						$("#count_unread_complaint").html(countComplaint);
					} else {
						$("#count_unread_complaint").hide();
					}
					if(countGeneralreports > 0) {
						// $("#count_unread_complaint").html('<span style="color: white" class="badge bg-red">' + countComplaint + '</span>');
						$("#count_unread_generalreports").html(countGeneralreports);
					} else {
						$("#count_unread_generalreports").hide();
					}
					if(countMonthlyreports > 0) {
						// $("#count_unread_complaint").html('<span style="color: white" class="badge bg-red">' + countComplaint + '</span>');
						$("#count_unread_monthlyreports").html(countMonthlyreports);
					} else {
						$("#count_unread_monthlyreports").hide();
					}
					if(countTechreports > 0) {
						// $("#count_unread_complaint").html('<span style="color: white" class="badge bg-red">' + countComplaint + '</span>');
						$("#count_unread_techreports").html(countTechreports);
					} else {
						$("#count_unread_techreports").hide();
					}
					if(countItreports > 0) {
						// $("#count_unread_complaint").html('<span style="color: white" class="badge bg-red">' + countComplaint + '</span>');
						$("#count_unread_itreports").html(countItreports);
					} else {
						$("#count_unread_itreports").hide();
					}
					if(countAnnouncement > 0) {
						// $("#count_unread_complaint").html('<span style="color: white" class="badge bg-red">' + countComplaint + '</span>');
						$("#count_unread_announcements").html(countAnnouncement);
					} else {
						$("#count_unread_announcements").hide();
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(jqXHR, textStatus, errorThrown)
				}
			});
		}


		function viewDetailNotifAdd(node) {
			var currentUrl = "<?= current_url() ?>";
			node = $(node);
			console.log("node", node);
			var idNotif = node.attr("data-id");
			// console.log("id notif", idNotif);
			var notif = null;
			/* mencari di list notif */
			console.log("listNotif "+listNotif);
			console.log("listNotifCuti "+listNotifCuti);
			console.log(listNotif);
			//return;
			for(var i = 0, i2 = listNotifadd.length; i < i2; i++) {
				var n = listNotifadd[i];
				if(n.id == idNotif) {
					notif = n;
					break;
				}
			}
			
			for(var i = 0, i2 = listNotifCuti.length; i < i2; i++) {
				var n = listNotifCuti[i];
				if(n.id == idNotif) {
					notif = n;
					break;
				}
			}
			
			for(var i = 0, i2 = listNotifAnn.length; i < i2; i++) {
				var n = listNotifAnn[i];
				if(n.id == idNotif) {
					notif = n;
					break;
				}
			}
			
			/* jika tidak ditemukan di list notif*/
			if(notif == null) {
				console.log('notif tidak ditemukan');
				return;
			}
			/* jika ditemukan di list notif*/
			var nData = null
			if(notif.data != undefined && notif.data != null) {
				var nData = notif.data;
			}
			/* jika notif tidak memiliki atribut data */
			if(nData == null) {
				console.log('nData is null');
				return;
			}
			var url = null;
			var id = null;
			
			if(nData.dailyreports_id != undefined && nData.dailyreports_id != null) {
				url = "dailyreports";
				id = nData.dailyreports_id;
			} else if(nData.dailyreport_id != undefined && nData.dailyreport_id != null) {
				url = "dailyreports";
				id = nData.dailyreport_id;
			} else if(nData.complaint_id != undefined && nData.complaint_id != null) {
				url = "complaint";
				id = nData.complaint_id;
			}  else if(nData.monthlyreports_id != undefined && nData.monthlyreports_id != null) {
				url = "monthlyreports";
				id = nData.monthlyreports_id;
			} else if(nData.techreports_id != undefined && nData.techreports_id != null) {
				url = "techreports";
				id = nData.techreports_id;
			} else if(nData.qcreport_id != undefined && nData.qcreport_id != null) {
				url = "qc";
				id = nData.qcreport_id;
				document.location = '<?= base_url() ?>admin/' + url + '?notifqc=' + id;
				return;
			} else if(nData.itreports_id != undefined && nData.itreports_id != null) {
				url = "itreports";
				id = nData.itreports_id;
			}
			else if(nData.doc_id != undefined && nData.doc_id != null) {
				url = "doc";
				id = nData.doc_id;
			}

			else if(nData.generalreports_id != undefined && nData.generalreports_id != null) {
				url = "generalreports";
				id = nData.generalreports_id;
			}

			else if(nData.generalhousekeepingreports_id != undefined && nData.generalhousekeepingreports_id != null) {
				url = "generalhousekeepingreports";
				id = nData.generalhousekeepingreports_id;
			}
			 else if(nData.lot_number != undefined && nData.lot_number != null) {
				url = "qc";
				id = nData.id_lot_number;
				
				urlSetReadUser = "<?php echo base_url('admin/announcement/update_notification') ?>";
				$.ajax({
				url: urlSetReadUser,
				type: "POST",
				dataType: 'json',
				data: {
				id: notif.id
				},
				success: function(data) {
					//var tr = $("button[data-id="+id+"]");
					//showEditLotNum(tr);
					document.location = '<?= base_url() ?>admin/' + url ;
					return;
				},
				error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR, textStatus, errorThrown)
				}
				});
				
				
			} else if(nData.event_type != undefined && nData.event_type != null) {
				url = "announcement";
				id = nData.id;
				
				urlSetReadUser = "<?php echo base_url('admin/announcement/update_notification') ?>";
				$.ajax({
				url: urlSetReadUser,
				type: "POST",
				dataType: 'json',
				data: {
				id: notif.id
				},
				success: function(data) {
					//document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id;

				},
				error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR, textStatus, errorThrown)
				}
				});
				
			} else if(nData.id_qc_input != undefined && nData.id_qc_input != null) {
				url = "qc";
				
				if(nData.id_hasil != undefined && nData.id_hasil != null) {
					id = nData.id_hasil;
					node.attr("id-hasil",id)
				}
				else{
					id = nData.id;
					node.attr("id-hasil",id)
				}
				node.attr("id-qc-input",nData.id_qc_input);
				 ; 
				showQcChart(node);
				
				//console.log('format data notifikasi tidak dikenal', node.attr("id-hasil"), notif);
				
				urlSetReadUser = "<?php echo base_url('admin/qc/set_user_read') ?>";
				$.ajax({
				url: urlSetReadUser,
				type: "POST",
				dataType: 'json',
				data: {
				id: idNotif
				},
				success: function(data) {
					//document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id;

				},
				error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR, textStatus, errorThrown)
				}
				});
				
				return;
			}
			else if(nData.notif_type == 'homeservice') {
				url = "homeservice";
				id = nData.id;
				// stay
				// alert(JSON.stringify(nData))
				document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id+'&date='+nData.date+'&branch_id='+nData.branch_id+'&notif_type='+nData.notif_type;
				
				urlSetReadUser = "<?php echo base_url('admin/announcement/update_notification') ?>";
				$.ajax({
				url: urlSetReadUser,
				type: "POST",
				dataType: 'json',
				data: {
				id: notif.id
				},
				success: function(data) {
					//var tr = $("button[data-id="+id+"]");
					//showEditLotNum(tr);
					return;
				},
				error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR, textStatus, errorThrown)
				}
				});
				
				return;
			}
			else if(nData.notif_type == 'bookinghs') {
				url = "homeservice";
				id = nData.id;
				document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id+'&date='+nData.date+'&branch_id='+nData.branch_id+'&notif_type='+nData.notif_type;
				
				urlSetReadUser = "<?php echo base_url('admin/announcement/update_notification') ?>";
				$.ajax({
				url: urlSetReadUser,
				type: "POST",
				dataType: 'json',
				data: {
				id: notif.id
				},
				success: function(data) {
					//var tr = $("button[data-id="+id+"]");
					return;
				},
				error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR, textStatus, errorThrown)
				}
				});
				
				return;
			}
			else if(nData.notif_type == 'petugashs') {
				url = "master_bm";
				id = nData.id;
				document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id+'&type=petugashs&branch_id='+nData.branch_id;
				
				urlSetReadUser = "<?php echo base_url('admin/announcement/update_notification') ?>";
				$.ajax({
				url: urlSetReadUser,
				type: "POST",
				dataType: 'json',
				data: {
				id: notif.id
				},
				success: function(data) {
					//var tr = $("button[data-id="+id+"]");
					return;
				},
				error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR, textStatus, errorThrown)
				}
				});
				
				return;
			}
			else if(nData.notif_type == 'suhu') {
				url = "suhu";
				id = nData.id;
				//document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id+'&date='+nData.date+'&branch_id='+nData.branch_id;
				$('#suhu-temp').html(nData.body_temperature);
				$('#suhu-nama').html(nData.user_name);
				$('#suhu-foto').html('<img alt="" width="200px" src="https://120.29.156.110'+nData.user_photo+'">');
				$('#modal_suhu_alert').modal("show");
				return;
			}			
			/*else if(nData.id_cuti != undefined && nData.id_cuti != null) {
				url = "reqcuti";
				id = nData.id_cuti+'_'+idNotif;
			}
				-- notif cuti dipisah
			*/
			else if (nData.notif_type=='clockin') {
				url = "hr";
				id = nData.clockin_id;
				document.location = '<?= base_url() ?>admin/' + url + '?notifAbsensi=' + id;
				return false;
			}

			else if (nData.notif_type=='clockin_suhu') {
				url = "clockin";
				id = nData.clockin_id;
				document.location = '<?= base_url() ?>admin/' + url + '?notifSuhu=' + id;
				return false;
			}

			if(url == null) {
				console.log('format data notifikasi tidak dikenal', nData, notif);
				return;
			}
					
			if(currentUrl.indexOf(url) >= 0) {
			 //   console.log('trigger click', url, id);
				$("#view_" + id).click();
				var tr = $('#view_' + id);
				if(tr.length > 0) {
					viewData(tr);
				} else {
					viewData(null, id);
				}
				return;
			}
			console.log('redirect to page', url);
			document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id;
			
		}

		function viewDetailNotif(node) {
			var currentUrl = "<?= current_url() ?>";
			node = $(node);
			console.log("node", node);
			var idNotif = node.attr("data-id");
			// console.log("id notif", idNotif);
			var notif = null;
			/* mencari di list notif */
			//console.log("listNotif "+listNotif);
			//console.log("listNotifCuti "+listNotifCuti);
			//console.log(listNotif);
			//return;
			for(var i = 0, i2 = listNotif.length; i < i2; i++) {
				var n = listNotif[i];
				if(n.id == idNotif) {
					notif = n;
					break;
				}
			}
			
			for(var i = 0, i2 = listNotifCuti.length; i < i2; i++) {
				var n = listNotifCuti[i];
				if(n.id == idNotif) {
					notif = n;
					break;
				}
			}
			
			for(var i = 0, i2 = listNotifAnn.length; i < i2; i++) {
				var n = listNotifAnn[i];
				if(n.id == idNotif) {
					notif = n;
					break;
				}
			}
			
			/* jika tidak ditemukan di list notif*/
			if(notif == null) {
				console.log('notif tidak ditemukan');
				return;
			}
			/* jika ditemukan di list notif*/
			var nData = null
			if(notif.data != undefined && notif.data != null) {
				var nData = notif.data;
			}
			/* jika notif tidak memiliki atribut data */
			if(nData == null) {
				console.log('nData is null');
				return;
			}
			var url = null;
			var id = null;
			
			if(nData.dailyreports_id != undefined && nData.dailyreports_id != null) {
				url = "dailyreports";
				id = nData.dailyreports_id;
			} else if(nData.dailyreport_id != undefined && nData.dailyreport_id != null) {
				url = "dailyreports";
				id = nData.dailyreport_id;
			} else if(nData.complaint_id != undefined && nData.complaint_id != null) {
				url = "complaint";
				id = nData.complaint_id;
			}  else if(nData.monthlyreports_id != undefined && nData.monthlyreports_id != null) {
				url = "monthlyreports";
				id = nData.monthlyreports_id;
			} else if(nData.techreports_id != undefined && nData.techreports_id != null) {
				url = "techreports";
				id = nData.techreports_id;
			} else if(nData.qcreport_id != undefined && nData.qcreport_id != null) {
				url = "qc";
				id = nData.qcreport_id;
				document.location = '<?= base_url() ?>admin/' + url + '?notifqc=' + id;
				return;
			} else if(nData.itreports_id != undefined && nData.itreports_id != null) {
				url = "itreports";
				id = nData.itreports_id;
			}
			else if(nData.doc_id != undefined && nData.doc_id != null) {
				url = "doc";
				id = nData.doc_id;
			}

			else if(nData.generalreports_id != undefined && nData.generalreports_id != null) {
				url = "generalreports";
				id = nData.generalreports_id;
			}
			else if(nData.generalhousekeepingreports_id != undefined && nData.generalhousekeepingreports_id != null) {
				url = "generalhousekeepingreports";
				id = nData.generalhousekeepingreports_id;
			}
			 else if(nData.lot_number != undefined && nData.lot_number != null) {
				url = "qc";
				id = nData.id_lot_number;
				
				urlSetReadUser = "<?php echo base_url('admin/announcement/update_notification') ?>";
				$.ajax({
				url: urlSetReadUser,
				type: "POST",
				dataType: 'json',
				data: {
				id: notif.id
				},
				success: function(data) {
					//var tr = $("button[data-id="+id+"]");
					//showEditLotNum(tr);
					document.location = '<?= base_url() ?>admin/' + url ;
					return;
				},
				error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR, textStatus, errorThrown)
				}
				});
				
				
			} else if(nData.event_type != undefined && nData.event_type != null) {
				url = "announcement";
				id = nData.id;
				
				urlSetReadUser = "<?php echo base_url('admin/announcement/update_notification') ?>";
				$.ajax({
				url: urlSetReadUser,
				type: "POST",
				dataType: 'json',
				data: {
				id: notif.id
				},
				success: function(data) {
					//document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id;

				},
				error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR, textStatus, errorThrown)
				}
				});
				
			} else if(nData.id_qc_input != undefined && nData.id_qc_input != null) {
				url = "qc";
				
				if(nData.id_hasil != undefined && nData.id_hasil != null) {
					id = nData.id_hasil;
					node.attr("id-hasil",id)
				}
				else{
					id = nData.id;
					node.attr("id-hasil",id)
				}
				node.attr("id-qc-input",nData.id_qc_input);
				 ; 
				showQcChart(node);
				
				//console.log('format data notifikasi tidak dikenal', node.attr("id-hasil"), notif);
				
				urlSetReadUser = "<?php echo base_url('admin/qc/set_user_read') ?>";
				$.ajax({
				url: urlSetReadUser,
				type: "POST",
				dataType: 'json',
				data: {
				id: idNotif
				},
				success: function(data) {
					//document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id;

				},
				error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR, textStatus, errorThrown)
				}
				});
				
				return;
			}
			else if(nData.notif_type == 'homeservice') {
				url = "homeservice";
				id = nData.id;
				// stay
				// alert(JSON.stringify(nData))
				document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id+'&date='+nData.date+'&branch_id='+nData.branch_id+'&notif_type='+nData.notif_type;
				
				urlSetReadUser = "<?php echo base_url('admin/announcement/update_notification') ?>";
				$.ajax({
				url: urlSetReadUser,
				type: "POST",
				dataType: 'json',
				data: {
				id: notif.id
				},
				success: function(data) {

					return;
				},
				error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR, textStatus, errorThrown)
				}
				});
				
				return;
			}
			else if(nData.notif_type == 'bookinghs') {
				url = "homeservice";
				id = nData.id;
				document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id+'&date='+nData.date+'&branch_id='+nData.branch_id+'&notif_type='+nData.notif_type;
				urlSetReadUser = "<?php echo base_url('admin/announcement/update_notification') ?>";
				$.ajax({
				url: urlSetReadUser,
				type: "POST",
				dataType: 'json',
				data: {
				id: notif.id
				},
				success: function(data) {
					return;
				},
				error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR, textStatus, errorThrown)
				}
				});
				
				return;
			}
			else if(nData.notif_type == 'petugashs') {
				url = "master_bm";
				id = nData.id;
				document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id+'&type=petugashs&branch_id='+nData.branch_id;
				urlSetReadUser = "<?php echo base_url('admin/announcement/update_notification') ?>";
				$.ajax({
				url: urlSetReadUser,
				type: "POST",
				dataType: 'json',
				data: {
				id: notif.id
				},
				success: function(data) {
					return;
				},
				error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR, textStatus, errorThrown)
				}
				});

				return;
			}
			else if(nData.notif_type == 'suhu') {
				url = "suhu";
				id = nData.id;
				//document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id+'&date='+nData.date+'&branch_id='+nData.branch_id;
				$('#suhu-temp').html(nData.body_temperature);
				$('#suhu-nama').html(nData.user_name);
				$('#suhu-foto').html('<img alt="" width="200px" src="https://120.29.156.110'+nData.user_photo+'">');
				$('#modal_suhu_alert').modal("show");
				return;
			}			
			/*else if(nData.id_cuti != undefined && nData.id_cuti != null) {
				url = "reqcuti";
				id = nData.id_cuti+'_'+idNotif;
			}
				-- notif cuti dipisah
			*/
			else if (nData.notif_type=='clockin') {
				url = "hr";
				id = nData.clockin_id;
				document.location = '<?= base_url() ?>admin/' + url + '?notifAbsensi=' + id;
				return false;
			}

			else if (nData.notif_type=='clockin_suhu') {
				url = "clockin";
				id = nData.clockin_id;
				document.location = '<?= base_url() ?>admin/' + url + '?notifSuhu=' + id;
				return false;
			}

			else if(nData.notif_type == 'absensi') {
				url = "dailyreports";
				id = nData.id;
				document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id+'&notif_type='+nData.notif_type;
				return;
			}

			if(url == null) {
				console.log('format data notifikasi tidak dikenal', nData, notif);
				return;
			}
					
			if(currentUrl.indexOf(url) >= 0) {
			 //   console.log('trigger click', url, id);
				$("#view_" + id).click();
				var tr = $('#view_' + id);
				if(tr.length > 0) {
					viewData(tr);
				} else {
					viewData(null, id);
				}
				return;
			}
			console.log('redirect to page', url);
			document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id;
			
		}

		function loadNotificationAnn() {
			url = "<?php echo base_url('admin/announcement/get_notification') ?>";
			$.ajax({
				url: url,
				type: "POST",
				dataType: 'json',
				success: function(data) {
					//$("#notification").html("");
					$("#message_cont").html("");
					$("#count_unread").html("");
					$("#count_message").html(data['count']);
					count = "";
					if(data['count'] > 0) {
						count = "";
						count += '<span style="color: white" class="badge bg-red"> ' + data['count'] + ' Unread</span>'
					}
					// $("#count_unread").append(count);
					var countAnnouncement = 0;
					listNotifAnn = [];
					
					var countNotRead = data['noread'];
					
					$.each(data, function(index, value) {
						if(index !== 'count') {
							if(value['id']!=undefined){
								listNotifAnn.push(value);
								var notif = "";
								
								//console.log(value);
								var isread = false;
								if(value["read"]=="0") 
								{
									isread = true;
								}
								
								notif += '<a href="#" style="color:black;'+((isread)?"font-weight:bold;font-style: italic;":"")+'" onclick="viewDetailNotif(this)" data-id="' + value['id'] + '" data-notif="' + value['id'] + '" class="p-3 d-flex border-bottom">'
								if(value["photo_profile"]!=null)
								{	
									notif += '	<div class="drop-img cover-image" data-image-src="<?php echo base_url()?>uploads/avatar/' + value["photo_profile"] + '" style="background: url(\'<?php echo base_url()?>uploads/avatar/' + value["photo_profile"] + '\') center center;">'
								}
								else
									notif += '	<div class="drop-img cover-image" data-image-src="<?php echo base_url()?>assets/biomedika_theme/admin/img/faces/5.jpg">'
								
								notif += '		<span class="avatar-status bg-teal"></span>'
								notif += '	</div>'
								notif += '	<div class="wd-90p">'
								notif += '		<div class="d-flex">'
								notif += '			<h5 class="mb-1 name">' + value["creator_name"] + '</h5>'
								notif += '			<p class="time mb-0 text-right ml-auto float-right">' + value['time'] + '</p>'
								notif += '		</div>'
								notif += '		<p class="mb-0 desc">' + value["description"] + '</p>'
								notif += '	</div>'
								notif += '</a>'
								
								
								$("#message_cont").append(notif);
								if(value.data != undefined && value.data != null) {
									var d = value.data;
									// console.log('notif',d.dailyreports_id, d.dailyreport_id, d.complaint_id);
									if(d.id != undefined && d.id != null) {
										countAnnouncement++;
									}	
								}
							}
						}
					});
					//console.log(countNotRead);
					if(countNotRead > 0)
					{
						$("#count_message").html(countNotRead);
						$(".message-new").html(countNotRead+" new My Announcements");
						//$(".main-header-message>a>span").removeClass('pulse').addClass("pulse-danger");
					} else {
						$("#count_message").html(countNotRead);
						$(".message-new").html(countNotRead+" new My Announcements");
						//$(".main-header-message>a>span").removeClass('pulse-danger').addClass("pulse");
					}
					if(countAnnouncement > 0) {
						$("#count_unread_announcements").html(countAnnouncement);
					} else {
						$("#count_unread_announcements").hide();
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(jqXHR, textStatus, errorThrown)
				}
			});
		}

		function loadNotifCuti(){
			url = "<?php echo base_url('admin/reqcuti/get_notif_cuti') ?>";
			$.ajax({
				url: url,
				type: "POST",
				dataType: 'json',
				success: function(data) {
					$("#cuti_cont").html("");
					$("#notif_cuti").html("");
					$("#count_notif_cuti").html(data['count']);
					
					var countNotRead = data['noread'];
					
					listNotifCuti = [];
					$.each(data, function(index, value) {
					   if (index != 'count') {
						  if(value['id']!=undefined){
								listNotifCuti.push(value);
								var notif = "";
						   
								var isread = false;
								if(value["read"]=="0") 
								{
									isread = true;
								}
							   
								notif += '<a href="#" style="color:black;'+((isread)?"font-weight:bold;font-style: italic;":"")+'" onclick="viewNotifCuti(this)" data-id="' + value['id'] + '" data-notif="' + value['id_cuti'] + '" data-hrd="' + value['hrd'] + '" class="p-3 d-flex border-bottom">'
								if(value["photo_profile"]!=null)
								{	
									notif += '	<div class="drop-img cover-image" data-image-src="<?php echo base_url()?>uploads/avatar/' + value["photo_profile"] + '" style="background: url(\'<?php echo base_url()?>uploads/avatar/' + value["photo_profile"] + '\') center center;">'
								}
								else
									notif += '	<div class="drop-img cover-image" data-image-src="<?php echo base_url()?>assets/biomedika_theme/admin/img/faces/5.jpg">'
								
								notif += '		<span class="avatar-status bg-teal"></span>'
								notif += '	</div>'
								notif += '	<div class="wd-90p">'
								notif += '		<div class="d-flex">'
								notif += '			<h5 class="mb-1 name">' + value["creator_name"] + '</h5>'
								notif += '			<p class="time mb-0 text-right ml-auto float-right">' + value['time'] + '</p>'
								notif += '		</div>'
								notif += '		<p class="mb-0 desc">' + value["description"] + '</p>'
								notif += '	</div>'
								notif += '</a>'
							   
							   $("#cuti_cont").append(notif); 
						   }
					   }
							
					});
					
					//console.log(countNotRead);
					if(countNotRead > 0)
					{
						$("#count_notif_cuti").html(countNotRead);
						$(".cuti-new").html(countNotRead+" new My Cuti & Izin");
						//$(".main-header-cuti>a>span").removeClass('pulse').addClass("pulse-danger");
					} else {
						$("#count_notif_cuti").html(countNotRead);
						$(".cuti-new").html(countNotRead+" new My Cuti & Izin");
						//$(".main-header-cuti>a>span").removeClass('pulse-danger').addClass("pulse");
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(jqXHR, textStatus, errorThrown)
				}
			});
		}

		function loadNotifDoc(){
			url = "<?php echo base_url('admin/doc/get_notif_doc') ?>";
			$.ajax({
				url: url,
				type: "POST",
				dataType: 'json',
				success: function(data) {
					$("#doc_cont").html("");
					$("#notif_doc").html("");
					$("#count_notif_doc").html(data['count']);
					
					var countNotRead = data['noread'];
					
					listNotifDoc = [];
					$.each(data, function(index, value) {

					   if (index != 'count') {
						  if(value['id']!=undefined){
								  listNotifDoc.push(value);
								   var notif = "";								  
								   
								    var isread = false;
									if(value["read"]=="0") 
									{
										isread = true;
									}
								   
									notif += '<a href="#" style="color:black;'+((isread)?"font-weight:bold;font-style: italic;":"")+'" onclick="viewNotifDoc(this)" data-id="' + value['id'] + '" data-notif="' + value['doc_id'] + '" data-hrd="' + value['hrd'] + '" class="p-3 d-flex border-bottom">'
									if(value["photo_profile"]!=null)
									{	
										notif += '	<div class="drop-img cover-image" data-image-src="<?php echo base_url()?>uploads/avatar/' + value["photo_profile"] + '" style="background: url(\'<?php echo base_url()?>uploads/avatar/' + value["photo_profile"] + '\') center center;">'
									}
									else
										notif += '	<div class="drop-img cover-image" data-image-src="<?php echo base_url()?>assets/biomedika_theme/admin/img/faces/5.jpg">'
									
									notif += '		<span class="avatar-status bg-teal"></span>'
									notif += '	</div>'
									notif += '	<div class="wd-90p">'
									notif += '		<div class="d-flex">'
									notif += '			<h5 class="mb-1 name">' + value["creator_name"] + '</h5>'
									notif += '			<p class="time mb-0 text-right ml-auto float-right">' + value['time'] + '</p>'
									notif += '		</div>'
									notif += '		<p class="mb-0 desc">' + value["description"] + '</p>'
									notif += '	</div>'
									notif += '</a>'
								   $("#doc_cont").append(notif); 
							}
					   }
							
					});
					
					if(countNotRead > 0)
					{
						$("#count_notif_doc").html(countNotRead);
						$(".doc-new").html(countNotRead+" new My DOC");
						//$(".main-header-cuti>a>span").removeClass('pulse').addClass("pulse-danger");
					} else {
						$("#count_notif_doc").html(countNotRead);
						$(".doc-new").html(countNotRead+" new My DOC");
						//$(".main-header-cuti>a>span").removeClass('pulse-danger').addClass("pulse");
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(jqXHR, textStatus, errorThrown)
				}
			});
		}

		function viewNotifDoc(node){
			node = $(node);
			var idNotif = node.attr("data-id");
			var notif = null;
			/* mencari di list notif */
			for(var i = 0, i2 = listNotifDoc.length; i < i2; i++) {
				var n = listNotifDoc[i];
				if(n.id == idNotif) {
					notif = n;
					break;
				}
			}
			/* jika tidak ditemukan di list notif*/
			if(notif == null) {
				console.log('notif tidak ditemukan doc: '+idNotif);
				return;
			}
			/* jika ditemukan di list notif*/
			var nData = null
			if(notif.data != undefined && notif.data != null) {
				var nData = notif.data;
			}
			/* jika notif tidak memiliki atribut data */
			if(nData == null) {
				console.log('nData is null');
				return;
			}
			var url = null;
			var id = null;

			if((nData.doc_id != undefined && nData.doc_id != null) && (nData.notif_type == "reqdoc")) {
				url = "doc";
				//if (nData.hrd) {
				//    url = "hr"
				// }
				id = nData.doc_id+'_'+idNotif;
				document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id;
			}
			else if((nData.doc_id != undefined && nData.doc_id != null) && (nData.notif_type == "changedoc")) {
				url = "doc";
				//if (nData.hrd) {
				//    url = "hr"
				// }
				id = nData.doc_id+'_'+idNotif;
				document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id+'&no_doc='+nData.no_doc;
			}
			else{
				console.log('tidak dapat menuju halaman detail!');
				return false;
			}

			
		}

		function viewNotifCuti(node){
			node = $(node);
			var idNotif = node.attr("data-id");
			var notif = null;
			/* mencari di list notif */
			for(var i = 0, i2 = listNotifCuti.length; i < i2; i++) {
				var n = listNotifCuti[i];
				if(n.id == idNotif) {
					notif = n;
					break;
				}
			}
			/* jika tidak ditemukan di list notif*/
			if(notif == null) {
				console.log('notif tidak ditemukan cuti: '+idNotif);
				return;
			}
			/* jika ditemukan di list notif*/
			var nData = null
			if(notif.data != undefined && notif.data != null) {
				var nData = notif.data;
			}
			/* jika notif tidak memiliki atribut data */
			if(nData == null) {
				console.log('nData is null');
				return;
			}
			var url = null;
			var id = null;
			console.log(nData);
			if(nData.id_cuti != undefined && nData.id_cuti != null) {
				url = "reqcuti";
				//if (nData.hrd) {
				//    url = "hr"
				// }
				id = nData.id_cuti+'_'+idNotif;
				document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id;
			}
			else if(nData.id_izin != undefined && nData.id_izin != null) {
				url = "reqizin";
				//if (nData.hrd) {
				//    url = "hr"
				// }
				id = nData.id_izin+'_'+idNotif;
				document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id;
			}
			else if(nData.id != undefined && nData.id != null) {
				url = "hr";
				//if (nData.hrd) {
				//    url = "hr"
				// }
				id = nData.id+'_'+idNotif;
				document.location = '<?= base_url() ?>admin/' + url + '?id=' + id;
			}
			else if(nData.id_user != undefined && nData.id_user != null) {
				url = "hr_data";
				//if (nData.hrd) {
				//    url = "hr"
				// }
				id_user = nData.id_user;
				document.location = '<?= base_url() ?>admin/' + url + '?id_user=' + id_user;
			}
			else if(nData.sp_id != undefined && nData.sp_id != null) {
				url = "overtime";
				id = nData.sp_id+'_'+idNotif;
				document.location = '<?= base_url() ?>admin/' + url + '?notif=' + id;
			}
			else{
				console.log('tidak dapat menuju halaman detail!');
				return false;
			}

			
		}

		function loadReminder() {
			url = "<?php echo base_url('admin/ijin/get_reminder') ?>";
			$.ajax({
				url: url,
				type: "POST",
				dataType: 'json',
				success: function(data) {
					$("#reminder_cont").html("");
					$("#count_reminder").html("0");
					if(data.length > 0) {
						$("#count_reminder").html(data.length);
						$(".reminder-new").html(data.length+" new Reminders");
						
						$.each(data, function(index, value) {
							$("#reminder_cont").append(value.data.html);
						})
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(jqXHR, textStatus, errorThrown)
				}
			});
		}
		var loadNotifWithInterval = true;
		$(function() {
			// window.history.replaceState(null, 'MIS APP', window.location.href);
			// hashDetection();
			loadNotification();
			// loadNotificationAnn();
			// loadReminder();
			// loadNotifCuti(); /*notif cuti*/
			// loadNotifDoc();

			// loadNotificationAdd();
			setInterval(function() {
				if(loadNotifWithInterval) {
					loadNotification();
					// loadNotificationAnn();
					// loadReminder();
					// loadNotifCuti(); /*notif cuti*/
					// loadNotifDoc();

					// loadNotificationAdd()
				}
			}, 30000);
			openModalFromNotif();
			$('#compose').click(function() {
				$('#add').on('hidden.bs.modal', function() {
					user = $("input[name='search_user']").val();
					if(user !== '') {
						save(true);
					}
				});
			});

			/*check session login*/
			setInterval(function() {
				var urlCheck = '<?= base_url("welcome/checkSession") ?>'
				$.get(urlCheck,function(res){
					var resp = $.parseJSON(res);
					if (resp.session=='end') {
						window.location = '<?= base_url()."?pesan=logout" ?>';
					}
				});
			}, 30000);
		});


		var hashDetection = new hashHandler();
		function hashHandler(){
			this.oldHash = window.location.hash;
			this.Check;

			var that = this;
			var detect = function(){
				if(that.oldHash!=window.location.hash){
					alert("HASH CHANGED - new has" + window.location.hash);
					that.oldHash = window.location.hash;
				}
			};
			this.Check = setInterval(function(){ detect() }, 100);
		}		

		function goToNotification(obj) {
			var notif = $(obj).data('notif');
			var id = $(obj).data('id');
			url_notif = "<?php echo base_url('admin/dailyreports?notif=') ?>" + notif;
			urlRead = "<?php echo base_url('admin/homeservice/update_notification') ?>";
			$.ajax({
				url: urlRead,
				type: "POST",
				dataType: 'json',
				data: {
					id: id
				},
				success: function(data) {
					if(data['status'] == true) {
						window.location.href = url_notif;
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(jqXHR, textStatus, errorThrown)
				}
			});
		}

		function goToReminder(obj,type=0) {
			var reminder = $(obj).data('id');
			url = "<?php echo base_url('admin/ijin?ijin_id=') ?>" + reminder;
			if (type=="1") {
				url = "<?php echo base_url('admin/qc?lot_number_id=') ?>" + reminder;
			}else if (type=="2") {
				url = "<?php echo base_url('admin/maintainext?id=') ?>" + reminder;
			}else if (type=="3") {
				url = "<?php echo base_url('admin/drugs?id=') ?>" + btoa(reminder);
			}
			window.location.href = url;
		}

		function openModalFromNotif() {
			var id = "<?php echo $this->input->get('notif') ?>";
			console.log("CRUSH1",id)
			if(id !== '') {
				// $("#view_"+id).click();
				setTimeout(function() {
					// var tr = $('#view_' + id);
					// console.log("CRUSH2",tr)
					// if(tr.length > 0) {
					// 	console.log('ada elemen target notif');
					// 	viewData(tr);
					// } else {
					// 	console.log('tidak ada elemen target notif');
					// 	viewData(null, id);
					// }
					viewData(id);
				}, 200);
			}
		}

		function read_all_notif(){
			urlSetReadUser = "<?php echo base_url('admin/homeservice/read_all_notif') ?>";
			$.ajax({
			url: urlSetReadUser,
			type: "POST",
			dataType: 'json',
			data: {
				//id: notif.id
			},
			success: function(data) {
				loadNotification();
				
				return;
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR, textStatus, errorThrown)
			}
			});
		}
		
		function read_all_message(){
			urlSetReadUser = "<?php echo base_url('admin/announcement/read_all_message') ?>";
			$.ajax({
			url: urlSetReadUser,
			type: "POST",
			dataType: 'json',
			data: {
				//id: notif.id
			},
			success: function(data) {
				loadNotificationAnn();
				
				return;
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR, textStatus, errorThrown)
			}
			});
		}
		
		function read_all_cuti(){
			urlSetReadUser = "<?php echo base_url('admin/reqcuti/read_all_cuti') ?>";
			$.ajax({
			url: urlSetReadUser,
			type: "POST",
			dataType: 'json',
			data: {
				//id: notif.id
			},
			success: function(data) {
				loadNotifCuti();
				
				return;
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR, textStatus, errorThrown)
			}
			});
		}
		
		function read_all_doc(){
			urlSetReadUser = "<?php echo base_url('admin/doc/read_all_doc') ?>";
			$.ajax({
			url: urlSetReadUser,
			type: "POST",
			dataType: 'json',
			data: {
				//id: notif.id
			},
			success: function(data) {
				loadNotifDoc();
				
				return;
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR, textStatus, errorThrown)
			}
			});
		}
		function idleLogout() {
			var t;
			window.onload = resetTimer;
			window.onmousemove = resetTimer;
			window.onmousedown = resetTimer;  // catches touchscreen presses as well      
			window.ontouchstart = resetTimer; // catches touchscreen swipes as well 
			window.onclick = resetTimer;      // catches touchpad clicks as well
			window.onkeydown = resetTimer;   
			window.addEventListener('scroll', resetTimer, true); 
			
			var editors = CKEDITOR.instances;
			for (var x in editors) {
			  if (editors[x]) {
				var thisName = editors[x].name;
				if (editors[thisName]) {
				  editors[thisName].on('focus', function (e) {
						//console.log('reset ck 1');
						resetTimer();
				  });
				  editors[thisName].on('key', function (e) {
						//console.log('reset ck 2');
						var data = e.editor.getData();
						resetTimer();
				  });
				  //editors[thisName].on('blur', function (e) {
					//	var data = e.editor.getData();
					//	setTimeout(function () {
					//		console.log('reset ck 3');
					//		resetTimer();
					//	}, 1000);
				  //});
				}
			  }
			}
			
			function logout() {
				window.location.href = '<?=base_url()?>admin/dashboard/logout';
				//console.log('LOGOUT');
			}

			function resetTimer() {
				//console.log('reset');
				clearTimeout(t);
				t = setTimeout(logout, 900000);  // in milliseconds
			}
		}
		idleLogout();
		</script>
		<style>
		/*bug fixing jquery ui on modal*/

		.ui-front {
			z-index: 9999;
		}
		</style>


			<!-- start modal chart -->

			<div class="modal fade" id="modal_chartjs" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-xl" style="width: 90vw">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Chart</h4>
							<br>
							<h5 id="test-name"></h5>
							<h5 id="lot-number"></h5>
							<h5 id="branch-name"></h5>
						</div>
						<div class="modal-body" style="width:90%;margin-left: 25px;padding: 0px !important;">
							
						</div>
						<div class="modal-footer">
							<!-- <input class="btn btn-primary btn-sm" type="button" onclick="saveHasil(this)" value="save" id="btn_save"> -->
							<!-- <input class="btn btn-danger btn-sm" type="button" data-dismiss="modal" value="Batal"> -->
							<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			
			<div class="modal fade" id="modal_suhu_alert" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-xl" style="width: 90vw">
					<div class="modal-content" style="background-color:white;">
						<div class="modal-header">
							<h4 class="modal-title" style="background-color:white;">Informasi Suhu Panas Karyawan</h4>
							<br>
							
						</div>
						<div class="modal-body" style="width:90%;margin-left: 25px;padding: 0px !important;text-align: center;">
							<h5 id="suhu-foto"></h5>
							Suhu : <h5 id="suhu-temp"></h5>							
							Nama : <h5 id="suhu-nama"></h5>
						</div>
						<div class="modal-footer">
							<!-- <input class="btn btn-primary btn-sm" type="button" onclick="saveHasil(this)" value="save" id="btn_save"> -->
							<!-- <input class="btn btn-danger btn-sm" type="button" data-dismiss="modal" value="Batal"> -->
							<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			

			<!-- end modal chart -->

		</section>


		<script type="text/javascript">
			var base_url = "<?= base_url() ?>";
		</script>

		<script src="<?php echo base_url() ?>assets/js/qc_chart.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/chartjs/Chart.js"></script>

		<?php flush(); ?>

		<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script> -->

	</body>
</html>