<?php

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=dailyreport.xls");
header("Pragma: no-cache");
header("Expires: 0");

?>
<p>Data Belum Report</p>
<table border="1" width="50%">
    <thead>
        <tr>
            <th>No</th>
            <th>NIP</th>
            <th>Username</th>
            <th>Name</th>
            <th>Department</th>
            <th>Branch</th>
            <th>Pada Tanggal</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($belum_reports as $key => $belum_report): ?>
            <tr>
                <td><?= $key + 1 ?></td>
                <td><?= $belum_report['NIP'] ?></td>
                <td><?= $belum_report['username'] ?></td>
                <td><?= $belum_report['name'] ?></td>
                <td><?= $belum_report['department_name'] ?></td>
                <td><?= $belum_report['branch_name'] ?></td>
                <td><?= $belum_report['date'] ?></td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>
