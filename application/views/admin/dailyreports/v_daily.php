
<?php
	$notification = $this->input->get('notif');
	if (!empty($notification)) {
		$id_user_temp = $this->session->userdata('id_user');
		$id_user = $this->session->userdata('id_user');
		$id_bagian = $this->session->userdata('id_bagian');
		$id_position = $this->session->userdata('id_position');
        $status = $this->session->userdata('STATUS');
	}else{
		$id_user_temp = $this->session->userdata('id_user');
		$id_user = $this->session->userdata('id_user');
		$id_bagian = $this->session->userdata('id_bagian');
		$id_position = $this->session->userdata('id_position');
        $status = $this->session->userdata('STATUS');
	}
?>

<!-- main-content opened -->
<div class="main-content horizontal-content">
    <!-- container opened -->
    <div class="container">
        <!-- Add Daily Reports -->
        <div class="modal fade" id="add" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="add">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" onclick="save_draft();"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="add_title">ADD DAILY REPORT</h4>
                    </div>
                    <div class="modal-body">
                        <form action="#" enctype="multipart/form-data" id="form_add">
                            <input type="hidden" name="id_dailyreports">
                            <div class="form-content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label">Sent To Position</label>
                                            <select name="position[]" class="form-control select2" id="senttopos" style="width: 100%" multiple="multiple" placeholder="Choose Position...">
                                                <?php
                                                foreach ($list_position as $key => $pos) {
                                                    echo '<option value="' . $pos->id_position . '">' . $pos->name_position . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Sent To Department</label>
                                            <select name="department[]" class="form-control select2" id="senttodept" style="width: 100%" multiple="multiple" placeholder="Choose Department...">
                                                <?php
                                                foreach ($list_department as $key => $dept) {
                                                    echo '<option value="' . $dept->id_department . '">' . $dept->name . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Sent To Bagian</label>
                                            <select name="bagian[]" class="form-control select2" id="senttobag" style="width: 100%" multiple="multiple" placeholder="Choose Bagian...">
                                                <!-- <option value="">Pilih Department</option> -->
                                                <?php
                                                foreach ($list_bagian as $key => $bg) {
                                                    echo '<option value="' . $bg->id . '">' . $bg->name . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Sent To Users</label>
                                            <!-- <select name="to_id[]" class="form-control search_user" id="senttouser" multiple="multiple" style="width: 100%"></select> -->
                                            
                                            <select name="to_id[]" class="form-control select2 search_user_" id="senttouser" style="width: 100%" multiple="multiple" placeholder="">
                                                <?php
                                                    foreach ($users_drpj as $key => $user) {
                                                        echo '<option value="' . $user['id_user'] . '">' . $user['name'] . " " . $user['last_name'] . '</option>';
                                                }?>
                                            </select>
                                            
                                            <input type="text" require name="<?php echo $model ?>[to_id]" class="form-control hidden">
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Date</label>
                                            <input type="date" name="<?php echo $model ?>[date]" value="<?php echo date('Y-m-d') ?>" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Subject</label>
                                            <input type="text" name="<?php echo $model ?>[title]" value="Daily Report <?php echo date('d-M-y') ?>" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Category</label>
                                            <select name="<?php echo $model ?>[kategori]" class="form-control">
                                                <option value="Urgent">Urgent</option>
                                                <option value="Report">Report</option>
                                                <option value="Follow Up">Follow Up</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Lampiran</label>
                                            <input class="form-control" type="file" id="file" name="files[]" multiple />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="dailyReport1">
                                            <h4 class="card-inside-title">Pelayanan berjalan dengan baik ?</h4>
                                            <input type="radio" name="<?php echo $model ?>[pelayanan_des8]" value="8" id="radio_18" checked data-toggle="collapse" data-target="#pelayanandes" />
                                            <label for="radio_18">Yes</label>
                                            <input name="<?php echo $model ?>[pelayanan_des8]" type="radio" id="radio_19" data-toggle="collapse" data-target="#pelayanandes" />
                                            <label for="radio_19">No</label>
                                            <div class="collapse" id="pelayanandes">
                                                <div class="card card-body">
                                                    <textarea id="editor12" name="<?php echo $model ?>[pelayanan_des]"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" id="dailyReport2">
                                            <h4 class="card-inside-title">Absensi Keterlambatan/Ijin/Lembur berjalan dengan baik ?</h4>
                                            <input type="radio" name="<?php echo $model ?>[absensi_des1]" value="1" id="radio_3" checked data-toggle="collapse" data-target="#absendes" />
                                            <label for="radio_3">Yes</label>
                                            <input name="<?php echo $model ?>[absensi_des1]" type="radio" id="radio_4" data-toggle="collapse" data-target="#absendes" />
                                            <label for="radio_4">No</label>
                                            <div class="collapse" id="absendes">
                                                <div class="card card-body">
                                                    <textarea id="editor5" name="<?php echo $model ?>[absensi_des]"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group form-float" id="dailyReport3">
                                            <h4 class="card-inside-title">Registrasi berjalan dengan baik ?</h4>
                                            <input type="radio" name="<?php echo $model ?>[reg_des2]" value="2" id="radio_5" checked data-toggle="collapse" data-target="#regdes" />
                                            <label for="radio_5">Yes</label>
                                            <input name="<?php echo $model ?>[reg_des2]" type="radio" id="radio_6" data-toggle="collapse" data-target="#regdes" />
                                            <label for="radio_6">No</label>
                                            <div class="collapse" id="regdes">
                                                <div class="card card-body">
                                                    <textarea id="editor6" name="<?php echo $model ?>[reg_des]"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group form-float" id="dailyReport4">
                                            <h4 class="card-inside-title">Sampling berjalan dengan baik ?</h4>
                                            <div class="demo-radio-button">
                                                <input type="radio" name="<?php echo $model ?>[sampling_des3]" value="3" id="radio_7" checked data-toggle="collapse" data-target="#samplingdes" />
                                                <label for="radio_7">Yes</label>
                                                <input name="<?php echo $model ?>[sampling_des3]" type="radio" id="radio_8" data-toggle="collapse" data-target="#samplingdes" />
                                                <label for="radio_8">No</label>
                                                <div class="collapse" id="samplingdes">
                                                    <div class="card card-body">
                                                        <textarea id="editor7" name="<?php echo $model ?>[sampling_des]"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-float" id="dailyReport5">
                                                <h4 class="card-inside-title">QC berjalan dengan baik ?</h4>
                                                <div class="demo-radio-button">
                                                    <input type="radio" name="<?php echo $model ?>[qc_des4]" value="4" id="radio_9" checked data-toggle="collapse" data-target="#qcdes" />
                                                    <label for="radio_9">Yes</label>
                                                    <input name="<?php echo $model ?>[qc_des4]" type="radio" id="radio_10" data-toggle="collapse" data-target="#qcdes" />
                                                    <label for="radio_10">No</label>
                                                    <div class="collapse" id="qcdes">
                                                        <div class="card card-body">
                                                            <textarea id="editor8" name="<?php echo $model ?>[qc_des]"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group form-float" id="dailyReport6">
                                                    <h4 class="card-inside-title">Alat berfungsi baik ?</h4>
                                                    <input type="radio" name="<?php echo $model ?>[alat_des5]" value="5" id="radio_11" checked data-toggle="collapse" data-target="#alatdes" />
                                                    <label for="radio_11">Yes</label>
                                                    <input name="<?php echo $model ?>[alat_des5]" type="radio" id="radio_12" data-toggle="collapse" data-target="#alatdes" />
                                                    <label for="radio_12">No</label>
                                                    <div class="collapse" id="alatdes">
                                                        <div class="card card-body">
                                                            <textarea id="editor9" name="<?php echo $model ?>[alat_des]"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group form-float" id="dailyReport7">
                                                    <h4 class="card-inside-title">Ketepatan janji hasil berjalan dengan baik ?</h4>
                                                    <input type="radio" name="<?php echo $model ?>[janji_des6]" value="6" id="radio_13" checked data-toggle="collapse" data-target="#janjides" />
                                                    <label for="radio_13">Yes</label>
                                                    <input name="<?php echo $model ?>[janji_des6]" type="radio" id="radio_14" data-toggle="collapse" data-target="#janjides" />
                                                    <label for="radio_14">No</label>
                                                    <div class="collapse" id="janjides">
                                                        <div class="card card-body">
                                                            <textarea id="editor10" name="<?php echo $model ?>[janji_des]"></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group form-float" id="dailyReport8">
                                                    <h4 class="card-inside-title">Lain - lain</h4>
                                                    <input type="radio" name="<?php echo $model ?>[lain_des7]" value="7" id="radio_15" checked data-toggle="collapse" data-target="#laindes" />
                                                    <label for="radio_15">Yes</label>
                                                    <input name="<?php echo $model ?>[lain_des7]" type="radio" id="radio_16" data-toggle="collapse" data-target="#laindes" />
                                                    <label for="radio_16">No</label>
                                                    <div class="collapse" id="laindes">
                                                        <div class="card card-body">
                                                            <textarea id="editor11" name="<?php echo $model ?>[lain_des]"></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group form-float" id="dailyReport9">
                                                    <h4 class="card-inside-title">Jam buka pelayanan?</h4>
                                                    <div class="input-group">
                                                        <input type="number" id="hours" class="form-control col-md-6" min="00" max="23" onkeyup="angka(this);"> 
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                :
                                                            </span>
                                                        </div>
                                                        <input type="number" id="minutes" class="form-control col-md-6" min="00" max="59" onkeyup="angka(this);">
                                                        <input type="hidden" class="form-control col-md-6" id="jam_buka" name="<?php echo $model ?>[jam_buka]" value="" autocomplete="off">
                                                        <div class="input-group-append" title="hapus">
                                                            <span class="input-group-text" id="clearTimePicker" style="cursor: pointer;">
                                                            <i class="fa fa-times-circle text-danger"></i>
                                                        </span>
                                                    </div> 
                                                </div>
                                            </div>

                                         <div class="form-group" id="dailyReport10">
                                            <label class="form-label">Upload file jam buka</label>
                                            <input class="form-control" type="file" id="fileJ" name="filesJ[]" multiple />
                                         </div>





                                            </div>
                                            <center>
                                                <input id="button_save" type="button" onclick="save_v2()" value="SEND" data-dismiss="modal" data-backdrop="false">
                                            </center>
                                            <div class="col-sm-12" style="<?= $show_daily == true ? '' : 'display: none;' ?>"></div>
                                        </div>
                                        <div class="col-sm-12" style="<?= $show_daily == true ? '' : 'display: none;' ?>"></div>
                                    </div>
                                    <div class="col-sm-12" style="<?= $show_daily == true ? '' : 'display: none;' ?>"></div>
                                </div>
                                <div class="col-sm-12" style="<?= $show_daily == true ? '' : 'display: none;' ?>"></div>
                            </div>
                            <input hidden="true" name="<?php echo $model ?>[status]" value="Open" type="radio" checked id="status_open" />
                            <div class="modal-footer">
                                <button type="button" class="close" data-dismiss="modal" onclick="save_draft();"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add End Daily Reports -->

        <!-- Inbox area Start -->
        <div class="row row-sm main-content-mail">
            <div class="col-xl-3 col-lg-4 col-md-12">
                <div class="card mg-b-20 mg-md-b-0">
                    <div class="card-body">
                        <div class="main-content-left main-content-left-mail">
                            <a style="color:white" class="btn btn-success btn-with-icon btn-block btn-compose" id="compose" data-toggle="modal" data-target="#add" onclick="resetForm();">Compose</a>
                            <!-- main-mail-menu start-->
                            <div class="main-mail-menu">
                                <nav class="nav main-nav-column mg-b-20">
                                    <ul class="nav-tabs" role="tablist">
                                        <a class="nav-link active inbox" href="#received" aria-controls="received" role="tab" data-toggle="tab"><i class="typcn typcn-mail"></i> Inbox</a>
                                        <a class="nav-link sent" href="#sent" aria-controls="sent" role="tab" data-toggle="tab"><i class="typcn typcn-arrow-forward-outline"></i> Sent </a>
                                        <a class="nav-link archived" href="#archived" aria-controls="archived" role="tab" data-toggle="tab"><i class="typcn typcn-pen"></i> Archived </a>
                                        <a class="nav-link draft" href="#draft" aria-controls="draft" role="tab" data-toggle="tab"><i class="typcn typcn-folder"></i> Draft </a>
                                    </ul>
                                </nav>
                            </div>
                            <!-- main-mail-menu end -->
                        </div>
                    </div>
                </div>
            </div>

            <!-- tab inbox start -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="received">
                    <input type="hidden" id="page" value="1">
                    <input type="hidden" id="last_page" value="<?php echo (!empty($received['row'])) ? $received['row'] : 1; ?>">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="inbox-text-list sm-res-mg-t-30">
                            <div class="form-group">
                                <form action="<?php echo base_url('admin/dailyreports') ?>" method="get">
                                    <div class="nk-int-st search-input search-overt">
                                        <input type="text" class="form-control" name="search" id="search-inbox" value="<?php echo $this->input->get('search') ?>" placeholder="Search daily report..." />
                                        <button type="button" class="btn search-ib search-inbox">Search</button>
                                </form>
                            </div>
                        </div>
                        <div class="inbox-btn-st-ls btn-toolbar">
                            <div class="btn-group ib-btn-gp active-hook nk-email-inbox">
                                <button class="btn btn-default btn-sm" onClick="history.go(0)"><i class="notika-icon notika-refresh"></i> Refresh</button>
                                <button class="btn btn-default btn-sm" onclick="deleteDataReceived();"><i class="notika-icon notika-trash"></i></button>
                            </div>
                            <div style="margin-left: auto" class="pagination-inbox">
                                <div id='pagination'><?php echo (!empty($received['pagination'])) ? $received['pagination'] : ''; ?></div>
                            </div>
                            <?php if (!empty($received['result'])) { ?>
                                <?php $i = 0; ?>
                                <div class="table-responsive inbox-cont">
                                    <?php foreach ($received['result'] as $key => $value) {
                                        $i++;
                                    ?>
                                        <table class="table table-inbox table-hover" index="<?= $i ?>" style="<?= $i > 10 ? 'display: none;' : ''; ?>">
                                            <tbody>
                                                <tr style="cursor: pointer;text-align: justify" class="row-report" id="view_<?php echo $value['id'] ?>" data-togglee="modal" data-target="#view" data-id="<?php echo $value['id'] ?>" data-url="received">
                                                    <td width="1%">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input filled-in pull-left delete_data" value="<?php echo $value['id'] ?>" id="ig_checkbox_a_<?php echo $value['id'] ?>">
                                                            <label class="custom-control-label pull-left" for="ig_checkbox_a_<?php echo $value['id'] ?>"></label>
                                                        </div>
                                                    </td>
                                                    <td width="10%">
                                                        <img class="b-lazy" data-src="<?php echo base_url() . 'uploads/avatar/' . $value['photo_profile']; ?>" alt="sender" />
                                                    </td>
                                                    <td width="30%">
                                                        <?php echo $value['from'] ?> (<?php echo $value['branch'] ?>)
                                                    </td>
                                                    <!-- <td style="display: none" width="1%"></td> -->
                                                    <td width="10%">
                                                        <!-- <button type="button" style="padding: 6px 4px;" class="btn btn-success" data-toggle="modal" id="count_<?php echo $value['id'] ?>" data-id="<?php echo $value['id'] ?>" data-target="#count" onclick="getUserRead(this);">
                                                            <?php echo $value['count_read'] ?> / <?php echo $value['count_send'] ?>
                                                        </button> -->
                                                        
                                                        <button type="button" style="padding: 6px 4px;" class="btn btn-success">
                                                            <?php echo $value['count_read'] ?> / <?php echo $value['count_send'] ?>
                                                        </button>
                                                    </td>
                                                    <td width="20%">
														<?php if ($value['read'] !== '1') : ?>
															<center id="subject-marquee">
																<span>
																	<marquee> NEW! <?php echo $value['subject'] ?></marquee>
																</span>
															</center>
														<?php endif ?>
														<center id="subject" style="<?= $value['read'] == '1' ? '' : 'display: none'; ?>"><span><?php echo substr($value['subject'], 0, 30) ?> <?php echo "....." ?></span></center>
													</td> 
                                                    <td width="10%"> <span class="">
                                                            <?php echo count($value['file_attachment']) > 0 ?
                                                                '<i style="color: black" class="notika-icon notika-form"></i>'
                                                                : "" ?>
                                                        </span></td>
                                                    <td width="10%" class="text-right">
                                                        <?php echo $value['date']; ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
            <!-- tab inbox end -->

            <!-- tab sent start -->
            <div role="tabpanel" class="tab-pane fade" id="sent">
                <input type="hidden" id="page2" value="1">
                <input type="hidden" id="last_page2">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="inbox-text-list sm-res-mg-t-30">
                        <div class="form-group">
                            <form action="<?php echo base_url('admin/dailyreports') ?>" method="get">
                                <div class="nk-int-st search-input search-overt">
                                    <input type="text" class="form-control" name="search" id="search-sent" value="<?php echo $this->input->get('search') ?>" placeholder="Search daily report..." />
                                    <button type="button" class="btn search-ib search-sent">Search</button>
                            </form>
                        </div>
                    </div>
                    <div class="inbox-btn-st-ls btn-toolbar">
                        <div class="btn-group ib-btn-gp active-hook nk-email-inbox">
                            <button class="btn btn-default btn-sm" onClick="history.go(0)"><i class="notika-icon notika-refresh"></i> Refresh</button>
                            <button class="btn btn-default btn-sm" onclick="deleteData();"><i class="notika-icon notika-trash"></i></button>
                        </div>
                        <div style="margin-left: auto" class="pagination-inbox">
                            <div id='pagination2'><?php echo (!empty($sent['pagination'])) ? $sent['pagination'] : ''; ?></div>
                        </div>
                        <?php if (!empty($sent['result'])) { ?>
                            <?php $i = 0; ?>
                            <div class="table-responsive sent-cont">
                                <?php foreach ($sent['result'] as $key => $value) {
                                    $i++;
                                ?>
                                    <table class="table table-inbox table-hover" index="<?= $i ?>" style="<?= $i > 10 ? 'display: none;' : ''; ?>">
                                        <tbody>
                                            <tr style="cursor: pointer;text-align: justify;margin-left: -100px;" class="row-report" id="view_<?php echo $value['id'] ?>" data-togglee="modal" data-target="#view" data-id="<?php echo $value['id'] ?>" data-url="sent">
                                                <td width="1%">
                                                    <div class="custom-control custom-checkbox" style="margin-left: -20px">
                                                        <a style="font-size: 20px"> <i style="color: black" id="edit_<?php echo $value['id'] ?>" class="lar la-edit" data-toggle="modal" data-target="#add" data-id="<?php echo $value['id'] ?>" data-url="sent" onclick="editData(this);"></i></a>
                                                        <input type="checkbox" class="custom-control-input filled-in pull-left delete_data" value="<?php echo $value['id'] ?>" id="ig_checkbox_a_<?php echo $value['id'] ?>">
                                                        <label class="custom-control-label pull-left" for="ig_checkbox_a_<?php echo $value['id'] ?>"></label>
                                                    </div>
                                                </td>
                                                <td width="10%">
                                                    <img class="b-lazy" data-src="<?php echo base_url() . 'uploads/avatar/' . $value['photo_profile']; ?>" alt="sender" />
                                                </td>
                                                <td width="30%">
                                                    <?php echo $value['from'] ?> (<?php echo $value['branch'] ?>)
                                                </td>
                                                <!-- <td style="display: none" width="1%"></td> -->
                                                <td width="10%">
                                                    <!-- <button type="button" style="padding: 6px 4px;" class="btn btn-success" data-toggle="modal" id="count_<?php echo $value['id'] ?>" data-id="<?php echo $value['id'] ?>" data-target="#count" onclick="getUserRead(this);">
                                                        <?php echo $value['count_read'] ?> / <?php echo $value['count_send'] ?>
                                                    </button> -->
                                                    <button type="button" style="padding: 6px 4px;" class="btn btn-success">
                                                        <?php echo $value['count_read'] ?> / <?php echo $value['count_send'] ?>
                                                    </button>
                                                </td>
                                                <td width="20%">
                                                    <center><span><?php echo $value['subject'] ?></span></center>
                                                </td>
                                                <td width="10%"> <span class="">
                                                        <?php echo count($value['file_attachment']) > 0 ?
                                                            '<i style="color: black" class="notika-icon notika-form"></i>'
                                                            : "" ?>
                                                    </span></td>
                                                <td width="10%" class="text-right">
                                                    <?php echo $value['date']; ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <br>
            </div>
            <!-- tab sent end -->
        </div>

        <!-- tab archived start -->
        <div role="tabpanel" class="tab-pane fade" id="archived">
            <input type="hidden" id="page3" value="1">
            <input type="hidden" id="last_page3">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="inbox-text-list sm-res-mg-t-30">
                    <div class="form-group">
                        <form action="<?php echo base_url('admin/dailyreports') ?>" method="get">
                            <div class="nk-int-st search-input search-overt">
                                <input type="text" class="form-control" name="search" id="search-archived" value="<?php echo $this->input->get('search') ?>" placeholder="Search daily report..." />
                                <button type="button" class="btn search-ib search-archived">Search</button>
                        </form>
                    </div>
                </div>
                <div class="inbox-btn-st-ls btn-toolbar">
                    <div class="btn-group ib-btn-gp active-hook nk-email-inbox">
                        <button class="btn btn-default btn-sm" onClick="history.go(0)"><i class="notika-icon notika-refresh"></i> Refresh</button>
                        <button class="btn btn-default btn-sm" onclick="deleteDataReceived();"><i class="notika-icon notika-trash"></i></button>
                    </div>
                    <div style="margin-left: auto" class="pagination-inbox">
                        <div id='pagination3'><?php echo (!empty($archived['pagination'])) ? $archived['pagination'] : ''; ?></div>
                    </div>
                    <?php if (!empty($archived['result'])) { ?>
                        <?php $i = 0; ?>
                        <div class="table-responsive archived-cont">
                            <?php foreach ($archived['result'] as $key => $value) {
                                $i++;
                            ?>
                                <table class="table table-inbox table-hover" index="<?= $i ?>" style="<?= $i > 10 ? 'display: none;' : ''; ?>">
                                    <tbody>
                                        <tr style="cursor: pointer;text-align: justify;margin-left: -100px;" class="row-report" id="view_<?php echo $value['id'] ?>" data-togglee="modal" data-target="#view" data-id="<?php echo $value['id'] ?>" data-url="archived">
                                            <td width="1%">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input filled-in pull-left delete_data" value="<?php echo $value['id'] ?>" id="ig_checkbox_a_<?php echo $value['id'] ?>">
                                                    <label class="custom-control-label pull-left" for="ig_checkbox_a_<?php echo $value['id'] ?>"></label>
                                                </div>
                                            </td>
                                            <td width="10%">
                                                <img class="b-lazy" data-src="<?php echo base_url() . 'uploads/avatar/' . $value['photo_profile']; ?>" alt="sender" width="50" height="50" />
                                            </td>
                                            <td width="30%">
                                                <?php echo $value['from'] ?> (<?php echo $value['branch'] ?>)
                                            </td>
                                            <!-- <td style="display: none" width="1%"></td> -->
                                            <!-- <td class="col-sm-1">
                                                <button type="button" class="btn btn-default" data-toggle="modal" id="count_<?php echo $value['id'] ?>" data-id="<?php echo $value['id'] ?>" data-target="#count" onclick="getUserRead(this);">
                                                    <?php echo $value['count_read'] ?> / <?php echo $value['count_send'] ?>
                                                </button>
                                            </td> -->
                                            <td width="10%">
                                                <button type="button" style="padding: 6px 4px;" class="btn btn-success">
                                                    <?php echo $value['count_read'] ?> / <?php echo $value['count_send'] ?>
                                                </button>
                                            </td>
                                            <td width="20%">
                                                <center>
                                                    <span>
                                                        <?php echo $value['subject'] ?>
                                                    </span>
                                                </center>
                                            </td>
                                            <td width="10%">
                                                <span class="">
                                                    <?php echo count($value['file_attachment']) > 0 ?
                                                        '<i style="color: black" class="notika-icon notika-form"></i>'
                                                        : "" ?>
                                                </span>
                                            </td>
                                            <td width="10%" class="">
                                                <?php echo $value['date']; ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <br>
        </div>
        <!-- tab archived end -->
    </div>

    <!-- tab draft start -->
    <div role="tabpanel" class="tab-pane fade" id="draft">
        <input type="hidden" id="page4" value="1">
        <input type="hidden" id="last_page4">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="inbox-text-list sm-res-mg-t-30">
                <div class="form-group">
                    <form action="<?php echo base_url('admin/dailyreports') ?>" method="get">
                        <div class="nk-int-st search-input search-overt">
                            <input type="text" class="form-control" name="search" id="search-draft" value="<?php echo $this->input->get('search') ?>" placeholder="Search daily report..." />
                            <button type="button" class="btn search-ib search-draft">Search</button>
                    </form>
                </div>
            </div>
            <div class="inbox-btn-st-ls btn-toolbar">
                <div class="btn-group ib-btn-gp active-hook nk-email-inbox">
                    <button class="btn btn-default btn-sm" onClick="history.go(0)"><i class="notika-icon notika-refresh"></i> Refresh</button>
                    <button class="btn btn-default btn-sm" onclick="deleteData('draft');"><i class="notika-icon notika-trash"></i></button>
                </div>
                <div style="margin-left: auto" class="pagination-inbox">
                    <div id='pagination4'><?php echo (!empty($draft['pagination'])) ? $draft['pagination'] : ''; ?></div>
                </div>
                <?php if (!empty($draft['result'])) { ?>
                    <?php $i = 0; ?>
                    <div class="table-responsive draft-cont">
                        <?php foreach ($draft['result'] as $key => $value) {
                            $i++;
                        ?>
                            <table class="table table-inbox table-hover" index="<?= $i ?>" style="<?= $i > 10 ? 'display: none;' : ''; ?>">
                                <tbody>
                                    <tr style="cursor: pointer;text-align: justify;margin-left: -100px;" class="row-report" id="view_<?php echo $value['id'] ?>" data-togglee="modal" data-target="#view" data-id="<?php echo $value['id'] ?>" data-url="draft">
                                        <td width="1%">
                                            <div class="custom-control custom-checkbox" style="margin-left: -20px">
                                                <a style="font-size: 20px"> <i style="color: black" id="edit_<?php echo $value['id'] ?>" class="lar la-edit" data-toggle="modal" data-target="#add" data-id="<?php echo $value['id'] ?>" data-url="draft" onclick="editData(this);"></i></a>
                                                <input type="checkbox" class="custom-control-input filled-in pull-left delete_data" value="<?php echo $value['id'] ?>" id="ig_checkbox_a_<?php echo $value['id'] ?>">
                                                <label class="custom-control-label pull-left" for="ig_checkbox_a_<?php echo $value['id'] ?>"></label>
                                            </div>
                                        </td>
                                        <td width="10%">
                                            <img class="b-lazy" data-src="<?php echo base_url() . 'uploads/avatar/' . $value['photo_profile']; ?>" alt="sender" width="50" height="50" />
                                        </td>
                                        <td width="30%">
                                            <?php echo $value['from'] ?> (<?php echo $value['branch'] ?>)
                                        </td>
                                        <!-- <td style="display: none" width="1%"></td> -->
                                        <!-- <td class="col-sm-1">
                                            <button type="button" style="padding: 6px 4px;" class="btn btn-default" data-toggle="modal" id="count_<?php echo $value['id'] ?>" data-id="<?php echo $value['id'] ?>" data-target="#count" onclick="getUserRead(this);">
                                                <?php echo $value['count_read'] ?> / <?php echo $value['count_send'] ?>
                                            </button>
                                        </td> -->
                                        <td width="10%">
                                            <button type="button" style="padding: 6px 4px;" class="btn btn-success">
                                                <?php echo $value['count_read'] ?> / <?php echo $value['count_send'] ?>
                                            </button>
                                        </td>
                                        <td width="20%">
                                            <center>
                                                <span>
                                                    <?php echo $value['subject'] ?>
                                                </span>
                                            </center>
                                        </td>
                                        <td width="10%">
                                            <span class="">
                                                <?php echo count($value['file_attachment']) > 0 ?
                                                    '<i style="color: black" class="notika-icon notika-form"></i>'
                                                    : "" ?>
                                            </span>
                                        </td>
                                        <td width="10%" class="">
                                            <?php echo $value['date']; ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
        <br>
    </div>
    <!-- tab draft end -->
</div>
</div>
</div>
</div>
</div>
</div>

<!-- View modal start -->
<div class="modal fade" id="view" tabindex="-1" role="dialog" style="padding: 50px">
    <div class="modal-dialog modal-lg" role="document">
        <div align="center" class="modal-content">
            <div class="modal-header">
                <input type="hidden" id="id" name="id" />
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h1><span align="center" id="subject"></span></h1> <b><span>Created : <span id="date"></span></span></b>
            </div>
            <a id="photo_profile">
                <img class="rounded mCS_img_loaded" id="photo_profile" src="" width="150" height="150" alt="sender" />
            </a>
            <div class="media-body"> 
                <span class="media-heading">
                    <b><span id="from"></span>&nbsp;<span id="from2"></span>&nbsp;(<span id="position"></span>&nbsp;-&nbsp;<span id="branch"></span>)</b>
                    <br> <span class="media-heading">
                        To : <span id="sent_to"></span> </span>
                    <div style="<?= $show_close == true ? '' : 'display: none;' ?>" class="switch">
                        <span class="badge badge-danger" id="kategori"></span>
                        <label class="badge badge-success"> Status
                            <input type="checkbox" id="status" onchange="ubah_status(this)"> <span class="lever"></span> <span id="status_name">buka</span> </label>
                    </div>

                    <!-- <div class="switch">
						<label> Status
							<input type="checkbox" id="status" onchange="ubah_status(this)"> <span class="lever"></span> <span id="status_name">buka</span> </label>
					</div> -->
            </div>
            <div class="col-sm-12">
                <div class="col-md-12 col-lg-12">
                    <table class="table table-bordered">
                        <tbody>
                            <tr class="row_daily">
                                <td>Pelayanan</td>
                                <td id="pelayanan_des"></td>
                            </tr>
                            <tr class="row_absensi">
                                <td>Absensi Karyawan</td>
                                <td id="absensi_des"></td>
                            </tr>
                            <tr class="row_daily">
                                <td>Registrasi</td>
                                <td id="reg_des"></td>
                            </tr>
                            <tr class="row_daily row_drpj">
                                <td>Sampling</td>
                                <td id="sampling_des"></td>
                            </tr>
                            <tr class="row_daily row_drpj">
                                <td>QC</td>
                                <td id="qc_des"></td>
                            </tr>
                            <tr class="row_daily row_drpj">
                                <td>Alat</td>
                                <td id="alat_des"></td>
                            </tr>
                            <tr class="row_daily">
                                <td>Ketepatan janji hasil</td>
                                <td id="janji_des"></td>
                            </tr>
                            <tr class="row_daily">
                                <td>Lain Lain</td>
                                <td id="lain_des"></td>
                            </tr>
                            <tr class="row_support">
                                <td>File Attachment</td>
                                <td id="file_attachment"></td>
                            </tr>
                            <tr class="row_daily">
                                <td>Jam Buka</td>
                                <td id="jam_buka_"></td>
                            </tr>
                            <tr class="row_support">
                                <td>File Attachment Jam Buka</td>
                                <td id="file_attachment_jam_buka"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="container mt-5">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-11">
                        <div class="card p-3">
                            <span class="user_replay"></span>
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="col-sm-12"> <span class="user_replay"></span> </div> -->
            <!-- /row -->
            <!-- <div class="modal-body"> -->
            <div class="">
                <!--<div class="row clearfix">-->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="inbox-btn-st-ls btn-toolbar">
                                <div class="btn-group ib-btn-gp active-hook nk-email-inbox">
                                    <button class="btn btn-default btn-sm waves-effect" onClick="history.go(0)"><i class="notika-icon notika-refresh"></i> Refresh</button>
                                    <button class="btn btn-default btn-sm waves-effect collapsed" data-toggle="collapse" href="#collapseTwo_20" aria-expanded="false" aria-controls="collapseTwo_20"><i class="notika-icon notika-next"></i>Reply</button>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="panel panel-col-cyan">
                                <div class="panel-heading" role="tab" id="headingTwo_20"> </div>
                                <form id="replay_form">
                                    <input type="hidden" name="id_dailyreports_replay">
                                    <input type="hidden" name="id_replay">
                                    <div id="collapseTwo_20" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_20">
                                        <div class="panel-body">
                                            <textarea id="editor3" name="message_reply"></textarea>
                                            <br>
                                            <center>
                                                <button type="button" class="btn btn-success" onclick="sendReplay();">SEND</button>
                                            </center>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# CKEditor -->
            <!-- </div> -->
            <div class="modal-footer">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            </div>
        </div>
    </div>
</div>
<!-- view modal end -->
<div class="modal fade" id="count" tabindex="-1" role="dialog" style="padding: 50px">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="count">Sent To</h4>
            </div>
            <div class="modal-body">
                <ol> <span id="user_received"></span> </ol>
            </div>
            <div class="modal-header">
                <h4 class="modal-title" id="count">Already Reading</h4>
            </div>
            <div class="modal-body">
                <ol> <span id="user_read"></span> </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            </div>
        </div>
    </div>
</div>

<!-- data table -->
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<script>
    // CKEDITOR.replace('editor13');
    var base_url = '<?= base_url() ?>';

  $('#hours').blur(function(){
  	generateTime();
  });
  $('#minutes').blur(function(){
  	generateTime();
  });

  $('#clearTimePicker').click(function(){
		clearTimePicker('jam_buka');
		clearTimePicker('hours');
		clearTimePicker('minutes');
	});

	function generateTime(){
		hours = $('#hours').val();
		minutes = $('#minutes').val();
		/*cek recreate*/
		hours = twoBit(batasAngka(hours,'jam'));
		minutes = twoBit(batasAngka(minutes,'menit'));
		picktime = hours+':'+minutes;
		$('#jam_buka').val(picktime);
		$('#hours').val(hours);
		$('#minutes').val(minutes);
	}

	function batasAngka(angka,jenis){
		if (jenis == 'jam') {
			if (angka > 23) {
				$('#hours').val('00');
				angka = '0';
			}
		}else{
			if (angka > 59) {
				$('#minutes').val('00');
				angka = '0';
			}
		}
		return angka;
	}
	function twoBit(num){
		num = (num.length>0)?parseInt(num):0;
		return num >= 10 ? num + '' : '0' + num;
	}

	function angka(e) {
	  if (!/^[0-9]+$/.test(e.value)) {
	    e.value = e.value.substring(0,e.value.length-1);
	  }
	}

	function clearTimePicker(selector=''){
		$('#'+selector).val('');
	}

    
    $(document).ready(function() {
        setTimeout(function() {
            $("#inbox").addClass("tab-pane");
            $("#sent").addClass("tab-pane");
            $("#archived").addClass("tab-pane");
            $("#draft").addClass("tab-pane");
        }, 1000);
        var modal_add = $('div#add');
        $('#add .select2').each(function(index, node) {
            node = $(node);
            node.select2({
                dropdownParent: modal_add,
                placeholder: node.attr('placeholder')
            });
        });
        $('#add .select2-search__field').css({
            width: '200px'
        });
        $(document).on('click', '.row-report td', function(ev) {
            //$('.row-report td').click(function(ev) {
            // console.log('td clicked', ev);
            var td = $(ev.currentTarget);
            // console.log('node', node);
            var tr = td.parent();
            var tds = tr.children();
            var index = tds.index(td);
            console.log('tds', index, td, tds);
            if (index == 0) {

            } 
            else if(index == 3){
                getUserRead(tr);
            }
            else {
                // viewData(tr);
                let x = (tr[0].id).replace('view_', '');
                viewData(x,tr);
                console.log("kasjhdkjashdjk",(tr[0].id).replace('view_', ''));
            }
        });
        initCK();

        $('#pagination').on('click', 'a', function(e) {
            e.preventDefault();
            var pageNum = $(this).attr('data-ci-pagination-page');
            createPagination(pageNum);
        });

        $('.search-inbox').click(function(e) {
            e.preventDefault();
            if ($('#search-inbox').val() != '') createPagination(1);
        });

        $('#search-inbox').keypress(function(e) {
            if (e.which == 13) {
                e.preventDefault();
                if ($('#search-inbox').val() != '') createPagination(1);
            }
        });

        $('#pagination2').on('click', 'a', function(e) {
            e.preventDefault();
            var pageNum = $(this).attr('data-ci-pagination-page');
            createPagination2(pageNum);
        });

        $('.search-sent').click(function(e) {
            e.preventDefault();
            if ($('#search-sent').val() != '') createPagination2(1);
        });

        $('#search-sent').keypress(function(e) {
            if (e.which == 13) {
                e.preventDefault();
                if ($('#search-sent').val() != '') createPagination2(1);
            }
        });

        $('#pagination3').on('click', 'a', function(e) {
            e.preventDefault();
            var pageNum = $(this).attr('data-ci-pagination-page');
            createPagination3(pageNum);
        });

        $('.search-archived').click(function(e) {
            e.preventDefault();
            if ($('#search-archived').val() != '') createPagination3(1);
        });

        $('#search-archived').keypress(function(e) {
            if (e.which == 13) {
                e.preventDefault();
                if ($('#search-archived').val() != '') createPagination3(1);
            }
        });

        $('#pagination4').on('click', 'a', function(e) {
            e.preventDefault();
            var pageNum = $(this).attr('data-ci-pagination-page');
            createPagination4(pageNum);
        });

        $('.search-draft').click(function(e) {
            e.preventDefault();
            if ($('#search-draft').val() != '') createPagination4(1);
        });

        $('#search-draft').keypress(function(e) {
            if (e.which == 13) {
                e.preventDefault();
                if ($('#search-draft').val() != '') createPagination4(1);
            }
        });
    });

    function initCK() {
        CKEDITOR.replace('editor3');
        CKEDITOR.replace('editor5');
        CKEDITOR.replace('editor6');
        CKEDITOR.replace('editor7');
        CKEDITOR.replace('editor8');
        CKEDITOR.replace('editor9');
        CKEDITOR.replace('editor10');
        CKEDITOR.replace('editor11');
        CKEDITOR.replace('editor12');
        CKEDITOR.replaceClass = 'ta_ckeditor';
    }

    function save(draft = false) {}

    function save_v2(draft = false) {


        // var khusus dokter PJ freelance
        // *fungsi jika nanti diperlukan/jika tidak akan dihapus
        var id_position = '<?= $id_position ?>';
        var status_karyawan = '<?= $this->status_karyawan ?>';
        var drpj_freelance = false;
        if (['13'].includes(id_position) && status_karyawan == 'FREELANCE') {
            drpj_freelance = true;
        }

        url = "<?php echo base_url('admin/dailyreports/send') ?>";
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var fd = new FormData();
        if (draft) {
            fd.append("<?php echo $model ?>[id_draft]", 1)
        } else {
            fd.append("<?php echo $model ?>[id_draft]", 0)
        }

        var file_data = $('#file')[0].files;
        for (var i = 0; i < file_data.length; i++) {
            fd.append("files[]", file_data[i]);
        }

        var file_data_ = $('#fileJ')[0].files;
        for (var i = 0; i < file_data_.length; i++) {
            fd.append("filesJ[]", file_data_[i]);
        }

        var other_data = $('form').serializeArray();
        $.each(other_data, function(key, input) {
            fd.append(input.name, input.value);
        });

        // console.log(file_data_);


        for (let [key, value] of fd) {
            console.log(`${key}: ${value}`)
        }


        if ($('#senttouser').val().length == 0 && $('#senttobag').val().length == 0 && !draft){
             swal({
                    title: 'Error',
                    text: 'Pilih Setidaknya Satu Penerima/Bagian!',
                    type: 'error',
                    allowOutsideClick: true,
                    allowEscapeKey: true,
                    showCancelButton: false,
                    showConfirmButton: true,
                })

                setTimeout(()=>{
                    $('#add').modal('show');
                }, 1000);
        }
        // *fungsi jika nanti diperlukan/jika tidak akan dihapus
        // else if ($('#jam_buka').val() == '' && !draft && drpj_freelance == false){
        else if ($('#jam_buka').val() == '' && !draft){
            swal({
                    title: 'Error',
                    text: 'Jam Buka Pelayanan Harus Diisi!',
                    type: 'error',
                    allowOutsideClick: true,
                    allowEscapeKey: true,
                    showCancelButton: false,
                    showConfirmButton: true,
                })
            
                setTimeout(()=>{
                    $('#add').modal('show');
                }, 1000);

        }else if ($('#fileJ').get(0).files.length === 0 && !draft) {
            swal({
                    title: 'Error',
                    text: 'Bukti File Jam buka Harus Diupload!',
                    type: 'error',
                    allowOutsideClick: true,
                    allowEscapeKey: true,
                    showCancelButton: false,
                    showConfirmButton: true,
                })

                setTimeout(()=>{
                    $('#add').modal('show');
                }, 1000);

        }else{

        var progress = function(loaded, total) {
            swal({
                title: 'Menyimpan data ' + Number(100 * loaded / total).toFixed(0) + '%',
                text: 'Harap tunggu',
                allowOutsideClick: false,
                allowEscapeKey: false,
                showCancelButton: false,
                showConfirmButton: false,
                type: 'info'
            });
        }
        $.ajax({
            url: url,
            type: "POST",
            data: fd,
            dataType: 'json',
            processData: false,
            contentType: false,
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener('progress', function(evt) {
                    if (evt.lengthComputable) {
                        progress(evt.loaded, evt.total);
                        // console.log('progress upload', evt.lengthComputable, evt.loaded, evt.total);
                    }
                }, false);
                // xhr.addEventListener('progress', function(evt){
                //     if(evt.lengthComputable){
                //         console.log('progress', evt.lengthComputable, evt.loaded, evt.total);
                //     }
                // },false);
                return xhr;
            },
            beforeSend: function() {
                progress(0, 100);
            },
            success: function(data) {
                if (data.status == false) {
                    swal("Kesalahan", data.message, 'error');
                } else {
                    resetForm();
                    loadNotification();
                    swal({
                        title: 'Daily Report',
                        text: data.message,
                        type: 'success',
                    }, function() {
                        location.reload();
                        $('#add').modal('hide');
                        createPagination2(1);
                        createPagination4(1);
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                resetForm();
                swal({
                    title: 'Daily Report',
                    text: 'Error',
                    type: 'error',
                });
                console.log(jqXHR, textStatus, errorThrown)
            }
        });

        }

    }

    function save_draft() {
        var id = $('input[name="id_dailyreports"]').val();
        if (id == "") {
            save_v2(true);
        }
        resetData();
    }

    function resetForm() {
        resetData();
    }

    function resetData() {
        $("input[name='id_dailyreports']").val('');
        $("input[name='<?php echo $model ?>[to_id]']").val('');
        //$("input[name='search_user']").val('').attr('id', '');
        $("select.search_user_").val(null).trigger("change");
        $("input[name='<?php echo $model ?>[title]']").attr('disabled', false);
        $("input[name='<?php echo $model ?>[date]']").attr('disabled', false);
        $("select[name='<?php echo $model ?>[kategori]']").attr('disabled', false);
        $("#file").attr('disabled', false);
        //$("input[name='search_user']").attr('disabled', false);
        $("select.search_user_").attr('disabled', false);
        $('#senttopos').attr('disabled', false);
        $('#senttodept').attr('disabled', false);
        $('#senttobag').attr('disabled', false);
        for (var i = 1; i < 16; i++) {
            $("#radio_" + i).attr('disabled', false);
        }
        CKupdate();
        // cekStatus();
    }

// *fungsi jika nanti diperlukan/jika tidak akan dihapus
function cekStatus() {

    var id_position = '<?= $id_position ?>';
    var status_karyawan = '<?= $this->status_karyawan ?>';
    console.log('<?= $this->status_karyawan ?>');
    
    if (['13'].includes(id_position) && status_karyawan == 'FREELANCE') {
        console.log("KONDISI 1")
        // $('#block_manager').show();
        $('#dailyReport1').hide();
        $('#dailyReport2').hide();
        $('#dailyReport3').hide();
        $('#dailyReport7').hide();
        $('#dailyReport8').hide();
        $('#dailyReport9').hide();
        $('#dailyReport10').hide();
    } else {
        console.log("KONDISI 2")
        $('#dailyReport1').show();
        $('#dailyReport2').show();
        $('#dailyReport3').show();
        $('#dailyReport7').show();
        $('#dailyReport8').show();
        $('#dailyReport9').show();
        $('#dailyReport10').show();
    }

}


    function CKupdate() {
        CKEDITOR.instances['editor3'].setData('');
        CKEDITOR.instances['editor5'].setData('');
        CKEDITOR.instances['editor6'].setData('');
        CKEDITOR.instances['editor7'].setData('');
        CKEDITOR.instances['editor8'].setData('');
        CKEDITOR.instances['editor9'].setData('');
        CKEDITOR.instances['editor10'].setData('');
        CKEDITOR.instances['editor11'].setData('');
        CKEDITOR.instances['editor12'].setData('');

        $("#radio_18").prop('checked', true);
        $("#radio_3").prop('checked', true);
        $("#radio_5").prop('checked', true);
        $("#radio_7").prop('checked', true);
        $("#radio_9").prop('checked', true);
        $("#radio_11").prop('checked', true);
        $("#radio_13").prop('checked', true);
        $("#radio_15").prop('checked', true);

        $("#pelayanandes").removeClass('show');
        $("#absendes").removeClass('show');
        $("#regdes").removeClass('show');
        $("#samplingdes").removeClass('show');
        $("#qcdes").removeClass('show');
        $("#alatdes").removeClass('show');
        $("#janjides").removeClass('show');
        $("#laindes").removeClass('show');
        // CKEDITOR.instances['editor12'].setData('');
    }

    function viewData(x,tr) {
        $('#view').modal('show');
        console.log('view data', x);
        obj = $(tr);
        var url = '';
        // if (obj != undefined && obj != null) {
        //     obj = $(obj);
        //     url = $(obj).data("url");
        //     id = $(obj).data("id");
        //     // console.log('id', id, 'url', url);
        // }

        id = x;

        if (id == undefined || id == null || id == '') {
            // console.log('id tidak valid', id);
            return;
        }
        $('input[name="id_dailyreports_replay"]').val(id);
        $('div.modal#view input#status').attr({
            disabled: false
        });
        // if(url == 'received') {
        //     var urlViewData = "<?php echo base_url('admin/dailyreports/view_received') ?>";
        //     $('div.modal#view input#status').attr({
        //         disabled: true
        //     });
        // }
        // if(url == 'sent') {
        //     var urlViewData = "<?php echo base_url('admin/dailyreports/view_send') ?>";
        // }
        // if(url == 'archived') {
        //     var urlViewData = "<?php echo base_url('admin/dailyreports/view_archived') ?>";
        // }
        // if(url == 'draft') {
        //     var urlViewData = "<?php echo base_url('admin/dailyreports/view_draft') ?>";
        // }
        $('#view .row_absensi').hide();
        $('#view .row_support').hide();
        $('#view .row_daily').hide();
        $.ajax({
            // url: urlViewData,
            url: '<?= base_url() ?>admin/dailyreports/detail',
            type: "POST",
            dataType: 'json',
            data: {
                id: id
            },
            success: function(data) {
                console.log("viewMyData", data);
                if (obj != undefined && obj != null) {
                    obj.find('#subject').show();
                    obj.find('#subject-marquee').hide();
                }

                if (data.show_absensi == true) {
                    $('#view .row_absensi').show();
                }
                if (data.show_support == true) {
                    $('#view .row_support').show();
                }
                if (data.show_daily == true) {
                    $('#view .row_daily').show();
                }
                if (data.jam_buka != null) {
                    $("#jam_buka_").text(data.jam_buka) 
                }
                // console.log('resp',data);
                $.each(data, function(index, value) {
                    if (index == 'receivers') {
                        var text = '';
                        var sep = '';
                        $.each(value, function(i, rcv) {
                            text += sep + '<b>' + rcv.name + '</b>';
                            sep = ', ';
                        });
                        $('#view #sent_to').html(text);
                    } else if (index == 'attachments') {
                        var files = $('#view #file_attachment');
                        var filesJ = $('#view #file_attachment_jam_buka');
                        files.html('');
                        filesJ.html('');
                        
                        $.each(value, function(i, file) {
                            if (file.type == "0"){
                                var file_name = '';
                            if (file.name != null && file.name.length > 0) {
                                file_name = file.name;
                            } else if (file.title != null && file.title.length > 0) {
                                file_name = file.title;
                            } else {
                                var paths = file.path.split('/');
                                var l = paths.length;
                                if (l > 0) {
                                    file_name = paths[l - 1];
                                }
                            }
                            var f = '<a target="_blank" href="<?php echo base_url(); ?>' + file.path + '"><i style="color: black" class="notika-icon notika-file" style="font-size: 15pt"></i> ' + file_name + '</a>';
                            files.append(f);
                            }else{
                                var file_name = '';
                            if (file.name != null && file.name.length > 0) {
                                file_name = file.name;
                            } else if (file.title != null && file.title.length > 0) {
                                file_name = file.title;
                            } else {
                                var paths = file.path.split('/');
                                var l = paths.length;
                                if (l > 0) {
                                    file_name = paths[l - 1];
                                }
                            }
                            var f = '<a target="_blank" href="<?php echo base_url(); ?>' + file.path + '"><i style="color: black" class="notika-icon notika-file" style="font-size: 15pt"></i> ' + file_name + '</a>';
                            filesJ.append(f);
                            }

                        });
                    } else if (index == 'status') {
                        // $('#view input#status_name[type=checkbox]').prop('checked', String(value).toUpperCase() == 'CLOSE');
                        $("input#" + index + '[type=checkbox]').prop('checked', String(value).toUpperCase() == 'CLOSE');
                        $('#view #status_name').html(value);
                    } else if (index == 'author') {
                        $('#view #from').html(value.call_name);
                       // $('#view #from2').html(value.last_name);
                        $('#view #position').html(value.position_name);
                        $('#view #branch').html(value.branch_name);
                        $('#view #photo_profile').attr('src', base_url + 'uploads/avatar/' + value.image);
                    } else {
                        var _target = $('#view #' + index);
                        if (_target.length > 0) {
                            var nodeName = _target[0].nodeName;
                            // console.log('target elemen', _target, nodeName);
                            if (nodeName == 'INPUT') {
                                _target.val(value);
                            } else {
                                _target.html(value);
                            }
                        }
                    }
                });
                // $.each(data, function(index, value) {
                //     $.each(value, function(i, val) {
                //         if(i == 'sent_to') {
                //             if(Array.isArray(val)) {
                //                 var name = [];
                //                 $.each(val, function(key, keyvalue) {
                //                     name.push(keyvalue['name']);
                //                 });
                //                 $("#" + i).html(name.join());
                //             } else {
                //                 $("#" + i).html(val);
                //             }
                //         } else if(i == 'file_attachment') {
                //             var html = "";
                //             $.each(val, function(key, keyvalue) {
                //                 var file_name = keyvalue['path'];
                //                 var file_names = file_name.split('/');
                //                 var len = file_names.length;
                //                 if(len > 0) {
                //                     file_name = file_names[len - 1];
                //                 }
                //                 html += '<a target="_blank" href="<?php echo base_url(); ?>' + keyvalue['path'] + '"><i style="color: black" class="notika-icon notika-form"></i> ' + file_name + '</a>';
                //             });
                //             $("#" + i).html(html);
                //         } else if(i == 'status') {
                //             $("input#" + i + '[type=checkbox]').prop('checked', String(val).toUpperCase() == 'CLOSE');
                //             $('#status_name').html(val);
                //         } else if(i == 'photo_profile') {
                //             $('#photo_profile').attr({
                //                 src: '<?= base_url() ?>uploads/avatar/' + val
                //             });
                //         } else {
                //             val = $("#" + i).html(val).text();
                //             $("#" + i).html(val);
                //             $("input#" + i).val(val);
                //         }
                //     });
                // });
                setReplayUser(id);
                setReadUser(id);

                // kondisi hide dr pj freelance
                var id_position = '<?= $id_position ?>';
                var status_karyawan = '<?= $this->status_karyawan ?>';
                console.log('<?= $this->status_karyawan ?>');
                if (['13'].includes(id_position) && status_karyawan == 'FREELANCE') {
                    $('#view .row_absensi').hide();
                    $('#view .row_support').hide();
                    $('#view .row_daily').hide();
                    $('#view .row_drpj').show();
                } else {}
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown)
            }
        });
    }

    function sendReplay() {
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var url = "<?php echo base_url('admin/dailyreports/replay') ?>";
        var data = $("#replay_form").serialize();
        /*swal({
            title: 'Sending Reply',
            text: 'Please wait....',
            showCancelButton: false,
            showConfirmButton: false,
            allowOutsideClick: false,
            type: 'info'
        });*/
        $.ajax({
            url: url,
            type: "POST",
            dataType: 'json',
            data: data,
            success: function(data) {
                CKEDITOR.instances['editor3'].setData('');
                swal({
                    title: 'Daily Report',
                    text: data.message,
                    type: 'success',
                }, function() {
                    // viewData();
                    // location.reload();
                    console.log(data);
                    setReplayUser(data.id);
                    $('input[name="id_replay"]').val('');
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
                swal({
                    title: 'Terjadi kesalahan',
                    text: 'Terjadi kesalahan saat mengirim balasan',
                    type: 'error'
                })
            }
        });
    }

    function setReplayUser(id = null) {
        url = "<?php echo base_url('admin/dailyreports/get_user_replay') ?>";
        url_photo = "<?php echo base_url() ?>";
        $.ajax({
            url: url,
            type: "POST",
            dataType: 'json',
            data: {
                id: id
            },
            success: function(data) {
                var replay = "";
                $(".user_replay").html(replay);
                if (data !== '') {
                    $.each(data, function(i, val) {
                        replay += '<div class="container mt-5">'
                        replay += '<div class="row d-flex justify-content-center">'
                        replay += '<div class="col-md-11">'
                        replay += '<div class="card p-3">'
                        replay += '<div class="d-flex justify-content-between align-items-center" id="containerdatareplay_' + val['id'] + '">'
                        replay += '<div class="user d-flex flex-row align-items-center">'
                        replay += '<img class="b-lazy" data-src="' + url_photo + 'uploads/avatar/' + val['photo_profile'] + '" width="30" class="user-img rounded-circle mr-2">'
                        replay += '<div class="user d-flex flex-row align-items-center">'
                        replay += '<span ><small class="font-weight-bold" style="color:#0ba360 !important">' + val['call_name'] + '&nbsp;(' + val['name_position'] + ')' + '<br>' + '</small></span>'

                        replay += '<div id="containerspan_' + val['id'] + '" style="margin-left:50px;text-align:justify;"><small class="font-weight-bold">' + decodeHTMLEntities(val['message_reply']) + '</small> </div>'
                        replay += '</div>'
                        replay += '<small style="margin-left:50px;color:#0ba360 !important;">' + val['datedate_added'] + '</small>'
                        replay += '</div>'
                        replay += '<div class="action d-flex justify-content-between mt-2 align-items-center">'
                        replay += '<div class="reply px-4">'

                        if (val['is_edit'] == '1') {
                            replay += '<button style="color:#0ba360 !important" class="btn btn-default btn-sm waves-effect collapsed" onclick="editDataReplay(' + val['id'] + ');" data-toggle="collapse" href="#collapseTwo_20" aria-expanded="false" aria-controls="collapseTwo_20"> <i class="notika-icon notika-edit"></i></button>'
                            replay += '<button style="color:#0ba360 !important" class="btn btn-default btn-sm waves-effect" onclick="deleteDataReplay(' + val['id'] + ');"><i class="notika-icon notika-trash"></i></button>'
                        }
                        replay += '</div></div></div></div></div></div>'
                    })
                    $(".user_replay").append(replay);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown)
            }
        });
    }

    function deleteData(param) {
        var val_dailyreport = [];
        $(".delete_data:checkbox:checked").each(function() {
            val_dailyreport.push($(this).val());
        });
        $.ajax({
            url: "<?php echo base_url('admin/dailyreports/delete_sent_draft') ?>",
            type: "POST",
            dataType: 'json',
            data: {
                id: val_dailyreport,
                param: param
            },
            success: function(data) {
                swal({
                    title: 'Daily Report',
                    text: data.message,
                    type: 'success',
                }, function() {
                    //location.reload();
                    createPagination(1);
                    createPagination2(1);
                    createPagination3(1);
                    createPagination4(1);
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown)
            }
        });
    }

    function deleteDataReplay(param) {
        $.ajax({
            url: "<?php echo base_url('admin/dailyreports/delete_data_replay') ?>",
            type: "POST",
            dataType: 'json',
            data: {
                id: param
            },
            success: function(data) {
                swal({
                    title: 'Daily Replay Report',
                    text: data.message,
                    type: 'success',
                }, function() {
                    //location.reload();
                    var id = $('#id').val();
                    setReplayUser(id);
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown)
            }
        });
    }

    function deleteDataReceived() {
        var val_dailyreport = [];
        $(".delete_data:checkbox:checked").each(function() {
            val_dailyreport.push($(this).val());
        });
        $.ajax({
            url: "<?php echo base_url('admin/dailyreports/delete_received') ?>",
            type: "POST",
            dataType: 'json',
            data: {
                id: val_dailyreport
            },
            success: function(data) {
                swal({
                    title: 'Daily Report',
                    text: data.message,
                    type: 'success',
                }, function() {
                    //location.reload();
                    createPagination(1);
                    createPagination2(1);
                    createPagination3(1);
                    createPagination4(1);
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown)
            }
        });
    }

    function editData(obj) {
        var url = $(obj).data("url");
        console.log(url);
        var id = $(obj).data("id");
        console.log(id);
        $('input[name="id_dailyreports"]').val(id);
        $("#button_save").val("EDIT");
        if (url == 'received') {
            var urlViewDataEdit = "<?php echo base_url('admin/dailyreports/view_received') ?>";
        }
        if (url == 'sent') {
            var urlViewDataEdit = "<?php echo base_url('admin/dailyreports/view_send') ?>";
        }
        if (url == 'archived') {
            var urlViewDataEdit = "<?php echo base_url('admin/dailyreports/view_archived') ?>";
        }
        if (url == 'draft') {
            var urlViewDataEdit = "<?php echo base_url('admin/dailyreports/view_draft') ?>";
        }
        $.ajax({
            url: urlViewDataEdit,
            type: "POST",
            dataType: 'json',
            data: {
                id: id
            },
            success: function(data) {
                setDataEdit(data.result, url);
                console.log("testing",data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown)
            }
        });
    }

    function editDataReplay(obj) {
        var text = $('#containerspan_' + obj).html();
        console.log(text);
        $('#editor3').val(text);

        CKEDITOR.instances['editor3'].setData(text);

        $('input[name="id_replay"]').val(obj);
    }

    function setDataEdit(data, url) {
        $.each(data, function(index, value) {
            $("input[name='<?php echo $model ?>[title]']").val(value['subject']).parent().addClass('focused');
            $("select[name='<?php echo $model ?>[kategori]']").val(value['kategori']);
            $("#file").attr('disabled', false);
            $("#status_" + value['status'].toLowerCase()).prop('checked', true);
            if (value['absensi_des'] !== 'Lancar') {
                $("#radio_4").prop('checked', true);
                $("#absendes").addClass('show');
                CKEDITOR.instances['editor5'].setData(decodeHTMLEntities(value['absensi_des']));
            } else {
                $("#radio_3").prop('checked', true);
                $("#absendes").removeClass('show');
            }
            if (value['reg_des'] !== 'Lancar') {
                $("#radio_6").prop('checked', true);
                $("#regdes").addClass('show');
                CKEDITOR.instances['editor6'].setData(decodeHTMLEntities(value['reg_des']));
            } else {
                $("#radio_5").prop('checked', true);
                $("#regdes").removeClass('show');
            }
            if (value['sampling_des'] !== 'Lancar') {
                $("#radio_8").prop('checked', true);
                $("#samplingdes").addClass('show');
                CKEDITOR.instances['editor7'].setData(decodeHTMLEntities(value['sampling_des']));
            } else {
                $("#radio_7").prop('checked', true);
                $("#samplingdes").removeClass('show');
            }
            if (value['qc_des'] !== 'Lancar') {
                $("#radio_10").prop('checked', true);
                $("#qcdes").addClass('show');
                CKEDITOR.instances['editor8'].setData(decodeHTMLEntities(value['qc_des']));
            } else {
                $("#radio_9").prop('checked', true);
                $("#qcdes").removeClass('show');
            }
            if (value['alat_des'] !== 'Lancar') {
                $("#radio_12").prop('checked', true);
                $("#alatdes").addClass('show');
                CKEDITOR.instances['editor9'].setData(decodeHTMLEntities(value['alat_des']));
            } else {
                $("#radio_11").prop('checked', true);
                $("#alatdes").removeClass('show');
            }
            if (value['janji_des'] !== 'Lancar') {
                $("#radio_14").prop('checked', true);
                $("#janjides").addClass('show');
                CKEDITOR.instances['editor10'].setData(decodeHTMLEntities(value['janji_des']));
            } else {
                $("#radio_13").prop('checked', true);
                $("#janjides").removeClass('show');
            }
            if (value['lain_des'] !== 'Lancar') {
                $("#radio_16").prop('checked', true);
                $("#laindes").addClass('show');
                CKEDITOR.instances['editor11'].setData(decodeHTMLEntities(value['lain_des']));
            } else {
                $("#radio_15").prop('checked', true);
                $("#laindes").removeClass('show');
            }
            if (value['pelayanan_des'] !== 'Lancar') {
                $("#radio_19").prop('checked', true);
                $("#pelayanandes").addClass('show');
                CKEDITOR.instances['editor12'].setData(decodeHTMLEntities(value['pelayanan_des']));
            } else {
                $("#radio_18").prop('checked', true);
                $("#pelayanandes").removeClass('show');
            }
            for (var i = 1; i < 16; i++) {
                $("#radio_" + i).attr('disabled', false);
            }

            console.log('OK');
            $('select.search_user_').select2({
                ajax: {
                    method: 'post',
                    url: '<?= base_url() ?>admin/master_data/many_user',
                    processResults: function(resp) {
                        console.log('receiver resp', resp);
                        var res = JSON.parse(resp);
                        var items = [];

                        for (var i = 0, i2 = res.length; i < i2; i++) {
                            items.push({
                                id: res[i].id_user,
                                text: res[i].call_name
                            });
                        }
                        return {
                            results: items
                        };
                    }
                }
            });

            if (value['sent_to'][0] !== '') {
                var name = [];
                var to_id = [];

                $("select.search_user_").html('');
                $.each(value['sent_to'], function(key, keyvalue) {
                    name.push(keyvalue['call_name']);
                    to_id.push(keyvalue['id_receiver']);
                    $("<option selected='selected'></option>").remove();
                    var $newOption = $("<option selected='selected'></option>").val(keyvalue['id_receiver']).text(keyvalue['call_name']);
                    $("select.search_user_").append($newOption).trigger('change');
                });

                $("select.search_user_").val(to_id).trigger('change');
                $("select.search_user_").parent().addClass('focused');
                $('#senttopos').attr('disabled', true);
                $('#senttodept').attr('disabled', true);
                $('#senttobag').attr('disabled', true);
                if (url == 'draft') {
                    $("input[name='<?php echo $model ?>[to_id]']").val(to_id.join());
                }
            } else {
                $("select.search_user_").attr('disabled', false);
            }
            

            // $('#jam_buka').val(value['jam_buka']);
        });
    }

    function getUserRead(obj, id) {
        $('#count').modal('show');
        // console.log('count data', obj, id);
        var id = $(obj).data("id");
        urlGetUserRead = "<?php echo base_url('admin/dailyreports/get_user_read') ?>";
        $.ajax({
            url: urlGetUserRead,
            type: "POST",
            dataType: 'json',
            data: {
                id: id
            },
            success: function(data) {
                $("#user_received").html("");
                $("#user_read").html("");
                $.each(data, function(index, value) {
                    var html = "";
                    html += "<li>" + value['call_name'] + ' - ' + value['name_position'] + "</li>"
                    $("#user_received").append(html);
                    if (value['is_view'] == '1') {
                        $("#user_read").append(html);
                    }
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown)
            }
        });
    }

    function setReadUser(id) {
        urlSetReadUser = "<?php echo base_url('admin/dailyreports/set_user_read') ?>";
        $.ajax({
            url: urlSetReadUser,
            type: "POST",
            dataType: 'json',
            data: {
                id: id
            },
            success: function(data) {},
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown)
            }
        });
    }

    function decodeHTMLEntities(text) {
        var entities = {
            'amp': '&',
            'apos': '\'',
            '#x27': '\'',
            '#x2F': '/',
            '#39': '\'',
            '#47': '/',
            'lt': '<',
            'gt': '>',
            'nbsp': ' ',
            'quot': '"'
        }
        return text.replace(/&([^;]+);/gm, function(match, entity) {
            return entities[entity] || match
        })
    }

    function ubah_status(node) {
        // console.log('ubah status', node);
        node = $(node);
        var modal = node.closest('div.modal');
        var id = modal.find('input#id').val();
        var newVal = node.prop('checked');
        swal({
            title: 'Ubah Status',
            text: 'Ubah status daily report menjadi ' + (newVal ? 'Close' : 'Open') + '?',
            type: 'warning',
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            showCancelButton: true
        }, function(v) {
            console.log('ubah status alert res', v);
            if (v == false) {
                node.prop('checked', !newVal);
            } else if (v == true) {
                // setTimeout(function(){
                $.ajax({
                    url: base_url + 'admin/dailyreports/update_status',
                    method: 'post',
                    data: {
                        id_dailyreport: id,
                        status: newVal ? 'Close' : 'Open'
                    },
                    success: function(resp) {
                        try {
                            var res = JSON.parse(resp);
                            if (res.status == true) {
                                swal({
                                    title: 'Berhasil',
                                    text: 'Status berhasil diubah',
                                    type: 'success'
                                }, function() {
                                    //window.location.reload();
                                    createPagination(1);
                                    createPagination2(1);
                                    createPagination3(1);
                                    createPagination4(1);
                                });
                            } else {
                                swal.showInputError('Gagal mengubah status, ' + res.message);
                            }
                        } catch (e) {
                            swal.showInputError('Respon server tidak dikenal');
                        }
                    },
                    complete: function() {}
                });
                // }, 1000);
            }
        })
    }

    function createPagination(pageNum) {
        var search = $('#search-inbox').val();

        $.ajax({
            url: '<?= base_url() ?>admin/dailyreports/view_received/' + pageNum,
            type: 'post',
            data: {
                rowno: pageNum,
                view: "false",
                search: search
            },
            dataType: 'json',
            success: function(responseData) {
                $('#pagination').html(responseData.pagination);
                paginationData(responseData.result);
            }
        });
    }

    function paginationData(data) {
        $('.inbox-cont').html('');
        html_table = '';
        $.each(data, function(key, value) {
            html_table +='<table class="table table-inbox table-hover">' +
            '<tbody>' +
            '   <tr style="cursor: pointer;text-align: justify" class="row-report" id="view_' + value.id + '" data-togglee="modal" data-target="#view" data-id="' + value.id + '" data-url="received">' +
            '       <td width="1%">' +
            '           <div class="custom-control custom-checkbox">' +
            '               <input type="checkbox" class="custom-control-input filled-in pull-left delete_data" value="' + value.id + '" id="ig_checkbox_a_' + value.id + '">' +
            '               <label class="custom-control-label pull-left" for="ig_checkbox_a_' + value.id + '"></label>' +
            '           </div>' +
            '       </td>' +
            '       <td width="10%">' +
            '           <img class="b-lazy" data-src="<?php echo base_url() ?>uploads/avatar/' + value.photo_profile + '" alt="sender" />' +
            '       </td>' +
            '       <td width="30%">' +
            '           ' + value.from + ' (' + value.branch + ')' +
            '       </td>' +
            '       <td width="10%">' +
            '           <button type="button" style="padding: 6px 4px;" class="btn btn-success">' +
            '               ' + value.count_read + ' / ' + value.count_send + '' +
            '           </button>' +
            '       </td>' +
            '       <td width="20%">';
						if (value.read != '1') {
							html_table += '<center id="subject-marquee"><span><marquee> NEW! ' + value.subject + '</marquee></span></center>';
						}
						html_table += '<center id="subject" style="' + ((value.read == '1') ? '' : 'display: none') + '"><span>' + value.subject.substring(0, 30) + "....." + '</span></center></td>' +
            '	    <td width="10%">' +
			'		    <span class=""> ' +
			'		        ' + ((value.file_attachment != '') ? '<i style="color: black" class="notika-icon notika-form"></i>' : "") + '' +
            '           </span>' +
            '       </td>' +
            '       <td width="10%" class="text-right">' +
            '           ' + value.date + ' ' +
            '       </td>' +
            '   </tr>' +
            '</tbody>' +
            '</table>';
        });
        $('.inbox-cont').append(html_table);
        //$('.inbox-cont').html(html_table);
    }

    function createPagination2(pageNum) {
        var search = $('#search-sent').val();
        $.ajax({
            url: '<?= base_url() ?>admin/dailyreports/view_send/' + pageNum,
            type: 'post',
            data: {
                rowno: pageNum,
                view: "false",
                search: search
            },
            dataType: 'json',
            success: function(responseData) {
                $('#pagination2').html(responseData.pagination);
                paginationData2(responseData.result);
            }
        });
    }

    function paginationData2(data) {
        $('.sent-cont').html('');
        html_table = '';
        $.each(data, function(key, value) {
            html_table  +='<table class="table table-inbox table-hover">' +
            '<tbody>' +
            '   <tr style="cursor: pointer;text-align: justify;margin-left: -100px;" class="row-report" id="view_' + value.id + '" data-togglee="modal" data-target="#view" data-id="' + value.id + '" data-url="sent">' +
            '       <td width="1%">' +
            '           <div class="custom-control custom-checkbox" style="margin-left: -20px">' +
            '               <a style="font-size: 20px"> <i style="color: black" id="edit_' + value.id + '" class="lar la-edit" data-toggle="modal" data-target="#add" data-id="' + value.id + '" data-url="sent" onclick="editData(this);"></i></a>' +
            '               <input type="checkbox" class="custom-control-input filled-in pull-left delete_data" value="' + value.id + '" id="ig_checkbox_a_' + value.id + '">' +
            '               <label class="custom-control-label pull-left" for="ig_checkbox_a_' + value.id + '"></label>' +
            '           </div>' +
            '       </td>' +
            '       <td width="10%">' +
            '           <img class="b-lazy" data-src="<?php echo base_url() ?>uploads/avatar/' + value.photo_profile + '" alt="sender" />' +
            '       </td>' +
            '       <td width="30%">' +
            '           ' + value.from + ' (' + value.branch + ')' +
            '       </td>' +
            '       <td width="10%">' +
            '           <button type="button" style="padding: 6px 4px;" class="btn btn-success">' +
            '               ' + value.count_read + ' / ' + value.count_send + '' +
            '           </button>' +
            '       </td>' +
            '       <td width="20%">' +
            '           <center><span>' + value.subject + '</span></center>' +
            '       </td>' +
            '	    <td width="10%">' +
			'		    <span class=""> ' +
			'		        ' + ((value.file_attachment != '') ? '<i style="color: black" class="notika-icon notika-form"></i>' : "") + '' +
            '           </span>' +
            '       </td>' +
            '       <td width="10%" class="text-right">' +
            '           ' + value.date + ' ' +
            '       </td>' +
            '   </tr>' +
            '</tbody>' +
            '</table>';
        });
        $('.sent-cont').append(html_table);
        //$('.inbox-cont').html(html_table);
    }

    function createPagination3(pageNum) {
        var search = $('#search-archived').val();
        $.ajax({
            url: '<?= base_url() ?>admin/dailyreports/view_archived/' + pageNum,
            type: 'post',
            data: {
                rowno: pageNum,
                view: "false",
                search: search
            },
            dataType: 'json',
            success: function(responseData) {
                $('#pagination3').html(responseData.pagination);
                paginationData3(responseData.result);
            }
        });
    }

    function paginationData3(data) {
        $('.archived-cont').html('');
        html_table = '';
        $.each(data, function(key, value) {
            html_table += '<table class="table table-inbox table-hover">' +
            '<tbody>' +
            '   <tr style="cursor: pointer;text-align: justify;margin-left: -100px;" class="row-report" id="view_' + value.id + '" data-togglee="modal" data-target="#view" data-id="' + value.id + '" data-url="archived">' +
            '       <td width="1%">' +
            '           <div class="custom-control custom-checkbox">' +
            '               <input type="checkbox" class="custom-control-input filled-in pull-left delete_data" value="' + value.id + '" id="ig_checkbox_a_' + value.id + '">' +
            '               <label class="custom-control-label pull-left" for="ig_checkbox_a_' + value.id + '"></label>' +
            '           </div>' +
            '       </td>' +
            '       <td width="10%">' +
            '           <img class="b-lazy" data-src="<?php echo base_url() ?>uploads/avatar/' + value.photo_profile + '" alt="sender" />' +
            '       </td>' +
            '       <td width="30%">' +
            '           ' + value.from + ' (' + value.branch + ')' +
            '       </td>' +
            '       <td width="10%">' +
            '           <button type="button" style="padding: 6px 4px;" class="btn btn-success">' +
            '               ' + value.count_read + ' / ' + value.count_send + '' +
            '           </button>' +
            '       </td>' +
            '       <td width="20%">' +
            '           <center><span>' + value.subject + '</span></center>' +
            '       </td>' +
            '	    <td width="10%">' +
			'		    <span class=""> ' +
			'		        ' + ((value.file_attachment != '') ? '<i style="color: black" class="notika-icon notika-form"></i>' : "") + '' +
            '           </span>' +
            '       </td>' +
            '       <td width="10%" class="text-right">' +
            '           ' + value.date + ' ' +
            '       </td>' +
            '   </tr>' +
            '</tbody>' +
            '</table>';
        });
        $('.archived-cont').append(html_table);
        //$('.inbox-cont').html(html_table);
    }

    function createPagination4(pageNum) {
        var search = $('#search-draft').val();
        $.ajax({
            url: '<?= base_url() ?>admin/dailyreports/view_draft/' + pageNum,
            type: 'post',
            data: {
                rowno: pageNum,
                view: "false",
                search: search
            },
            dataType: 'json',
            success: function(responseData) {
                $('#pagination4').html(responseData.pagination);
                paginationData4(responseData.result);
            }
        });
    }

    function paginationData4(data) {
        $('.draft-cont').html('');
        html_table = '';
        $.each(data, function(key, value) {
            html_table += '<table class="table table-inbox table-hover">' +
            '<tbody>' +
            '   <tr style="cursor: pointer;text-align: justify;margin-left: -100px;" class="row-report" id="view_' + value.id + '" data-togglee="modal" data-target="#view" data-id="' + value.id + '" data-url="draft">' +
            '       <td width="1%">' +
            '           <div class="custom-control custom-checkbox" style="margin-left: -20px">' +
            '               <a style="font-size: 20px"> <i style="color: black" id="edit_' + value.id + '" class="lar la-edit" data-toggle="modal" data-target="#add" data-id="' + value.id + '" data-url="draft" onclick="editData(this);"></i></a>' +
            '               <input type="checkbox" class="custom-control-input filled-in pull-left delete_data" value="' + value.id + '" id="ig_checkbox_a_' + value.id + '">' +
            '               <label class="custom-control-label pull-left" for="ig_checkbox_a_' + value.id + '"></label>' +
            '           </div>' +
            '       </td>' +
            '       <td width="10%">' +
            '           <img class="b-lazy" data-src="<?php echo base_url() ?>uploads/avatar/' + value.photo_profile + '" alt="sender" />' +
            '       </td>' +
            '       <td width="30%">' +
            '           ' + value.from + ' (' + value.branch + ')' +
            '       </td>' +
            '       <td width="10%">' +
            '           <button type="button" style="padding: 6px 4px;" class="btn btn-success">' +
            '               ' + value.count_read + ' / ' + value.count_send + '' +
            '           </button>' +
            '       </td>' +
            '       <td width="20%">' +
            '           <center><span>' + value.subject + '</span></center>' +
            '       </td>' +
            '	    <td width="10%">' +
			'		    <span class=""> ' +
			'		        ' + ((value.file_attachment != '') ? '<i style="color: black" class="notika-icon notika-form"></i>' : "") + '' +
            '           </span>' +
            '       </td>' +
            '       <td width="10%" class="text-right">' +
            '           ' + value.date + ' ' +
            '       </td>' +
            '   </tr>' +
            '</tbody>' +
            '</table>';
        });
        $('.draft-cont').append(html_table);
        //$('.inbox-cont').html(html_table);
    }
</script>

<style type="text/css">
    #pagination {
        display: inline-block;
        padding-left: 0;
        border-radius: 4px;
    }

    #pagination>a,
    #pagination>strong {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #0aa35f;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    #pagination2 {
        display: inline-block;
        padding-left: 0;
        border-radius: 4px;
    }

    #pagination2>a,
    #pagination2>strong {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #0aa35f;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    #pagination3 {
        display: inline-block;
        padding-left: 0;
        border-radius: 4px;
    }

    #pagination3>a,
    #pagination3>strong {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #0aa35f;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    #pagination4 {
        display: inline-block;
        padding-left: 0;
        border-radius: 4px;
    }

    #pagination4>a,
    #pagination4>strong {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #0aa35f;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }
</style>