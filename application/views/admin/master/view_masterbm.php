
<!-- main-content opened -->
<div class="main-content horizontal-content">

<!-- container opened -->
<div class="container">


<!-- Data Table area Start-->
<div class="data-table-area">
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="data-table-list">
<div>
<ul class="nav nav-pills" role="tablist">

<li role="presentation" class="nav-item active"><a href="#pay" class="nav-link active" aria-controls="settings" role="tab" data-toggle="tab">Payment</a></li>
<li role="presentation" class="nav-item"><a href="#time" class="nav-link" aria-controls="settings" role="tab" data-toggle="tab">Home Service Time</a></li>
<li role="presentation" class="nav-item"><a href="#abbr" class="nav-link" aria-controls="settings" role="tab" data-toggle="tab">Jalur HS</a></li>
<li role="presentation" class="nav-item"><a href="#block" class="nav-link " aria-controls="settings" role="tab" data-toggle="tab">Block HS</a></li>
<li role="presentation" class="nav-item"><a href="#phs" class="nav-link " aria-controls="settings" role="tab" data-toggle="tab">Petugas HS</a></li>
<!-- <li role="presentation" class="nav-item"><a href="#kary" class="nav-link " aria-controls="settings" role="tab" data-toggle="tab">Karyawan</a></li> -->
<!-- <li role="presentation" class="nav-item"><a href="#finger" id="tab_finger" class="nav-link " aria-controls="settings" role="tab" data-toggle="tab">Finger</a></li> -->
<!-- <li role="presentation" class="nav-item"><a href="#schedule" class="nav-link " aria-controls="settings" role="tab" data-toggle="tab">Internal Schedule</a></li> -->
<!-- <li role="presentation" class="nav-item"><a href="#branch" class="nav-link " aria-controls="settings" role="tab" data-toggle="tab">Branch</a></li> -->
<!-- <li role="presentation" class="nav-item"><a href="#appdokter_shift" class="nav-link " aria-controls="settings" role="tab" data-toggle="tab">Dokter Shift</a></li> -->


</ul>


<!-- Start Master Payment -->
<div class="tab-content" style="width:100%">
<?php
$this->load->view("admin/bm/tab_petugashs");
$this->load->view("admin/bm/tab_fs");
$this->load->view("admin/bm/tab_abbr");
$this->load->view("admin/bm/tab_time");
$this->load->view("admin/bm/tab_block");
// $this->load->view("admin/bm/tab_karyawan");
// $this->load->view("admin/bm/tab_internal_schedule");
// $this->load->view("admin/bm/tab_finger");
// $this->load->view("admin/bm/tab_branch");
// $this->load->view("admin/bm/tab_shift_dokter");
?>

<div role="tabpanel" class="tab-pane fade in active" id="pay">

<div class="header">
<center><button type="button" class="btn btn-success" onclick="tambahPay();">Add</button></center>
</div>
<div class="table-responsive">
<table id="table_masterpay" class="table table-bordered table-striped table-hover dataTable js-exportable" width="100%" cellspacing="0">
<thead>
<tr>
<th>No</th>
<th>Pay Name</th>
<th>Creator</th>
<th>Created Date</th>
<th>Action</th>
</tr>
</thead>
</table>
</div>


<div class="modal fade" id="modal-pay" tabindex="-1" role="dialog">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Tambah Payment</h4>
</div>
<div class="modal-body">
<form action="#" id="form_masterpay">
<input type="hidden" readonly name="pay_id" class="form-control">

<label class="form-label">Pay Name</label>
<div class="form-group form-float">
<div class="form-line">
<input type="text" name="pay_name" class="form-control">
</div>
</div>


<button class="btn btn-danger btn-sm hidden" type="reset">Reset</button>
</form>
</div>
<div class="modal-footer">
<input type="button" onclick="savePay();" value="Save">
<input type="button" data-dismiss="modal" value="Cancel">
</div>

</div>
</div>
</div>


</div>
</div>
<!-- End Master Payment -->





</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<br>


<script type="text/javascript">

$(document).ready( function () {
getDataPay();
resetFormPay();

var id_notif = '<?= isset($_GET["notif"]) ? $_GET["notif"] : "" ?>';
var type_notif = '<?= isset($_GET["type"]) ? $_GET["type"] : "" ?>';
if (id_notif.length > 0) {
	if(type_notif=='petugashs')
	{
		$('.nav-pills li:nth-child(5)').find('a').trigger('click');
		//editPhs(id_notif);
	}
}


});

    
$("#table_masterpay");

function getDataPay()
{
url = "<?php echo base_url('admin/master_bm/get_pay') ?>";
$('#table_masterpay').DataTable({
scrollCollapse: true,
"order": [[ 0, "desc" ]],
"ajax": url,
"columns": [
{ "data": "no" },
{ "data": "pay_name" },
{ "data": "creator_id" },
{ "data": "created_at" },
{  "width": "8%", 
"mRender": function(index, type, data) {
html = '';
html += '<a href="#" onclick="editPay('+data.pay_id+');"><i class="notika-icon notika-draft"></i></a>';
html += '<a href="#" onclick="hapusPay('+data.pay_id+');"><i class="notika-icon notika-trash"></i></a>';
return html;
}
},
],
"oLanguage": {
"sSearch": "Pencarian :",
"sZeroRecords": "Data tidak ditemukan.",
"sLengthMenu": "Tampilkan _MENU_ data",
"sEmptyTable": "Data tidak ditemukan.",
"sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data.",
"infoFiltered": "(dari total _MAX_ data)"
}
});
}

function resetFormPay()
{

$("#modal-pay .modal-title").text('Modal Data');
$('#form_masterpay').find('button[type=reset]').click();
$("#form_masterpay input").attr('disabled', false);
$("#form_masterpay select").attr('disabled', false);
$("#form_masterpay input[name='pay_id']").val("");
$("#modal-pay .modal-footer button").show();
$("#modal-pay .modal-footer button.action").hide();


}



function reloadDataPay()
{
$("#modal-pay").modal('hide');
var table = $('#table_masterpay').DataTable();
table.ajax.reload();
}

function tambahPay()
{
$("#modal-pay .modal-title").text('Tambah Data');
resetFormPay();
$("#modal-pay").modal('show');
}

function editPay(pay_id)
{
resetFormPay();
url = "<?php echo base_url('admin/master_bm/edit_pay') ?>";
$.ajax({
url:url,
dataType: "json",
method: "post",
data:{
pay_id:pay_id
},
success: function(result) {
$("#modal-pay .modal-title").text('Edit Data');
$.each(result.data, function(index, value){
$("input[name='"+index+"']").val(value);
$("select[name='"+index+"']").val(value);
$("input[type=date][name='"+index+"']").val(value);
})
$("#modal-pay").modal('show');
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'Payment',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}

function savePay()
{
url = "<?php echo base_url('admin/master_bm/save_pay') ?>";

var fd = new FormData();
var other_data = $('#form_masterpay').serializeArray();
$.each(other_data, function(key, input) {
fd.append(input.name, input.value);
});

$.ajax({
url: url,
type: "POST",
data: fd,
dataType: 'json',
processData: false,
contentType: false,
success: function(result) {
swal({
title: 'Payment',
text: result.message,
type: 'success',
});
reloadDataPay();
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'Payment',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown)
}
});
};

function hapusPay(pay_id)
{
resetFormPay();
var konfirmasi = confirm("Apakah anda yakin ?");
if (konfirmasi == true) {
url = "<?php echo base_url('admin/master_bm/delete_pay') ?>";
$.ajax({
url:url,
dataType: "json",
method: "post",
data:{
pay_id:pay_id
},
success: function(result) {
swal({
title: 'Payment',
text: result.message,
type: 'success',
}, function() {
reloadDataPay();
});
},
error: function(jqXHR, textStatus, errorThrown) {
swal({
title: 'Payment',
text: 'Error',
type: 'error',
});
console.log(jqXHR, textStatus, errorThrown);
}
});
}
}

function refreshSelectPay(){
var select1 = $("#form_masterpay #pay_id");

select1.html("");

$.ajax({
method: "post",
url: base_url + "admin/master_bm/get_pay",
success: function (resp) {
var listPay = [];
try{
var json = JSON.parse(resp);
listPay = json.data;
}catch(x) {
console.log("error parse to json", resp, x);
}
$.each(listPay, function(index, unit) {
select1.append( $("<option/>").attr({value: hs_payment.pay_id}).html( hs_payment.pay_name ) );
});
}
});
}

function decodeHTMLEntities(text) {
var entities = {
'amp': '&',
'apos': '\'',
'#x27': '\'',
'#x2F': '/',
'#39': '\'',
'#47': '/',
'lt': '<',
'gt': '>',
'nbsp': ' ',
'quot': '"'
}

return text.replace(/&([^;]+);/gm, function(match, entity) {
return entities[entity] || match
})
}



</script>



