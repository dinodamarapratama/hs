<!-- main-content opened -->
<div class="main-content horizontal-content">



<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-windows"></i>
                                </div>
                                <div class="breadcomb-ctn">
                                    <?php
                                    $header = 'Menu Master';
                                    $btnBack = '';
                                    if(empty($parent) == false) {
                                        if(empty($parent_menu) == false) {
                                            $header = 'Submenu dari <b>' . $parent_menu->nama . '</b>';
                                            $btnBack = '<a href="' . base_url() . 'admin/master_menu?parent=' . $parent_menu->parent . '" class="btn btn-primary" style="margin-left: 10px; font-size: 8pt; padding: 4px;"><i class="material-icons" style="font-size: 10pt;">arrow_back</i>Kembali</a>';
                                        }
                                    }
                                    echo '<h2>'.$header.'</h2>';
                                    echo $btnBack;
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
                            <div class="breadcomb-report">
                                <button type="button" data-placement="left" class="btn waves-effect" id="btn-add-menu" data-toggle="tooltip" data-original-title="Add New Data">
                                    <i class="notika-icon notika-plus-symbol"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- </div> -->

<div class="data-table-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="data-table-list">
                    <div class="basic-table-hd">
                        <!-- <h2>Table title</h2> -->
                        <!-- <p>Table description</p> -->
                    </div>
                    <div class="table-responsive">
                        <table id="table_menu" class="table table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th style="min-width: 230px">Action</th>
                                    <th>Urutan</th>
                                    <th>Nama</th>
                                    <th>URL</th>
                                    <th>Mobile Route</th>
                                    <th>Submenu</th>
                                    <th>Icon Class</th>
                                    <th>Icon Inner Text</th>
                                    <th>Mobile Icon</th>
                                    <th>Show Mobile Drawer</th>
                                    <th>Show Mobile Dashboard</th>
                                    <th>Show In Web</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Action</th>
                                    <th>Urutan</th>
                                    <th>Nama</th>
                                    <th>URL</th>
                                    <th>Mobile Route</th>
                                    <th>Submenu</th>
                                    <th>Icon Class</th>
                                    <th>Icon Inner Text</th>
                                    <th>Mobile Icon</th>
                                    <th>Show Mobile Drawer</th>
                                    <th>Show Mobile Dashboard</th>
                                    <th>Show In Web</th>
                                </tr>
                            </tfoot>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->

<!-- Add menu -->
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="add">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" onclick="resetForm();"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">ADD MENU</h4>
            </div>
            <div class="modal-body">
                <form method="POST" enctype="multipart/form-data">
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <label class="form-label">Nama</label>
                            <div class="form-line focused">
                                <input type="text" name="nama"id="nama" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <label class="form-label">URL</label>
                            <div class="form-line focused">
                                <input type="text" name="url" id="url" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <label class="form-label">Mobile Route</label>
                            <div class="form-line focused">
                                <input type="text" name="mobile_route" id="mobile_route" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                             <label class="form-label">Urutan</label>
                            <div class="form-line focused">
                                <input type="number" name="urutan" id="urutan" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <label class="form-label">Icon Class</label>
                            <div class="form-line focused">
                                <input type="text" name="icon_class" id="icon_class" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <label class="form-label">Icon Text</label>
                            <div class="form-line focused">
                                <input type="text" name="icon_text" id="icon_text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <label class="form-label">Icon Mobile</label>
                            <div class="form-line focused">
                                <input type="text" name="mobile_icon" id="mobile_icon" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <label class="form-label">Tampil di Mobile Drawer</label>
                            <div class="form-line focused">
                                <select class="form-control" id="show_mobile_drawer" name="show_mobile_drawer">
                                    <option value="0">Tidak</option>
                                    <option value="1">Ya</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <label class="form-label">Tampil di Mobile Dashboard</label>
                            <div class="form-line focused">
                                <select class="form-control" id="show_mobile_dashboard" name="show_mobile_dashboard">
                                    <option value="0">Tidak</option>
                                    <option value="1">Ya</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <label class="form-label">Tampil di web</label>
                            <div class="form-line focused">
                                <select class="form-control" id="show_in_web" name="show_in_web">
                                    <option value="0">Tidak</option>
                                    <option value="1">Ya</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" id="id_menu" />

                    <center>
                        <button type="button" class="btn btn-primary btn-lg" id="btn-save" save-mode='save'>Save</button>
                    </center>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="close" data-dismiss="modal" onclick="resetForm();"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>

            </div>
        </div>
    </div>
</div>
<!-- end add menu -->

<!-- Add role -->
<div class="modal fade" id="modal_add_member" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="add">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn btn-link waves-effect pull-right" data-dismiss="modal">
                    CLOSE
                </button>
                <h4 class="modal-title">ADD Member</h4>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url()?>" method="POST" enctype="multipart/form-data" id='form'>
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" name="nama"id="nama" class="form-control"/>
                                <label class="form-label">Nama</label>
                            </div>
                        </div>
                    </div>
                </form>
                <center>
                    <button type="button" class="btn btn-primary btn-lg" id="btn-add">
                        <i class="material-icons">add_circle</i>
                    </button>
                </center>
                <label>Terpilih: </label>
                <div class="col-sm-12" id="selected-member">
                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sucess" id="btn-save">SIMPAN</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
<!-- end add role -->

<!-- begin detail menu -->
<div class="modal fade" id="modal_detail_menu" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="add">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn btn-link waves-effect pull-right" data-dismiss="modal">
                    CLOSE
                </button>
                <h4 class="modal-title">Detail Menu</h4>
            </div>
            <div class="modal-body">
                <div class="col-sm-12">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" value="" class="form-control" id="nama" readonly="" />
                            <label class="form-label">Nama Menu</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" value="" class="form-control" id="url" readonly="" />
                            <label class="form-label">URL</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group form-float">
                        <div class="form-line focused">
                            <input type="text" value="" class="form-control" id="urutan" readonly="" />
                            <label class="form-label">Urutan</label>
                        </div>
                    </div>
                </div>
                
                <label>Daftar User:</label>
                <div class="col-sm-12" id="list-member-user">
                </div>

                <br/>
                <br/>
                <br/>

                <label style="margin-top: 10px">Daftar Department:</label>
                <div class="col-sm-12" id="list-member-dept">
                </div>

                <br/>
                <br/>
                <br/>

                <label style="margin-top: 10px">Daftar Position:</label>
                <div class="col-sm-12" id="list-member-pos">
                </div>

            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-sucess" id="btn-save">SIMPAN</button> -->
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
</div>
<br>
<!-- end detail menu -->

<script type="text/javascript">
    var dtMenu = null;
    var base_url = '<?= base_url() ?>';
    var parent_id = '<?= $parent ?>';
    var listSelectedUser = [];
    var listSelectedDept = [];
    var listSelectedPos = [];
    var listUser = [];
    var listDept = [];
    var listPos = [];
    var memberAddMode = 'user';

    var dtLanguangeSetting = {
      "sSearch": "Pencarian :",
      "sZeroRecords": "Data tidak ditemukan.",
      "sLengthMenu": "Tampilkan _MENU_ data",
      "sEmptyTable": "Data tidak ditemukan.",
      "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data.",
      "infoFiltered": "(dari total _MAX_ data)"
    };

    $(document).ready(function(){
        initDtMenu();

        $('#modal_form #btn-save').on('click', function(e){
            var node = $(e.target);
            var saveMode = node.attr('save-mode');
            if(saveMode == 'update') {
                submitUpdate();
            } else {
                submitSave();
            }
        });

        $('#btn-add-menu').on('click', function(e){
            showFormAdd();
        });
        $('#modal_add_member #btn-add').on('click', function(e){
            addMemberToQueue();
        });
        $('#modal_add_member #btn-save').on('click', function(e) {
            saveMember();
        });
    });

    function addMemberToQueue() {
        if(memberAddMode == 'user') {
            addUserToQueue();
        } else if(memberAddMode == 'dept') {
            addDeptToQueue();
        } else if(memberAddMode == 'pos') {
            addPosToQueue();
        }
        // debugger;
    }

    function saveMember(){
        if(memberAddMode == 'user') {
            saveMemberUser();
        } else if(memberAddMode == 'dept') {
            saveMemberDept();
        } else if(memberAddMode == 'pos') {
            saveMemberPos();
        }
    }

    function saveMemberPos(){
        if(listSelectedPos.length == 0) {
            swal('Kesalahan', 'Anda belum memilih position satupun', 'error');
            return;
        }

        var doSave = function(){
            var f = {
                poss: listSelectedPos,
                menu: $('#modal_add_member #id_menu').val()
            };

            $.ajax({
                method: 'post',
                url: base_url + 'admin/master_menu/add_role_pos',
                data: {
                    data: JSON.stringify(f)
                },
                success: function(resp) {
                    try{
                        var res = JSON.parse(resp);
                        if(res == null) {
                            swal.showInputError('server tidak merespon');
                        } else if(res.status == true) {
                            swal('Berhasil', res.message, 'success');
                            $('#modal_add_member').modal('hide');
                            listSelectedDept = [];
                        } else if(res.status == false) {
                            swal.showInputError(res.message);
                        } else {
                            swal.showInputError('Respon server tidak dikenal');
                        }
                    }catch(ex) {
                        swal.showInputError('Gagal menerjemahkan respon server');
                    }
                },
                error: function(){
                    swal.showInputError('Kesalahan server/jaringan');
                }
            });
        }

        swal({
            type: 'warning',
            title: 'Konfirmasi Simpan Role Position',
            text: 'Anda akan menambahkan Position ke menu?',
            allowOutsideClick: false,
            showCancelButton: true,
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            allowEscapeKey: false
        }, function (v) {
            if(v == true) {
                doSave();
            }
        });
    }

    function saveMemberDept(){
        if(listSelectedDept.length == 0) {
            swal('Kesalahan', 'Anda belum memilih departement satupun', 'error');
            return;
        }

        var doSave = function(){
            var f = {
                depts: listSelectedDept,
                menu: $('#modal_add_member #id_menu').val()
            };

            $.ajax({
                method: 'post',
                url: base_url + 'admin/master_menu/add_role_dept',
                data: {
                    data: JSON.stringify(f)
                },
                success: function(resp) {
                    try{
                        var res = JSON.parse(resp);
                        if(res == null) {
                            swal.showInputError('server tidak merespon');
                        } else if(res.status == true) {
                            swal('Berhasil', res.message, 'success');
                            $('#modal_add_member').modal('hide');
                            listSelectedDept = [];
                        } else if(res.status == false) {
                            swal.showInputError(res.message);
                        } else {
                            swal.showInputError('Respon server tidak dikenal');
                        }
                    }catch(ex) {
                        swal.showInputError('Gagal menerjemahkan respon server');
                    }
                },
                error: function(){
                    swal.showInputError('Kesalahan server/jaringan');
                }
            });
        }

        swal({
            type: 'warning',
            title: 'Konfirmasi Simpan Role Dept',
            text: 'Anda akan menambahkan Dept ke menu?',
            allowOutsideClick: false,
            showCancelButton: true,
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            allowEscapeKey: false
        }, function (v) {
            if(v == true) {
                doSave();
            }
        });
    }

    function saveMemberUser(){
        if(listSelectedUser.length == 0) {
            swal('Kesalahan', 'Anda belum memilih satupun user', 'error');
            return;
        }

        var doSave = function(){
            var f = {
                users: listSelectedUser,
                menu: $('#modal_add_member #id_menu').val()
            };

            $.ajax({
                method: 'post',
                url: base_url + 'admin/master_menu/add_role_user',
                data: {
                    data: JSON.stringify(f)
                },
                success: function(resp) {
                    try{
                        var res = JSON.parse(resp);
                        if(res == null) {
                            swal.showInputError('server tidak merespon');
                        } else if(res.status == true) {
                            swal('Berhasil', res.message, 'success');
                            $('#modal_add_member').modal('hide');
                            listSelectedUser = [];
                        } else if(res.status == false) {
                            swal.showInputError(res.message);
                        } else {
                            swal.showInputError('Respon server tidak dikenal');
                        }
                    }catch(ex) {
                        swal.showInputError('Gagal menerjemahkan respon server');
                    }
                },
                error: function(){
                    swal.showInputError('Kesalahan server/jaringan');
                }
            });
        }

        swal({
            type: 'warning',
            title: 'Konfirmasi Simpan Member',
            text: 'Anda akan menambahkan User ke menu?',
            allowOutsideClick: false,
            showCancelButton: true,
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            allowEscapeKey: false
        }, function (v) {
            if(v == true) {
                doSave();
            }
        });
    }

    function addUserToQueue() {
        var selectedIdUser = $('#modal_add_member #select-user').val();
        var alreadyInQueue = false;
        for(var i = 0, i2 = listSelectedUser.length; i < i2; i++) {
            if(Number(listSelectedUser[i]) == Number(selectedIdUser)) {
                alreadyInQueue = true;
                break;
            }
        }
        if(alreadyInQueue == false) {
            listSelectedUser.push(selectedIdUser);
            renderSelectedUsers();
        }
    }

    function addPosToQueue() {
        var selectedIdPos = $('#modal_add_member #select-pos').val();
        var alreadyInQueue = false;
        for(var i = 0, i2 = listSelectedPos.length; i < i2; i++) {
            if(Number(listSelectedPos[i]) == Number(selectedIdPos)) {
                alreadyInQueue = true;
                break;
            }
        }
        if(alreadyInQueue == false) {
            listSelectedPos.push(selectedIdPos);
            renderSelectedPosition();
        }
        // debugger;
    }

    function addDeptToQueue(){
        var selectedIdDept = $('#modal_add_member #select-dept').val();
        var alreadyInQueue = false;
        for(var i = 0, i2 = listSelectedDept.length; i < i2; i++) {
            if(Number(listSelectedDept[i]) == Number(selectedIdDept)) {
                alreadyInQueue = true;
                break;
            }
        }
        if(alreadyInQueue == false) {
            listSelectedDept.push(selectedIdDept);
            renderSelectedDept();
        }
    }

    function renderSelectedDept(){
        var container = $('#modal_add_member #selected-member');
        container.html('');
        var counter = 1;
        for(var i = 0, i2 = listSelectedDept.length; i < i2; i++) {
            var idDept = listSelectedDept[i];
            var dept = null;
            for(var j = 0, j2 = listDept.length; j < j2; j++) {
                var u = listDept[j];
                if(Number(u.id_department) == Number(idDept)) {
                    dept = u;
                    break;
                }
            }
            if(dept == null) {
                continue;
            }
            var btnDel = $('<icon/>').addClass('material-icons').attr({id: dept.id_department}).css({'font-size': '15pt', color: 'red', cursor: 'pointer'}).html('delete');
            btnDel.on('click', function(e){
                var target = $(e.currentTarget);
                var idDept = target.attr('id');
                var index = listSelectedDept.indexOf(idDept);
                if(index >= 0) {
                    listSelectedDept.splice(index, 1);
                    renderSelectedDept();
                }
            });
            var m = $('<div/>').addClass('col-sm-12').html(counter + '. ' + dept.name ).append(btnDel);
            container.append(m);
            counter++;
        }
    }

    function renderSelectedPosition(){
        var container = $('#modal_add_member #selected-member');
        container.html('');
        var counter = 1;
        for(var i = 0, i2 = listSelectedPos.length; i < i2; i++) {
            var idPos = listSelectedPos[i];
            var pos = null;
            for(var j = 0, j2 = listPos.length; j < j2; j++) {
                var u = listPos[j];
                if(Number(u.id_position) == Number(idPos)) {
                    pos = u;
                    break;
                }
            }
            if(pos == null) {
                continue;
            }
            var btnDel = $('<icon/>').addClass('material-icons').attr({id: pos.id_position}).css({'font-size': '15pt', color: 'red', cursor: 'pointer'}).html('delete');
            btnDel.on('click', function(e){
                var target = $(e.currentTarget);
                var idPos = target.attr('id');
                var index = listSelectedPos.indexOf(idPos);
                if(index >= 0) {
                    listSelectedPos.splice(index, 1);
                    renderSelectedPosition();
                }
            });
            var m = $('<div/>').addClass('col-sm-12').html(counter + '. ' + pos.name_position ).append(btnDel);
            container.append(m);
            counter++;
        }
        // debugger;
    }

    function renderSelectedUsers(){
        var container = $('#modal_add_member #selected-member');
        container.html('');
        var counter = 1;
        for(var i = 0, i2 = listSelectedUser.length; i < i2; i++) {
            var idUser = listSelectedUser[i];
            var user = null;
            for(var j = 0, j2 = listUser.length; j < j2; j++) {
                var u = listUser[j];
                if(Number(u.id_user) == Number(idUser)) {
                    user = u;
                    break;
                }
            }
            if(user == null) {
                continue;
            }
            var btnDel = $('<icon/>').addClass('material-icons').attr({id: user.id_user}).css({'font-size': '15pt', color: 'red', cursor: 'pointer'}).html('delete');
            // var btnDel = $('<button/>').addClass('btn btn-danger').attr({id: user.id_user}).html(iconDel);
            btnDel.on('click', function(e){
                var target = $(e.currentTarget);
                var idUser = target.attr('id');
                var index = listSelectedUser.indexOf(idUser);
                if(index >= 0) {
                    listSelectedUser.splice(index, 1);
                    renderSelectedUsers();
                }
            });
            var m = $('<div/>').addClass('col-sm-12').html(counter + '. ' + user.name + ' (@' + user.username + ')').append(btnDel);
            container.append(m);
            counter++;
        }
    }

    function submitUpdate(){
        var f = {
            nama:   $('#modal_form #nama').val(),
            url:    $('#modal_form #url').val(),
            urutan: $('#modal_form #urutan').val(),
            id_menu: $('#modal_form #id_menu').val(),
            icon_text: $('#modal_form #icon_text').val(),
            icon_class: $('#modal_form #icon_class').val(),
            mobile_route: $('#modal_form #mobile_route').val(),
            mobile_icon: $('#modal_form #mobile_icon').val(),
            show_mobile_dashboard: $('#modal_form #show_mobile_dashboard').val(),
            show_mobile_drawer: $('#modal_form #show_mobile_drawer').val(),
            show_in_web: $('#modal_form #show_in_web').val(),
        };

        var doUpdate = function(){
            $.ajax({
                url: base_url + 'admin/master_menu/update_menu',
                method: 'post',
                data: {
                    data: JSON.stringify(f)
                },
                success: function(resp) {
                    try{
                        var res = JSON.parse(resp);
                        if(res == null) {
                            swal.showInputError('Server tidak merespon');
                        } else if(res.status == true) {
                            swal.close();
                            $('#modal_form').modal('hide');
                            dtMenu.ajax.reload(null, false);
                        } else if(res.status == false) {
                            swal.showInputError(res.message);
                        } else {
                            swal.showInputError('Respon server tidak dikenal');
                        }
                    }catch(ex){
                        swal.showInputError('Gagal menerjemahkan respon dari server');
                    }
                },
                error: function(){
                    swal.showInputError('Kesalahan di server/jaringan');
                }
            });
        }

        swal({
            type: 'warning',
            title: 'Update Menu',
            text: 'Anda akan mengupdate menu?',
            allowOutsideClick: false, 
            allowEscapeKey: false,
            showCancelButton: true,
            showLoaderOnConfirm: true,
            closeOnConfirm: false
        }, function(v) {
            if(v == true) {
                doUpdate();
            }
        });
    }

    function submitSave(){
        var f = {
            nama:   $('#modal_form #nama').val(),
            url:    $('#modal_form #url').val(),
            urutan:  $('#modal_form #urutan').val(),
            parent: parent_id,
            icon_text: $('#modal_form #icon_text').val(),
            icon_class: $('#modal_form #icon_class').val(),
            mobile_route: $('#modal_form #mobile_route').val(),
            mobile_icon: $('#modal_form #mobile_icon').val(),
            show_mobile_dashboard: $('#modal_form #show_mobile_dashboard').val(),
            show_mobile_drawer: $('#modal_form #show_mobile_drawer').val(),
            show_in_web: $('#modal_form #show_in_web').val(),
        };

        var doSave = function(){
            $.ajax({
                method: 'post',
                url: base_url + 'admin/master_menu/save_menu',
                data: {
                    data: JSON.stringify(f)
                },
                success: function (resp) {
                    var res = null;
                    try{
                        res = JSON.parse(resp);
                        if(res == null) {
                            swal.showInputError('server tidak merespon');
                        } else if(res.status == true) {
                            swal('Berhasil', res.message, 'success');
                            $('#modal_form').modal('hide');
                            dtMenu.ajax.reload(null, false);
                        } else if(res.status == false) {
                            swal.showInputError(res.message);
                        } else {
                            swal.showInputError('respon server tidak dikenal');
                        }
                    } catch (ex) {
                        // console.log('exception parse to json', ex);
                        swal.showInputError('Gagal menerjemahkan respon dari server');
                    }
                    
                },
                error: function(){
                    swal.showInputError('Kesalahan jaringan/server');
                }
            })
        }

        swal({
            type: 'warning',
            title: 'Simpan Menu',
            text: 'Simpan Menu?',
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            showCancelButton: true,
            allowEscapeKey: false,
            allowOutsideClick: false
        }, function(v) {
            if(v == true) {
                doSave();
            }
        })
    }

    function showFormAdd(){
        $('#modal_form').modal('show');
        $('#modal_form #btn-save').attr('save-mode', 'save');
        $('#modal_form #nama').val('');
        $('#modal_form #url').val('');
        $('#modal_form #urutan').val('');
    }

    function initDtMenu(){
        if(dtMenu != null) {
            dtMenu.destroy();
        }
        var offset = 0;
        dtMenu = $('#table_menu').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: base_url + 'admin/master_menu/dt_menu',
                data: {
                    parent: parent_id
                },
                dataSrc: function(resp){
                    offset = resp.offset;
                    return resp.data;
                },
                method: 'post'
            },
            order: [ [1,'asc'] ],
            columns: [
                {data: 'id_menu'},
                {data: 'urutan'},
                {data: 'nama', render: function(text,b,data,d){
                    // console.log('render nama menu',text,b,data,d);
                    return '<a href="' + base_url + 'admin/master_menu?parent=' + data.id_menu + '" style="color: blue">' + text + '</a>';
                }},
                {data: 'url'},
                {data: 'mobile_route'},
                {data: 'jumlah_submenu'},
                {data: 'icon_class'},
                {data: 'icon_text'},
                {data: 'mobile_icon'},
                {data: 'show_mobile_drawer', render: function(text,b,data){
                    if(text == '1'){
                        return 'Ya';
                    }
                    return 'Tidak';
                }},
                {data: 'show_mobile_dashboard', render: function(text){
                    if(text == '1') {
                        return 'Ya';
                    }
                    return 'Tidak';
                }},
                {data: 'show_in_web', render: function(text){
                    if(text == '1') {
                        return 'Ya';
                    }
                    return 'Tidak';
                }}
            ],
            oLanguage: dtLanguangeSetting,
            createdRow: function(row,data,index) {
                var iconTrash = $('<icon/>').addClass('material-icons').html('delete');
                // var iconTrash = $('<icon/>').addClass('notika-icon notika-trash').html('');
                var iconEdit = $('<icon/>').addClass('material-icons').html('create');
                // var iconEdit = $('<icon/>').addClass('notika-icon notika-plus-symbol').html('');
                var iconDetail = $('<icon/>').addClass('material-icons').html('description');
                var iconAccount = $('<icon/>').addClass('material-icons').html('account_box');
                var iconDept = $('<icon/>').addClass('material-icons').html('class');
                var iconPos = $('<icon/>').addClass('material-icons').html('note');

                var btnDel = $('<button/>').addClass('btn btn-danger')
                            .attr({type: 'button', index: index, title: 'Delete menu'})
                            .css({'margin-right': '5px'})
                            .html(iconTrash);
                var btnEdit = $('<button/>').addClass('btn btn-warning')
                            .attr({type: 'button', index: index, title: 'Edit menu'})
                            .css({'margin-right': '5px'})
                            .html(iconEdit);
                var btnDetail = $('<button/>').addClass('btn btn-success')
                            .attr({type: 'button', index: index, title: 'Show detail menu'})
                            .css({'margin-right': '5px'})
                            .html(iconDetail);
                var btnUser = $('<button/>').addClass('btn btn-info')
                            .attr({type: 'button', index: index, title: 'Manage role users'})
                            .css({'margin-right': '5px'})
                            .html(iconAccount);
                var btnDept = $('<button/>').addClass('btn btn-info')
                            .attr({type: 'button', index: index, title: 'Manage role department'})
                            .css({'margin-right': '5px'})
                            .html(iconDept);
                var btnPos = $('<button/>').addClass('btn btn-info')
                            .attr({type: 'button', index: index, title: 'Manage role position'})
                            .css({'margin-right': '5px'})
                            .html(iconPos);

                btnDel.on('click', function(e){
                    dialogDelete(e.currentTarget); 
                });
                btnUser.on('click', function(e){
                    showFormMemberUser(e.currentTarget);
                });
                btnDetail.on('click', function(e){
                    showDetailMenu(e.currentTarget);
                });
                btnEdit.on('click', function(e){
                    showFormEdit(e.currentTarget);
                });
                btnDept.on('click', function(e){
                    showFormMemberDept(e.currentTarget);
                });
                btnPos.on('click', function(e){
                    showFormMemberPosition(e.currentTarget);
                });

                $('td', row).eq(0).html(btnDel).append(btnEdit).append(btnDetail).append(btnUser).append(btnDept).append(btnPos);
            }
        });
    }

    function showFormEdit(node) {
        node = $(node);
        var index = Number(node.attr('index'));
        var menu = dtMenu.data()[index];

        if(menu == undefined || menu == null) {
            console.log('menu is not valid', menu);
            return;
        }

        $('#modal_form').modal('show');
        $('#modal_form #nama').val(menu.nama);
        $('#modal_form #url').val(menu.url);
        $('#modal_form #mobile_route').val(menu.mobile_route);
        $('#modal_form #urutan').val(menu.urutan);
        $('#modal_form #id_menu').val(menu.id_menu);
        $('#modal_form #icon_class').val(menu.icon_class);
        $('#modal_form #icon_text').val(menu.icon_text);
        $('#modal_form #mobile_icon').val(menu.mobile_icon);
        $('#modal_form #show_mobile_drawer').val(menu.show_mobile_drawer);
        $('#modal_form #show_mobile_dashboard').val(menu.show_mobile_dashboard);
        $('#modal_form #show_in_web').val(menu.show_in_web);
        $('#modal_form #btn-save').attr('save-mode','update');
    }

    function showDetailMenu(node) {
        node = $(node);
        var index = Number(node.attr('index'));
        var menu = dtMenu.data()[index];
        if(menu == undefined || menu == null ) {
            console.log('menu is not valid', menu);
            return;
        }

        var loading = $('<img/>').attr({src: base_url + 'assets/img/ajax-loader.gif'});

        $('#modal_detail_menu').modal('show');
        $('#modal_detail_menu #nama').val(menu.nama);
        $('#modal_detail_menu #url').val(menu.url);
        $('#modal_detail_menu #urutan').val(menu.urutan);

        var containerListUser = $('#modal_detail_menu #list-member-user');
        var containerListDept = $('#modal_detail_menu #list-member-dept');
        var containerListPos = $('#modal_detail_menu #list-member-pos');

        containerListUser.html(loading);
        containerListDept.html(loading);
        containerListPos.html(loading);

        $.ajax({
            url: base_url + 'admin/master_menu/list_role_user',
            data: {id_menu: menu.id_menu},
            success: function(resp) {
                try{
                    var res = JSON.parse(resp);
                    if(res.length == 0) {
                        containerListUser.html('Menu tidak memiliki role user');
                    } else {
                        containerListUser.html('');
                    }
                    var renderedUsers = [];
                    var counter = 0;
                    for(var i = 0, i2 = res.length; i < i2; i++) {
                        var user = res[i];
                        if(renderedUsers.indexOf(user.id_user) >= 0) {
                            continue;
                        }
                        renderedUsers.push(user.id_user);
                        counter++;
                        var ru = $('<div/>').attr({id: 'role-user-' + menu.id_menu + '-' + user.id_user}).addClass('col-sm-12').html(counter + ' ' + user.name + ' (@' + user.username + ')');
                        var btnHapus = $('<icon/>').addClass('material-icons').attr({id_menu: menu.id_menu, id_user: user.id_user}).css({color: 'red', cursor: 'pointer'}).html('delete');
                        btnHapus.on('click', function(e){
                            dialogDeleteRoleUser(e.currentTarget, function(v){
                                if(v.status == true) {
                                    $('#role-user-' + v.id_menu + '-' + v.id_user).remove();
                                }
                            });
                        });
                        ru.append(btnHapus)
                        containerListUser.append(ru);
                    }
                } catch (ex) {
                    containerListUser.html('Gagal menerjemahkan respon dari server');
                }
            },
            error: function(){
                containerListUser.html('Terjadi kesalahan di server/jaringan');
            }
        });

        $.ajax({
            url: base_url + 'admin/master_menu/list_role_dept',
            data: {id_menu: menu.id_menu},
            success: function(resp) {
                try{
                    var res = JSON.parse(resp);
                    if(res.length == 0) {
                        containerListDept.html('Menu tidak memiliki role dept');
                    } else {
                        containerListDept.html('');
                    }
                    var renderedDept = [];
                    var counter = 0;
                    for(var i = 0, i2 = res.length; i < i2; i++) {
                        var dept = res[i];
                        if(renderedDept.indexOf(dept.id_dept) >= 0) {
                            continue;
                        }
                        renderedDept.push(dept.id_dept);
                        counter++;
                        var ru = $('<div/>').attr({id: 'role-dept-' + menu.id_menu + '-' + dept.id_dept}).addClass('col-sm-12').html(counter + ' ' + dept.name);
                        var btnHapus = $('<icon/>').addClass('material-icons').attr({id_menu: menu.id_menu, id_dept: dept.id_dept}).css({color: 'red', cursor: 'pointer'}).html('delete');
                        btnHapus.on('click', function(e){
                            dialogDeleteRoleDept(e.currentTarget, function(v){
                                if(v.status == true) {
                                    $('#role-dept-' + v.id_menu + '-' + v.id_dept).remove();
                                }
                            });
                        });
                        ru.append(btnHapus)
                        containerListDept.append(ru);
                    }
                } catch (ex) {
                    containerListDept.html('Gagal menerjemahkan respon dari server');
                    console.log('exception render role dept', ex);
                }
            },
            error: function(){
                containerListDept.html('Terjadi kesalahan di server/jaringan');
            }
        });

        $.ajax({
            url: base_url + 'admin/master_menu/list_role_pos',
            data: {id_menu: menu.id_menu},
            success: function(resp) {
                try{
                    var res = JSON.parse(resp);
                    if(res.length == 0) {
                        containerListPos.html('Menu tidak memiliki role pos');
                    } else {
                        containerListPos.html('');
                    }
                    var renderedPos = [];
                    var counter = 0;
                    for(var i = 0, i2 = res.length; i < i2; i++) {
                        var pos = res[i];
                        if(renderedPos.indexOf(pos.id_pos) >= 0) {
                            continue;
                        }
                        renderedPos.push(pos.id_pos);
                        counter++;
                        var ru = $('<div/>').attr({id: 'role-pos-' + menu.id_menu + '-' + pos.id_pos}).addClass('col-sm-12').html(counter + ' ' + pos.name_position);
                        var btnHapus = $('<icon/>').addClass('material-icons').attr({id_menu: menu.id_menu, id_pos: pos.id_pos}).css({color: 'red', cursor: 'pointer'}).html('delete');
                        btnHapus.on('click', function(e){
                            dialogDeleteRolePos(e.currentTarget, function(v){
                                if(v.status == true) {
                                    $('#role-pos-' + v.id_menu + '-' + v.id_pos).remove();
                                }
                            });
                        });
                        ru.append(btnHapus)
                        containerListPos.append(ru);
                    }
                } catch (ex) {
                    containerListPos.html('Gagal menerjemahkan respon dari server');
                    console.log('exception render role dept', ex);
                }
            },
            error: function(){
                containerListPos.html('Terjadi kesalahan di server/jaringan');
            }
        });
    }

    function dialogDeleteRoleUser(node, cb) {
        node = $(node);
        var id_menu = node.attr('id_menu');
        var id_user = node.attr('id_user');
        var doDelete = function(){
            $.ajax({
                url: base_url + 'admin/master_menu/hapus_role_user',
                data: {
                    id_menu: id_menu,
                    id_user: id_user
                },
                success: function(resp) {
                    try{
                        var res = JSON.parse(resp);
                        if(res == null) {
                            swal.showInputError('Server tidak merespon');
                        } else if(res.status == true) {
                            swal.close();
                            cb({status: true, id_menu: id_menu, id_user: id_user});
                        } else if(res.status == false) {
                            swal.showInputError(res.message);
                        } else {
                            swal.showInputError('Respon server tidak dikenal');
                        }
                    } catch (ex) {
                        swal.showInputError('Gagal menerjemahkan respon dari server');
                    }
                },
                error: function(){
                    swal.showInputError('Kesalahan di server/jaringan');
                }
            });
        }
        swal({
            type: 'warning',
            title: 'Hapus Role User',
            text: 'Anda akan menghapus role user?',
            allowEscapeKey: false,
            allowOutsideClick: false,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            showCancelButton: true
        }, function(v) {
            if(v == true) {
                doDelete();
            }
        });
    }

    function dialogDeleteRoleDept(node, cb) {
        node = $(node);
        var id_menu = node.attr('id_menu');
        var id_dept = node.attr('id_dept');
        var doDelete = function(){
            $.ajax({
                url: base_url + 'admin/master_menu/hapus_role_dept',
                data: {
                    id_menu: id_menu,
                    id_dept: id_dept
                },
                success: function(resp) {
                    try{
                        var res = JSON.parse(resp);
                        if(res == null) {
                            swal.showInputError('Server tidak merespon');
                        } else if(res.status == true) {
                            swal.close();
                            cb({status: true, id_menu: id_menu, id_dept: id_dept});
                        } else if(res.status == false) {
                            swal.showInputError(res.message);
                        } else {
                            swal.showInputError('Respon server tidak dikenal');
                        }
                    } catch (ex) {
                        swal.showInputError('Gagal menerjemahkan respon dari server');
                    }
                },
                error: function(){
                    swal.showInputError('Kesalahan di server/jaringan');
                }
            });
        }
        swal({
            type: 'warning',
            title: 'Hapus Role Dept',
            text: 'Anda akan menghapus role dept?',
            allowEscapeKey: false,
            allowOutsideClick: false,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            showCancelButton: true
        }, function(v) {
            if(v == true) {
                doDelete();
            }
        });
    }

    function dialogDeleteRolePos(node, cb) {
        node = $(node);
        var id_menu = node.attr('id_menu');
        var id_pos = node.attr('id_pos');
        var doDelete = function(){
            $.ajax({
                url: base_url + 'admin/master_menu/hapus_role_pos',
                data: {
                    id_menu: id_menu,
                    id_pos: id_pos
                },
                success: function(resp) {
                    try{
                        var res = JSON.parse(resp);
                        if(res == null) {
                            swal.showInputError('Server tidak merespon');
                        } else if(res.status == true) {
                            swal.close();
                            cb({status: true, id_menu: id_menu, id_pos: id_pos});
                        } else if(res.status == false) {
                            swal.showInputError(res.message);
                        } else {
                            swal.showInputError('Respon server tidak dikenal');
                        }
                    } catch (ex) {
                        swal.showInputError('Gagal menerjemahkan respon dari server');
                    }
                },
                error: function(){
                    swal.showInputError('Kesalahan di server/jaringan');
                }
            });
        }
        swal({
            type: 'warning',
            title: 'Hapus Role pos',
            text: 'Anda akan menghapus role pos?',
            allowEscapeKey: false,
            allowOutsideClick: false,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            showCancelButton: true
        }, function(v) {
            if(v == true) {
                doDelete();
            }
        });
    }

    function showFormMemberDept(node) {
        node = $(node);
        var index = Number(node.attr('index'));
        var menu = dtMenu.data()[index];
        if(menu == undefined || menu == null ) {
            console.log('menu is not valid', menu, index, node);
            return;
        }
        memberAddMode = 'dept';
        swal({
            type: 'info',
            title: 'Memuat data',
            text: 'Mohon tunggu',
            allowEscapeKey: false,
            allowOutsideClick: false,
            showConfirmButton: false
        });

        var select_dept = $('<select/>').addClass('form-control').attr({id: 'select-dept'}).css({width: '100%'});
        var label = $('<label/>').addClass('form-label').html('Pilih Departemen');
        var formLine = $('<div/>').addClass('form-line focused').html(select_dept).append(label);
        var formGroup = $('<div/>').addClass('form-group form-float').html(formLine);
        var colsm12 = $('<div/>').addClass('col-sm-12').html(formGroup);
        var idMenu = $('<input/>').attr({type: 'hidden', id: 'id_menu'}).val(menu.id_menu);
        $('#modal_add_member #form').html(colsm12).append(idMenu);

        renderSelectedDept();

        var modal_add_member = $('#modal_add_member');
        select_dept.select2({ dropdownParent: modal_add_member });

        $.ajax({
            url: base_url + 'admin/master_menu/list_dept',
            success: function(resp) {
                let res = [];
                try{
                    res = JSON.parse(resp);
                    $.each(res, function(index, item){
                        select_dept.append(
                            $('<option/>').attr({value: item.id_department}).html(item.name)
                        );
                    });
                    swal.close();
                    $('#modal_add_member').modal('show');
                    listDept = res;
                } catch(ex) {
                    swal('Kesalahan', 'Kesalahan menerjemahkan respon dari server', 'error');
                    console.log('exception', resp, ex);
                }
            },
            error: function(){
                swal('Kesalahan', 'Kesalahan di server/jaringan', 'error');
            }
        });
    }

    function showFormMemberUser(node){
        node = $(node);
        var index = node.attr('index');
        var menu = dtMenu.data()[index];
        if(menu == undefined || menu == null) {
            console.log('menu is not valid', menu, index, node);
            return;
        }
        memberAddMode = 'user';
        swal({
            type: 'info',
            title: 'Memuat data',
            text: 'Mohon tunggu',
            allowEscapeKey: false,
            allowOutsideClick: false,
            showConfirmButton: false
        });

        var select_user = $('<select/>').addClass('form-control').attr({id: 'select-user', title: 'Role User'}).css({width: '100%'});
        var label = $('<label/>').addClass('form-label').html('Pilih User');
        var formLine = $('<div/>').addClass('form-line focused').html(select_user).append(label);
        var formGroup = $('<div/>').addClass('form-group form-float').html(formLine);
        var colsm12 = $('<div/>').addClass('col-sm-12').html(formGroup);
        var idMenu = $('<input/>').attr({type: 'hidden', id: 'id_menu'}).val(menu.id_menu);
        $('#modal_add_member #form').html(colsm12).append(idMenu);
        renderSelectedUsers();

        var modal_add_member = $('#modal_add_member');
        select_user.select2({ dropdownParent: modal_add_member });

        $.ajax({
            url: base_url + 'admin/master_menu/list_user',
            success: function(resp) {
                let res = [];
                try{
                    res = JSON.parse(resp);
                    $.each(res, function(index, user){
                        select_user.append(
                            $('<option/>').attr({value: user.id_user}).html(user.name + ' (@' + user.username + ')')
                        );
                    });
                    swal.close();
                    $('#modal_add_member').modal('show');
                    listUser = res;
                } catch(ex) {
                    swal('Kesalahan', 'Kesalahan menerjemahkan respon dari server', 'error');
                    console.log('exception', resp, ex);
                }
            },
            error: function(){
                swal('Kesalahan', 'Kesalahan di server/jaringan', 'error');
            }
        });
    }

    function showFormMemberPosition(node){
        node = $(node);
        var index = node.attr('index');
        var menu = dtMenu.data()[index];
        if(menu == undefined || menu == null) {
            console.log('menu is not valid', menu, index, node);
            return;
        }
        memberAddMode = 'pos';
        swal({
            type: 'info',
            title: 'Memuat data',
            text: 'Mohon tunggu',
            allowEscapeKey: false,
            allowOutsideClick: false,
            showConfirmButton: false
        });

        var select_pos = $('<select/>').addClass('form-control').attr({id: 'select-pos', title: 'Role position'}).css({width: '100%'});
        var label = $('<label/>').addClass('form-label').html('Pilih Position');
        var formLine = $('<div/>').addClass('form-line focused').html(select_pos).append(label);
        var formGroup = $('<div/>').addClass('form-group form-float').html(formLine);
        var colsm12 = $('<div/>').addClass('col-sm-12').html(formGroup);
        var idMenu = $('<input/>').attr({type: 'hidden', id: 'id_menu'}).val(menu.id_menu);
        $('#modal_add_member #form').html(colsm12).append(idMenu);
        renderSelectedPosition();

        var modal_add_member = $('#modal_add_member');
        select_pos.select2({ dropdownParent: modal_add_member });

        $.ajax({
            url: base_url + 'admin/master_menu/list_position',
            success: function(resp) {
                let res = [];
                try{
                    res = JSON.parse(resp);
                    $.each(res, function(index, item){
                        select_pos.append(
                            $('<option/>').attr({value: item.id_position}).html(item.name_position)
                        );
                    });
                    swal.close();
                    $('#modal_add_member').modal('show');
                    listPos = res;
                } catch(ex) {
                    swal('Kesalahan', 'Kesalahan menerjemahkan respon dari server', 'error');
                    console.log('exception', resp, ex);
                }
            },
            error: function(){
                swal('Kesalahan', 'Kesalahan di server/jaringan', 'error');
            }
        });
    }

    function dialogDelete(node) {
        node = $(node);
        var index = node.attr('index');
        var data = dtMenu.data()[index];

        if(data == undefined || data == null) {
            console.log('menu is not valid', data, index, node);
            return;
        }

        var doDelete = function(){
            $.ajax({
                method: 'post',
                url: base_url + 'admin/master_menu/hapus_menu',
                data: {
                    id: data.id_menu
                },
                success: function(resp) {
                    var res = null;
                    try{
                        res = JSON.parse(resp);
                    } catch(ex) {
                        console.log('exception delete respon', resp);
                    }
                    if(res == null) {
                        swal.showInputError('server tidak merespon');
                    } else if(res.status == true) {
                        swal('Berhasil', res.message, 'success');
                        dtMenu.ajax.reload(null, false);
                    } else if(res.status == false) {
                        swal.showInputError(res.message);
                    } else {
                        swal.showInputError('respon server tidak dikenal');
                    }
                },
                error: function(){
                    swal.showInputError('kesalahan jaringan/server');
                }
            });
        }

        swal({
            type: 'warning',
            title: 'Hapus',
            text: 'Hapus menu?',
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            showCancelButton: true,
            allowOutsideClick: false,
            allowEscapeKey: false
        }, function(v){
            if(v == true) {
                doDelete();
            }
        })
    }
</script>