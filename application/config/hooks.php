<?php
defined('BASEPATH') OR exit('No direct script access allowed');


// Security improvments when using SSL
$hook['post_controller'][] = function()
{
	// Check if the base url starts with HTTPS
	if(substr(base_url(), 0, 5) !== 'https'){
		return;
	}

	// If we are not using HTTPS and not in CLI
	if(!is_https() && !is_cli()){
		// Redirect to the HTTPS version
		redirect(base_url(uri_string()));
	}

	// Get CI instance
	$CI =& get_instance();

	// Only allow HTTPS cookies (no JS)
	$CI->config->set_item('cookie_secure', TRUE);
	$CI->config->set_item('cookie_httponly', TRUE);
	
	// Set headers
	$CI->output->set_header("X-XSS-Protection: 1; mode=block")
				->set_header("Content-Security-Policy: default-src 'self'; script-src 'nonce-{NONCE}'; img-src www.gstatic.com; frame-src www.google.com; object-src 'none'; base-uri 'none';")
				->set_header("X-Frame-Options: SAMEORIGIN")
			    ->set_header("X-Content-Type-Options: nosniff")
			   	->set_header("Strict-Transport-Security:maxage=16070400;includeSubDomains")
			   	->set_header("Referrer-Policy: no-referrer") 
			   	->set_header("X-Permitted-Cross-Domain-Policies: none");
			   
};