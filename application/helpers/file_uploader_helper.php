<?php

function start_upload($conf) {
    $key = empty($conf['key']) ? 'file' : $conf['key'];
    $mode = empty($conf['mode']) ? 'single' : $conf['mode'];
    $table_fk = empty($conf['table_fk']) ? 'file' : $conf['table_fk'];
    $id = empty($conf['id']) ? -1 : $conf['id'];
    $user_id = empty($conf['creator_id']) ? null : $conf['creator_id'];

    $ci =& get_instance();

    $ci->load->model('m_attachment');
    $config = [
        'upload_path' => './uploads/attachments/' . $table_fk . '/' . $id . '/',
        'allowed_types' => '*',
        'encrypt_name' => true
    ];

    // print_r($config);

    if(file_exists($config['upload_path']) == false) {
        $m = umask(0);
        @mkdir($config['upload_path'], 0777, true);
        @umask($m);
    }

    $ci->upload->initialize($config);

    if($mode == 'single') {
        if($ci->upload->do_upload($key)) {
            $uploaded = $ci->upload->data();
            $ci->m_attachment->add_attachment([
                'table_fk' => $table_fk,
                'id_fk' => $id,
                'path' => $config['upload_path'] . $uploaded['file_name'],
                'name' => $uploaded['orig_name'],
                'created_at' => date('Y-m-d H:i:s'),
                'filetype' => $uploaded['file_type'],
                'filesize' => $uploaded['file_size'],
                'title' => $uploaded['orig_name'],
                'description' => $uploaded['orig_name'],
                'creator_id' => $user_id
            ]);
            // echo "uploaded";
            return ['status' => true, 'message' => 'success'];
        }
        else {
            // echo "gagal upload";
            // echo $ci->upload->display_errors();
            return ['status' => false, 'message' => $ci->upload->display_errors()];
        }
    }
}