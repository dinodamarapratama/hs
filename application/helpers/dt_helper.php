<?php

function getDataTable($args) {
    $sql = '';
    $sql2 = "SET @no_urut = 0; \n\n ";
    $order_subs = array();
    $defaultOrder = isset($args["default_order"]) ? $args["default_order"] : [];
    if(isset($args['sql'])){
       $sql = $args['sql'];
    }
    if(isset($args['order_subs'])){
        $order_subs = $args['order_subs'];
    }
    if(is_array($defaultOrder) == false) {
        $defaultOrder = [];
    }
    
    $ci = & get_instance();

    $columns        = $ci->input->get_post('columns');
    $order          = $ci->input->get_post('order');
    $draw           = intval($ci->input->get_post('draw'));
    $filter         = $ci->input->get_post('search');
    $offset         = intval($ci->input->get_post('start'));
    $take           = intval($ci->input->get_post('length'));
    $dataVersion    = intval($ci->input->get_post("data_version"));
    $custom_filter  = isset($args['custom_filter'])?$args['custom_filter']:[]; 
    // $disable_paging = (isset($args['disable_paging'])&&$args['disable_paging']==1)?$take = "-1":'';

    /*custom pagination format response*/
    /*indikasi kirim dari jq easyui grid*/
    $page =  $ci->input->get_post('page'); 
    if (!empty($page)) {
        $limit = $ci->input->get_post('rows');
        $offset = (($page-1)*$limit);
        $take = $limit;
    }

    /*indikasi kirim dari api*/
    if (isset($args['rest_paging']) && !empty($args['rest_paging'])) {
        $limit =  $args['rest_paging']['rows']; 
        $page =  $args['rest_paging']['page'];
        $offset = (($page-1)*$limit);
        $take = $limit;
    }
    /**/

    if (isset($args['disable_paging'])&&$args['disable_paging']==1) {
        $take = "-1";
    }
    /**/
    
    if($order == null) {
        $order = [];
    }

    if($dataVersion == 2) {
        $order = json_decode($order, true);
    } else {
        if(is_array($order) == false) {
            $order = [$order];
        }
    }
    $filter_order = [];
    if($dataVersion == 2) {
        foreach ($order as $index => $orderConf) {
            if(isset($orderConf["column"])){
                $column = $orderConf["column"];
                
                if(isset($order_subs[$column])) {
                    $column = $order_subs[$column];
                }
                
                if(isset($orderConf["dir"])) {
                    $filter_order[$column] = $orderConf["dir"];
                } else {
                    $filter_order[$column] = "asc";
                }
            }
        }
    }else {
        if (!empty($ci->input->get_post('sort'))) {
            /*indikasi short dari easyui(qc) */
            $column_sort = $ci->input->get_post('sort');
            $direction_sort = $ci->input->get_post('order');
            $filter_order[$column_sort] = $direction_sort;
            /**/
        }else{
            if (count($order) > 0) {
                foreach ($order as $order1) {
                    $c = $columns[$order1['column']]['data'];
                    $d = $order1['dir'];

                    if (isset($order_subs [$c])) {
                        $c = $order_subs [$c];
                    }

                    if(isset($defaultOrder[$c])) {
                        unset($defaultOrder[$c]);
                    }
                    
                    $filter_order[$c] = $d;

                }
            }

            foreach ($defaultOrder as $col => $dir) {
                $filter_order[$col] = $dir;
            }
        }
        
    }

    $offset = max(0, $offset);
    
    if($take == "-1"){
        /** select all */
        $take = null;
        $offset = null;
    } else {
        /** dengan limit */
        $take = max(10, $take);
    }
    

    $filter_or_like = [];

    if($dataVersion == 2){
        $filter = json_decode($filter, true);
        $cols = isset($filter["cols"]) ? $filter["cols"] : [];
        $key = isset($filter["key"]) ? $filter["key"] : "";
        foreach($cols as $col) {
            $filter_or_like[$col] = $key;
        }
    } else {
        if (strlen($filter['value']) > 0) {
            foreach ($columns as $column) {
                if ($column['searchable'] == 'true' ) {
                    // $filter_or_like[$column["data"]] =  $column["value"];
                    $filter_or_like[$column["data"]] = $filter["value"];
                }
            }
        }
    }

    $countFiltered = $countAllRows = $ci->db->from("($sql) t1")->get()->num_rows();

    // $filter_or_like = array_merge($filter_or_like,$custom_filter);
    if (count($filter_or_like) > 0 || count($custom_filter) > 0) {
        $q = $ci->db->from("($sql) t1");
        $q->group_start();
        if (!empty($filter_or_like)) {
            foreach ($filter_or_like as $col => $val) {
                $q->or_like($col, $val, "both");
            }
        }

        if (!empty($custom_filter)) {
            foreach ($custom_filter as $col => $val) {
                $q->like($col, $val);
            }
        }
        
        $q->group_end();
        $countFiltered = $q->get()->num_rows();
    }

    // $ci->db->query($sql2);
    
    $q = $ci->db->from("($sql) t1");
    if (count($filter_or_like) > 0 || count($custom_filter) > 0) {
        $q->group_start();
         if (!empty($filter_or_like)) {
            foreach ($filter_or_like as $col => $val) {
                $q->or_like($col, $val, "both");
            }
        }

        if (!empty($custom_filter)) {
            foreach ($custom_filter as $col => $val) {
                $q->like($col, $val);
            }
        }
        $q->group_end();
    }
    foreach ($filter_order as $col => $dir) {
        $q->order_by($col, $dir);
    }
    
    $ci->db->trans_begin();
    $q->limit($take, $offset);

    $sql = "";

    // if(in_array($ci->db->dbdriver, ["mysqli"])) {
        
    //     $q->select([
    //         "@no_urut:=@no_urut + 1 as rnum",
    //          "t1.*"
    //     ]);
    //     $sql = $q->get_compiled_select();
    //     // $sql = $sql2 . $sql . ";";
    // } else {
    //     $q->select([
    //         "t1.*"
    //     ]);
    //     $sql = $q->get_compiled_select();
    // }
    $sql = $q->get_compiled_select();
    
    $r = $ci->db->query($sql)->result_array();
    // $ci->db->trans_complete();
    $result = [
        'draw' => $draw,
        'recordsTotal' => $countAllRows,
        'recordsFiltered' => $countFiltered,
        'data' => $r,
        'csrfName' => $ci->security->get_csrf_token_name(),
        'csrfHash' => $ci->security->get_csrf_hash(),
        'sql' => $sql,
        "driver" => $ci->db->dbdriver,
        "take" => $take,
        "offset" => $offset
    ];

    return $result;
}
