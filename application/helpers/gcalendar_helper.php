<?php

function gc_token_path() {
    return 'gcalendar-token.json';
}

function gc_calendar_id(){
    return 'primary';
}

function gc_get_client(){
	$ci =& get_instance();

	require FCPATH . '/vendor/autoload.php';

	$client = new Google_Client();
    $client->setApplicationName('Google Calendar API PHP Quickstart');
    // $client->setScopes(Google_Service_Calendar::CALENDAR_READONLY);
    $client->setScopes(Google_Service_Calendar::CALENDAR);
    // $client->setAuthConfig('gcalendar-credentials.json');
    // $client->setAuthConfig('gcalendar-client-dev.json');
    $client->setAuthConfig('gcalendar-client-prod.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    
    if (file_exists( gc_token_path() )) {
        $accessToken = json_decode(file_get_contents( gc_token_path() ), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        try{
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                $authUrl = $client->createAuthUrl();
                return [
                    'status' => 1,
                    'url' => $authUrl,
                    'client' => $client
                ];
            }
        }
        catch( Exception $exc) {
            return [
                'status' => 2,
                'client' => null
            ];
        }

        // if ($client->getRefreshToken()) {
        //     $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        // } else {
        //     // Request authorization from the user.
        //     $authUrl = $client->createAuthUrl();
        //     return [
        //         'status' => 1,
        //         'url' => $authUrl,
        //         'client' => $client
        //     ];
        //     // printf("Open the following link in your browser:\n%s\n", $authUrl);
        //     // print 'Enter verification code: ';
        //     // $authCode = trim(fgets(STDIN));

        //     // Exchange authorization code for an access token.
        //     // $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
        //     // $client->setAccessToken($accessToken);

        //     // // Check to see if there was an error.
        //     // if (array_key_exists('error', $accessToken)) {
        //     //     throw new Exception(join(', ', $accessToken));
        //     // }
        // }
        gc_save_token($client->getAccessToken());
    }
    // return $client;
    return [
        'status' => 0,
        'client' => $client,
        'conf_file' => gc_token_path()
    ];
}

function gc_save_token($token) {
    $path = gc_token_path();
    // Save the token to a file.
    // if (!file_exists(dirname($path))) {
    //     $u = umask(0);
    //     mkdir(dirname($path), 0700, true);
    //     umask($u);
    // }
    // if (!file_exists($path)) {
    //     $u = umask(0);
    //     // mkdir($path, 0700, true);
    //     umask($u);
    // }
    file_put_contents($path, json_encode($token));
    return [
        'status' => 0,
        'message' => 'Success'
    ];
}

function gc_set_token($token) {
    $r = gc_get_client();
    if($r['status'] == 1) {
        $client = $r['client'];
        

        $accessToken = $client->fetchAccessTokenWithAuthCode($token);
        echo "access token: ";
        print_r($accessToken);

        $client->setAccessToken($accessToken);


        gc_save_token($client->getAccessToken());

        // // Check to see if there was an error.
        // if (array_key_exists('error', $accessToken)) {
        //     throw new Exception(join(', ', $accessToken));
        // }
    }

    return ['status' => 0];
}

function gc_config(){
    $r = gc_get_client();
    return $r;
}

function gc_test(){
	// Get the API client and construct the service object.
	// $client = gc_get_client();
    $t = gc_get_client();
    $client = null;
    if($t['status'] == 0) {
        $client = $t['client'];
        $service = new Google_Service_Calendar($client);

        // Print the next 10 events on the user's calendar.
        $calendarId = gc_calendar_id();
        $optParams = array(
          'maxResults' => 10,
          'orderBy' => 'startTime',
          'singleEvents' => true,
          'timeMin' => date('c'),
        );
        $results = $service->events->listEvents($calendarId, $optParams);
        $events = $results->getItems();

        if (empty($events)) {
            print "No upcoming events found.\n";
        } else {
            print "Upcoming events:\n";
            foreach ($events as $event) {
                $start = $event->start->dateTime;
                if (empty($start)) {
                    $start = $event->start->date;
                }
                printf("%s (%s)\n", $event->getSummary(), $start);
            }
        }
    }
	else if($t['status'] == 1){
        echo $t['url'];
    }
}

function gc_add_event($data) {
    $emails = empty($data['emails']) ? [] : $data['emails'];
    $users = empty($data['users']) ? [] : $data['users'];
    $id_users = empty($data['id_users']) ? [] : $data['id_users'];

    if(count($id_users) > 0) {
        $ci = & get_instance();
        $ci->load->model('restapi/user_model');
        $rows = $ci->user_model->many_user(['where_in' => ['_.id_user', $id_users]  ]);

        foreach ($rows as $key => $row) {
            $users[] = $row;
        }
    }

    foreach ($users as $key => $user) {
        $emails[] = $user['email'];
    }

    $temp_email = [];
    foreach ($emails as $key => $em) {
        if(empty($em)) {
            continue;
        }
        if(in_array($em, $temp_email)) {
            continue;
        }
        $temp_email[] = $em;
    }

    $emails = $temp_email;

    $r = gc_get_client();
    if($r['status'] == 0) {
        $client = $r['client'];
        $conf = [
            'summary' => $data['title'],
            'description' => $data['description'],
            'start' => [
                'dateTime' => date('c', strtotime($data['start']))
            ],
            'end' => [
                'dateTime' => date('c', strtotime($data['end']))
            ],
            // 'attendees' => $emails
        ];
        if(count($emails) > 0) {
            $att = [];
            foreach ($emails as $key => $email) {
                $att[] = [
                    'email' => $email
                ];
            }
            $conf['attendees'] = $att;
        }
        $event = new Google_Service_Calendar_Event($conf);

        $service = new Google_Service_Calendar($client);
        $res = $service->events->insert(gc_calendar_id(), $event);

        return ['status' => 0, 'message' => '', 'res' => $res];
    }

    return ['status' => 1, 'message' => 'google client not ready'];
}