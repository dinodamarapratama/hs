<?php

function myquery($query, $args) {
	if($args == null) {
		$args = [];
	}

	// print_r($args);
	// exit();

	if(isset($args["where"])) {
		$query->where($args["where"]);
	}

	if(isset($args['where_in'])) {
		$listWhereIn = $args['where_in'];
		if(is_array($listWhereIn) == false) {
			$listWhereIn = [ $listWhereIn ];
		}

		foreach ($listWhereIn as $col_name => $values) {
			if(is_array($values) == false) {
				$values = [ $values ];
			}
			if(count($values) > 0) {
				$query->where_in($col_name, $values);
			}
			
		}
	}

	if(isset($args['where_not_in'])) {
		$listWhereNotIn = $args['where_not_in'];
		if(is_array($listWhereNotIn) == false) {
			$listWhereNotIn = [ $listWhereNotIn ];
		}

		foreach ($listWhereNotIn as $col_name => $values) {
			if(is_array($values) == false) {
				$values = [ $values ];
			}
			if(count($values) > 0) {
				$query->where_not_in($col_name, $values);
			}
			
		}
	}

	if(isset($args["order"])) {
		$order = $args["order"];
		// print_r($order);
		// exit();
		if(is_array($order)) {
			foreach ($order as $key => $ord) {
				if(is_array($ord)) {
					$countOrd = count($ord);
					if($countOrd > 1) {
						$query->order_by($ord[0], $ord[1]);
					} else if($countOrd > 0) {
						$query->order_by($ord[0]);
					}
				}
			}
		}
	}

	if(isset($args["limit"])) {
		$limit = $args["limit"];
		if(is_array($limit)){
			$countLimit = count($limit);
			if($countLimit > 1) {
				$query->limit(intval($limit[1]), intval($limit[0]));
			} else if($countLimit > 0) {
				$query->limit(intval($limit[0]));
			}
		}
	}

	// print_r($args);

	if(isset($args['like'])) {
		$likes = $args['like'];
		if(is_array($likes) == false) {
			$likes = [];
		}

		// echo 'likes: ';
		// print_r($likes);

		foreach ($likes as $key => $like) {
			// echo 'like: ';
			// print_r($like);
			if(is_array($like) == false) {
				$like = [];
			}
			$cols = isset($like['cols']) ? $like['cols'] : [];
			$search = isset($like['search']) ? $like['search'] : '';
			// echo 'cols: ';
			// print_r($cols);
			// echo 'search:  ' . $search;
			if(is_array($cols) && strlen($search) > 0) {
				$query->group_start();
				foreach ($cols as $key => $col) {
					$query->or_like($col, $search);
				}
				$query->group_end();
			}
		}
	}

	// echo $query->get_compiled_select();
	// exit();

	return $query;
}