<?php


function __clean_text($text = "", $leftPad = "")
{
    $text = trim($text);
    $text = str_replace("<p>", "", $text);
    $text = str_replace("</p>", "", $text);
    $text = str_replace("<u>", "", $text);
    $text = str_replace("</u>", "", $text);
    $text = str_replace("<strong>", "", $text);
    $text = str_replace("</strong>", "", $text);
    $text = str_replace("<em>", "", $text);
    $text = str_replace("</em>", "", $text);
    $text = str_replace("<sub>", "", $text);
    $text = str_replace("</sub>", "", $text);
    $text = str_replace("<s>", "", $text);
    $text = str_replace("</s>", "", $text);
    $text = str_replace("\r\n\r\n", "\r\n", $text);
    $text = str_replace("\r\n", "\r\n$leftPad", $text);
    $text = str_replace("&nbsp;", " ", $text);
    return $text;
}

// clean string htmletites decodede
function __clean_text_htmlentites($string)
{
    $new_string = html_entity_decode($string);
    return strip_tags($new_string);
}
