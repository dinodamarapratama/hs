<?php

class M_absensi extends CI_Model {

    function save($data, $id_receivers = [], $dept_receiver = [], $pos_receiver = [], $bagian_receiver = [], $file_prefix = 'file_') {
        $now = date('Y-m-d H:i:s');
        if(empty($data['created_at'])) {
            $data['created_at'] = $now;
        }
        if(empty($data['updated_at'])) {
            $data['updated_at'] = $now;
        }
        if(empty($data['absensi_des'])) {
            $data['absensi_des'] = '1';
        }
        if(empty($data['from_id'])) {
            return ['status' => false, 'message' => 'field sender untuk absensi report diperlukan'];
        }

        $send_notif = false;
        if(array_key_exists('send_notif', $data)){
            $send_notif = $data['send_notif'];
            unset($data['send_notif']);
        }

        if(is_array($id_receivers) == false) {
            $id_receivers = [ $id_receivers ];
        }
        if(is_array($dept_receiver) == false) {
            $dept_receiver = [ $dept_receiver ];
        }
        if(is_array($pos_receiver) == false) {
            $pos_receiver = [ $pos_receiver ];
        }
        if(is_array($bagian_receiver) == false) {
            $bagian_receiver = [ $bagian_receiver ];
        }

        $this->load->model('restapi/user_model');
        $user_dept = [];
        if(count($dept_receiver) > 0) {
            $user_dept = $this->user_model->many_user(['where_in' => ['dep.id_department' => $dept_receiver]]);

            foreach ($user_dept as $key => $user) {
                $id_receivers[] = $user['id_user'];
            }
        }
        $user_pos = [];
        if(count($pos_receiver) > 0) {
            $user_pos = $this->user_model->many_user(['where_in' => ['pos.id_position' => $pos_receiver]]);

            foreach ($user_pos as $key => $user) {
                $id_receivers[] = $user['id_user'];
            }
        }
        $user_bagian = [];
        if(count($bagian_receiver) > 0) {
            $user_bagian = $this->user_model->many_user(['where_in' => ['bg.id' => $bagian_receiver]]);

            foreach ($user_bagian as $key => $user) {
                $id_receivers[] = $user['id_user'];
            }
        }

        $temp = [];
        foreach ($id_receivers as $key => $id_receiver) {
            if(in_array($id_receiver, $temp)){
                continue;
            }
            $temp[] = $id_receiver;
        }
        $id_receivers = $temp;

        if(count($id_receivers) == 0) {
            return ['status' => false, 'message' => 'Receiver belum diisi'];
        }

        $this->db->insert('absensi', $data);
        $id = $this->db->insert_id();

        if(empty($id)) {
            return ['status' => false, 'message' => 'ID simpan tidak valid'];
        }

        $this->load->model('m_receiver');
        $this->m_receiver->add_receiver($id, 'absensi', $id_receivers);

        $data = $this->one(['where' => ['_.id' => $id]]);

        if( $send_notif == true && in_array($data['absensi_des'], ['','1']) == false) {
            $sender = $this->user_model->one_user(['where' => ['_.id_user' => $data['from_id']]]);
            // $user_hrd = $this->user_model->get_user_hrd();
            // $id_user_hrd = [];
            // foreach ($user_hrd as $key => $user) {
                // $id_user_hrd[] = $user['id_user'];
            // }
            $this->load->library('Notify_lib');
            $ntf = new Notify_lib();
            $ntf->send(
                'New Absensi Report',
                //$sender['name'] . ' has Sent new Absensi Report',
				$sender['name'] . ' - '.$sender['position_name'].' ('.$sender['branch_name'].') has Sent new Absensi Report',
                $sender['id_user'],
                $id_receivers,
                ['id' => $data['id']],
                'absensi',
                true
            );
        }

        return ['status' => true, 'message' => 'Data telah disimpan', 'data' => $data, 'id' => $id];
    }

    function baseQuery($args = []) {
        $q = $this->db->from('absensi _')
            ->select([
                '_.*'
            ]);

        return $q;
    }

    function many($args = []) {
        $this->load->helper('myquery');
        $q = myquery($this->baseQuery($args), $args);
        return $q->get()->result_array();
    }

    function one($args) {
        if(empty($args) || $args == null) {
            $args = [];
        }
        $args['limit'] = [1];
        $q = $this->many($args);
        if(count($q) > 0) {
            return $q[0];
        }
        return null;
    }

    function reply($data) {
        // echo "m absensi, data:";
        // print_r($data);
        $absensi = $this->one(['where' => ['_.id' => $data['id']]]);
        // echo "m absensi: absesi:";
        // print_r($absensi);
        if($absensi == null) {
            return ['status' => false, 'message' => 'Absensi not found'];
        }

        $this->load->model('m_reply');
        $r = $this->m_reply->save_reply($data['user_id'], 'absensi', $absensi['id'], $data['message']);

        if($r['status'] == true) {
            $this->load->library('Notify_lib');
            $this->load->model('restapi/user_model');
            $sender = $this->user_model->one_user(['where' => ['_.id_user' => $data['user_id']]]);
            $sender_name = '';
            $position_name = '';
			$branch_name = '';
            if($sender != null) {
                $sender_name = $sender['name'];
				$position_name = $sender['position_name'];
				$branch_name = $sender['branch_name'];
            }
            $receivers = [];
            if($data['user_id'] != $absensi['from_id']) {
                $receivers[] = $absensi['from_id'];
            }
            $this->load->model('m_receiver');
            $rcvs = $this->m_receiver->many_receiver(['where' => ['_.table_fk' => 'absensi', '_.id_fk' => $absensi['id']]]);
            foreach ($rcvs as $key => $rcv) {
                if($rcv['id_receiver'] == $data['user_id']) {
                    continue;
                }
                $receivers[] = $rcv['id_receiver'];
            }
            $ntf = new Notify_lib();
            $notif_data = $absensi;
            $notif_data['id'] = $absensi['id_dailyreports'];
            $ntf->send(
                'Reply Absensi',
                //$sender_name . ' has reply on Absensi Report',
				$sender_name . ' - '.$position_name.' ('.$branch_name.') has reply on Absensi Report',
                $data['user_id'],
                $receivers,
                $notif_data,
                'absensi',
                true
            );
        }

        return $r;
    }

    function mark_read($id, $id_user, $report = null) {
        if($report == null) {
            $report = $this->one(['where' => ['_.id' => $id]]);
        }
        if($report == null) {
            return ['status' => false, 'message' => 'Report tidak ditemukan'];
        }

        if($report['from_id'] == $id_user) {

        }

        $now = date('Y-m-d H:i:s');

        $this->db->where(['id_fk' => $report['id'], 'table_fk' => 'absensi', 'id_receiver' => $id_user, 'closed_at' => null])->update('receivers', ['view_at' => $now, 'view_comment_at' => $now]);

        return ['status' => true, 'message' => 'Report ditandai telah dibaca'];
    }
}