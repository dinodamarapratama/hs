<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_homeservice extends CI_model
{

    public function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $result = $this->db->delete($table);
        return $result;
    }

     public function get_all_users()
    {
        $this->db->select('usr.*,usr.name, brch.branch_name,pst.name_position');
        $this->db->from('users usr');
        $this->db->join('branch brch','brch.branch_id = usr.branch_id', "left");
        $this->db->join("position pst", "pst.id_position = usr.id_position", "left");
        return $this->db->get();
    }

    public function get_all_relations($table)
    {
        return $this->db->get($table);
    }

    public function edit_data_masterusers($where, $table)
    {
        $this->db->select('users.*');
        $this->db->from('users');
        $this->db->where($where);
        return $this->db->get();
    }
}
