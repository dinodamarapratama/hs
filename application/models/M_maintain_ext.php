<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_maintain_ext extends CI_model
{

    public function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $result = $this->db->delete($table);
        return $result;
    }


    public function getAllData($branchId){
        $this->db->select('_.*, _.created_at AS date, master_qc_alat.alat_name, branch.branch_name, users.name,users.call_name,users.username, users.image, position.name_position');
        $this->db->from('maintain_ext _');
        $this->db->order_by("id_maintain_ex", "desc");
        $this->db->order_by("tanggal", "desc");
        $this->db->join('master_qc_alat', 'master_qc_alat.id_alat = _.id_alat');
        $this->db->join('users', '_.user_id=users.id_user');
		$this->db->join('position', 'position.id_position=users.id_position');
        $this->db->join('branch', '_.branch_id=branch.branch_id');
        if ($branchId != 16):
            $this->db->where('_.branch_id',$branchId);
        endif;
        return $this->db->get();
    }

    public function get($params=[])
    {
        $this->db->select('_.*, master_qc_alat.alat_name, branch.branch_name');
        $this->db->from('maintain_ext _');
        $this->db->where($params);
        $this->db->order_by("tanggal", "asc");
        $this->db->join('master_qc_alat', 'master_qc_alat.id_alat = _.id_alat');
        $this->db->join('branch', 'branch.branch_id = _.branch_id','left');
        return $this->db->get();
    }

    public function get_maintain($params=[],$offset,$limit)
    {
        $this->db->select('_.*, master_qc_alat.alat_name, branch.branch_name');
        $this->db->from('maintain_ext _');
        $this->db->where($params);
        $this->db->order_by("tanggal", "asc");
        $this->db->join('master_qc_alat', 'master_qc_alat.id_alat = _.id_alat','left');
        $this->db->join('branch', 'branch.branch_id = _.branch_id','left');
        $this->db->limit($limit, $offset);
        return $this->db->get();
    }

    public function get_reminder($branch)
    {
        $this->db->select('_.*, master_qc_alat.alat_name');
        $this->db->from('maintain_ext _');
        $this->db->where('_.branch_id',$branch);
        $this->db->where('DATE_SUB(tanggal, INTERVAL remind_before_day DAY) <= CURRENT_DATE()');
        $this->db->where('CURRENT_DATE() <= tanggal');
        $this->db->order_by("tanggal", "asc");
        $this->db->join('master_qc_alat', 'master_qc_alat.id_alat = _.id_alat');
        return $this->db->get();
    }
}
