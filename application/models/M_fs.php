<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
*
*/
class M_fs extends CI_model
{

public function edit_data($where, $table)
{
return $this->db->get_where($table, $where);
}

public function get_data($table)
{
return $this->db->get($table);
}

public function insert_data($data, $table)
{
$this->db->insert($table, $data);
return $this->db->insert_id();
}

public function update_data($where, $data, $table)
{
$this->db->where($where);
$this->db->update($table, $data);
}

public function delete_data($where, $table)
{
$this->db->where($where);
$result = $this->db->delete($table);
return $result;
}

public function get_all_relations($table)
{
return $this->db->get($table);
}



// START FS

public function get_all_fs()
{
$this->db->select('fs.*,br.*,usr.name as fullname , br.branch_name as branch_name , fs.created_at as created_date');
$this->db->from('master_fs fs');
$this->db->join('users usr','usr.id_user = fs.creator_id');
$this->db->join('branch br','br.branch_id = fs.branch_id');
$this->db->order_by('fs.fs_id','asc');

return $this->db->get();
}

public function get_pdf()
{
$this->db->select('fs.*');
$this->db->from('fs');
$this->db->join('m_ipropinsi prov','prov.id_propinsi = fs.id_propinsi');
$this->db->join('m_ikabkota kab','kab.id_kabupaten = fs.id_kabupaten');
$this->db->join('m_ikabkota kab','kab.id_kabupaten = fs.id_kabupaten');
$this->db->join('m_ikecamatan kec','kec.id_kecamatan = fs.id_kecamatan');
$this->db->join('m_ikelurahan desa','desa.id_kelurahan = fs.id_desa');
$this->db->join('branch br','br.branch_id = fs.branch_id');
$this->db->order_by('fs.id','asc');

return $this->db->get();
}


public function edit_data_fs($where, $table)
{
$this->db->select('fs.*');
$this->db->from('master_fs fs');
$this->db->where($where);
return $this->db->get();
}

// END FS










}
