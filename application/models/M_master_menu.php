<?php

class M_master_menu extends CI_Model {

    /* begin menu */
    function baseQueryMenu($args = []) {
        if(isset($args['include_submenu_count']) == false) {
            $args['include_submenu_count'] = false;
        }
        $qSubmenuCount = '';
        if($args['include_submenu_count'] == true) {
            $q = $this->db->from('master_menu sm')
                ->select('count(*) as jumlah', false)
                ->select('parent')
                ->group_by('parent');
            $qSubmenuCount = $q->get_compiled_select();
        }
        $q = $this->db->from('master_menu _')
                ->select([
                    '_.*'
                ]);
        if($args['include_submenu_count'] == true) {
            $q->join("($qSubmenuCount) sm", 'sm.parent = _.id_menu', 'left')
                ->select('coalesce(sm.jumlah, 0) as jumlah_submenu');
        }
        return $q;
    }

    function dt_menu($args = []){
        $this->load->helper('dt');
        $this->load->helper('myquery');
        if(isset($args['where']) == false) {
            $args['where'] = [];
        }
        $q = myquery($this->baseQueryMenu( $args ), ['where' => $args['where'] ]);
        $sql = $q->get_compiled_select();

        return getDataTable([
            'sql' => $sql
        ]);
    }

    function add_menu($data) {
        $this->db->insert('master_menu', $data);
        $this->db->insert('admin_mis.master_menu', $data);
        $id = $this->db->insert_id();

        if($id == null) {
            return ['status' => false, 'message' => 'Terjadi kesalahan'];
        }
        $menu = $this->one_menu(['where' => ['_.id_menu' => $id] ]);
        if($menu == null) {
            return ['status' => false, 'message' => 'Terjadi kesalahan saat menyimpan data'];
        }
        if($menu->parent == '0') {
        } else {
            $parent = $this->one_menu(['where' => ['_.id_menu' => $menu->parent]]);
            // echo "parent:\n\n";
            // print_r($parent);
            if($parent != null) {
                $roleUsers = $this->many_role_user(['where' => ['_.id_menu' => $parent->id_menu]]);
                // echo "role user:\n\n";
                // print_r($roleUsers);
                $listIdUser = [];
                foreach ($roleUsers as $key => $role) {
                    if(in_array($role->id_user, $listIdUser)) {
                        continue;
                    }
                    $listIdUser[] = $role->id_user;
                }
                $this->add_role_user([
                    'menu' => $menu->id_menu,
                    'users' => $listIdUser
                ]);
            } else {
                return ['status' => true, 'message' => 'Menu disimpan tetapi parent tidak valid'];
            }
        }
        return ['status' => true, 'message' => 'Data berhasil disimpan'];
    }

    function update_menu($where, $data) {
        $target = $this->one_menu(['where' => $where]);
        if($target == null) {
            return ['status' => false, 'message' => 'Data tidak ditemukan'];
        }
        $this->db->where(['id_menu' => $target->id_menu])->update('master_menu', $data);
        $this->db->where(['id_menu' => $target->id_menu])->update('admin_mis.master_menu', $data);
        return ['status' => true, 'message' => 'Data berhasil di-update'];
    }

    function many_menu($args) {
        $this->load->helper('myquery');
        return myquery($this->baseQueryMenu($args), $args)->get()->result();
    }

    function one_menu($args) {
        $this->load->helper('myquery');
        return myquery($this->baseQueryMenu($args), $args)->get()->row();
    }

    function hapus_menu($where) {
        $targets = $this->many_menu(['where' => $where]);
        if(count($targets) == 0) {
            return ['status' => false, 'message' => 'Data tidak ditemukan'];
        }
        foreach ($targets as $key => $menu) {
            $this->db->where(['id_menu' => $menu->id_menu])->delete('master_menu');
            $this->db->where(['id_menu' => $menu->id_menu])->delete('admin_mis.master_menu');
        }
        return ['status' => true, 'message' => 'Menu berhasil dihapus'];
    }
    /* end menu*/


    /* begin role menu user */
    function baseQueryRoleUser($args = []){
        $q = $this->db->from('role_menu_user _')
            ->join('users usr', 'usr.id_user = _.id_user', 'left')
            ->select([
                '_.*',
                'usr.name',
                'usr.username'
            ]);
        return $q;
    }

    function baseQueryRolePos($args = []){
        $q = $this->db->from('role_menu_pos _')
            ->join('position pos', 'pos.id_position = _.id_pos', 'left')
            ->select([
                '_.*',
                'pos.name_position'
            ]);
        return $q;
    }

    function baseQueryRoleDept($args = []){
        $q = $this->db->from('role_menu_dept _')
            ->join('departments dep', 'dep.id_department = _.id_dept', 'left')
            ->select([
                '_.*',
                'dep.name',
            ]);
        return $q;
    }

    function many_role_dept($args = []) {
        $this->load->helper('myquery');
        return myquery($this->baseQueryRoleDept($args), $args)->get()->result();
    }

    function many_role_user($args = []){
        $this->load->helper('myquery');
        return myquery($this->baseQueryRoleUser($args), $args)->get()->result();
    }

    function many_role_pos($args = []){
        $this->load->helper('myquery');
        return myquery($this->baseQueryRolePos($args), $args)->get()->result();
    }

    function add_role_dept($data) {
        if(empty($data['menu'])) {
            return ['status' => false, 'message' => 'Field menu diperlukan'];
        }
        if(empty($data['depts'])) {
            return ['status' => false, 'message' => 'Tidak ada dept diterima'];
        }

        $depts = $data['depts'];
        if(is_array($depts) == false) {
            return ['status' => false, 'message' => 'format data depts tidak benar'];
        }

        $idMenu = $data['menu'];

        $existingRoles = $this->many_role_dept(['where' => ['_.id_menu' => $idMenu]]);

        foreach ($depts as $key => $idDept) {
            $exist = false;
            foreach ($existingRoles as $key => $role) {
                if($role->id_dept == $idDept) {
                    $exist = true;
                    break;
                }
            }
            if($exist == true){
                continue;
            }
            $this->db->insert('role_menu_dept', [
                'id_menu' => $idMenu,
                'id_dept' => $idDept
            ]);

            $this->db->insert('admin_mis.role_menu_dept', [
                'id_menu' => $idMenu,
                'id_dept' => $idDept
            ]);
        }

        return ['status' => true, 'message' => 'Data berhasil disimpan'];
    }

    function add_role_pos($data) {
        if(empty($data['menu'])) {
            return ['status' => false, 'message' => 'Field menu diperlukan'];
        }
        if(empty($data['poss'])) {
            return ['status' => false, 'message' => 'Tidak ada poss diterima'];
        }

        $poss = $data['poss'];
        if(is_array($poss) == false) {
            return ['status' => false, 'message' => 'format data position tidak benar'];
        }

        $idMenu = $data['menu'];

        $existingRoles = $this->many_role_pos(['where' => ['_.id_menu' => $idMenu]]);

        foreach ($poss as $key => $idPos) {
            $exist = false;
            foreach ($existingRoles as $key => $role) {
                if($role->id_pos == $idPos) {
                    $exist = true;
                    break;
                }
            }
            if($exist == true){
                continue;
            }
            $this->db->insert('role_menu_pos', [
                'id_menu' => $idMenu,
                'id_pos' => $idPos
            ]);

            $this->db->insert('admin_mis.role_menu_pos', [
                'id_menu' => $idMenu,
                'id_pos' => $idPos
            ]);
        }

        return ['status' => true, 'message' => 'Data berhasil disimpan'];
    }

    function add_role_user($data) {
        // print_r($data);
        if(empty($data['menu'])) {
            return ['status' => false, 'message' => 'Field menu diperlukan'];
        }
        if(empty($data['users'])) {
            return ['status' => false, 'message' => 'Tidak ada user diterima'];
        }

        $users = $data['users'];
        if(is_array($users) == false) {
            return ['status' => false, 'message' => 'format data users tidak benar'];
        }

        $idMenu = $data['menu'];

        $existingRoles = $this->many_role_user(['where' => ['_.id_menu' => $idMenu]]);

        foreach ($users as $key => $idUser) {
            $exist = false;
            foreach ($existingRoles as $key => $role) {
                if($role->id_user == $idUser) {
                    $exist = true;
                    break;
                }
            }
            if($exist == true){
                continue;
            }
            $this->db->insert('role_menu_user', [
                'id_menu' => $idMenu,
                'id_user' => $idUser
            ]);

            $this->db->insert('admin_mis.role_menu_user', [
                'id_menu' => $idMenu,
                'id_user' => $idUser
            ]);
        }

        return ['status' => true, 'message' => 'Data berhasil disimpan'];
    }

    function hapus_role_user($where) {
        $targets = $this->many_role_user(['where' => $where]);
        if(count($targets) == 0) {
            return ['status' => false, 'message' => 'Data tidak ditemukan'];
        }
        foreach ($targets as $key => $role) {
            $this->db->where(['id' => $role->id])->delete('role_menu_user');
            $this->db->where(['id' => $role->id])->delete('admin_mis.role_menu_user');
        }
        return ['status' => true, 'message' => 'Data berhasil dihapus'];
    }

    function hapus_role_dept($where) {
        $targets = $this->many_role_dept(['where' => $where]);
        if(count($targets) == 0) {
            return ['status' => false, 'message' => 'Data tidak ditemukan'];
        }
        foreach ($targets as $key => $role) {
            $this->db->where(['id' => $role->id])->delete('role_menu_dept');
            $this->db->where(['id' => $role->id])->delete('admin_mis.role_menu_dept');
        }
        return ['status' => true, 'message' => 'Data berhasil dihapus'];
    }

    function hapus_role_pos($where) {
        $targets = $this->many_role_pos(['where' => $where]);
        if(count($targets) == 0) {
            return ['status' => false, 'message' => 'Data tidak ditemukan'];
        }
        foreach ($targets as $key => $role) {
            $this->db->where(['id' => $role->id])->delete('role_menu_pos');
            $this->db->where(['id' => $role->id])->delete('admin_mis.role_menu_pos');
        }
        return ['status' => true, 'message' => 'Data berhasil dihapus'];
    }
    /* end role menu user*/


    function getMyMenu($id_user) {
        $q = $this->db->from('master_menu _')
            ->join('role_menu_user rusr', 'rusr.id_menu = _.id_menu', 'left')
            ->join('role_menu_dept rdept', 'rdept.id_menu = _.id_menu', 'left')
            ->join('role_menu_pos rpos', 'rpos.id_menu = _.id_menu', 'left')
            // ->join('departments dep', 'dep.id_department = rdept.id_dept', 'left')
            // ->join('position pos', 'pos.id_department = dep.id_department or pos.id_position = rpos.id_pos', 'left')
            // ->join('bagian bg', 'bg.id_department = dep.id_department', 'left')
            // ->join('users usr', 'usr.id_user = rusr.id_user or usr.id_position = pos.id_position or bg.id = usr.id_bagian', 'left')
            ->join('v_user usr', 'usr.id_user = rusr.id_user or usr.id_position = rpos.id_pos or usr.id_department = rdept.id_dept', 'left')
            ->where(['usr.id_user' => $id_user])
            ->select([
                '_.*'
            ])
            ->order_by('_.urutan', 'asc');
        // echo $q->get_compiled_select();
        // exit();
        $menus = $q->get()->result();
        $res = [];
        $ids = [];
        foreach ($menus as $key => $menu) {
            if(in_array($menu->id_menu, $ids)) {
                continue;
            }
            $ids[] = $menu->id_menu;
            $res[] = $menu;
        }
        return $res;
    }

    function getParentMenu($id_user) {
        $q = $this->db->from('master_menu _')
            ->join('role_menu_user rusr', 'rusr.id_menu = _.id_menu', 'left')
            ->join('role_menu_dept rdept', 'rdept.id_menu = _.id_menu', 'left')
            ->join('role_menu_pos rpos', 'rpos.id_menu = _.id_menu', 'left')
            // ->join('departments dep', 'dep.id_department = rdept.id_dept', 'left')
            // ->join('position pos', 'pos.id_department = dep.id_department or pos.id_position = rpos.id_pos', 'left')
            // ->join('bagian bg', 'bg.id_department = dep.id_department', 'left')
            // ->join('users usr', 'usr.id_user = rusr.id_user or usr.id_position = pos.id_position or bg.id = usr.id_bagian', 'left')
            ->join('v_user usr', 'usr.id_user = rusr.id_user or usr.id_position = rpos.id_pos or usr.id_department = rdept.id_dept', 'left')
            ->where(['usr.id_user' => $id_user])
            ->where(['parent' => 0])
            ->where(['show_in_web' => 1])
            ->select([
                '_.*'
            ])
            ->order_by('_.urutan', 'asc');
        // echo $q->get_compiled_select();
        // exit();
        $menus = $q->get()->result();
        $res = [];
        $ids = [];
        foreach ($menus as $key => $menu) {
            if(in_array($menu->id_menu, $ids)) {
                continue;
            }
            $ids[] = $menu->id_menu;
            $res[] = $menu;
        }
        return $res;
    }

    function getChildMenu($id_user) {
        $q = $this->db->from('master_menu _')
            ->join('role_menu_user rusr', 'rusr.id_menu = _.id_menu', 'left')
            ->join('role_menu_dept rdept', 'rdept.id_menu = _.id_menu', 'left')
            ->join('role_menu_pos rpos', 'rpos.id_menu = _.id_menu', 'left')
            // ->join('departments dep', 'dep.id_department = rdept.id_dept', 'left')
            // ->join('position pos', 'pos.id_department = dep.id_department or pos.id_position = rpos.id_pos', 'left')
            // ->join('bagian bg', 'bg.id_department = dep.id_department', 'left')
            // ->join('users usr', 'usr.id_user = rusr.id_user or usr.id_position = pos.id_position or bg.id = usr.id_bagian', 'left')
            ->join('v_user usr', 'usr.id_user = rusr.id_user or usr.id_position = rpos.id_pos or usr.id_department = rdept.id_dept', 'left')
            ->where(['usr.id_user' => $id_user])
            ->where(['parent !=' => 0])
            ->where(['show_in_web' => 1])
            ->select([
                '_.*'
            ])
            ->order_by('_.urutan', 'asc');
        // echo $q->get_compiled_select();
        // exit();
        $menus = $q->get()->result();
        $res = [];
        $ids = [];
        foreach ($menus as $key => $menu) {
            if(in_array($menu->id_menu, $ids)) {
                continue;
            }
            $ids[] = $menu->id_menu;
            $res[] = $menu;
        }
        return $res;
    }
    
    function isMenuAccessByUser($id_user, $id_menu) {
        $q = $this->db->from('master_menu _')
            ->join('role_menu_user rusr', 'rusr.id_menu = _.id_menu', 'left')
            ->join('role_menu_dept rdept', 'rdept.id_menu = _.id_menu', 'left')
            ->join('role_menu_pos rpos', 'rpos.id_menu = _.id_menu', 'left')
            // ->join('departments dep', 'dep.id_department = rdept.id_dept', 'left')
            // ->join('position pos', 'pos.id_department = dep.id_department or pos.id_position = rpos.id_pos', 'left')
            // ->join('bagian bg', 'bg.id_department = dep.id_department', 'left')
            // ->join('users usr', 'usr.id_user = rusr.id_user or usr.id_position = pos.id_position or bg.id = usr.id_bagian', 'left')
            ->join('v_user usr', 'usr.id_user = rusr.id_user or usr.id_position = rpos.id_pos or usr.id_department = rdept.id_dept', 'left')
            ->where(['usr.id_user' => $id_user])
			->where(['_.id_menu' => $id_menu])
            ->select([
                '_.*'
            ])
            ->order_by('_.urutan', 'asc');
        // echo $q->get_compiled_select();
        // exit();
        $rownum = $q->get()->num_rows();
        return $rownum;
    }

    
}