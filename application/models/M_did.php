<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_did extends CI_model
{

    public function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $result = $this->db->delete($table);
        return $result;
    }

     public function get_all_did()
    {
        $this->db->select('did.*,dpt.name');
        $this->db->from('did did');
        $this->db->join('departments dpt','dpt.id_department = did.id_department', "left");
        return $this->db->get();
    }

    public function get_all_relations($table)
    {
        return $this->db->get($table);
    }

    public function edit_data_did($where, $table)
    {
        $this->db->select('did.*');
        $this->db->from('did');
        $this->db->where($where);
        return $this->db->get();
    }
}
