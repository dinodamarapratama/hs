<?php

class M_receiver extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->helper('myquery');
	}

	function tableName() {
		return 'receivers';
	}

	function add_receiver($id_fk, $table_fk, $receivers) {
		if(is_array($receivers) == false) {
			$receivers = [ $receivers ];
		}

		$added = [];
		foreach ($receivers as $key => $id_rcv) {
			if(in_array($id_rcv, $added)) {
				continue;
			}
			$added[] = $id_rcv;

			$this->db->insert('receivers', [
				'id_fk' => $id_fk,
				'id_receiver' => $id_rcv,
				'view_at' => null,
				'view_comment_at' => null,
				'closed_at' => null,
				'comment_at' => null,
				'table_fk' => $table_fk,
				'updated_at' => date('Y-m-d')
			]);
		}
	}

	function baseQuery($args = []) {
		$q = $this->db->from('receivers _')
			->join('users usr', 'usr.id_user = _.id_receiver', 'left')
			->join('position p', 'p.id_position = usr.id_position', 'left')
			->join('departments d', 'd.id_department = p.id_department', 'left')
			->select([
				'_.*',
				'usr.name as rcv_name',
				'usr.username as rcv_username',
				'p.name_position as rcv_position',
				'd.name as rcv_department'
			]);
		return $q;
	}

	function many_receiver($args = []){
		return myquery($this->baseQuery($args), $args)->get()->result_array();
	}

	function one_receiver($args) {
		return myquery($this->baseQuery($args), $args)->get()->row_array();
	}

	function simpleUpdate($where, $data) {
		$this->db->where($where)->update('receivers', $data);
		return ['status' => true, 'message' => 'Update success'];
	}
}