<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
*
*/
class M_profile extends CI_model
{

public function edit_data($where, $table)
{
return $this->db->get_where($table, $where);
}

public function get_data($table)
{
return $this->db->get($table);
}

public function insert_data($data, $table)
{
$this->db->insert($table, $data);
return $this->db->insert_id();
}

public function update_data($where, $data, $table)
{
$this->db->where($where);
$this->db->update($table, $data);
}

public function delete_data($where, $table)
{
$this->db->where($where);
$result = $this->db->delete($table);
return $result;
}

public function get_all_profile($id_user = null)
{
	$this->db->select('usr.*,br.branch_name,dpt.name as dpt_name,pst.name_position,bg.name as bagian_name');
	$this->db->from('users usr');
	$this->db->join('branch br','br.branch_id = usr.branch_id', "left");
	$this->db->join('departments dpt','dpt.id_department = usr.id_department', "left");
	$this->db->join('position pst','pst.id_position = usr.id_position', "left");
	$this->db->join('bagian bg','bg.id = usr.id_bagian', "left");
	if(!empty($id_user)){
	$this->db->where('usr.id_user', $id_user);
	}
	return $this->db->get();
}

public function get_all_relations($table)
{
	return $this->db->get($table);
}

public function get_list_select($table='',$param=[]){
	$this->db->where($param);
	return $this->db->get($table)->result();
}

}
