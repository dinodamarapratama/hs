<?php

class M_qc_group_level_member extends CI_Model {
	public $table = "master_qc_group_level_member";

	function baseQuery(){
		$q = $this->db->from("$this->table _")
			->select([
				"_.*"
			]);
		return $q;
	}

	function add($data) {
		$this->db->insert($this->table, $data);
		$id = $this->db->insert_id();
		$r = $this->one(["where" => ["id" => $id]]);
		if($id == null || $r == null) {
			return ["status" => false, "message" => "Terjadi kesalahan menyimpan data"];
		}
		return ["status" => true, "message" => "Success"];
	}

	function add_many($arr) {
		if($arr == null) {
			$arr = [];
		}
		if(is_array($arr) == false) {
			$arr = [$arr];
		}

		$result = [];
		foreach ($arr as $key => $data) {
			$result[] = $this->add($data);
		}

		return [
			"status" => count($result) > 0,
			"message" => "",
			"result" => $result
		];
	}

	function one($args = null) {
		$r = $this->many($args);
		if(count($r) > 0) {
			return $r[0];
		}
		return null;
	}

	function many($args = null) {
		if($args == null) {
			$args = [];
		}
		if(isset($args["where"]) == false) {
			$args["where"] = [];
		}

		return $this->baseQuery()
			->where($args["where"])
			->get()->result_array();
	}

	function delete($where) {
		$target = $this->many([
			"where" => $where
		]);

		if(count($target) == 0) {
			return ["status" => false, "message" => "Data tidak ditemukan"];
		}

		// $this->db->where(["id" => $target["id"]])->delete($this->table);
		foreach ($target as $key => $t) {
			$this->db->where(["id" => $t["id"]])->delete($this->table);
		}

		return ["status" => true, "message" => "Success"];
	}

}