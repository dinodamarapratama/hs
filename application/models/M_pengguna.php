<?php
defined('BASEPATH') or exit ('No Direct Script Access Allowed');

/**
* 
*/
class M_pengguna extends CI_model{
	function __construct(){
            parent::__construct();
            //load our second db and put in $db2
            $this->db2 = $this->load->database('second', TRUE);
        }

	
	function edit_data($where,$table){
		return $this->db2->get_where($table,$where);
	}

	public function msrwhere($where,$table,$field,$sb){
		$this->db2->order_by($field,$sb);
		return $this->db2->get_where($table, $where);
	}
	
	public function selwhere($where,$table,$sel){
		$this->db2->select($sel);
		return $this->db2->get_where($table, $where);
	}
	
	function get_data($table){
		return $this->db2->get($table);
	}

	function insert_data($data,$table){
		 $this->db2->insert($table,$data);
		 return $this->db2->insert_id();
	}

	function update_data($where,$data,$table){
	 	$this->db2->where($where);
	 	$this->db2->update($table,$data);
	}	

	function delete_data($where,$table){
		$this->db2->where($where);
	 	$this->db2->where($table);
	}

	public function get_all_relations($table)
	{
		return $this->db2->get($table);
	}
	
	public function get_list_select($table='',$param=[]){
		$this->db2->where($param);
		return $this->db2->get($table)->result();
	}

	
     function many_reg($args = []) {
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
			$args["where"] = [];
		}
		else{
			$t = '';
			foreach($args["where"] as $v){
				$t = ($t=='')?$v:$v.' AND '.$t;
			}
			//print_r($t);
			
			$args["where"] = $t;
		}
		if(isset($args["limit"]) == false) {
			$args["limit"] = [];
		}
		if(isset($args["start"]) == false) {
			$args["start"] = [];
		}
		if(isset($args["order"]) == false) {
			$args["order"] = [];
		}
        return $this->baseQueryReg($args)->where($args["where"])->order_by($args["sort"],$args["order"])->limit($args["limit"], $args["start"])->get()->result_array();
    }

     function baseQueryReg($args = []) {
		return 
		$this->db2->from("fs _")
		->join("fs_log_print log","log.fs_id = _.id")
		->group_by("_.id")
            ->select([
                "_.*",
                "DATE_FORMAT(tgl_periksa,'%d/%m/%Y') AS periksa",
				"(select count(fs_id) from fs_log_print where fs_id=_.id and id_pengguna=creator_id) AS jml_print",
				"MAX(DATE_FORMAT(log.createddate,'%d/%m/%Y %H:%i:%s')) as createddate"
				
				
            ]);
    }

    function many_kar($args = []) {
        if($args == null) {
			$args = [];
		}
		if(isset($args["where"]) == false) {
			$args["where"] = [];
		}
		else{
			$t = '';
			foreach($args["where"] as $v){
				$t = ($t=='')?$v:$v.' AND '.$t;
			}
			//print_r($t);
			
			$args["where"] = $t;
		}
		if(isset($args["limit"]) == false) {
			$args["limit"] = [];
		}
		if(isset($args["start"]) == false) {
			$args["start"] = [];
		}
		if(isset($args["order"]) == false) {
			$args["order"] = [];
		}
		
        return $this->baseQueryKar($args)->where($args["where"])->order_by($args["sort"],$args["order"])->limit($args["limit"], $args["start"])->group_by("no_reg")->get()->result_array();
    }

     function baseQueryKar($args = []) {
        return $this->db2->from("fs _")
        	->group_by("no_reg")
            ->select([
                "_.*",
                "(YEAR(CURDATE())-YEAR(tgl_lahir)) AS umur",
                "DATE_FORMAT(tgl_lahir,'%d/%m/%Y') AS lahir",
                "COUNT(no_reg) AS test1",
     			"COUNT(no_reg) AS test2",
            ]);
    }

    
    public function buat_kode()   {

		  $this->db2->select('RIGHT(fs.no_reg,4) as no_reg', FALSE);
		  $this->db2->order_by('no_reg','DESC');    
		  $this->db2->limit(1);    
		  $query = $this->db2->get('fs');      //cek dulu apakah ada sudah ada kode di tabel.    
		  if($query->num_rows() <> 0){      
		   //jika kode ternyata sudah ada.      
		   $data = $query->row();      
		   $kode = intval($data->no_reg) + 1;    
		  }
		  else {      
		   //jika kode belum ada      
		   $kode = 1;    
		  }

		  $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
		  $kodejadi = "COM".$kodemax;    // hasilnya ODJ-9921-0001 dst.
		  return $kodejadi;  
	}

	function many_users($args = []) {
        if($args == null) {
			$args = [];
		}
		if(isset($args["where"]) == false) {
			$args["where"] = [];
		}
		else{
			$t = '';
			foreach($args["where"] as $v){
				$t = ($t=='')?$v:$v.' AND '.$t;
			}
			//print_r($t);
			
			$args["where"] = $t;
		}
		if(isset($args["limit"]) == false) {
			$args["limit"] = [];
		}
		if(isset($args["start"]) == false) {
			$args["start"] = [];
		}
		if(isset($args["order"]) == false) {
			$args["order"] = [];
		}
		
        return $this->baseQueryUsers($args)->where($args["where"])->order_by($args["sort"],$args["order"])->limit($args["limit"], $args["start"])->get()->result_array();
    }

     function baseQueryUsers($args = []) {
        return $this->db2->from("pengguna _")
        	->limit("5")
            ->select([
                "_.*",
               
            ]);
    }

    function add_input_pengguna($data) {
        
        $this->db2->insert("pengguna", $data);
        $id = $this->db2->insert_id();
        

        return ["status" => true, "message" => "Success"];
    }

     function update_input_pengguna($where, $data) {
        $this->db2->where($where)->update("pengguna", $data);
        return ["status" => true, "message" => "Success"];
    }

    function delete_input_pengguna($where){
        $del = $this->db2->delete('pengguna', $where);
        return ["status" => true, "message" => "Success"];
    }


}


?>