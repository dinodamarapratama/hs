<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class Ths_model extends CI_model
{
    public function getHs($where)
    {
        $this->db->select('id, date, nama, pemeriksaan, homeservice.time_id, jumlah_pasien, dokter, dokter, alamat, status, time_name, time_name, no_telp, catatan, hs_initial_petugas_hs.ptgshs_id, hs_initial_petugas_hs.id_user, hs_initial_petugas_hs.abbr_hs, hs_initial_petugas_hs.start_date, hs_initial_petugas_hs.end_date, hs_initial_petugas_hs.branch_id, hs_initial_petugas_hs.creator_id ');
        $this->db->from('homeservice');
        $this->db->join('hs_time', 'homeservice.time_id = hs_time.time_id');
        $this->db->join('hs_initial_petugas_hs', 'homeservice.ptgshs_id=hs_initial_petugas_hs.ptgshs_id');
        $this->db->where($where);
        $this->db->order_by('time_name', 'asc');
        $data = $this->db->get();
        return $data;
    }

    public function getTesByThs($ths = null, $child = null){
        if($child){
            $pricelists = $this->db->get_where('ths_price_child', ['id_child_ths' => $child->id_child])->result_array();
            $data = [];
            foreach($pricelists as $pl){
                $pricelist = $this->db->get_where('ths_master_pricelist', ['id_pricelist' => $pl['id_price_list']])->row_array();
                $pricelist['name'] = $child->name;
                // $pricelist['relation'] = $this->db->get_where('ths_relation', ['id_relation' => $child->id_relation])->row()->relation_name;
                $pricelist['relation'] = $this->db->get_where('ths_relation', ['id_relation' => $child->id_relation])->row()->relation_status;
                array_push($data, $pricelist);           
            }
            foreach($data as $k => $v){
                $data[$k]['price'] = $this->rupiah($v['price']);
            }
            return $data;
        }else{
            $pricelists = $this->db->get_where('ths_price', ['id_ths' => $ths->id_ths])->result_array();
            $data = [];
            foreach($pricelists as $pl){
                $pricelist = $this->db->get_where('ths_master_pricelist', ['id_pricelist' => $pl['id_price_list']])->row_array();
                $pricelist['name'] = $this->db->get_where('homeservice', ['id' => $ths->id_hs])->row()->nama;
                
                array_push($data, $pricelist);           
            }
            foreach($data as $k => $v){
                $data[$k]['price'] = $this->rupiah($v['price']);
            }
            return $data;
        }       
    }

    public function getChild($id_ths){
        $this->db->select('ths_child.*, ths_relation.relation_name, ths_relation.relation_status');
        $this->db->from('ths_child');
        $this->db->where('ths_child.id_ths', $id_ths);
        $this->db->join('ths_relation', 'ths_child.id_relation = ths_relation.id_relation');
        return $this->db->get();
    }

    protected function rupiah($angka){
	
        $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
        return $hasil_rupiah;
     
    }
}
