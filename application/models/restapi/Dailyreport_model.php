<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dailyreport_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function detail($id, $id_reply = null, $id_user = null)
	{
		$user_id = $id_user;

		if(empty($id) && empty($id_reply)==false) {
			$q = $this->db->from('dailyreports_reply')->where(['id' => $id_reply])->get()->row();
			if($q != null) {
				$id = $q->id_dailyreports;
			}
		}
		if ($id_user != null ) {
			$this->readReceiver($id, $id_user );

		}
		$dailyreport = $this->db->where('id', $id)->select('dailyreports.*,DATE_FORMAT(dailyreports.date,"%d-%m-%Y") as date')->get('dailyreports')->row();
		if(empty($dailyreport)) {
			return null;
		}
		$this->load->model(['restapi/generalreports_model', 'm_absensi']);
		$generalreport = $this->generalreports_model->one(['where' => ['_.id_dailyreports' => $dailyreport->id]]);
		$absensi = $this->m_absensi->one(['where' => ['_.id_dailyreports' => $dailyreport->id]]);

		if($generalreport != null) {
			$this->generalreports_model->mark_read($generalreport->id, $user_id, $generalreport);
		}
		if($absensi != null) {
			$this->m_absensi->mark_read($absensi['id'], $user_id, $absensi);
		}

		if($user_id){
			$this->db->where([
				'id_receiver' => $user_id,
				'id_dailyreports' => $id,
				'is_closed' => 0
			])->update('dailyreports_receiver', ['is_view' => 1, 'is_view_comment' => 1]);
		}

		if(empty($dailyreport)) {
			return [];
		}

		if($dailyreport->from_id == $id_user) {
			$this->db->where(['id' => $id])->where_in('is_view', [0,1])->update('dailyreports', ['is_view' => 1]);
			$this->db->where(['id_dailyreports' => $id])->where_in('is_view', [0,1])->update('generalreports', ['is_view' => 1]);
			// echo 'update sender ';
		} else {
			// echo 'sender : ' . $dailyreport->from_id;
		}

		$all_normal = $dailyreport->absensi_des == '1' && $dailyreport->reg_des == '2' && $dailyreport->sampling_des == '3'
			&& $dailyreport->qc_des == '4' && $dailyreport->alat_des == '5' && $dailyreport->janji_des == '6'
			&& $dailyreport->lain_des == '7' && $dailyreport->pelayanan_des == '8';
		// var_dump($all_normal);
		// exit();
		if($all_normal == true) {
			$receiversNotRead = $this->db->from('dailyreports_receiver')
				->group_start()
					->or_where([
						'is_view' => 0
					])
				->group_end()
				->where(['id_dailyreports' => $dailyreport->id])
				->get()->result();
				// ->get_compiled_select();

			// print_r($receiversNotRead);
			// exit();

			$counter = 0;
			foreach($receiversNotRead as $r) {
				$counter++;
				break;
			}
			// print_r($counter);
			// exit();
			if($counter == 0) {
				$this->db->where(['id' => $dailyreport->id])->update('dailyreports', ['status' => 'Close']);
				// $dailyreport = $this->db->from('dailyreports')->where(['id'=>$dailyreport->id])->get()->row();
			}
		}

		if(!empty($dailyreport)){

			/* tandai notifikasi sudah dibaca */
			$this->load->library("Notify_lib");
        	$nl = new Notify_lib();
        	$nl->mark_read($user_id, "dailyreport", $id);
        	if($dailyreport->from_id == $user_id) {
        		$this->db->where(['id' => $dailyreport->id])->update('dailyreports', ['is_view' => 1]);
        	}
			// $this->db->group_start()
			// 		->or_like("data", '"dailyreports_id":"' . $id . '"')
			// 		->or_like("data", '"dailyreport_id":"'  . $id . '"')
			// 		->or_like("data", '"dailyreports_id":'  . $id )
			// 		->or_like("data", '"dailyreport_id":'   . $id )
			// 	->group_end()
			// 	->where([
			// 		"to_id" => $user_id
			// 	])
			// 	->group_start()
			// 		->or_where(["read" => null])
			// 		->or_where(["read" => 0])
			// 	->group_end()
			// 	// ->where(["naruto" => "anime"])
			// 	->update("notifications", ["read" => 1]);


		   if(!empty($dailyreport->absensi_des)){
				$myCaption1 = strip_tags($dailyreport->absensi_des);
				$myCaptionEncoded1 = htmlspecialchars_decode($myCaption1);
				// $dailyreport->absensi_des = str_replace(['<p>','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>','<br','</br>','&quot;'], '',$myCaptionEncoded1);
				$dailyreport->absensi_des = html_entity_decode($dailyreport->absensi_des);
			}
			if(!empty($dailyreport->reg_des)){
				$myCaption2 = strip_tags($dailyreport->reg_des);
				$myCaptionEncoded2 = htmlspecialchars_decode($myCaption2);
				// $dailyreport->reg_des = str_replace(['<p>','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>','<br','</br>','&quot;'], '',$myCaptionEncoded2);
				$dailyreport->reg_des = html_entity_decode($dailyreport->reg_des);
			}
			if(!empty($dailyreport->sampling_des)){
				$myCaption3 = strip_tags($dailyreport->sampling_des);
				$myCaptionEncoded3 = htmlspecialchars_decode($myCaption3);
				// $dailyreport->sampling_des = str_replace(['<p>','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>','<br','</br>','&quot;'], '',$myCaptionEncoded3);
				$dailyreport->sampling_des = html_entity_decode($dailyreport->sampling_des);
			}
			if(!empty($dailyreport->qc_des)){
				$myCaption4 = strip_tags($dailyreport->qc_des);
				$myCaptionEncoded4 = htmlspecialchars_decode($myCaption4);
				// $dailyreport->qc_des = str_replace(['<p>','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>','<br','</br>','&quot;'], '',$myCaptionEncoded4);
				$dailyreport->qc_des = html_entity_decode($dailyreport->qc_des);
			}
			if(!empty($dailyreport->qc_des)){
				// $myCaption4 = strip_tags($dailyreport->qc_des);
				// $myCaptionEncoded4 = htmlspecialchars_decode($myCaption4);
				// $dailyreport->qc_des = str_replace(['<p>','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>','<br','</br>','&quot;'], '',$myCaptionEncoded4);

			}
			if(!empty($dailyreport->alat_des)){
				$myCaption5 = strip_tags($dailyreport->alat_des);
				$myCaptionEncoded5 = htmlspecialchars_decode($myCaption5);
				// $dailyreport->alat_des = str_replace(['<p>','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>','<br','</br>','&quot;'], '',$myCaptionEncoded5);
				$dailyreport->alat_des = html_entity_decode($dailyreport->alat_des);
			}
			if(!empty($dailyreport->janji_des)){
				$myCaption6 = strip_tags($dailyreport->janji_des);
				$myCaptionEncoded6 = htmlspecialchars_decode($myCaption6);
				// $dailyreport->janji_des = str_replace(['<p>','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>','<br','</br>','&quot;'], '',$myCaptionEncoded6);
				$dailyreport->janji_des = html_entity_decode($dailyreport->janji_des);
			}
			if(!empty($dailyreport->lain_des)){
				$myCaption7 = strip_tags($dailyreport->lain_des);
				$myCaptionEncoded7 = htmlspecialchars_decode($myCaption7);
				// $dailyreport->lain_des = str_replace(['<p>','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>','<br','</br>','&quot;'], '',$myCaptionEncoded7);
				$dailyreport->lain_des = html_entity_decode($dailyreport->lain_des);
			}
			if(!empty($dailyreport->pelayanan_des)){
				$myCaption8 = strip_tags($dailyreport->pelayanan_des);
				$myCaptionEncoded8 = htmlspecialchars_decode($myCaption8);
				// $dailyreport->pelayanan_des = str_replace(['<p>','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>','<br','</br>','&quot;'], '',$myCaptionEncoded8);
				$dailyreport->pelayanan_des = html_entity_decode($dailyreport->pelayanan_des);
			}

			if(empty($dailyreport->id_des) == false) {
				$dailyreport->id_des = html_entity_decode($dailyreport->id_des);
			}

		}

		// $this->db->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
		// $this->db->select('users.image');
		// $this->db->select('position.name_position as position');
		// $this->db->select('users.name, users.username, users.id_user as id, departments.name as department');
		// $this->db->join('position', 'position.id_position=users.id_position');
		// $this->db->join('departments', 'position.id_department=position.id_department', 'left');
		// $dailyreport->author = $this->db->where('users.id_user', $dailyreport->from_id)->get('users')->row();
		$this->load->model('restapi/user_model');
		$dailyreport->author = $this->user_model->one_user(['where' => ['_.id_user' => $dailyreport->from_id]]);

		// $this->db->select('users.*, users.id_user as id, is_view');
		// $this->db->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
		// $this->db->select('users.image');
		// $this->db->select('position.name_position as position');
		// $this->db->select('departments.name as department');
		// $this->db->join('users', 'dailyreports_receiver.id_receiver=users.id_user');
		// $this->db->join('position', 'position.id_position=users.id_position');
		// $this->db->join('departments', 'position.id_department=position.id_department', 'left');
		// $this->db->where('dailyreports_receiver.id_dailyreports', $id);
		// $this->db->group_by('dailyreports_receiver.id_receiver');
		// $dailyreport->receivers = $this->db->get('dailyreports_receiver')->result();


		// $this->db->select('users.*, users.id_user as id')
		// 			->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false)
		// 			->select('users.image')
		// 			->select('position.name_position as position')
		// 			->select('departments.name as department')
		// 			->select('dailyreports_reply.*')
		// 			->join('users', 'dailyreports_reply.sender=users.id_user')
		// 			->join('position', 'position.id_position=users.id_position')
		// 			->join('departments', 'position.id_department=position.id_department', 'left')
		// 			->where('dailyreports_reply.id_dailyreports', $id)
		// 			->group_by('dailyreports_reply.date_added')
		// 			->order_by('dailyreports_reply.date_added', 'desc');
		// $dailyreport->replies = $this->db->get('dailyreports_reply')->result();
		$dailyreport->replies = $this->getReplies($id);
		$dailyreport->receivers = $this->getReceivers($id);

		if (count($dailyreport->replies) > 0){
			foreach ($dailyreport->replies as $key => $value) {
				$myCaption8 = strip_tags($value->message_reply);
				$myCaptionEncoded8 = htmlspecialchars_decode($myCaption8);
				$dailyreport->replies[$key]->message_reply = str_replace(['<p>','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>'], '',$myCaptionEncoded8);
			}
		}
		// file_put_contents("/var/www/html/application/logs/querylog.sql", $this->db->last_query());

		$dailyreport->attachments = $this->db->where('dailyreport_id', $id)->get('dailyreports_attachment')->result();

		if($dailyreport != null) {
            if($dailyreport->absensi_des == '1')     { $dailyreport->absensi_des = 'Lancar'; }
            if($dailyreport->reg_des == '2')         { $dailyreport->reg_des = 'Lancar'; }
            if($dailyreport->sampling_des == '3')    { $dailyreport->sampling_des = 'Lancar'; }
            if($dailyreport->sampling_des == '3')    { $dailyreport->sampling_des = 'Lancar'; }
            if($dailyreport->qc_des == '4')          { $dailyreport->qc_des = 'Lancar'; }
            if($dailyreport->alat_des == '5')        { $dailyreport->alat_des = 'Lancar'; }
            if($dailyreport->janji_des == '6')       { $dailyreport->janji_des = 'Lancar'; }
            if($dailyreport->lain_des == '7')        { $dailyreport->lain_des = 'Lancar'; }
            if($dailyreport->pelayanan_des == '8')   { $dailyreport->pelayanan_des = 'Lancar'; }


            $dailyreport->show_daily = true;
        }
		return $dailyreport;
	}

	function getReplies($id_dailyreport) {
		$q = $this->db->from('dailyreports_reply dr')
			->join('users usr', 'usr.id_user = dr.sender')
			->join('position pos', 'pos.id_position = usr.id_position')
			->join('departments dept', 'dept.id_department = pos.id_department', 'left')
			->order_by('dr.date_added', 'asc')
			->where(['dr.id_dailyreports' => $id_dailyreport])
			->select([
				'dr.*',
				'usr.image',
				'pos.name_position as position',
				'dept.name as department',
				'usr.name',
				'usr.call_name',
				'usr.username'
			])
			->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false)
			->get()->result();
		$ids = [];
		foreach ($q as $key => $row) {
			$ids[] = $row->id;
		}
		$attachments = [];
		if(count($ids) > 0){
			$attachments
				= $this->db->from('dailyreport_reply_attachment')
				->where_in('id_dailyreport_reply', $ids)
				->get()->result();
		}

		foreach ($q as $index => $row) {
			if(empty($row->attachments)) {
				$q[$index]->attachments = [];
			}
			foreach ($attachments as $key => $item) {
				if($item->id_dailyreport_reply == $row->id) {
					$q[$index]->attachments[] = $item;
				}
			}
		}
		return $q;
	}

	function hapus_attachment($id, $attachment = null) {
		if($attachment != null){

		} else {
			$attachment = $this->db->where(['id' => $id])->get('dailyreport_reply_attachment')->row();
		}
		if($attachment != null){
			if(file_exists($attachment->path)) {
				unlink($attachment->path);
			}
			$this->db->where(['id' => $attachment->id])->delete('dailyreport_reply_attachment');
			return ['status' => true, 'message' => 'Attachment berhasil dihapus'];
		}
		return ['status' => false, 'message' => 'Terjadi kesalahan'];
	}

	function recent_report($id_user) {
		$id_user = intval($id_user);
		$type = "Daily Report";
		$sql = " 	SELECT r.id, '$type Reply' as type_name, r.from_id as sender_id, r.last_update as last_update,
						r.is_view
					from dailyreports r
					where r.from_id = '$id_user' and r.is_view = 0 and UPPER(r.status) = 'OPEN'

					union

					Select r.id, '$type' as type_name, r.from_id as sender_id, r.last_update as last_update,
						case when rcv.is_view = 0 or rcv.is_view_comment = 0 then 0 else 1 end as is_view
					from dailyreports_receiver rcv
					join dailyreports r on rcv.id_dailyreports = r.id
					where rcv.id_receiver = '$id_user' and UPPER(r.status) = 'OPEN' ";
		return $sql;


		// $select1 = "SELECT dr.id, 'Daily Report' as type_name, dr.from_id as sender_id, dr.last_update as last_update, drc.is_view
		// 			from dailyreports dr
		// 			join dailyreports_receiver drc on drc.id_dailyreports = dr.id
		// 			where drc.id_receiver = '$id_user' and drc.is_view = 0 and UPPER(dr.status) = 'OPEN'

		// 			union

		// 			Select gr.id, 'General Report' as type_name, gr.from_id as sender_id, gr.date as last_update, grc.is_view
		// 			from generalreports gr
		// 			join generalreports_receiver grc on grc.id_generalreports = gr.id
		// 			where grc.id_receiver = '$id_user' and grc.is_view = 0 and UPPER(gr.status) = 'OPEN'

		// // 			union

		// // 			Select rp.id, 'Daily Report Reply' as type_name, rp.sender as sender_id, rp.date_added as last_update, case when d.from_id = '$id_user' then d.is_view else drc.is_view_comment end as is_view
		// // 			from dailyreports_reply rp
		// // 			join dailyreports d on d.id = rp.id_dailyreports
		// // 			join dailyreports_receiver drc on drc.id_dailyreports = d.id
		// // 			where (d.from_id = '$id_user' or drc.id_receiver = '$id_user' ) and rp.sender not in ($id_user) and UPPER(d.status) = 'OPEN'

		// // 			union

		// // 			Select rpgr.id, 'General Reports Reply' as type_name, rpgr.sender as sender_id, rpgr.date_added as last_update, case when g.from_id = '$id_user' then g.is_view else grc.is_view_comment end as is_view
		// // 			from generalreports_reply rpgr
		// // 			join generalreports g on g.id = rpgr.id_generalreports
		// // 			join generalreports_receiver grc on grc.id_generalreports = g.id
		// // 			where (g.from_id = '$id_user' or grc.id_receiver = '$id_user' ) and rpgr.sender not in ($id_user) and UPPER(g.status) = 'OPEN'

		// // 			";



		// $select2 = "SELECT s1.*, usr.name, usr.username,
		// 			usr.image
		// 			from ($select1) s1
		// 			join users usr on usr.id_user = s1.sender_id
		// 			where s1.is_view = 0
		// 			order by last_update desc limit 3";
		// return $this->db->query($select2)->result();
	}

	function hapus_reply($id, $id_user){
		$reply = $this->db->from('dailyreports_reply')->where(['id' => $id])->get()->row();
		if($reply == null){
			return ['status' => false, 'message' => "Reply tidak dapat ditemukan"];
		}

		if($reply->sender != $id_user) {
			return ['status' => false, 'message' => 'Anda tidak berhak menghapus reply dari orang lain'];
		}

		$this->db->where(['id' => $reply->id])->delete('dailyreports_reply');

		$files = $this->db->from('dailyreport_reply_attachment')->where(['id_dailyreport_reply' => $reply->id])->get()->result();
		foreach ($files as $key => $file) {
			$this->hapus_attachment(null, $file);
		}
		// $this->db->where(['id_dailyreport_reply' => $reply->id])->delete('dailyreport_reply_attachment');
		return ['status' => true, 'message' => 'reply berhasil dihapus'];
	}

	public function save($data = [], $receivers = [], $dept = [], $attachment_prefix = 'file_', $attachment_prefixJ = 'fileJ_', $attach_mode = 'seq')
	{
		$this->load->model('restapi/user_model');
		$this->db->trans_start();
		// $this->db->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
		// $this->db->select('users.image');
		// $this->db->select('users.*, users.id_user as id, position.name_position as position, position.id_department');
		// $this->db->join('position', 'position.id_position=users.id_position', 'left');
		// $me = $this->db->where('id_user', $this->input->post('user_id'))->get('users')->row();
		// $save['title'] = $me->username;
		$save = [
			'is_it' => 0,
			'is_absensi' => 0
		];
		$save['id_draft'] = 0;
		$save['is_view'] = 1;
		$save['mengetahui'] = '';
		$save['dateplanning'] = date('Y-m-d H:i:s');

		if(array_key_exists('title', $data)){
			$save['title'] = $data['title'];
		}else{
			$save['title'] = $this->input->post('subject');
		}


		if(array_key_exists('jam_buka', $data)){
			$save['jam_buka'] = $data['jam_buka'];
		}else{
			$save['jam_buka'] = "";
		}

		$bagian = [];
		if(array_key_exists('bagian', $data)) {
			$bagian = $data['bagian'];
			unset($data['bagian']);
		}
		else {
			$bagian = $this->input->post('bagian');
		}

		if(is_array($bagian) == false) {
			$bagian = [$bagian];
		}
		$user_bagian = [];
		if(count($bagian) > 0) {
			$users = $this->user_model->many_user(['where_in' => ['bg.id' => $bagian]]);

			foreach ($users as $key => $usr) {
				$user_bagian[] = $usr['id_user'];
			}
		}

		// if(array_key_exists('description', $data)) {
		// 	$save['description'] = $data['description'];
		// }else {
		// 	$save['description'] = ($v = $this->input->post('body')) ? $v : '0';
		// }

		if(array_key_exists('absensi_des', $data)) {
			$save['absensi_des'] = empty($r = $data['absensi_des']) ? '1' : $r;
			// if(empty($save['absensi_des'])) {
			// 	$save['absensi_des'] = 1;
			// }
		}else{
			$save['absensi_des'] = $this->input->post('body1') ? $this->input->post('body1') : '1';
		}
		$absensi = $save['absensi_des'];
		if(empty($absensi) == false && $absensi != '1') {
			$save['is_absensi'] = 1;
		}

		if(array_key_exists('reg_des', $data)){
			$save['reg_des'] = empty($c = $data['reg_des']) ? '2' : $c;
		}else{
			$save['reg_des'] = $this->input->post('body2') ? $this->input->post('body2') : '2';
		}

		if(array_key_exists('sampling_des', $data)){
			$save['sampling_des'] = empty($c = $data['sampling_des']) ? '3' : $c;
		}else{
			$save['sampling_des'] = $this->input->post('body3') ? $this->input->post('body3') : '3';
		}

		if(array_key_exists('qc_des', $data)){
			$save['qc_des'] = empty($c = $data['qc_des']) ? '4' : $c;
		}else{
			$save['qc_des'] = $this->input->post('body4') ? $this->input->post('body4') : '4';
		}

		if(array_key_exists('alat_des', $data)) {
			$save['alat_des'] = empty($c = $data['alat_des']) ? '5' : $c;
		}else{
			$save['alat_des'] = $this->input->post('body5') ? $this->input->post('body5') : '5';
		}

		if(array_key_exists('janji_des', $data)) {
			$save['janji_des'] = empty($c = $data['janji_des']) ? '6' : $c;
		}else{
			$save['janji_des'] = $this->input->post('body6') ? $this->input->post('body6') : '6';
		}

		if(array_key_exists('lain_des', $data)) {
			$save['lain_des'] = empty($c = $data['lain_des']) ? '7' : $c;
		}else{
			$save['lain_des'] = $this->input->post('body7') ? $this->input->post('body7') : '7';
		}

		if(array_key_exists('pelayanan_des', $data)) {
			$save['pelayanan_des'] = empty($c = $data['pelayanan_des']) ? '8' : $c;
		}else{
			$save['pelayanan_des'] = $this->input->post('body8') ? $this->input->post('body8') : '8';
		}

		// $save['dept'] = empty($me->id_department) ? '' : $me->id_department;

		// $save['dateplanning'] = $this->input->post('date_planning');
		if(array_key_exists('date', $data)) {
			$save['date'] = $data['date'];
		} else {
			$save['date'] = date('Y-m-d');
		}

		if(array_key_exists('status', $data)){
			$save['status'] = $data['status'];
		}else {
			$save['status'] = $this->input->post('status');
		}

		if(array_key_exists('kategori', $data)){
			$save['kategori'] = $data['kategori'];
		}else{
			$save['kategori'] = $this->input->post('category', 'Follow Up');
		}

		if(array_key_exists('from_id', $data)){
			$save['from_id'] = $data['from_id'];
		}else{
			// $save['from_id'] = $me->id_user;
			return ['status' => false, 'message' => 'Sender is not valid'];
		}

		$sender = $this->user_model->one_user(['where' => ['_.id_user' => $save['from_id']]]);

		$save['last_update'] = date('Y-m-d H:i:s');

		if(empty($receivers)){
			if(array_key_exists('receivers', $data)) {
				$receivers = $data['receivers'];
				unset($data['receivers']);
			}
			else {
				$receivers = $this->input->post('receivers');
			}
		}
		if(is_array($receivers) == false) {
			$receivers = [ $receivers ];
		}

		if(empty($dept)) {
			$dept = $this->input->post('department');
		}

		if(is_array($dept) == false) {
			$dept = [ $dept ];
		}
		if(empty($dept) == false) {
			$users = $this->user_model->many_user(['where_in' => ['dep.id_department' => $dept] ]);
			foreach ($users as $key => $user) {
				$userDept[] = $user['id_user'];
				$receivers[] = $user['id_user'];
			}
		}
		foreach ($user_bagian as $key => $id_user) {
			$receivers[] = $id_user;
		}

		$receiver_absensi = [];
		$receiver_support = [];

		if($save['is_absensi'] == 1) {
			$users = $this->user_model->many_user(['where_in' => ['_.id_user' => [48,62,68,81,67,55,37,57]]]);
			foreach ($users as $key => $usr) {
				$receivers[] = $usr['id_user'];
				$receiver_absensi[] = $usr['id_user'];
			}
		}

		if($save['is_it'] == 1) {
			$users = $this->user_model->many_user(['where_in' => [19]]);
			foreach ($users as $key => $usr) {
				$receiver_support[] = $usr['id_user'];
			}
		}

		$temp = [];
		foreach ($receivers as $key => $id_user) {
			if(in_array($id_user, $temp)) {
				continue;
			}
			if($id_user == $save['from_id']) {
				continue;
			}
			$temp[] = $id_user;
		}
		$receivers = $temp;


		if(count($receivers) == 0) {
			return ['status' => false, 'message' => 'Receiver is empty'];
		}

		// fungsi peniadaan notifikasi absensi khusus dokter PJ freelance
		$filterUsers_ = [];
		$users = $this->user_model->many_user(['where_in' => ['_.id_user' => $receivers]]);
		foreach ($users as $key => $usr) {
			$status = $usr['STATUS'];
			$id_position = $usr['id_position'];
			if ($status != "FREELANCE" && $id_position != 13){
				$filterUsers_[] = $usr['id_user'];
			}elseif($status != "FREELANCE" && $id_position == 13){
				$filterUsers_[] = $usr['id_user'];
			}elseif($status == "FREELANCE" && $id_position == 13 && (($save["absensi_des"] == "1") == true)){
				$filterUsers_[] = $usr['id_user'];
			}else{}
		}


		// fungsi peniadaan notifikasi absensi ke receivers to_id
        $filterUsersFinal = [];
            foreach ($filterUsers_ as $key => $usr) {
                if (!in_array($usr, $receivers)) {
                        $filterUsersFinal[] = $usr;
                }
            }

		// return $save;
		// $id = $this->input->post('id');
		if (array_key_exists('id', $data)){
			$save['id'] = $data['id'];
		}
		if(isset($save['id']) && !empty($save['id']) && ($save['id'] > 0)){
			$id = $save['id'];
			$this->db->where('id', $id)->update('dailyreports', $save);
		} else {
			$this->db->insert('dailyreports', $save);
			$id = $this->db->insert_id();
		}

		$dailyreport = $this->db->where('id', $id)->get('dailyreports')->row_array();
		if($dailyreport == null) {
			return ['status' => false, 'message' => 'Report tidak tersimpan', 'report' => $dailyreport];
		}
		// $dailyreport->attributes = $this->insert_attributes($dailyreport->id);

		//if(!$this->input->post('id')) {
			$dailyreport['receivers'] = $this->place_receivers($dailyreport['id'], $filterUsers_, $dailyreport);
		//}
		$dailyreport['attachments'] = $this->save_attachment($dailyreport['id'], $dailyreport['from_id'], $attachment_prefix, $attach_mode);
		$dailyreport['attachments'] = $this->save_attachmentJ($dailyreport['id'], $dailyreport['from_id'], $attachment_prefixJ, $attach_mode);
		$this->db->trans_complete();

		$notif = [];

		//if($data['is_edit'] == false){
			/* kirim notif ke penerima hanya jika ada yg no */
			$isNotifyReceiver = $save["reg_des"] == "2";
			$isNotifyReceiver = $isNotifyReceiver && $save["sampling_des"] == "3" && $save["qc_des"] == "4" && $save["alat_des"] == "5";
			$isNotifyReceiver = $isNotifyReceiver && $save["janji_des"] == "6" && $save["lain_des"] == "7" && $save["pelayanan_des"] == "8";
			$this->load->library('Notify_lib');
			$ntf = new Notify_lib();
			if($isNotifyReceiver == false) {
				// $notif[] = $this->send_notification(
				// 	// $this->input->post('receivers'),
				// 	$receivers,
				// 	'New Daily Report Received',
				// 	sprintf('%s has sent a new daily report', ucwords($sender['name'])),
				// 	$dailyreport
				// );
				$rcv = [];
				foreach ($receivers as $key => $id_rcv) {
					if(in_array($id_rcv, $receiver_absensi) == false) {
						$rcv[] = $id_rcv;
					}
				}

				// echo json_encode($rcv);
				// echo ("-------------");
				// echo json_encode($filterUsers_);
				// echo ("-------------");
				// echo json_encode($receiver_absensi);


				$ntf->send(
					'New Daily Report',
					$sender['name'] . ' has sent new Daily Report',
					$sender['id_user'],
					$rcv,
					['id' => $dailyreport['id']],
					'dailyreport',
					true
				);
			}

			if($data['absensi_des'] != '1') {
				$ntf->send(
					'New Absensi Report',
					$sender['name'] . ' has sent new Absensi Report',
					$sender['id_user'],
					$receiver_absensi,
					['id' => $dailyreport['id']],
					'dailyreport',
					true
				);
			}

			// if(in_array($data['absensi_des'], ['1']) == false) {
			// 	$this->load->model('m_absensi');
			// 	$data_absensi = [
			// 		'id_dailyreports' => $dailyreport['id'],
			// 		'from_id' => $this->user_id,
			// 		'absensi_des' => $data['absensi_des'],
			// 		'send_notif' => false
			// 	];
			// 	$r = $this->m_absensi->save(
			// 		$data_absensi,
			// 		[48], // user receiver
			// 		[], // dept receiver
			// 		[], // pos receiver
			// 		[22], // bagian
			// 		'__file__'
			// 	);

			// }
			/* jika daily report merupakan report absensi, tambahkan hrd sebagai tujuan daily report dan kirim notifikasi ke hrd */
			if(($save["absensi_des"] == "1") == false) {
				$list_id_hrd = [];
				$this->load->model("m_mis");
				$hrds = $this->m_mis->get_user_hrd()->result_array();

				/* tambah user hrd sebagai tujuan daily report */
				foreach ($hrds as $key => $hrd) {
					$this->db->insert("dailyreports_receiver", [
						"id_dailyreports" => $id,
						"id_receiver" => $hrd["id_user"]
					]);
					$list_id_hrd[] = $hrd["id_user"];
				}

				if(count($list_id_hrd) > 0) {
					$notif[] = $this->send_notification($list_id_hrd, "New Presence Report Received", ucwords($sender['name']) . ": " . $save["absensi_des"], $dailyreport);
				}

				$data_absensi = [
					'id_dailyreports' =>$id,
					'from_id' => $this->user_id,
					'absensi_des' => $data['absensi_des'],
					'send_notif' => true
				];
				$this->load->model('m_absensi');
				$r = $this->m_absensi->save( 
					$data_absensi, 
					$filterUsersFinal, // user receiver
					[], // dept receiver
					[], // pos receiver
					[22], // bagian
					'__file__'
				);
			}
		//}

		$dailyreport["notifications"] = $notif;

		// return $dailyreport;
		return ['status' => true, 'message' => 'Daily Report berhasil disimpan', 'report' => $dailyreport];
	}

	private function save_attachment($id, $creator_id, $prefix = 'file_', $mode = 'seq')
	{
		// echo "id: $id, prefix: $prefix, mode: $mode";
		// exit();
		/*
		mode = seq : file diupload dengan name prefix_0, prefix_1, dst
		mode = array : file diupload dengan array of files
		*/
		$n = [];
		$config = [
			'upload_path' => './uploads/attachments/dailyreport/' . $id . '/',
			'encrypt_name' => true,
			'allowed_types' => '*'
		];
		$directory = $config['upload_path'];
		$old = umask(0);
		if(file_exists($directory) == false) {
			mkdir($directory, 0777, true);
		}
		umask($old);
		if($mode == 'array'){
			if(!empty($_FILES)){
				// $directory = sprintf('./uploads/attachments/dailyreport/%s/', sha1(md5($id)));
				// $directory = './uploads/attachments/dailyreport/' . $id . '/';
				// $n[] = $directory;
				// $directory = sprintf('./uploads/attachments/dailyreport/%s/', sha1(md5($id)));

				// $config = [
				// 	'encrypt_name' => true
				// ];
				// $config['upload_path']          = $directory;
		        // $config['allowed_types']        = 'gif|jpg|png|jpeg|zip|txt|doc|docx|xls|xlsx|pdf|bmp|rar|ppt|pptx';
		        // $config['allowed_types']        = '*';
		        // $config['max_size']             = 1024 * 100;  /*  KB */
		        $this->load->library('upload', $config);
		        $this->upload->initialize($config);

		        // $n[] = $attachments = $this->input->post("attachments");
		        $counterAttachment = 0;
		        foreach ($_FILES as $key => $value) {
		        	// break;
		        	if($this->upload->do_upload($key)){
		        		// $attachment = $this->input->post('attachments')[$n];
		        		// $attachment = $attachments[$counterAttachment++];
		        		// $desc = $this->input->post('file_desc_' . $)
		        		$uploaded = $this->upload->data();
		        		$this->db->insert('dailyreports_attachment', [
		        			'dailyreport_id' => $id,
		        			// 'title' => $attachment['title'],
		        			// 'title' => $attachment['title'],
		        			// 'description' => $attachment['description'],
		        			// 'description' => $attachment['description'],
		        			'path' => sprintf('%s%s', substr($directory, 2), $value['name']),
		        			'filetype' => $value['type'],
		        			'filesize' => $value['size'],
		        			'creator_id' => $creator_id
		        		]);
		        		$n[] = [
		        			"attachment" => $attachment,
		        			"val" => $value,
		        			"uploaded" => $uploaded
		        		];
		        	} else {
		        		$n[] = $this->upload->display_errors();
		        	}
		        	// $n++;
		        }
			}
		}

		else if($mode == 'seq') {
			$counter = -1;
			while(true) {
				$counter++;
				if($counter > 200) {
					break;
				}
				$file_key = $prefix . $counter;
				$title_key = $prefix . 'title_' . $counter;
				$desc_key = $prefix . 'desc_' . $counter;

				if(isset($_FILES[$file_key]) == false) {
					break;
				}

				$this->upload->initialize($config);
				if($this->upload->do_upload($file_key)) {
					$uploaded = $this->upload->data();
					$title = $this->input->get_post($title_key);
					$desc = $this->input->get_post($desc_key);

					$d = [
						'dailyreport_id' => $id,
						'title' => $title,
						'description' => $desc,
						'path' => $config['upload_path'] . $uploaded['file_name'],
						'filetype' => $uploaded['file_type'],
						'filesize' => $uploaded['file_size'],
						'name' => $uploaded['orig_name'],
						'creator_id' => $creator_id,
						'type' => 0,
					];

					$this->db->insert('dailyreports_attachment', $d);

					$n[] = $d;
				}
				else {
					$n[] = $this->upload->display_errors();
				}
			}
		}

		return $n;
	}

	private function save_attachmentJ($id, $creator_id, $prefix = 'fileJ_', $mode = 'seq')
	{
		// echo "id: $id, prefix: $prefix, mode: $mode";
		// exit();
		/*
		mode = seq : file diupload dengan name prefix_0, prefix_1, dst
		mode = array : file diupload dengan array of files
		*/
		$n = [];
		$config = [
			'upload_path' => './uploads/attachments/dailyreport/' . $id . '/',
			'encrypt_name' => true,
			'allowed_types' => '*'
		];
		$directory = $config['upload_path'];
		$old = umask(0);
		if(file_exists($directory) == false) {
			mkdir($directory, 0777, true);
		}
		umask($old);
		if($mode == 'array'){
			if(!empty($_FILES)){
				// $directory = sprintf('./uploads/attachments/dailyreport/%s/', sha1(md5($id)));
				// $directory = './uploads/attachments/dailyreport/' . $id . '/';
				// $n[] = $directory;
				// $directory = sprintf('./uploads/attachments/dailyreport/%s/', sha1(md5($id)));

				// $config = [
				// 	'encrypt_name' => true
				// ];
				// $config['upload_path']          = $directory;
		        // $config['allowed_types']        = 'gif|jpg|png|jpeg|zip|txt|doc|docx|xls|xlsx|pdf|bmp|rar|ppt|pptx';
		        // $config['allowed_types']        = '*';
		        // $config['max_size']             = 1024 * 100;  /*  KB */
		        $this->load->library('upload', $config);
		        $this->upload->initialize($config);

		        // $n[] = $attachments = $this->input->post("attachments");
		        $counterAttachment = 0;
		        foreach ($_FILES as $key => $value) {
		        	// break;
		        	if($this->upload->do_upload($key)){
		        		// $attachment = $this->input->post('attachments')[$n];
		        		// $attachment = $attachments[$counterAttachment++];
		        		// $desc = $this->input->post('file_desc_' . $)
		        		$uploaded = $this->upload->data();
		        		$this->db->insert('dailyreports_attachment', [
		        			'dailyreport_id' => $id,
		        			// 'title' => $attachment['title'],
		        			// 'title' => $attachment['title'],
		        			// 'description' => $attachment['description'],
		        			// 'description' => $attachment['description'],
		        			'path' => sprintf('%s%s', substr($directory, 2), $value['name']),
		        			'filetype' => $value['type'],
		        			'filesize' => $value['size'],
		        			'creator_id' => $creator_id
		        		]);
		        		$n[] = [
		        			"attachment" => $attachment,
		        			"val" => $value,
		        			"uploaded" => $uploaded
		        		];
		        	} else {
		        		$n[] = $this->upload->display_errors();
		        	}
		        	// $n++;
		        }
			}
		}

		else if($mode == 'seq') {
			$counter = -1;
			while(true) {
				$counter++;
				if($counter > 200) {
					break;
				}
				$file_key = $prefix . $counter;
				$title_key = $prefix . 'title_' . $counter;
				$desc_key = $prefix . 'desc_' . $counter;

				if(isset($_FILES[$file_key]) == false) {
					break;
				}

				$this->upload->initialize($config);
				if($this->upload->do_upload($file_key)) {
					$uploaded = $this->upload->data();
					$title = $this->input->get_post($title_key);
					$desc = $this->input->get_post($desc_key);

					$d = [
						'dailyreport_id' => $id,
						'title' => $title,
						'description' => $desc,
						'path' => $config['upload_path'] . $uploaded['file_name'],
						'filetype' => $uploaded['file_type'],
						'filesize' => $uploaded['file_size'],
						'name' => $uploaded['orig_name'],
						'creator_id' => $creator_id,
						'type' => 1,
					];

					$this->db->insert('dailyreports_attachment', $d);

					$n[] = $d;
				}
				else {
					$n[] = $this->upload->display_errors();
				}
			}
		}

		return $n;
	}


	private function place_receivers($id, $receivers, $report = null)
	{
		$oldReceiver = $this->db->where('id_dailyreports', $id)->get('dailyreports_receiver')->result();
		
		
		
		if(count($oldReceiver)>0){
			$this->db->where('id_dailyreports', $id)->delete('dailyreports_receiver');
			foreach ($receivers as $uid) {
				if(empty($uid)) {
					continue;
				}
				if($report != null && $report['from_id'] == $uid) {
					continue;
				}
				$is_exist = $this->db->where(['id_dailyreports' => $id,'id_receiver' => $uid])->get('dailyreports_receiver')->num_rows();
				if($is_exist==0){
					$this->db->insert('dailyreports_receiver', [
						'id_dailyreports' => $id,
						'id_receiver' => $uid
					]);
				}
			}
		}
		else{
			// foreach ($this->input->post('receivers') as $uid)
			// 	$this->db->insert('dailyreports_receiver', [
			// 		'id_dailyreports' => $id,
			// 		'id_receiver' => $uid
			// 	]);
			foreach ($receivers as $uid) {
				if(empty($uid)) {
					continue;
				}
				if($report != null && $report['from_id'] == $uid) {
					continue;
				}
				$this->db->insert('dailyreports_receiver', [
					'id_dailyreports' => $id,
					'id_receiver' => $uid
				]);
			}
		}
		return $this->db->where('id_dailyreports', $id)->get('dailyreports_receiver')->result();
	}

	public function mine($id_user, $query)
	{
		// $this->db->select('dailyreports.*,branch.*');
		// $this->db->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
		// $this->db->select('users.image');
		// $this->db->select('users.name, users.username');
		// $this->db->join('users', 'users.id_user = dailyreports.from_id');
		// $this->db->join('branch','branch.branch_id = users.branch_id');
		// // $query = $this->input->post('query', false);
		// // if($query && strlen($query) < 2) $query = false;
		// if($query){
		// 	$this->db->like('dailyreports.title', $query);
		// }

		$data = [];

		$queryCountReply = $this->db->from('dailyreports_reply')
							->select([
								'id_dailyreports',
								'count(*) as jumlah_reply'
							])
							->group_by('id_dailyreports')
							->get_compiled_select();

		$q = $this->db->from('dailyreports d')
			->join("($queryCountReply) creply", 'creply.id_dailyreports = d.id', 'left')
			->join('users usr', 'usr.id_user = d.from_id')
			->where('d.from_id', $id_user)
			// ->order_by('d.last_update', 'desc')
			// ->order_by('d.id', 'desc')
			->select([
				'd.*',
				'coalesce(creply.jumlah_reply,0) as jumlah_reply',
				'usr.name',
				'usr.call_name',
				'usr.username',
				'usr.image'
			])
			->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false)
			->select('case when d.is_view = 0 then d.last_update else d.created_at end as last_update2', false);
		if($query) {
			$q->like('d.title', $query);
		}

		$sql = $q->get_compiled_select();
		$sql = "SELECT * from ($sql) s1 order by last_update2 desc, id desc";
		$data['sent'] = $this->db->query($sql)->result();
		// $data['sent'] = $q->get()->result();

		$q	= $this->db->from('dailyreports_receiver dr')
			->join('dailyreports d', 'd.id = dr.id_dailyreports')
			->join("($queryCountReply) as creply", 'creply.id_dailyreports = d.id', 'left')
			->join('users usr', 'usr.id_user = d.from_id')
			->where('dr.id_receiver', $id_user)
			// ->order_by('dr.is_view', 'asc')
			// ->order_by('d.id', 'desc')
			->select([
				'dr.is_view',
				'd.status',
				'd.title',
				'd.id',
				'd.date',
				'coalesce(creply.jumlah_reply,0) as jumlah_reply',
				'usr.name',
				'usr.username',
				'usr.call_name',
				'usr.image'
			])
			->select('case when dr.is_view = 0 or dr.is_view_comment = 0 then d.last_update else d.created_at end as last_update2', false)
			->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
		if($query) {
			$q->like('d.title', $query);
		}
		$sql = $q->get_compiled_select();
		$sql = "SELECT * from ($sql) s1 order by last_update2 desc, id desc";
		$data['received'] = $this->db->query($sql)->result();
		// $data['received'] = $q->get()->result();
		// if($received_dailyreports){
			// foreach ($received_dailyreports as $item) $ids[] = $item->id_dailyreports;
			// foreach ($received_dailyreports as $item) $ids1[] = $item->id_receiver;
			// $this->db->select('dailyreports.*,branch.*');
			// $this->db->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
			// $this->db->select('users.image');
			// $this->db->select('users.name, users.username');
			// $this->db->select('dailyreports_receiver.is_view');
			// $this->db->join('users', 'users.id_user = dailyreports.from_id');
			// $this->db->join('dailyreports_receiver', 'dailyreports_receiver.id_dailyreports = dailyreports.id');
			// $this->db->join('branch','branch.branch_id = users.branch_id');
			// $this->db->where_in('dailyreports.id', $ids);
			// $this->db->where_in('dailyreports_receiver.id_receiver', $ids1);
			// if($query){
			// 	$this->db->like('dailyreports.title', $query);
			// }
			// $data['received'] = $this->db->order_by('dailyreports.id', 'desc')->get('dailyreports')->result();
		// }
		return $data;
	}

	public function send_notification($ids = [0], $title, $message, $data = []) {
		$this->load->library("Notify_lib");
		$nl = new Notify_lib();
		return $nl->send($title, $message, $this->input->post("user_id"), $ids, $data, "dailyreport", true);
	}

	function reply($data)
	{
		/*
			data: {
				id: id daily report,
				user_id: user yang membalas,
				message: isi balasan
			}
		*/
		// $insert = $this->db->insert('dailyreports_reply', [
		// 	'id_dailyreports' => $this->input->post('id'),
		// 	'sender' => $this->input->post('user_id'),
		// 	'message_reply' => $this->input->post('message'),
		// 	'date_added' => date('Y-m-d H:i:s')
		// ]);
		$reply_id = null;
		$insert = 1;
		$idReply = 0;
		if(array_key_exists('reply_id', $data)) {
			$reply_id = $data['reply_id'];
			unset($data['reply_id']);
		}
		if(array_key_exists('message', $data) == false && array_key_exists('message_reply', $data) == true) {
			$data['message'] = $data['message_reply'];
		}
		if(array_key_exists('id', $data) == false && array_key_exists('id_dailyreports', $data) == true) {
			$data['id'] = $data['id_dailyreports'];
		}
		if(array_key_exists('sender', $data) == true && array_key_exists('user_id', $data) == false) {
			$data['user_id'] = $data['sender'];
		}
		$dailyreport = $this->one(['where' => ['_.id' => $data['id']]]);
		if($dailyreport == null) {
			return ['status' => false, 'message' => 'Daily Report not found'];
		}
		$this->load->model('restapi/user_model');
		$sender = $this->user_model->one_user(['where' => ['_.id_user' => $data['user_id']]]);
		if($sender == null) {
			return ['status' => false, 'message' => 'Sender tidak valid'];
		}
		// var_dump($dailyreport);
		// var_dump($sender);
		// exit();
		if(in_array($sender['id_department'], [24, 26]) && $dailyreport->is_it == '1') {
			$this->load->model('restapi/generalreports_model');
			$generalreport = $this->generalreports_model->one(['where' => ['_.id_dailyreports' => $dailyreport->id ]]);
			if($generalreport == null) {
				return ['status' => false, 'message' => 'General report not found'];
			}
			return $this->generalreports_model->reply([
				'id' => $generalreport->id,
				'user_id' => $sender['id_user'],
				'message' => $data['message']
			]);
		}
		else if(in_array($sender['id_department'], [27]) && $dailyreport->is_absensi == '1') {
			$this->load->model('m_absensi');
			$absensi = $this->m_absensi->one(['where' => ['_.id_dailyreports' => $dailyreport->id]]);
			if($absensi == null) {
				return ['status' => false, 'message' => 'Absensi not found'];
			}
			// print_r($absensi);
			// exit();
			return $this->m_absensi->reply([
				'id' => $absensi['id'],
				'user_id' => $sender['id_user'],
				'message' => $data['message']
			]);
		}
		// print_r($sender);
		// exit();
		if($reply_id != null){
			$insert = $this->db->where(['id' => $reply_id])
				->update('dailyreports_reply',[
					'message_reply' => $data['message']
				]);
			$idReply = $reply_id;
		}else{
			$insert = $this->db->insert('dailyreports_reply', [
				'id_dailyreports' => $data["id"],
				'sender' => $data['user_id'],
				'message_reply' => $data['message'],
				'date_added' => date('Y-m-d H:i:s')
			]);
			$idReply = $this->db->insert_id();
		}

		$this->db->where(['id' => $data['id']])->update('dailyreports', [ 'last_update' => date('Y-m-d H:i:s') ]);
		// $this->db->where(['id_dailyreports' => $data['id']])->update('dailyreports_receiver', ['is_view_comment' => 0]);

		$dailyReport = $this->db->from("dailyreports dr")
			->join("users usr", "usr.id_user = dr.from_id", "left")
			->where(["dr.id" => $data["id"]])
			->select([
				"dr.*",
				"usr.name as sender_name"
			])
			->get()->row_array();

		if($dailyReport['from_id'] == $data['user_id']) {
			/* yg me-reply adalah creator report */
			$this->db->where(['id_dailyreports' => $data['id'], 'is_closed' => 0])->update('dailyreports_receiver', ['is_view_comment' => 0]);
		} else {
			/* yg me-reply adalah salah satu receiver */
			$exc = [ $data['user_id'] ];
			$this->db->where([
					'id' => $data['id']
				])
				->where_in('is_view', [0,1])
				->update('dailyreports', ['is_view' => 0]);

			$this->db->where([
					'id_dailyreports' => $data['id'],
					'is_closed' => 0
				])
				->where_not_in('id_receiver', $exc)
				->update('dailyreports_receiver', [
					'is_view_comment' => 0
				]);
		}

		$sender = $this->db->from("users usr")
			->where(["usr.id_user" => $data["user_id"]])
			->get()->row_array();

		$r = false;

		if($dailyReport != null && $sender != null) {
			/* kirim notify ke penerima lain */
			$listReceiver = $this->getReceivers($dailyReport["id"]);
			$idReceivers = [];
			/* jika yg membalas dailyreport bukan user pembuat, tambahkan pembuat dailyreport ke penerima notif */
			if($sender["id_user"] != $dailyReport["from_id"]) {
				$idReceivers[] = $dailyReport["from_id"];
			}
			foreach ($listReceiver as $key => $rc) {
				/* jika penerima dailyreport adalah yg sedang membalas, skip */
				if($rc["id_user"] == $data["user_id"]) {
					continue;
				}
				if(in_array($rc['id_user'], $idReceivers) == false) {
					$idReceivers[] = $rc["id_user"];
				}
			}
			$this->load->library("Notify_lib");
			$nt = new Notify_lib();
			$r = $nt->send(
				"Daily Report Reply", 										//title
				// $dailyReport["sender_name"] . " has send a new reply", 	// msg
				$sender["name"] . " has send a new reply", 	// msg
				$sender["id_user"],  									//sender
				$idReceivers,  											//receiver
				$dailyReport,											//payload
				"dailyreport",											//tipe notif
				true
			);

		} else {
			$r = "";
			if($dailyReport == null) {
				$r = "daily report is null. ";
			}
			if($sender == null) {
				$r .= "sender is null. ";
			}
		}

		return ["insert" => $insert, "notif" => $r, 'status' => true, 'id' => $idReply, 'message' => 'Reply berhasil disimpan'];
	}

	function save_reply_attachment($data) {
		$this->db->insert('dailyreport_reply_attachment', $data);
		return ['status' => true, 'message' => 'success', 'id' => $this->db->insert_id() ];
	}

	function delete()
	{
		$deleted = $this->db->where('id', $this->input->post('id'))->delete('dailyreports');
		return ['deleted' => $deleted];
	}

	function getReceivers($id_dailyreport) {
		$q = $this->db->from("dailyreports_receiver dc")
			->join("users usr", "usr.id_user = dc.id_receiver", "left")
			->join('position p', 'p.id_position = usr.id_position', 'left')
			->join('departments dep', 'dep.id_department = p.id_department', 'left')
			->where([
				"dc.id_dailyreports" => $id_dailyreport
			])
			->select([
				"usr.name",
				'usr.username',
				'usr.call_name',
				'dc.is_view',
				'dc.is_view_comment',
				'usr.image',
				'p.name_position as position',
				'dep.name as department',
				'usr.id_user'
			])
			->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false)
			->get()->result_array();

		return $q;
	}

	function baseQuery($args=[]){
		if(isset($args['join_receiver']) == false) {
			$args['join_receiver'] = false;
		}
		if(isset($args['join_reply']) == false) {
			$args['join_reply'] = true;
		}
		if(isset($args['join_absensi']) == false) {
			$args['join_absensi'] = false;
		}
		if(isset($args['join_it']) == false) {
			$args['join_it'] = false;
		}

		$qReply = '';
		if($args['join_reply'] == true) {
			$q = $this->db->from('dailyreports_reply _')
				->select('count(*) as jumlah', false)
				->select('GROUP_CONCAT(_.message_reply) as reply_all', false)
				->select([
					'id_dailyreports'
				])
				->group_by('id_dailyreports');
			$qReply = $q->get_compiled_select();
		}
		$r = $this->db->from("dailyreports _")
			->join('users usr', 'usr.id_user = _.from_id', 'left')
			->select([
				'_.*',
				'usr.name',
				'usr.username',
				'usr.call_name',
				'usr.image'
			]);

		if($args['join_receiver'] == true) {
			$r->join('dailyreports_receiver rcv', 'rcv.id_dailyreports = _.id', 'left')
				->select('case when (rcv.is_view = 0 or rcv.is_view_comment = 0) then 0 else 1 end as is_view_rcv', false);
		}

		if($args['join_reply'] == true) {
			$r->join("($qReply) rply", 'rply.id_dailyreports = _.id', 'left')
				->select([
					'coalesce(rply.jumlah, 0) as jumlah_reply'
				]);
		}

		if($args['join_absensi'] == true) {
			$r  ->join('absensi absen', 'absen.id_dailyreports = _.id', 'left')
				->join('receivers ra', "ra.id_fk = absen.id and ra.table_fk = 'absensi'", 'left');
		}

		if($args['join_it'] == true) {
			$r 	->join('generalreports gr', 'gr.id_dailyreports = _.id', 'left')
				->join('generalreports_receiver rg', 'rg.id_generalreports = gr.id', 'left');
		}

		/*by default join in on branch & position */
		$r 	->join('branch br', 'br.branch_id = usr.branch_id', 'left')
			->join('position pos', 'pos.id_position = usr.id_position', 'left')
			->select([
					'br.branch_name','pos.id_position','pos.name_position'
				]);

		return $r;
	}

	function many($args=[]){
		$this->load->helper('myquery');
		$q = myquery($this->baseQuery($args), $args);
		// return $q->get_compiled_select();
		return $q->get()->result();
	}

	function one($args = []) {
		$this->load->helper('myquery');
		return myquery($this->baseQuery($args), $args)->get()->row();
	}

	function update($where, $data){
		$targets = $this->many(['where'=>$where]);
		if(count($targets)==0){
			return ['status'=>false, 'message'=>'Data not found'];
		}
		foreach ($targets as $key => $target) {
			$this->db->where(['id' => $target->id])->update('dailyreports', $data);
		}
		return ['status'=>true,'message'=>'success'];
	}

	function queryReceived($args) {
		$this->load->helper('myquery');
		$args['join_receiver'] = true;
		// $args['join_absensi'] = true;
		// $args['join_it'] = true;
		$q = myquery($this->baseQuery($args), $args);
		if(isset($args['id_user'])) {
			$id_user = $args['id_user'];
			$q 	->group_start()
					->or_where(['rcv.id_receiver' => $id_user])
					// ->or_where(['rg.id_receiver' => $id_user])
					// ->or_where(['ra.id_receiver' => $id_user])
				->group_end();

		}
		$q->select('case when (rcv.is_view = 0 or rcv.is_view_comment = 0) then _.last_update else _.created_at end as last_update2', false);
		// $q->select('case when (rcv.is_view = 0 or rcv.is_view_comment = 0 or rg.is_view = 0 or rg.is_view_comment = 0 or ra.view_at is null or ra.view_comment_at is null) then _.last_update else _.created_at end as last_update2', false);
		// return $q->get()->result();
		$sql = $q->get_compiled_select();
		// echo $sql;
		// exit();
		return $sql;
	}

	function querySent($args) {
		$this->load->helper('myquery');
		$args['join_receiver'] = false;
		if(isset($args['where_in']) == false) {
			$args['where_in'] = [];
		}
		$args['where_in']['_.is_view'] = ['0','1'];
		$q = myquery($this->baseQuery($args), $args);
		// return $q->get()->result();
		$q 	->select('0 as is_view_rcv')
			->select('case when _.is_view = 0 then _.last_update else _.created_at end as last_update2', false);
		$sql = $q->get_compiled_select();
		// echo $sql;
		// exit();
		return $sql;
	}

	function queryArchived($id_user, $args) {
		$argsReceived = $args;
		$argsReceived['where']['rcv.id_receiver'] = $id_user;

		$argsSent = $args;
		$argsSent['where']['_.from_id'] = $id_user;

		$sqlReceived = $this->queryReceived($argsReceived);
		$sqlSent = $this->querySent($argsSent);

		$sql = "$sqlReceived  \n\n\nunion  \n\n\n$sqlSent";

		return $sql;
	}

	function getReceived($id_user, $args) {
		// $args['where']['rcv.id_receiver'] = $id_user;
		$args['where']['upper(_.status)'] = 'OPEN';
		$args['join_receiver'] = true;
		$args['id_user'] = $id_user;
		$limit = [];
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		$sql = $this->queryReceived($args);
		$q = myquery($this->db->from("($sql) s1"), ['limit' => $limit, 'order' => [['last_update2', 'desc'], ['id', 'desc']]]);
		// echo $q->get_compiled_select();
		// exit();
		return $q->get()->result();
		// echo $q->get_compiled_select();
	}

	function getSent($id_user, $args) {
		$args['where']['_.from_id'] = $id_user;
		$args['where']['upper(_.status)'] = 'OPEN';
		$limit = [];
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}

		$sql = $this->querySent($args);
		$q = myquery($this->db->from("($sql) s1"), ['limit' => $limit, 'order' => [['last_update2', 'desc'], ['id', 'desc']]]);
		return $q->get()->result();
		// echo $q->get_compiled_select();
	}

	function getArchived($id_user, $args) {
		$id_user = intval($id_user);
		$args['where']['upper(_.status)'] = 'CLOSE';
		$args['join_receiver'] = true;
		$limit = [];
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}

		$sql = $this->queryArchived($id_user, $args);
		$q = myquery($this->db->from("($sql) s1"), ['limit' => $limit, 'order' => [ ['last_update2', 'desc'], ['id', 'desc'] ]]);
		$sql = $q->get_compiled_select();

		// echo $sql . "\n\n\n";
		return $this->db->query($sql)->result();
	}

	function getCountReceived($id_user, $args) {
		$args['where']['rcv.id_receiver'] = $id_user;
		$args['where']['upper(_.status)'] = 'OPEN';
		$q = $this->queryReceived($args);
		$sql = "select count(*) as jumlah from ($q) s1";
		return $this->db->query($sql)->row();
	}

	function getCountSent($id_user, $args) {
		$args['where']['_.from_id'] = $id_user;
		$args['where']['upper(_.status)'] = 'OPEN';
		$q = $this->querySent($args);
		$sql = "select count(*) as jumlah from ($q) s1";
		return $this->db->query($sql)->row();
	}

	function getCountArchived($id_user, $args) {
		$args['where']['upper(_.status)'] = 'CLOSE';
		$args['join_receiver'] = true;
		$sql = $this->queryArchived($id_user, $args);
		$sql = "select count(*) as jumlah from ($sql) s1";
		return $this->db->query($sql)->row();
	}

	function markAsRead($id) {
		$this->db->set('is_view', 1);
		$this->db->where('id', $id);
		$this->db->update('dailyreports');
	}

	function readReceiver($id, $receiverId) {
		$this->db->set('is_view', 1);
		$this->db->set('is_view_comment', 1);
		$this->db->where('id_dailyreports', $id);
		$this->db->where('id_receiver', $receiverId);
		$this->db->update('dailyreports_receiver');
	}
}
