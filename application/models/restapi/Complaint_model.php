<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Memento admin_model model
 *
 * This class handles admin_model management related functionality
 *
 * @package		Admin
 * @subpackage	admin_model
 * @author		propertyjar
 * @link		#
 */

class complaint_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function action()
	{
		if ($this->input->post('id')) :
			return $this->db->where([
				'id' => $this->input->post('id')
			])->update('complaint_actions', ['content' => $this->input->post('content')]);
		else :
			$this->db->insert('complaint_actions', [
				'author_id' => $this->input->post('user_id'),
				'complaint_id' => $this->input->post('complaint_id'),
				'type' => $this->input->post('type'),
				'content' => $this->input->post('content'),
				'action_time' => date('Y-m-d H:i:s')
			]);
			return $this->detail($this->input->post('complaint_id'));
		endif;
	}

	function setStatus($id, $value)
	{
		return $this->db->where([
			'id' => $id
		])->update('complaints', ['state' => $value]);
	}

	function detail($id)
	{
		$user_id = $this->input->post('user_id');
		if ($user_id) {
			$this->db->where([
				'receiver_id' => $user_id,
				'complaint_id' => $id
			])->update('complaint_receiver', ['read' => 1]);

			/* update notifications sudah dibaca */
			$this->load->library("Notify_lib");
			$nl = new Notify_lib();
			$nl->mark_read($id, "complaint", $id);
			// $this->db->where([
			// 		"to_id" => $user_id
			// 	])
			// 	->like("data", '"complaint_id":' . $id)
			// 	->group_start()
			// 		->or_where(["read" => null])
			// 		->or_where(["read" => 0])
			// 	->group_end()
			// 	// ->where(["anime" => "naruto"])
			// 	->update("notifications", ["read" => 1]);

		}
		$complaint = $this->db->where('id', $id)->get('complaints')->row();


		$this->db->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
		$this->db->select('users.image');
		$this->db->select('users.*, position.name_position as position, position.id_department,branch.*,branch.branch_name as branch');
		$this->db->join('position', 'position.id_position=users.id_position', 'left');
		$this->db->join('branch', 'branch.branch_id=users.branch_id');
		$complaint->author = $this->db->where('users.id_user', $complaint->creator_id)->get('users')->row();

		$attributes = $this->db->where('complaint_id', $complaint->id)->get('complaint_attributes')->result();
		$complaint->attr = new stdClass();
		foreach ($attributes as $value) {
			$complaint->attr->{$value->key} = $value->value;
		}
		// $this->db->select('users.*, read');
		// $this->db->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
		// $this->db->select('users.image');
		// $this->db->select('position.name_position as position, position.id_department');
		// $this->db->join('users', 'complaint_receiver.receiver_id=users.id_user');
		// $this->db->join('position', 'position.id_position=users.id_position', 'left');
		// $this->db->where('complaint_receiver.complaint_id', $complaint->id);
		// $complaint->receivers = $this->db->get('complaint_receiver')->result();
		$complaint->receivers = $this->getReceivers($complaint->id);

		$this->db->select('users.*');
		$this->db->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
		$this->db->select('users.image');
		$this->db->select('position.name_position as position, position.id_department');
		$this->db->select('complaint_actions.*');
		$this->db->join('users', 'complaint_actions.author_id=users.id_user');
		$this->db->join('position', 'position.id_position=users.id_position', 'left');
		$this->db->where('complaint_actions.complaint_id', $complaint->id);
		$complaint->actions = $this->db->get('complaint_actions')->result();

		$complaint->attachments = $this->db->where('complaint_id', $id)->get('complaint_attachment')->result();
		return $complaint;
	}

	function getReceivers($id_complaint)
	{
		$q = $this->db->from('complaint_receiver _')
			->join('users usr', 'usr.id_user = _.receiver_id', 'left')
			->join('position p', 'p.id_position = usr.id_position', 'left')
			->join('departments dep', 'dep.id_department = p.id_department', 'left')
			->where(['_.complaint_id' => $id_complaint])
			->select([
				'_.read',
				'usr.name',
				'usr.call_name',
				'usr.username',
				'usr.image',
				'p.name_position as position',
				'dep.name as department',
				'usr.id_user'
			]);
		return $q->get()->result();
	}

	public function create()
	{
		$this->db->trans_start();
		$this->db->insert('complaints', [
			'creator_id' => $this->input->post('user_id'),
			'pengaju_komplain' => $this->input->post('pengaju_komplain'),
			'branch_id' => $this->input->post('branch_id'),
			'id_pasien' => $this->input->post('id_pasien'),
			'nama_pasien' => $this->input->post('nama_pasien'),
			'nomor_registrasi' => $this->input->post('nomor_registrasi'),
			'jenis_komplain' => $this->input->post('jenis_komplain'),
			'waktu_komplain' => $this->input->post('waktu_komplain'),
			'waktu_respon' => $this->input->post('waktu_respon'),
			'deskripsi' => $this->input->post('attributes')['deskripsi'],
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		]);
		$id = $this->db->insert_id();
		$complaint = $this->db->where('id', $id)->get('complaints')->row();

		if ($complaint == null) {
			return [
				'status' => false,
				'message' => 'Failed to save complaint'
			];
		}

		$complaint->attributes = $this->insert_attributes($complaint->id);

		$this->load->model('restapi/user_model');
		$additionalReceiver = [];
		$pos = [
			1, /*direktur utama */
			//6, /* gm opt */
			//15, /* deputi gm opt */
			//3, /* bussiness development */
			// 7, /* BM */
			4 /* qmr */
		];
		$users = $this->user_model->many_user(['where_in' => ['pos.id_position' => $pos]]);

		$receivers = $this->input->post('receivers');
		if (is_array($receivers) == false) {
			$receivers = [];
		}

		foreach ($users as $key => $user) {
			$receivers[] = $user['id_user'];
		}

		$this->load->model('restapi/user_model');
		$users = $this->user_model->many_user(['where_in' => ['_.id_position' => 13, '_.STATUS' => 'FREELANCE' ]]);
		
		foreach ($users as $key => $usr) {
			// mengubah string pada array many_branch menjadi array
			// echo json_encode($usr);
			$input   = $usr['many_branch'];
			$numbers = str_split("',");
			$output  = str_replace($numbers,' ',$input);
			$numArray = explode(" ", $output);
			$final = (array_values(array_filter($numArray)));
			foreach ($final as $key => $x) {
				// $this->branch_id
				if ($x == $this->input->post('branch_id')){
					$receivers[] = $usr['id_user'];;
				}
			}
		}

		$receivers[] = 62; //dr.Edy
		$receivers[] = 29; //Lisa
		$receivers[] = 70; //dr.Vero
		

		/* hilangkan duplikat */
		$tempReceivers = [];
		foreach ($receivers as $key => $rcv) {
			if (in_array($rcv, $tempReceivers)) {
				continue;
			}
			$tempReceivers[] = $rcv;
		}

		$receivers = $tempReceivers;

		echo json_encode($receivers);

		// $complaint->receivers = $this->place_receivers($complaint->id, $this->input->post('receivers'));
		$complaint->attachments = $this->save_attachment($complaint->id);
		$this->db->trans_complete();
		
		// kirim semua ke table receivers
		foreach ($receivers as $uid) {
			$this->db->insert('complaint_receiver', [
				'complaint_id' => $complaint->id,
				'receiver_id' => $uid
			]);
		}

		$me = $this->db->where('id_user', $this->input->post('user_id'))->get('users')->row();

		$this->send_notification(
			// $this->input->post('receivers'),
			$receivers,
			'New Complaint Received',
			sprintf('%s has sent a new complaint', ucwords($me->name)),
			(array) $complaint
		);

		// return $complaint;
		return [
			'status' => true,
			'message' => 'Complaint saved successfuly',
			'data' => $complaint
		];
	}

	private function save_attachment($id)
	{
		$result = [];
		if (!empty($_FILES)) {
			$directory = sprintf('./uploads/attachments/complaint/%s/', sha1(md5($id)));
			$old = umask(0);
			if (file_exists($directory) == false) {
				mkdir($directory, 0777, true);
			}
			umask($old);
			$config['upload_path']          = $directory;
			// $config['allowed_types']        = 'gif|jpg|png|jpeg|zip|txt|doc|docx|xls|xlsx|pdf|bmp|rar|ppt|pptx';
			$config['allowed_types']        = '*';
			// $config['max_size']             = 1024 * 50;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$n = 0;
			$attachments = $this->input->post("attachments");
			foreach ($_FILES as $key => $value) {
				if ($this->upload->do_upload($key)) {
					$attachment = $attachments[$n];
					$d = $this->upload->data();
					$this->db->insert('complaint_attachment', [
						'complaint_id' => $id,
						'title' => $attachment['title'],
						'description' => $attachment['description'],
						'path' => sprintf('%s%s', substr($directory, 2), $d['file_name']),
						'filetype' => $d['file_type'],
						'filesize' => $d['file_size'],
						'name' => $d['orig_name']
					]);
					$result[] = [
						"attachment" => $attachment,
						"value" => $value
					];
				}
				$n++;
			}
		}
		return $result;
	}

	private function insert_attributes($id)
	{
		$this->db->where('complaint_id', $id)->delete('complaint_attributes');
		foreach ($this->input->post('attributes') as $key => $value)
			$this->db->insert('complaint_attributes', [
				'complaint_id' => $id,
				'key' => $key,
				'value' => $value
			]);
		return $this->db->where('complaint_id', $id)->get('complaint_attributes')->result();
	}

	private function place_receivers($id, $receivers)
	{
		$this->db->where('complaint_id', $id)->delete('complaint_receiver');
		foreach ($receivers as $uid)
			$this->db->insert('complaint_receiver', [
				'complaint_id' => $id,
				'receiver_id' => $uid
			]);
		return $this->db->where('complaint_id', $id)->get('complaint_receiver')->result();
	}

	public function mine()
	{
		$user_id = $this->input->post('user_id');

		$this->db->select('complaints.*');
		$this->db->select('users.image');
		$this->db->select('users.name, users.username, users.call_name');
		$this->db->select('position.name_position');
		$this->db->join('users', 'users.id_user = complaints.creator_id');
		$this->db->join('position', 'users.id_position = position.id_position');
		$query = $this->input->post('query', false);
		// if($query && strlen($query) < 2) $query = false;
		if ($query) {
			$this->db->like('nama_pasien', $query);
		}

		$data['sent'] = $this->db->where('complaints.creator_id', $this->input->post('user_id'))->where('complaints.status', 'OPEN')->order_by('complaints.updated_at', 'desc')->get('complaints')->result();

		$received_complaints = $this->db->where('receiver_id', $this->input->post('user_id'))
			->group_start()
			->or_where("deleted", null)
			->or_where("deleted", "0")
			->group_end()
			->get('complaint_receiver')->result();

		$data['received'] = []; /*define as inbox on web*/
		if ($received_complaints) {
			$this->db->select('complaints.*');
			$this->db->select('users.image');
			$this->db->select('users.name, users.username, users.call_name');
			$this->db->select('position.name_position');
			$this->db->select('complaint_receiver.read');
			$this->db->select('complaint_receiver.receiver_id');
			$this->db->join('users', 'users.id_user = complaints.creator_id');
			$this->db->join('position', 'users.id_position = position.id_position');
			$this->db->join('complaint_receiver', 'complaint_receiver.complaint_id = complaints.id');
			$this->db->where('complaints.status', 'OPEN');
			$this->db->where('complaints.status <>', 'CLOSED');
			$this->db->where('complaint_receiver.receiver_id', $user_id);
			if ($query) {
				$this->db->like('nama_pasien', $query);
			}
			$this->db->group_by('complaints.id');
			$data['received'] = $this->db->order_by('complaints.updated_at', 'desc')->get('complaints')->result();
			// print_r('<pre>');
			// print_r($this->db->last_query());
			// print_r('</pre>');die;

		}
		$data['archived'] = [];
		if ($received_complaints) {
			// foreach ($received_complaints as $item) $ids[] = $item->complaint_id;
			// foreach ($received_complaints as $item) $ids1[] = $item->receiver_id;
			$this->db->select('complaints.*');
			// $this->db->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
			$this->db->select('users.image');
			$this->db->select('users.name, users.username, users.call_name');
			$this->db->select('complaint_receiver.read');
			$this->db->select('position.name_position');
			$this->db->join('users', 'users.id_user = complaints.creator_id');
			$this->db->join('position', 'users.id_position = position.id_position');
			$this->db->join('complaint_receiver', 'complaint_receiver.complaint_id = complaints.id');
			// $this->db->where_in('complaints.id', $ids);
			// $this->db->where_in('complaint_receiver.receiver_id', $ids1);

			$this->db->where('complaints.status', 'CLOSED');
			$this->db->group_start();
			$this->db->or_where('complaints.creator_id', $user_id);
			$this->db->or_where('complaint_receiver.receiver_id', $user_id);
			$this->db->group_end();
			if ($query) {
				$this->db->like('nama_pasien', $query);
			}
			$this->db->group_by('complaints.id');
			$data['archived'] = $this->db->order_by('complaints.updated_at', 'desc')->get('complaints')->result();
			// print_r('<pre>');
			// print_r($this->db->last_query());
			// print_r('</pre>');die;

		}
		return $data;
	}

	public function send_notification($ids = [0], $title, $message, $data = [])
	{
		$this->load->library("Notify_lib");
		$nl = new Notify_lib();
		$nl->send($title, $message, $this->input->post("user_id"), $ids, $data, "complaint", true);
		// foreach ($ids as $to_id) {
		// 	$this->db->insert('notifications', [
		// 		'from_id' => $this->input->post('user_id'),
		// 		'to_id' => $to_id,
		// 		'description' => $message,
		// 		'data' => json_encode(["complaint_id" => $data['id']]),
		// 		'time' => date('Y-m-d H:i:s')
		// 	]);
		// }
		// $onesignal = $this->db->where_in('user_id', $ids)->get('onesignal_ids')->result();
		// foreach ($onesignal as $key => $value) $player_ids[] = $value->player_id;

		// if(isset($player_ids)){
		// 	$heading = array(
		// 		"en" => $title
		// 	);

		// 	$content = array(
		// 		"en" => $message
		// 	);

		// 	$fields = array(
		// 		'template_id' => '4cf52564-432b-45a6-8abe-e3146c58e821',
		// 		'app_id' => "329f9103-0d3d-4daa-a7b7-c9fc3a0eda42",
		// 		'include_player_ids' => $player_ids,
		// 		'data' => $data,
		// 		'headings' => $heading,
		// 		'contents' => $content
		// 	);

		// 	$fields = json_encode($fields);

		//    	$ch = curl_init();
		// 	curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		// 	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
		// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		// 	curl_setopt($ch, CURLOPT_HEADER, FALSE);
		// 	curl_setopt($ch, CURLOPT_POST, TRUE);
		// 	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		// 	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		// 	$response = curl_exec($ch);
		// 	curl_close($ch);
		// }
	}

	function delete()
	{
		$deleted = $this->db->where('id', $this->input->post('id'))->delete('complaints');
		return ['deleted' => $deleted];
	}

	function getAttributes($id_complaint)
	{
		$q = $this->db->from('complaint_attributes _')
			->where(['_.complaint_id' => $id_complaint])
			->select([
				'_.*'
			]);
		$res = $q->get()->result_array();

		$listKey = [];
		$tempRes = [];
		foreach ($res as $index => $row) {
			$key = $row['key'];
			if (in_array($key, $listKey)) {
				$this->db->where(['id' => $row['id']])->delete('complaint_attributes');
				continue;
			}
			$tempRes[] = $row;
			$listKey[] = $key;
		}
		$res = $tempRes;
		return $res;
	}

	function getActions($id_complaint)
	{
		$q = $this->db->from('complaint_actions _')
			->join('users usr', 'usr.id_user = _.author_id', 'left')
			->where(['_.complaint_id' => $id_complaint])
			->select([
				'_.*',
				'usr.name',
				'usr.username',
				'usr.image'
			]);
		return $q->get()->result_array();
	}

	function baseQueryComplaint($args = [])
	{
		$q = $this->db->from('complaints _')
			->select([
				'_.*'
			]);
		return $q;
	}

	function one_complaint($args)
	{
		$this->load->helper('myquery');
		$c = myquery($this->baseQueryComplaint($args), $args)->get()->row_array();

		if ($c != null) {
			$c['attributes'] = $this->getAttributes($c['id']);
			$c['actions'] = $this->getActions($c['id']);
			$c['receivers'] = $this->getReceivers($c['id']);
		}

		return $c;
	}

	function update($where, $data)
	{
		$target = $this->one_complaint(['where' => $where]);

		if ($target == null) {
			return [
				'status' => false,
				'message' => 'Complaint not found'
			];
		}

		// $attr = isset($data['attributes']) ? $data['attributes'] : [];
		$attr = [];
		if (isset($data['attributes'])) {
			$attr = $data['attributes'];
			unset($data['attributes']);
		}

		$this->db->where(['id' => $target['id']])->update('complaints', $data);

		foreach ($attr as $key => $_attr) {
			$exist = false;
			foreach ($target['attributes'] as $index => $attr2) {
				if ($attr2['key'] == $key) {
					$exist = true;
					$this->db->where(['id' => $attr2['id']])->update('complaint_attributes', [
						'value' => $_attr
					]);
				}
			}
			if ($exist == false) {
				$this->db->insert('complaint_attributes', [
					'key' => $key,
					'value' => $_attr,
					'complaint_id' => $target['id']
				]);
			}
		}

		return [
			'status' => true,
			'message' => 'Complaint updated',
			'data' => $target
		];
	}


	function add_attachment($data)
	{
		$this->db->insert('complaint_attachment', $data);
		$id = $this->db->insert_id();
		return [
			'status' => true,
			'message' => '',
			'id' => $id
		];
	}

	function dateRangeComplaints($startDate, $endDate)
	{
		$this->db->select('users.name, complaints.*, branch.branch_name');
		$this->db->from('complaints');
		$this->db->join('users', 'users.id_user = complaints.creator_id');
		$this->db->join('branch', 'complaints.branch_id = branch.branch_id ');
		$this->db->where('updated_at >=', $startDate);
		$this->db->where('updated_at <=', $endDate);
		return $this->db->get()->result();
	}

	function getComplaintAttributesByIdComplaint($complaint_id)
	{
		$this->db->select('complaint_attributes.key, complaint_attributes.value');
		$this->db->from('complaint_attributes');
		$this->db->where('complaint_id', $complaint_id);
		return $this->db->get()->result();
	}

	function l24($branch_id, $startDate, $endDate)
	{
		$this->db->select('users.name,complaints.nomor_registrasi,complaints.jenis_komplain,complaints.pengaju_komplain,complaints.nama_pasien,complaints.deskripsi,complaints.id,complaints.branch_id as branch_id,complaints.waktu_komplain,complaints.waktu_respon,branch.branch_name as branch');
		$this->db->from('complaints');
		$this->db->join('users', 'users.id_user = complaints.creator_id');
		$this->db->join('branch', 'branch.branch_id = complaints.branch_id');
		$this->db->where('complaints.branch_id =', $branch_id);
		$this->db->where('waktu_komplain >=', $startDate);
		$this->db->where('waktu_komplain <=', $endDate);
		return $this->db->get()->result();
	}
}
