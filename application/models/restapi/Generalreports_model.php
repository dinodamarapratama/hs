<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Generalreports_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function detail($id, $id_reply = null, $id_user = null)
	{
		$user_id = $id_user;

		if (empty($id) && empty($id_reply) == false) {
			$q = $this->db->from('generalreports_reply')->where(['id' => $id_reply])->get()->row();
			if ($q != null) {
				$id = $q->id_generalreports;
			}
		}

		if ($user_id) {
			$this->db->where([
				'id_receiver' => $user_id,
				'id_generalreports' => $id
			])->update('generalreports_receiver', ['is_view' => 1, 'is_view_comment' => 1]);
			// echo "update is view = 1 untuk receiver $user_id dan generalreports $id";
			// exit();
		}
		$generalreports = $this->db->where('id', $id)->get('generalreports')->row();

		if (!empty($generalreports)) {

			/* tandai notifikasi sudah dibaca */
			$this->load->library("Notify_lib");
			$nl = new Notify_lib();
			$nl->mark_read($user_id, "generalreports", $id);
			if ($generalreports->from_id == $user_id) {
				$this->db->where(['id' => $generalreports->id])->update('generalreports', ['is_view' => 1]);
			}

			if (!empty($generalreports->description)) {
				$myCaption1 = strip_tags($generalreports->description);
				$myCaptionEncoded1 = htmlspecialchars_decode($myCaption1);
				// $dailyreport->absensi_des = str_replace(['<p>','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>','<br','</br>','&quot;'], '',$myCaptionEncoded1);
				$generalreports->description = html_entity_decode($generalreports->description);
			}
		} else {
			return ['r' => $generalreports, 'id' => $id];
		}
		$this->load->model('restapi/user_model');
		// $this->db->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
		// $this->db->select('users.image');
		// $this->db->select('position.name_position as position');
		// $this->db->select('users.*, users.id_user as id, departments.name as department');
		// $this->db->join('position', 'position.id_position=users.id_position');
		// $this->db->join('departments', 'position.id_department=position.id_department', 'left');
		// $generalreports->author = $this->db->where('users.id_user', $generalreports->from_id)->get('users')->row();
		$generalreports->author = $this->user_model->one_user(['where' => ['_.id_user' => $generalreports->from_id]]);

		$this->db->select('users.*, users.id_user as id, is_view');
		// $this->db->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
		$this->db->select('users.image');
		$this->db->select('position.name_position as position');
		$this->db->select('departments.name as department');
		$this->db->join('users', 'generalreports_receiver.id_receiver=users.id_user');
		$this->db->join('position', 'position.id_position=users.id_position');
		$this->db->join('departments', 'position.id_department=position.id_department', 'left');
		$this->db->where('generalreports_receiver.id_generalreports', $id);
		$this->db->group_by('generalreports_receiver.id_receiver');
		$generalreports->receivers = $this->db->get('generalreports_receiver')->result();

		// $this->db->select('users.*, users.id_user as id')
		// 			->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false)
		// 			->select('users.image')
		// 			->select('position.name_position as position')
		// 			->select('departments.name as department')
		// 			->select('generalreports_reply.*')
		// 			->join('users', 'generalreports_reply.sender=users.id_user')
		// 			->join('position', 'position.id_position=users.id_position')
		// 			->join('departments', 'position.id_department=position.id_department', 'left')
		// 			->where('generalreports_reply.id_generalreports', $id)
		// 			->group_by('generalreports_reply.date_added')
		// 			->order_by('generalreports_reply.date_added', 'desc');
		// $generalreports->replies = $this->db->get('generalreports_reply')->result();
		$generalreports->replies = $this->getReplies($id);

		if (count($generalreports->replies) > 0) {
			foreach ($generalreports->replies as $key => $value) {
				$myCaption8 = strip_tags($value->message_reply);
				$myCaptionEncoded8 = htmlspecialchars_decode($myCaption8);
				$generalreports->replies[$key]->message_reply = str_replace(['<p>', '</p>', '<strong>', '</strong>', '<em>', '</em>', '<s>', '</s>', '--&gt', '&nbsp;', '<ul>', '</ul>', '<li>', '</li>'], '', $myCaptionEncoded8);
			}
		}
		// file_put_contents("/var/www/html/application/logs/querylog.sql", $this->db->last_query());

		$generalreports->attachments = $this->db->where('general_id', $id)->get('generalreports_attachment')->result();
		return $generalreports;
	}

	function getReplies($id_generalreports)
	{
		$q = $this->db->from('generalreports_reply dr')
			->join('users usr', 'usr.id_user = dr.sender')
			->join('position pos', 'pos.id_position = usr.id_position')
			->join('departments dept', 'dept.id_department = pos.id_department', 'left')
			->order_by('dr.date_added', 'asc')
			->where(['dr.id_generalreports' => $id_generalreports])
			->select([
				'dr.*',
				'usr.image',
				'pos.name_position as position',
				'dept.name as department',
				'usr.name',
				'usr.call_name',
				'usr.username'
			])
			// ->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false)
			->get()->result();
		$ids = [];
		foreach ($q as $key => $row) {
			$ids[] = $row->id;
		}
		$attachments = [];
		if (count($ids) > 0) {
			$attachments
				= $this->db->from('generalreports_reply_attachment')
				->where_in('id_generalreports_reply', $ids)
				->get()->result();
		}

		foreach ($q as $index => $row) {
			if (empty($row->attachments)) {
				$q[$index]->attachments = [];
			}
			foreach ($attachments as $key => $item) {
				if ($item->id_generalreports_reply == $row->id) {
					$q[$index]->attachments[] = $item;
				}
			}
		}
		return $q;
	}

	function hapus_attachment($id, $attachment = null)
	{
		if ($attachment != null) { } else {
			$attachment = $this->db->where(['id' => $id])->get('generalreports_reply_attachment')->row();
		}
		if ($attachment != null) {
			if (file_exists($attachment->path)) {
				unlink($attachment->path);
			}
			$this->db->where(['id' => $attachment->id])->delete('generalreports_reply_attachment');
			return ['status' => true, 'message' => 'Attachment berhasil dihapus'];
		}
		return ['status' => false, 'message' => 'Terjadi kesalahan'];
	}

	function recent_report($id_user)
	{
		$id_user = intval($id_user);

		$type = "General Report";
		$sql = " 	SELECT r.id, '$type Reply' as type_name, r.from_id as sender_id, r.last_update as last_update,
						r.is_view
					from generalreports r
					where r.from_id = '$id_user' and r.is_view = 0 and UPPER(r.status) = 'OPEN'

					union

					Select r.id, '$type' as type_name, r.from_id as sender_id, r.last_update as last_update,
						case when rcv.is_view = 0 or rcv.is_view_comment = 0 then 0 else 1 end as is_view
					from generalreports_receiver rcv
					join generalreports r on rcv.id_generalreports = r.id
					where rcv.id_receiver = '$id_user' and UPPER(r.status) = 'OPEN' ";
		return $sql;
		// $id_user = intval($id_user);
		// $select1 = "SELECT dr.id, 'General Reports' as type_name, dr.from_id as sender_id, dr.last_update as last_update, drc.is_view
		// 			from generalreports dr
		// 			join generalreports_receiver drc on drc.id_generalreports = dr.id
		// 			where drc.id_receiver = '$id_user' and drc.is_view = 0

		// 			union

		// 			Select gr.id, 'General Report' as type_name, gr.from_id as sender_id, gr.date as last_update, grc.is_view
		// 			from generalreports gr
		// 			join generalreports_receiver grc on grc.id_generalreports = gr.id
		// 			where grc.id_receiver = '$id_user' and grc.is_view = 0

		// 			union

		// 			Select rp.id, 'General Reports Reply' as type_name, rp.sender as sender_id, rp.date_added as last_update, case when d.from_id = '$id_user' then d.is_view else drc.is_view_comment end as is_view
		// 			from generalreports_reply rp
		// 			join generalreports d on d.id = rp.id_generalreports
		// 			join generalreports_receiver drc on drc.id_generalreports = d.id
		// 			where (d.from_id = '$id_user' or drc.id_receiver = '$id_user' ) and rp.sender not in ($id_user)";

		// $select2 = "SELECT s1.*, usr.name, usr.username,
		// 			CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar,
		// 			usr.image
		// 			from ($select1) s1
		// 			join users usr on usr.id_user = s1.sender_id
		// 			order by is_view asc, last_update desc limit 3";
		// return $this->db->query($select2)->result();
	}

	function hapus_reply($id, $id_user)
	{
		$reply = $this->db->from('generalreports_reply')->where(['id' => $id])->get()->row();
		if ($reply == null) {
			return ['status' => false, 'message' => "Reply tidak dapat ditemukan"];
		}

		if ($reply->sender != $id_user) {
			return ['status' => false, 'message' => 'Anda tidak berhak menghapus reply dari orang lain'];
		}

		$this->db->where(['id' => $reply->id])->delete('generalreports_reply');

		$files = $this->db->from('generalreports_reply_attachment')->where(['id_generalreports_reply' => $reply->id])->get()->result();
		foreach ($files as $key => $file) {
			$this->hapus_attachment(null, $file);
		}
		// $this->db->where(['id_generalreports_reply' => $reply->id])->delete('generalreports_reply_attachment');
		return ['status' => true, 'message' => 'reply berhasil dihapus'];
	}

	public function save($data = [], $receivers = [], $depts = [], $attachment_prefix = 'file_')
	{
		// $this->db->trans_start();

		// $save['title'] = $me->username;
		$save['id_draft'] = 0;
		$save['is_view'] = 1;
		$isUpdate = false;
		$id = $this->input->post('id');
		$isUpdate = null;
		if ($id){
			$save['id'] = $id;
			$isUpdate = true;
		}
		if (array_key_exists('is_update', $data)) {
			$isUpdate = $data['is_update'];
			if ($isUpdate == true) {
				$id = $data['id'];
			}
		} else {
			$isUpdate = $id != null;
		}

		$bagian = [];
		if (array_key_exists('bagian', $data)) {
			$bagian = $data['bagian'];
		} else {
			$bagian = $this->input->post('bagian');
		}

		if (is_array($bagian) == false) {
			$bagian = [$bagian];
		}
		if (array_key_exists('title', $data)) {
			$save['title'] = $data['title'];
		} else {
			$save['title'] = $this->input->post('subject');
		}

		if (array_key_exists('description', $data)) {
			$save['description'] = $data['description'];
		} else {
			$save['description'] = ($v = $this->input->post('body')) ? $v : '0';
		}

		// $save['dept'] = $me->id_department;
		$save['date'] = date('Y-m-d');
		// $save['dateplanning'] = $this->input->post('date_planning');
		if (array_key_exists('status', $data)) {
			$save['status'] = $data['status'];
		} else {
			$save['status'] = $this->input->post('status');
		}

		if (array_key_exists('kategori', $data)) {
			$save['kategori'] = $data['kategori'];
		} else {
			$save['kategori'] = $this->input->post('category', 'IT');
		}
		$me = null;
		if (array_key_exists('from_id', $data)) {
			$save['from_id'] = $data['from_id'];
		} else {
			// $save['from_id'] = $me->id_user;
			$save['from_id'] = $this->input->post('user_id');
		}

		if (array_key_exists('id_dailyreports', $data)) {
			$save['id_dailyreports'] = $data['id_dailyreports'];
		}

		$this->db->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
		$this->db->select('users.image');
		$this->db->select('users.*, users.id_user as id, position.name_position as position, position.id_department');
		$this->db->join('position', 'position.id_position=users.id_position', 'left');
		$me = $this->db->where('id_user', $save['from_id'])->get('users')->row();

		$save['last_update'] = date('Y-m-d H:i:s');

		// $id = $this->input->post('id');
		if(isset($save['id']) && !empty($save['id']) && ($save['id'] > 0)){
			$id = $save['id'];
			$this->db->where('id', $id)->update('generalreports', $save);
		} else {
			$this->db->insert('generalreports', $save);
			$id = $this->db->insert_id();
		}

		$generalreports = $this->db->where('id', $id)->get('generalreports')->row_array();
		if ($generalreports == null) {
			return ['status' => false, 'message' => 'Data tidak tersimpan', 'report' => $generalreports];
		}
		// $generalreports->attributes = $this->insert_attributes($generalreports->id);
		if (empty($receivers)) {
			$receivers = $this->input->post('receivers');
		}
		if (is_array($receivers) == false) {
			$receivers = [$receivers];
		}

		if (empty($depts)) {
			$depts = $this->input->post('department');
		}

		// print_r($depts);
		if (is_array($depts) == false) {
			$depts = [$depts];
		}
		// print_r($depts);
		// print_r(count($depts));
		$this->load->model('restapi/user_model');
		if (count($depts) > 0) {
			$users = $this->user_model->many_user(['where_in' => ['dep.id_department' => $depts]]);
			// echo 'user in dept:';
			// print_r($users);
			foreach ($users as $key => $user) {
				$receivers[] = $user['id_user'];
			}
		}
		if (count($bagian) > 0) {
			$user_bagian = $this->user_model->many_user(['where_in' => ['bg.id' => $bagian]]);
			foreach ($user_bagian as $key => $usr) {
				$receivers[] = $usr['id_user'];
			}
		}

		$temp = [];
		foreach ($receivers as $key => $id_user) {
			$id_user = trim($id_user);
			if (empty($id_user)) {
				continue;
			}
			if (in_array($id_user, $temp)) {
				continue;
			}
			$temp[] = $id_user;
		}

		$receivers = $temp;
		// if (!$this->input->post('id')) {
			$generalreports['receivers'] = $this->place_receivers($generalreports['id'], $receivers);
		// }
		$generalreports['attachments'] = $this->save_attachment($generalreports['id'], $attachment_prefix);
		// $this->db->trans_complete();

		$notif = [];

		if ($isUpdate == false) {
			/* kirim notif ke penerima hanya jika ada yg no */
			$isNotifyReceiver = $save["description"] == "1";

			if ($isNotifyReceiver == false) {
				// $notif[] = $this->send_notification(
				// 	// $this->input->post('receivers'),
				// 	$receivers,
				// 	'New General Reports Received',
				// 	sprintf('%s has sent a new General Reports', ucwords($me->name)),
				// 	$generalreports
				// );
				$this->load->library('Notify_lib');
				$ntf = new Notify_lib();
				$ntf->send(
					'New General Report',
					$me->name . ' has sent a new General Report',
					$save['from_id'],
					$receivers,
					['id' => $id],
					'generalreports',
					true
				);
			}
		}

		$generalreports["notifications"] = $notif;

		// return $generalreports;
		return ['status' => true, 'message' => 'Report berhasil disimpan', 'report' => $generalreports, 'id' => $id];
	}

	private function save_attachment($id, $attachment_prefix = 'file_')
	{
		$n = [];
		if (!empty($_FILES)) {
			$directory = 'uploads/attachments/generalreports/' . $id . '/';
			$n[] = $directory;
			// $directory = sprintf('./uploads/attachments/generalreports/%s/', sha1(md5($id)));

			if (file_exists($directory) == false) {
				$old = umask(0);
				mkdir($directory, 0777, true);
				umask($old);
			}

			$config = [
				'encrypt_name' => true
			];
			$config['upload_path']          = $directory;
			// $config['allowed_types']        = 'gif|jpg|png|jpeg|zip|txt|doc|docx|xls|xlsx|pdf|bmp|rar|ppt|pptx';
			$config['allowed_types']        = '*';
			// $config['max_size']             = 1024 * 100;  /*  KB */

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			$n[] = $attachments = $this->input->post("attachments");
			$counterAttachment = -1;
			while (true) {
				$counterAttachment++;
				if ($counterAttachment > 100) {
					break;
				}
				// $key = 'file_' . $counterAttachment;
				$key = $attachment_prefix . $counterAttachment;
				if (isset($_FILES[$key]) == false) {
					break;
				}
				$this->upload->initialize($config);
				$title = $this->input->post('file_title_' . $counterAttachment);
				$desc = $this->input->post('file_desc_' . $counterAttachment);

				if ($this->upload->do_upload($key)) {
					$n[] = $uploaded = $this->upload->data();
					$this->db->insert('generalreports_attachment', [
						'general_id' => $id,
						'title' => empty($title) ? $uploaded['orig_name'] : $title,
						'description' => $desc,
						'path' => $directory . $uploaded['file_name'],
						'filesize' => $uploaded['file_size'],
						'filetype' => $uploaded['file_type'],
						'name' => $uploaded['orig_name']
					]);
				} else {
					$n[] = $this->upload->display_errors();
				}
			}
			// foreach ($_FILES as $key => $value) {
			// 	// break;
			// 	if($this->upload->do_upload($key)){
			// 		// $attachment = $this->input->post('attachments')[$n];
			// 		$attachment = $attachments[$counterAttachment++];
			// 		$uploaded = $this->upload->data();
			// 		$this->db->insert('generalreports_attachment', [
			// 			'general_id' => $id,
			// 			'title' => $attachment['title'],
			// 			'description' => $attachment['description'],
			// 			'path' => sprintf('%s%s', substr($directory, 2), $value['name']),
			// 			'filetype' => $value['type'],
			// 			'filesize' => $value['size']
			// 		]);
			// 		$n[] = [
			// 			"attachment" => $attachment,
			// 			"val" => $value,
			// 			"uploaded" => $uploaded
			// 		];
			// 	} else {
			// 		$n[] = $this->upload->display_errors();
			// 	}
			// 	// $n++;
			// }
		}
		return $n;
	}

	private function place_receivers($id, $receivers)
	{
		$this->db->where('id_generalreports', $id)->delete('generalreports_receiver');
		// foreach ($this->input->post('receivers') as $uid)
		// 	$this->db->insert('generalreports_receiver', [
		// 		'id_generalreports' => $id,
		// 		'id_receiver' => $uid
		// 	]);
		foreach ($receivers as $uid) {
			if (empty($uid)) {
				// echo "uid is null $uid\n";
				continue;
			} else {
				// echo "uid not null $uid\n";
			}
			// var_dump($uid);
			$this->db->insert('generalreports_receiver', [
				'id_generalreports' => $id,
				'id_receiver' => $uid
			]);
		}
		return $this->db->where('id_generalreports', $id)->get('generalreports_receiver')->result();
	}

	public function mine($id_user, $query)
	{
		// $this->db->select('generalreports.*,branch.*');
		// $this->db->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
		// $this->db->select('users.image');
		// $this->db->select('users.name, users.username');
		// $this->db->join('users', 'users.id_user = generalreports.from_id');
		// $this->db->join('branch','branch.branch_id = users.branch_id');
		// // $query = $this->input->post('query', false);
		// // if($query && strlen($query) < 2) $query = false;
		// if($query){
		// 	$this->db->like('generalreports.title', $query);
		// }

		$data = [];

		$queryCountReply = $this->db->from('generalreports_reply')
			->select([
				'id_generalreports',
				'count(*) as jumlah_reply'
			])
			->group_by('id_generalreports')
			->get_compiled_select();

		$q = $this->db->from('generalreports d')
			->join("($queryCountReply) creply", 'creply.id_generalreports = d.id', 'left')
			->join('users usr', 'usr.id_user = d.from_id')
			->where('d.from_id', $id_user)
			// ->order_by('d.last_update', 'desc')
			// ->order_by('d.id', 'desc')
			->select([
				'd.*',
				'coalesce(creply.jumlah_reply,0) as jumlah_reply',
				'usr.name',
				'usr.username',
				'usr.call_name',
				'usr.image'
			])
			->select('case when d.is_view = 0 then d.last_update else d.created_at end as last_update2', false)
			->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
		if ($query) {
			$q->like('d.title', $query);
		}

		$sql = $q->get_compiled_select();
		$sql = "SELECT * from ($sql) s1 order by last_update2 desc, id desc";
		$data['sent'] = $this->db->query($sql)->result();
		// $data['sent'] = $q->get()->result();

		$q	= $this->db->from('generalreports_receiver dr')
			->join('generalreports d', 'd.id = dr.id_generalreports')
			->join("($queryCountReply) as creply", 'creply.id_generalreports = d.id', 'left')
			->join('users usr', 'usr.id_user = d.from_id')
			->where('dr.id_receiver', $id_user)
			// ->order_by('dr.is_view', 'asc')
			// ->order_by('d.id', 'desc')
			->select([
				'dr.is_view',
				'd.status',
				'd.title',
				'd.id',
				'd.date',
				'coalesce(creply.jumlah_reply,0) as jumlah_reply',
				'usr.name',
				'usr.username',
				'usr.call_name',
				'usr.image'
			])
			->select('case when dr.is_view = 0 or dr.is_view_comment = 0 then d.last_update else d.created_at end as last_update2', false)
			->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
		if ($query) {
			$q->like('d.title', $query);
		}
		$sql = $q->get_compiled_select();
		$sql = "SELECT * FROM ($sql) s1 order by last_update2 desc, id desc";
		$data['received'] = $this->db->query($sql)->result();
		// $data['received'] = $q->get()->result();
		// if($received_generalreports){
		// foreach ($received_generalreports as $item) $ids[] = $item->id_generalreports;
		// foreach ($received_generalreports as $item) $ids1[] = $item->id_receiver;
		// $this->db->select('generalreports.*,branch.*');
		// $this->db->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
		// $this->db->select('users.image');
		// $this->db->select('users.name, users.username');
		// $this->db->select('generalreports_receiver.is_view');
		// $this->db->join('users', 'users.id_user = generalreports.from_id');
		// $this->db->join('generalreports_receiver', 'generalreports_receiver.id_generalreports = generalreports.id');
		// $this->db->join('branch','branch.branch_id = users.branch_id');
		// $this->db->where_in('generalreports.id', $ids);
		// $this->db->where_in('generalreports_receiver.id_receiver', $ids1);
		// if($query){
		// 	$this->db->like('generalreports.title', $query);
		// }
		// $data['received'] = $this->db->order_by('generalreports.id', 'desc')->get('generalreports')->result();
		// }
		return $data;
	}

	public function send_notification($ids = [0], $title, $message, $data = [])
	{
		$this->load->library("Notify_lib");
		$nl = new Notify_lib();
		return $nl->send($title, $message, $this->input->post("user_id"), $ids, $data, "generalreports", true);
		// foreach ($ids as $to_id) {
		// 	$this->db->insert('notifications', [
		// 		'from_id' => $this->input->post('user_id'),
		// 		'to_id' => $to_id,
		// 		'description' => $message,
		// 		'data' => json_encode(["generalreports_id" => $data['id']]),
		// 		'time' => date('Y-m-d H:i:s')
		// 	]);
		// }
		// $onesignal = $this->db->where_in('user_id', $ids)->get('onesignal_ids')->result();
		// foreach ($onesignal as $key => $value) $player_ids[] = $value->player_id;

		// if(isset($player_ids)){
		// 	$heading = array(
		// 		"en" => $title
		// 	);

		// 	$content = array(
		// 		"en" => $message
		// 	);

		// 	$fields = array(
		// 		'template_id' => '4cf52564-432b-45a6-8abe-e3146c58e821',
		// 		'app_id' => "329f9103-0d3d-4daa-a7b7-c9fc3a0eda42",
		// 		'include_player_ids' => $player_ids,
		// 		'data' => $data,
		// 		'headings' => $heading,
		// 		'contents' => $content
		// 	);

		// 	$fields = json_encode($fields);

		// 	if($this->input->post('body')){
		// 		$ch = curl_init();
		// 		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		// 		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
		// 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		// 		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		// 		curl_setopt($ch, CURLOPT_POST, TRUE);
		// 		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		// 		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		// 		$response = curl_exec($ch);
		// 		curl_close($ch);
		// 	}

		// ob_start();
		//       print_r($response);
		//       file_put_contents("/var/www/html/application/logs/os-generalreports.log", ob_get_clean());
		// }
	}

	function reply($data)
	{
		/*
			data: {
				id: id General Reports,
				user_id: user yang membalas,
				message: isi balasan
			}
		*/
		// $insert = $this->db->insert('generalreports_reply', [
		// 	'id_generalreports' => $this->input->post('id'),
		// 	'sender' => $this->input->post('user_id'),
		// 	'message_reply' => $this->input->post('message'),
		// 	'date_added' => date('Y-m-d H:i:s')
		// ]);
		$reply_id = null;
		$insert = 1;
		$idReply = 0;
		$sendNotif = true;
		if (isset($data['send_notif'])) {
			$sendNotif = $data['send_notif'];
		}
		if (array_key_exists('reply_id', $data)) {
			$reply_id = $data['reply_id'];
			unset($data['reply_id']);
		}
		if ($reply_id != null) {
			$insert = $this->db->where(['id' => $reply_id])
				->update('generalreports_reply', [
					'message_reply' => $data['message']
				]);
			$idReply = $reply_id;
		} else {
			$insert = $this->db->insert('generalreports_reply', [
				'id_generalreports' => $data["id"],
				'sender' => $data['user_id'],
				'message_reply' => $data['message'],
				'date_added' => date('Y-m-d H:i:s')
			]);
			$idReply = $this->db->insert_id();
		}

		$this->db->where(['id' => $data['id']])->update('generalreports', ['last_update' => date('Y-m-d H:i:s')]);
		// $this->db->where(['id_generalreports' => $data['id']])->update('generalreports_receiver', ['is_view_comment' => 0]);

		$generalreports = $this->db->from("generalreports dr")
			->join("users usr", "usr.id_user = dr.from_id", "left")
			->where(["dr.id" => $data["id"]])
			->select([
				"dr.*",
				"usr.name as sender_name"
			])
			->get()->row_array();

		if ($generalreports['from_id'] == $data['user_id']) {
			/* yg me-reply adalah creator report */
			$this->db->where([
				'id_generalreports' => $data['id'],
				'is_closed' => 0
			])
				->update('generalreports_receiver', [
					'is_view_comment' => 0,
				]);
		} else {
			$exc = [$data['user_id']];
			$this->db->where([
				'id' => $data['id']
			])
				->where_in('is_view', [0, 1])
				->update('generalreports', [
					'is_view' => 0
				]);

			$this->db->where([
				'id_generalreports' => $data['id'],
				'is_closed' => 0
			])
				->where_not_in('id_receiver', $exc)
				->update('generalreports_receiver', [
					'is_view_comment' => 0
				]);
		}

		$sender = $this->db->from("users usr")
			->where(["usr.id_user" => $data["user_id"]])
			->get()->row_array();

		$r = false;

		if ($generalreports != null && $sender != null) {
			/* kirim notify ke penerima lain */
			$listReceiver = $this->getReceivers($generalreports["id"]);
			$idReceivers = [];
			/* jika yg membalas generalreports bukan user pembuat, tambahkan pembuat generalreports ke penerima notif */
			if ($sender["id_user"] != $generalreports["from_id"]) {
				$idReceivers[] = $generalreports["from_id"];
			}
			foreach ($listReceiver as $key => $rc) {
				/* jika penerima generalreports adalah yg sedang membalas, skip */
				if ($rc["id_user"] == $data["user_id"]) {
					continue;
				}
				if (in_array($rc['id_user'], $idReceivers) == false) {
					$idReceivers[] = $rc["id_user"];
				}
			}
			if ($sendNotif == true) {
				$this->load->library("Notify_lib");
				$nt = new Notify_lib();
				$notif_data = $generalreports;
				$notif_data['id'] = intval($generalreports['id']);
				$r = $nt->send(
					"General Reports Reply", 										//title
					// $generalreports["sender_name"] . " has send a new reply", 	// msg
					$sender["name"] . " has send a new reply", 						// msg
					$sender["id_user"],  											//sender
					$idReceivers,       											//receiver
					// $generalreports,												//payload
					$notif_data,													//payload
					"generalreports",												//tipe notif
					true
				);
			} else {
				$r = 'send notif is disabled';
			}
		} else {
			$r = "";
			if ($generalreports == null) {
				$r = "General Reports is null. ";
			}
			if ($sender == null) {
				$r .= "sender is null. ";
			}
		}

		return ["insert" => $insert, "notif" => $r, 'status' => true, 'id' => $idReply, 'message' => 'Reply berhasil disimpan'];
	}

	function save_reply_attachment($data)
	{
		$this->db->insert('generalreports_reply_attachment', $data);
		return ['status' => true, 'message' => 'success', 'id' => $this->db->insert_id()];
	}

	function delete()
	{
		$deleted = $this->db->where('id', $this->input->post('id'))->delete('generalreports');
		return ['deleted' => $deleted];
	}

	function getReceivers($id_generalreports)
	{
		$q = $this->db->from("generalreports_receiver dc")
			->join("users usr", "usr.id_user = dc.id_receiver", "left")
			->where([
				"dc.id_generalreports" => $id_generalreports
			])
			->select([
				"dc.is_view",
				'dc.is_view_comment',
				'usr.name',
				'usr.username',
				'usr.call_name',
				'usr.image',
				'usr.id_user'
			])
			->get()->result_array();

		return $q;
	}

	function baseQuery($args = [])
	{
		if (isset($args['join_receiver']) == false) {
			$args['join_receiver'] = false;
		}
		if (isset($args['join_reply']) == false) {
			$args['join_reply'] = true;
		}

		$qReply = '';
		if ($args['join_reply'] == true) {
			$q = $this->db->from('generalreports_reply _')
				->select('count(*) as jumlah', false)
				->select('GROUP_CONCAT(_.message_reply) as reply_all', false)
				->select([
					'id_generalreports'
				])
				->group_by('id_generalreports');
			$qReply = $q->get_compiled_select();
		}
		$r = $this->db->from("generalreports _")
			->join('users usr', 'usr.id_user = _.from_id', 'left')
			->select([
				'_.id',
				'_.title',
				'_.date',
				'_.is_view',
				'_.from_id',
				'usr.name',
				'usr.username',
				'usr.call_name',
				'usr.image'
			]);
		if ($args['join_reply'] == true) {
			$r->join("($qReply) rply", 'rply.id_generalreports = _.id', 'left')
				->select([
					'coalesce(rply.jumlah, 0) as jumlah_reply'
				]);
		}

		if ($args['join_receiver'] == true) {
			$r->join('generalreports_receiver rcv', 'rcv.id_generalreports = _.id', 'left')
				->select('case when (rcv.is_view = 0 or rcv.is_view_comment = 0) then 0 else 1 end as is_view_rcv', false);
		}

		/*by default join in on branch & position */
		$r 	->join('branch br', 'br.branch_id = usr.branch_id', 'left')
			->join('position pos', 'pos.id_position = usr.id_position', 'left')
			->select([
					'br.branch_name','pos.id_position','pos.name_position'
				]);
				
		return $r;
	}

	function many($args = [])
	{
		$this->load->helper('myquery');
		return myquery($this->baseQuery($args), $args)->get()->result();
	}

	function one($args = [])
	{
		$this->load->helper('myquery');
		return myquery($this->baseQuery($args), $args)->get()->row();
	}

	function update($where, $data)
	{
		$targets = $this->many(['where' => $where]);
		if (count($targets) == 0) {
			return ['status' => false, 'message' => 'Data not found'];
		}
		foreach ($targets as $key => $target) {
			$this->db->where(['id' => $target->id])->update('generalreports', $data);
		}
		return ['status' => true, 'message' => 'success'];
	}

	function queryReceived($args)
	{
		$this->load->helper('myquery');
		$args['join_receiver'] = true;
		$q = myquery($this->baseQuery($args), $args);
		$q->select('case when (rcv.is_view = 0 or rcv.is_view_comment = 0) then _.last_update else _.created_at end as last_update2', false);
		// return $q->get()->result();
		$sql = $q->get_compiled_select();
		return $sql;
	}

	function querySent($args)
	{
		$this->load->helper('myquery');
		$args['join_receiver'] = false;
		$q = myquery($this->baseQuery($args), $args);
		// return $q->get()->result();
		$q->select('0 as is_view_rcv')
			->select('case when _.is_view = 0 then _.last_update else _.created_at end as last_update2', false);
		$sql = $q->get_compiled_select();
		return $sql;
	}

	function queryArchived($id_user, $args)
	{
		$argsReceived = $args;
		$argsReceived['where']['rcv.id_receiver'] = $id_user;

		$argsSent = $args;
		$argsSent['where']['_.from_id'] = $id_user;

		$sqlReceived = $this->queryReceived($argsReceived);
		$sqlSent = $this->querySent($argsSent);

		$sql = "$sqlReceived  \n\n\nunion  \n\n\n$sqlSent";

		return $sql;
	}

	function getReceived($id_user, $args)
	{
		$args['where']['rcv.id_receiver'] = $id_user;
		$args['where']['upper(_.status)'] = 'OPEN';
		$args['join_receiver'] = true;
		$limit = [];
		if (isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		$sql = $this->queryReceived($args);
		$q = myquery($this->db->from("($sql) s1"), ['limit' => $limit, 'order' => [['last_update2', 'desc'], ['id', 'desc']]]);
		return $q->get()->result();
		// echo $q->get_compiled_select();
	}

	function getSent($id_user, $args)
	{
		$args['where']['_.from_id'] = $id_user;
		$args['where']['upper(_.status)'] = 'OPEN';
		$limit = [];
		if (isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}

		$sql = $this->querySent($args);
		$q = myquery($this->db->from("($sql) s1"), ['limit' => $limit, 'order' => [['last_update2', 'desc'], ['id', 'desc']]]);
		return $q->get()->result();
		// echo $q->get_compiled_select();
	}

	function getArchived($id_user, $args)
	{
		$id_user = intval($id_user);
		$args['where']['upper(_.status)'] = 'CLOSE';
		$args['join_receiver'] = true;
		$limit = [];
		if (isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}

		$sql = $this->queryArchived($id_user, $args);
		$q = myquery($this->db->from("($sql) s1"), ['limit' => $limit, 'order' => [['last_update2', 'desc'], ['id', 'desc']]]);
		$sql = $q->get_compiled_select();

		// echo $sql . "\n\n\n";
		return $this->db->query($sql)->result();
	}

	function getCountReceived($id_user, $args)
	{
		$args['where']['rcv.id_receiver'] = $id_user;
		$args['where']['upper(_.status)'] = 'OPEN';
		$q = $this->queryReceived($args);
		$sql = "select count(*) as jumlah from ($q) s1";
		return $this->db->query($sql)->row();
	}

	function getCountSent($id_user, $args)
	{
		$args['where']['_.from_id'] = $id_user;
		$args['where']['upper(_.status)'] = 'OPEN';
		$q = $this->querySent($args);
		$sql = "select count(*) as jumlah from ($q) s1";
		return $this->db->query($sql)->row();
	}

	function getCountArchived($id_user, $args)
	{
		$args['where']['upper(_.status)'] = 'CLOSE';
		$args['join_receiver'] = true;
		$sql = $this->queryArchived($id_user, $args);
		$sql = "select count(*) as jumlah from ($sql) s1";
		return $this->db->query($sql)->row();
	}

	function mark_read($id, $id_user, $report = null)
	{
		if ($report == null) {
			$report = $this->one(['where' => ['_.id' => $id]]);
		}
		if ($report == null) {
			return ['status' => false, 'message' => 'Report tidak ditemukan'];
		}

		if ($report->from_id == $id_user) {
			$this->db->where(['id' => $report->id])->where_in('is_view', [0, 1])->update('generalreports', ['is_view' => 1]);
		}

		$this->db->where(['id_generalreports' => $report->id, 'id_receiver' => $id_user, 'is_closed' => 0])->update('generalreports_receiver', ['is_view' => 1, 'is_view_comment' => 1]);

		return ['status' => true, 'message' => 'Report ditandai telah dibaca'];
	}
	public function find($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('generalreports')->row_object();
	}
}
