<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Memento admin_model model
 *
 * This class handles admin_model management related functionality
 *
 * @package		Admin
 * @subpackage	admin_model
 * @author		propertyjar
 * @link		#
 */

class notification_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get($unread = false, $limit=100, $offset=0)
	{
		$this->db->select('notifications.*');
		// $this->db->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
		$this->db->select('users.name, users.username, users.call_name, users.image,users.last_login, branch.branch_name, position.name_position');
		$this->db->join('users', 'users.id_user=notifications.from_id');
		$this->db->join('branch', 'branch.branch_id = users.branch_id', 'left');
		$this->db->join('position', 'position.id_position = users.id_position', 'left');
		$this->db->limit($limit, $offset);
		$this->db->where('cuti_flag',0);
		if($unread){
			$this->db->group_start();
			$this->db->where('read',null);
			$this->db->or_where("read","0");
			$this->db->group_end();
		}
		$notifications = $this->db->where('to_id', $this->input->post('user_id'))->order_by('notifications.id', 'desc')->get('notifications')->result();
		return $notifications;
	}

	function getCuti($unread = false, $limit=100, $offset=0){
		$this->db->select('notifications.*');
		// $this->db->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
		$this->db->select('users.name, users.username, users.call_name, users.image, branch.branch_name, position.name_position');
		$this->db->from('notifications');
		$this->db->join('users', 'users.id_user=notifications.from_id');
		$this->db->join('branch', 'branch.branch_id = users.branch_id', 'left');
		$this->db->join('position', 'position.id_position = users.id_position', 'left');
		$this->db->limit($limit, $offset);
		if($unread){
			// $this->db->group_start();
			// $this->db->where('read',null);
			$this->db->or_where('read','0');
			// $this->db->group_end();
		}
		$notifications = $this->db->where('to_id', $this->input->post('user_id'))
							->where('cuti_flag',1)
							->order_by('notifications.id', 'desc')
							->get()
							->result();
		return $notifications;
	}

	function read()
	{
		return $this->db->where('id', $this->input->post('id'))->update('notifications', [
			'read' => 1
		]);
	}


	function baseQueryNotif($args = []) {
		$r = $this->db->from('notifications _')
			->select([
				'_.*'
			]);
		return $r;
	}

	function many_notif($args = []) {
		$this->load->helper('myquery');
		return myquery($this->baseQueryNotif($args), $args)->get()->result();
	}

	function one_notif($args = []) {
		$this->load->helper('myquery');
		return myquery($this->baseQueryNotif($args), $args)->get()->row();
	}

	function mark_read($id_notif) {
		$notif = $this->one_notif(['where' => ['_.id' => $id_notif]]);

		if($notif == null) {
			return ['status' => false, 'message' => 'notif tidak dapat ditemukan'];
		}

		$notif_data = $notif->data;

		$notif_data = json_decode($notif_data);

		$this->load->library('Notify_lib');
		$nl = new Notify_lib();
		$notifType = '';
		if(empty($notif_data->notif_type) == false) {
			$notifType = $notif_data->notif_type;
		}
		$idReport = '';
		if(empty($notif_data->id) == false) {
			$idReport = $notif_data->id;
		}
		$nl->mark_read($notif->to_id, $notifType, $idReport);

		return [
			'status' => true,
			'message' => 'Notif telah dibaca',
			// 'n' => $notif,
			// 'd' => $notif_data
		];
	}

	function mark_all_read($to_id) {
				$this->load->library('Notify_lib');
		$nl = new Notify_lib();
		$nl->mark_all_read($to_id);

		return [
			'status' => true,
			'message' => 'Notif telah dibaca',
			// 'n' => $notif,
			// 'd' => $notif_data
		];
	}

}
