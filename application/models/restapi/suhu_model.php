<?php

class suhu_model extends CI_Model{

    public function getData($args){
        $sql = $this->db->select(['_.*'])
            ->from('suhu AS _')
            ->where($args['where']);
        if (isset($args['search'])):
            $sql->like('_.user_name', $args['search']);
            $sql->or_like('_.user_id', $args['search']);
            $sql->or_like('_.recog_time', $args['search']);
            $sql->or_like('_.body_temperature', $args['search']);
        endif;
        if (isset($args['start'])) :
            if ($args['start'] == 0) :
                $sql->limit($args['length']);
            else :
                $sql->limit($args['length'], $args['start']);
            endif;
        endif;
        $sql->order_by('_.id', 'desc');
        return $sql->get()->result();
    }

    public function detail($id){
        $this->db->select('*')
            ->from('suhu')
            ->where('id', $id);
        return $this->db->get()->row();
    }
}
