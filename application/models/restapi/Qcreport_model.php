<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Qcreport_model extends CI_Model
{
	public $modelDetail4 = 'qcreports_alat';

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function detail($id, $id_reply = null, $id_user = null)
	{
		$user_id = $id_user;

		if(empty($id) && empty($id_reply)==false) {
			$q = $this->db->from('qcreports_reply')->where(['id' => $id_reply])->get()->row();
			if($q != null) {
				$id = $q->id_qcreports;
			}
		}
		if ($id_user != null ) {
			$this->readReceiver($id, $id_user );

		}
		$qcreport = $this->db->where('id', $id)->get('qcreports')->row();
		if(empty($qcreport)) {
			return null;
		}
		$this->load->model(['restapi/generalreports_model', 'm_absensi']);

		if($user_id){
			$this->db->where([
				'id_receiver' => $user_id,
				'id_qcreports' => $id,
				'is_closed' => 0
			])->update('qcreports_receiver', ['is_view' => 1, 'is_view_comment' => 1]);
		}

		if(empty($qcreport)) {
			return [];
		}

		$all_normal = $qcreport->reagen_des == '1' && $qcreport->kontrol_des == '2' && $qcreport->kalibrasi_des == '3'
			&& $qcreport->qc_des == '4';


		if($all_normal == true) {
			$receiversNotRead = $this->db->from('qcreports_receiver')
				->group_start()
					->or_where([
						'is_view' => 0
					])
				->group_end()
				->where(['id_qcreports' => $qcreport->id])
				->get()->result();





			$counter = 0;
			foreach($receiversNotRead as $r) {
				$counter++;
				break;
			}


			if($counter == 0) {
				$this->db->where(['id' => $qcreport->id])->update('qcreports', ['status' => 'Close']);

			}
		}

		if(!empty($qcreport)){


			$this->load->library("Notify_lib");
        	$nl = new Notify_lib();
        	$nl->mark_read($user_id, "qcreport", $id);
        	if($qcreport->from_id == $user_id) {
        		$this->db->where(['id' => $qcreport->id])->update('qcreports', ['is_view' => 1]);
        	}
		   if(!empty($qcreport->reagen_des)){
				$myCaption1 = strip_tags($qcreport->reagen_des);
				$myCaptionEncoded1 = htmlspecialchars_decode($myCaption1);

				$qcreport->reagen_des = html_entity_decode($qcreport->reagen_des);
			}
			if(!empty($qcreport->kontrol_des)){
				$myCaption2 = strip_tags($qcreport->kontrol_des);
				$myCaptionEncoded2 = htmlspecialchars_decode($myCaption2);

				$qcreport->kontrol_des = html_entity_decode($qcreport->kontrol_des);
			}
			if(!empty($qcreport->kalibrasi_des)){
				$myCaption3 = strip_tags($qcreport->kalibrasi_des);
				$myCaptionEncoded3 = htmlspecialchars_decode($myCaption3);

				$qcreport->kalibrasi_des = html_entity_decode($qcreport->kalibrasi_des);
			}
			if(!empty($qcreport->qc_des)){
				$myCaption4 = strip_tags($qcreport->qc_des);
				$myCaptionEncoded4 = htmlspecialchars_decode($myCaption4);

				$qcreport->qc_des = html_entity_decode($qcreport->qc_des);
			}

			if(empty($qcreport->id_des) == false) {
				$qcreport->id_des = html_entity_decode($qcreport->id_des);
			}

		}

		$this->load->model('restapi/user_model');
		$qcreport->author = $this->user_model->one_user(['where' => ['_.id_user' => $qcreport->from_id]]);
		$qcreport->replies = $this->getReplies($id);
		$qcreport->receivers = $this->getReceivers($id);
		$qcreport->alat = $this->getAlat($id);

		if (count($qcreport->replies) > 0){
			foreach ($qcreport->replies as $key => $value) {
				$myCaption8 = strip_tags($value->message_reply);
				$myCaptionEncoded8 = htmlspecialchars_decode($myCaption8);
				$qcreport->replies[$key]->message_reply = str_replace(['<p>','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>'], '',$myCaptionEncoded8);
			}
		}


		$qcreport->attachments = $this->db->where('qcreport_id', $id)->get('qcreports_attachment')->result();

		if($qcreport != null) {
            /*if($qcreport->reagen_des == '1')     { $qcreport->reagen_des = 'Lancar'; }
            if($qcreport->kontrol_des == '2')         { $qcreport->kontrol_des = 'Lancar'; }
            if($qcreport->kalibrasi_des == '3')    { $qcreport->kalibrasi_des = 'Lancar'; }
            if($qcreport->qc_des == '4')          { $qcreport->qc_des = 'Lancar'; }*/
			
			if(is_numeric($qcreport->reagen_des))     { $qcreport->reagen_des = 'Lancar'; }
            if(is_numeric($qcreport->kontrol_des))         { $qcreport->kontrol_des = 'Lancar'; }
            if(is_numeric($qcreport->kalibrasi_des))    { $qcreport->kalibrasi_des = 'Lancar'; }
            if(is_numeric($qcreport->qc_des))          { $qcreport->qc_des = 'Lancar'; }

            $qcreport->show_daily = true;
        }
		return $qcreport;
	}

	function getReplies($id_qcreport) {
		$q = $this->db->from('qcreports_reply dr')
			->join('users usr', 'usr.id_user = dr.sender')
			->join('position pos', 'pos.id_position = usr.id_position')
			->join('departments dept', 'dept.id_department = pos.id_department', 'left')
			->order_by('dr.date_added', 'asc')
			->where(['dr.id_qcreports' => $id_qcreport])
			->select([
				'dr.*',
				'usr.image',
				'pos.name_position as position',
				'dept.name as department',
				'usr.name',
				'usr.call_name',
				'usr.username'
			])
			->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false)
			->get()->result();
		$ids = [];
		foreach ($q as $key => $row) {
			$ids[] = $row->id;
		}
		$attachments = [];
		if(count($ids) > 0){
			$attachments
				= $this->db->from('qcreport_reply_attachment')
				->where_in('id_qcreport_reply', $ids)
				->get()->result();
		}

		foreach ($q as $index => $row) {
			if(empty($row->attachments)) {
				$q[$index]->attachments = [];
			}
			foreach ($attachments as $key => $item) {
				if($item->id_qcreport_reply == $row->id) {
					$q[$index]->attachments[] = $item;
				}
			}
		}
		return $q;
	}

	function hapus_attachment($id, $attachment = null) {
		if($attachment != null){

		} else {
			$attachment = $this->db->where(['id' => $id])->get('qcreport_reply_attachment')->row();
		}
		if($attachment != null){
			if(file_exists($attachment->path)) {
				unlink($attachment->path);
			}
			$this->db->where(['id' => $attachment->id])->delete('qcreport_reply_attachment');
			return ['status' => true, 'message' => 'Attachment berhasil dihapus'];
		}
		return ['status' => false, 'message' => 'Terjadi kesalahan'];
	}

	function recent_report($id_user) {
		$id_user = intval($id_user);
		$type = "Daily Report";
		$sql = " 	SELECT r.id, '$type Reply' as type_name, r.from_id as sender_id, r.last_update as last_update,
						r.is_view
					from qcreports r
					where r.from_id = '$id_user' and r.is_view = 0 and UPPER(r.status) = 'OPEN'

					union

					Select r.id, '$type' as type_name, r.from_id as sender_id, r.last_update as last_update,
						case when rcv.is_view = 0 or rcv.is_view_comment = 0 then 0 else 1 end as is_view
					from qcreports_receiver rcv
					join qcreports r on rcv.id_qcreports = r.id
					where rcv.id_receiver = '$id_user' and UPPER(r.status) = 'OPEN' ";
		return $sql;
	}

	function hapus_reply($id, $id_user){
		$reply = $this->db->from('qcreports_reply')->where(['id' => $id])->get()->row();
		if($reply == null){
			return ['status' => false, 'message' => "Reply tidak dapat ditemukan"];
		}

		if($reply->sender != $id_user) {
			return ['status' => false, 'message' => 'Anda tidak berhak menghapus reply dari orang lain'];
		}

		$this->db->where(['id' => $reply->id])->delete('qcreports_reply');

		$files = $this->db->from('qcreport_reply_attachment')->where(['id_qcreport_reply' => $reply->id])->get()->result();
		foreach ($files as $key => $file) {
			$this->hapus_attachment(null, $file);
		}
		return ['status' => true, 'message' => 'reply berhasil dihapus'];
	}

	public function save($data = [], $receivers = [], $dept = [], $attachment_prefix = 'file_', $attach_mode = 'seq')
	{
		$this->load->model('restapi/user_model');
		$this->db->trans_start();
		$save = [
			'is_it' => 0,
			'is_absensi' => 0
		];
		$save['id_draft'] = 0;
		$save['is_view'] = 1;
		$save['mengetahui'] = '';
		$save['dateplanning'] = date('Y-m-d H:i:s');

		if(array_key_exists('title', $data)){
			$save['title'] = $data['title'];
		}else{
			$save['title'] = $this->input->post('subject');
		}



		$bagian = [];
		if(array_key_exists('bagian', $data)) {
			$bagian = $data['bagian'];
			unset($data['bagian']);
		}
		else {
			$bagian = $this->input->post('bagian');
		}

		if(is_array($bagian) == false) {
			$bagian = [$bagian];
		}
		$user_bagian = [];
		if(count($bagian) > 0) {
			$users = $this->user_model->many_user(['where_in' => ['bg.id' => $bagian]]);

			foreach ($users as $key => $usr) {
				$user_bagian[] = $usr['id_user'];
			}
		}

		if(array_key_exists('reagen_des', $data)) {
			$save['reagen_des'] = empty($r = $data['reagen_des']) ? '1' : $r;
		}else{
			$save['reagen_des'] = $this->input->post('body1') ? $this->input->post('body1') : '1';
		}
		$absensi = $save['reagen_des'];
		if(empty($absensi) == false && $absensi != '1') {
			$save['is_absensi'] = 1;
		}

		if(array_key_exists('kontrol_des', $data)){
			$save['kontrol_des'] = empty($c = $data['kontrol_des']) ? '2' : $c;
		}else{
			$save['kontrol_des'] = $this->input->post('body2') ? $this->input->post('body2') : '2';
		}

		if(array_key_exists('kalibrasi_des', $data)){
			$save['kalibrasi_des'] = empty($c = $data['kalibrasi_des']) ? '3' : $c;
		}else{
			$save['kalibrasi_des'] = $this->input->post('body3') ? $this->input->post('body3') : '3';
		}

		if(array_key_exists('qc_des', $data)){
			$save['qc_des'] = empty($c = $data['qc_des']) ? '4' : $c;
		}else{
			$save['qc_des'] = $this->input->post('body4') ? $this->input->post('body4') : '4';
		}

		if(array_key_exists('date', $data)) {
			$save['date'] = $data['date'];
		} else {
			$save['date'] = date('Y-m-d');
		}

		if(array_key_exists('id_alat', $data)) {
			$save['id_alat'] = $data['id_alat'];
		} else {
			$save['id_alat'] = 0;
		}

		if(array_key_exists('status', $data)){
			$save['status'] = $data['status'];
		}else {
			$save['status'] = $this->input->post('status');
		}

		if(array_key_exists('kategori', $data)){
			$save['kategori'] = $data['kategori'];
		}else{
			$save['kategori'] = $this->input->post('category', 'Follow Up');
		}

		if(array_key_exists('from_id', $data)){
			$save['from_id'] = $data['from_id'];
		}else{
			return ['status' => false, 'message' => 'Sender is not valid'];
		}

		$sender = $this->user_model->one_user(['where' => ['_.id_user' => $save['from_id']]]);

		$save['last_update'] = date('Y-m-d H:i:s');

		if(empty($receivers)){
			if(array_key_exists('receivers', $data)) {
				$receivers = $data['receivers'];
				unset($data['receivers']);
			}
			else {
				$receivers = $this->input->post('receivers');
			}
		}
		if(is_array($receivers) == false) {
			$receivers = [ $receivers ];
		}

		$branch = [];
		if(array_key_exists('branch_id', $data)) {
			$branch = $data['branch_id'];
			$save['branch_id'] = $data['branch_id'];
			unset($data['branch_id']);
		}
		else {
			$branch = $this->input->post('branch_id');
			$save['branch_id']  = $this->input->post('branch_id');
		}

        $pos = array('4', '13','1');//AREA MANAGER,PENANGGUNG JAWAB(dokter pj area),direksi
        if(is_array($pos) == false) {
            $pos = [ $pos ];
        }

        $this->load->model('restapi/user_model');
        if(empty($pos) == false) {
            $users = $this->user_model->many_user(['where_in' => ['pos.id_position' => $pos]]);
            foreach ($users as $key => $usr) {
				if (strpos($usr['many_branch'], "'".$branch."'") !== false) {
                	$receivers[] = $usr['id_user'];
				}
            }
		}

		$pos2 = array('21', '7', '5');//DIREKTUR utama,BRANCH MANAGER,ASS. BRANCH MANAGER
		if(empty($pos2) == false) {
            $users = $this->user_model->many_user(['where_in' => ['_.id_position' => $pos2],'where' => ['_.branch_id' => $branch]]);
            foreach ($users as $key => $usr) {
				$receivers[] = $usr['id_user'];
            }
        }

		$pos3 = array('9');//GM OPT  
        if(empty($pos3) == false) {
            $users = $this->user_model->many_user(['where_in' => ['_.id_position' => $pos3],
			'where' => ['_.id_bagian' => 27]]);
			//,'_.branch_id' => $branch_id
            foreach ($users as $key => $usr) {
				$receivers[] = $usr['id_user'];
			}
		}


		if(empty($dept)) {
			$dept = $this->input->post('department');
		}

		if(is_array($dept) == false) {
			$dept = [ $dept ];
		}
		if(empty($dept) == false) {
			$users = $this->user_model->many_user(['where_in' => ['dep.id_department' => $dept] ]);
			foreach ($users as $key => $user) {
				$userDept[] = $user['id_user'];
				$receivers[] = $user['id_user'];
			}
		}
		foreach ($user_bagian as $key => $id_user) {
			$receivers[] = $id_user;
		}

		$alat = $this->input->post('alat');
        if(is_array($alat) == false) {
            $alat = [ $alat ];
        }



		$temp = [];
		foreach ($receivers as $key => $id_user) {
			if(in_array($id_user, $temp)) {
				continue;
			}
			if($id_user == $save['from_id']) {
				continue;
			}
			$temp[] = $id_user;
		}
		$receivers = $temp;

		if(count($receivers) == 0) {
			return ['status' => false, 'message' => 'Receiver is empty'];
		}
		$id = $data['id'];
		if($data['is_edit'] == true){
			$this->db->where('id', $data['id'])->update('qcreports', $save);

				if (!empty($alat)) {

					$data_alat = ['status' => 0 ];
					$status = $this->m_mis->update_data(array(
						'qcreport_id' => $id
					),$data_alat, $this->modelDetail4);

					foreach ($alat as $key => $value) {
						$cek = $this->m_mis->edit_data(array(
							'qcreport_id' => $id,
							'id_alat' => $value
						),$this->modelDetail4)->result_array();

						$data_alat = ['qcreport_id' => $id, 'id_alat' => $value, 'status' => 1 ];

						if(isset($cek[0]->id))
						{
							$status = $this->m_mis->update_data(array(
								'qcreport_id' => $id,
								'id_alat' => $value
							),$data_alat, $this->modelDetail4);
						}else{
							$status = $this->m_mis->insert_data($data_alat, $this->modelDetail4);
						}
					}
				}

		} else {
			$this->db->insert('qcreports', $save);
			$id = $this->db->insert_id();
				if (!empty($alat)) {

					$data_alat = ['status' => 0 ];
					$status = $this->m_mis->update_data(array(
						'qcreport_id' => $id
					),$data_alat, $this->modelDetail4);

					foreach ($alat as $key => $value) {
						$cek = $this->m_mis->edit_data(array(
							'qcreport_id' => $id,
							'id_alat' => $value
						),$this->modelDetail4)->result_array();

						$data_alat = ['qcreport_id' => $id, 'id_alat' => $value, 'status' => 1 ];

						if(isset($cek[0]->id))
						{
							$status = $this->m_mis->update_data(array(
								'qcreport_id' => $id,
								'id_alat' => $value
							),$data_alat, $this->modelDetail4);
						}else{
							$status = $this->m_mis->insert_data($data_alat, $this->modelDetail4);
						}
					}
				}
		}

		$qcreport = $this->db->where('id', $id)->get('qcreports')->row_array();
		if($qcreport == null) {
			return ['status' => false, 'message' => 'Report tidak tersimpan', 'report' => $qcreport];
		}

		if(!$this->input->post('id')) {
			$qcreport['receivers'] = $this->place_receivers($qcreport['id'], $receivers, $qcreport);
		}
		$qcreport['attachments'] = $this->save_attachment($qcreport['id'], $qcreport['from_id'], $attachment_prefix, $attach_mode);
		$this->db->trans_complete();

		$notif = [];

		if($data['is_edit'] == false){
			$this->load->library('Notify_lib');
			$ntf = new Notify_lib();
				$rcv = [];

				$ntf->send(
					'New Qc Report',
					$sender['name'] . ' has sent new Qc Report',
					$sender['id_user'],
					$rcv,
					['id' => $qcreport['id']],
					'qcreport',
					true
				);
		}

		$qcreport["notifications"] = $notif;
		return ['status' => true, 'message' => 'Reprot berhasil disimpan', 'report' => $qcreport];
	}

	private function save_attachment($id, $creator_id, $prefix = 'file_', $mode = 'seq')
	{

		$n = [];
		$config = [
			'upload_path' => './uploads/attachments/qcreport/' . $id . '/',
			'encrypt_name' => true,
			'allowed_types' => '*'
		];
		$directory = $config['upload_path'];
		$old = umask(0);
		if(file_exists($directory) == false) {
			mkdir($directory, 0777, true);
		}
		umask($old);
		if($mode == 'array'){
			if(!empty($_FILES)){
		        $this->load->library('upload', $config);
		        $this->upload->initialize($config);
		        $counterAttachment = 0;
		        foreach ($_FILES as $key => $value) {
		        	if($this->upload->do_upload($key)){
		        		$uploaded = $this->upload->data();
		        		$this->db->insert('qcreports_attachment', [
		        			'qcreport_id' => $id,
		        			'path' => sprintf('%s%s', substr($directory, 2), $value['name']),
		        			'filetype' => $value['type'],
		        			'filesize' => $value['size'],
		        			'creator_id' => $creator_id
		        		]);
		        		$n[] = [
		        			"attachment" => $attachment,
		        			"val" => $value,
		        			"uploaded" => $uploaded
		        		];
		        	} else {
		        		$n[] = $this->upload->display_errors();
		        	}
		        }
			}
		}

		else if($mode == 'seq') {
			$counter = -1;
			while(true) {
				$counter++;
				if($counter > 200) {
					break;
				}
				$file_key = $prefix . $counter;
				$title_key = $prefix . 'title_' . $counter;
				$desc_key = $prefix . 'desc_' . $counter;

				if(isset($_FILES[$file_key]) == false) {
					break;
				}

				$this->upload->initialize($config);
				if($this->upload->do_upload($file_key)) {
					$uploaded = $this->upload->data();
					$title = $this->input->get_post($title_key);
					$desc = $this->input->get_post($desc_key);

					$d = [
						'qcreport_id' => $id,
						'title' => $title,
						'description' => $desc,
						'path' => $config['upload_path'] . $uploaded['file_name'],
						'filetype' => $uploaded['file_type'],
						'filesize' => $uploaded['file_size'],
						'name' => $uploaded['orig_name'],
						'creator_id' => $creator_id
					];

					$this->db->insert('qcreports_attachment', $d);

					$n[] = $d;
				}
				else {
					$n[] = $this->upload->display_errors();
				}
			}
		}

		return $n;
	}

	private function place_receivers($id, $receivers, $report = null)
	{
		$this->db->where('id_qcreports', $id)->delete('qcreports_receiver');
		foreach ($receivers as $uid) {
			if(empty($uid)) {
				continue;
			}
			if($report != null && $report['from_id'] == $uid) {
				continue;
			}
			$this->db->insert('qcreports_receiver', [
				'id_qcreports' => $id,
				'id_receiver' => $uid
			]);
		}
		return $this->db->where('id_qcreports', $id)->get('qcreports_receiver')->result();
	}

	public function mine($id_user, $query)
	{

		$data = [];

		$queryCountReply = $this->db->from('qcreports_reply')
							->select([
								'id_qcreports',
								'count(*) as jumlah_reply'
							])
							->group_by('id_qcreports')
							->get_compiled_select();

		$q = $this->db->from('qcreports d')
			->join("($queryCountReply) creply", 'creply.id_qcreports = d.id', 'left')
			->join('users usr', 'usr.id_user = d.from_id')
			->where('d.from_id', $id_user)
			->select([
				'd.*',
				'coalesce(creply.jumlah_reply,0) as jumlah_reply',
				'usr.name',
				'usr.username',
				'usr.call_name',
				'usr.image'
			])
			->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false)
			->select('case when d.is_view = 0 then d.last_update else d.created_at end as last_update2', false);
		if($query) {
			$q->like('d.title', $query);
		}

		$sql = $q->get_compiled_select();
		$sql = "SELECT * from ($sql) s1 order by last_update2 desc, id desc";
		$data['sent'] = $this->db->query($sql)->result();

		$q	= $this->db->from('qcreports_receiver dr')
			->join('qcreports d', 'd.id = dr.id_qcreports')
			->join("($queryCountReply) as creply", 'creply.id_qcreports = d.id', 'left')
			->join('users usr', 'usr.id_user = d.from_id')
			->where('dr.id_receiver', $id_user)
			->select([
				'dr.is_view',
				'd.status',
				'd.title',
				'd.id',
				'd.date',
				'coalesce(creply.jumlah_reply,0) as jumlah_reply',
				'usr.name',
				'usr.username',
				'usr.call_name',
				'usr.image'
			])
			->select('case when dr.is_view = 0 or dr.is_view_comment = 0 then d.last_update else d.created_at end as last_update2', false)
			->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
		if($query) {
			$q->like('d.title', $query);
		}
		$sql = $q->get_compiled_select();
		$sql = "SELECT * from ($sql) s1 order by last_update2 desc, id desc";
		$data['received'] = $this->db->query($sql)->result();
		return $data;
	}

	public function send_notification($ids = [0], $title, $message, $data = []) {
		$this->load->library("Notify_lib");
		$nl = new Notify_lib();
		return $nl->send($title, $message, $this->input->post("user_id"), $ids, $data, "qcreport", true);
	}

	function reply($data)
	{
		$reply_id = null;
		$insert = 1;
		$idReply = 0;
		if(array_key_exists('reply_id', $data)) {
			$reply_id = $data['reply_id'];
			unset($data['reply_id']);
		}
		if(array_key_exists('message', $data) == false && array_key_exists('message_reply', $data) == true) {
			$data['message'] = $data['message_reply'];
		}
		if(array_key_exists('id', $data) == false && array_key_exists('id_qcreports', $data) == true) {
			$data['id'] = $data['id_qcreports'];
		}
		if(array_key_exists('sender', $data) == true && array_key_exists('user_id', $data) == false) {
			$data['user_id'] = $data['sender'];
		}
		$qcreport = $this->one(['where' => ['_.id' => $data['id']]]);
		if($qcreport == null) {
			return ['status' => false, 'message' => 'Daily Report not found'];
		}
		$this->load->model('restapi/user_model');
		$sender = $this->user_model->one_user(['where' => ['_.id_user' => $data['user_id']]]);
		if($sender == null) {
			return ['status' => false, 'message' => 'Sender tidak valid'];
		}
		if(in_array($sender['id_department'], [24, 26]) && $qcreport->is_it == '1') {
			$this->load->model('restapi/generalreports_model');
			$generalreport = $this->generalreports_model->one(['where' => ['_.id_qcreports' => $qcreport->id ]]);
			if($generalreport == null) {
				return ['status' => false, 'message' => 'General report not found'];
			}
			return $this->generalreports_model->reply([
				'id' => $generalreport->id,
				'user_id' => $sender['id_user'],
				'message' => $data['message']
			]);
		}
		else if(in_array($sender['id_department'], [27]) && $qcreport->is_absensi == '1') {
			$this->load->model('m_absensi');
			$absensi = $this->m_absensi->one(['where' => ['_.id_qcreports' => $qcreport->id]]);
			if($absensi == null) {
				return ['status' => false, 'message' => 'Absensi not found'];
			}
			return $this->m_absensi->reply([
				'id' => $absensi['id'],
				'user_id' => $sender['id_user'],
				'message' => $data['message']
			]);
		}
		if($reply_id != null){
			$insert = $this->db->where(['id' => $reply_id])
				->update('qcreports_reply',[
					'message_reply' => $data['message']
				]);
			$idReply = $reply_id;
		}else{
			$insert = $this->db->insert('qcreports_reply', [
				'id_qcreports' => $data["id"],
				'sender' => $data['user_id'],
				'message_reply' => $data['message'],
				'date_added' => date('Y-m-d H:i:s')
			]);
			$idReply = $this->db->insert_id();
		}

		$this->db->where(['id' => $data['id']])->update('qcreports', [ 'last_update' => date('Y-m-d H:i:s') ]);

		$dailyReport = $this->db->from("qcreports dr")
			->join("users usr", "usr.id_user = dr.from_id", "left")
			->where(["dr.id" => $data["id"]])
			->select([
				"dr.*",
				"usr.name as sender_name"
			])
			->get()->row_array();

		if($dailyReport['from_id'] == $data['user_id']) {

			$this->db->where(['id_qcreports' => $data['id'], 'is_closed' => 0])->update('qcreports_receiver', ['is_view_comment' => 0]);
		} else {

			$exc = [ $data['user_id'] ];
			$this->db->where([
					'id' => $data['id']
				])
				->where_in('is_view', [0,1])
				->update('qcreports', ['is_view' => 0]);

			$this->db->where([
					'id_qcreports' => $data['id'],
					'is_closed' => 0
				])
				->where_not_in('id_receiver', $exc)
				->update('qcreports_receiver', [
					'is_view_comment' => 0
				]);
		}

		$sender = $this->db->from("users usr")
			->where(["usr.id_user" => $data["user_id"]])
			->get()->row_array();

		$r = false;

		if($dailyReport != null && $sender != null) {

			$listReceiver = $this->getReceivers($dailyReport["id"]);
			$idReceivers = [];

			if($sender["id_user"] != $dailyReport["from_id"]) {
				$idReceivers[] = $dailyReport["from_id"];
			}
			foreach ($listReceiver as $key => $rc) {

				if($rc["id_user"] == $data["user_id"]) {
					continue;
				}
				if(in_array($rc['id_user'], $idReceivers) == false) {
					$idReceivers[] = $rc["id_user"];
				}
			}
			$this->load->library("Notify_lib");
			$nt = new Notify_lib();
			$r = $nt->send(
				"Qc Report Reply",
				$sender["name"] . " has send a new reply",
				$sender["id_user"],
				$idReceivers,
				$dailyReport,
				"qcreport",
				true
			);

		} else {
			$r = "";
			if($dailyReport == null) {
				$r = "daily report is null. ";
			}
			if($sender == null) {
				$r .= "sender is null. ";
			}
		}

		return ["insert" => $insert, "notif" => $r, 'status' => true, 'id' => $idReply, 'message' => 'Reply berhasil disimpan'];
	}

	function save_reply_attachment($data) {
		$this->db->insert('qcreport_reply_attachment', $data);
		return ['status' => true, 'message' => 'success', 'id' => $this->db->insert_id() ];
	}

	function delete()
	{
		$deleted = $this->db->where('id', $this->input->post('id'))->delete('qcreports');
		return ['deleted' => $deleted];
	}

	function getReceivers($id_qcreport) {
		$q = $this->db->from("qcreports_receiver dc")
			->join("users usr", "usr.id_user = dc.id_receiver", "left")
			->join('position p', 'p.id_position = usr.id_position', 'left')
			->join('departments dep', 'dep.id_department = p.id_department', 'left')
			->where([
				"dc.id_qcreports" => $id_qcreport
			])
			->select([
				"usr.name",
				'usr.username',
				'usr.call_name',
				'dc.is_view',
				'dc.is_view_comment',
				'usr.image',
				'p.name_position as position',
				'dep.name as department',
				'usr.id_user'
			])
			->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false)
			->get()->result_array();

		return $q;
	}


	function getAlat($id_qcreport) {
		$q = $this->db->from("qcreports_alat dc")
			->join("master_qc_alat alat", "alat.id_alat = dc.id_alat", "left")
			->where([
				"dc.qcreport_id" => $id_qcreport
			])
			->select(['alat.*'])
			->get()->result_array();

		return $q;
	}

	function baseQuery($args=[]){
		if(isset($args['join_receiver']) == false) {
			$args['join_receiver'] = false;
		}
		if(isset($args['join_reply']) == false) {
			$args['join_reply'] = true;
		}
		if(isset($args['join_absensi']) == false) {
			$args['join_absensi'] = false;
		}
		if(isset($args['join_it']) == false) {
			$args['join_it'] = false;
		}

		$qReply = '';
		if($args['join_reply'] == true) {
			$q = $this->db->from('qcreports_reply _')
				->select('count(*) as jumlah', false)
				->select('GROUP_CONCAT(_.message_reply) as reply_all', false)
				->select([
					'id_qcreports'
				])
				->group_by('id_qcreports');
			$qReply = $q->get_compiled_select();
		}
		$r = $this->db->from("qcreports _")
			->join('users usr', 'usr.id_user = _.from_id', 'left')
			->select([
				'_.*',
				'usr.name',
				'usr.username',
				'usr.call_name',
				'usr.image'
			]);

		if($args['join_receiver'] == true) {
			$r->join('qcreports_receiver rcv', 'rcv.id_qcreports = _.id', 'left')
				->select('case when (rcv.is_view = 0 or rcv.is_view_comment = 0) then 0 else 1 end as is_view_rcv', false);
		}

		if($args['join_reply'] == true) {
			$r->join("($qReply) rply", 'rply.id_qcreports = _.id', 'left')
				->select([
					'coalesce(rply.jumlah, 0) as jumlah_reply'
				]);
		}

		if($args['join_absensi'] == true) {
			$r  ->join('absensi absen', 'absen.id_qcreports = _.id', 'left')
				->join('receivers ra', "ra.id_fk = absen.id and ra.table_fk = 'absensi'", 'left');
		}

		if($args['join_it'] == true) {
			$r 	->join('generalreports gr', 'gr.id_qcreports = _.id', 'left')
				->join('generalreports_receiver rg', 'rg.id_generalreports = gr.id', 'left');
		}

		/*by default join in on branch & position */
		$r 	->join('branch br', 'br.branch_id = usr.branch_id', 'left')
			->join('position pos', 'pos.id_position = usr.id_position', 'left')
			->select([
					'br.branch_name','pos.id_position','pos.name_position'
				]);
		return $r;
	}

	function many($args=[]){
		$this->load->helper('myquery');
		$q = myquery($this->baseQuery($args), $args);
		return $q->get()->result();
	}

	function one($args = []) {
		$this->load->helper('myquery');
		return myquery($this->baseQuery($args), $args)->get()->row();
	}

	function update($where, $data){
		$targets = $this->many(['where'=>$where]);
		if(count($targets)==0){
			return ['status'=>false, 'message'=>'Data not found'];
		}
		foreach ($targets as $key => $target) {
			$this->db->where(['id' => $target->id])->update('qcreports', $data);
		}
		return ['status'=>true,'message'=>'success'];
	}

	function queryReceived($args) {
		$this->load->helper('myquery');
		$args['join_receiver'] = true;
		$q = myquery($this->baseQuery($args), $args);
		if(isset($args['id_user'])) {
			$id_user = $args['id_user'];
			$q 	->group_start()
					->or_where(['rcv.id_receiver' => $id_user])
				->group_end();

		}
		$q->select('case when (rcv.is_view = 0 or rcv.is_view_comment = 0) then _.last_update else _.created_at end as last_update2', false);
		$sql = $q->get_compiled_select();
		return $sql;
	}

	function querySent($args) {
		$this->load->helper('myquery');
		$args['join_receiver'] = false;
		if(isset($args['where_in']) == false) {
			$args['where_in'] = [];
		}
		$args['where_in']['_.is_view'] = ['0','1'];
		$q = myquery($this->baseQuery($args), $args);
		$q 	->select('0 as is_view_rcv')
			->select('case when _.is_view = 0 then _.last_update else _.created_at end as last_update2', false);
		$sql = $q->get_compiled_select();
		return $sql;
	}

	function queryArchived($id_user, $args) {
		$argsReceived = $args;
		$argsReceived['where']['rcv.id_receiver'] = $id_user;

		$argsSent = $args;
		$argsSent['where']['_.from_id'] = $id_user;

		$sqlReceived = $this->queryReceived($argsReceived);
		$sqlSent = $this->querySent($argsSent);

		$sql = "$sqlReceived  \n\n\nunion  \n\n\n$sqlSent";

		return $sql;
	}

	function getReceived($id_user, $args) {
		$args['where']['upper(_.status)'] = 'OPEN';
		$args['join_receiver'] = true;
		$args['id_user'] = $id_user;
		$limit = [];
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		$sql = $this->queryReceived($args);
		$q = myquery($this->db->from("($sql) s1"), ['limit' => $limit, 'order' => [['last_update2', 'desc'], ['id', 'desc']]]);
		return $q->get()->result();
	}

	function getSent($id_user, $args) {
		$args['where']['_.from_id'] = $id_user;
		$args['where']['upper(_.status)'] = 'OPEN';
		$limit = [];
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}

		$sql = $this->querySent($args);
		$q = myquery($this->db->from("($sql) s1"), ['limit' => $limit, 'order' => [['last_update2', 'desc'], ['id', 'desc']]]);
		return $q->get()->result();
	}

	function getArchived($id_user, $args) {
		$id_user = intval($id_user);
		$args['where']['upper(_.status)'] = 'CLOSE';
		$args['join_receiver'] = true;
		$limit = [];
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}

		$sql = $this->queryArchived($id_user, $args);
		$q = myquery($this->db->from("($sql) s1"), ['limit' => $limit, 'order' => [ ['last_update2', 'desc'], ['id', 'desc'] ]]);
		$sql = $q->get_compiled_select();
		return $this->db->query($sql)->result();
	}

	function getCountReceived($id_user, $args) {
		$args['where']['rcv.id_receiver'] = $id_user;
		$args['where']['upper(_.status)'] = 'OPEN';
		$q = $this->queryReceived($args);
		$sql = "select count(*) as jumlah from ($q) s1";
		return $this->db->query($sql)->row();
	}

	function getCountSent($id_user, $args) {
		$args['where']['_.from_id'] = $id_user;
		$args['where']['upper(_.status)'] = 'OPEN';
		$q = $this->querySent($args);
		$sql = "select count(*) as jumlah from ($q) s1";
		return $this->db->query($sql)->row();
	}

	function getCountArchived($id_user, $args) {
		$args['where']['upper(_.status)'] = 'CLOSE';
		$args['join_receiver'] = true;
		$sql = $this->queryArchived($id_user, $args);
		$sql = "select count(*) as jumlah from ($sql) s1";
		return $this->db->query($sql)->row();
	}

	function markAsRead($id) {
		$this->db->set('is_view', 1);
		$this->db->where('id', $id);
		$this->db->update('qcreports');
	}

	function readReceiver($id, $receiverId) {
		$this->db->set('is_view', 1);
		$this->db->set('is_view_comment', 1);
		$this->db->where('id_qcreports', $id);
		$this->db->where('id_receiver', $receiverId);
		$this->db->update('qcreports_receiver');
	}

}
