<?php

class izin_model extends CI_Model {

	public function get_data_izin($params=[], $rows=0, $offset=0,$all=0){
	   	$this->db->select(['users.id_user','users.name','users.call_name','izin.*','branch.branch_name']);
	   	$this->db->from('izin');
	   	if (isset($params['in_param'])) {
	     	foreach ($params['in_param'] as $key => $value) {
	         $this->db->where_in($key,$value);
	     	}
	     	unset($params['in_param']);
	   	}

	   if (isset($params['not_in_param'])) {
	      	foreach ($params['not_in_param'] as $k => $v) {
	          $this->db->where_not_in($k,$v);
	      	}
	      	unset($params['not_in_param']);
	    }

      	if (isset($params['or_param'])) {
	      foreach ($params['or_param'] as $k => $v) {
	          $this->db->or_where([$k => $v]);
	      }
	      unset($params['or_param']);
	    }

	   	$this->db->where($params);
	   	if ($all==0) {
	     	$this->db->limit($rows, $offset);
	   	}
	   $this->db->order_by('izin.date_created', 'DESC');
	   $this->db->join('branch', 'izin.branch_id = branch.branch_id','left');
	   $this->db->join('users', 'users.id_user = izin.id_user','left');
	   $query = $this->db->get();
	   return $query->result_array();
	}

	public function get_last_id(){
		$this->db->select('izin.id_izin');
	   	$this->db->from('izin');
	   	$this->db->order_by('izin.id_izin','DESC');
	   	$this->db->limit(1);
		$query = $this->db->get();
	   	return $query->result_array();
	}

    public function get_karyawan($params=[], $rows=0, $offset=10,$all=0){
	   $this->db->select(['users.id_user','users.name','users.call_name','users.last_name','users.CUTI','users.cuti_keseluruhan','branch.branch_name','bagian.name as nama_bagian','position.name_position','users.STATUS','users.TGLMASUK']);
	   $this->db->from('users');
	   if (isset($params['in_param'])) {
	     foreach ($params['in_param'] as $key => $value) {
	         $this->db->where_in($key,$value);
	     }
	     unset($params['in_param']);
	   }

	   if (isset($params['not_in_param'])) {
        foreach ($params['not_in_param'] as $k => $v) {
            $this->db->where_not_in($k,$v);
        }
        unset($params['not_in_param']);
      }

	   $this->db->where($params);
	   if ($all==0) {
	     $this->db->limit($rows, $offset);
	   }
	   $this->db->order_by('name', 'ASC');
	   $this->db->join('branch', 'users.branch_id = branch.branch_id','left');
	   $this->db->join('bagian', 'users.id_bagian = bagian.id','left');
	   $this->db->join('position', 'users.id_position = position.id_position','left');
	   $query = $this->db->get();
	   return $query->result_array();
	}

	public function save_izin($data){
		$this->db->insert('izin', $data);
		$id_izin = $this->db->insert_id();
		$this->save_attachment($id_izin);
		return $id_izin;
	}

	private function save_attachment($id){
		$result = [];
		if(!empty($_FILES)){
			$directory = sprintf('./uploads/attachments/izin/%s/', sha1(md5($id)));
			$old = umask(0);
			if(file_exists($directory) == false) {
				mkdir($directory, 0777, true);
			}
			umask($old);
			$config['upload_path']          = $directory;
	        // $config['allowed_types']        = 'gif|jpg|png|jpeg|zip|txt|doc|docx|xls|xlsx|pdf|bmp|rar|ppt|pptx';
	        $config['allowed_types']        = '*';
	        // $config['max_size']             = 1024 * 50;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $n = 0;
	        $attachments = $this->input->post("attachments");
	        foreach ($_FILES as $key => $value) {
	        	if($this->upload->do_upload($key)){
	        		$attachment = $attachments[$n];
	        		$d = $this->upload->data();
	        		$this->db->insert('izin_attachment', [
	        			'izin_id' => $id,
	        			'title' => 'Izin Sakit-'.date('Y-m-d').'-'.$id,
	        			'description' => 'Izin Sakit '.$id,
	        			'path' => sprintf('%s%s', substr($directory, 2), $d['file_name']),
	        			'filetype' => $d['file_type'],
	        			'filesize' => $d['file_size'],
	        			'name' => $d['orig_name']
	        		]);
	        		$result[] = [
	        			"attachment" => $attachment,
	        			"value" => $value
	        		];
	        	}
	        	$n++;
	        }
		}
		return $result;
	}

	public function getIzinReceiverDetail($idUser, $idIzin){
		$receivers = [];
		$getIzin = $this->db->select('c.id_user, cr.id_receiver')
						->from('izin c')
						->join('izin_receiver cr', 'cr.id_izin = c.id_izin')
						->where('c.id_izin',$idIzin)
						// ->where('cr.id_receiver','!=',$idUser)
						->where('cr.is_hrd',0)
						->where('cr.is_acc',0)
						->get()->result_array();
		if (count($getIzin) > 0):
			foreach($getIzin as $receiver):
				array_push($receivers, $receiver['id_receiver']);
			endforeach;
		else:
			$getReceiver = $this->db->select('c.id_user, cr.id_receiver')
							->from('izin c')
							->join('izin_receiver cr', 'cr.id_izin = c.id_izin')
							->where('c.id_izin',$idIzin)
							->where('cr.is_hrd',1)
							->get()->result_array();
			if (count($getReceiver) > 0):
				foreach($getReceiver as $receiver):
					array_push($receivers, $receiver['id_receiver']);
				endforeach;
			endif;
		endif;
		return [$receiver['id_user'],$receivers];
	}

	public function approveIzin($user,$idIzin){
		if ($user->id_bagian == 22){
			$getIzin =  $this->db->select('c.*, cr.is_acc')
							->from('izin c')
							->join('izin_receiver cr', 'cr.id_izin = c.id_izin')
							->where('c.id_izin',$idIzin)
							->where('cr.is_hrd',1)
							->get()->row_array();
			if ($getIzin){
				$data['status_pengajuan'] = 'Disetujui HRD';
				$updateIzin = $this->db->update('izin', $data, ['id_izin' => intval($idIzin)]);
				if ($updateIzin){
					$data2['is_acc'] = 1;
					$updateReciver = $this->db->update('izin_receiver',$data2, ['id_izin' => intval($idIzin), 'id_receiver' => $user->id_user]);
					return $updateReciver;
				}else{
					return false;
				}
			}
		}else{
			$getIzin =  $this->db->select('c.*, cr.is_acc')
							->from('izin c')
							->join('izin_receiver cr', 'cr.id_izin = c.id_izin')
							->where('c.id_izin',$idIzin)
							->where('cr.id_receiver',$user->id_user)
							->get()->row_array();
			if ($getIzin){
				$data['status_pengajuan'] = 'Disetujui '.$user->name;
				$updateIzin = $this->db->update('izin', $data, ['id_izin' => intval($idIzin)]);
				if ($updateIzin){
					$data2['is_acc'] = 1;
					$updateReciver = $this->db->update('izin_receiver',$data2, ['id_izin' => intval($idIzin), 'id_receiver' => $user->id_user]);
					return $updateReciver;
				}else{
					return false;
				}
			}
		}
	}

	public function rejectIzin($user,$idIzin, $alasan){
		if ($user->id_bagian == 22):
			$data['status_pengajuan'] = 'DiTolak HRD';
		else:
			$data['status_pengajuan'] = 'Ditolak Atasan';
		endif;
		$data['reject_by'] = $user->id_user;
		$data['reject_reason'] = $alasan;
		$data['deleted'] = 1;
		$updateIzin = $this->db->update('izin', $data, ['id_izin' => intval($idIzin)]);
		$data2['is_closed'] = 1;
		$updateIzinReceiver = $this->db->update('izin_receiver', $data2, ['id_izin' => intval($idIzin)]);
		return $updateIzin;
	}

	public function UpdateStatusIzin($id){
			$izin =  $this->db->select('*')
							->from('izin')
							->where('id_izin',$id)
							->get()->row_array();
			// if ($izin['keperluan'] == 1 && $izin['status_izin'] === 'tidak_masuk'):
			// 	$cuti_data = [
		    //       	'creator_id' => $user->id,
		    //       	'branch_id' => $user->branch_id,
		    //       	'id_user' =>  $user->id,
		    //       	'tgl_request' => date('Y-m-d'),
		    //       	'tgl_ambil_cuti' => $input['tgl_ambil_cuti'],
		    //       	'jml_hari' => $input['jml_hari'],
		    //       	'status_at_cuti' => $this->current_user()->STATUS,
		    //       	'no_dokumen' => $new_number,
		    //       	'no_telp' => $input['telp'],
		    //       	'jatah_cuti' => $this->current_user()->total_jatah_cuti,
		    //       	'jatah_diambil' => $this->getLastCuti($this->current_user),
		    //       	'sisa_jatah' => $this->current_user()->total_jatah_cuti - $this->getLastCuti($this->current_user), // sisa jatah skrg
		    //       	'request_cuti' => $input['jml_hari'],
		    //       	'sisa_cuti' => $this->current_user()->total_jatah_cuti - $this->getLastCuti($this->current_user) - $input['jml_hari'], // sisa jatah skrg setelah dikurangi request
		    //       	'status_cuti' => $input['jenis_cuti']
		    //     ];
			// 	if ($izin['jml_hari'] > 2):
			// 		$this->db->insert('cuti',$dataCuti);
			// 	endif;
			// endif;
			$data['deleted'] = '1';
			$data['status_pengajuan'] = 'Sudah Disetujui!';
			$data['jatah_diambil'] = $izin['request_izin'];
			$data['sisa_jatah'] = (intval($izin['sisa_jatah'])-intval($izin['request_izin']));
			$data['sisa_izin'] = (intval($izin['sisa_izin'])-intval($izin['request_izin']));
			$updateIzin = $this->db->update('izin', $data, ['id_izin' => intval($id)]);
			$data2['is_closed'] = 1;
			$updateIzinReceiver = $this->db->update('izin_receiver', $data2, ['id_izin' => intval($id)]);
			return $updateIzin;
	}

	public function save_izin_receiver($data){
		$save = $this->db->insert('izin_receiver', $data);
		return $save;
	}

	public function updateIzinAsRead($userId, $izinId){
		$updateIzin = $this->db->update('izin_receiver',
			['is_view' => 1],
			[
				'id_izin' => intval($izinId),
				'id_receiver' => intval($userId)
			]
		);
		return $updateIzin;
	}

	public function getReceiver($izinId){
		$getReceiver = $this->db->select('cr.*,u.name,u.username,u.image,p.name_position')
						->from('izin_receiver cr')
						->join('users u', 'cr.id_receiver=u.id_user')
						->join('position p', 'u.id_position=p.id_position')
						->where('cr.id_izin',$izinId)
						->get();
		return $getReceiver->result_array();
	}

	public function getIzinReceiver($params = []){
		$this->db->select('*');
	   	$this->db->from('izin_receiver');
	   	$this->db->where($params);

	   	$query = $this->db->get();
	   	return $query->result_array();
	}

	public function get_user_read($params=[])
    {
        $this->db->select('name, name_position, position.id_position, id_receiver, is_view');
        $this->db->from('izin_receiver');
        $this->db->join('users', 'izin_receiver.id_receiver = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');

        if (isset($params['in_param'])) {
	     	foreach ($params['in_param'] as $key => $value) {
	        	$this->db->where_in($key,$value);
	     	}
	     	unset($params['in_param']);
	   	}
        $this->db->where($params);

        $query = $this->db->get();
	   	return $query->result_array();

    }

    public function delete_izin($id_izin,$type_delete='soft'){
		$this->db->where('id_izin', $id_izin);
		if ($type_delete == 'soft') {
			$data['deleted'] = 1;
	    	$q = $this->db->update('izin', $data);
		}else{
			/*hard way*/
			$this->db->where('id_izin', $id_izin);
			$q = $this->db->delete('izin');
		}

	    return $q;
	}

	public function delete_izin_receiver($id_izin){
		$this->db->where('id_izin', $id_izin);
	    $q = $this->db->delete('izin_receiver');
	    return $q;
	}

	public function delete_notification($id_izin){
		/*by id_receiver*/
		$param = [
			'cuti_flag' => 1,
			'data like ' => '%{"id":'.$id_izin.',"notif_type":"reqizin","id_izin":'.$id_izin.'}%'
		];
		$this->db->where($param);
	    $q = $this->db->delete('notifications');
	    return $q;
	}

	public function update_izin_receiver($data,$param){
	    $this->db->where($param);
	    $q = $this->db->update('izin_receiver', $data);
	    return $q;
	}

	public function update_izin($data,$param){
	    $this->db->where($param);
	    $q = $this->db->update('izin', $data);
	    return $q;
	}

	public function update_notification($data,$param){
	    $this->db->where($param);
	    $q = $this->db->update('notifications', $data);
	    return $q;
	}

	public function restore_izin($id_izin){
	    $param['id_izin'] = $id_izin;
	    $data = [
	    	'deleted' => 0
	    ];
	    $this->db->where($param);
	    $q = $this->db->update('izin', $data);
	    return $q;
	}

	public function getSent($user,$param){
		$query = $this->db->select('iz.*,iz.id_izin AS id,iz.date_created AS date,usr.name,usr.call_name,usr.image,br.branch_name,pos.name_position')
		 				->from('izin iz')
						->join('branch br', 'br.branch_id = iz.branch_id', 'left')
						->join('users usr', 'usr.id_user = iz.id_user', 'left')
						->join('position pos', 'pos.id_position = usr.id_position', 'left')
						->where('iz.id_user',$user)
						->group_start()
						->or_like($param['like'])
						->group_end()
						->where($param['where'])
						//->where_not_in('iz.status_pengajuan',['Sudah Disetujui!'])
						->where('iz.deleted',0)
						->order_by('iz.id_izin','desc')
						->get();
		return $query->result();
	}

	public function getInbox($user,$param){
		$this->db->select('iz.*,iz.id_izin AS id,iz.date_created AS date,usr.name,usr.call_name,usr.image,br.branch_name,pos.name_position')
 				->from('izin iz')
 				//->join('izin_receiver izr', 'izr.id_izin = iz.id_izin','left')
				->join('branch br', 'br.branch_id = iz.branch_id', 'left')
				->join('users usr', 'usr.id_user = iz.id_user', 'left')
				->join('position pos', 'pos.id_position = usr.id_position', 'left')
				->where('(iz.id_izin in (select id_izin from izin_receiver where id_receiver ='.$user->id_user.' and is_asigned=1)) and iz.deleted=0',null) 
				->group_start()
				->or_like($param['like'])
				->group_end()
				->where($param['where'])
				//->where('izr.is_acc',0)
				//->where('izr.id_receiver',$user->id_user)
				->order_by('iz.id_izin','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function getArchived($user,$param){
		$query = $this->db->select('iz.*,iz.id_izin AS id,iz.date_created AS date,usr.name,usr.call_name,usr.image,br.branch_name,pos.name_position')
		 				->from('izin iz')
						->join('branch br', 'br.branch_id = iz.branch_id', 'left')
						->join('users usr', 'usr.id_user = iz.id_user', 'left')
						->join('position pos', 'pos.id_position = usr.id_position', 'left')
						//->where('iz.id_user',$user)
						//->where('iz.status_pengajuan','!=','')
						->where('(iz.id_user ='.$user.' or iz.id_izin in (select id_izin from izin_receiver where id_receiver ='.$user.' and is_asigned=1)) and iz.deleted=1',null) 
						->group_start()
						->or_like($param['like'])
						->group_end()
						->where($param['where'])
						->order_by('iz.id_izin','desc')
						->get();
		return $query->result();
	}

	function detail($id){
		if($id) {
			$query = $this->db->select('iz.*,iz.id_izin AS id,iz.date_created AS date,usr.name,usr.call_name,usr.image,br.branch_name,pos.name_position')
 				->from('izin iz')
				->join('branch br', 'br.branch_id = iz.branch_id', 'left')
				->join('users usr', 'usr.id_user = iz.id_user', 'left')
				->join('position pos', 'pos.id_position = usr.id_position', 'left')
				->where('iz.id_izin',$id)
				->get();
		}

		return $query->row();
	}

	public function get_notification($id)
    {
        return $this->db->from("notifications")
            ->where([
                "to_id" => $id,
                "cuti_flag" => 1
            ])
            ->group_start()
                ->or_where(["read" => null])
                ->or_where(["read" => 0])
            ->group_end()
            ->get();
    }

}
