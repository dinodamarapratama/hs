<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Memento admin_model model
 *
 * This class handles admin_model management related functionality
 *
 * @package		Admin
 * @subpackage	admin_model
 * @author		propertyjar
 * @link		#
 */

class user_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function verify()
	{

		return $this->db->get_where($table, $where);
	}

	public function getLoginDate($id){
		$getDb = $this->db->select(['last_login'])
						  ->from('users')
						  ->where('id_user',$id)
						  ->get();
		return $getDb->row();
	}

	public function register($data){
        $this->db->insert('users', $data);
        return $this->db->insert_id();
    }

	public function signin($username = '', $password = ''){
		if(empty($username)) {
			$username = $this->input->post('username');
		}
		if(empty($password)) {
			$password = $this->input->post('password');
		}
	//	if(empty($phone)) {
	//		$phone = $this->input->post('phone');
	//	}
		$getUser = null;
		$message = '';
		if (isset($username)):
			$getUsername = $this->db->select('*')->from('users usr')->or_where('usr.username', $username)->or_where('usr.name', $username)->or_where('usr.NIP', $username)->get()->num_rows();
			if ($getUsername > 0):
				$this->db->select([
					'usr.*',
					'usr.image as avatar',
					'usr.id_user as id',
					'pos.name_position as position',
					'dep.id_department',
					'bg.name as bagian_name',
					'dep.name as department_name',
					'b.branch_name as branch'
				]);
				$this->db->from('users usr')
				->join('bagian bg', 'bg.id = usr.id_bagian', 'left')
				->join('branch b', 'b.branch_id = usr.branch_id', 'left')
				->join('position pos', 'pos.id_position=usr.id_position', 'left')
				->join('departments dep', 'dep.id_department = bg.id_department or dep.id_department = pos.id_department', 'left');
				// $this->db->join('bagian', 'bagian.id=users.id_bagian', 'left');
				$this->db->group_start();
				$this->db->or_where('usr.username', $username);
				$this->db->or_where('usr.name', $username);
				$this->db->or_where('usr.NIP', $username);
				$this->db->group_end();
				$this->db->where('usr.password', strtoupper(sha1('Bio Medika' . $password)));
				$this->db->where('usr.STATUS !=', 'KELUAR');
				$getUser = $this->db->get()->row();
				if ($getUser) :
					$lastLogin = date('Y-m-d H:i:s');
					$getUser->last_login = $lastLogin;
					$data['last_login'] = $lastLogin;
				//	if ($phone) :
				//		$data['TELPONHP'] = $phone;
				//	endif;
					$message = 'success';
					$updateLogin = $this->db->update('users', $data, ['id_user' => $getUser->id_user]);
				else:
					$message = 'invalid';
				endif;
			else:
				$message = 'notfound';
			endif;
		endif;
		
		return json_encode([
			'user' => $getUser,
			'authorized' => $getUser ? true : false,
			'message' => $message
		]);
		// echo $this->db->from('users')->get_compiled_select();
		// exit();
	}

 /** public function all()
	{
		$this->db->select("CONCAT('https://cdn.iconscout.com/icon/free/png-128/avatar-372-456324.png') as avatar", false);
		$this->db->select('users.*, users.id_user as id, position.name_position as position');
		$this->db->select('departments.name as department');
		$this->db->join('position', 'position.id_position=users.id_position', 'inner');
		$this->db->join('departments', 'position.id_department=departments.id_department', 'left');
		return $this->db->order_by('name', 'asc')->get('users')->result();
	}
	*/

	public function all()
	{

		return $this->db->get('users')->result();
	}

	public function save($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

	public function store_onesignal($user_id, $player_id, $old_id)
	{
		// $user_id = $this->input->post('user_id');
		// $player_id = $this->input->post('player_id');
		// $old_id = $this->input->post("old_id");
		if(is_array($old_id) == false) {
			$old_id = json_decode($old_id, true);
		}

		if(is_array($old_id) == false) {
			$old_id = [];
		}

		// $this->db->where('player_id', $player_id)->delete('onesignal_ids');

		/** hapus record dari parameter $player_id */
		if(empty($player_id) == false) {
			$this->db->where(['player_id' => $player_id])
				->where_not_in('user_id', [$user_id])
				->delete('onesignal_ids');
		}


		/** jika player id yang akan disimpan terdapat di dalam array old_id, dihapus, untuk menghemat operasi db */
		foreach($old_id as $key => $id) {
			if($id == $player_id) {
				unset($old_id[$key]);
			}
		}

		if(count($old_id) > 0){
			// $this->db->where_in("player_id", $old_id)->delete("onesignal_ids");
			$this->db->where_in('player_id', $old_id)
				->where_not_in('user_id', [$user_id])
				->delete('onesignal_ids');
		}
		// $this->db->insert('onesignal_ids', [
		// 	'user_id' => $user_id,
		// 	'player_id' => $player_id
		// ]);
		/** cek apakah user telah memiliki player id ini */
		$old = $this->db->from('onesignal_ids')->where(['user_id' => $user_id, 'player_id' => $player_id])->get()->row();
		if($old == null) {
			$this->db->insert('onesignal_ids', [
				'user_id' => $user_id,
				'player_id' => $player_id
			]);
		}
		return ["success" => true, "old_id" => $old_id];
	}

	function _check_length($input, $min, $max)
{
    $length = strlen($input);

    if ($length <= $max && $length >= $min)
    {
        return TRUE;
    }
    elseif ($length < $min)
    {
        $this->form_validation->set_message('_check_length', 'Minimum number of characters is ' . $min);
        return FALSE;        
    }
    elseif ($length > $max)
    {
        $this->form_validation->set_message('_check_length', 'Maximum number of characters is ' . $max);
        return FALSE;        
    }
}

	function change_password()
	{
		

		$user = $this->db->where('id_user', $this->input->post('user_id'))->get('users')->row();
		$authorized = false;
		if($user->password === sha1('Bio Medika'.$this->input->post('old_password'))){
			$authorized = true;
		
			$this->db->where('id_user', $this->input->post('user_id'))->update('users', [
				'password' => sha1('Bio Medika'.$this->input->post('new_password'))
			]);
		}
		return [
			'user' => $user,
			'authorized' => $authorized
		];
	}

	public function change_avatar($filename)
	{
		$this->db->where('id_user', $this->input->post('user_id'))->update('users', [
			'image' => $filename
		]);
		$user = $this->db->where('id_user', $this->input->post('user_id'))->get('users')->row();
		return $user;
	}

	function delete_onesignal($data) {
		if(empty($data["user_id"])) {
			return ["success" => false, "msg" => "user_id harus diisi"];
		}
		if(empty($data["player_id"])) {
			return ["success" => false, "msg" => "player id harus diisi"];
		}
		if(is_array($data["player_id"]) == false) {
			$data["player_id"] = json_decode($data["player_id"], true);
		}
		if(is_array($data["player_id"]) == false) {
			$data["player_id"] = [];
		}

		if(count($data['player_id']) > 0) {
			$this->db->where_in("player_id", $data["player_id"])
				->where(["user_id" => $data["user_id"]])
				->delete("onesignal_ids");
		}

		return ["success" => true];
	}

	function baseQueryUser($args = [],$select=[]){
		$q = $this->db->from("users _")
			->join('position pos', '_.id_position = pos.id_position', 'left')
			->join('bagian bg', 'bg.id = _.id_bagian', 'left')
			//->join('departments dep', 'dep.id_department = pos.id_department or dep.id_department = bg.id_department', 'left') //jadi ke double2 datanya
			->join('departments dep', 'dep.id_department = _.id_department', 'left')
			->join('branch b1', 'b1.branch_id = _.branch_id', 'left')
			->select(!empty($select)?$select:[
				"_.*",
				'dep.id_department',
				'dep.name as department_name',
				'b1.branch_name',
				'pos.name_position as position_name',
				'bg.name as bagian'
			]);
		return $q;
	}

	function many_user($args = []){
		$this->load->helper("myquery");
		$q = myquery($this->baseQueryUser($args), $args);
		// echo $q->get_compiled_select();
		return $q->order_by('name','ASC')->get()->result_array();
	}

	function get_users($args = [],$select=[]){
		$this->load->helper("myquery");
		$q = myquery($this->baseQueryUser([],$select), $args);
		return $q->get()->result_array();
	}

	function one_user($args = []) {
		$this->load->helper("myquery");
		return myquery($this->baseQueryUser($args), $args)->get()->row_array();
	}

	function get_user_hrd() {
		return $this->many_user([
			'where' => [
				'pos.name_position' => 'STAFF HRD'
			]
		]);
	}

	function login_verify($username='', $password=''){
		$this->db->from('users usr');
		$this->db->group_start();
		$this->db->or_where('usr.username', $username);
		$this->db->or_where('usr.name', $username);
		$this->db->or_where('usr.NIP', $username);
		$this->db->group_end();
		$this->db->where('usr.password', strtoupper($password));
		$this->db->where('usr.STATUS !=', 'KELUAR');
		return $this->db->get()->result();
	}

	function login_verify1($username='', $password=''){
		$this->db->from('users usr');
		$this->db->group_start();
		$this->db->or_where('usr.username', $username);
		$this->db->or_where('usr.name', $username);
		$this->db->or_where('usr.NIP', $username);
		$this->db->or_where('usr.email', $username);
		$this->db->group_end();
		$this->db->where('usr.password', strtoupper($password));
		$this->db->where('usr.STATUS !=', 'KELUAR');
		return $this->db->get()->result();
	}
}
