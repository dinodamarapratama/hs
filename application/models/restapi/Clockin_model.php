<?php

class clockin_model extends CI_Model {

	public function getData($type, $args){
		$sql = $this->db->select([
				'cu.*',
				'users.image',
				'users.call_name',
				'users.username',
				'branch.branch_name',
		])
						->from('clockin_user AS cu')
						->join('users', 'cu.id_user=users.id_user')
						->join('branch', 'branch.branch_id=users.branch_id')
						->where($args['where']);
		if (isset($args['search'])):
			$sql->like('cu.suhu',$args['search']);
			$sql->or_like('cu.status',$args['search']);
			$sql->or_like('cu.card_status',$args['search']);
			$sql->or_like('cu.kondisi_badan',$args['search'] === 'Sehat' ? 1 : 0);
			$sql->or_like('users.call_name',$args['search']);
			$sql->or_like('branch.branch_name',$args['search']);
			// $sql->or_like('mcu_jumlah',$args['search']);
		endif;
		if ($type !== 'HR'):
			$sql->where('cu.id_user', $this->input->post('user_id'));
		endif;
		if (isset($args['tanggal'])):
			$sql->where('cu.tgl_absen',$args['tanggal']);
		endif;
		if (isset($args['start'])):
			if ($args['start'] == 0):
				$sql->limit($args['length']);
			else:
				$sql->limit($args['length'],$args['start']);
			endif;
		endif;
		$sql->order_by('cu.id','desc');
		return $sql->get()->result();
	}

	public function save($data){
		$this->db->insert('clockin_user', $data);
		$ciId = $this->db->insert_id();
		return $ciId;
	}

	public function updateSuhu($id,$suhu,$suhu_id){
		$update = $this->db->update('clockin_user', ['suhu' => $suhu, 'card_status' => 'expired','suhu_id' => $suhu_id], ['id' => intval($id)]);
		return TRUE;
	}

	public function cekSuhu($id,$date){
		$sql = $this->db->select('su.*')
			->from('suhu AS su')
			->join('users', 'su.user_id=users.eintell_id')
			->like('su.recog_time',$date)
			->where('users.id_user',$id)
			->order_by('recog_time','ASC');
		return $sql->get()->row();
	}

	public function detail($id){
		$this->db->select([
			'cu.*',
			'users.image',
			'users.call_name',
			'users.username',
			'branch.branch_name',
		])
				->from('clockin_user AS cu')
				->join('users', 'cu.id_user=users.id_user')
				->join('branch', 'branch.branch_id=users.branch_id')
				 ->where('cu.id',$id);
		$getDetail = $this->db->get()->row_array();
		return $getDetail;
	}

	function delete($id){
		return $this->db->where('id', $id)->delete('clockin_user');
	}
}
