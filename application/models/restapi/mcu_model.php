<?php

class mcu_model extends CI_Model {

	public function getData($type, $args){
		if ($type === 'view2'):
			$args['where']['_.branch_id !=']  = null;
		endif;
		$sql = $this->db->select([
				'_.*', 
				'(SELECT COUNT(id_mcu_pemeriksaan) FROM mcu_pemeriksaan WHERE id_kat=1 AND id_mcu=_.id_mcu)  AS covid',
				'(SELECT COUNT(id_mcu_pemeriksaan) FROM mcu_pemeriksaan WHERE id_sub=1 AND id_mcu=_.id_mcu)  AS lab',
				'(SELECT COUNT(id_mcu_pemeriksaan) FROM mcu_pemeriksaan WHERE id_sub=2 AND id_mcu=_.id_mcu)  AS nonlab',
				])
						->from('mcu AS _')
						->where($args['where']);
		if (isset($args['search'])):
			$sql->like('mcu_perusahaan',$args['search']);
			$sql->or_like('mcu_kode',$args['search']);
			$sql->or_like('mcu_lokasi',$args['search']);
			$sql->or_like('mcu_detail_lokasi',$args['search']);
			$sql->or_like('mcu_tgl_mulai',$args['search']);
			$sql->or_like('mcu_tgl_selesai',$args['search']);
			$sql->or_like('mcu_jumlah',$args['search']);
		endif;
		if (isset($args['tanggal'])):
			$sql->like('mcu_tgl_mulai',$args['tanggal']);
			$sql->or_like('mcu_tgl_selesai',$args['tanggal']);
		endif;
		if (isset($args['start'])):
			if ($args['start'] == 0):
				$sql->limit($args['length']);
			else:
				$sql->limit($args['length'],$args['start']);
			endif;
		endif;
		$sql->order_by('_.mcu_tgl_mulai','desc');
		$sql->order_by('_.mcu_tgl_selesai','desc');
		return $sql->get()->result();
	}

	public function save($data,$pemeriksaan){
		if (isset($data['branch_id']) && !empty($data['branch_id'])) :
			$data['branch_id'] = join(',', $data['branch_id']);
		endif;
		if (isset($data['id_mcu']) && !empty($data['id_mcu'])):
			$this->db->where('id_mcu',$data['id_mcu'])
					 ->update('mcu',$data);
			$mcuId = $data['id_mcu'];
			$this->db->delete('mcu_pemeriksaan',['id_mcu' => $data['id_mcu']]);
		else:
			$this->db->insert('mcu', $data);
			$mcuId = $this->db->insert_id();
		endif;
		if (isset($pemeriksaan)):
			foreach($pemeriksaan as $key=>$value):
				$data2['id_mcu'] = $mcuId;
				$data2['id_kat'] = $value['id_kat'];
				$data2['id_sub'] = $value['id_sub'];
				$data2['id_tes'] = $value['id_tes'];
				$this->db->insert('mcu_pemeriksaan',$data2);
			endforeach;
		endif;
		return TRUE;
		
	}

	public function detail($id){
		$this->db->select([
			'm.*',
			"(SELECT GROUP_CONCAT(DISTINCT(a.id_kat)) FROM mcu_pemeriksaan AS a LEFT JOIN master_mcu_tes AS b ON(a.id_tes=b.id_tes) WHERE a.id_mcu = m.id_mcu) AS id_kat",
			"(SELECT GROUP_CONCAT(DISTINCT(a.id_sub)) FROM mcu_pemeriksaan AS a LEFT JOIN master_mcu_tes AS b ON(a.id_tes=b.id_tes) WHERE a.id_mcu = m.id_mcu) AS id_sub",
			"(SELECT GROUP_CONCAT(a.id_tes) FROM mcu_pemeriksaan AS a LEFT JOIN master_mcu_tes AS b ON(a.id_tes=b.id_tes) WHERE a.id_mcu = m.id_mcu) AS id_tes",
		])
				 ->from('mcu AS m')
				 ->where('m.id_mcu',$id);
		$getDetail = $this->db->get()->row_array();
		if ($getDetail):
			$pemeriksaan = '';
			$kat = explode(',',$getDetail['id_kat']);
			if (in_array(1,$kat)):
				$pemeriksaan .= 'Covid,';
			endif;
			$getTestName = $this->db->select('nama_tes')
										->where_in('id_tes',explode(',',$getDetail['id_tes']))
										->get('master_mcu_tes');
				if ($getTestName):
					$namaTest = [];
					foreach($getTestName->result() as $testName):
						array_push($namaTest,$testName->nama_tes);
					endforeach;
					$pemeriksaan .= join(',',$namaTest);
				endif;
			$getDetail['pemeriksaan'] = $pemeriksaan;
		endif;
		return $getDetail;
	}

	function delete($id){
		$this->db->where('id_mcu', $id)->delete('mcu_pemeriksaan');
		$deleted2 = $this->db->where('id_mcu', $id)->delete('mcu');
		return ['deleted' => $deleted2];
	}

	public function getDataDropdown($table, $order = '', $where = []){
		if ($order) :
			$this->db->order_by($order, 'ASC');
		endif;
		if (count($where) > 0):
			foreach($where as $key=>$value):
				$this->db->where_in($key,$value);
			endforeach;
		endif;
		// $this->db->get($table);
		// print_r($where);
		// print_r($this->db->last_query());
		return $this->db->get($table);
	}
}
