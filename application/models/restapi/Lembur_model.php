<?php

class Lembur_model extends CI_Model{
    
    public function getOvertime($id_user, $perPage = null, $offset = null, $type = null, $id_sp = null){
       
        if ($id_sp) {
            $this->db->where('sp.id_sp',$id_sp);
        }elseif($type == 'sent'){
            $this->db->where('sp.creator',$id_user);
        }
        $this->db->select('sp.*, usr.name, usr.NOREK, br.branch_name, dprt.name as department, position.name_position');
        $this->db->from('sp_overtime sp');
        $this->db->join('branch br', 'br.branch_id = sp.branch_id', 'left');
        $this->db->join('users usr', 'usr.id_user = sp.id_user', 'left');
        $this->db->join('position', 'usr.id_position = position.id_position', 'left');
        $this->db->join('departments dprt', 'dprt.id_department  = sp.department_id', 'left');      
        $this->db->order_by('sp.id_sp','desc');
        $this->db->limit($perPage, $offset);
        return  $this->db->get();
        
    }

}