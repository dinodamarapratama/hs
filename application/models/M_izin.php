<?php

class M_izin extends CI_Model {
	
	public function get_data_izin($params=[], $rows=0, $offset=0,$all=0){
	   	$this->db->select(['users.id_user','users.name','users.last_name','izin.*','branch.branch_name','position.name_position','(select is_hrd from izin_receiver where id_izin=izin.id_izin and id_receiver = '.'"'.$this->session->userdata('id_user').'"'.') as is_hrd','users.id_position','users.id_bagian','users.last_reset_cuti']);
	   	$this->db->from('izin');
	   	
		if (isset($params['in_param'])) {
	     	foreach ($params['in_param'] as $key => $value) {
				if (strpos($value, '(') !== false) 
					$this->db->where_in($key,$value,false);  
				else
					$this->db->where_in($key,$value);  
	     	}
	     	unset($params['in_param']);
	   	}
	   
	   if (isset($params['not_in_param'])) {
	      	foreach ($params['not_in_param'] as $k => $v) {
	          $this->db->where_not_in($k,$v);  
	      	}
	      	unset($params['not_in_param']);
	    }

      	if (isset($params['or_param'])) {
	      foreach ($params['or_param'] as $k => $v) {
	          $this->db->or_where([$k => $v]);  
	      }
	      unset($params['or_param']);
	    }

	   	$this->db->where($params);
	   	if ($all==0) {
	     	$this->db->limit($rows, $offset);  
	   	}
	   $this->db->order_by('izin.date_created', 'DESC');
	   $this->db->join('branch', 'izin.branch_id = branch.branch_id','left');
	   $this->db->join('users', 'users.id_user = izin.id_user','left');
	   $this->db->join('position', 'users.id_position = position.id_position','left');
	   $query = $this->db->get();
	   return $query->result_array();
	}

	public function get_last_id(){
		$this->db->select('izin.id_izin');
	   	$this->db->from('izin');
	   	$this->db->order_by('izin.id_izin','DESC');
	   	$this->db->limit(1);
		$query = $this->db->get();
	   	return $query->result_array();
	}
    public function get_karyawan($params=[], $rows=0, $offset=10,$all=0){
	   $this->db->select(['users.id_user','users.name','users.last_name','users.CUTI','users.cuti_keseluruhan','branch.branch_name','bagian.name as nama_bagian','position.name_position','users.STATUS','users.TGLMASUK','users.id_position','users.id_bagian','users.branch_id']);
	   $this->db->from('users');
	   if (isset($params['in_param'])) {
	     foreach ($params['in_param'] as $key => $value) {
	         $this->db->where_in($key,$value);  
	     }
	     unset($params['in_param']);
	   }
	   
	   if (isset($params['not_in_param'])) {
        foreach ($params['not_in_param'] as $k => $v) {
            $this->db->where_not_in($k,$v);  
        }
        unset($params['not_in_param']);
      }

	   $this->db->where($params);
	   if ($all==0) {
	     $this->db->limit($rows, $offset);  
	   }
	   $this->db->order_by('name', 'ASC');
	   $this->db->join('branch', 'users.branch_id = branch.branch_id','left');
	   $this->db->join('bagian', 'users.id_bagian = bagian.id','left');
	   $this->db->join('position', 'users.id_position = position.id_position','left');
	   $query = $this->db->get();
	   return $query->result_array();
	}

	public function save_izin($data){
		$this->db->insert('izin', $data);
		$id_izin = $this->db->insert_id();
		return $id_izin;
	}

	public function save_izin_receiver($data){
		$save = $this->db->insert('izin_receiver', $data);
		return $save;
	}

	public function getIzinReceiver($params = []){
		$this->db->select('*');
	   	$this->db->from('izin_receiver');
	   	$this->db->where($params);
	   	
	   	$query = $this->db->get();
	   	return $query->result_array();
	}

	public function get_user_read($params=[])
    {
        $this->db->select('name, name_position, position.id_position, id_receiver, is_view, is_view, is_hrd, is_acc');
        $this->db->from('izin_receiver');
        $this->db->join('users', 'izin_receiver.id_receiver = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        
        if (isset($params['in_param'])) {
	     	foreach ($params['in_param'] as $key => $value) {
	        	$this->db->where_in($key,$value);  
	     	}
	     	unset($params['in_param']);
	   	}
        $this->db->where($params);

        $query = $this->db->get();
	   	return $query->result_array();

    }

    public function delete_izin($id_izin,$type_delete='soft'){
		$this->db->where('id_izin', $id_izin);
		if ($type_delete == 'soft') {
			$data['deleted'] = 1;
	    	$q = $this->db->update('izin', $data);
		}else{
			/*hard way*/
			$this->db->where('id_izin', $id_izin);
			$q = $this->db->delete('izin');	
		}

	    return $q;
	}

	public function delete_izin_receiver($id_izin){
		$this->db->where('id_izin', $id_izin);
	    $q = $this->db->delete('izin_receiver');
	    return $q;
	}

	public function delete_notification($id_izin){
		/*by id_receiver*/
		$param = [
			'cuti_flag' => 1,
			'data like ' => '%{"id":'.$id_izin.',"notif_type":"reqizin","id_izin":'.$id_izin.'}%'
		];
		$this->db->where($param);
	    $q = $this->db->delete('notifications');
	    return $q;
	}

	public function update_izin_receiver($data,$param){
	    $this->db->where($param);
	    $q = $this->db->update('izin_receiver', $data);
	    return $q;
	}

	public function update_izin($data,$param){
	    $this->db->where($param);
	    $q = $this->db->update('izin', $data);
	    return $q;
	}

	public function update_notification($data,$param){
	    $this->db->where($param);
	    $q = $this->db->update('notifications', $data);
	    return $q;
	}

	public function restore_izin($id_izin){
	    $param['id_izin'] = $id_izin;
	    $data = [
	    	'deleted' => 0
	    ];
	    $this->db->where($param);
	    $q = $this->db->update('izin', $data);
	    return $q;
	}

	public function get_notification($id)
    {
        return $this->db->from("notifications")
            ->where([
                "to_id" => $id,
                "cuti_flag" => 1
            ])
            ->group_start()
                ->or_where(["read" => null])
                ->or_where(["read" => 0])
            ->group_end()
            ->get();
    }

}