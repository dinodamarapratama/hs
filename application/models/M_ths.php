<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_ths extends CI_model
{

    public function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $result = $this->db->delete($table);
        return $result;
    }

    public function getAllThs()
    {
        $this->db->select('id, date, nama, pemeriksaan, jumlah_pasien, dokter, dokter, homeservice.alamat, homeservice.status, time_name, time_name, no_telp, catatan, users.name as petugas, users.id_user');
        $this->db->from('homeservice');
        $this->db->join('hs_time', 'homeservice.time_id = hs_time.time_id');
        $this->db->join('hs_initial_petugas_hs', 'homeservice.ptgshs_id=hs_initial_petugas_hs.ptgshs_id');
        $this->db->join('users', 'hs_initial_petugas_hs.id_user = users.id_user');
        $this->db->order_by('id', 'desc');
        $data = $this->db->get();
        return $data;
    }

    public function getSelectedThs2($id_hs)
    {
        $this->db->select('id, date, nama, pemeriksaan, jumlah_pasien, dokter, dokter, homeservice.alamat, homeservice.status, time_name, time_name, hs_time.time_id, no_telp, catatan, users.name as petugas, users.id_user, hs_initial_petugas_hs.ptgshs_id');
        $this->db->from('homeservice');
        $this->db->join('hs_time', 'homeservice.time_id = hs_time.time_id');
        $this->db->join('hs_initial_petugas_hs', 'homeservice.ptgshs_id=hs_initial_petugas_hs.ptgshs_id');
        $this->db->join('users', 'hs_initial_petugas_hs.id_user = users.id_user');
        $this->db->where(['id' => $id_hs]);
        $hs = $this->db->get()->row();

        try {        
            $ths = $this->db->get_where('ths', ['id_hs' => $id_hs])->row_array();
            if(!empty($ths)){
                $id_ths = $ths['id_ths'];
                $this->db->select('id_price');
                $this->db->from('ths_price');
                $this->db->where(['id_ths' => $id_ths]);
                $lists = $this->db->get()->result_array();
                $new_array = [];
                foreach ($lists as $list) {
                    array_push($new_array, $list['id_price']);
                }
                $lists_string = implode(',', $new_array);
                $hs->pricelist = $lists_string;
            }else{
                throw new Exception("No result was found for id:$id_hs");
            }
        } catch (\Exception $e) {
            $hs->pricelist = $e->getMessage();
        }
        

        return $hs;

        // $this->db->select('ths.id_ths, ths.tgl_ths, ths.total_harga, ths.disc, ths.subtotal, homeservice.nama, homeservice.alamat, homeservice.no_telp, homeservice.pemeriksaan, homeservice.status, users.name as petugas,');
        // $this->db->from('ths');
        // $this->db->join('homeservice', 'ths.id_hs = homeservice.id');
        // $this->db->join('hs_initial_petugas_hs', 'homeservice.petugas_id = hs_initial_petugas_hs.ptgshs_id');
        // $this->db->join('users', 'hs_initial_petugas_hs.id_user = users.id_user');
        // $this->db->where(['id_ths' => $id_ths]);
        // $hs = $this->db->get()->row();
        // $hs->tgl_ths = $this->dateForHtml($hs->tgl_ths);

        // $this->db->select('id_price');
        // $this->db->from('ths_price');
        // $this->db->where(['id_ths' => $id_ths]);
        // $lists = $this->db->get()->result_array();
        // $new_array = [];
        // foreach ($lists as $list) {
        //     array_push($new_array, $list['id_price']);
        // }
        // $lists_string = implode(',', $new_array);
        // $hs->pricelist = $lists_string;

        // return $hs;
    }

    private function dateForHtml($date)
    {
        $newdate = explode(' ', $date);
        return($newdate[0].'T'.$newdate[1]);
    }

    public function getSelectedThs($id_ths)
    {
        $this->db->select('id_ths, tgl_ths, total_harga, disc, nama');
        $this->db->from('ths');
        $this->db->join('homeservice', 'homeservice.id = ths.id_hs');
        $this->db->where(['id_ths' => $id_ths]);
        $hs = $this->db->get()->row();

        $this->db->select('id_price, id_ths, test_name, price');
        $this->db->from('ths_price');
        $this->db->join('ths_master_pricelist', 'ths_price.id_price_list = ths_master_pricelist.id_pricelist');
        $this->db->where(['id_ths' => $id_ths]);
        $hs->lists = $this->db->get()->result();

        return $hs;
    }
    

    public function getPrice($id_ths)
    {
        //price list
        $this->db->select('ths_price.id_price, ths_master_pricelist.test_name, ths_master_pricelist.price');
        $this->db->from('ths_price');
        $this->db->where('ths_price.id_ths', $id_ths);
        $this->db->join('ths_master_pricelist', 'ths_price.id_price_list = ths_master_pricelist.id_pricelist');
        $price_list = $this->db->get()->result_array();
        foreach($price_list as $k => $v){
            $price_list[$k]['price'] = $this->rupiah($v['price']);
        }
        //get ths total and sub total
        $data_ths = $this->db->get_where('ths', ['id_ths' => $id_ths])->row_array();
        unset($data_ths['id_ths']);
        unset($data_ths['id_hs']);
        unset($data_ths['tgl_ths']);
        unset($data_ths['creator_id']);
        
        // setup array 
        $array = [
            "total"=> count($price_list),
            "rows" => $price_list,
            "footer" => [
                [
                "test_name" => 'Subtotal Harga',
                "price" => $this->rupiah($data_ths['subtotal'])
                ],
                [
                "test_name" => 'Diskon',
                "price" => $this->rupiah($data_ths['disc'])
                ],
                [
                "test_name" => 'Total Harga',
                "price" => $this->rupiah($data_ths['total_harga'])
                ]
            ]
        ];
        return $array;
        
    }

    public function getPetugas()
    {
        $this->db->select('hs_initial_petugas_hs.ptgshs_id ,hs_initial_petugas_hs.abbr_hs, users.name');
        $this->db->from('hs_initial_petugas_hs');
        $this->db->join('users', 'hs_initial_petugas_hs.id_user = users.id_user');
        return $this->db->get()->result();  
        
    }

    public function saveNewThs($user_id)
    {

        // Array
        // (
        //     [id] => 24955 //id homeservice
        //     [date] => 2021-01-08
        //     [ptgshs_id] => 3399
        //     [nama] => klinik indra dharma
        //     [pemeriksaan] => mcu
        //     [jumlah_pasien] => 2
        //     [dokter] => indra dharma
        //     [alamat] => Alam Sutera
        //     [status] => Diproses
        //     [time_id] => 5
        //     [no_telp] => 021
        //     [catatan] => 
        //     [pricelist] => Array
        //         (
        //             [0] => 2
        //             [1] => 5
        //         )

        // )
        $form = $this->input->post();
        $pricelist = "";
        $pricelist = isset($form['pricelist']) ? $form['pricelist']: '';
        
        // update homeservice
        $this->db->set('date', $form['date']);
        $this->db->set('ptgshs_id', $form['ptgshs_id']);
        $this->db->set('nama', $form['nama']);
        $this->db->set('pemeriksaan', $form['pemeriksaan']);
        $this->db->set('jumlah_pasien', $form['jumlah_pasien']);
        $this->db->set('dokter', $form['dokter']);
        $this->db->set('alamat', $form['alamat']);
        $this->db->set('status', $form['status']);
        $this->db->set('time_id', $form['time_id']);
        $this->db->set('no_telp', $form['no_telp']);
        $this->db->set('catatan', $form['catatan']);
        $this->db->where('id', $form['id']);
        $this->db->update('homeservice');
        
        // pricelist send
        if($pricelist != ''){
            // update or insert new ths
            $ths = $this->db->get_where('ths', ['id_hs' => $form['id']])->row();
            if (isset($ths)) {
                //update ths
                $total = 0;
                foreach ($form['pricelist'] as $id_pricelist) {
                    $price = $this->db->get_where('ths_master_pricelist', ['id_pricelist' => $id_pricelist])->row()->price;
                    $total += $price;
                }
                $data['id_hs'] = $form['id'];
                $data['tgl_ths'] = date("Y-m-d H:i:s");
                $data['total_harga'] = $total - $ths->disc;
                $data['disc'] = $ths->disc;
                $data['creator_id'] = $user_id;
                $data['subtotal'] = $total;
                $this->db->where('id_ths', $ths->id_ths);
                $this->db->update('ths', $data);
                // update price list
                $this->db->where('id_ths', $ths->id_ths);
                $this->db->delete('ths_price');
                
                $insert_list = array();
    
                foreach ($form['pricelist'] as $i => $ls) {
                    $insert_list[$i]['id_ths'] = $ths->id_ths;
                    $insert_list[$i]['id_price_list'] = $ls;
                }
    
                $this->db->insert_batch('ths_price', $insert_list);
                
            }else{
                //add new ths
                $total = 0;
                foreach ($form['pricelist'] as $id_pricelist) {
                    $price = $this->db->get_where('ths_master_pricelist', ['id_pricelist' => $id_pricelist])->row()->price;
                    $total += $price;
                }
    
                $data['id_hs'] = $form['id'];
                $data['tgl_ths'] = date("Y-m-d H:i:s");
                $data['total_harga'] = $total;
                $data['disc'] = 0;
                $data['creator_id'] = $user_id;
                $data['subtotal'] = $total;
                $this->db->insert('ths', $data);
                $id_ths = $this->db->insert_id();
                // add price list
                $insert_list = array();
    
                foreach ($form['pricelist'] as $i => $ls) {
                    $insert_list[$i]['id_ths'] = $id_ths;
                    $insert_list[$i]['id_price_list'] = $ls;
                }
    
                $this->db->insert_batch('ths_price', $insert_list);
            }
            return true;
        }

        return true;
            
    }
            
    protected function rupiah($angka){
	
        $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
        return $hasil_rupiah;
     
    }

    public function get_pricelist()
    {
        $this->db->select('*');
        $this->db->from('ths_master_pricelist p');
        return $this->db->get()->result();  
        
    }

    public function get_pricelist10()
    {
        $this->db->select('*');
        $this->db->from('ths_master_pricelist_10 p');
        return $this->db->get()->result();  
        
    }

    public function get_pricelistloyalti()
    {
        $this->db->select('*');
        $this->db->from('ths_master_pricelist_loyalty p');
        return $this->db->get()->result();  
        
    }

    public function edit_data_p($where, $table)
    {
        $this->db->select('tmp.*');
        $this->db->from('ths_master_pricelist tmp');
        $this->db->where($where);
        return $this->db->get();
    }

     public function edit_data_p1($where, $table)
    {
        $this->db->select('tmp.*');
        $this->db->from('ths_master_pricelist_loyalty tmp');
        $this->db->where($where);
        return $this->db->get();
    }

      public function edit_data_p2($where, $table)
    {
        $this->db->select('tmp.*');
        $this->db->from('ths_master_pricelist_10 tmp');
        $this->db->where($where);
        return $this->db->get();
    }

    /*
    * Fetch members data from the database
    * @param array filter data based on the passed parameters
    */
    function getRows($params = array()){
    $this->db->select('*');
    $this->db->from($this->table);

    if(array_key_exists("where", $params)){
    foreach($params['where'] as $key => $val){
    $this->db->where($key, $val);
    }
    }

    if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
    $result = $this->db->count_all_results();
    }else{
    if(array_key_exists("id", $params)){
    $this->db->where('id', $params['id']);
    $query = $this->db->get();
    $result = $query->row_array();
    }else{
    $this->db->order_by('id', 'desc');
    if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
    $this->db->limit($params['limit'],$params['start']);
    }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
    $this->db->limit($params['limit']);
    }

    $query = $this->db->get();
    $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    }

    // Return fetched data
    return $result;
    }

    /*
    * Insert members data into the database
    * @param $data data to be insert based on the passed parameters
    */
    public function insert($data = array()) {
    if(!empty($data)){
    // Add created and modified date if not included
    if(!array_key_exists("created", $data)){
    $data['created'] = date("Y-m-d H:i:s");
    }
    if(!array_key_exists("modified", $data)){
    $data['modified'] = date("Y-m-d H:i:s");
    }

    // Insert member data
    $insert = $this->db->insert($this->table, $data);

    // Return the status
    return $insert?$this->db->insert_id():false;
    }
    return false;
    }

    /*
    * Update member data into the database
    * @param $data array to be update based on the passed parameters
    * @param $condition array filter data
    */
    public function update($data, $condition = array()) {
    if(!empty($data)){
    // Add modified date if not included
    if(!array_key_exists("modified", $data)){
    $data['modified'] = date("Y-m-d H:i:s");
    }

    // Update member data
    $update = $this->db->update($this->table, $data, $condition);

    // Return the status
    return $update?true:false;
    }
    return false;
    }


    
}
