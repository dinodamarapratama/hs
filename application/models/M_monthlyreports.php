<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_monthlyreports extends CI_model
{

    public function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    public function insert_batch($table, $data)
    {
        $this->db->insert_batch($table, $data);
    }

    public function get_data_like($where, $colum, $table)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->like($colum, $where);
        return $this->db->get();
    }

    public function get_data_join()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('position', 'users.id_position = position.id');
        return $this->db->get();
    }

    public function get_sent_to($id)
    {
        $this->db->select('name, (select name_position from position where users.id_position = position.id_position) as name_position, id_receiver');
        $this->db->from('monthlyreports_receiver');
        $this->db->join('users', 'monthlyreports_receiver.id_receiver = users.id_user');
        //$this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('monthlyreports_receiver.id_receiver', $id);

        return $this->db->get();

    }

    public function get_sent_count($id)
    {
        $this->db->from('monthlyreports_receiver');
        $this->db->where('monthlyreports_receiver.id_monthlyreports', $id);

        return $this->db->get()->num_rows();
    }

    public function get_read_count($id)
    {
        $this->db->from('monthlyreports_receiver');
        $this->db->where('monthlyreports_receiver.id_monthlyreports', $id);
        $this->db->where('monthlyreports_receiver.is_view', '1');

        return $this->db->get()->num_rows();
    }

    public function get_user_read($id)
    {
        $this->db->select('name, name_position, id_receiver, is_view');
        $this->db->from('monthlyreports_receiver');
        $this->db->join('users', 'monthlyreports_receiver.id_receiver = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('monthlyreports_receiver.id_monthlyreports', $id);

        return $this->db->get();

    }

    
    public function get_data_received($id, $id_monthlyreports = null, $position = null, $search = null)
    {
        $this->db->select('monthlyreports.*,monthlyreports.status as status,users.*,position.*,branch.*,
            monthlyreports_receiver.id_monthlyreports,
            monthlyreports_receiver.id_receiver,monthlyreports_receiver.is_view as read,monthlyreports.status as status');
        $this->db->from('monthlyreports');
        $this->db->join('monthlyreports_receiver', 'monthlyreports_receiver.id_monthlyreports = monthlyreports.id');
        $this->db->join('users', 'monthlyreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($id_monthlyreports)) {
            $this->db->where('monthlyreports_receiver.id_monthlyreports', $id_monthlyreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('monthlyreports.kategori',$search,'both');
            $this->db->or_like('monthlyreports.title',$search,'both');
            $this->db->or_like('monthlyreports.date',$search,'both');
            $this->db->or_like('monthlyreports.description',$search,'both');
            $this->db->limit(1);
            

        }
        $this->db->where('monthlyreports_receiver.id_receiver', $id);
        // $this->db->where('monthlyreports_receiver.is_closed <>', '1');
        $this->db->where('upper(monthlyreports.status)', 'OPEN');
        /* data belum dihapus oleh creator */
        $this->db->where_in('monthlyreports.is_view', [0,1]);
        /* data belum dihapus oleh receiver */
        $this->db->where('monthlyreports_receiver.is_closed', '0');
        // $this->db->where('dailyreports.status <>', 'Close');
        $this->db->order_by('monthlyreports_receiver.id', 'desc');

        return $this->db->get();
    }

    public function get_data_sent($id, $id_monthlyreports = null, $position = null, $search = null)
    {
        $this->db->select('monthlyreports.*,users.*,position.*,branch.*,
            monthlyreports.id as id_monthlyreports,monthlyreports.status as status, monthlyreports_receiver.id_receiver');
        $this->db->from('monthlyreports');
        $this->db->join('monthlyreports_receiver', 'monthlyreports_receiver.id_monthlyreports = monthlyreports.id');
        $this->db->join('users', 'monthlyreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($id_monthlyreports)) {
            // $this->db->where('monthlyreports_receiver.id_monthlyreports', $id_monthlyreports);
            $this->db->where('monthlyreports.id', $id_monthlyreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('monthlyreports.kategori',$search,'both');
            $this->db->or_like('monthlyreports.title',$search,'both');
            $this->db->or_like('monthlyreports.date',$search,'both');
            $this->db->or_like('monthlyreports.description',$search,'both');
            $this->db->limit(1);
            
        }
        $this->db->where('monthlyreports.from_id', $id);
        $this->db->where('monthlyreports.id_draft', '0');
        $this->db->where('upper(monthlyreports.status)', 'OPEN');
        // $this->db->where('monthlyreports.status <>', 'Close');
        // $this->db->where('monthlyreports.id_draft <>', '1');
        // $this->db->where('monthlyreports.is_view <>', '1');
        /* data belum dihapus oleh creator */
        $this->db->where_in('monthlyreports.is_view', [0,1]);
        $this->db->order_by('monthlyreports.id', 'desc');
        // echo $this->db->get_compiled_select();
        // exit();
        return $this->db->get();
    }

    public function get_data_archived($id, $id_monthlyreports = null, $position = null, $search = null)
    {
        $this->db->select('monthlyreports.*,users.*,position.*,branch.*,
            monthlyreports_receiver.id_monthlyreports,monthlyreports_receiver.id_receiver,monthlyreports.status as status');
        $this->db->from('monthlyreports');
        $this->db->join('monthlyreports_receiver', 'monthlyreports_receiver.id_monthlyreports = monthlyreports.id');
        $this->db->join('users', 'monthlyreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($id_monthlyreports)) {
            $this->db->where('monthlyreports_receiver.id_monthlyreports', $id_monthlyreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('monthlyreports.kategori',$search,'both');
            $this->db->or_like('monthlyreports.title',$search,'both');
            $this->db->or_like('monthlyreports.date',$search,'both');
            $this->db->or_like('monthlyreports.description',$search,'both');
            $this->db->limit(1);
            $this->db->group_end(); 
        }
        
         $this->db->where('upper(monthlyreports.status)', 'CLOSE');
        // $this->db->where('dailyreports.status <>', 'Open');
        $this->db->where('monthlyreports.is_view <>', '1');
        $this->db->group_start();
        $this->db->or_where('monthlyreports.from_id', $id);
        $this->db->or_where('monthlyreports_receiver.id_receiver', $id); 
        $this->db->group_end();
        $this->db->order_by('monthlyreports.id', 'desc');
        return $this->db->get();
    }

    public function get_data_draft($id, $id_monthlyreports = null, $position = null, $search = null)
    {
        $this->db->select('monthlyreports.*,users.*,position.*,branch.*,
            monthlyreports_receiver.id_monthlyreports,monthlyreports_receiver.id_receiver,monthlyreports.status as status');
        $this->db->from('monthlyreports');
        $this->db->join('monthlyreports_receiver', 'monthlyreports_receiver.id_monthlyreports = monthlyreports.id');
        $this->db->join('users', 'monthlyreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($id_monthlyreports)) {
            $this->db->where('monthlyreports_receiver.id_monthlyreports', $id_monthlyreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
           $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('monthlyreports.kategori',$search,'both');
            $this->db->or_like('monthlyreports.title',$search,'both');
            $this->db->or_like('monthlyreports.date',$search,'both');
            $this->db->or_like('monthlyreports.description',$search,'both');
            $this->db->limit(1);
        }
        $this->db->where('monthlyreports.from_id', $id);
        $this->db->where('monthlyreports.is_view <>', '1');
        $this->db->where('monthlyreports.id_draft <>', '0');
        $this->db->where('monthlyreports.id_draft', '1');
        $this->db->order_by('monthlyreports.id', 'desc');
        return $this->db->get();
    }

    public function get_user_replay($id)
    {
        $this->db->select('*');
        $this->db->from('monthlyreports');
        $this->db->join('monthlyreports_reply', 'monthlyreports.id = monthlyreports_reply.id_monthlyreports');
        $this->db->join('users', 'monthlyreports_reply.sender = users.id_user');
        $this->db->where('monthlyreports.id', $id);

        return $this->db->get();
    }

    public function get_to_id($id,$user_id)
    {
        $this->db->select('*');
        $this->db->from('monthlyreports_receiver');
        $this->db->join('monthlyreports', 'monthlyreports.id = monthlyreports_receiver.id_monthlyreports');
        $this->db->where('monthlyreports.id', $id);
        $this->db->where('monthlyreports_receiver.id_receiver', $user_id);

        return $this->db->get();
    }

    public function get_from_id($id)
    {
        $this->db->select('*');
        $this->db->from('monthlyreports_receiver');
        $this->db->join('monthlyreports', 'monthlyreports.id = monthlyreports_receiver.id_monthlyreports');
        $this->db->where('monthlyreports.id', $id);

        return $this->db->get();
    }

    public function get_notification($id)
    {
        return $this->db->from("notifications")
            ->where([
                "to_id" => $id
            ])
            ->group_start()
                ->or_where(["read" => null])
                ->or_where(["read" => 0])
            ->group_end()
            // ->where(["anime" => "naruto"])
            ->get();
        
        // $this->db->select('*');
        // $this->db->from('notifications');
        // $this->db->where('to_id', $id);
        // $this->db->where('read', 0);
        // $this->db->where('read <>', 1);
        // $this->db->order_by('id', 'desc');
        
        // return $this->db->get();
    }


    public function get_user_hrd()
    {
        $this->db->select('id_user,name');
        $this->db->from('users');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('name_position','STAFF HRD');

        return $this->db->get();
    } 

    public function testing($array)
    {
        return print("<pre>" . print_r($array, true) . "</pre>");
    }

    function getCountReceived($id_user, $args) {
        $args['where']['rcv.id_receiver'] = $id_user;
        $args['where']['upper(_.status)'] = 'OPEN';
        $q = $this->queryReceived($args);
        $sql = "select count(*) as jumlah from ($q) s1";
        return $this->db->query($sql)->row();
    }

    function getCountSent($id_user, $args) {
        $args['where']['_.from_id'] = $id_user;
        $args['where']['upper(_.status)'] = 'OPEN';
        $q = $this->querySent($args);
        $sql = "select count(*) as jumlah from ($q) s1";
        return $this->db->query($sql)->row();
    }

    function getCountArchived($id_user, $args) {
        $args['where']['upper(_.status)'] = 'CLOSE';
        $args['join_receiver'] = true;
        $sql = $this->queryArchived($id_user, $args);
        $sql = "select count(*) as jumlah from ($sql) s1";
        return $this->db->query($sql)->row();
    }

    function queryReceived($args) {
        $this->load->helper('myquery');
        $args['join_receiver'] = true;
        $q = myquery($this->baseQuery($args), $args);
        $q->select('case when (rcv.is_view = 0 or rcv.is_view_comment = 0) then _.last_update else _.created_at end as last_update2', false);
        /* data belum dihapus oleh creator */
        $q->where_in('_.is_view', [0,1]);
        /* data belum dihapus oleh receiver*/
        $q->where('rcv.is_closed', '0');
        // return $q->get()->result();
        $sql = $q->get_compiled_select();
        return $sql;
    }

    function querySent($args) {
        $this->load->helper('myquery');
        $args['join_receiver'] = false;
        $q = myquery($this->baseQuery($args), $args);
        // return $q->get()->result();
        $q  ->select('0 as is_view_rcv')
            ->select('case when _.is_view = 0 then _.last_update else _.created_at end as last_update2', false);
        /* data belum dihapus oleh creator */
        $q->where_in('_.is_view', [0,1]);
        $sql = $q->get_compiled_select();
        return $sql;
    } 

    function queryArchived($id_user, $args) {
        $argsReceived = $args;
        $argsReceived['where']['rcv.id_receiver'] = $id_user;

        $argsSent = $args;
        $argsSent['where']['_.from_id'] = $id_user;

        $sqlReceived = $this->queryReceived($argsReceived);
        $sqlSent = $this->querySent($argsSent);

        $sql = "$sqlReceived  \n\n\nunion  \n\n\n$sqlSent";

        return $sql;
    }

    function baseQuery($args=[]){
        if(isset($args['join_receiver']) == false) {
            $args['join_receiver'] = false;
        }
        if(isset($args['join_reply']) == false) {
            $args['join_reply'] = true;
        }

        $qReply = '';
        if($args['join_reply'] == true) {
            $q = $this->db->from('monthlyreports_reply _')
                ->select('count(*) as jumlah', false)
                ->select('GROUP_CONCAT(_.message_reply) as reply_all', false)
                ->select([
                    'id_monthlyreports'
                ])
                ->group_by('id_monthlyreports');
            $qReply = $q->get_compiled_select();
        }
        $r = $this->db->from("monthlyreports _")
            ->join('users usr', 'usr.id_user = _.from_id', 'left')
            ->select([
                '_.id',
                '_.title',
                '_.date',
                '_.is_view',
                '_.from_id',
                '_.status',
                '_.kategori',
                '_.description',
                'usr.name',
                'usr.call_name',
                'usr.username',
                'usr.image'
            ]);
        if($args['join_reply'] == true) {
            $r->join("($qReply) rply", 'rply.id_monthlyreports = _.id', 'left')
                ->select([
                    'coalesce(rply.jumlah, 0) as jumlah_reply'
                ]);
        }

        if($args['join_receiver'] == true) {
            $r->join('monthlyreports_receiver rcv', 'rcv.id_monthlyreports = _.id', 'left')
                ->select('case when (rcv.is_view = 0 or rcv.is_view_comment = 0) then 0 else 1 end as is_view_rcv', false);
        }
        return $r;
    }

    function getReceived($id_user, $args) {
        $args['where']['rcv.id_receiver'] = $id_user;
        $args['where']['upper(_.status)'] = 'OPEN';
        $args['join_receiver'] = true;
        $limit = [];
        if(isset($args['limit'])) {
            $limit = $args['limit'];
            unset($args['limit']);
        }
        $sql = $this->queryReceived($args);
        $q = myquery($this->db->from("($sql) s1"), ['limit' => $limit, 'order' => [['last_update2', 'desc'], ['id', 'desc']]]);
        return $q->get()->result();
        // echo $q->get_compiled_select();
    }

    function getSent($id_user, $args) {
        $args['where']['_.from_id'] = $id_user;
        $args['where']['upper(_.status)'] = 'OPEN';
        $limit = [];
        if(isset($args['limit'])) {
            $limit = $args['limit'];
            unset($args['limit']);
        }

        $sql = $this->querySent($args);
        $q = myquery($this->db->from("($sql) s1"), ['limit' => $limit, 'order' => [['last_update2', 'desc'], ['id', 'desc']]]);
        return $q->get()->result();
        // echo $q->get_compiled_select();
    }

    function getArchived($id_user, $args) {
        $id_user = intval($id_user);
        $args['where']['upper(_.status)'] = 'CLOSE';
        $args['join_receiver'] = true;
        $limit = [];
        if(isset($args['limit'])) {
            $limit = $args['limit'];
            unset($args['limit']);
        }
        
        $sql = $this->queryArchived($id_user, $args);
        $q = myquery($this->db->from("($sql) s1"), ['limit' => $limit, 'order' => [ ['last_update2', 'desc'], ['id', 'desc'] ]]);
        $sql = $q->get_compiled_select();

        // echo $sql . "\n\n\n";
        return $this->db->query($sql)->result();
    }

    function save($data) {
        $receivers = [];
        if(array_key_exists('receivers', $data)) {
            $receivers = $data['receivers'];
            unset($data['receivers']);

            if(is_array($receivers) == false) {
                $receivers = [ $receivers ];
            }
        }

        $departments = [];
        if(array_key_exists('departments', $data)) {
            $departments = $data['departments'];
            unset($data['departments']);

            if(is_array($departments) == false) {
                $departments = [];
            }
        }

        $this->load->model('restapi/user_model');
        
        if(count($departments) > 0) {
            $users = $this->user_model->many_user(['where_in' => ['dep.id_department' => $departments]]);
            foreach ($users as $key => $user) {
                $receivers[] = $user['id_user'];
            }
        }

        $bagian = [];
        if(array_key_exists('bagian', $data)) {
            $bagian = $data['bagian'];
            unset($data['bagian']);
            if(is_array($bagian) == false) {
                $bagian = [$bagian];
            }
            $users = $this->user_model->many_user(['where_in' => ['bg.id' => $bagian]]);
            foreach ($users as $key => $usr) {
                $receivers[] = $usr['id_user'];
            }
        }

        $temp = [];
        foreach ($receivers as $key => $id_user) {
            $id_user = trim($id_user);
            if(empty($id_user)) {
                continue;
            }
            if(in_array($id_user, $temp)) {
                continue;
            }
            $temp[] = $id_user;
        }

        $receivers = $temp;

        if(count($receivers) == 0 ) {
            return ['status' => false, 'message' => 'Penerima kosong'];
        }

        $this->db->insert('monthlyreports', $data);
        $id = $this->db->insert_id();
        $report = $this->one_report(['_.id' => $id]);
        if($report == null) {
            return ['status' => false, 'message' => 'Terjadi kesalahan saat menyimpan data'];
        }

        $added = [];
        foreach ($receivers as $key => $rcv) {
            if(in_array($rcv, $added)) {
                continue;
            }

            $added[] = $rcv;
            
            $this->db->insert('monthlyreports_receiver', [
                'id_monthlyreports' => $id,
                'id_receiver' => $rcv,
                'is_view' => 0,
                'is_view_comment' => 0,
                'is_closed' => 0
            ]);
        }
		
		$this->load->model('restapi/user_model');
		$sender = $this->user_model->one_user(['where' => ['_.id_user' => $report->from_id]]);
		$sender_name = '';
		$position_name = '';
		$branch_name = '';
		if($sender != null) {
			$sender_name = $sender['name'];
			$position_name = $sender['position_name'];
			$branch_name = $sender['branch_name'];
		}
		
        $this->load->library('Notify_lib');
        $nl = new Notify_lib();
        $nl->send(
            'New Monthly Report',
            //$report->author->name . ' has sent new Monthly Report',
			$sender_name . ' - '.$position_name.' ('.$branch_name.') has sent new Monthly Report',
            $report->from_id,
            $receivers,
            ['id' => $report->id],
            'monthlyreports',
            true
        );

        return ['status' => true, 'message' => 'Data monthlyreports berhasil disimpan', 'id' => $id];
    }

    function update_report($where, $data) {
        $target = $this->one_report(['where' => $where]);

        if($target == null) {
            return ['status' => false, 'message' => 'Report tidak ditemukan'];
        }

        $this->db->where(['id' => $target->id])->update('monthlyreports', $data);

        return ['status' => true, 'message' => 'Report berhasil diupdate', 'id' => $target->id];
    }

    function save_attachment($data) {
        $this->db->insert('monthlyreports_attachment', $data);
        $id = $this->db->insert_id();
        return ['status' => true, 'id' => $id, 'message' => 'Suksess'];
    }

    function many_report($args) {
        $this->load->helper('myquery');
        return myquery($this->baseQuery($args), $args)->get()->result();
    }

    function one_report($args) {
        $this->load->helper('myquery');
        $report = myquery($this->baseQuery($args), $args)->get()->row();

        if($report != null) {
            $report->receivers = $this->getReceivers($report->id);
            $report->author = $this->getAuthor($report->from_id);
            $report->attachments = $this->getAttachments($report->id);
            $report->replies = $this->getReplies($report->id);
        }

        return $report;
    }

    function getReplies($id_report) {
        // $q = $this->db->from('monthlyreports_reply _')
        //     ->join('users usr', 'usr.id_user = _.sender')
        //     ->select([
        //         '_.*',
        //         'usr.name',
        //         'usr.image',
        //         'usr.username'
        //     ])
        //     ->where(['_.id_monthlyreports' => $id_report]);
        // return $q->get()->result();
        $replies = $this->many_reply(['where' => ['_.id_monthlyreports' => $id_report]]);

        foreach ($replies as $key => $rep) {
            $rep->attachments = $this->db->from('monthlyreports_reply_attachment')->where(['id_monthlyreports_reply' => $rep->id])->get()->result();
            $replies[$key] = $rep;
        }

        return $replies;
    }

    function getAttachments($id_report) {
        $q = $this->db->from('monthlyreports_attachment') 
            ->where(['monthlyreports_id' => $id_report]);
        return $q->get()->result();
    }

    function getAuthor($id_user) {
        $q = $this->db->from('users')
            ->where(['id_user' => $id_user]);
        return $q->get()->row();
    }

    function getReceivers($id_report) {
        $q = $this->db->from('monthlyreports_receiver _')
            ->join('users usr', 'usr.id_user = _.id_receiver')
            ->join('position pos', 'pos.id_position = usr.id_position')
            ->join('departments dep', 'dep.id_department = pos.id_department')
            ->where(['id_monthlyreports' => $id_report])
            ->select([
                '_.*',
                'usr.image',
                'usr.name',
                'usr.call_name',
                'usr.username',
                'pos.name_position as position',
                'dep.name as department'
            ]);
        return $q->get()->result();
    }

    function hapus_report($where) {
        $targets = $this->many_report(['where' => $where]);

        if(count($targets) == 0) {
            return ['status' => false, 'message' => 'Data tidak ditemukan'];
        }

        foreach ($targets as $key => $report) {
            /* data dihapus dengan flag is_view=2*/
            $this->db->where(['id' => $report->id])->update('monthlyreports', ['is_view' => 2]);
            // $this->db->where(['monthlyreports_id' => $report->id])->delete('monthlyreports_attachment');
            // $this->db->where(['id_monthlyreports' => $report->id])->delete('monthlyreports_receiver');
            // $this->db->where(['id_monthlyreports' => $report->id])->delete('monthlyreports_reply');
            // // $this->db->where(['id_monthlyreports' => $report->id])->delete('monthlyreports_reply_attachment');
            // $this->db->where(['id' => $report->id])->delete('monthlyreports');
        }

        return ['status' => true, 'message' => 'Data berhasil dihapus'];
    }

    function baseQueryReply($args = []) {
        $q = $this->db->from('monthlyreports_reply _')
            ->join('users usr', 'usr.id_user = _.sender')
            ->select([
                '_.*',
                'usr.name',
                'usr.username',
                'usr.call_name',
                'usr.image'
            ]);
        return $q;
    }

    function many_reply($args = []) {
        $this->load->helper('myquery');
        return myquery($this->baseQueryReply($args), $args)->get()->result();
    }

    function one_reply($args = []) {
        $this->load->helper('myquery');
        return myquery($this->baseQueryReply($args), $args)->get()->row();
    }

    function save_reply($data) {
        if(isset($data['id_monthlyreports']) == false) {
            return ['status' => false, 'message' => 'id monthlyreports diperlukan'];
        }
        if(isset($data['sender']) == false) {
            return ['status' => false, 'message' => 'sender diperlukan'];
        }

        $report = $this->one_report(['where' => ['_.id' => $data['id_monthlyreports']]]);

        if($report == null) {
            return ['status' => false, 'message' => 'Report tidak ditemukan'];
        }

        $this->db->insert('monthlyreports_reply', $data);
        $id = $this->db->insert_id();

        if($id == null) {
            return ['status' => false, 'message' => 'Terjadi kesalahan saat menyimpan reply'];
        }

        $reply = $this->one_reply(['where' => ['_.id' => $id]]);

        $receiversReport = $this->getReceivers($report->id);
        $receiversNotif = [];

        foreach ($receiversReport as $key => $rcv) {
            if($rcv->id_receiver == $data['sender']) {
                continue;
            }
            $receiversNotif[] = $rcv->id_receiver;
        }

        if($report->from_id == $data['sender']) {} else {
            $receiversNotif[] = $report->from_id;
        }
		
		$this->load->model('restapi/user_model');
		$sender = $this->user_model->one_user(['where' => ['_.id_user' => $data['sender']]]);
		$sender_name = '';
		$position_name = '';
		$branch_name = '';
		if($sender != null) {
			$sender_name = $sender['name'];
			$position_name = $sender['position_name'];
			$branch_name = $sender['branch_name'];
		}
		
        $this->load->library('Notify_lib');
        $nl = new Notify_lib();
        $nl->send(
            'New Montyly Report Reply',
            //$reply->name . ' has sent new Montyly Report Reply',
			$sender_name . ' - '.$position_name.' ('.$branch_name.') has sent new Montyly Report Reply',
            $data['sender'],
            $receiversNotif,
            ['id' => $report->id],
            'monthlyreports',
            true
        );

        return ['status' => true, 'message' => 'Reply berhasil disimpan', 'id' => $id];
    }

    function update_reply($where, $data) {
        $target = $this->one_reply(['where' => $where]);

        if($target == null) {
            return ['status' => false, 'message' => 'Reply tidak ditemukan'];
        }

        $this->db->where(['id' => $target->id])->update('monthlyreports_reply', $data);

        return ['status' => true, 'message' => 'Reply berhasil diupdate', 'id' => $target->id];
    }

    function hapus_reply($where) {
        $targets = $this->many_reply(['where' => $where]);

        if(count($targets) == 0) {
            return ['status' => false, 'message' => 'Reply tidak ditemukan'];
        }

        foreach ($targets as $key => $reply) {
            $this->db->where(['id_monthlyreports_reply' => $reply->id])->delete('monthlyreports_reply_attachment');
            $this->db->where(['id' => $reply->id])->delete('monthlyreports_reply');
        }

        return ['status' => true, 'message' => 'Reply berhasil dihapus'];
    }

    function save_reply_attachment($data) {
        $this->db->insert('monthlyreports_reply_attachment', $data);
        $id = $this->db->insert_id();

        return ['status' => true, 'message' => 'Attachment Reply berhasil disimpan', 'id' => $id];
    }

    function hapus_reply_attachment($where){
        $targets = $this->db->from('monthlyreports_reply_attachment')->where($where)->get()->result();
        if(count($targets) == 0) {
            return ['status' => false, 'message' => 'Data tidak ditemukan'];
        }

        foreach ($targets as $key => $att) {
            $this->db->where(['id' => $att->id])->delete('monthlyreports_reply_attachment');
        }

        return ['status' => true, 'message' => 'Attachment berhasil dihapus'];
    }

    function mark_read($id_report, $id_user, $report = null) {
        if($report == null) {
            $report = $this->one_report(['where' => ['_.id' => $id_report]]);
        }
        if($report == null) {
            return ['status' => false, 'message' => 'Report is null'];
        }

        $this->load->library('Notify_lib');
        $nl = new Notify_lib();
        if($report->from_id == $id_user) {
            // $this->update_report(['id' => $report->id], ['is_view' => 1]);
            $this->db->where(['id' => $report->id])->where_in('is_view', [0,1])->update('monthlyreports', ['is_view' => 1]);
        } 
        // else {
        $this->db->where(['id_receiver' => $id_user, 'id_monthlyreports' => $report->id, 'is_closed' => 0])->update('monthlyreports_receiver',['is_view' => 1, 'is_view_comment' => 1]);
        // }
        $nl->mark_read($id_user, 'monthlyreports', $report->id);

        return ['status' => true, 'message' => 'Berhasil menandai telah dibaca'];
    }

    function auto_close($id_report, $report = null) {
        if($report==null) {
            $report = $this->one_report(['where' => ['_.id' => $id_report]]);
        }
        if($report == null) {
            return ['status' => false, 'message' => 'report is null'];
        }

        if(strtolower($report->status) == 'close') {
            return ['status' => false,'message' => 'status report sudah ter-close'];
        }

        return ['status' => true, 'message' => 'Berhasil di close'];
    }

    function recent_report($id_user) {
        $id_user = intval($id_user);
        $type = "Monthly Report";
        $sql = "    SELECT r.id, '$type Reply' as type_name, r.from_id as sender_id, r.last_update as last_update, 
                        r.is_view
                    from monthlyreports r
                    where r.from_id = '$id_user' and r.is_view = 0 and UPPER(r.status) = 'OPEN'

                    union

                    Select r.id, '$type' as type_name, r.from_id as sender_id, r.last_update as last_update, 
                        case when rcv.is_view = 0 or rcv.is_view_comment = 0 then 0 else 1 end as is_view
                    from monthlyreports_receiver rcv
                    join monthlyreports r on rcv.id_monthlyreports = r.id
                    where rcv.id_receiver = '$id_user' and UPPER(r.status) = 'OPEN' ";
        return $sql;
    }

}