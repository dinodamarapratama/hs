<?php
defined("BASEPATH") or exit("No Direct Script Access Allowed");

/**
 *
 */
class M_masterinventaris extends CI_model
{
    public function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function get_where($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $result = $this->db->delete($table);
        return $result;
    }

    function delete_invenit_sub_kategori($where)
    {
        $del = $this->db->delete("master_inventaris_it_subkategori", $where);
        return ["status" => true, "message" => "Success"];
    }

    public function get_all_inven()
    {
        $this->db->select("inven.*,brch.branch_name");
        $this->db->from("master_inventaris_ga inven");
        $this->db->join(
            "branch brch",
            "brch.branch_id = inven.branch_id",
            "left"
        );
        return $this->db->get();
    }

    public function get_all_relations($table)
    {
        return $this->db->get($table);
    }

    public function edit_data_masterinven($where, $table)
    {
        $this->db->select("inven.*");
        $this->db->from("master_inventaris_ga inven");
        $this->db->where($where);
        return $this->db->get();
    }

    function many_invenit($args = [])
    {
        $this->load->helper("myquery");
        if ($args == null) {
            $args = [];
        }
        if (isset($args["where"]) == false) {
            $args["where"] = [];
        } else {
            $t = "";
            foreach ($args["where"] as $v) {
                $t = $t == "" ? $v : $v . " AND " . $t;
            }
            //print_r($t);

            $args["where"] = $t;
        }
        if(isset($args["limit"]) == false) {
            $args["limit"] = [];
        }
        if (isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if (isset($args["order"]) == false) {
            $args["order"] = [];
        }

        $result = [];
        $row = $this->baseQueryInvenit($args)
            ->where($args["where"])
            ->limit($args["limit"], $args["start"])
            ->get()
            ->result_array();
        $result = array_merge($result, ["rows" => $row]);
        $result["total"] = $this->baseQueryInvenit($args)
            ->where($args["where"])
            ->get()
            ->num_rows();

        return $result;
    }

    function many_invenitCategory($args = [])
    {
        $this->load->helper("myquery");
        if ($args == null) {
            $args = [];
        }
        if (isset($args["where"]) == false) {
            $args["where"] = [];
        } else {
            $t = "";
            foreach ($args["where"] as $v) {
                $t = $t == "" ? $v : $v . " AND " . $t;
            }

            $args["where"] = $t;
        }
        // if(isset($args["limit"]) == false) {
        //     $args["limit"] = [];
        // }
        if (isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if (isset($args["order"]) == false) {
            $args["order"] = [];
        }

        $result = [];
        $row = $this->baseQueryInvenit($args)
            ->where($args["where"])
            // ->limit($args["limit"], $args["start"])
            ->get()
            ->result_array();
        $result = array_merge($result, ["rows" => $row]);
        $result["total"] = $this->baseQueryInvenit($args)
            ->where($args["where"])
            ->get()
            ->num_rows();

        return $result;
    }

    function baseQueryInvenit($args = [])
    {
        return $this->db
            ->from("master_inventaris_it _")
            ->join("branch br", "br.branch_id = _.branch_id",'left')
            ->join("users usr", "usr.id_user = _.creator_id")
            //   ->join("master_inventaris_it_subkategori m_invt_it_s","m_invt_it_s.id_subkategori = _.sub_kategori" ,'left')
            ->order_by($args["sort"], $args["order"])
            ->select([
                "_.*",
                "br.branch_name",
                "usr.name as creator_name",
                // "m_invt_it_s.subkategori as subkategori",
                // "m_invt_it_s.kategori as _kategori"
            ]);
    }

    function add_input_invenit($data)
    {
        $this->db->insert("master_inventaris_it", $data);
        $id = $this->db->insert_id();

        return ["status" => true, "message" => "Success"];
    }

    function update_input_invenit($where, $data)
    {
        $this->db->where($where)->update("master_inventaris_it", $data);
        return ["status" => true, "message" => "Success"];
    }

    function delete_input_invenit($where)
    {
        $del = $this->db->delete("master_inventaris_it", $where);
        return ["status" => true, "message" => "Success"];
    }

    function delete_input_invenit_history($where, $data)
    {
        $this->db->where($where)->update("master_inventaris_it_history", $data);
        return ["status" => true, "message" => "Success"];
    }

    function delete_input_itreports($where, $data)
    {
        $this->db->where($where)->update("itreports", $data);
        return ["status" => true, "message" => "Success"];
    }

    // End Inven IT

    function many_invenga($args = [])
    {
        $this->load->helper("myquery");
        if ($args == null) {
            $args = [];
        }
        if (isset($args["where"]) == false) {
            $args["where"] = [];
        } else {
            $t = "";
            foreach ($args["where"] as $v) {
                $t = $t == "" ? $v : $v . " AND " . $t;
            }
            //print_r($t);

            $args["where"] = $t;
        }
        if (isset($args["limit"]) == false) {
            $args["limit"] = [];
        }
        if (isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if (isset($args["order"]) == false) {
            $args["order"] = [];
        }

        $result = [];
        $row = $this->baseQueryInvenga($args)
            ->where($args["where"])
            ->limit($args["limit"], $args["start"])
            ->get()
            ->result_array();
        $result = array_merge($result, ["rows" => $row]);
        $result["total"] = $this->baseQueryInvenga($args)
            ->where($args["where"])
            ->get()
            ->num_rows();

        return $result;
    }

    function baseQueryInvenga($args = [])
    {
        return $this->db
            ->from("master_inventaris_ga _")
            ->join("branch br", "br.branch_id = _.branch_id")
            ->join("users usr", "usr.id_user = _.creator_id")
            ->order_by("_.id", "desc")
            ->select([
                "_.*",
                "br.branch_name as branch",
                "usr.name as creator_name",
            ])
            ->order_by($args["sort"], $args["order"]);
    }

    function add_input_invenga($data)
    {
        $this->db->insert("master_inventaris_ga", $data);
        $id = $this->db->insert_id();

        return ["status" => true, "message" => "Success"];
    }

    function update_input_invenga($where, $data)
    {
        $this->db->where($where)->update("master_inventaris_ga", $data);
        return ["status" => true, "message" => "Success"];
    }

    function delete_input_invenga($where)
    {
        $del = $this->db->delete("master_inventaris_ga", $where);
        return ["status" => true, "message" => "Success"];
    }

    function detail($id)
    {
        $inven_it = $this->db
            ->where("id", $id)
            ->get("master_inventaris_it")
            ->row();

        $this->db->select(
            "users.*, position.name_position as position, position.id_department,branch.*,branch.branch_name as branch"
        );
        $this->db->join(
            "position",
            "position.id_position=users.id_position",
            "left"
        );
        $this->db->join("branch", "branch.branch_id=users.branch_id");

        $inven_it->author = $this->db
            ->where("users.id_user", $inven_it->creator_id)
            ->get("users")
            ->row();

        $inven_it->branch = $this->db
            ->where("branch.branch_id", $inven_it->branch_id)
            ->get("branch")
            ->row();

        $inven_it->receivers = $this->getReceivers($complaint->id);

        $this->db->select("users.*");
        $this->db->select(
            "position.name_position as position, position.id_department"
        );
        $this->db->select("generalreports_reply.*");
        $this->db->select("users.name as reply_name");
        $this->db->join("users", "generalreports_reply.sender=users.id_user");
        $this->db->join(
            "position",
            "position.id_position=users.id_position",
            "left"
        );
        $this->db->where(
            "generalreports_reply.id_generalreports",
            $generalreports->id
        );
        $inven_it->reply = $this->db->get("generalreports_reply")->result();

        return $inven_it;
    }

    function getReceivers($id)
    {
        $q = $this->db
            ->from("generalreports_receiver _")
            ->join("users usr", "usr.id_user = _.id_receiver", "left")
            ->join("position p", "p.id_position = usr.id_position", "left")
            ->join(
                "departments dep",
                "dep.id_department = p.id_department",
                "left"
            )
            ->where(["_.id_generalreports" => $id])
            ->select([
                "_.is_view",
                "usr.name",
                "usr.username",
                "p.name_position as position",
                "dep.name as department",
                "usr.id_user",
            ]);
        return $q->get()->result();
    }

    function many_invenit_history($args = [])
    {
        $this->load->helper("myquery");
        if ($args == null) {
            $args = [];
        }
        if (isset($args["where"]) == false) {
            $args["where"] = [];
        } else {
            $t = "";
            foreach ($args["where"] as $v) {
                $t = $t == "" ? $v : $v . " AND " . $t;
            }
            //print_r($t);

            $args["where"] = $t;
        }
        // if(isset($args["limit"]) == false) {
        //     $args["limit"] = [];
        // }
        if (isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if (isset($args["order"]) == false) {
            $args["order"] = [];
        }

        $result = [];
        $row = $this->baseQueryInvenitHistory($args)
            ->where($args["where"])
            ->limit($args["limit"], $args["start"])
            ->get()
            ->result_array();
        $result = array_merge($result, ["rows" => $row]);
        $result["total"] = $this->baseQueryInvenitHistory($args)
            ->where($args["where"])
            ->get()
            ->num_rows();

        return $result;
    }

    function baseQueryInvenitHistory($args = [])
    {
        return $this->db
            ->from("master_inventaris_it_history _")
            ->join("itreports it", "it.id = id_itreports")
            ->join("users usr", "usr.id_user = _.creator_id")
            ->join(
                "master_inventaris_it_subkategori m_invt_it_s",
                "m_invt_it_s.id_subkategori = _.id_subkategori",
                "left"
            )
            ->order_by($args["sort"], $args["order"])
            ->select([
                "_.*",
                "it.title as title",
                "m_invt_it_s.subkategori as subkategori",
                "usr.name as creator_name",
            ]);
    }

    function many_invenit_report($args = [])
    {
        $this->load->helper("myquery");
        if ($args == null) {
            $args = [];
        }
        if (isset($args["where"]) == false) {
            $args["where"] = [];
        } else {
            $t = "";
            foreach ($args["where"] as $v) {
                $t = $t == "" ? $v : $v . " AND " . $t;
            }
            //print_r($t);

            $args["where"] = $t;
        }
        // if(isset($args["limit"]) == false) {
        //     $args["limit"] = [];
        // }
        if (isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if (isset($args["order"]) == false) {
            $args["order"] = [];
        }

        $result = [];
        $row = $this->baseQueryInvenitReport($args)
            ->where($args["where"])
            ->limit($args["limit"], $args["start"])
            ->get()
            ->result_array();
        $result = array_merge($result, ["rows" => $row]);
        $result["total"] = $this->baseQueryInvenitReport($args)
            ->where($args["where"])
            ->get()
            ->num_rows();

        return $result;
    }

    function baseQueryInvenitReport($args = [])
    {
        return $this->db
            ->from("master_inventaris_it_history _")
            ->join("master_inventaris_it mit", "mit.kategori= kategori")
            ->join("branch br", "br.branch_id= mit.branch_id")
            ->join("users usr", "usr.id_user = _.creator_id")
            ->order_by($args["sort"], $args["order"])
            ->select([
                "_.*",
                "kategori",
                "mit.id as id_invt",
                "mit.branch_id",
                "br.branch_name",
                "nama_inventaris",
                "nomer_inventaris",
                "usr.name as creator_name",
            ]);
    }

    function many_invenit_report_master($args = [])
    {
        $this->load->helper("myquery");
        if ($args == null) {
            $args = [];
        }
        if (isset($args["where"]) == false) {
            $args["where"] = [];
        } else {
            $t = "";
            foreach ($args["where"] as $v) {
                $t = $t == "" ? $v : $v . " AND " . $t;
            }

            $args["where"] = $t;
        }
        if (isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if (isset($args["order"]) == false) {
            $args["order"] = [];
        }

        $result = [];
        $row = $this->baseQueryInvenitReportMaster($args)
            ->where($args["where"])
            ->limit($args["limit"], $args["start"])
            ->get()
            ->result_array();
        $result = array_merge($result, ["rows" => $row]);
        $result["total"] = $this->baseQueryInvenitReportMaster($args)
            ->where($args["where"])
            ->get()
            ->num_rows();

        return $result;
    }

    function baseQueryInvenitReportMaster($args = [])
    {
        return $this->db
            ->from("master_inventaris_it_history _")
            ->join("master_inventaris_it mit", "mit.id = _.id_inventaris")
            ->join("branch br", "br.branch_id = mit.branch_id")
            ->join("users usr", "usr.id_user = _.creator_id")
            ->join("itreports it", "it.id = _.id_itreports")
            ->join("master_inventaris_it_subkategori mit_s","mit_s.id_subkategori = _.id_subkategori")
            ->order_by($args["sort"], $args["order"])
            ->select([
                "_.*",
                "mit.id as id_invt",
                "mit.branch_id",
                "br.branch_name",
                "mit.nama_inventaris",
                "mit.nomer_inventaris",
                "mit_s.subkategori",
                "usr.name as creator_name",
                "it.status",
                "it.to_branch",
            ]);
    }
}
