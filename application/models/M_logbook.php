<?php

class M_logbook extends CI_Model {

    public function get_all_data($param)
    {
        $this->db->select('L.*, L.id as logbook_id,LM.*,LK.*,LM.jumlah as jumlah_masuk,LK.jumlah as jumlah_keluar,JL.nama_jenis,B.branch_name, S.nama_sumber');
        $this->db->from('logbook_btiga L');
        $this->db->join('jenis_limbah JL', 'L.jenis_limbah_id = JL.id', 'left');
        $this->db->join('limbah_masuk LM', 'LM.logbook_id = L.id', 'inner');
        $this->db->join('limbah_keluar LK', 'LK.logbook_id = L.id', 'inner');
        $this->db->join('branch B', 'L.branch_id = B.branch_id', 'left');
        $this->db->join('sumber_limbah S', 'LM.sumber = S.sumber_limbah_id', 'left');
        $this->db->where($param);
        return $this->db->get();
    }

    public function get_all_jenis($param){
        $this->db->select('*');
        $this->db->from('jenis_limbah');
        $this->db->where($param);
        return $this->db->get()->result();;
    }

    public function get_data_limbah($table,$key){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where(['logbook_id' => $key]);
        return $this->db->get()->result();;
    }

    public function insert_data($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function delete_data($key){
        $where = array('id' => $key);
        $q = $this->db->delete('logbook_btiga', $where);
            
        return $q;
    }

    public function hapus($table,$param){
        $where = $param;
        $q = $this->db->delete($table, $where);
            
        return $q;
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $q = $this->db->update($table, $data);
        return $q;
    }

    public function last_query_db(){
        return $this->db->last_query();
    }
}