<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_ijin extends CI_model
{

    public function last_query_db(){
        return $this->db->last_query();
    }
    
    
    public function get_one_ijin($id)

    {
        $this->db->select('ijin.*, branch.*, ijin_subgroup.*, ijin_group.*, ijin_code.*, reminder.*,bag_urus_ijin.*,pengurusan.*');
        $this->db->from('ijin');
        $this->db->join('branch', 'ijin.branch_id = branch.branch_id', 'left');
        $this->db->join('ijin_subgroup', 'ijin.ijin_subgroup_id = ijin_subgroup.ijin_subgroup_id', 'left');
        $this->db->join('ijin_group', 'ijin_subgroup.ijin_group_id = ijin_group.ijin_group_id', 'left');
        $this->db->join('ijin_code', 'ijin.ijin_code_id = ijin_code.ijin_code_id', 'left');
        $this->db->join('reminder', 'ijin.reminder_id = reminder.reminder_id', 'left');
        $this->db->join('bag_urus_ijin', 'ijin.bag_urus_ijin_id = bag_urus_ijin.bag_urus_ijin_id', 'left');
        $this->db->join('pengurusan', 'ijin.pengurusan_id = pengurusan.pengurusan_id', 'left');
        $this->db->where('ijin.ijin_id', $id);
        return $this->db->get();
    }
    
    public function get_all_ijin($user_id = null, $branch_id = null, $id_bagian = null, $start = null, $length = null)

    {
        $this->db->select('ijin.*, branch.*, ijin_subgroup.*, ijin_group.*, ijin_code.*, reminder.*,bag_urus_ijin.*,pengurusan.*');
        $this->db->from('ijin');
        $this->db->join('branch', 'ijin.branch_id = branch.branch_id', 'left');
        $this->db->join('ijin_subgroup', 'ijin.ijin_subgroup_id = ijin_subgroup.ijin_subgroup_id', 'left');
        $this->db->join('ijin_group', 'ijin_subgroup.ijin_group_id = ijin_group.ijin_group_id', 'left');
        $this->db->join('ijin_code', 'ijin.ijin_code_id = ijin_code.ijin_code_id', 'left');
        $this->db->join('reminder', 'ijin.reminder_id = reminder.reminder_id', 'left');
        $this->db->join('bag_urus_ijin', 'ijin.bag_urus_ijin_id = bag_urus_ijin.bag_urus_ijin_id', 'left');
        $this->db->join('pengurusan', 'ijin.pengurusan_id = pengurusan.pengurusan_id', 'left');
        if( !in_array($id_bagian, array(21,22,29)) ){
            if(!empty($branch_id)){
                $this->db->where('ijin.branch_id', $branch_id);
            }else{
                $this->db->where('ijin.creator_id', $user_id);
            }
        }
        if ($start != null & $length != null ) {
            $this->db->limit($length, $start);
        }
        return $this->db->get();
        // echo $this->db->get_compiled_select();
        // exit();
    }


    public function get_all_ijin_code()
    {
        $this->db->select('ijin_code.*, ijin_subgroup.*, ijin_group.*');
        $this->db->from('ijin_code');
        $this->db->join('ijin_subgroup', 'ijin_code.ijin_subgroup_id = ijin_subgroup.ijin_subgroup_id');
        $this->db->join('ijin_group', 'ijin_subgroup.ijin_group_id = ijin_group.ijin_group_id');
        return $this->db->get();
    }

    public function get_all_branch($filter=[])
    {
        $this->db->select('branch.*');
        $this->db->from('branch');
        $this->db->where($filter);
        return $this->db->get();
    }

    public function get_all_ijin_group()
    {
        $this->db->select('ijin_group.*');
        $this->db->from('ijin_group');
        return $this->db->get();
    }

    public function get_all_ijin_sub_group()
    {
        $this->db->select('ijin_subgroup.*, ijin_group.*');
        $this->db->from('ijin_subgroup');
        $this->db->join('ijin_group', 'ijin_subgroup.ijin_group_id = ijin_group.ijin_group_id');
        return $this->db->get();
    }

    public function get_all_pengurusan()
    {
        $this->db->select('pengurusan.*, branch.*');
        $this->db->from('pengurusan');
        $this->db->join('branch', 'pengurusan.branch_id = branch.branch_id');
        return $this->db->get();
    }

    public function get_all_reminder()
    {
        $this->db->select('reminder.*');
        $this->db->from('reminder');
        return $this->db->get();
    }

    public function get_all_bag_urus_ijin()
    {
        $this->db->select('bag_urus_ijin.*');
        $this->db->from('bag_urus_ijin');
        return $this->db->get();
    }

    public function edit_data_ijin($where, $table)
    {
        $this->db->select('ijin.*, ijin_subgroup.*');
        $this->db->from('ijin');
        $this->db->join('ijin_subgroup', 'ijin.ijin_subgroup_id = ijin_subgroup.ijin_subgroup_id','left');
        $this->db->where($where);
        return $this->db->get();
    }

    public function get_history_renew($where)
    {
        $this->db->select('ijin_renew.*, ijin_subgroup.*');
        $this->db->from('ijin_renew');
        $this->db->join('ijin_subgroup', 'ijin_renew.ijin_subgroup_id = ijin_subgroup.ijin_subgroup_id','left');
        $this->db->where($where);
        $this->db->order_by('ijin_renew_id','DESC');
        return $this->db->get();
    }

    public function get_renew($where){
        $this->db->select('ijin_renew.*, ijin_subgroup.*, ijin.ijin_id');
        $this->db->from('ijin_renew');
        $this->db->join('ijin', 'ijin_renew.ijin_parent_id = ijin.ijin_id','left');
        $this->db->join('ijin_subgroup', 'ijin_renew.ijin_subgroup_id = ijin_subgroup.ijin_subgroup_id','left');
        $this->db->where($where);
        return $this->db->get();
    }

    public function edit_data_ijin_code($where, $table)
    {
        $this->db->select('ijin_code.*, ijin_subgroup.*, ijin_group.*');
        $this->db->from('ijin_code');
        $this->db->join('ijin_subgroup', 'ijin_code.ijin_subgroup_id = ijin_subgroup.ijin_subgroup_id');
        $this->db->join('ijin_group', 'ijin_subgroup.ijin_group_id = ijin_group.ijin_group_id');
        $this->db->where($where);
        return $this->db->get();
    }

    public function edit_data_branch($where, $table)
    {
        $this->db->select('branch.*');
        $this->db->from('branch');
        $this->db->where($where);
        return $this->db->get();
    }

    public function edit_data_reminder($where, $table)
    {
        $this->db->select('reminder.*');
        $this->db->from('reminder');
        $this->db->where($where);
        return $this->db->get();
    }

    public function edit_data_bag_urus_ijin($where, $table)
    {
        $this->db->select('bag_urus_ijin.*');
        $this->db->from('bag_urus_ijin');
        $this->db->where($where);
        return $this->db->get();
    }

    public function edit_data_pengurusan($where, $table)
    {
        $this->db->select('pengurusan.*');
        $this->db->from('pengurusan');
        $this->db->where($where);
        return $this->db->get();
    }

    public function edit_data_ijin_group($where, $table)
    {
        $this->db->select('ijin_group.*');
        $this->db->from('ijin_group');
        $this->db->where($where);
        return $this->db->get();
    }

    public function edit_data_ijin_subgroup($where, $table)
    {
        $this->db->select('ijin_subgroup.*');
        $this->db->from('ijin_subgroup');
        $this->db->where($where);
        return $this->db->get();
    }

    public function get_all_relations($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function update_data_lama($ijin_parent_id, $table)
    {
        $this->db->where(array('ijin_id' => $ijin_parent_id));
        $this->db->update($table, array('status' => 'RENEW'));
    }

    public function insert_data_renew($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $result = $this->db->delete($table);
        return $result;
    }

    function detail($id)
    {
        $ijin = $this->db->where('ijin_id', $id)->get('ijin')->row();

        $this->db->select('users.*, position.name_position as position, position.id_department,branch.*,branch.branch_name as branch');
        $this->db->join('position', 'position.id_position=users.id_position', 'left');
        $this->db->join('branch', 'branch.branch_id=users.branch_id');

        $ijin->author = $this->db->where('users.id_user', $ijin->creator_id)->get('users')->row();



       
        $this->db->select('ijin.*');
        
        $this->db->join('ijin_code', 'ijin_code.ijin_code_id=ijin.ijin_code_id', 'left');
       // $this->db->where('generalreports_reply.id_generalreports', $generalreports->id);
        $ijin->rel = $this->db->get('ijin')->result();

        return $ijin;
    }


    public function get_data_cabang($args = [])
{
    $this->load->helper("myquery");
    if($args == null) {
        $args = [];
    }
    if(isset($args["where"]) == false) {
        $args["where"] = [];
    }
    else{
        $t = '';
        foreach($args["where"] as $v){
            $t = ($t=='')?$v:$v.' AND '.$t;
        }
        //print_r($t);
        
        $args["where"] = $t;
    }
    if(isset($args["limit"]) == false) {
        $args["limit"] = [];
    }
    if(isset($args["start"]) == false) {
        $args["start"] = [];
    }
    if(isset($args["order"]) == false) {
        $args["order"] = [];
    }
    
    $result = array();
    $row = $this->db->select('b.*')->from('branch b')->order_by($args["sort"],$args["order"])->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();    
    $result = array_merge($result, ['rows' => $row]);
    $result['total'] = $this->db->select('b.*')->from('branch b')->where($args["where"])->get()->num_rows();
    
    return $result;
}


    function baseQueryIjin($args = []) {
        return $this->db->from("ijin _")
            ->join("ijin_code ic","ic.ijin_code_id = _.ijin_code_id")
            ->join("ijin_subgroup is","is.ijin_subgroup_id = _.ijin_subgroup_id")
            ->join("ijin_group ig","ig.ijin_group_id = is.ijin_group_id")
            ->group_by("_.ijin_code_id")
            ->select([
                "_.*",
                "ic.ijin_code_name as nama_ijin" ,
                "is.ijin_subgroup_name as subgroup",
                "is.ijin_group_id",
                "ig.ijin_group_name as group",
            ])->order_by($args["sort"],$args["order"]);
    }


    function get_data_ijin($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }

        else {

            $t = '';
            foreach($args["where"] as $v) {
                $t = ($t=='')?$v:$v.' AND '.$t;
            }

            $args["where"] = $t;
        }

        if(isset($args["order"]) == false) {
            $args["order"] = [];
        }

        return $this->baseQueryIjin($args)->where($args["where"])->get()->result_array();
    }


    function baseQueryData($args = []) {
        return $this->db->from("ijin _")
            ->join("reminder remin","remin.reminder_id = _.reminder_id")
            ->join("pengurusan urus","urus.pengurusan_id = _.pengurusan_id")
            ->join("bag_urus_ijin bag","bag.bag_urus_ijin_id = _.bag_urus_ijin_id")
         //   ->group_by("_.ijin_code_id")
            ->select([
                "_.*",
                "DATE_FORMAT(ijin_reg,'%d/%m/%Y') AS ijin_reg",
                "DATE_FORMAT(ijin_exp,'%d/%m/%Y') AS ijin_exp",
                "remin.reminder_name",
                "urus.alamat_pengurusan",
                "bag.bag_urus_ijin_name",

            ])->order_by($args["sort"],$args["order"]);
    }


    function get_data_dokumen($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }

        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }

        else {

            $t = '';
            foreach($args["where"] as $v) {
                $t = ($t=='')?$v:$v.' AND '.$t;
            }

            $args["where"] = $t;
        }

        
        if(isset($args["order"]) == false) {
            $args["order"] = [];
        }

        return $this->baseQueryData($args)->where($args["where"])->get()->result_array();
    }
   


}