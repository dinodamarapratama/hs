<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_itreports extends CI_model
{

    public function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $this->db->where($table);
    }

    public function insert_batch($table, $data)
    {
        $this->db->insert_batch($table, $data);
    }

    public function get_data_like($where, $colum, $table)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->like($colum, $where);
        return $this->db->get();
    }

    public function get_data_join()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('position', 'users.id_position = position.id');
        return $this->db->get();
    }

    public function get_sent_to($id)
    {
        $this->db->select('name, (select name_position from position where users.id_position = position.id_position) as name_position, id_receiver');
        $this->db->from('itreports_receiver');
        $this->db->join('users', 'itreports_receiver.id_receiver = users.id_user');
        //$this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('itreports_receiver.id_receiver', $id);

        return $this->db->get();

    }

	public function get_all_sent_to($id)
    {
        $this->db->select('name, (select name_position from position where users.id_position = position.id_position) as name_position, id_receiver');
        $this->db->from('itreports_receiver');
        $this->db->join('users', 'itreports_receiver.id_receiver = users.id_user');
        //$this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('itreports_receiver.id_itreports', $id);

        return $this->db->get();

    }

    public function get_sent_count($id)
    {
        $this->db->from('itreports_receiver');
        $this->db->where('itreports_receiver.id_itreports', $id);

        return $this->db->get()->num_rows();
    }

    public function get_read_count($id)
    {
        $this->db->from('itreports_receiver');
        $this->db->where('itreports_receiver.id_itreports', $id);
        $this->db->where('itreports_receiver.is_view', '1');

        return $this->db->get()->num_rows();
    }

    public function get_user_read($id)
    {
        $this->db->select('name, name_position, id_receiver, is_view');
        $this->db->from('itreports_receiver');
        $this->db->join('users', 'itreports_receiver.id_receiver = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('itreports_receiver.id_itreports', $id);

        return $this->db->get();

    }


    public function total_data_received($id /*id user*/, $id_itreports = null, $position = null, $search = null, $startDate = null,$endDate = null, $args)
    {
        $this->db->select('itreports.id');
        $this->db->from('itreports');
        //$this->db->join('itreports_receiver', 'itreports_receiver.id_itreports = itreports.id');
        $this->db->join('users', 'itreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch to_branch', 'to_branch.branch_id = itreports.to_branch');
        if (!empty($id_itreports)) {
            //$this->db->where('itreports_receiver.id_itreports', $id_itreports);
			$this->db->where('itreports.id', $id_itreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($startDate)) {
            $this->db->where('itreports.date >=', $startDate);
        }

         if (!empty($endDate)) {
            $this->db->where('itreports.date <=', $endDate);
        }

        if (!empty($search)) {
			$this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('itreports.kategori',$search,'both');
            $this->db->or_like('itreports.title',$search,'both');
            $this->db->or_like('itreports.date',$search,'both');
            $this->db->or_like('itreports.pc_des',$search,'both');
            $this->db->or_like('itreports.printer_des',$search,'both');
            $this->db->or_like('itreports.alat_des',$search,'both');
            $this->db->or_like('itreports.jaringan_des',$search,'both');
            $this->db->or_like('itreports.server_des',$search,'both');
            $this->db->or_like('itreports.prog_des',$search,'both');
            $this->db->or_like('itreports.backup_des',$search,'both');
            $this->db->or_like('itreports.dll',$search,'both');
            //$this->db->limit(1);
			$this->db->group_end();
        }

		$this->db->where('itreports.id in (select itreports_receiver.id_itreports from itreports_receiver where id_receiver = '.$id.' and itreports_receiver.is_closed<>"1")', null);
		
        //$this->db->where('itreports_receiver.id_receiver', $id);
        //$this->db->where('itreports_receiver.is_closed <>', '1');
        $this->db->where('upper(itreports.status)', 'Open');
        // $this->db->where('itreports.status <>', 'Close');
        return $this->db->get();
    }

    public function total_data_sent($id, $id_itreports = null, $position = null, $search = null,$startDate = null , $endDate = null, $args)
    {
        $this->db->select('itreports.id');
        $this->db->from('itreports');
        //$this->db->join('itreports_receiver', 'itreports_receiver.id_itreports = itreports.id');
        $this->db->join('users', 'itreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch to_branch', 'to_branch.branch_id = itreports.to_branch');
        if (!empty($id_itreports)) {
            //$this->db->where('itreports_receiver.id_itreports', $id_itreports);
			$this->db->where('itreports.id', $id_itreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($startDate)) {
            $this->db->where('itreports.date >=', $startDate);
        }

         if (!empty($endDate)) {
            $this->db->where('itreports.date <=', $endDate);
        }
        if (!empty($search)) {
			$this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('itreports.kategori',$search,'both');
            $this->db->or_like('itreports.title',$search,'both');
            $this->db->or_like('itreports.date',$search,'both');
            $this->db->or_like('itreports.pc_des',$search,'both');
            $this->db->or_like('itreports.printer_des',$search,'both');
            $this->db->or_like('itreports.alat_des',$search,'both');
            $this->db->or_like('itreports.jaringan_des',$search,'both');
            $this->db->or_like('itreports.server_des',$search,'both');
            $this->db->or_like('itreports.prog_des',$search,'both');
            $this->db->or_like('itreports.backup_des',$search,'both');
            $this->db->or_like('itreports.dll',$search,'both');
            //$this->db->limit(1);
			$this->db->group_end();
        }
        $this->db->where('itreports.from_id', $id);
        $this->db->where('itreports.id_draft', '0');
        $this->db->where('upper(itreports.status)', 'OPEN');
        /* data belum dihapus oleh creator */
        $this->db->where_in('itreports.is_view', [0,1]);
        /* data belum dihapus oleh receiver */
        //$this->db->where('itreports_receiver.is_closed', '0');
        return $this->db->get();
    }

    public function total_data_archived($id, $id_itreports = null, $position = null, $search = null,$startDate = null , $endDate = null, $args)
    {
        $this->db->select('itreports.id');
        $this->db->from('itreports');
       // $this->db->join('itreports_receiver', 'itreports_receiver.id_itreports = itreports.id');
        $this->db->join('users', 'itreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch to_branch', 'to_branch.branch_id = itreports.to_branch');
        if (!empty($id_itreports)) {
            //$this->db->where('itreports_receiver.id_itreports', $id_itreports);
			$this->db->where('itreports.id', $id_itreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($startDate)) {
            $this->db->where('itreports.date >=', $startDate);
        }

         if (!empty($endDate)) {
            $this->db->where('itreports.date <=', $endDate);
        }
        if (!empty($search)) {
            $this->db->group_start();

            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('itreports.kategori',$search,'both');
            $this->db->or_like('itreports.title',$search,'both');
            $this->db->or_like('itreports.date',$search,'both');
            $this->db->or_like('itreports.pc_des',$search,'both');
            $this->db->or_like('itreports.printer_des',$search,'both');
            $this->db->or_like('itreports.alat_des',$search,'both');
            $this->db->or_like('itreports.jaringan_des',$search,'both');
            $this->db->or_like('itreports.server_des',$search,'both');
            $this->db->or_like('itreports.prog_des',$search,'both');
            $this->db->or_like('itreports.backup_des',$search,'both');
            $this->db->or_like('itreports.dll',$search,'both');
            //$this->db->limit(1);

            $this->db->group_end();
        }
        $this->db->where('upper(itreports.status)', 'CLOSE');
        // $this->db->where('itreports.status <>', 'Open');
        // $this->db->where('itreports.is_view <>', '1');
        $this->db->group_start();
            $this->db->or_group_start();
                $this->db->where('itreports.from_id', $id);
                /* data belum dihapus oleh creator*/
                $this->db->where_in('itreports.is_view', [0,1]);
            $this->db->group_end();
            /* data belum dihapus oleh receiver*/
            /*$this->db->or_group_start();
                $this->db->where('itreports_receiver.id_receiver', $id);
                $this->db->where('itreports_receiver.is_closed', '0');
            $this->db->group_end();*/
			
			$this->db->or_group_start();
                $this->db->where('itreports.id in (select id_itreports from itreports_receiver where itreports_receiver.id_itreports = itreports.id and itreports_receiver.id_receiver = '.$id.' and itreports_receiver.is_closed="0") ', null);
            $this->db->group_end();
			
        $this->db->group_end();
        return $this->db->get();
    }

    public function total_data_draft($id, $id_itreports = null, $position = null, $search = null,$args)
    {
        $this->db->select('itreports.id');
        $this->db->from('itreports');
        //$this->db->join('itreports_receiver', 'itreports_receiver.id_itreports = itreports.id','left');
        $this->db->join('users', 'itreports.from_id = users.id_user','left');
        $this->db->join('position', 'users.id_position = position.id_position','left');
        $this->db->join('branch', 'itreports.to_branch = branch.branch_id','left');
        if (!empty($id_itreports)) {
            //$this->db->where('itreports_receiver.id_itreports', $id_itreports);
			$this->db->where('itreports.id', $id_itreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($startDate)) {
            $this->db->where('itreports.date >=', $startDate);
        }

         if (!empty($endDate)) {
            $this->db->where('itreports.date <=', $endDate);
        }
        if (!empty($search)) {
			$this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('itreports.kategori',$search,'both');
            $this->db->or_like('itreports.title',$search,'both');
            $this->db->or_like('itreports.date',$search,'both');
            $this->db->or_like('itreports.pc_des',$search,'both');
            $this->db->or_like('itreports.printer_des',$search,'both');
            $this->db->or_like('itreports.alat_des',$search,'both');
            $this->db->or_like('itreports.jaringan_des',$search,'both');
            $this->db->or_like('itreports.server_des',$search,'both');
            $this->db->or_like('itreports.prog_des',$search,'both');
            $this->db->or_like('itreports.backup_des',$search,'both');
            $this->db->or_like('itreports.dll',$search,'both');
            //$this->db->limit(1);
			$this->db->group_end();
        }
        
        $this->db->where('itreports.from_id', $id);
        // $this->db->where('itreports.is_view <>', '1');
        $this->db->where('itreports.id_draft <>', '0');
        $this->db->where('itreports.id_draft', '1');		
        return $this->db->get();
    }


    public function get_data_received($id /*id user*/, $id_itreports = null, $position = null, $search = null, $startDate = null,$endDate = null, $args)
    {
        $this->db->select('itreports.*,itreports.status as status,users.*,position.*,to_branch.*,
            (select itreports_receiver.is_view from itreports_receiver where itreports_receiver.id_itreports = itreports.id and itreports_receiver.id_receiver = '.$id.' group by itreports_receiver.id_receiver) as `read`,
			itreports.status as status');//itreports_receiver.id_itreports, itreports_receiver.id_receiver,itreports_receiver.is_view as read,
        $this->db->from('itreports');
        //$this->db->join('itreports_receiver', 'itreports_receiver.id_itreports = itreports.id');
        $this->db->join('users', 'itreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch to_branch', 'to_branch.branch_id = itreports.to_branch');
        if (!empty($id_itreports)) {
            //$this->db->where('itreports_receiver.id_itreports', $id_itreports);
			$this->db->where('itreports.id', $id_itreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($startDate)) {
            $this->db->where('itreports.date >=', $startDate);
        }

         if (!empty($endDate)) {
            $this->db->where('itreports.date <=', $endDate);
        }

        if (!empty($search)) {
			$this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('itreports.kategori',$search,'both');
            $this->db->or_like('itreports.title',$search,'both');
            $this->db->or_like('itreports.date',$search,'both');
            $this->db->or_like('itreports.pc_des',$search,'both');
            $this->db->or_like('itreports.printer_des',$search,'both');
            $this->db->or_like('itreports.alat_des',$search,'both');
            $this->db->or_like('itreports.jaringan_des',$search,'both');
            $this->db->or_like('itreports.server_des',$search,'both');
            $this->db->or_like('itreports.prog_des',$search,'both');
            $this->db->or_like('itreports.backup_des',$search,'both');
            $this->db->or_like('itreports.dll',$search,'both');
            //$this->db->limit(1);
			$this->db->group_end();
        }

		$this->db->where('itreports.id in (select itreports_receiver.id_itreports from itreports_receiver where id_receiver = '.$id.' and itreports_receiver.is_closed<>"1")', null);
        //$this->db->where('itreports_receiver.id_receiver', $id);
        //$this->db->where('itreports_receiver.is_closed <>', '1');
        $this->db->where('upper(itreports.status)', 'Open');
        // $this->db->where('itreports.status <>', 'Close');
        //$this->db->order_by('itreports_receiver.id', 'desc');
		$this->db->order_by('itreports.id', 'desc');
		
		$limit = 10;
		$offset = 0;
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		if(isset($args['offset'])) {
			$offset = $args['offset'];
			unset($args['offset']);
		}
		
		
		$this->db->limit($limit,$offset);
		
        return $this->db->get();
    }


    public function get_data_sent($id, $id_itreports = null, $position = null, $search = null,$startDate = null , $endDate = null, $args)
    {
        $this->db->select('itreports.*,users.*,position.*,to_branch.*,
            itreports.status as status,itreports.id as id_itreports');//itreports_receiver.id_itreports,itreports_receiver.id_receiver,
        $this->db->from('itreports');
        //$this->db->join('itreports_receiver', 'itreports_receiver.id_itreports = itreports.id');
        $this->db->join('users', 'itreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch to_branch', 'to_branch.branch_id = itreports.to_branch');
        if (!empty($id_itreports)) {
            //$this->db->where('itreports_receiver.id_itreports', $id_itreports);
			$this->db->where('itreports.id', $id_itreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($startDate)) {
            $this->db->where('itreports.date >=', $startDate);
        }

         if (!empty($endDate)) {
            $this->db->where('itreports.date <=', $endDate);
        }
        if (!empty($search)) {
			$this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('itreports.kategori',$search,'both');
            $this->db->or_like('itreports.title',$search,'both');
            $this->db->or_like('itreports.date',$search,'both');
            $this->db->or_like('itreports.pc_des',$search,'both');
            $this->db->or_like('itreports.printer_des',$search,'both');
            $this->db->or_like('itreports.alat_des',$search,'both');
            $this->db->or_like('itreports.jaringan_des',$search,'both');
            $this->db->or_like('itreports.server_des',$search,'both');
            $this->db->or_like('itreports.prog_des',$search,'both');
            $this->db->or_like('itreports.backup_des',$search,'both');
            $this->db->or_like('itreports.dll',$search,'both');
            //$this->db->limit(1);
			$this->db->group_end();
        }
        $this->db->where('itreports.from_id', $id);
        $this->db->where('itreports.id_draft', '0');
        $this->db->where('upper(itreports.status)', 'OPEN');
        /* data belum dihapus oleh creator */
        $this->db->where_in('itreports.is_view', [0,1]);
        /* data belum dihapus oleh receiver */
        
		//$this->db->where('itreports_receiver.is_closed', '0');
        
		// $this->db->where('itreports.status <>', 'Close');
        // $this->db->where('itreports.id_draft <>', '1');
        // $this->db->where('itreports.is_view <>', '1');
        $this->db->order_by('itreports.id', 'desc');
		$limit = 10;
		$offset = 0;
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		if(isset($args['offset'])) {
			$offset = $args['offset'];
			unset($args['offset']);
		}
		
		
		$this->db->limit($limit,$offset);
        return $this->db->get();
    }

    public function get_data_archived($id, $id_itreports = null, $position = null, $search = null,$startDate = null , $endDate = null, $args)
    {
        $this->db->select('itreports.*,users.*,position.*,to_branch.*,
            itreports.status as status,itreports.id as id_itreports');//itreports_receiver.id_itreports,itreports_receiver.id_receiver,
        $this->db->from('itreports');
        //$this->db->join('itreports_receiver', 'itreports_receiver.id_itreports = itreports.id');
        $this->db->join('users', 'itreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch to_branch', 'to_branch.branch_id = itreports.to_branch');
        if (!empty($id_itreports)) {
            //$this->db->where('itreports_receiver.id_itreports', $id_itreports);
			$this->db->where('itreports.id', $id_itreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($startDate)) {
            $this->db->where('itreports.date >=', $startDate);
        }

         if (!empty($endDate)) {
            $this->db->where('itreports.date <=', $endDate);
        }
        if (!empty($search)) {
            $this->db->group_start();

            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('itreports.kategori',$search,'both');
            $this->db->or_like('itreports.title',$search,'both');
            $this->db->or_like('itreports.date',$search,'both');
            $this->db->or_like('itreports.pc_des',$search,'both');
            $this->db->or_like('itreports.printer_des',$search,'both');
            $this->db->or_like('itreports.alat_des',$search,'both');
            $this->db->or_like('itreports.jaringan_des',$search,'both');
            $this->db->or_like('itreports.server_des',$search,'both');
            $this->db->or_like('itreports.prog_des',$search,'both');
            $this->db->or_like('itreports.backup_des',$search,'both');
            $this->db->or_like('itreports.dll',$search,'both');
            //$this->db->limit(1);

            $this->db->group_end();
        }
        $this->db->where('upper(itreports.status)', 'CLOSE');
        // $this->db->where('itreports.status <>', 'Open');
        // $this->db->where('itreports.is_view <>', '1');
        $this->db->group_start();
            $this->db->or_group_start();
                $this->db->where('itreports.from_id', $id);
                /* data belum dihapus oleh creator*/
                $this->db->where_in('itreports.is_view', [0,1]);
            $this->db->group_end();
            /* data belum dihapus oleh receiver*/
            /*$this->db->or_group_start();
                $this->db->where('itreports_receiver.id_receiver', $id);
                $this->db->where('itreports_receiver.is_closed', '0');
            $this->db->group_end();*/
			
			$this->db->or_group_start();
                $this->db->where('itreports.id in (select id_itreports from itreports_receiver where itreports_receiver.id_itreports = itreports.id and itreports_receiver.id_receiver = '.$id.' and itreports_receiver.is_closed="0") ', null);
            $this->db->group_end();
			
        $this->db->group_end();
        $this->db->order_by('itreports.id', 'desc');
		
		$limit = 10;
		$offset = 0;
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		if(isset($args['offset'])) {
			$offset = $args['offset'];
			unset($args['offset']);
		}
		
		
		$this->db->limit($limit,$offset);
		
        return $this->db->get();
    }

    public function get_data_draft($id, $id_itreports = null, $position = null, $search = null,$args)
    {
        $this->db->select('itreports.*,users.*,position.*,branch.*,
            itreports.status as status,itreports.id as id_itreports');//itreports_receiver.id_itreports,itreports_receiver.id_receiver,
        $this->db->from('itreports');
        //$this->db->join('itreports_receiver', 'itreports_receiver.id_itreports = itreports.id','left');
        $this->db->join('users', 'itreports.from_id = users.id_user','left');
        $this->db->join('position', 'users.id_position = position.id_position','left');
        $this->db->join('branch', 'itreports.to_branch = branch.branch_id','left');
        if (!empty($id_itreports)) {
            //$this->db->where('itreports_receiver.id_itreports', $id_itreports);
			$this->db->where('itreports.id', $id_itreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($startDate)) {
            $this->db->where('itreports.date >=', $startDate);
        }

         if (!empty($endDate)) {
            $this->db->where('itreports.date <=', $endDate);
        }
        if (!empty($search)) {
			$this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('itreports.kategori',$search,'both');
            $this->db->or_like('itreports.title',$search,'both');
            $this->db->or_like('itreports.date',$search,'both');
            $this->db->or_like('itreports.pc_des',$search,'both');
            $this->db->or_like('itreports.printer_des',$search,'both');
            $this->db->or_like('itreports.alat_des',$search,'both');
            $this->db->or_like('itreports.jaringan_des',$search,'both');
            $this->db->or_like('itreports.server_des',$search,'both');
            $this->db->or_like('itreports.prog_des',$search,'both');
            $this->db->or_like('itreports.backup_des',$search,'both');
            $this->db->or_like('itreports.dll',$search,'both');
            //$this->db->limit(1);
			$this->db->group_end();
        }
        
        $this->db->where('itreports.from_id', $id);
        // $this->db->where('itreports.is_view <>', '1');
        $this->db->where('itreports.id_draft <>', '0');
        $this->db->where('itreports.id_draft', '1');
        $this->db->order_by('itreports.id', 'desc');
		
		$limit = 10;
		$offset = 0;
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		if(isset($args['offset'])) {
			$offset = $args['offset'];
			unset($args['offset']);
		}
		
		
		$this->db->limit($limit,$offset);
		
        return $this->db->get();
    }


    public function get_detail($id, $id_itreports = null, $position = null, $search = null,$startDate = null , $endDate = null, $args)
    {
        $this->db->select('itreports.*,users.*,position.*,to_branch.*,
            itreports.status as status,itreports.id as id_itreports');//itreports_receiver.id_itreports,itreports_receiver.id_receiver,
        $this->db->from('itreports');
        //$this->db->join('itreports_receiver', 'itreports_receiver.id_itreports = itreports.id');
        $this->db->join('users', 'itreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch to_branch', 'to_branch.branch_id = itreports.to_branch');
        if (!empty($id_itreports)) {
            //$this->db->where('itreports_receiver.id_itreports', $id_itreports);
			$this->db->where('itreports.id', $id_itreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($startDate)) {
            $this->db->where('itreports.date >=', $startDate);
        }

         if (!empty($endDate)) {
            $this->db->where('itreports.date <=', $endDate);
        }
        if (!empty($search)) {
			$this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('itreports.kategori',$search,'both');
            $this->db->or_like('itreports.title',$search,'both');
            $this->db->or_like('itreports.date',$search,'both');
            $this->db->or_like('itreports.pc_des',$search,'both');
            $this->db->or_like('itreports.printer_des',$search,'both');
            $this->db->or_like('itreports.alat_des',$search,'both');
            $this->db->or_like('itreports.jaringan_des',$search,'both');
            $this->db->or_like('itreports.server_des',$search,'both');
            $this->db->or_like('itreports.prog_des',$search,'both');
            $this->db->or_like('itreports.backup_des',$search,'both');
            $this->db->or_like('itreports.dll',$search,'both');
            //$this->db->limit(1);
			$this->db->group_end();
        }

        $this->db->order_by('itreports.id', 'desc');
		$limit = 10;
		$offset = 0;
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		if(isset($args['offset'])) {
			$offset = $args['offset'];
			unset($args['offset']);
		}
		
		// $this->db->where('itreports.from_id', $id);
		$this->db->limit($limit,$offset);
        return $this->db->get();
    }


    public function total_data($id, $id_itreports = null, $position = null, $search = null,$startDate = null , $endDate = null, $args)
    {
        $this->db->select('itreports.id');
        $this->db->from('itreports');
        //$this->db->join('itreports_receiver', 'itreports_receiver.id_itreports = itreports.id');
        $this->db->join('users', 'itreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch to_branch', 'to_branch.branch_id = itreports.to_branch');
        if (!empty($id_itreports)) {
            //$this->db->where('itreports_receiver.id_itreports', $id_itreports);
			$this->db->where('itreports.id', $id_itreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($startDate)) {
            $this->db->where('itreports.date >=', $startDate);
        }

         if (!empty($endDate)) {
            $this->db->where('itreports.date <=', $endDate);
        }
        if (!empty($search)) {
			$this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('itreports.kategori',$search,'both');
            $this->db->or_like('itreports.title',$search,'both');
            $this->db->or_like('itreports.date',$search,'both');
            $this->db->or_like('itreports.pc_des',$search,'both');
            $this->db->or_like('itreports.printer_des',$search,'both');
            $this->db->or_like('itreports.alat_des',$search,'both');
            $this->db->or_like('itreports.jaringan_des',$search,'both');
            $this->db->or_like('itreports.server_des',$search,'both');
            $this->db->or_like('itreports.prog_des',$search,'both');
            $this->db->or_like('itreports.backup_des',$search,'both');
            $this->db->or_like('itreports.dll',$search,'both');
            //$this->db->limit(1);
			$this->db->group_end();
        }
        // $this->db->where('itreports.from_id', $id);
        return $this->db->get();
    }


    public function get_user_replay($id)
    {
        $this->db->select('*');
        $this->db->from('itreports');
        $this->db->join('itreports_reply', 'itreports.id = itreports_reply.id_itreports');
        $this->db->join('users', 'itreports_reply.sender = users.id_user');
        $this->db->join('branch', 'branch.branch_id = users.branch_id');
        $this->db->join('position', 'position.id_position = users.id_position');
        $this->db->where('itreports.id', $id);

        return $this->db->get();
    }

    public function get_to_id($id,$user_id)
    {
        $this->db->select('*');
        $this->db->from('itreports_receiver');
        $this->db->join('itreports', 'itreports.id = itreports_receiver.id_itreports');
        $this->db->where('itreports.id', $id);
        $this->db->where('itreports_receiver.id_receiver', $user_id);

        return $this->db->get();
    }

    public function get_from_id($id)
    {
        $this->db->select('*');
        $this->db->from('itreports_receiver');
        $this->db->join('itreports', 'itreports.id = itreports_receiver.id_itreports');
        $this->db->where('itreports.id', $id);

        return $this->db->get();
    }

    public function get_notification($id)
    {
        return $this->db->from("notifications")
            ->where([
                "to_id" => $id
            ])
            ->group_start()
                ->or_where(["read" => null])
                ->or_where(["read" => 0])
            ->group_end()
            // ->where(["anime" => "naruto"])
            ->get();

        // $this->db->select('*');
        // $this->db->from('notifications');
        // $this->db->where('to_id', $id);
        // $this->db->where('read', 0);
        // $this->db->where('read <>', 1);
        // $this->db->order_by('id', 'desc');

        // return $this->db->get();
    }


    public function get_user_hrd()
    {
        $this->db->select('id_user,name');
        $this->db->from('users');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('name_position','STAFF HRD');

        return $this->db->get();
    }

    public function testing($array)
    {
        return print("<pre>" . print_r($array, true) . "</pre>");
    }

    function getCountReceived($id_user, $args) {
        $args['where']['rcv.id_receiver'] = $id_user;
        $args['where']['upper(_.status)'] = 'OPEN';
        $q = $this->queryReceived($args);
        $sql = "select count(*) as jumlah from ($q) s1";
        return $this->db->query($sql)->row();
    }

    function getCountSent($id_user, $args) {
        $args['where']['_.from_id'] = $id_user;
        $args['where']['upper(_.status)'] = 'OPEN';
        $q = $this->querySent($args);
        $sql = "select count(*) as jumlah from ($q) s1";
        return $this->db->query($sql)->row();
    }

    function getCountArchived($id_user, $args) {
        $args['where']['upper(_.status)'] = 'CLOSE';
        $args['join_receiver'] = true;
        $sql = $this->queryArchived($id_user, $args);
        $sql = "select count(*) as jumlah from ($sql) s1";
        return $this->db->query($sql)->row();
    }

    function queryReceived($args) {
        $this->load->helper('myquery');
        $args['join_receiver'] = true;
        $q = myquery($this->baseQuery($args), $args);
        $q->select('case when (rcv.is_view = 0 or rcv.is_view_comment = 0) then _.last_update else _.created_at end as last_update2', false);
        /* data belum dihapus oleh creator */
        $q->where_in('_.is_view', [0,1]);
        /* data belum dihapus oleh receiver */
        $q->where('rcv.is_closed', '0');
        // return $q->get()->result();
        $sql = $q->get_compiled_select();
        return $sql;
    }

    function querySent($args) {
        $this->load->helper('myquery');
        $args['join_receiver'] = false;
        $q = myquery($this->baseQuery($args), $args);
        // return $q->get()->result();
        $q  ->select('0 as is_view_rcv')
            ->select('case when _.is_view = 0 then _.last_update else _.created_at end as last_update2', false);
        /* data belum dihapus oleh creator */
        $q->where_in('_.is_view', [0,1]);
        $sql = $q->get_compiled_select();
        return $sql;
    }

    

    function queryArchived($id_user, $args) {
        $argsReceived = $args;
        $argsReceived['where']['rcv.id_receiver'] = $id_user;

        $argsSent = $args;
        $argsSent['where']['_.from_id'] = $id_user;

        $sqlReceived = $this->queryReceived($argsReceived);
        $sqlSent = $this->querySent($argsSent);

        $sql = "$sqlReceived  \n\n\nunion  \n\n\n$sqlSent";

        return $sql;
    }

    function baseQuery($args=[]){
        if(isset($args['join_receiver']) == false) {
            $args['join_receiver'] = false;
        }
        if(isset($args['join_reply']) == false) {
            $args['join_reply'] = true;
        }

        $qReply = '';
        if($args['join_reply'] == true) {
            $q = $this->db->from('itreports_reply _')
                ->select('count(*) as jumlah', false)
                ->select('GROUP_CONCAT(_.message_reply) as reply_all', false)
                ->select([
                    'id_itreports'
                ])
                ->group_by('id_itreports');
            $qReply = $q->get_compiled_select();
        }
        $r = $this->db->from("itreports _")
            ->join('users usr', 'usr.id_user = _.from_id', 'left')
            ->join('branch b', 'b.branch_id = _.to_branch', 'left')
            ->select([
                '_.*',
                'usr.name',
                'usr.last_name',
                'usr.call_name',
                'usr.username',
                'usr.image',
                'b.branch_name as to_branch_name'
            ]);
        if($args['join_reply'] == true) {
            $r->join("($qReply) rply", 'rply.id_itreports = _.id', 'left')
                ->select([
                    'coalesce(rply.jumlah, 0) as jumlah_reply'
                ]);
        }

        if($args['join_receiver'] == true) {
            $r->join('itreports_receiver rcv', 'rcv.id_itreports = _.id', 'left')
                ->select('case when (rcv.is_view = 0 or rcv.is_view_comment = 0) then 0 else 1 end as is_view_rcv', false);
        }

        /*by default join in on branch & position */
		$r 	->join('branch br', 'br.branch_id = usr.branch_id', 'left')
			->join('position pos', 'pos.id_position = usr.id_position', 'left')
            ->join('bagian bag', 'bag.id = usr.id_bagian', 'left')
			->select([
					'br.branch_name','pos.id_position','pos.name_position','bag.name as bagian'
				]);
        return $r;
    }

    function getReceived($id_user, $args) {
        $args['where']['rcv.id_receiver'] = $id_user;
        $args['where']['upper(_.status)'] = 'OPEN';
        $args['join_receiver'] = true;
        $limit = [];
        if(isset($args['limit'])) {
            $limit = $args['limit'];
            unset($args['limit']);
        }
        $sql = $this->queryReceived($args);
        $q = myquery($this->db->from("($sql) s1"), ['limit' => $limit, 'order' => [['last_update2', 'desc'], ['id', 'desc']]]);
        return $q->get()->result();
        // echo $q->get_compiled_select();
    }

    function getSent($id_user, $args) {
        $args['where']['_.from_id'] = $id_user;
        $args['where']['upper(_.status)'] = 'OPEN';
        $limit = [];
        if(isset($args['limit'])) {
            $limit = $args['limit'];
            unset($args['limit']);
        }

        $sql = $this->querySent($args);
        $q = myquery($this->db->from("($sql) s1"), ['limit' => $limit, 'order' => [['last_update2', 'desc'], ['id', 'desc']]]);
        return $q->get()->result();
        // echo $q->get_compiled_select();
    }

    function getArchived($id_user, $args) {
        $id_user = intval($id_user);
        $args['where']['upper(_.status)'] = 'CLOSE';
        $args['join_receiver'] = true;
        $limit = [];
        if(isset($args['limit'])) {
            $limit = $args['limit'];
            unset($args['limit']);
        }

        $sql = $this->queryArchived($id_user, $args);
        $q = myquery($this->db->from("($sql) s1"), ['limit' => $limit, 'order' => [ ['last_update2', 'desc'], ['id', 'desc'] ]]);
        $sql = $q->get_compiled_select();

        // echo $sql . "\n\n\n";
        return $this->db->query($sql)->result();
    }

    function save($data) {
        $receivers = [];
        $departments = [];
        if(array_key_exists('receivers', $data)) {
            $receivers = $data['receivers'];
            unset($data['receivers']);

            if(is_array($receivers) == false) {
                $receivers = [];
            }
        }
        if(array_key_exists('departments', $data)) {
            $departments = $data['departments'];
            unset($data['departments']);

            if(is_array($departments) == false) {
                $departments = [];
            }
        }

        $this->load->model('restapi/user_model');

        if(count($departments) > 0) {
            $users = $this->user_model->many_user(['where_in' => ['dep.id_department' => $departments]]);
            foreach ($users as $key => $user) {
                $receivers[] = $user['id_user'];
            }
        }

        $bagian = [];
        if(array_key_exists('bagian', $data)) {
            $bagian = $data['bagian'];
            unset($data['bagian']);
            if(is_array($bagian) == false) {
                $bagian=[$bagian];
            }
            if(count($bagian) > 0){
                $users = $this->user_model->many_user(['where_in' => ['bg.id' => $bagian]]);
                foreach ($users as $key => $usr) {
                    $receivers[] = $usr['id_user'];
                }
            }

        }

        $temp = [];
        foreach ($receivers as $key => $id_user) {
            $id_user = trim($id_user);
            if(empty($id_user)) {
                continue;
            }
            if(in_array($id_user, $temp)) {
                continue;
            }
            $temp[] = $id_user;
        }
        $receivers = $temp;

        if(count($receivers) == 0 ) {
            return ['status' => false, 'message' => 'Penerima kosong'];
        }

        $this->db->insert('itreports', $data);
        $id = $this->db->insert_id();

        $report = $this->one_report(['where' => ['_.id' => $id]]);
        if($report == null) {
            return ['status' => false, 'message' => 'Terjadi kesalahan menyimpan report', 'id' => $id, 'report' => $report];
        }

        $added = [];
        foreach ($receivers as $key => $rcv) {
            if(in_array($rcv, $added)) {
                continue;
            }

            $added[] = $rcv;

            $this->db->insert('itreports_receiver', [
                'id_itreports' => $id,
                'id_receiver' => $rcv,
                'is_view' => 0,
                'is_view_comment' => 0,
                'is_closed' => 0
            ]);
        }

		$this->load->model('restapi/user_model');
		$sender = $this->user_model->one_user(['where' => ['_.id_user' => $report->from_id]]);
		$sender_name = '';
		$position_name = '';
		$branch_name = '';
		if($sender != null) {
			$sender_name = $sender['name'];
			$position_name = $sender['position_name'];
			$branch_name = $sender['branch_name'];
		}

        $this->load->library('Notify_lib');
        $nl = new Notify_lib();
        $nl->send(
            'New IT Report',
            //$report->author->name . ' has sent new IT Report',
			$sender_name . ' - '.$position_name.' ('.$branch_name.') has sent new IT Report',
            $report->from_id,
            $receivers,
            ['id' => $report->id],
            'itreports',
            true
        );

        return ['status' => true, 'message' => 'Data itreports berhasil disimpan', 'id' => $id];
    }

    function save_attachment($data) {
        $this->db->insert('itreports_attachment', $data);
        $id = $this->db->insert_id();
        return ['status' => true, 'id' => $id, 'message' => 'Suksess'];
    }

    function many_report($args) {
        $this->load->helper('myquery');
        return myquery($this->baseQuery($args), $args)->get()->result();
    }

    function one_report($args) {
        $this->load->helper('myquery');
        $report = myquery($this->baseQuery($args), $args)->get()->row();
        if($report != null) {
            $this->load->model('restapi/user_model');
            $report->receivers = $this->getReceivers($report->id);
            $report->author = $this->user_model->one_user(['where' => ['_.id_user' => $report->from_id]]);
            $report->attachments = $this->getAttachments($report->id);
            $report->replies = $this->getReplies($report->id);
        }

        return $report;
    }

    function update_report($where, $data) {
        $target = $this->one_report(['where' => $where]);

        if($target == null) {
            return ['status' => false, 'message' => 'Report tidak ditemukan'];
        }

        $this->db->where(['id' => $target->id])->update('itreports', $data);

        return ['status' => true, 'message' => 'Report berhasil diupdate', 'id' => $target->id];
    }

    function baseQueryReply($args = []) {
        $q = $this->db->from('itreports_reply _')
            ->join('users usr', 'usr.id_user = _.sender')
            ->join('position pos', 'pos.id_position = usr.id_position')
            ->join('bagian bag', 'bag.id = usr.id_bagian', 'left')
            ->select([
                '_.*',
                'usr.name',
                'usr.last_name',
                'usr.call_name',
                'usr.image',
                'usr.username',
                'pos.name_position as position',
                'bag.name as bagian'
            ]);
        return $q;
    }

    function getReplies($id_report) {
        $q = $this->baseQueryReply()
            ->where(['_.id_itreports' => $id_report]);
        $res = $q->get()->result();

        foreach ($res as $key => $r) {
            $r->message_reply = html_entity_decode($r->message_reply);
            $r->attachments = $this->get_reply_attachment($r->id);

            $res[$key] = $r;
        }

        return $res;
    }

    function getAttachments($id_report) {
        $q = $this->db->from('itreports_attachment')
            ->where(['itreports_id' => $id_report]);
        return $q->get()->result();
    }

    function getAuthor($id_user) {
        $q = $this->db->from('users')
            ->join('bagian bag', 'bag.id = users.id_bagian', 'left')
            ->where(['id_user' => $id_user]);
        return $q->get()->row();
    }

    

    function getReceivers($id_generalreports)
	{
		$q = $this->db->from("itreports_receiver dc")
        ->join('users usr', 'usr.id_user = dc.id_receiver')
        ->join('position pos', 'pos.id_position = usr.id_position')
        ->join('departments dep', 'dep.id_department = usr.id_department')
        ->join('bagian bag', 'bag.id = usr.id_bagian', 'left')
			->where([
				"dc.id_itreports" => $id_generalreports
			])
			->select([
				"dc.is_view",
				'dc.is_view_comment',
                'usr.id_user',
				'usr.image',
                'usr.name',
                'usr.last_name',
                'usr.call_name',
                'usr.username',
                'pos.name_position as position',
                'dep.name as department',
                'bag.name as bagian'
			])
			->get()->result_array();

		return $q;
	}

    function hapus_report($where) {
        $targets = $this->many_report(['where' => $where]);

        if(count($targets) == 0) {
            return ['status' => false, 'message' => 'Data tidak ditemukan'];
        }

        foreach ($targets as $key => $report) {
            /* data dihapus dengan flag is_view=2*/
            $this->db->where(['id' => $report->id])->update('itreports', ['is_view' => 2]);
            // $this->db->where(['itreports_id' => $report->id])->delete('itreports_attachment');
            // $this->db->where(['id_itreports' => $report->id])->delete('itreports_receiver');
            // $this->db->where(['id_itreports' => $report->id])->delete('itreports_reply');
            // $this->db->where(['id_itreports' => $report->id])->delete('itreports_reply_attachment');
            // $this->hapus_reply(['_.id_itreports' => $report->id]);
            // $this->db->where(['id' => $report->id])->delete('itreports');
        }

        return ['status' => true, 'message' => 'Data berhasil dihapus'];
    }

    function save_reply($data) {
        if(isset($data['id_itreports']) == false) {
            return ['status' => false, 'message' => 'id report di perlukan'];
        }
        if(isset($data['sender']) == false) {
            return ['status' => false, 'message' => 'sender diperlukan'];
        }
        $report = $this->one_report(['where' => ['_.id' => $data['id_itreports']]]);
        if($report == null) {
            return ['status' => false, 'message' => 'report tidak dapat ditemukan'];
        }
        $this->db->insert('itreports_reply', $data);
        $id = $this->db->insert_id();

        if($id == null) {
            return ['status' => false, 'message' => 'Terjadi kesalahan menyimpan reply'];
        }

        $reply = $this->one_reply(['where' => ['_.id' => $id]]);
        if($reply == null) {
            return ['status' => false, 'message' => 'Terjadi kesalahan menyimpan reply'];
        }

        // stay
        $receiversReport = $this->getReceivers($report->id);

        $receiversNotif = [];
        if($data['sender'] == $report->from_id) {

        } else {
            $receiversNotif[] = $report->from_id;
        }



        foreach ($receiversReport as $key => $rcv) {
            if($receiversReport[$key]['id_user']== $data['sender']) {
                $receiversNotif[] = $receiversReport[$key]['id_user'];
            }
            
        }
        //  echo json_encode($receiversNotif);

		$this->load->model('restapi/user_model');
		$sender = $this->user_model->one_user(['where' => ['_.id_user' => $reply->sender]]);
		$sender_name = '';
		$position_name = '';
		$branch_name = '';
		if($sender != null) {
			$sender_name = $sender['name'];
			$position_name = $sender['position_name'];
			$branch_name = $sender['branch_name'];
		}

        /* kirim notifikasi */
        $this->load->library('Notify_lib');
        $nl = new Notify_lib();
        $nl->send(
            'New Itreport Reply',
            //$reply->name . ' has send new Itreport reply',
			$sender_name . ' - '.$position_name.' ('.$branch_name.') has send new Itreport reply',
            $reply->sender,
            $receiversNotif,
            ['id' => $report->id],
            'itreports',
            true
        );
        return ['status' => true, 'message' => 'Reply disimpan', 'id' => $id];
    }

    function update_reply($where, $data) {
        $target = $this->one_reply(['where' => $where]);
        if($target == null) {
            return ['status' => false, 'message' => 'Reply tidak ditemukan'];
        }

        $this->db->where(['id' => $target->id])->update('itreports_reply', $data);

        return ['status' => true, 'message' => 'Reply berhasil diupdate', 'id' => $target->id];
    }

    function many_reply($args) {
        $this->load->helper('myquery');
        return myquery($this->baseQueryReply($args), $args)->get()->result();
    }

    function one_reply($args) {
        $this->load->helper('myquery');
        return myquery($this->baseQueryReply($args), $args)->get()->row();
    }

    function hapus_reply($where) {
        $targets = $this->many_reply(['where' => $where]);
        if(count($targets) == 0) {
            return ['status' => false, 'message' => 'Reply tidak ditemukan'];
        }

        foreach ($targets as $key => $reply) {
            $this->db->where(['id_itreports_reply' => $reply->id])->delete('itreports_reply_attachment');
            $this->db->where(['id' => $reply->id])->delete('itreports_reply');
        }

        return ['status' => true, 'message' => 'Reply berhasil dihapus'];
    }

    function save_reply_attachment($data) {
        $this->db->insert('itreports_reply_attachment', $data);
        $id = $this->db->insert_id();

        return ['status' => true, 'message' => '', 'id' => $id];
    }

    function get_reply_attachment($id_reply) {
        $q = $this->db->from('itreports_reply_attachment')
            ->where(['id_itreports_reply' => $id_reply]);
        return $q->get()->result();
    }

    function mark_read($id_report, $id_user, $report = null) {
        if($report == null) {
            $report = $this->one_report(['where' => ['_.id' => $id_report]]);
        }

        if($report == null) {
            return ['status' => false, 'message' => 'Report tidak ditemukan'];
        }

        if($report->from_id == $id_user) {
            $this->db->where(['id' => $report->id])->where_in('is_view', [0,1])->update('itreports', ['is_view' => 1]);
        }
        // else {
        $this->db->where(['id_itreports' => $report->id, 'id_receiver' => $id_user, 'is_closed'=>0])
            ->update('itreports_receiver', ['is_view' => 1, 'is_view_comment' => 1]);
        // }
        $this->load->library('Notify_lib');
        $nl = new Notify_lib();
        $nl->mark_read($id_user, 'itreports', $report->id);

        return ['status' => true, 'message' => 'Report berhasil ditandai sudah dibaca'];
    }

    function auto_close($id_report, $report = null) {
        if($report == null) {
            $report = $this->one_report(['where' => ['_.id' => $id_report]]);
        }
        if($report == null) {
            return ['status' => false, 'message' => 'report is null'];
        }
        if(strtolower($report->status) == 'close'){
            return ['status' => false, 'message' => 'report already closed'];
        }
        $everythingOk = $report->pc_des == '1' && $report->printer_des == '2' && $report->alat_des == '3'
                        && $report->alat_des == '4' && $report->jaringan_des == '5' && $report->server_des == '6';
        if($everythingOk == false) {
            return ['status' => false, 'message' => 'Not closed, there is some issues'];
        }
        $allHaveRead = true;
        foreach ($report->receivers as $key => $rcv) {
            if($rcv->is_view == '0' || $rcv->is_view_comment == '0') {
                $allHaveRead = false;
                break;
            }
        }

        if($allHaveRead == false) {
            return ['status' => false, 'message' => 'Some receivers have not read'];
        }

        $this->update_report(['_.id' => $report->id], ['status' => 'Close']);
        return ['status' => true, 'message' => 'Report has been closed'];
    }

    function recent_report($id_user) {
        $id_user = intval($id_user);
        $type = "IT Report";
        $sql = "    SELECT r.id, '$type Reply' as type_name, r.from_id as sender_id, r.last_update as last_update,
                        r.is_view
                    from itreports r
                    where r.from_id = '$id_user' and r.is_view = 0 and UPPER(r.status) = 'OPEN'

                    union

                    Select r.id, '$type' as type_name, r.from_id as sender_id, r.last_update as last_update,
                        case when rcv.is_view = 0 or rcv.is_view_comment = 0 then 0 else 1 end as is_view
                    from itreports_receiver rcv
                    join itreports r on rcv.id_itreports = r.id
                    where rcv.id_receiver = '$id_user' and UPPER(r.status) = 'OPEN' ";
        return $sql;
    }

    function detail($id)
    {
        $itreports = $this->db->where('id', $id)->get('itreports')->row();

        $this->db->select('users.*, position.name_position as position, position.id_department,branch.*,branch.branch_name as branch,users.last_name lastname');
        $this->db->join('position', 'position.id_position=users.id_position', 'left');
        $this->db->join('branch', 'branch.branch_id=users.branch_id');

        $itreports->author = $this->db->where('users.id_user', $itreports->from_id)->get('users')->row();

        $itreports->branch = $this->db->where('branch.branch_id', $itreports->branch_id)->get('branch')->row();

        $itreports->receivers = $this->getReceivers($itreports->id);

       // $itreports->report = $this->db->query('select * from itreports inner join itreports on gene');



        $this->db->select('users.*');
        $this->db->select('position.name_position as position, position.id_department');
        $this->db->select('itreports_reply.*');
        $this->db->select('users.name as reply_name');
        $this->db->join('users', 'itreports_reply.sender=users.id_user');
        $this->db->join('position', 'position.id_position=users.id_position', 'left');
        $this->db->where('itreports_reply.id_itreports', $itreports->id);
        $itreports->reply = $this->db->get('itreports_reply')->result();

      
        return $itreports;
    }

    

    function update_invenit_history($where, $data) {
        $this->db->where($where)->update("master_inventaris_it_history", $data);
        return ["status" => true, "message" => "Success"];
    }

}
