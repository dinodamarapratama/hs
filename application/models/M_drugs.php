<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');
/**
*
*/
class M_drugs extends CI_model
{
	public function edit_data($where, $table)
	{
		return $this->db->get_where($table, $where);
	}

	public function get_data($table)
	{
		return $this->db->get($table);
	}

	public function insert_data($data, $table)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	public function update_data($where, $data, $table)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}

	public function delete_data($where, $table)
	{
		$this->db->where($where);
		$result = $this->db->delete($table);
		return $result;
	}

	public function get_all_relations($table)
    {
        return $this->db->get($table);
    }

	public function get_karyawan($params=[], $rows=0, $offset=10,$all=0){
		$this->db->select(['users.id_user','users.name','users.last_name','users.CUTI','users.cuti_keseluruhan','branch.branch_name','bagian.name as nama_bagian','position.name_position','users.STATUS','users.TGLMASUK','users.id_position','users.id_bagian','users.branch_id']);
		$this->db->from('users');
		if (isset($params['in_param'])) {
		  foreach ($params['in_param'] as $key => $value) {
			  $this->db->where_in($key,$value);  
		  }
		  unset($params['in_param']);
		}
		
		if (isset($params['not_in_param'])) {
		 foreach ($params['not_in_param'] as $k => $v) {
			 $this->db->where_not_in($k,$v);  
		 }
		 unset($params['not_in_param']);
	   }
 
		$this->db->where($params);
		if ($all==0) {
		  $this->db->limit($rows, $offset);  
		}
		$this->db->order_by('name', 'ASC');
		$this->db->join('branch', 'users.branch_id = branch.branch_id','left');
		$this->db->join('bagian', 'users.id_bagian = bagian.id','left');
		$this->db->join('position', 'users.id_position = position.id_position','left');
		$query = $this->db->get();
		return $query->result_array();
	 }

	public function get_all_drugs($conds=[])
	{
		if(isset($conds["where"]) == false) {
			$conds["where"] = [];
		}
		if(isset($args["limit"]) == false) {
			$args["limit"] = [];
		}
		if(isset($args["start"]) == false) {
			$args["start"] = [];
		}
		if(isset($args["order"]) == false) {
			$args["order"] = [];
		}

		return $this->db
		->select('drugs.*,branch.branch_name,DATE_FORMAT(exp_date, "%d-%m-%Y") as exp_date')
		->join('branch','branch.branch_id=drugs.branch_id','left')
		->where($conds["where"])
		->order_by($conds["sort"],$conds["order"])
		->limit($conds["limit"], $conds["start"])
		->get('drugs')->result_array();
	}

	
	public function get_drugs($conds=[])
	{
		if(isset($conds["where"]) == false) {
			$conds["where"] = [];
		}
		return $this->db
		->select('drugs.*,branch.branch_name,DATE_FORMAT(exp_date, "%d-%m-%Y") as exp_date')
		->join('branch','branch.branch_id=drugs.branch_id','left')
		->where($conds["where"])
		->get('drugs')->result_array();
	}

	public function get_reminder(){
		/*
			- get the reminder notif & go for send mail to the 4,7
			- reminder hanya untuk area manager(4) & manager(7)
		*/
		$id_user_ = $this->session->userdata('id_user');
		$id_user = 57;
		// echo $id_user_;
		return $this->response_user($id_user);
	}

	public function response_user($id_user=0){
		$cnds=[
			'id_user' => $id_user
		];
		$rslt = $this->db->where($cnds)->get('users')->result();
		
		/* cek is area manager or branch manager */
		if (!empty($rslt)) { 
			/*
				- sent mail if expired, filtered by id branch, so sent to area manager & branch manager too
			*/
			$exp = $this->notif([
				'sent_mail'=> 0
			]);
			if (!empty($exp)) {
				$this->sendMail($exp);	
			}
			/*--------------------*/

			if ($rslt[0]->id_position==4) {
				$many_branch = $rslt[0]->many_branch;
				$cnds = [
					// 'sent_mail'=> 0,
					'branch.branch_id in('.$many_branch.')' => null
				];
				$expired = $this->notif($cnds);
				
				return $expired;
			}elseif ($rslt[0]->id_position==7) {
				$branch_id = $rslt[0]->branch_id;
				$cnds = [
					// 'sent_mail'=> 0,
					'branch.branch_id' => $branch_id
				];
				$expired = $this->notif($cnds);
				
				return $expired;
			}else{
				return [];
			}
		}else{
			return [];/*empty notif*/
		}
	}

	// ERROR UBAH KE QUERY QHERE
	public function notif($cnds=[]){
		return $this->db
		->select('drugs.*,branch.branch_name,DATE_FORMAT(exp_date, "%d-%m-%Y") as exp_date')
		->join('branch','branch.branch_id=drugs.branch_id','left')
		// ->where($cnds)
		// ->where('CURRENT_DATE() = exp_date',null)
		// ->or_where('stock',0)
		->where('(sent_mail = 0 AND CURRENT_DATE() = exp_date) OR (sent_mail = 0 AND stock = 0)',null)
		->get('drugs')->result();
	}

	public function sendMail($parseData=[]){
		/**/
		$ci =& get_instance();
		$ci->load->library('kirim_email');

		$mailBody = [];
		$tmp_obat = [];
		$rcv_email = [];
		foreach ($parseData as $key => $value) {
			
			if ($value->branch_id == 16) {
				/*sent to all member*/
				$conds = [
					'id_position in (4,7)' => null
				];
			}else{
				/*filter area & branch manager by branch_id*/
				$conds = [
					'id_position in (4,7)' => null,
					'or_where' => [
						'branch_id' => $value->branch_id,
						'many_branch like ' => '%'.$value->branch_id.'%'
					]
				];
			}

			$or_where=[];
			if (isset($conds['or_where']) && !empty($conds['or_where'])) {
				$or_where = $conds['or_where'];
				unset($conds['or_where']);
				$where_clause = $conds; 
			}
			$this->db->select('id_user,email');
			$this->db->where($where_clause);
			if (!empty($or_where)) {
				$this->db->group_start();
				foreach ($or_where as $field => $vvv) {
					$this->db->or_where($field,$vvv);
				}
				$this->db->group_end();
			}
			$rslt = $this->db->get('users')->result();
			foreach ($rslt as $x => $v) {
				$tmp_obat[$v->id_user][] = $value;
				$rcv_email[$v->id_user] = $v->email;
			}
			
		}

		
		$receiver_mails = [];
		
		foreach ($tmp_obat as $rcv_id => $vals) {
			$mailBody = '<h1>Reminder</h1>';
			$mailBody .= '<h3>Obat yang Expired atau Stoknya kosong</h2>';
			$mailBody .= '<table class="styled-table" style="border-collapse: collapse; margin: 25px 0; font-size: 0.9em; font-family: sans-serif; min-width: 400px; box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);">
			<thead>
			<tr style="background-color: #04AA6D; color: #ffffff; text-align: left;" bgcolor="#04AA6D" align="left">
				<th style="padding: 12px 15px;width:10px">No</th>
				<th style="padding: 12px 15px;">Nama Obat</th>
				<th style="padding: 12px 15px;">Stock</th>
				<th style="padding: 12px 15px;">Exp</th>
				<th style="padding: 12px 15px;">Cabang</th>
			</tr>
		</thead>
		<tbody>';	
			$nom = 1;
			foreach ($vals as $i => $v) {
				$mailBody .= '<tr style="border-bottom: 1px solid #dddddd;"><td style="padding: 12px 15px;">'.$nom++.'</td>';
				$mailBody .= '<td style="padding: 12px 15px;">'.$v->drug_name.'</td>';
				$mailBody .= '<td style="padding: 12px 15px;">'.$v->stock.'</td>';
				$mailBody .= '<td style="padding: 12px 15px;">'.$v->exp_date.'</td>';
				$mailBody .= '<td style="padding: 12px 15px;">'.$v->branch_name.'</td></tr>';

				/*update drugs set flag sent_email = 1*/
				$this->update_data(['id'=>$v->id], ['sent_mail' => 1], 'drugs');
				/**/
			}
			$mailBody .='</tbody></table>';	
			// $receiver_mails[$rcv_id] = $mailBody;

			/*sent email*/
			if ($rcv_email[$rcv_id] !='' && !empty($rcv_email[$rcv_id]) && $rcv_email[$rcv_id] != null && $rcv_email[$rcv_id] !='null') {
				$send_data = [
					'pengirim' => 'MIS Biomedika',
					'penerima' => $rcv_email[$rcv_id],
					// 'penerima' => 'dino@biomedika.id',
					'subject' => 'MIS | Reminder Obat',
					'content' => $mailBody
				];

				$this->kirim_email->send_email($send_data);
			}
			
			/**/
			

		}

	}

}
