<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_jenis_limbah extends CI_model
{


    public function get_all_data($param)
    {
        $this->db->select('jenis_limbah.*');
        $this->db->from('jenis_limbah');
        $this->db->where($param);
        return $this->db->get();
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function last_query_db(){
        return $this->db->last_query();
    }
    
    public function get_data_by($id){
        return $this->db->select('*')
            ->from('jenis_limbah')
            ->where(array('id' => $id))
            ->get()->result();
    }

    public function delete_data($key){
        $where = array('id' => $key);
        $q = $this->db->delete('jenis_limbah', $where);
            
        return $q;
    }
}