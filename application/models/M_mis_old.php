<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_mis extends CI_model
{

    public function last_query_db(){
        return $this->db->last_query();
    }

    public function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
        // echo $this->db->from($table)->where($where)->get_compiled_select();
        // exit();
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $result = $this->db->delete($table);
        return $result;
    }
	
	public function delete($param=[],$table)
	{
        $this->db->where($param);
        $q = $this->db->delete($table);

        return $q;
    }
	
    public function insert_batch($table, $data)
    {
        $this->db->insert_batch($table, $data);
    }

    public function get_data_like($where, $colum, $table)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->like($colum, $where);
        return $this->db->get();
    }

    public function get_data_join()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('position', 'users.id_position = position.id');
        return $this->db->get();
    }

    public function get_sent_to($id)
    {
        $this->db->select('name, (select name_position from position where users.id_position = position.id_position) as name_position, id_receiver');
        $this->db->from('dailyreports_receiver');
        $this->db->join('users', 'dailyreports_receiver.id_receiver = users.id_user');
        //$this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('dailyreports_receiver.id_receiver', $id);

        return $this->db->get();

    }
	
	public function get_all_sent_to($id)
    {
        $this->db->select('name, (select name_position from position where users.id_position = position.id_position) as name_position, id_receiver');
        $this->db->from('dailyreports_receiver');
        $this->db->join('users', 'dailyreports_receiver.id_receiver = users.id_user');
        //$this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('dailyreports_receiver.id_dailyreports', $id);

        return $this->db->get();

    }

    public function get_sent_count($id)
    {
        $this->db->from('dailyreports_receiver');
        $this->db->where('dailyreports_receiver.id_dailyreports', $id);

        return $this->db->get()->num_rows();
    }

    public function get_read_count($id)
    {
        $this->db->from('dailyreports_receiver');
        $this->db->where('dailyreports_receiver.id_dailyreports', $id);
        $this->db->where('dailyreports_receiver.is_view', '1');

        return $this->db->get()->num_rows();
    }

    public function get_user_read($id)
    {
        $this->db->select('name, name_position, id_receiver, is_view');
        $this->db->from('dailyreports_receiver');
        $this->db->join('users', 'dailyreports_receiver.id_receiver = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('dailyreports_receiver.id_dailyreports', $id);

        return $this->db->get();

    }
	
	
    public function total_data_received($id /*id user*/, $id_dailyreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('dailyreports.id');
        $this->db->from('dailyreports');
        $this->db->join('dailyreports_receiver', 'dailyreports_receiver.id_dailyreports = dailyreports.id');
        $this->db->join('users', 'dailyreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($id_dailyreports)) {
            $this->db->where('dailyreports_receiver.id_dailyreports', $id_dailyreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('dailyreports.kategori',$search,'both');
            $this->db->or_like('dailyreports.title',$search,'both');
            $this->db->or_like('dailyreports.date',$search,'both');
            $this->db->or_like('dailyreports.pelayanan_des',$search,'both');
            $this->db->or_like('dailyreports.absensi_des',$search,'both');
            $this->db->or_like('dailyreports.reg_des',$search,'both');
            $this->db->or_like('dailyreports.sampling_des',$search,'both');
            $this->db->or_like('dailyreports.qc_des',$search,'both');
            $this->db->or_like('dailyreports.alat_des',$search,'both');
            $this->db->or_like('dailyreports.janji_des',$search,'both');
            $this->db->or_like('dailyreports.lain_des',$search,'both');
            $this->db->limit(1);

        }
        $this->db->where('dailyreports_receiver.id_receiver', $id);
        /* data belum dihapus oleh penerima */
        $this->db->where('dailyreports_receiver.is_closed', '0');
        $this->db->where('upper(dailyreports.status)', 'OPEN');
        /* data belum dihapus oleh creator */
        $this->db->where_in('dailyreports.is_view', [0,1]);
        // $this->db->where('dailyreports.status <>', 'Close');
		return $this->db->get();
    }

    public function total_data_sent($id /*id user*/, $id_dailyreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('dailyreports.id');
        $this->db->from('dailyreports');
        //$this->db->join('dailyreports_receiver', 'dailyreports_receiver.id_dailyreports = dailyreports.id');
        $this->db->join('users', 'dailyreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position', 'left');
        $this->db->join('branch', 'users.branch_id = branch.branch_id', 'left');
        if (!empty($id_dailyreports)) {
            //$this->db->where('dailyreports_receiver.id_dailyreports', $id_dailyreports);
			$this->db->where('dailyreports.id', $id_dailyreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('dailyreports.kategori',$search,'both');
            $this->db->or_like('dailyreports.title',$search,'both');
            $this->db->or_like('dailyreports.date',$search,'both');
            $this->db->or_like('dailyreports.pelayanan_des',$search,'both');
            $this->db->or_like('dailyreports.absensi_des',$search,'both');
            $this->db->or_like('dailyreports.reg_des',$search,'both');
            $this->db->or_like('dailyreports.sampling_des',$search,'both');
            $this->db->or_like('dailyreports.qc_des',$search,'both');
            $this->db->or_like('dailyreports.alat_des',$search,'both');
            $this->db->or_like('dailyreports.janji_des',$search,'both');
            $this->db->or_like('dailyreports.lain_des',$search,'both');
            $this->db->limit(1);
        }
        $this->db->where('dailyreports.from_id', $id);
        $this->db->where('dailyreports.id_draft', '0');
        $this->db->where('upper(dailyreports.status)', 'OPEN');
        /* data belum dihapus oleh creator */
        $this->db->where_in('dailyreports.is_view', [0,1]);
		return $this->db->get();
    }
	
	public function total_data_archived($id /*id user*/, $id_dailyreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('dailyreports.id');
        $this->db->from('dailyreports');
        //$this->db->join('dailyreports_receiver', 'dailyreports_receiver.id_dailyreports = dailyreports.id');
        $this->db->join('users', 'dailyreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($id_dailyreports)) {
            //$this->db->where('dailyreports_receiver.id_dailyreports', $id_dailyreports);
			$this->db->where('dailyreports.id', $id_dailyreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('dailyreports.kategori',$search,'both');
            $this->db->or_like('dailyreports.title',$search,'both');
            $this->db->or_like('dailyreports.date',$search,'both');
            $this->db->or_like('dailyreports.pelayanan_des',$search,'both');
            $this->db->or_like('dailyreports.absensi_des',$search,'both');
            $this->db->or_like('dailyreports.reg_des',$search,'both');
            $this->db->or_like('dailyreports.sampling_des',$search,'both');
            $this->db->or_like('dailyreports.qc_des',$search,'both');
            $this->db->or_like('dailyreports.alat_des',$search,'both');
            $this->db->or_like('dailyreports.janji_des',$search,'both');
            $this->db->or_like('dailyreports.lain_des',$search,'both');
            $this->db->limit(1);
            $this->db->group_end();
        }
        $this->db->where('upper(dailyreports.status)', 'CLOSE');
        // $this->db->where('dailyreports.status <>', 'Open');
        // $this->db->where('dailyreports.is_view <>', '1');
        $this->db->group_start();
            $this->db->or_group_start();
                $this->db->where('dailyreports.from_id', $id);
                /* data belum dihapus oleh creator */
                $this->db->where_in('dailyreports.is_view', [0,1]);
            $this->db->group_end();
            /* data belum dihapus oleh receiver */
			/*$this->db->or_group_start();
                $this->db->where('dailyreports_receiver.id_receiver', $id);
                
                $this->db->where('dailyreports_receiver.is_closed', '0');
            $this->db->group_end();*/
			
			$this->db->or_group_start();
                $this->db->where('dailyreports.id in (select id_dailyreports from dailyreports_receiver where dailyreports_receiver.id_dailyreports = dailyreports.id and dailyreports_receiver.id_receiver = '.$id.' and dailyreports_receiver.is_closed="0") ', null);
            $this->db->group_end();
			
        $this->db->group_end();
		return $this->db->get();
    }

	public function total_data_draft($id /*id user*/, $id_dailyreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('dailyreports.id');
        $this->db->from('dailyreports');
        //$this->db->join('dailyreports_receiver', 'dailyreports_receiver.id_dailyreports = dailyreports.id','left');
        $this->db->join('users', 'dailyreports.from_id = users.id_user','left');
        $this->db->join('position', 'users.id_position = position.id_position','left');
        $this->db->join('branch', 'users.branch_id = branch.branch_id','left');
        if (!empty($id_dailyreports)) {
            // $this->db->where('dailyreports_receiver.id_dailyreports', $id_dailyreports); // old query return null data
            $this->db->where('dailyreports.id', $id_dailyreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
           $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('dailyreports.kategori',$search,'both');
            $this->db->or_like('dailyreports.title',$search,'both');
            $this->db->or_like('dailyreports.date',$search,'both');
            $this->db->or_like('dailyreports.pelayanan_des',$search,'both');
            $this->db->or_like('dailyreports.absensi_des',$search,'both');
            $this->db->or_like('dailyreports.reg_des',$search,'both');
            $this->db->or_like('dailyreports.sampling_des',$search,'both');
            $this->db->or_like('dailyreports.qc_des',$search,'both');
            $this->db->or_like('dailyreports.alat_des',$search,'both');
            $this->db->or_like('dailyreports.janji_des',$search,'both');
            $this->db->or_like('dailyreports.lain_des',$search,'both');
            $this->db->limit(1);
        }
        $this->db->where('dailyreports.from_id', $id);
        // $this->db->where('dailyreports.is_view <>', '1');
        // $this->db->where('dailyreports.id_draft <>', '0');
        $this->db->where('dailyreports.id_draft', '1');
		return $this->db->get();
    }

    public function get_data_received($id /*id user*/, $id_dailyreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('dailyreports.*,dailyreports.status as status,users.*,position.*,branch.*,
            dailyreports_receiver.id_dailyreports,
            dailyreports_receiver.id_receiver,dailyreports_receiver.is_view as read,dailyreports.status as status');
        $this->db->from('dailyreports');
        $this->db->join('dailyreports_receiver', 'dailyreports_receiver.id_dailyreports = dailyreports.id');
        $this->db->join('users', 'dailyreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($id_dailyreports)) {
            $this->db->where('dailyreports_receiver.id_dailyreports', $id_dailyreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('dailyreports.kategori',$search,'both');
            $this->db->or_like('dailyreports.title',$search,'both');
            $this->db->or_like('dailyreports.date',$search,'both');
            $this->db->or_like('dailyreports.pelayanan_des',$search,'both');
            $this->db->or_like('dailyreports.absensi_des',$search,'both');
            $this->db->or_like('dailyreports.reg_des',$search,'both');
            $this->db->or_like('dailyreports.sampling_des',$search,'both');
            $this->db->or_like('dailyreports.qc_des',$search,'both');
            $this->db->or_like('dailyreports.alat_des',$search,'both');
            $this->db->or_like('dailyreports.janji_des',$search,'both');
            $this->db->or_like('dailyreports.lain_des',$search,'both');
            $this->db->limit(1);

        }
        $this->db->where('dailyreports_receiver.id_receiver', $id);
        /* data belum dihapus oleh penerima */
        $this->db->where('dailyreports_receiver.is_closed', '0');
        $this->db->where('upper(dailyreports.status)', 'OPEN');
        /* data belum dihapus oleh creator */
        $this->db->where_in('dailyreports.is_view', [0,1]);
        // $this->db->where('dailyreports.status <>', 'Close');
        $this->db->order_by('dailyreports_receiver.id', 'desc');
		
		$limit = 10;
		$offset = 0;
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		if(isset($args['offset'])) {
			$offset = $args['offset'];
			unset($args['offset']);
		}
		
		
		$this->db->limit($limit,$offset);
        return $this->db->get();
    }

    public function get_data_sent($id, $id_dailyreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('dailyreports.*,users.*,position.*,branch.*,dailyreports.status as status,dailyreports.id as id_dailyreports'); //dailyreports_receiver.id_dailyreports,dailyreports_receiver.id_receiver
        $this->db->from('dailyreports');
        //$this->db->join('dailyreports_receiver', 'dailyreports_receiver.id_dailyreports = dailyreports.id');
        $this->db->join('users', 'dailyreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position', 'left');
        $this->db->join('branch', 'users.branch_id = branch.branch_id', 'left');
        if (!empty($id_dailyreports)) {
            //$this->db->where('dailyreports_receiver.id_dailyreports', $id_dailyreports);
			$this->db->where('dailyreports.id', $id_dailyreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('dailyreports.kategori',$search,'both');
            $this->db->or_like('dailyreports.title',$search,'both');
            $this->db->or_like('dailyreports.date',$search,'both');
            $this->db->or_like('dailyreports.pelayanan_des',$search,'both');
            $this->db->or_like('dailyreports.absensi_des',$search,'both');
            $this->db->or_like('dailyreports.reg_des',$search,'both');
            $this->db->or_like('dailyreports.sampling_des',$search,'both');
            $this->db->or_like('dailyreports.qc_des',$search,'both');
            $this->db->or_like('dailyreports.alat_des',$search,'both');
            $this->db->or_like('dailyreports.janji_des',$search,'both');
            $this->db->or_like('dailyreports.lain_des',$search,'both');
            $this->db->limit(1);
        }
        $this->db->where('dailyreports.from_id', $id);
        $this->db->where('dailyreports.id_draft', '0');
        $this->db->where('upper(dailyreports.status)', 'OPEN');
        /* data belum dihapus oleh creator */
        $this->db->where_in('dailyreports.is_view', [0,1]);
        // $this->db->where('dailyreports.status <>', 'Close');
        // $this->db->where('dailyreports.id_draft <>', '1');
        // $this->db->where('dailyreports.is_view <>', '1');
        $this->db->order_by('dailyreports.id', 'desc');
		$limit = 10;
		$offset = 0;
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		if(isset($args['offset'])) {
			$offset = $args['offset'];
			unset($args['offset']);
		}
		
		
		$this->db->limit($limit,$offset);
        return $this->db->get();
    }

    public function get_data_archived($id, $id_dailyreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('dailyreports.*,users.*,position.*,branch.*,
            dailyreports.status as status,dailyreports.id as id_dailyreports'); //dailyreports_receiver.id_dailyreports,dailyreports_receiver.id_receiver,
        $this->db->from('dailyreports');
        //$this->db->join('dailyreports_receiver', 'dailyreports_receiver.id_dailyreports = dailyreports.id');
        $this->db->join('users', 'dailyreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($id_dailyreports)) {
            //$this->db->where('dailyreports_receiver.id_dailyreports', $id_dailyreports);
			$this->db->where('dailyreports.id', $id_dailyreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('dailyreports.kategori',$search,'both');
            $this->db->or_like('dailyreports.title',$search,'both');
            $this->db->or_like('dailyreports.date',$search,'both');
            $this->db->or_like('dailyreports.pelayanan_des',$search,'both');
            $this->db->or_like('dailyreports.absensi_des',$search,'both');
            $this->db->or_like('dailyreports.reg_des',$search,'both');
            $this->db->or_like('dailyreports.sampling_des',$search,'both');
            $this->db->or_like('dailyreports.qc_des',$search,'both');
            $this->db->or_like('dailyreports.alat_des',$search,'both');
            $this->db->or_like('dailyreports.janji_des',$search,'both');
            $this->db->or_like('dailyreports.lain_des',$search,'both');
            $this->db->limit(1);
            $this->db->group_end();
        }
        $this->db->where('upper(dailyreports.status)', 'CLOSE');
        // $this->db->where('dailyreports.status <>', 'Open');
        // $this->db->where('dailyreports.is_view <>', '1');
        $this->db->group_start();
            $this->db->or_group_start();
                $this->db->where('dailyreports.from_id', $id);
                /* data belum dihapus oleh creator */
                $this->db->where_in('dailyreports.is_view', [0,1]);
            $this->db->group_end();
            /* data belum dihapus oleh receiver */
			/*$this->db->or_group_start();
                $this->db->where('dailyreports_receiver.id_receiver', $id);
                
                $this->db->where('dailyreports_receiver.is_closed', '0');
            $this->db->group_end();*/
			
			$this->db->or_group_start();
                $this->db->where('dailyreports.id in (select id_dailyreports from dailyreports_receiver where dailyreports_receiver.id_dailyreports = dailyreports.id and dailyreports_receiver.id_receiver = '.$id.' and dailyreports_receiver.is_closed="0") ', null);
            $this->db->group_end();
			
        $this->db->group_end();
        $this->db->order_by('dailyreports.id', 'desc');
		$limit = 10;
		$offset = 0;
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		if(isset($args['offset'])) {
			$offset = $args['offset'];
			unset($args['offset']);
		}
		
		
		$this->db->limit($limit,$offset);
        return $this->db->get();
    }

    public function get_data_draft($id, $id_dailyreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('dailyreports.*,users.*,position.*,branch.*,
            dailyreports.status as status,dailyreports.id as id_dailyreports');//dailyreports_receiver.id_dailyreports,dailyreports_receiver.id_receiver,
        $this->db->from('dailyreports');
        //$this->db->join('dailyreports_receiver', 'dailyreports_receiver.id_dailyreports = dailyreports.id','left');
        $this->db->join('users', 'dailyreports.from_id = users.id_user','left');
        $this->db->join('position', 'users.id_position = position.id_position','left');
        $this->db->join('branch', 'users.branch_id = branch.branch_id','left');
        if (!empty($id_dailyreports)) {
            // $this->db->where('dailyreports_receiver.id_dailyreports', $id_dailyreports); // old query return null data
            $this->db->where('dailyreports.id', $id_dailyreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
           $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('dailyreports.kategori',$search,'both');
            $this->db->or_like('dailyreports.title',$search,'both');
            $this->db->or_like('dailyreports.date',$search,'both');
            $this->db->or_like('dailyreports.pelayanan_des',$search,'both');
            $this->db->or_like('dailyreports.absensi_des',$search,'both');
            $this->db->or_like('dailyreports.reg_des',$search,'both');
            $this->db->or_like('dailyreports.sampling_des',$search,'both');
            $this->db->or_like('dailyreports.qc_des',$search,'both');
            $this->db->or_like('dailyreports.alat_des',$search,'both');
            $this->db->or_like('dailyreports.janji_des',$search,'both');
            $this->db->or_like('dailyreports.lain_des',$search,'both');
            $this->db->limit(1);
        }
        $this->db->where('dailyreports.from_id', $id);
        // $this->db->where('dailyreports.is_view <>', '1');
        // $this->db->where('dailyreports.id_draft <>', '0');
        $this->db->where('dailyreports.id_draft', '1');
        $this->db->order_by('dailyreports.id', 'desc');
		$limit = 10;
		$offset = 0;
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		if(isset($args['offset'])) {
			$offset = $args['offset'];
			unset($args['offset']);
		}
		
		
		$this->db->limit($limit,$offset);
        return $this->db->get()->result();
    }

    public function get_user_replay($id)
    {
        $this->db->select('dailyreports.*,dailyreports_reply.*,users.*,branch.branch_name as branch_name,position.name_position as name_position');
        $this->db->from('dailyreports');
        $this->db->join('dailyreports_reply', 'dailyreports.id = dailyreports_reply.id_dailyreports');
        $this->db->join('users', 'dailyreports_reply.sender = users.id_user');
        $this->db->join('branch', 'branch.branch_id = users.branch_id');
        $this->db->join('position', 'position.id_position = users.id_position');
        $this->db->where('dailyreports.id', $id);

        return $this->db->get();
    }

    public function get_to_id($id,$user_id)
    {
        $this->db->select('*');
        $this->db->from('dailyreports_receiver');
        $this->db->join('dailyreports', 'dailyreports.id = dailyreports_receiver.id_dailyreports');
        $this->db->where('dailyreports.id', $id);
        $this->db->where('dailyreports_receiver.id_receiver', $user_id);

        return $this->db->get();
    }

    public function get_from_id($id)
    {
        $this->db->select('*');
        $this->db->from('dailyreports_receiver');
        $this->db->join('dailyreports', 'dailyreports.id = dailyreports_receiver.id_dailyreports');
        $this->db->where('dailyreports.id', $id);

        return $this->db->get();
    }

    public function get_notification($id)
    {
        return $this->db->from("notifications")
            ->where([
                "to_id" => $id,
				"data not like '%doc_id%'" => null,
                "cuti_flag" => 0
            ])
            ->group_start()
                ->or_where(["read" => null])
                ->or_where(["read" => 0])
            ->group_end()
            // ->where(["anime" => "naruto"])
			->order_by('id', 'desc')
            ->get();
        
        // $this->db->select('*');
        // $this->db->from('notifications');
        // $this->db->where('to_id', $id);
        // $this->db->where('read', 0);
        // $this->db->where('read <>', 1);
        // $this->db->order_by('id', 'desc');
        
        // return $this->db->get();
    }


    public function get_user_hrd()
    {
        $this->db->select('id_user,name');
        $this->db->from('users');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('name_position','STAFF HRD');

        return $this->db->get();
    } 

    public function testing($array)
    {
        return print("<pre>" . print_r($array, true) . "</pre>");
    }

}