<?php

class M_master_control extends CI_Model {
    public $table = "master_control";

    function baseQuery(){
        $q = $this->db->from("$this->table _")
            ->join("users creator", "creator.id_user = _.creator_id", "left")
            ->select([
                "_.*",
                "creator.name as creator_name"
            ]);

        return $q;
    }

    function dt(){
        $q = $this->baseQuery();
        $sql = $q->get_compiled_select();

        $this->load->helper("dt");

        return getDataTable([
            "sql" => $sql
        ]);
    }

    function one($args){
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        return $this->baseQuery()
            ->where($args["where"])
            ->get()->row_array();
    }

    function many($args) {
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        return $this->baseQuery()
            ->where($args["where"])
            ->get()->result_array();
    }

    function save($data) {
        if(empty($data["control_code"])) {
            return ["status" => false, "message" => "Control ID harus diisi"];
        }
        if(empty($data["control_name"])) {
            return ["status" => false, "message" => "Control Name harus diisi"];
        }

        $existing = $this->one(["where" => ["control_code" => $data["control_code"]]]);

        if($existing != null) {
            return ["status" => false, "message" => "Code telah dipakai"];
        }

        $this->db->insert($this->table, $data);
        $id = $this->db->insert_id();
        if($id == null) {
            return ["status" => false, "message" => "Terjadi kesalahan menyimpan data"];
        }
        $data = $this->db->where(["id_control" => $id])->get($this->table)->row_array();

        return ["status" => true, "id" => $id, "data" => $data];
    }

    function delete($where) {
        $target = $this->many(["where" => $where]);

        if(count($target) < 1) {
            return ["status" => false, "message" => "Data tidak ditemukan"];
        }

        $this->db->where($where)->delete($this->table);

        return ["status" => true];
    }

    function update($where, $data) {
        $target = $this->one(["where" => $where]);

        if($target == null) {
            return ["status" => false, "message" => "Data tidak ditemukan"];
        }

        $this->db->where([
            "id_control" => $target["id_control"]
        ])->update($this->table, $data);

        return ["status" => true, "message" => "Data berhasil diupdate"];
    }
}