<?php

class M_master_data extends CI_Model {

	function baseQueryBranch($args = [],$fields = []) {
		if (empty($fields)) {
			$fields = ['_.*'];
		}
		$r = $this->db->from('branch _')
			->select($fields);
		return $r;
	}

	function many_branch($args = [],$fields = []) {
		$this->load->helper('myquery');
		return myquery($this->baseQueryBranch($args,$fields), $args)->get()->result();
	}

	function one_branch($args = []) {
		$this->load->helper('myquery');
		return myquery($this->baseQueryBranch($args,$fields), $args)->get()->row();
	}

	function baseQueryDepartment($args = [],$fields = []) {
		if (empty($fields)) {
			$fields = ['_.*'];
		}
		$r = $this->db->from('departments _')
			->select($fields);
		return $r;
	}

	function many_department($args = [],$fields=[]) {
		$this->load->helper('myquery');
		return myquery($this->baseQueryDepartment($args,$fields), $args)->get()->result();
	}

	function one_department($args = [],$fields=[]) {
		$this->load->helper('myquery');
		return myquery($this->baseQueryDepartment($args,$fields), $args)->get()->row();
	}

	function baseQueryPosition($args = []){
		if (empty($fields)) {
			$fields = ['_.*'];
		}
		$r = $this->db->from('position _')
			->select($fields);
		return $r;
	}

	function many_position($args = [],$fields=[]) {
		$this->load->helper('myquery');
		return myquery($this->baseQueryPosition($args,$fields), $args) ->get()->result();
	}

	function one_position($args = [],$fields=[]) {
		$this->load->helper('myquery');
		return myquery($this->baseQueryPosition($args,$fields), $args) ->get()->row();
	}

	function baseQueryBagian($args = [],$fields=[]){
		if (empty($fields)) {
			$fields = ['_.*'];
		}
		$r = $this->db->from('bagian _')
			->select($fields);
		return $r;
	}

	function many_bagian($args = [],$fields=[]) {
		$this->load->helper('myquery');
		return myquery($this->baseQueryBagian($args,$fields), $args)->get()->result();
	}

	function one_bagian($args,$fields=[]) {
		if($args == null) {
			$args = [];
		}
		$args['limit'] = [1];

		$q = $this->many_bagian($args,$fields);
		if(count($q) > 0) {
			return $q[0];
		}
		return null;
	}
}