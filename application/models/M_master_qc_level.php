<?php


class M_master_qc_level extends CI_Model {

	public $table = "master_qc_level";

	function baseQuery() {
		$q = $this->db->from("$this->table _")
			->join("users usr", "usr.id_user = _.creator_id", "left")
			->select([
				"_.*",
				"usr.name as creator_name"
			]);

		return $q;
	}

	function one($args = null) {
		$r = $this->many($args);
		if(count($r) > 0) {
			return $r[0];
		}
		return null;
	}

	function many($args = null){
		if($args == null) {
			$args = [];
		}
		if(isset($args["where"]) == false) {
			$args["where"] = [];
		}

		return $this->baseQuery()->where($args["where"])->get()->result_array();
	}

	function dt($args = null){
		if($args == null) {
			$args = [];
		}
		if(isset($args["where"]) == false) {
			$args["where"] = [];
		}

		$q = $this->baseQuery();
		$q->where($args["where"]);
		$sql = $q->get_compiled_select();

		$this->load->helper("dt");

		return getDataTable([
			"sql" => $sql
		]);
	}

	function delete($where = null) {
		if($where == null) {
			return ["status" => false, "message" => "Argumen harus diisi"];
		}
		$argsCount = 0;
		foreach ($where as $key => $value) {
			$argsCount++;
		}
		if($argsCount == 0) {
			return ["status" => false, "message" => "Argumen harus diisi"];
		}

		$this->db->where($where)->delete($this->table);

		return ["status" => true, "message" => "Success"];
	}

	function add($data) {
		$this->db->insert($this->table, $data);
		$id = $this->db->insert_id();

		$data = $this->one(["id_level" => $id]);

		if($id == null || $data == null) {
			return ["status" => false, "message" => "Terjadi kesalahan menyimpan data"];
		}

		return ["status" => true, "message" => "success", "id" => $id, "data" => $data];
	}

	function update($where, $data) {
		$target = $this->one($where);

		if($target == null) {
			return ["status" => false, "message" => "Data tidak ditemukan"];
		}

		$this->db->where(["id_level" => $target["id_level"]])->update($this->table, $data);

		return ["status" => true, "message" => "Success"];
	}

}