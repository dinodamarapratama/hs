<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_overtime extends CI_model
{

    public function get_where($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }


    public function update_data_overtime($where,$data,$reason,$table,$isUser)
    {     
      $id_sp = $where['id_sp'];
      $status = $data['status'];
      $ket_status = $data['ket_status'];
      $reject_reason = $reason;
      
      //hrd
      if ($isUser == 1){
      $cost = $data['cost'];
      $norek_transfer = $data['norek_transfer'];
      $total_cost = $data['total_cost'];
      $status_transfer = $data['status_transfer'];
        $r = $this->db->query("update sp_overtime SET status='$status',ket_status='$ket_status',reject_reason='$reject_reason',cost='$cost',norek_transfer='$norek_transfer',total_cost='$total_cost',status_transfer='$status_transfer' where id_sp = '$id_sp'");
      }else if($isUser == 2){
        $status_transfer = $data['status_transfer'];
        $transfer_by = $data['transfer_by'];
        $r = $this->db->query("update sp_overtime SET status='$status',ket_status='$ket_status',status_transfer='$status_transfer',transfer_by='$transfer_by' where id_sp = '$id_sp'");
      }else{
        $r = $this->db->query("update sp_overtime SET status='$status', ket_status='$ket_status',reject_reason='$reject_reason' where id_sp = '$id_sp'");
      }      
      return $r;
    }

    public function update_data_receiver($x, $z, $v)
    {

      // $id_receiver = $x['id_receiver'];
      // $id_sp = $x['id_sp'];

      // $r = $this->db->query("update sp_receiver SET id_receiver='$id_receiver' where id_sp = '$id_sp'")->result();
      // return $this->response($r);
        // print_r($x['id_sp']);
        // print_r($x['id_receiver']);
        // print_r($z);
        // print_r($v);
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $result = $this->db->delete($table);
        return $result;
    }

    /*main base query*/
    function get_related_data($config=[]){
        /* params
            - master
            - relation
            - where
            - select
            - baseQuery => 1:use as obj func
        */
        (!empty($config['fields'])?$config['fields']:$config['fields']='*');
        $this->db->select($config['fields']);
        if (!empty($config['relation'])) {
            foreach ($config['relation'] as $key => $value) {
                $this->db->join($value['table'],$value['key'],'left');  
            }
            
        }
        if (!empty($config['where'])) {
          $this->db->where($config['where']);
        }
        if (!empty($config['or_where'])) {
            $this->db->or_where($config['or_where']);
        }
        if (!empty($config['or_where_group'])) {
            $this->db->group_start();
            $this->db->or_where($config['or_where_group']);
            $this->db->group_end();
        }

        if (!empty($config['in_where'])) {
          foreach ($config['in_where'] as $key => $value) {
            $this->db->where_in($key,$value);
          }
        }

        if (!empty($config['not_in_where'])) {
          foreach ($config['not_in_where'] as $key => $value) {
            $this->db->where_not_in($k,$v);
          }
        }
        
        if (!empty($config['order'])) {
            // $this->db->order_by($config['order']['field'],$config['order']['dir']);
            $this->db->order_by($config['order']);
        }

        if (!empty($config['baseQuery']) && $config['baseQuery']==1) {
            $this->db->from($config['master']);
            return $this->db; /*as obj*/
        }else{
            /*default*/
            return $this->db->get($config['master']);
        }
        
    }

    function get_list($config=[],$offset,$limit){
      if (!empty($config['baseQuery']) && $config['baseQuery']==1) {
        $queryList = $this->get_related_data($config);
        $queryList->limit($offset, $limit);
        return $queryList->get();
      }else{
        return false;
      }
     
    }

    function count_list_all($config=[]){
      if (!empty($config['baseQuery']) && $config['baseQuery']==1) {
        $queryList = $this->get_related_data($config);
        return $queryList->count_all_results();
      }else{
        return false;
      }
    }

    function get_current_user($user_id){
      $query = $this->db->query("Select a.*,b.branch_name,c.name_position,a.CUTI as total_jatah_cuti
            from users a
            join branch b on (a.branch_id = b.branch_id)
            join position c on (a.id_position = c.id_position)
            where id_user='".$user_id."'
        ");
        return $query->first_row();
    }

    function get_notification($id){
        return $this->db->from("notifications")
            ->where([
                "to_id" => $id,
                "cuti_flag" => 1
            ])
            
            ->select('notifications.*,(select image from users where id_user = notifications.from_id) as photo_profile,(select name from users where id_user = notifications.from_id) as creator_name')
            ->limit(20)
            
            ->order_by('id', 'desc')
            ->get()->result();
    }

    function get_sp_receiver($params = []){
        $this->db->select('*');
        $this->db->from('sp_receiver');
        $this->db->where($params);

        $query = $this->db->get();
        return $query->result();
    }
}
