<?php

class M_neraca extends CI_Model {

    public function getLimbah($param)
    {
        $this->db->select('sum(LM.jumlah) as jumlah_masuk');
        $this->db->from('logbook_btiga L');
        $this->db->join('jenis_limbah JL', 'L.jenis_limbah_id = JL.id', 'left');
        $this->db->join('limbah_masuk LM', 'LM.logbook_id = L.id', 'inner');
        $this->db->join('limbah_keluar LK', 'LK.logbook_id = L.id', 'inner');
        $this->db->join('branch B', 'L.branch_id = B.branch_id', 'left');
        $this->db->where($param);
        return $this->db->get()->result();
    }

    public function getNeraca($param,$order=[],$limit=0)
    {
        $this->db->select('*,N.date_created as tgl_input, N.id as neraca_id');
        $this->db->from('neraca N');
        $this->db->join('jenis_limbah JL', 'N.jenis_limbah_id = JL.id', 'left');
        $this->db->join('branch B', 'N.branch_id = B.branch_id', 'left');
        if (!empty($param)) {
            $this->db->where($param);    
        }
        
        if (!empty($order)) {
            $this->db->order_by($param);    
        }

        if ($limit > 0) {
            $this->db->limit($limit);    
        }
        
        return $this->db->get()->result();
    }

    public function getBranchById($key){
        $this->db->select('*');
        $this->db->from('branch');
        $this->db->where(['branch_id' => $key]);
        return $this->db->get()->result();
    }

    public function get_sumber($param=[])
    {
        $this->db->select('sumber_limbah.*');
        $this->db->from('sumber_limbah');
        $this->db->where($param);
        return $this->db->get()->result();
    }

    public function save_neraca($param=[]){
        $this->db->insert('neraca', $param);
        return $this->db->insert_id();
    }

    public function update_neraca($data, $where)
    {
        $this->db->where($where);
        return $this->db->update('neraca', $data);
    }

    public function hapus_neraca($param=[]){
        $del = $this->db->delete('neraca', $param);
        return $del;
    }

    public function last_query_db(){
        return $this->db->last_query();
    }
}