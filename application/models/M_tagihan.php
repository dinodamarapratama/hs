<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_tagihan extends CI_model
{

    public function last_query_db(){
        return $this->db->last_query();
    }

    public function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
        // echo $this->db->from($table)->where($where)->get_compiled_select();
        // exit();
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $result = $this->db->delete($table);
        return $result;
    }
    
    public function delete($param=[],$table)
    {
        $this->db->where($param);
        $q = $this->db->delete($table);

        return $q;
    }
    
    public function insert_batch($table, $data)
    {
        $this->db->insert_batch($table, $data);
    }

    public function get_data_like($where, $colum, $table)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->like($colum, $where);
        return $this->db->get();
    }

  


// frontend start 

    function many_tag($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        else{
            $t = '';
            foreach($args["where"] as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            //print_r($t);
            
            $args["where"] = $t;
        }
        if(isset($args["limit"]) == false) {
            $args["limit"] = [];
        }
        if(isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if(isset($args["order"]) == false) {
            $args["order"] = [];
        }
        
        $result = array();
        $row = $this->baseQueryTag($args)->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();    
        $result = array_merge($result, ['rows' => $row]);
        $result['total'] = $this->baseQueryTag($args)->where($args["where"])->get()->num_rows();
        
        return $result;
        
    }


    function baseQueryTag($args = []) {
        return $this->db->from("tagihan _")
          //  ->join('mcu_pemeriksaan mp', 'mp.id_mcu = _.id_mcu','left')
            //->order_by($args["sort"],$args["order"])
			->order_by('_.tgl','asc')
            ->order_by('_.nama_perusahaan','asc')
			->order_by('_.id_tagihan','asc')
			->group_by("_.id_tagihan")
            ->select([
				"_.*",
				"DATE_FORMAT(_.tgl,'%d-%b-%y') as tgl",
                "DATE_FORMAT(_.tgl_bayar,'%d-%b-%y') as tgl_bayar",
                "('_.debet' + '_.kredit') as kalsaldo",
                //"CONCAT('Rp ',fRupiah(total_tagaihan)) as total_tagihan",
				

            ]);
    }


    function many_tagihan($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        else{
            $t = '';
            foreach($args["where"] as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            //print_r($t);
            
            $args["where"] = $t;
        }
        if(isset($args["limit"]) == false) {
            $args["limit"] = [];
        }
        if(isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if(isset($args["order"]) == false) {
            $args["order"] = [];
        }
        
        $result = array();
        $row = $this->baseQueryTagihan($args)->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();    
        $result = array_merge($result, ['rows' => $row]);
        $result['total'] = $this->baseQueryTagihan($args)->where($args["where"])->get()->num_rows();
        
        return $result;
        
    }

    function baseQueryTagihan($args = []) {
        return $this->db->from("tagihan _")
          //  ->join('mcu_pemeriksaan mp', 'mp.id_mcu = _.id_mcu','left')
            ->order_by($args["sort"],$args["order"])
            //->order_by('_.tgl','asc')
            //->order_by('_.nama_perusahaan','asc')
            //->order_by('_.id_tagihan','asc')
            //->group_by("_.id_tagihan")
            ->select([
                "_.*",
                "DATE_FORMAT(_.tgl,'%d-%b-%y') as tgl",
                "DATE_FORMAT(_.tgl_bayar,'%d-%b-%y') as tgl_bayar",
                "('_.debet' + '_.kredit') as kalsaldo",
               // "CONCAT('Rp ',fRupiah(total_tagihan)) as total_tagihan",
                

            ]);
    }


    function add_input_tag($data) {
        
        $this->db->insert("tagihan", $data);
        $id = $this->db->insert_id();
        

        return ["status" => true, "message" => "Success"];
    }

     function update_input_kat($where, $data) {
        $this->db->where($where)->update("master_mcu_kategori", $data);
        return ["status" => true, "message" => "Success"];
    }

    function delete_input_kat($where){
        $del = $this->db->delete('master_mcu_kategori', $where);
        return ["status" => true, "message" => "Success"];
    }

    



}
