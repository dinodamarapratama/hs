<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
*
*/
class M_masterhs extends CI_model
{

public function edit_data($where, $table)
{
return $this->db->get_where($table, $where);
}

public function get_data($table)
{
return $this->db->get($table);
}

public function insert_data($data, $table)
{
$this->db->insert($table, $data);
return $this->db->insert_id();
}

public function update_data($where, $data, $table)
{
$this->db->where($where);
$this->db->update($table, $data);
}

public function delete_data($where, $table)
{
$this->db->where($where);
$result = $this->db->delete($table);
return $result;
}

public function get_all_relations($table)
{
return $this->db->get($table);
}

// START HS

public function get_all_masterpayment()
{
$this->db->select('pay.*,usr.name as fullname,pay.pay_id');
$this->db->from('hs_payment pay');
$this->db->join('users usr','usr.id_user = pay.creator_id');
return $this->db->get();
}

public function edit_data_masterpayment($where, $table)
{
$this->db->select('pay.*');
$this->db->from('hs_payment pay');
$this->db->where($where);
return $this->db->get();
}

// END HS


// START HS

public function get_all_masterphs($branch_id = null)
{
$this->db->select('phs.*,br.*,(select usr.name from users usr where usr.id_user = phs.id_user) as fullname,
(select user.name from users user where user.id_user = phs.creator_id) as namefull,phs.ptgshs_id,phs.created_at');
$this->db->from('hs_initial_petugas_hs phs');
//$this->db->join('users usr','usr.id_user = phs.id_user');
//$this->db->join('users user','user.id_user = phs.creator_id');
$this->db->join('branch br','br.branch_id = phs.branch_id');
$this->db->order_by('phs.ptgshs_id','asc');

if(!empty($branch_id)){
$this->db->where('phs.branch_id', $branch_id);
}

return $this->db->get();
}

public function get_masterphsCondition($branch_id = null)
{
$this->db->select('phs.*,br.*,usr.name as fullname,user.name as namefull,phs.ptgshs_id,phs.created_at');
$this->db->from('hs_initial_petugas_hs phs');
$this->db->join('users usr','usr.id_user = phs.id_user');
$this->db->join('users user','user.id_user = phs.creator_id');
$this->db->join('branch br','br.branch_id = phs.branch_id');
$this->db->where_not_in('abbr_hs','SELECT abbr_name from hs_abbr Where date("Y-m-d") = start_date');

if(!empty($branch_id)){
$this->db->where('phs.branch_id', $branch_id);
}

return $this->db->get();

}

public function edit_data_masterphs($where, $table)
{
$this->db->select('phs.*');
$this->db->from('hs_initial_petugas_hs phs');
$this->db->where($where);
return $this->db->get();
}

// END HS

// START BLOCK

public function get_all_masterblock($branch_id = null)
{
$this->db->select('block.*,br.*,ptgshs.abbr_hs,ptgs.name as ptgs,usr.name as fullname,block.created_at');
$this->db->from('hs_block block');
$this->db->join('hs_initial_petugas_hs ptgshs','ptgshs.ptgshs_id = block.ptgs_id');
$this->db->join('hs_time time','time.time_id = block.time_id');
$this->db->join('branch br','br.branch_id = block.branch_id');
$this->db->join('users usr','usr.id_user = block.creator_id');
$this->db->join('users ptgs','ptgs.id_user = ptgshs.id_user');

if(!empty($branch_id)){
$this->db->where('block.branch_id', $branch_id);
}

return $this->db->get();
}



public function edit_data_masterblock($where, $table)
{
$this->db->select('block.*');
$this->db->from('hs_block block');
$this->db->where($where);
return $this->db->get();
}

// END BLOCK


// START TIME

public function get_all_mastertime()
{
$this->db->select('time.*,br.*,usr.name as fullname,time.time_id,time.created_at');
$this->db->from('hs_time time');
$this->db->join('users usr','usr.id_user = time.creator_id');
$this->db->join('branch br','br.branch_id = time.branch_id');

return $this->db->get();
}



public function edit_data_mastertime($where, $table)
{
$this->db->select('time.*');
$this->db->from('hs_time time');
$this->db->where($where);
return $this->db->get();
}

// END TIME


// START ABBR



public function get_all_masterabbr($branch_id = null)
{
$this->db->select('abbr.*,br.*,usr.name as fullname,abbr.abbr_id,abbr.created_at');
$this->db->from('hs_abbr abbr');
$this->db->join('users usr','usr.id_user = abbr.creator_id');
$this->db->join('branch br','br.branch_id = abbr.branch_id');
if(!empty($branch_id)){
$this->db->where('abbr.branch_id', $branch_id);
}

return $this->db->get();
}

public function edit_data_masterabbr($where, $table)
{
$this->db->select('abbr.*');
$this->db->from('hs_abbr abbr');
$this->db->where($where);
return $this->db->get();
}

// END ABBR








}
