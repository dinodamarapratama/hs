<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_masterqc extends CI_model
{

    public function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $result = $this->db->delete($table);
        return $result;
    }

    public function get_karyawan($params=[], $rows=0, $offset=10,$all=0){
        $this->db->select(['users.id_user','users.name','users.last_name','users.CUTI','users.cuti_keseluruhan','users.branch_id','branch.branch_name','bagian.name as nama_bagian','position.name_position','users.STATUS','users.TGLMASUK','users.id_position','users.id_bagian','users.last_reset_cuti','users.masabakti','users.masabakti_keseluruhan','users.last_reset_masabakti']);
        $this->db->from('users');
        if (isset($params['in_param'])) {
          foreach ($params['in_param'] as $key => $value) {
              $this->db->where_in($key,$value);
          }
          unset($params['in_param']);
        }
 
        if (isset($params['not_in_param'])) {
         foreach ($params['not_in_param'] as $k => $v) {
             $this->db->where_not_in($k,$v);
         }
         unset($params['not_in_param']);
       }
 
        $this->db->where($params);
        if ($all==0) {
          $this->db->limit($rows, $offset);
        }
        $this->db->order_by('name', 'ASC');
        $this->db->join('branch', 'users.branch_id = branch.branch_id','left');
        $this->db->join('bagian', 'users.id_bagian = bagian.id','left');
        $this->db->join('position', 'users.id_position = position.id_position','left');
        $query = $this->db->get();
        return $query->result_array();
     }

    // START TEST
    function baseQueryTest($args = []) {
        if(empty($args['include_info_qc']) == true) {
            $args['include_info_qc'] = false;
        }

        $qInfoQc = '';
        if($args['include_info_qc'] == true) {
            $q = $this->db->from('qc_input _')
                ->group_by('_.id_test')
                ->select([
                    '_.id_test',
                    'count(*) as jumlah'
                ]);
            if(!empty($args['branch'])) {
                $q->where('_.branch_id', $args['branch']);
            }
            $qInfoQc = $q->get_compiled_select();
        }
        $r = $this->db->from("master_test _")
            ->join('users usr', 'usr.id_user = _.creator_id','left')
            ->join('branch b1', 'b1.branch_id = _.branch_id','left')
            ->select(["_.*",'usr.name as creator_name','usr.username as creator_username','b1.branch_name']);

        if($args['include_info_qc'] == true) {
            $r  ->join("($qInfoQc) iqc", 'iqc.id_test = _.test_id', 'left')
                ->select(['coalesce(iqc.jumlah, 0) as jumlah_iqc']);
        }

        return $r;
    }

    function many_test($args = []) {
        $this->load->helper("myquery");
        return myquery($this->baseQueryTest($args), $args)->get()->result_array();
    }

    public function get_all_mastertest($args = [])
    {
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        $this->db->select('mtest.*, usr.name, unit.*, gtest.group_name, b1.branch_name');
        $this->db->from('master_test mtest');
        $this->db->join('users usr','usr.id_user = mtest.creator_id', "left");
        $this->db->join('master_test_group gtest','gtest.group_id = mtest.group_id', "left");
        $this->db->join('master_unit unit','unit.id_unit = mtest.id_unit', "left");
        $this->db->join("branch b1", "b1.branch_id = mtest.branch_id", "left");
        $this->db->where($args["where"]);

        return $this->db->get();
    }

    public function edit_data_mastertest($where, $table)
    {
        $this->db->select('master_test.*');
        $this->db->from('master_test');
        $this->db->where($where);
        return $this->db->get();
    }

    // END TEST

     // START GROUP

     public function get_all_mastertestgroup()
    {
        $this->db->select('gtest.*,usr.name, b1.branch_name');
        $this->db->from('master_test_group gtest');
        $this->db->join('users usr','usr.id_user = gtest.creator_id', "left");
        $this->db->join("branch b1", "b1.branch_id = gtest.branch_id", "left");
        return $this->db->get();
    }

    public function edit_data_mastertestgroup($where, $table)
    {
        $this->db->select('master_test_group.*');
        $this->db->from('master_test_group');
        $this->db->where($where);
        return $this->db->get();
    }

    function baseQueryGroupTest($args = []){
        $r = $this->db->from("master_test_group _")
            ->join('users usr', 'usr.id_user = _.creator_id', 'left')
            ->join('branch b1', 'b1.branch_id = _.branch_id', 'left')
            ->select([
                "_.*",
                'usr.name as creator_name',
                'usr.username as creator_username',
                'b1.branch_name'
            ]);
        return $r;
    }

    function many_group_test($args = []) {
        $this->load->helper("myquery");
        return myquery($this->baseQueryGroupTest($args), $args)->get()->result_array();
    }

    function baseQueryTestFilter($args = []){
        $s = $this->db->from("master_test _")
            ->join('users usr', 'usr.id_user = _.creator_id', 'left')
            ->join('branch b1', 'b1.branch_id = _.branch_id', 'left')
            ->select([
                "_.*",
                'usr.name as creator_name',
                'usr.username as creator_username',
                'b1.branch_name'
            ]);
        return $s;
    }

    function many_testFilter($args = []) {
        $this->load->helper("myquery");
        return myquery($this->baseQueryTestFilter($args), $args)->get()->result_array();
    }

    // END GROUP

    // START UNIT

    public function get_all_unit()
    {
        $this->db->select('unit.*,usr.name, b1.branch_name');
        $this->db->from('master_unit unit');
        $this->db->join('users usr','usr.id_user = unit.creator_id', "left");
        $this->db->join("branch b1", "b1.branch_id = unit.branch_id", "left");
        return $this->db->get();
    }

    public function edit_data_masterunit($where, $table)
    {
        $this->db->select('master_unit.*');
        $this->db->from('master_unit');
        $this->db->where($where);
        return $this->db->get();
    }

    // END UNIT


    public function get_all_relations($table)
    {
        return $this->db->get($table);
    }


    /* start level */

    function baseQueryLevel($args = null) {
        if($args == null) {
            $args = [];
        }
        $q = $this->db->from("master_qc_level _")
            ->join("users usr", "usr.id_user = _.creator_id", "left")
            ->join("branch b1", "b1.branch_id = _.branch_id", "left")
            ->select([
                "_.*",
                "usr.name as creator_name",
                "b1.branch_name"
            ]);

        return $q;
    }

    function dt_level($args = []) {
        $this->load->helper('myquery');
        // $sql = $this->baseQueryLevel()->get_compiled_select();
        $sql = myquery($this->baseQueryLevel($args), $args)->get_compiled_select();
        $this->load->helper('dt');
        return getDataTable([
            "sql" => $sql
        ]);
    }

    function add_level($data) {
        $this->db->insert("master_qc_level", $data);
        $id = $this->db->insert_id();
        $r = $this->one_level(["where" => ["id_level" => $id]]);
        if($id == null || $r == null) {
            return ["status" => false, "message" => "Terjadi kesalahan"];
        }
        return ["status" => true, "message" => "", "id" => $id, "data" => $r];
    }

    function update_level($where, $data) {
        $target = $this->many_level(["where" => $where]);
        if(count($target) == 0) {
            return ["status" => false, "message" => "Data tidak ditemukan"];
        }
        foreach ($target as $key => $t) {
            $this->db->where(["id_level" => $t["id_level"]])->update("master_qc_level", $data);
        }
        return ["status" => true, "message" => "Success"];
    }

    function many_level($args = []) {
        if($args == null) {
            $args = [];
        }
        if(empty($args["where"])) {
            $args["where"] = [];
        }
        $r = $this->baseQueryLevel()->where($args["where"])->get()->result_array();
        return $r;
    }

    function one_level($args = []) {
        if($args == null) {
            $args = [];
        }
        $r = $this->many_level($args);
        if(count($r) == 0) {
            return null;
        }
        return $r[0];
    }

    function delete_level($where) {
        $target = $this->many_level(["where" => $where]);
        if(count($target) == 0) {
            return ["status" => false, "message" => "Data tidak ditemukan"];
        }

        foreach ($target as $key => $t) {
            $this->db->where(["id_level" => $t["id_level"]])->delete("master_qc_level");
        }
        return ["status" => true, "message" => "Success"];
    }

    /* end level */

    /* start group level*/

    function baseQueryGroupLevel($args = null) {
        if($args == null) {
            $args = [];
        }
        if(isset($args["join_level"]) == false) {
            $args["join_level"] = true;
        }

        $sqlMember = "";
        if($args["join_level"] == true) {
            $sqlMember = $this->db->from("master_qc_group_level_member member")
                ->join("master_qc_level level", "level.id_level = member.id_level", "left")
                ->select("GROUP_CONCAT( level.level_name SEPARATOR '-----' ) as list_level, member.id_group_level")
                ->group_by("member.id_group_level")
                ->get_compiled_select();
        }

        $q = $this->db->from("master_qc_group_level _")
            ->join("users usr", "usr.id_user = _.creator_id", "left")
            ->join("branch b1", "b1.branch_id = _.branch_id", "left")
            ->select([
                "_.*",
                "usr.name as creator_name",
                "b1.branch_name"
            ]);

        if($args["join_level"] == true) {
            // $q->join("master_qc_group_level_member member", "member.id_group_level = _.id_group_level", "left");
            // $q->join("master_qc_level level", "level.id_level = member.id_level", "left");
            // $q->select("GROUP_CONCAT( level.level_name SEPARATOR '-----' ) as list_level");
            // $q->group_by("member.id_group_level");
            $q->join("($sqlMember) jm", "jm.id_group_level = _.id_group_level", "left")
                ->select("list_level");
        }
        return $q;
    }

    function dt_group_level($args = []){
        $this->load->helper('myquery');
        // $sql = $this->baseQueryGroupLevel($args)->get_compiled_select();
        $sql = myquery($this->baseQueryGroupLevel($args), $args)->get_compiled_select();
        $this->load->helper("dt");
        return getDataTable([
            "sql" => $sql
        ]);
    }

    function many_group_level($args = []) {
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        return $this->baseQueryGroupLevel($args)->where($args["where"])->get()->result_array();
    }

    function one_group_level($args = []) {
        if($args == null) {
            $args = [];
        }
        $args["join_level"] = false;
        $r = $this->many_group_level($args);
        if(count($r) == 0) {
            return null;
        }
        $r = $r[0];
        $r["list_member"] = $this->db->from("master_qc_group_level_member glmember")
                            ->join("master_qc_level lvl", "lvl.id_level = glmember.id_level", "left")
                            ->where(["id_group_level" => $r["id_group_level"]])
                            ->select([
                                "glmember.*",
                                "lvl.level_name"
                            ])
                            ->order_by('lvl.level_name')
                            ->get()->result_array();
        return $r;
    }

    function delete_group_level($where){
        $target = $this->many_group_level(["where" => $where]);
        if(count($target) == 0) {
            return ["status" => false, "message" => "Data tidak ditemukan"];
        }
        foreach ($target as $key => $t) {
            $this->db->where(["id_group_level" => $t["id_group_level"]])->delete("master_qc_group_level_member");
            $this->db->where(["id_group_level" => $t["id_group_level"]])->delete("master_qc_group_level");
        }
        return ["status" => true, "message" => "Success"];
    }

    function add_group_level($data) {
        $id_levels = [];
        if(array_key_exists("id_level", $data)) {
            if($data["id_level"] != null) {
                if(is_array($data["id_level"])) {
                    $id_levels = $data["id_level"];
                } else {
                    $id_levels = [ $data["id_level"] ];
                }
            }

            unset($data["id_level"]);
        }

        $this->db->insert("master_qc_group_level", $data);
        $id = $this->db->insert_id();
        $r = $this->one_group_level(["where" => ["id_group_level" => $id]]);
        foreach ($id_levels as $key => $id_level) {
            $this->db->insert("master_qc_group_level_member", [
                "id_level" => $id_level,
                "id_group_level" => $id
            ]);
        }

        if($id == null || $r == null ) {
            return ["status" => false, "message" => "Terjadi kesalahan"];
        }
        return ["status" => true, "message" => "Success", "id" => $id, "data" => $r];
    }

    function update_group_level($where, $data) {
        $target = $this->many_group_level(["where" => $where]);
        if(count($target) == 0) {
            return ["status" => false, "message" => "Data tidak ditemukan"];
        }
        $target = $target[0];
        $id_level = [];
        if(array_key_exists("id_level", $data)) {
            if($data["id_level"] != null) {
                $id_level = $data["id_level"];
                if(is_array($id_level) == false) {
                    $id_level = [ $id_level ];
                }
            }

            unset($data["id_level"]);

        }
        $this->db->where(["id_group_level" => $target["id_group_level"]])->update("master_qc_group_level", $data);
        $old_member = $this->db->where(["id_group_level" => $target["id_group_level"]])->get("master_qc_group_level_member")->result_array();
        $updated = [];
        foreach ($id_level as $key => $idlevel) {
            $exist = false;
            foreach ($old_member as $key => $old) {
                if($old["id_level"] == $idlevel) {
                    $exist = true;
                    $updated[] = $old["id"];
                    break;
                }
            }
            if($exist == false) {
                $this->db->insert("master_qc_group_level_member", [
                    "id_group_level" => $target["id_group_level"],
                    "id_level" => $idlevel
                ]);
            }
        }

        foreach ($old_member as $key => $old) {
            if(in_array($old["id"], $updated) == false) {
                $this->db->where(["id" => $old["id"]])->delete("master_qc_group_level_member");
            }
        }

        return ["status" => true, "message" => "Success"];
    }
    /* end group level*/

    /* start control */

    function baseQueryControl($args = []) {
        if($args == null) {
            $args = [];
        }
        $q = $this->db->from("master_control _")
            ->join("users usr", "usr.id_user = _.creator_id", "left")
            ->join("master_qc_group_level glevel", "glevel.id_group_level = _.id_group_level", "left")
            ->join("branch b1", "b1.branch_id = _.branch_id", "left")
            ->join('master_qc_control_category cat', 'cat.id_control_category = _.id_category', 'left')
            ->select([
                "_.*",
                "usr.name as creator_name",
                "glevel.group_level_name",
                "b1.branch_name",
                'cat.category_name'
            ]);
        return $q;
    }

    function dt_control($args = []) {
        $this->load->helper('myquery');
        // $sql = $this->baseQueryControl($args)->get_compiled_select();
        $sql = myquery($this->baseQueryControl($args), $args)->get_compiled_select();
        $this->load->helper("dt");
        return getDataTable([
            "sql" => $sql
        ]);
    }

    function update_control($where, $data) {
        $target = $this->one_control(["where" => $where]);
        if($target == null) {
            return ["status" => false, "message" => "Data tidak ditemukan"];
        }
        $this->db->where(["id_control" => $target["id_control"]])->update("master_control", $data);
        return ["status" => true, "message" => "Success"];
    }

    function one_control($args = []) {
        if($args == null) {
            $args = [];
        }
        $r = $this->many_control($args);
        if(count($r) == 0) {
            return null;
        }
        $ctrl = $r[0];
        $ctrl["group_level"] = $this->one_group_level(["where" => ["id_group_level" => $ctrl["id_group_level"]]]);
        return $ctrl;
    }

    function many_control($args = []) {
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        return $this->baseQueryControl($args)->where($args["where"])->get()->result_array();
    }

    function add_control($data) {
        $this->db->insert("master_control", $data);
        $id = $this->db->insert_id();
        $r = $this->one_control(["where" => ["id_control" => $id]]);
        if($id == null || $r == null) {
            return ["status" => false, "message" => "Terjadi kesalahan"];
        }
        return ["status" => true, "message" => "Success", "id" => $id, "data" => $r];
    }

    function delete_control($where) {
        $targets = $this->many_control(["where" => $where]);
        if(count($targets) == 0) {
            return ["status" => false, "message" => "Data tidak ditemukan"];
        }
        foreach ($targets as $key => $target) {
            $this->db->where(["id_control" => $target["id_control"]])->delete("master_control");
        }
        return ["status" => true, "message" => "Success"];
    }
    /* end control */

    /* start master alat */
    function add_alat($data) {
        $this->db->insert("master_qc_alat", $data);
        $id = $this->db->insert_id();
        $r = $this->one_alat(["where" =>  ["id_alat" => $id]]);
        if($id == null || $r == null) {
            return ["status" => false, "message" => "Terjadi kesalahan"];
        }
        return ["status" => true, "message" => "Success"];
    }

    function one_alat($args = []) {
        $r = $this->many_alat($args);
        if(count($r) == 0) {
            return null;
        }
        return $r[0];
    }

    function many_alat($args = []) {
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        return $this->baseQueryAlat($args)->where($args["where"])->get()->result_array();
    }

    function baseQueryAlat($args = []) {
        return $this->db->from("master_qc_alat _")
            ->join("users usr", "usr.id_user = _.creator_id", "left")
            ->join("master_test_group gtest", "gtest.group_id = _.group_id", "left")
            ->join("branch b1", "b1.branch_id = _.branch_id", "left")
            ->select([
                "_.*",
                "usr.name as creator_name",
                "gtest.group_name as test_group_name",
                "b1.branch_name"
            ]);
    }

    function dt_alat($args = []) {
        $this->load->helper('myquery');
        // $sql = $this->baseQueryAlat($args)->get_compiled_select();
        $sql = myquery($this->baseQueryAlat($args), $args)->get_compiled_select();
        $this->load->helper("dt");
        return getDataTable([
            "sql" => $sql
        ]);
    }

    function update_alat($where, $data) {
        $target = $this->one_alat(["where" => $where]);
        if($target == null) {
            return ["status" => false, "message" => "Data tidak ditemukan"];
        }
        $this->db->where(["id_alat" => $target["id_alat"]])->update("master_qc_alat", $data);
        return ["status" => true, "message" => "Success"];
    }

    function delete_alat($where) {
        $targets = $this->many_alat(["where" => $where]);
        if(count($targets) == 0) {
            return ["status" => false, "message" => "Data tidak ditemukan"];
        }
        foreach ($targets as $key => $target) {
            $this->db->where(["id_alat" => $target["id_alat"]])->delete("master_qc_alat");
        }
        return ["status" => true, "message" => "Success"];
    }
    /* end master alat */

    /* start lot number */
    function dt_lot_number($args = []) {
        $sql = $this->baseQueryLotNumber($args)->where($args['where'])->get_compiled_select();
        $this->load->helper("dt");
        return getDataTable([
            "sql" => $sql,
			"default_order" => ['t1.active'=>'desc','t1.id_lot_number'=>'desc']
        ]);
    }

    // mobile
    function dt_lot_number_m($args = []) {
        $this->load->helper('myquery');
        $q = myquery($this->baseQueryLotNumber($args), $args);
        $sql = $q->get()->result();
        return $sql;
    }

    function baseQueryLotNumber($args = []) {
        if($args == null) {
            $args = [];
        }
        $q = $this->db->from("master_qc_lot_number _")
            ->join("users usr", "usr.id_user = _.creator_id", "left")
            ->join("master_control ctrl", "ctrl.id_control = _.id_control", "left")
            ->join("master_qc_group_level gl", "gl.id_group_level = ctrl.id_group_level", "left")
            ->join("branch b1", "b1.branch_id = _.branch_id", "left")
			->select([
                "_.*",
                "usr.name as creator_name",
                "ctrl.control_name",
                "b1.branch_name",
                "gl.group_level_name as group_level",
                "CONCAT(_.lot_number, ' - ', ctrl.control_name) AS lot_number_text"
            ]);
        return $q;
    }

    function add_lot_number($data) {
        $this->db->insert("master_qc_lot_number", $data);
        $id = $this->db->insert_id();
        $r = $this->one_lot_number(["where" => ["id_lot_number" => $id]]);
        if($id == null || $r == null) {
            return ["status" => false, "message" => "Terjadi kesalahan"];
        }

		// $creator = $this->db->from('users')->where(['id_user' => $this->session->userdata('id_user')])->get()->row();
        $creator = $this->db->from('users')->where(['id_user' => $data['creator_id']])->get()->row();

		$listIdReceiver = [];
		$this->load->model('restapi/user_model');
		$userReceivers = $this->user_model->many_user(['where_in' => ['_.branch_id' => $data['branch_id']]]);
		$userReceivers = json_decode(json_encode($userReceivers));
		foreach ($userReceivers as $key => $user) {
			if ($user->id_position == "7")
				$listIdReceiver[] = $user->id_user;
		}

		$this->load->model('restapi/user_model');
		$sender = $this->user_model->one_user(['where' => ['_.id_user' => $creator->id_user]]);
		$sender_name = '';
		$position_name = '';
		$branch_name = '';
		if($sender != null) {
			$sender_name = $sender['name'];
			$position_name = $sender['position_name'];
			$branch_name = $sender['branch_name'];
		}

		$this->load->library('Notify_lib');
		$nl = new Notify_lib();
		$notif = $nl->send(
				'New lot control QC',
				//$creator->name . ' (@' . $creator->username . ') New lot control QC',
				$sender_name . ' - '.$position_name.' ('.$branch_name.') New lot control QC',
				$creator->id_user,
				$listIdReceiver,
				['id_lot_number' => $id, 'lot_number' => $data['lot_number']],
				'qc_lot_number',
				true
		  );

        return ["status" => true, "message" => "Success"];
    }

    function one_lot_number($args = []) {
        $r = $this->many_lot_number($args);
        if(count($r) == 0) {
            return null;
        }
        $ln = $r[0];
        $ln["control"] = $this->one_control(["where" => ["id_control" => $ln["id_control"]]]);
        return $ln;
    }

    function many_lot_number($args = []){
        // if($args == null) {
        //     $args = [];
        // }
        // if(isset($args["where"]) == false) {
        //     $args["where"] = [];
        // }
        // return $this->baseQueryLotNumber($args)->where($args["where"])->get()->result_array();
        $this->load->helper("myquery");
        return myquery($this->baseQueryLotNumber($args), $args)->get()->result_array();
    }

    function delete_lot_number($where) {
        $targets = $this->many_lot_number(["where" => $where]);
        if(count($targets) == 0) {
            return ["status" => false, "message" => "Data tidak ditemukan"];
        }
        foreach ($targets as $key => $target) {
            $this->db->where(["id_lot_number" => $target["id_lot_number"]])->delete("master_qc_lot_number");
        }
        return ["status" => true, "message" => "Success"];
    }

    function update_lot_number($where, $data) {
        $target = $this->one_lot_number(["where" => $where]);
        if($target == null) {
            return ["status" => false, "message" => "Data tidak ditemukan"];
        }
        $this->db->where(["id_lot_number" => $target["id_lot_number"]])->update("master_qc_lot_number", $data);
        return ["status" => true, "message" => "Success"];
    }
    /* end lot number */

    /* start master control category */
    function baseQueryControlCategory($args = []) {
        $q = $this->db->from('master_qc_control_category _')
            ->join('users usr', 'usr.id_user = _.creator_id', 'left')
            ->join('branch b1', 'b1.branch_id = _.branch_id', 'left')
            ->select([
                '_.*',
                'usr.name as creator_name',
                'usr.username as creator_username',
                'b1.branch_name'
            ]);
        return $q;
    }

    function one_control_category($args = []) {
        $this->load->helper('myquery');
        return myquery($this->baseQueryControlCategory($args), $args)->get()->row();
    }

    function many_control_category($args = []) {
        $this->load->helper('myquery');
        return myquery($this->baseQueryControlCategory($args), $args)->get()->result();
    }

    function add_control_category($data) {
        $this->db->insert('master_qc_control_category', $data);
        $id = $this->db->insert_id();
        if($id == null){
            return ['status'=>false, 'message' => 'Terjadi kesalahan saat menyimpan data control category'];
        }
        $d = $this->one_control_category(['where' => ['_.id_control_category' => $id]]);
        if($d == null) {
            return ['status' => false, 'message' => 'Terjadi kesalahan saat membaca kembali data yang telah disimpan'];
        }
        return ['status'=>true, 'message' => 'Success'];
    }

    function update_control_category($where, $data) {
        $target = $this->one_control_category(['where' => $where]);
        if($target == null) {
            return ['status' => false, 'message' => 'Data tidak ditemukan'];
        }
        $this->db->where(['id_control_category' => $target->id_control_category])->update('master_qc_control_category', $data);
        return ['status' => true, 'message' => 'Success'];
    }

    function delete_control_category($where) {
        $targets = $this->many_control_category(['where' => $where]);
        if(count($targets) == 0) {
            return ['status' => false, 'message' => "Data tidak dapat ditemukan"];
        }

        foreach ($targets as $key => $target) {
            $this->db->where(['id_control_category' => $target->id_control_category])->delete('master_qc_control_category');
        }

        return ['status' => true, 'message' => 'Success'];
    }

    function dt_control_category($args = []){
        $this->load->helper('myquery');
        // $q = $this->baseQueryControlCategory($args);
        $q = myquery($this->baseQueryControlCategory($args), $args);
        $sql = $q->get_compiled_select();
        $this->load->helper('dt');
        return getDataTable([
            'sql' => $sql
        ]);
    }
    /* end master control category */
	
	/* start master urinalisa */
    function add_urinalisa($data) {
        $this->db->insert("master_qc_urinalisa", $data);
        $id = $this->db->insert_id();
        $r = $this->one_urinalisa(["where" =>  ["id_urinalisa" => $id]]);
        if($id == null || $r == null) {
            return ["status" => false, "message" => "Terjadi kesalahan"];
        }
        return ["status" => true, "message" => "Success"];
    }

    function one_urinalisa($args = []) {
        $r = $this->many_urinalisa($args);
        if(count($r) == 0) {
            return null;
        }
        return $r[0];
    }

    function many_urinalisa($args = []) {
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
		if(isset($args["order"]) == false) {
            $args["order"] = [];
        }
        return $this->baseQueryUrinalisa($args)->order_by('_.id_unit','asc')->order_by('_.id_urinalisa','asc')->get()->result_array();//->where($args["where"])
    }

    function baseQueryUrinalisa($args = []) {
        $temp = (isset($args["where"]['id_lot_number'])?"id_lot_number = ".$args["where"]['id_lot_number'].' and':"");
		unset($args["where"]['id_lot_number']);
		return $this->db->from("master_qc_urinalisa _")
            ->join("users usr", "usr.id_user = _.creator_id", "left")
            ->join("master_unit gtest", "gtest.id_unit = _.id_unit", "left")
            ->join("branch b1", "b1.branch_id = _.branch_id", "left")
            ->where($args["where"])
			->select([
                "_.*",
                "usr.name as creator_name",
                "gtest.unit_name as unit_name",
                "b1.branch_name",
				"(select is_red from qc_urinalisa_is_red where ".$temp."  _.id_urinalisa=id_urin limit 1) as is_red2",
            ]);
    }

    function dt_urinalisa($args = []) {
        $this->load->helper('myquery');
        // $sql = $this->baseQueryAlat($args)->get_compiled_select();
        $sql = myquery($this->baseQueryUrinalisa($args), $args)->get_compiled_select();
        $this->load->helper("dt");
        return getDataTable([
            "sql" => $sql
        ]);
    }

    function update_urinalisa($where, $data) {
        $target = $this->one_urinalisa(["where" => $where]);
        if($target == null) {
            return ["status" => false, "message" => "Data tidak ditemukan"];
        }
        $this->db->where(["id_urinalisa" => $target["id_urinalisa"]])->update("master_qc_urinalisa", $data);
        return ["status" => true, "message" => "Success"];
    }

    function delete_urinalisa($where) {
        $targets = $this->many_urinalisa(["where" => $where]);
        if(count($targets) == 0) {
            return ["status" => false, "message" => "Data tidak ditemukan"];
        }
        foreach ($targets as $key => $target) {
            $this->db->where(["id_urinalisa" => $target["id_urinalisa"]])->delete("master_qc_urinalisa");
        }
        return ["status" => true, "message" => "Success"];
    }
    /* end master urinalisa */
	
    
}
