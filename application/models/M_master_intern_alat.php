<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_master_intern_alat extends CI_model
{


    public function save_data($data=[]){
        $this->db->insert('master_intern_alat', $data);
        $saved_id = $this->db->insert_id();
        return $saved_id;
    }

    public function update_data($key,$data=[]){
        $this->db->where(['id' => $key]);
        $update = $this->db->update('master_intern_alat', $data);

        return $update;
    }

    public function get_all_data($params=[]){
    	$this->db->select('m_a.*,mqc_a.alat_name,b.branch_name,usr.name as creator');
    	$this->db->from('master_intern_alat m_a');
    	$this->db->join('branch b','m_a.branch_id=b.branch_id','left');
    	$this->db->join('master_qc_alat mqc_a','m_a.id_alat=mqc_a.id_alat','left');
        $this->db->join('users usr','usr.id_user=m_a.creator','left');
    	$this->db->where(!empty($params['where'])?$params['where']:$params);
    	$query = $this->db->get();
    	return $query;
    }
    
    public function get_alat(){
    	$this->db->select('id_alat,alat_name');
    	$this->db->from('master_qc_alat mqc_a');
    	$query = $this->db->get();
    	return $query;
    }

    public function get_cabang(){
    	$this->db->select('branch_id, branch_name');
    	$this->db->from('branch');
    	$query = $this->db->get();
    	return $query;
    }

    public function hapus_data($param=[]){
        $del = $this->db->delete('master_intern_alat', $param);
        return $del;
    }

    /*------------------------ check list ----------------------------------------------*/

    public function hapus_checklist($param=[]){
        $del = $this->db->delete('intern_checklist', $param);
        return $del;
    }

    public function get_all_checklist($params=[]){
        $this->db->select('m_a.id as mia_id,m_a.branch_id,mqc_a.alat_name,b.branch_name, ic.*, m_a.serial_number,usr.name as creator');
        $this->db->from('intern_checklist ic');
        $this->db->join('master_intern_alat m_a','ic.id_intern_alat=m_a.id','left');
        $this->db->join('branch b','m_a.branch_id=b.branch_id','left');
        $this->db->join('master_qc_alat mqc_a','m_a.id_alat=mqc_a.id_alat','left');
        $this->db->join('users usr','usr.id_user=ic.creator','left');
        $this->db->where(!empty($params['where'])?$params['where']:$params);
        $query = $this->db->get();
        return $query;
    }

    public function save_checklist($data=[]){
        $this->db->insert('intern_checklist', $data);
        $saved_id = $this->db->insert_id();
        return $saved_id;
    }

    public function update_checklist($key,$data=[]){
        $this->db->where(['id' => $key]);
        $update = $this->db->update('intern_checklist', $data);

        return $update;
    }
}
