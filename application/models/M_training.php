<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_training extends CI_model
{

    public function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $result = $this->db->delete($table);
        return $result;
    }

    public function get_all_relations($table)
    {
        return $this->db->get($table);
    }

    // End Inven IT



//Master Training
	public function get_all_training()
    {
        $this->db->select('*');
        $this->db->from('master_training');
        return $this->db->get();
    }

    function many_master_training($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        else{
            $t = '';
            foreach($args["where"] as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            //print_r($t);
            
            $args["where"] = $t;
        }
        if(isset($args["limit"]) == false) {
            $args["limit"] = [];
        }
        if(isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if(isset($args["order"]) == false) {
            $args["order"] = [];
        }
        
        $result = array();
        $row = $this->baseQueryMasterTraining($args)->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();    
        $result = array_merge($result, ['rows' => $row]);
        $result['total'] = $this->baseQueryMasterTraining($args)->where($args["where"])->get()->num_rows();
        
        return $result;
        
    }

    function baseQueryMasterTraining($args = []) {
        return $this->db->from("master_training _")
          //->join("branch br","br.branch_id = _.branch_id")
          //->join("users usr","usr.id_user = _.creator_id")
            ->order_by("_.m_training_id","desc")
            ->select([
                "_.*", 
                "CONCAT(_.duration,' ',_.duration_type) as durasi",
                //"usr.name as creator_name"

            ])->order_by($args["sort"],$args["order"]);
    }

    function add_master_training($data) {
        
        $this->db->insert("master_training", $data);
        $id = $this->db->insert_id();
        

        return ["status" => true, "message" => "Success"];
    }

     function update_master_training($where, $data) {
        $this->db->where($where)->update("master_training", $data);
        return ["status" => true, "message" => "Success"];
    }

    function delete_master_training($where){
        $del = $this->db->delete('master_training', $where);
        return ["status" => true, "message" => "Success"];
    }

//Master Training


//Trainer Board
	public function get_all_training_certificate()
    {
        $this->db->select('*');
        $this->db->from('training_certificate');
        return $this->db->get();
    }

    function many_trainer_board($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        else{
            $t = '';
            foreach($args["where"] as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            //print_r($t);
            
            $args["where"] = $t;
        }
        if(isset($args["limit"]) == false) {
            $args["limit"] = [];
        }
        if(isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if(isset($args["order"]) == false) {
            $args["order"] = [];
        }
        
        $result = array();
        $row = $this->baseQueryTrainerBoard($args)->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();    
        $result = array_merge($result, ['rows' => $row]);
        $result['total'] = $this->baseQueryTrainerBoard($args)->where($args["where"])->get()->num_rows();
        
        return $result;
        
    }

    function baseQueryTrainerBoard($args = []) {
        return $this->db->from("training_certificate _")
          ->join("master_training mt","mt.m_training_id = _.m_training_id")
          //->join("users usr","usr.id_user = _.creator_id")
            ->order_by("_.certificate_id","desc")
            ->select([
                "_.*", 
                //"CONCAT(_.duration,' ',_.duration_type) as durasi",
                "mt.nama_training as nama_training"

            ])->order_by($args["sort"],$args["order"]);
    }

    function add_trainer_board($data) {
        
        $this->db->insert("training_certificate", $data);
        $id = $this->db->insert_id();
        

        return ["status" => true, "message" => "Success"];
    }

     function update_trainer_board($where, $data) {
        $this->db->where($where)->update("training_certificate", $data);
        return ["status" => true, "message" => "Success"];
    }

    function delete_trainer_board($where){
        $del = $this->db->delete('training_certificate', $where);
        return ["status" => true, "message" => "Success"];
    }

//Trainer Board
	
	public function get_all_position_training()
    {
        $this->db->select('*');
        $this->db->from('position');
		$this->db->where('FIND_IN_SET(id_position, (select GROUP_CONCAT(id_position) from master_training))', null);
        return $this->db->get();
    }

	
    /*function getReceivers($id) {
        $q = $this->db->from('generalreports_receiver _')
            ->join('users usr', 'usr.id_user = _.id_receiver', 'left')
            ->join('position p', 'p.id_position = usr.id_position', 'left')
            ->join('departments dep', 'dep.id_department = p.id_department', 'left')
            ->where(['_.id_generalreports' => $id])
            ->select([
                '_.is_view',
                'usr.name',
                'usr.username',
                'p.name_position as position',
                'dep.name as department',
                'usr.id_user'
            ]);
        return $q->get()->result();
    }*/

	
	
	
	
	
	
    function many_training_and_develop($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        else{
            $t = '';
            foreach($args["where"] as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            //print_r($t);
            
            $args["where"] = $t;
        }
        if(isset($args["limit"]) == false) {
            $args["limit"] = [];
        }
        if(isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if(isset($args["order"]) == false) {
            $args["order"] = [];
        }
        
        $result = array();
        $row = $this->baseQueryTrainingDevelopment($args)->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();    
        $result = array_merge($result, ['rows' => $row]);
        $result['total'] = $this->baseQueryTrainingDevelopment($args)->where($args["where"])->get()->num_rows();
        
        return $result;
        
    }

    function baseQueryTrainingDevelopment($args = []) {
        $arr = array();
		$arr[] = "_.id_user";
		$arr[] = "(concat(IFNULL(_.name, ''),' ',IFNULL(_.last_name, ''))) as name";
		//$arr[] = "_.last_name";
		foreach($args['position'] as $v){
			$arr[] = '(select status from training_status a join training b on a.training_id=b.training_id where b.id_user=_.id_user and b.m_training_id='.str_replace('field_','',$v['field']).' order by a.id desc limit 1) as '.$v['field'];
		}
		
		return $this->db->from("users _")
          //->join("master_training mt","mt.m_training_id = _.m_training_id")
          //->join("users usr","usr.id_user = _.creator_id")
          //  ->order_by("_.certificate_id","desc")
            ->select($arr)->order_by($args["sort"],$args["order"]);
    }
	
	
    function many_person_training($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        else{
            $t = '';
            foreach($args["where"] as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            //print_r($t);
            
            $args["where"] = $t;
        }
        if(isset($args["limit"]) == false) {
            $args["limit"] = [];
        }
        if(isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if(isset($args["order"]) == false) {
            $args["order"] = [];
        }
        
        $result = array();
        $row = $this->baseQueryPersonTraining($args)->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();    
        $result = array_merge($result, ['rows' => $row]);
        $result['total'] = $this->baseQueryPersonTraining($args)->where($args["where"])->get()->num_rows();
        
        return $result;
        
    }

    function baseQueryPersonTraining($args = []) {
        return $this->db->from("users _")
          ->join("position p","_.id_position=p.id_position")
          ->join("branch b","_.branch_id=b.branch_id")
          //  ->order_by("_.certificate_id","desc")
            ->select( 
			"_.*,b.branch_name,p.name_position"
			)->order_by($args["sort"],$args["order"]);
    }

    function many_progress_training($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        else{
            $t = '';
            foreach($args["where"] as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            //print_r($t);
            
            $args["where"] = $t;
        }
        if(isset($args["limit"]) == false) {
            $args["limit"] = [];
        }
        if(isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if(isset($args["order"]) == false) {
            $args["order"] = [];
        }
        
        $result = array();
        $row = $this->baseQueryProgressTraining($args)->where($args["where"])->get()->result_array();    
        $result = array_merge($result, ['rows' => $row]);
        $result['total'] = $this->baseQueryProgressTraining($args)->where($args["where"])->get()->num_rows();
        
        return $result;
        
    }

    function baseQueryProgressTraining($args = []) {
        return $this->db->from("training _")
          //->join("training_status t","_.training_id=t.training_id")
          ->join("master_training m","_.m_training_id=m.m_training_id")
          //  ->order_by("_.certificate_id","desc")
            ->select( 
			"_.*,m.nama_training,(select status from training_status where _.training_id=training_id order by id desc limit 1) as status"
			)->order_by($args["sort"],$args["order"]);
    }

    function many_training_person($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        else{
            $t = '';
            foreach($args["where"] as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            //print_r($t);
            
            $args["where"] = $t;
        }
        if(isset($args["limit"]) == false) {
            $args["limit"] = [];
        }
        if(isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if(isset($args["order"]) == false) {
            $args["order"] = [];
        }
        
        $result = array();
        $row = $this->baseQueryTrainingPerson($args)->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();    
        $result = array_merge($result, ['rows' => $row]);
        $result['total'] = $this->baseQueryTrainingPerson($args)->where($args["where"])->get()->num_rows();
        
        return $result;
        
    }

    function baseQueryTrainingPerson($args = []) {
        return $this->db->from("training _")
          ->join("master_training t","_.m_training_id=t.m_training_id")
          ->join("users c","_.id_user=c.id_user")
		  ->join("training_certificate d","_.id_user=d.id_user and _.m_training_id=d.m_training_id",'left')
          //  ->order_by("_.certificate_id","desc")
            ->select( 
			"t.*,c.name,c.last_name,d.mulai_training,d.selesai_training,d.path"
			)->order_by($args["sort"],$args["order"]);
    }

}
