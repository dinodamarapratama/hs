<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_complaints extends CI_model
{

  public function get_where($table, array $where, $limit = null, $offset = null)
    {
      $this->db->order_by('created_at', 'desc');
        return $this->db->get_where($table, $where, $limit, $offset);
    }


    public function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    public function insert_batch($table, $data)
    {
        $this->db->insert_batch($table, $data);
    }

    public function get_data_like($where, $colum, $table)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->like($colum, $where);
        return $this->db->get();
    }

    public function get_data_join()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('position', 'users.id_position = position.id');
        return $this->db->get();
    }
	
	public function get_all_sent_to($id)
    {
        $this->db->select('name, (select name_position from position where users.id_position = position.id_position) as name_position, id_receiver');
        $this->db->from('complaint_receiver');
        $this->db->join('users', 'complaint_receiver.id_receiver = users.id_user');
        //$this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('complaint_receiver.complaint_id', $id);

        return $this->db->get();

    }
	
	public function total_complaints_received($id /* user id */, $complaint_id = null, $position = null, $search = null, $args)
    {
        $this->db->select('complaints.id'
        );
        $this->db->from('complaints');
        $this->db->join('complaint_receiver', 'complaint_receiver.complaint_id = complaints.id');
        $this->db->join('users', 'complaints.creator_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        $this->db->join('complaint_attributes', 'complaint_attributes.complaint_id = complaints.id');
        if (!empty($complaint_id)) {
            $this->db->where('complaint_receiver.complaint_id', $complaint_id);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('complaints.jenis_komplain',$search,'both');
            $this->db->or_like('complaints.deskripsi',$search,'both');
            $this->db->or_like('complaints.created_at',$search,'both');
            $this->db->or_like('complaints.nama_pasien',$search,'both');
            $this->db->or_like('complaints.nomor_registrasi',$search,'both');
        }


        $this->db->where('complaint_receiver.receiver_id', $id)
            ->where("complaint_receiver.deleted", "0");
        // $this->db->where('complaint_receiver.read <>', '1');
        $this->db->where('complaints.status','OPEN');
        $this->db->where('complaints.status <>','CLOSED');
        $this->db->group_by('complaints.id');
		
        return $this->db->get();
    }

    public function total_complaints_sent($id /* user id */, $complaint_id = null, $position = null, $search = null, $args)
    {
        $this->db->select('complaints.id');
		$this->db->from('complaints');
        //$this->db->join('complaint_receiver', 'complaint_receiver.complaint_id = complaints.id');
        $this->db->join('users', 'complaints.creator_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($complaint_id)) {
            //$this->db->where('complaint_receiver.complaint_id', $complaint_id);
			$this->db->where('complaints.id', $complaint_id);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('complaints.jenis_komplain',$search,'both');
            $this->db->or_like('complaints.deskripsi',$search,'both');
            $this->db->or_like('complaints.created_at',$search,'both');
            $this->db->or_like('complaints.nama_pasien',$search,'both');
            $this->db->or_like('complaints.nomor_registrasi',$search,'both');
        }
        $this->db->where('complaints.creator_id', $id);
        $this->db->where('complaints.status', 'OPEN');
        // $this->db->where('complaints.status <>', 'CLOSED');
        // $this->db->where('complaint_receiver.read <>', '1');
        return $this->db->get();
    }
	
	public function total_complaints_archived($id /* user id */, $complaint_id = null, $position = null, $search = null, $args)
    {
        
		$this->db->select('complaints.id');
        $this->db->from('complaints');
        //$this->db->join('complaint_receiver', 'complaint_receiver.complaint_id = complaints.id');
        $this->db->join('users', 'complaints.creator_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($complaint_id)) {
            //$this->db->where('complaint_receiver.complaint_id', $complaint_id);
			$this->db->where('complaints.id', $complaint_id);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('complaints.jenis_komplain',$search,'both');
            $this->db->or_like('complaints.deskripsi',$search,'both');
            $this->db->or_like('complaints.created_at',$search,'both');
            $this->db->or_like('complaints.nama_pasien',$search,'both');
            $this->db->or_like('complaints.nomor_registrasi',$search,'both');
        }
        $this->db->group_start()->or_where(["complaints.creator_id" => $id])->or_where(["complaints.id in (select complaint_id from complaint_receiver where receiver_id=".$id.")" => null])->group_end();//->or_where(["complaint_receiver.receiver_id" => $id])
		
		$this->db->where([
			"UPPER(complaints.status)" => 'CLOSED',
			// "archived" => 1
		]);
        return $this->db->get();
    }


    public function get_complaints_received($id /* user id */, $complaint_id = null, $position = null, $search = null, $args)
    {
        /* tandai notifikasi untuk complaint ini sudah dibaca */

        // $this->db->where([
        //     "to_id" => $id
        // ])
        // ->like("data", '"complaint_id":' . $complaint_id)
        // ->group_start()
        //     ->or_where(["read" => 0])
        //     ->or_where(["read" => null])
        // ->group_end()
        // ->where(["anime" => "naruto"])
        // ->update("notifications", ["read" => 1]);
        if($complaint_id != null) {
            $this->load->library("Notify_lib");
            $nl = new Notify_lib();
            $nl->mark_read($id, "complaint", $complaint_id);

            $this->db->where([
                "complaint_id" => $complaint_id,
                "receiver_id" => $id
            ])->update("complaint_receiver", ["read" => 1]);
        }


        $this->db->select('complaints.*,
            complaints.status as status,
            users.*,
            position.*,
            branch.*,
            complaint_receiver.complaint_id,
            complaint_receiver.receiver_id,
            complaint_receiver.read as read,
            complaints.status as status'
        );
        $this->db->from('complaints');
        $this->db->join('complaint_receiver', 'complaint_receiver.complaint_id = complaints.id');
        $this->db->join('users', 'complaints.creator_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        $this->db->join('complaint_attributes', 'complaint_attributes.complaint_id = complaints.id');
        if (!empty($complaint_id)) {
            $this->db->where('complaint_receiver.complaint_id', $complaint_id);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('complaints.jenis_komplain',$search,'both');
            $this->db->or_like('complaints.deskripsi',$search,'both');
            $this->db->or_like('complaints.created_at',$search,'both');
            $this->db->or_like('complaints.nama_pasien',$search,'both');
            $this->db->or_like('complaints.nomor_registrasi',$search,'both');
        }


        $this->db->where('complaint_receiver.receiver_id', $id)
            ->where("complaint_receiver.deleted", "0");
        // $this->db->where('complaint_receiver.read <>', '1');
        $this->db->where('complaints.status','OPEN');
        $this->db->where('complaints.status <>','CLOSED');
        $this->db->order_by('complaint_receiver.id', 'desc');
        $this->db->group_by('complaints.id');
		
		$limit = 10;
		$offset = 0;
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		if(isset($args['offset'])) {
			$offset = $args['offset'];
			unset($args['offset']);
		}
		
		
		$this->db->limit($limit,$offset);
        return $this->db->get();
    }

    public function get_complaints_sent($id /* user id */, $complaint_id = null, $position = null, $search = null, $args)
    {
        $this->db->select('complaints.*,users.*,position.*,branch.*,
            complaints.status as status,complaints.id as complaint_id');//complaint_receiver.complaint_id,complaint_receiver.receiver_id,
        $this->db->from('complaints');
        //$this->db->join('complaint_receiver', 'complaint_receiver.complaint_id = complaints.id');
        $this->db->join('users', 'complaints.creator_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($complaint_id)) {
            //$this->db->where('complaint_receiver.complaint_id', $complaint_id);
			$this->db->where('complaints.id', $complaint_id);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('complaints.jenis_komplain',$search,'both');
            $this->db->or_like('complaints.deskripsi',$search,'both');
            $this->db->or_like('complaints.created_at',$search,'both');
            $this->db->or_like('complaints.nama_pasien',$search,'both');
            $this->db->or_like('complaints.nomor_registrasi',$search,'both');
        }
        $this->db->where('complaints.creator_id', $id);
        $this->db->where('complaints.status', 'OPEN');
        // $this->db->where('complaints.status <>', 'CLOSED');
        // $this->db->where('complaint_receiver.read <>', '1');
        $this->db->order_by('complaints.id', 'desc');
		
		$limit = 10;
		$offset = 0;
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		if(isset($args['offset'])) {
			$offset = $args['offset'];
			unset($args['offset']);
		}
		
		
		$this->db->limit($limit,$offset);
        return $this->db->get();
    }

    public function get_complaints_archived($id /* user id */, $complaint_id = null, $position = null, $search = null, $args)
    {
        $this->db->select('complaints.*,users.*,position.*,branch.*,
            complaints.status as status,complaints.id as complaint_id');//complaint_receiver.complaint_id,complaint_receiver.receiver_id,
        $this->db->from('complaints');
        //$this->db->join('complaint_receiver', 'complaint_receiver.complaint_id = complaints.id');
        $this->db->join('users', 'complaints.creator_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($complaint_id)) {
            //$this->db->where('complaint_receiver.complaint_id', $complaint_id);
			$this->db->where('complaints.id', $complaint_id);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('complaints.jenis_komplain',$search,'both');
            $this->db->or_like('complaints.deskripsi',$search,'both');
            $this->db->or_like('complaints.created_at',$search,'both');
            $this->db->or_like('complaints.nama_pasien',$search,'both');
            $this->db->or_like('complaints.nomor_registrasi',$search,'both');
        }
        $this->db->group_start()->or_where(["complaints.creator_id" => $id])->or_where(["complaints.id in (select complaint_id from complaint_receiver where receiver_id=".$id.")" => null])->group_end();//->or_where(["complaint_receiver.receiver_id" => $id])
		$this->db->where([
			"UPPER(complaints.status)" => 'CLOSED',
			// "archived" => 1
		]);
        $this->db->order_by('complaints.id', 'desc');
		
		$limit = 10;
		$offset = 0;
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		if(isset($args['offset'])) {
			$offset = $args['offset'];
			unset($args['offset']);
		}
		
		
		$this->db->limit($limit,$offset);
        return $this->db->get();
    }

    // function get_send(){
    //     return $this->
    // }
     public function get_notification_complaints($id)
    {
        return $this->db->from("notifications")
            ->where([
                "to_id" => $id
            ])
            ->group_start()
                ->or_where(["read" => null])
                ->or_where(["read" => 0])
            ->group_end()
            // ->where(["anime" => "naruto"])
            ->get();

        // $this->db->select('*');
        // $this->db->from('notifications');
        // $this->db->where('to_id', $id);
        // $this->db->where('read', 0);
        // $this->db->where('read <>', 1);
        // $this->db->order_by('id', 'desc');

        // return $this->db->get();
    }

    function get_attributes($id_complaint) {
        return $this->db->from("complaint_attributes")
            ->where(["complaint_id" => $id_complaint])
            ->get()->result_array();
    }

    function get_receivers($id_complaint) {
        return $this->db->from("complaint_receiver cr")
            ->join("users usr", "usr.id_user = cr.receiver_id", "left")
            ->where(["complaint_id" => $id_complaint])
            ->select(["cr.*", "usr.name"])
            ->get()->result_array();
    }

    function update($where, $data, $attr) {
        $targets = $this->db->from("complaints")
            ->where($where)
            ->get()->result_array();

        if(count($targets) < 1) {
            return ["status" => false, "message" => "Data Complaint not found"];
        }

        $target = $targets[0];

        $isDataEmpty = true;
        foreach ($data as $key => $value) {
            $isDataEmpty = false;
            break;
        }

        if($isDataEmpty == false) {
            $this->db->where([
                "id" => $target["id"]
            ])->update("complaints", $data);
        }

        // $this->db->where(["complaint_id" => $target["id"]])->delete("complaint_attributes");

        foreach ($attr as $key => $value) {
            $this->db->where([
                "key" => $key,
                "complaint_id" => $target["id"]
            ])->delete("complaint_attributes");

            $this->db->insert("complaint_attributes", [
                "complaint_id" => $target["id"],
                "key" => $key,
                "value" => $value
            ]);
        }

        return ["status" => true, "message" => "Data berhasil diupdate"];
    }

    function list_archived($user_id){
        $q = $this->db->from("complaints c")
                ->join("complaint_receiver cr", "cr.complaint_id = c.id", "left")
                ->join("users creator", "creator.id_user = c.creator_id", "left")
                ->join("branch br", "br.branch_id = creator.branch_id", "left")
                ->group_start()
                    ->or_where(["c.creator_id" => $user_id])
                    ->or_where(["cr.receiver_id" => $user_id])
                ->group_end()
                ->where([
                    "UPPER(c.status)" => 'CLOSED',
                    // "archived" => 1
                ])
                ->select([
                    "c.*",
                    "creator.name as creator",
                    "br.branch_name as branch"
                ])
                ->group_by(['c.id']);
		
		
        return $q->get()->result_array();
    }

    function detail_archived($id_complaint, $user_id) {
        $result = $this->db->from("complaints c")
                ->join("complaint_receiver cr", "cr.complaint_id = c.id", "left")
                ->join("users creator", "creator.id_user = c.creator_id", "left")
                ->where(["c.id" => $id_complaint])
                ->group_start()
                    ->or_where([
                        "c.creator_id" => $user_id,
                        "cr.receiver_id" => $user_id
                    ])
                ->group_end()
                ->select([
                    "c.*",
                    "creator.image as photo_profile"
                ])
                ->get()->result_array();


        /* get receivers and get attachment */
        foreach ($result as $key => $row) {
            $row["sent_to"] = $this->db->from("complaint_receiver cr")
                ->join("users rcv", "rcv.id_user = cr.receiver_id", "left")
                ->where([
                    "complaint_id" => $row["id"]
                ])
                ->select([
                    "cr.*",
                    "rcv.name"
                ])
                ->get()->result_array();

            $row["file_attachment"] = $this->db->from("complaint_attachment ca")
                ->where([
                    "ca.complaint_id" => $row["id"],
                ])
                ->get()->result_array();

            $result[$key] = $row;
        }

        return $result;
    }

}
