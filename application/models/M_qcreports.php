<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_qcreports extends CI_model
{

    public function last_query_db(){
        return $this->db->last_query();
    }
    
    public function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $this->db->where($table);
    }

    public function insert_batch($table, $data)
    {
        $this->db->insert_batch($table, $data);
    }

    public function get_data_like($where, $colum, $table)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->like($colum, $where);
        return $this->db->get();
    }

    public function get_data_join()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('position', 'users.id_position = position.id');
        return $this->db->get();
    }

    public function get_sent_to($id)
    {
        $this->db->select('name, name_position, id_receiver');
        $this->db->from('qcreports_receiver');
        $this->db->join('users', 'qcreports_receiver.id_receiver = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('qcreports_receiver.id_receiver', $id);

        return $this->db->get();

    }
	
	public function get_many_alat($id)
    {
        $this->db->select('id, id_alat');
        $this->db->from('qcreports_alat');
        $this->db->where('qcreports_alat.qcreport_id', $id);
		 $this->db->where('qcreports_alat.status', 1);

        return $this->db->get();

    }
	
    public function get_sent_count($id)
    {
        $this->db->from('qcreports_receiver');
        $this->db->where('qcreports_receiver.id_qcreports', $id);

        return $this->db->get()->num_rows();
    }

    public function get_read_count($id)
    {
        $this->db->from('qcreports_receiver');
        $this->db->where('qcreports_receiver.id_qcreports', $id);
        $this->db->where('qcreports_receiver.is_view', '1');

        return $this->db->get()->num_rows();
    }

    public function get_user_read($id)
    {
        $this->db->select('name, name_position, id_receiver, is_view');
        $this->db->from('qcreports_receiver');
        $this->db->join('users', 'qcreports_receiver.id_receiver = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('qcreports_receiver.id_qcreports', $id);

        return $this->db->get();

    }

    
    public function get_data_received($id, $id_qcreports = null, $position = null, $search = null)
    {
        $this->db->select('qcreports.*,qcreports.status as status,users.*,position.*,branch.*,
            qcreports_receiver.id_qcreports,
            qcreports_receiver.id_receiver,qcreports_receiver.is_view as read,qcreports.status as status');
        $this->db->from('qcreports');
        $this->db->join('qcreports_receiver', 'qcreports_receiver.id_qcreports = qcreports.id');
        $this->db->join('users', 'qcreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($id_qcreports)) {
            $this->db->where('qcreports_receiver.id_qcreports', $id_qcreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('qcreports.kategori',$search,'both');
            $this->db->or_like('qcreports.title',$search,'both');
            $this->db->or_like('qcreports.date',$search,'both');
            $this->db->or_like('qcreports.description',$search,'both');
            

        }
        $this->db->where('qcreports_receiver.id_receiver', $id);
        /* data belum dihapus oleh receiver */
        $this->db->where('qcreports_receiver.is_closed', '0');
        $this->db->where('upper(qcreports.status)', 'OPEN');
        /* data belum dihapus oleh creator */
        $this->db->where_in('qcreports.is_view', [0,1]);
        // $this->db->where('dailyreports.status <>', 'Close');
        $this->db->order_by('qcreports_receiver.id', 'desc');

        return $this->db->get();
    }

    public function get_data_sent($id, $id_qcreports = null, $position = null, $search = null)
    {
        $this->db->select('qcreports.*,usr.*,
            qcreports_receiver.id_qcreports,qcreports_receiver.id_receiver,qcreports.status as status');
        $this->db->from('qcreports');
        $this->db->join('qcreports_receiver', 'qcreports_receiver.id_qcreports = qcreports.id', 'left');
        // $this->db->join('users', 'qcreports.from_id = users.id_user');
        $this->db->join('v_user usr', 'qcreports.from_id = usr.id_user', 'left');
        // $this->db->join('position', 'users.id_position = position.id_position');
        // $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($id_qcreports)) {
            // $this->db->where('qcreports_receiver.id_qcreports', $id_qcreports);
            $this->db->where('qcreports.id', $id_qcreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->like('usr.name',$search,'both');
            $this->db->or_like('usr.username',$search,'both');
            $this->db->or_like('qcreports.kategori',$search,'both');
            $this->db->or_like('qcreports.title',$search,'both');
            $this->db->or_like('qcreports.date',$search,'both');
            $this->db->or_like('qcreports.description',$search,'both');
            
        }
        $this->db->where('qcreports.from_id', $id);
        $this->db->where('qcreports.id_draft', '0');
        $this->db->where('upper(qcreports.status)', 'OPEN');
        // $this->db->where('qcreports.status <>', 'Close');
        // $this->db->where('qcreports.id_draft <>', '1');
        // $this->db->where('qcreports.is_view <>', '1');
        /* data belum dihapus oleh creator */
        $this->db->where_in('qcreports.is_view', [0,1]);
        $this->db->order_by('qcreports.id', 'desc');
        // echo $this->db->get_compiled_select();
        // exit();
        return $this->db->get();
    }

    public function get_data_archived($id, $id_qcreports = null, $position = null, $search = null)
    {
        $this->db->select('qcreports.*,users.*,position.*,branch.*,
            qcreports_receiver.id_qcreports,qcreports_receiver.id_receiver,qcreports.status as status');
        $this->db->from('qcreports');
        $this->db->join('qcreports_receiver', 'qcreports_receiver.id_qcreports = qcreports.id');
        $this->db->join('users', 'qcreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($id_qcreports)) {
            $this->db->where('qcreports_receiver.id_qcreports', $id_qcreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('qcreports.kategori',$search,'both');
            $this->db->or_like('qcreports.title',$search,'both');
            $this->db->or_like('qcreports.date',$search,'both');
            $this->db->or_like('qcreports.description',$search,'both');
            $this->db->group_end(); 
        }
        
         $this->db->where('upper(qcreports.status)', 'CLOSE');
        // $this->db->where('dailyreports.status <>', 'Open');
        // $this->db->where('dailyreports.is_view <>', '1');
        $this->db->group_start();
            $this->db->or_group_start();
                $this->db->where('qcreports.from_id', $id);
                /* data belum dihapus oleh creator */
                $this->db->where_in('qcreports.is_view', [0,1]);
            $this->db->group_end();
            $this->db->or_group_start();
                $this->db->where('qcreports_receiver.id_receiver', $id);
                /* data belum dihapus oleh receiver */
                $this->db->where('qcreports_receiver.is_closed', '0');
            $this->db->group_end();
        $this->db->group_end();
        $this->db->order_by('qcreports.id', 'desc');
        return $this->db->get();
    }

    public function get_data_draft($id, $id_qcreports = null, $position = null, $search = null)
    {
        $this->db->select('qcreports.*,users.*,position.*,branch.*,
            qcreports_receiver.id_qcreports,qcreports_receiver.id_receiver,qcreports.status as status');
        $this->db->from('qcreports');
        $this->db->join('qcreports_receiver', 'qcreports_receiver.id_qcreports = qcreports.id');
        $this->db->join('users', 'qcreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($id_qcreports)) {
            $this->db->where('qcreports_receiver.id_qcreports', $id_qcreports);
            // $this->db->where('qcreports.id', $id_qcreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
           $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('qcreports.kategori',$search,'both');
            $this->db->or_like('qcreports.title',$search,'both');
            $this->db->or_like('qcreports.date',$search,'both');
            $this->db->or_like('qcreports.description',$search,'both');
            
        }
        $this->db->where('qcreports.from_id', $id);
        // $this->db->where('qcreports.is_view <>', '1');
        $this->db->where('qcreports.id_draft <>', '0');
        $this->db->where('qcreports.id_draft', '1');
        $this->db->order_by('qcreports.id', 'desc');
        return $this->db->get();
    }

    public function get_user_replay($id)
    {
        $this->db->select('*');
        $this->db->from('qcreports');
        $this->db->join('qcreports_reply', 'qcreports.id = qcreports_reply.id_qcreports');
        $this->db->join('users', 'qcreports_reply.sender = users.id_user');
        $this->db->where('qcreports.id', $id);

        return $this->db->get();
    }

    public function get_to_id($id,$user_id)
    {
        $this->db->select('*');
        $this->db->from('qcreports_receiver');
        $this->db->join('qcreports', 'qcreports.id = qcreports_receiver.id_qcreports');
        $this->db->where('qcreports.id', $id);
        $this->db->where('qcreports_receiver.id_receiver', $user_id);

        return $this->db->get();
    }

    public function get_from_id($id)
    {
        $this->db->select('*');
        $this->db->from('qcreports_receiver');
        $this->db->join('qcreports', 'qcreports.id = qcreports_receiver.id_qcreports');
        $this->db->where('qcreports.id', $id);

        return $this->db->get();
    }

    public function get_notification($id)
    {
        return $this->db->from("notifications")
            ->where([
                "to_id" => $id
            ])
            ->group_start()
                ->or_where(["read" => null])
                ->or_where(["read" => 0])
            ->group_end()
            // ->where(["anime" => "naruto"])
            ->get();
        
        // $this->db->select('*');
        // $this->db->from('notifications');
        // $this->db->where('to_id', $id);
        // $this->db->where('read', 0);
        // $this->db->where('read <>', 1);
        // $this->db->order_by('id', 'desc');
        
        // return $this->db->get();
    }


    public function get_user_hrd()
    {
        $this->db->select('id_user,name');
        $this->db->from('users');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('name_position','STAFF HRD');

        return $this->db->get();
    } 

    public function testing($array)
    {
        return print("<pre>" . print_r($array, true) . "</pre>");
    }
	
	function load_data($params=[], $rows=0, $offset=10,$all=0,$table,$id)
   {
      $this->db->select(['*']);
      $this->db->from($table);
      if (isset($params['in_param'])) {
        foreach ($params['in_param'] as $key => $value) {
            $this->db->where_in($key,$value);  
        }
        unset($params['in_param']);
      }
      $this->db->where($params);
      if (!$all) {
        $this->db->limit($rows, $offset);  
      }
      $this->db->order_by($id, 'DESC');
      //$this->db->join('branch', 'users.branch_id = branch.branch_id');
      $query = $this->db->get();
      return $query->result_array();
   }
	
}