<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
*
*/
class M_masterdoc extends CI_model
{

public function edit_data($where, $table)
{
return $this->db->get_where($table, $where);
}

public function get_data($table)
{
return $this->db->get($table);
}

public function insert_data($data, $table)
{
$this->db->insert($table, $data);
return $this->db->insert_id();
}

public function update_data($where, $data, $table)
{
$this->db->where($where);
$this->db->update($table, $data);
}

public function delete_data($where, $table)
{
$this->db->where($where);
$result = $this->db->delete($table);
return $result;
}

public function get_all_relations($table)
{
return $this->db->get($table);
}

public function selwhere($table,$sel,$com,$field,$sb){
	$this->db->select($sel);
	$this->db->order_by($field,$sb);
	return $this->db->get_where($table, $com);
}

// START KALK

public function get_all_masterkalk()
{
$this->db->select('mk.*,usr.*,usr.name as fullname,mk.id as id');
$this->db->from('master_kalk mk');
$this->db->join('users usr','usr.id_user = mk.creator_id');
return $this->db->get();
}

public function edit_data_masterkalk($where, $table)
{
$this->db->select('kalk.*');
$this->db->from('master_kalk kalk');
$this->db->where($where);
return $this->db->get();
}

public function get_masterkalk_easy($args = [])
{
	$this->load->helper("myquery");
	if($args == null) {
		$args = [];
	}
	if(isset($args["where"]) == false) {
		$args["where"] = [];
	}
	else{
		$t = '';
		foreach($args["where"] as $v){
			$t = ($t=='')?$v:$v.' AND '.$t;
		}
		//print_r($t);
		
		$args["where"] = $t;
	}
	if(isset($args["limit"]) == false) {
		$args["limit"] = [];
	}
	if(isset($args["start"]) == false) {
		$args["start"] = [];
	}
	if(isset($args["order"]) == false) {
		$args["order"] = [];
	}
	
	$result = array();
	$row = $this->db->select('mk.*,usr.*,usr.name as fullname,mk.id as id')->from('master_kalk mk')->join('users usr','usr.id_user = mk.creator_id')->order_by($args["sort"],$args["order"])->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();    
	$result = array_merge($result, ['rows' => $row]);
	$result['total'] = $this->db->select('mk.*,usr.*,usr.name as fullname,mk.id as id')->from('master_kalk mk')->join('users usr','usr.id_user = mk.creator_id')->where($args["where"])->get()->num_rows();
	
	return $result;
}

// END KALK


// START DEPT
public function get_all_masterdpt()
{
$this->db->select('dpt.*,usr.*,usr.name as fullname,dpt.name as dpt_name,dpt.id_department as id_department');
$this->db->from('departments dpt');
$this->db->join('users usr','usr.id_user = dpt.creator_id');
return $this->db->get();
}

public function edit_data_masterdpt($where, $table)
{
$this->db->select('dept.*');
$this->db->from('departments dept');
$this->db->where($where);
return $this->db->get();
}

public function get_masterdpt_easy($args = [])
{
	$this->load->helper("myquery");
	if($args == null) {
		$args = [];
	}
	if(isset($args["where"]) == false) {
		$args["where"] = [];
	}
	else{
		$t = '';
		foreach($args["where"] as $v){
			$t = ($t=='')?$v:$v.' AND '.$t;
		}
		//print_r($t);
		
		$args["where"] = $t;
	}
	if(isset($args["limit"]) == false) {
		$args["limit"] = [];
	}
	if(isset($args["start"]) == false) {
		$args["start"] = [];
	}
	if(isset($args["order"]) == false) {
		$args["order"] = [];
	}
	
	$result = array();
	$row = $this->db->select('dpt.*,usr.*,usr.name as fullname,dpt.name as dpt_name,dpt.id_department as id_department')->from('departments dpt')->join('users usr','usr.id_user = dpt.creator_id')->order_by($args["sort"],$args["order"])->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();    
	$result = array_merge($result, ['rows' => $row]);
	$result['total'] = $this->db->select('dpt.*,usr.*,usr.name as fullname,dpt.name as dpt_name,dpt.id_department as id_department')->from('departments dpt')->join('users usr','usr.id_user = dpt.creator_id')->where($args["where"])->get()->num_rows();
	
	return $result;
}

// END DEPT

// START BAGIAN
public function get_all_masterbagian()
{
$this->db->select('bg.*,usr.*,usr.name as fullname,bg.name as bg_name,bg.id as id,dpt.name as dpt_name');
$this->db->from('master_doc_kategori bg');
$this->db->join('users usr','usr.id_user = bg.creator_id');
$this->db->join('departments dpt','dpt.id_department = bg.id_department');
return $this->db->get();
}

public function edit_data_masterbagian($where, $table)
{
$this->db->select('bg.*');
$this->db->from('master_doc_kategori bg');
$this->db->where($where);
return $this->db->get();
}

public function get_masterbagian_easy($args = [])
{
	$this->load->helper("myquery");
	if($args == null) {
		$args = [];
	}
	if(isset($args["where"]) == false) {
		$args["where"] = [];
	}
	else{
		$t = '';
		foreach($args["where"] as $v){
			$t = ($t=='')?$v:$v.' AND '.$t;
		}
		//print_r($t);
		
		$args["where"] = $t;
	}
	if(isset($args["limit"]) == false) {
		$args["limit"] = [];
	}
	if(isset($args["start"]) == false) {
		$args["start"] = [];
	}
	if(isset($args["order"]) == false) {
		$args["order"] = [];
	}
	
	$result = array();
	$row = $this->db->select('bg.*,usr.*,usr.name as fullname,bg.name as bg_name,bg.id as id,dpt.name as dpt_name')->from('master_doc_kategori bg')->join('users usr','usr.id_user = bg.creator_id')->join('departments dpt','dpt.id_department = bg.id_department')->order_by($args["sort"],$args["order"])->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();    
	$result = array_merge($result, ['rows' => $row]);
	$result['total'] = $this->db->select('bg.*,usr.*,usr.name as fullname,bg.name as bg_name,bg.id as id,dpt.name as dpt_name')->from('master_doc_kategori bg')->join('users usr','usr.id_user = bg.creator_id')->join('departments dpt','dpt.id_department = bg.id_department')->where($args["where"])->get()->num_rows();
	
	return $result;
}

// END BAGIAN




// START TYPE

public function get_all_mastertype()
{
$this->db->select('type.*,usr.*,usr.name as fullname,type.type_id as type_id');
$this->db->from('master_doc_type type');
$this->db->join('users usr','usr.id_user = type.creator_id');
return $this->db->get();
}

public function edit_data_mastertype($where, $table)
{
$this->db->select('type.*');
$this->db->from('master_doc_type type');
$this->db->where($where);
return $this->db->get();
}

public function get_mastertype_easy($args = [])
{
	$this->load->helper("myquery");
	if($args == null) {
		$args = [];
	}
	if(isset($args["where"]) == false) {
		$args["where"] = [];
	}
	else{
		$t = '';
		foreach($args["where"] as $v){
			$t = ($t=='')?$v:$v.' AND '.$t;
		}
		//print_r($t);
		
		$args["where"] = $t;
	}
	if(isset($args["limit"]) == false) {
		$args["limit"] = [];
	}
	if(isset($args["start"]) == false) {
		$args["start"] = [];
	}
	if(isset($args["order"]) == false) {
		$args["order"] = [];
	}
	
	$result = array();
	$row = $this->db->select('type.*,usr.*,usr.name as fullname,type.type_id as type_id')->from('master_doc_type type')->join('users usr','usr.id_user = type.creator_id')->order_by($args["sort"],$args["order"])->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();    
	$result = array_merge($result, ['rows' => $row]);
	$result['total'] = $this->db->select('type.*,usr.*,usr.name as fullname,type.type_id as type_id')->from('master_doc_type type')->join('users usr','usr.id_user = type.creator_id')->where($args["where"])->get()->num_rows();
	
	return $result;
}

// END TYPE


// START MASTER DOC

public function get_all_masterdoc()
{
$this->db->select('md.*,usr.*,usr.name as updated_by,md.doc_id as doc_id,mk.abbr as kalk_name,dept.name as dept_name,bg.name as bagian_name,type.type_name as type_name,md.iso_9001 as iso1');
$this->db->from('master_doc md');
$this->db->join('master_kalk mk','mk.id = md.kalk_id');
$this->db->join('departments dept','dept.id_department = md.dept_id');
$this->db->join('master_doc_kategori bg','bg.id = md.bagian_id');
$this->db->join('users usr','usr.id_user = md.updated_by');
$this->db->join('master_doc_type type','type.type_id = md.type_id');
return $this->db->get();
}

public function edit_data_masterdoc($where, $table)
{
$this->db->select('doc.*');
$this->db->from('master_doc doc');
$this->db->where($where);
return $this->db->get();
}

public function get_masterdoc_easy($args = [])
{
	$this->load->helper("myquery");
	if($args == null) {
		$args = [];
	}
	if(isset($args["where"]) == false) {
		$args["where"] = [];
	}
	else{
		$t = '';
		foreach($args["where"] as $v){
			$t = ($t=='')?$v:$v.' AND '.$t;
		}
		//print_r($t);
		
		$args["where"] = $t;
	}
	if(isset($args["limit"]) == false) {
		$args["limit"] = [];
	}
	if(isset($args["start"]) == false) {
		$args["start"] = [];
	}
	if(isset($args["order"]) == false) {
		$args["order"] = [];
	}
	
	$result = array();
	
	//(select case when (locked_by=0) then md.doc_id when (locked_by = '.$this->session->userdata('id_user').') then md.doc_id else 0 end)
	
	$row = $this->db->select('md.*,usr.*,usr.name as updated_by,md.doc_id as doc_id,(select abbr from master_kalk where id = md.kalk_id) as kalk_name,dept_name,(select name from master_doc_kategori where id=md.bagian_id) as bagian_name,(select type_name from master_doc_type where type_id=md.type_id) as type_name,md.iso_9001 as iso1,(select case when ((select COUNT(doc_id) from master_doc_log where status=1 and doc_id=md.doc_id)=0) then md.doc_id when ((select count(creator_id) from master_doc_log where status=1 and doc_id=md.doc_id and creator_id = '.$this->session->userdata('id_user').') > 0) then md.doc_id else 0 end) as raw_id')->from('master_doc md')
	//->join('master_kalk mk','mk.id = md.kalk_id')
	//->join('departments dept','dept.id_department = md.dept_id') //dept.name as 
	//->join('master_doc_kategori bg','bg.id = md.bagian_id')
	->join('users usr','usr.id_user = md.updated_by')
	//->join('master_doc_type type','type.type_id = md.type_id')
	->order_by($args["sort"],$args["order"])->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();    
	$result = array_merge($result, ['rows' => $row]);
	$result['total'] = $this->db->select('md.*,usr.*,usr.name as updated_by,md.doc_id as doc_id,(select abbr from master_kalk where id = md.kalk_id) as kalk_name,dept_name,(select name from master_doc_kategori where id=md.bagian_id) as bagian_name,(select type_name from master_doc_type where type_id=md.type_id) as type_name,md.iso_9001 as iso1')->from('master_doc md')
	//->join('master_kalk mk','mk.id = md.kalk_id')
	//->join('departments dept','dept.id_department = md.dept_id') //dept.name as 
	//->join('master_doc_kategori bg','bg.id = md.bagian_id')
	->join('users usr','usr.id_user = md.updated_by')
	//->join('master_doc_type type','type.type_id = md.type_id')
	->where($args["where"])->get()->num_rows();
	
	return $result;
}

public function get_doc_easy($args = [])
{
	$this->load->helper("myquery");
	if($args == null) {
		$args = [];
	}
	if(isset($args["where"]) == false) {
		$args["where"] = [];
	}
	else{
		$t = '';
		foreach($args["where"] as $v){
			$t = ($t=='')?$v:$v.' AND '.$t;
		}
		//print_r($t);
		
		$args["where"] = $t;
	}
	if(isset($args["limit"]) == false) {
		$args["limit"] = [];
	}
	if(isset($args["start"]) == false) {
		$args["start"] = [];
	}
	if(isset($args["order"]) == false) {
		$args["order"] = [];
	}
	
	$result = array();
	if(isset($args["find"])) //&& $this->session->userdata('id_user')!='29'
		$args["find"]=array("FIND_IN_SET(".$args["find"].", md.dept_id)"=>null);
	else
		$args["find"]=array("1=1"=>null);
	//(select case when (locked_by=0) then md.doc_id when (locked_by = '.$this->session->userdata('id_user').') then md.doc_id else 0 end)
	
	$row = $this->db->select('md.*,usr.*,usr.name as updated_by,md.doc_id as doc_id,(select abbr from master_kalk where id = md.kalk_id) as kalk_name,dept_name,(select name from master_doc_kategori where id=md.bagian_id) as bagian_name,(select type_name from master_doc_type where type_id=md.type_id) as type_name,md.iso_9001 as iso1,(select case when (FIND_IN_SET('.$this->session->userdata('id_user').',md.approved_by)) then concat("1_",md.doc_id) when ((select COUNT(id) from master_doc_approval where status=1 and doc_id=md.doc_id and from_acc = '.$this->session->userdata('id_user').' and date_acc=CURDATE())>0) then concat("1_",md.doc_id) else concat("0_",md.doc_id) end) as raw_id')->from('master_doc md')
	//->join('master_kalk mk','mk.id = md.kalk_id')
	//->join('departments dept','dept.id_department = md.dept_id') //dept.name as 
	//->join('master_doc_kategori bg','bg.id = md.bagian_id')
	->join('users usr','usr.id_user = md.updated_by')
	//->join('master_doc_type type','type.type_id = md.type_id')
	->order_by($args["sort"],$args["order"])->where($args["where"])->where($args["find"])->limit($args["limit"], $args["start"])->get()->result_array();    
	$result = array_merge($result, ['rows' => $row]);
	$result['total'] = $this->db->select('md.*,usr.*,usr.name as updated_by,md.doc_id as doc_id,(select abbr from master_kalk where id = md.kalk_id) as kalk_name,dept_name,(select name from master_doc_kategori where id=md.bagian_id) as bagian_name,(select type_name from master_doc_type where type_id=md.type_id) as type_name,md.iso_9001 as iso1')->from('master_doc md')
	//->join('master_kalk mk','mk.id = md.kalk_id')
	//->join('departments dept','dept.id_department = md.dept_id') //dept.name as 
	//->join('master_doc_kategori bg','bg.id = md.bagian_id')
	->join('users usr','usr.id_user = md.updated_by')
	//->join('master_doc_type type','type.type_id = md.type_id')
	->where($args["where"])->where($args["find"])->get()->num_rows();
	
	return $result;
}

public function get_doc_hist_easy($args = [])
{
	$this->load->helper("myquery");
	if($args == null) {
		$args = [];
	}
	if(isset($args["where"]) == false) {
		$args["where"] = [];
	}
	else{
		$t = '';
		foreach($args["where"] as $v){
			$t = ($t=='')?$v:$v.' AND '.$t;
		}
		//print_r($t);
		
		$args["where"] = $t;
	}
	if(isset($args["limit"]) == false) {
		$args["limit"] = [];
	}
	if(isset($args["start"]) == false) {
		$args["start"] = [];
	}
	if(isset($args["order"]) == false) {
		$args["order"] = [];
	}
	
	$result = array();
	if(isset($args["find"])) //&& $this->session->userdata('id_user')!='29'
		$args["find"]=array("FIND_IN_SET(".$args["find"].", a.dept_id)"=>null);
	else
		$args["find"]=array("1=1"=>null);
	//(select case when (locked_by=0) then md.doc_id when (locked_by = '.$this->session->userdata('id_user').') then md.doc_id else 0 end)
	
	$row = $this->db->select('md.*,
	(select usr_from.name from users usr_from where usr_from.id_user = md.from_acc) as from_acc,
	
	(select x.to_acc from master_doc_approval x where x.doc_id=md.doc_id and x.date_acc=md.date_acc and status<>0 order by status desc limit 1) as acc,
	
	(select GROUP_CONCAT(usr_to.name SEPARATOR ",") from users usr_to where FIND_IN_SET(usr_to.id_user,a.approved_by) ) as to_acc,
	
	a.*,DATE_FORMAT(md.date_acc, "%d-%m-%y") as date_acc')->from('master_doc_approval md')
	
	//->join('master_kalk mk','mk.id = md.kalk_id')
	//->join('departments dept','dept.id_department = md.dept_id') //dept.name as 
	->join('master_doc a','a.doc_id = md.doc_id')
	->group_by("md.doc_id,md.date_acc")
	//->join('users usr_from','usr_from.id_user = md.from_acc')
	//->join('users usr_to','usr_to.id_user = md.to_acc')
	
	//->join('master_doc_type type','type.type_id = md.type_id')
	->order_by($args["sort"],$args["order"])
	
	//->where($args["where"])->where($args["find"])
	->group_start()
			->where('md.from_acc', $this->session->userdata('id_user'))
			->or_where('md.to_acc', $this->session->userdata('id_user'))
	->group_end()
	
	->limit($args["limit"], $args["start"])->get()->result_array();    
	$result = array_merge($result, ['rows' => $row]);
	$result['total'] = $this->db->select('md.*')->from('master_doc_approval md')
	//->join('master_kalk mk','mk.id = md.kalk_id')
	//->join('departments dept','dept.id_department = md.dept_id') //dept.name as 
	//->join('master_doc_kategori bg','bg.id = md.bagian_id')
	->join('master_doc a','a.doc_id = md.doc_id')
	->group_by("md.doc_id,md.date_acc")
	//->join('users usr_from','usr_from.id_user = md.from_acc')
	//->join('users usr_to','usr_to.id_user = md.to_acc')
	
	//->join('master_doc_type type','type.type_id = md.type_id')
	->where($args["where"])->where($args["find"])->get()->num_rows();
	
	return $result;
}

// END MASTER DOC




public function get_notification($id){
	return $this->db->from("notifications")
		->where([
			"to_id" => $id,
			"data like '%doc_id%'" => null,
			"cuti_flag" => 0
		])
		->group_start()
			->or_where(["read" => null])
			->or_where(["read" => 0])
		->group_end()
		->order_by('id', 'desc')
		->get();
}

public function update_notification($data,$param){
	$this->db->where($param);
	$q = $this->db->update('notifications', $data);
	return $q;
}
}
