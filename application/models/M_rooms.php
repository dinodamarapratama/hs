<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_rooms extends CI_model
{

	/* master rooms & room schedule model (CUD)*/
    public function save_data($data=[],$table){
        $this->db->insert($table, $data);
        $saved_id = $this->db->insert_id();
        return $saved_id;
    }

    public function update_data($param=[],$data=[],$table){
        $this->db->where($param);
        $update = $this->db->update($table, $data);

        return $update;
    }

    public function hapus_data($param=[],$table){
        $del = $this->db->delete($table, $param);
        return $del;
    }


    /*get data (R)*/
    public function get_all_data($params=[],$source=[]){

    	/*
			structure example
			$source = [
				query_select => '',
				master_table => '',
				relations => [
					'table1' => [
						'conditions' => '',
						'join_type' => ''
					],
					'table2' => [
						'conditions' => '',
						'join_type' => ''
					],
				]
			]
    	*/
    	$this->db->select($source['query_select']);
    	$this->db->from($source['master_table']);

    	if (!empty($source['relations'])) {
    		foreach ($source['relations'] as $table => $opts) {
    			$this->db->join($table,$opts['conditions'],$opts['join_type']);		
    		}
    		
    	}
    	
    	$this->db->where($params);

        if (isset($source['order']) && !empty($source['order'])) {
            $this->db->order_by($source['order']);            
        }

    	$query = $this->db->get();
    	return $query;
    }

 
}