<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_appdokter extends CI_model
{

    public function get_where($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $result = $this->db->delete($table);
        return $result;
    }


    function get_related_data($config=[]){
        /* params
            - master
            - relation
            - where
            - select
        */
        (!empty($config['fields'])?$config['fields']:$config['fields']='*');
        $this->db->select($config['fields']);
        if (!empty($config['relation'])) {
            foreach ($config['relation'] as $key => $value) {
                $this->db->join($value['table'],$value['key'],'left');  
            }
            
        }
        $this->db->where($config['where']);
        if (!empty($config['or_where'])) {
            $this->db->or_where($config['or_where']);
        }
        if (!empty($config['or_where_group'])) {
            $this->db->group_start();
            $this->db->or_where($config['or_where_group']);
            $this->db->group_end();
        }
        
        if (!empty($config['order'])) {
            $this->db->order_by($config['order']['field'],$config['order']['dir']);
        }
        return $this->db->get($config['master']);
    }

    function get_list($params=[],$offset,$limit){
        $this->db->select('_.*, branch.branch_name, u.name as p_input');
        $this->db->from('appdokter_shift _');
        $this->db->where($params);
        $this->db->order_by("tgl_input", "desc");
        $this->db->join('branch', 'branch.branch_id = _.branch_id','left');
        $this->db->join('users u', 'u.id_user = _.creator','left');
        $this->db->limit($limit, $offset);
        return $this->db->get();
    }

    function count_list_all($params=[]){
        // $this->db->count('_.id');
        $this->db->from('appdokter_shift _');
        $this->db->where($params);
        $this->db->order_by("tgl_input", "desc");
        $this->db->join('branch', 'branch.branch_id = _.branch_id','left');
        $this->db->join('users u', 'u.id_user = _.creator','left');
        return $this->db->count_all_results();
    }
}
