<?php

class M_master_qc_group_level extends CI_Model {

	public $table = "master_qc_group_level";

	function baseQuery($args = null) {
		if($args == null) {
			$args = [];
		}
		if(isset($args["join_level"]) == false) {
			$args["join_level"] = true;
		}

		$q = $this->db->from("$this->table _")
			->join("users usr", "usr.id_user = _.creator_id", "left")
			->select([
				"_.*",
				"usr.name as creator_name"
			]);
		
		if($args["join_level"] == true) {
			$q->join("master_qc_group_level_member member", "member.id_group_level = _.id_group_level", "left");
			$q->join("master_qc_level level", "level.id_level = member.id_level", "left");
			$q->select("GROUP_CONCAT(CONCAT(level.level_code, ' - ', level.level_name) SEPARATOR '-----') as list_level");
			$q->group_by("member.id_group_level");
		}
		return $q;
	}

	function many($args = null) {
		if($args == null) {
			$args = [];
		}
		if(isset($args["where"]) == false) {
			$args["where"] = [];
		}

		$q = $this->baseQuery($args)
			->where($args["where"]);

		return $q->get()->result_array();
	}

	function one($args = null) {
		if($args == null) {
			$args = [];
		}
		$args["join_level"] = false;
		$r = $this->many($args);
		if(count($r) == 0) {
			return null;
		}
		$r = $r[0];

		$this->load->model("m_qc_group_level_member", "m_member");
		$listMember = $this->m_member->many([
			"where" => [
				"id_group_level" => $r["id_group_level"]
			]
		]);

		$r["list_member"] = $listMember;

		return $r;
	}

	function dt($args = null){
		if($args == null) {
			$args = [];
		}
		if(isset($args["where"]) == false) {
			$args["where"] = [];
		}

		$q = $this->baseQuery()
			->where($args["where"]);
		$sql = $q->get_compiled_select();

		$this->load->helper("dt");
		return getDataTable([
			"sql" => $sql
		]);
	}

	function add($data) {
		$idLevels = [];
		if(array_key_exists("id_level", $data)) {
			$idLevels = $data["id_level"];
			unset($data["id_level"]);

			if(is_array($idLevels) == false) {
				$idLevels = [];
			}
		}

		$this->db->insert($this->table, $data);
		$id = $this->db->insert_id();

		if($id != null) {
			$this->load->model("m_qc_group_level_member", "m_member");
			foreach ($idLevels as $key => $idLevel) {
				$this->m_member->add([
					"id_level" => $idLevel,
					"id_group_level" => $id
				]);
			}
		}

		$r = $this->one(["where" => ["id_group_level" => $id]]);

		if($id == null || $r == null) {
			return ["status" => false, "message" => "Terjadi kesalahan saat menyimpan group level", "id" => $id, "data" => $r];
		}

		return ["status" => true, "message" => "", "id" => $id, "data" => $r];
	}

	function update($where, $data) {
		$target = $this->one(["where" => $where]);

		if($target == null) {
			return ["status" => false, "message" => "Data tidak ditemukan"];
		}

		$idLevels = [];
		if(array_key_exists("id_level", $data)) {
			$idLevels = $data["id_level"];
			unset($data["id_level"]);

			if(is_array($idLevels) == false) {
				$idLevels = [];
			}
		}

		$this->db->where(["id_group_level" => $target["id_group_level"]])->update($this->table, $data);

		/* update list level*/
		$this->load->model("m_qc_group_level_member", "m_member");
		$oldLevels = $this->m_member->many([
			"where" => [
				"id_group_level" => $target["id_group_level"]
			]
		]);
		$updatedLevel = [];
		foreach ($idLevels as $key => $idLevel) {
			$exist = false;
			foreach ($oldLevels as $key => $level) {
				if($idLevel == $level["id_level"]) {
					$updatedLevel[] = $level["id"];
					$exist = true;
					break;
				}
			}
			if($exist == false) {
				$this->m_member->add([
					"id_group_level" => $target["id_group_level"],
					"id_level" => $idLevel
				]);
			}
		}
		foreach ($oldLevels as $key => $level) {
			if(in_array($level["id"], $updatedLevel) == false) {
				$this->m_member->delete(["id" => $level["id"]]);
			}
		}

		return ["status" => true, "message" => ""];

	}

	function delete($where) {
		$target = $this->many(["where" => $where]);

		if(count($target) == 0) {
			return ["status" => false, "message" => "Data tidak ditemukan"];
		}

		// $this->db->where(["id_group_level" => $target["id_group_level"]])->delete($this->table);
		$this->load->model("m_qc_group_level_member", "m_member");
		foreach ($target as $key => $t) {
			$this->db->where(["id_group_level" => $t["id_group_level"]])->delete($this->table);
			$this->m_member->delete(["id_group_level" => $t["id_group_level"]]);
		}

		return ["status" => true, "message" => ""];
	}
}