<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_suhu extends CI_model
{

    public function last_query_db(){
        return $this->db->last_query();
    }

    public function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
        // echo $this->db->from($table)->where($where)->get_compiled_select();
        // exit();
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $result = $this->db->delete($table);
        return $result;
    }
    
    public function delete($param=[],$table)
    {
        $this->db->where($param);
        $q = $this->db->delete($table);

        return $q;
    }
    
    public function insert_batch($table, $data)
    {
        $this->db->insert_batch($table, $data);
    }

    public function get_data_like($where, $colum, $table)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->like($colum, $where);
        return $this->db->get();
    }

   function many_kat($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        else{
            $t = '';
            foreach($args["where"] as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            //print_r($t);
            
            $args["where"] = $t;
        }
        if(isset($args["limit"]) == false) {
            $args["limit"] = [];
        }
        if(isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if(isset($args["order"]) == false) {
            $args["order"] = [];
        }
        
        $result = array();
        $row = $this->baseQueryKat($args)->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();    
        $result = array_merge($result, ['rows' => $row]);
        $result['total'] = $this->baseQueryKat($args)->where($args["where"])->get()->num_rows();
        
        return $result;
        
    }

    function baseQueryKat($args = []) {
        return $this->db->from("master_mcu_kategori _")
            ->order_by($args["sort"],$args["order"])
            ->group_by("_.nama_kat")
            ->select([
                "_.*"

            ]);
    }



    function many_sub($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        
        if(isset($args["limit"]) == false) {
            $args["limit"] = [];
        }
        if(isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if(isset($args["order"]) == false) {
            $args["order"] = [];
        }

        return $this->baseQuerySub($args)->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();   
    }

    function baseQuerySub($args = []) {
        return $this->db->from("master_mcu_sub _")

           ->order_by($args["sort"],$args["order"])
           //->group_by("_.sub_kat")
            ->select([
                "_.*"

            ]);
    }

    function many_tes($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        
        if(isset($args["limit"]) == false) {
            $args["limit"] = [];
        }
        if(isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if(isset($args["order"]) == false) {
            $args["order"] = [];
        }

        return $this->baseQueryTes($args)->where($args["where"])->get()->result_array();
    }

    function baseQueryTes($args = []) {
        return $this->db->from("master_mcu_tes _")
              ->join('master_mcu_kategori kat', 'kat.id_kat = _.id_kat','left')
              ->join('master_mcu_sub sub', 'sub.id_sub = _.id_sub','left')
              ->order_by($args["sort"],$args["order"])
         //   ->group_by("_.sub_kat")
            ->select([
                "_.*",
                "kat.*",
                "sub.*"

            ]);
    }

    function add_input_kat($data) {
        
        $this->db->insert("master_mcu_kategori", $data);
        $id = $this->db->insert_id();
        

        return ["status" => true, "message" => "Success"];
    }

     function update_input_kat($where, $data) {
        $this->db->where($where)->update("master_mcu_kategori", $data);
        return ["status" => true, "message" => "Success"];
    }

    function delete_input_kat($where){
        $del = $this->db->delete('master_mcu_kategori', $where);
        return ["status" => true, "message" => "Success"];
    }

    
    function add_input_sub($data) {
        
        $this->db->insert("master_mcu_sub", $data);
        $id = $this->db->insert_id();
        

        return ["status" => true, "message" => "Success"];
    }

     function update_input_sub($where, $data) {
        $this->db->where($where)->update("master_mcu_sub", $data);
        return ["status" => true, "message" => "Success"];
    }

    function delete_input_sub($where){
        $del = $this->db->delete('master_mcu_sub', $where);
        return ["status" => true, "message" => "Success"];
    }


    function add_input_tes($data) {
        
        $this->db->insert("master_mcu_tes", $data);
        $id = $this->db->insert_id();
        

        return ["status" => true, "message" => "Success"];
    }

     function update_input_tes($where, $data) {
        $this->db->where($where)->update("master_mcu_tes", $data);
        return ["status" => true, "message" => "Success"];
    }

    function delete_input_tes($where){
        $del = $this->db->delete('master_mcu_tes', $where);
        return ["status" => true, "message" => "Success"];
    }


// frontend start 

    function many_v1($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        else{
            $t = '';
            foreach($args["where"] as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            //print_r($t);
            
            $args["where"] = $t;
        }
        if(isset($args["limit"]) == false) {
            $args["limit"] = [];
        }
        if(isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if(isset($args["order"]) == false) {
            $args["order"] = [];
        }
        
        $result = array();
        $row = $this->baseQueryV1($args)->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();    
        $result = array_merge($result, ['rows' => $row]);
        $result['total'] = $this->baseQueryV1($args)->where($args["where"])->get()->num_rows();
        
        return $result;
        
    }


    function baseQueryV1($args = []) {
        return $this->db->from("suhu _")
            //->join('mcu_pemeriksaan mp', 'mp.id_mcu = _.id_mcu','left')
            ->order_by($args["sort"],$args["order"])
            //->group_by("_.id_mcu")
            ->select([
                //"mp.*",
				"_.*",
				/*"DATE_FORMAT(_.mcu_tgl_mulai,'%d-%b-%y') as tgl_dari",
				"DATE_FORMAT(_.mcu_tgl_selesai,'%d-%b-%y') as tgl_ke",
                "GROUP_CONCAT(id_kat) as id_kat",
                "GROUP_CONCAT(mp.id_sub) as id_sub2",
                "DATE_FORMAT(mcu_tgl_mulai, '%d-%m-%Y') as dari",
                "DATE_FORMAT(mcu_tgl_selesai, '%d-%m-%Y') as ke",*/
				"_.*",
				"DATE_FORMAT(recog_time, '%d-%m-%Y %h:%i:%s') as recog_time"
            ]);
    }



}
