<?php

class M_announcement extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->helper('myquery');
	}

	function detail($id, $id_user = null){
		$user_id = $id_user;

		if ($user_id) {
			$this->db->where([
				'id_receiver' => $user_id,
				'id_fk' => $id,
				'table_fk' => 'announcement'
			])->update('receivers', ['view_at' => 'NOW()', 'view_comment_at' => 'NOW()']);
		}
		$generalreports = $this->db->where('id', $id)->get('announcements')->row();

		if (!empty($generalreports)) {

			/* tandai notifikasi sudah dibaca */
			$this->load->library("Notify_lib");
			$nl = new Notify_lib();
			$nl->mark_read($user_id, "announcements", $id);
			
			if (!empty($generalreports->description)) {
				$myCaption1 = strip_tags($generalreports->description);
				$myCaptionEncoded1 = htmlspecialchars_decode($myCaption1);
				$generalreports->description = html_entity_decode($generalreports->description);
			}
		} else {
			return ['r' => $generalreports, 'id' => $id];
		}
		$this->load->model('restapi/user_model');
		$generalreports->author = $this->user_model->one_user(['where' => ['_.id_user' => $generalreports->creator_id]]);

		$this->db->select('users.*, users.id_user as id, (CASE view_at WHEN null then 0 else 1 end) as is_view');
		$this->db->select('users.image');
		$this->db->select('position.name_position as position');
		$this->db->select('departments.name as department');
		$this->db->join('users', 'receivers.id_receiver=users.id_user');
		$this->db->join('position', 'position.id_position=users.id_position');
		$this->db->join('departments', 'position.id_department=position.id_department', 'left');
		$this->db->where('receivers.id_fk', $id);
		$this->db->group_by('receivers.id_receiver');
		$generalreports->receivers = $this->db->get('receivers')->result();
		
		$generalreports->attachments = $this->db->where('id_announcement', $id)->get('announcements_attachment')->result();
		return $generalreports;
	}
	
	function baseQueryAnnouncement($args = []) {
		// $q = $this->db->from('announcements _')
		// 	->join('v_user usr', 'usr.id_user = _.creator_id', 'left')
		// 	->join('position p', 'p.id_position = usr.id_position', 'left')
		// 	->join('departments d', 'd.id_department = p.id_department', 'left')
		// 	->join('v_receivers_read_count rc', "rc.table_fk = 'announcement' and rc.id_fk = _.id", 'left')
		// 	// ->limit('100')
		// 	->select([
		// 		'_.*',
		// 		'usr.name as creator_name',
		// 		'usr.call_name as creator_call_name',
		// 		'usr.username as creator_username',
		// 		'usr.image as creator_image',
		// 		'usr.branch_name',
		// 		'p.name_position as creator_pos',
		// 		'd.name as creator_dept',
		// 		// "CONCAT(DATE_FORMAT(_.start,'%Y-%M-%D'), ' - ', DATE_FORMAT(_.end, '%Y-%M-%D')) as start_end",
		// 		"CONCAT(DATE_FORMAT(_.start,'%d %b %Y'), ' - ', DATE_FORMAT(_.end, '%d %b %Y')) as start_end",
		// 		"CONCAT(coalesce(rc.dibaca,0), '/', coalesce(rc.jumlah_rcv,0)) as read_count"
		// 	]);

		$q = $this->db->from('announcements _')
			->join('v_user usr', 'usr.id_user = _.creator_id', 'left')
			->join('position p', 'p.id_position = usr.id_position', 'left')
			->join('departments d', 'd.id_department = p.id_department', 'left')
			->join('v_receivers_read_count rc', "rc.table_fk = 'announcement' and rc.id_fk = _.id", 'left')
			// ->limit('100')
			->select([
				'_.*',
				'usr.name as creator_name',
				'usr.name as creator_call_name',
				'usr.username as creator_username',
				'usr.image as creator_image',
				'usr.branch_name',
				'p.name_position as creator_pos',
				'd.name as creator_dept',
				"CONCAT(DATE_FORMAT(_.start,'%d %b %Y'), ' - ', DATE_FORMAT(_.end, '%d %b %Y')) as start_end",
				"CONCAT(coalesce(rc.dibaca,0), '/', coalesce(rc.jumlah_rcv,0)) as read_count"
			]);
		return $q;
	}
	
	function baseQueryAnnouncementReceived($args = []) {
		// $q = $this->db->from('notifications rc')
		// 	->join('v_user usr', 'usr.id_user = rc.to_id', 'left')
		// 	//->join('position p', 'p.id_position = usr.id_position', 'left')
		// 	//->join('departments d', 'd.id_department = p.id_department', 'left')
		// 	//->join('v_receivers_read_count rc', "rc.table_fk = 'announcement' and rc.id_fk = _.id", 'left')
		// 	//->join('announcements _', 'rc.data like CONCAT(\'%"id_announcement":"\',_.id,\'"%\')', 'left')
		// 	//->join('announcements _', 'rc.from_id=_.creator_id', 'left')
		// 	->select([
		// 		//'_.*',
		// 		'rc.time',
		// 		'rc.data',
		// 		'usr.name as creator_name',
		// 		'usr.call_name as creator_call_name',
		// 		'usr.username as creator_username',
		// 		'usr.image as creator_image',
		// 		'usr.branch_name',
		// 		'(select p.name_position from position p where p.id_position = usr.id_position) as creator_pos',
		// 		'(select d.name from departments d where d.id_department = usr.id_department) as creator_dept',
				
		// 		//"CONCAT(DATE_FORMAT(_.start,'%d %b %Y'), ' - ', DATE_FORMAT(_.end, '%d %b %Y')) as start_end",
				
		// 		//"CONCAT(coalesce(rc.dibaca,0), '/', coalesce(rc.jumlah_rcv,0)) as read_count"
		// 		//"'' as read_count"
		// 		//"CONCAT(coalesce((select count(f.id) from notifications f where f.read=1 and (f.time between DATE_SUB(rc.time, INTERVAL 2 MINUTE) and DATE_ADD(rc.time, INTERVAL 2 MINUTE)) and f.from_id=rc.from_id ),0), '/', coalesce((select count(f.id) from notifications f where (f.time between DATE_SUB(rc.time, INTERVAL 2 MINUTE) and DATE_ADD(rc.time, INTERVAL 2 MINUTE)) and f.from_id=rc.from_id ),0)) as read_count"
		// 	]);

		$q = $this->db->from('notifications rc')
			->join('v_user usr', 'usr.id_user = rc.to_id', 'left')
			->select([
				'rc.time',
				'rc.data',
				'usr.name as creator_name',
				'usr.name as creator_call_name',
				'usr.username as creator_username',
				'usr.image as creator_image',
				'usr.branch_name',
				'(select p.name_position from position p where p.id_position = usr.id_position) as creator_pos',
				'(select d.name from departments d where d.id_department = usr.id_department) as creator_dept',
			]);
		return $q;
	}

	function many_announcement($args = []) {
		$q = myquery($this->baseQueryAnnouncement($args), $args);
		

		if(empty($args['id_user']) == false) {

		}

		return $q->get()->result_array();
	}

	function whereYearMonth($query, $args) {
		if(empty($args) == false &&  empty($args['year']) == false && empty($args['month']) == false) {
			if(empty($args['day'])){

				$query->group_start();
					$query->group_start();
						$query->group_start();
							$query->where('year(_.start)', $args['year'])
								->where('month(_.start)', str_pad($args['month'], 2, '0', STR_PAD_LEFT));
						$query->group_end();

						$query->or_group_start();
							$query->where('year(_.end)', $args['year'])
								->where('month(_.end)', str_pad($args['month'], 2, '0', STR_PAD_LEFT));
						$query->group_end();
						
						$query->or_group_start();
							$query->like('_.tgl_multi' ,(str_pad($args['month'], 2, '0', STR_PAD_LEFT)).'-'.substr($args['year'],-2), 'both');
						$query->group_end();
					$query->group_end();
					
					
					
				$query->group_end();		
				
			}

			else {
				$date = $args['year'] . '-' . str_pad($args['month'], 2, '0', STR_PAD_LEFT) . '-' . str_pad($args['day'], 2, '0', STR_PAD_LEFT);
				// $end = date('Y-m-t', strtotime($start));
				$query->group_start();
				$query 	->where("_.start <=", $date)
						->where('_.end >=', $date);
				$query->or_group_start();
						$query->like('_.tgl_multi' ,str_pad($args['day'], 2, '0', STR_PAD_LEFT).'-'.str_pad($args['month'], 2, '0', STR_PAD_LEFT).'-'.substr($args['year'],-2), 'both');
				$query->group_end();			
				$query->group_end();	
				
					
						
			}

		}

		return $query;
	}
	
	function whereYearMonthReceived($query, $args) {
		if(empty($args) == false &&  empty($args['year']) == false && empty($args['month']) == false) {
			if(empty($args['day'])){

				$query->group_start();
					$query->group_start();
						$query->where('year(rc.time)', $args['year'])
							->where('month(rc.time)', $args['month']);
					$query->group_end();
					
					/*
					$query->or_group_start();
							$query->like('_.tgl_multi' ,(str_pad($args['month'], 2, '0', STR_PAD_LEFT)).'-'.substr($args['year'],-2), 'both');
					$query->group_end();
					*/
				$query->group_end();
				
				

			}

			else {
				$date = $args['year'] . '-' . str_pad($args['month'], 2, '0', STR_PAD_LEFT) . '-' . str_pad($args['day'], 2, '0', STR_PAD_LEFT);
				// $end = date('Y-m-t', strtotime($start));
				$query 	->where("DATE(rc.time)", $date);
			}
			//$query 	->where('rc.data like CONCAT(\'%"id_announcement":"\',_.id,\'"%\')',null);
			$query 	->where('rc.to_id',$args['id_user']);
		}

		return $query;
	}

	function baseQuerySent($args = []) {
		$q = myquery($this->baseQueryAnnouncement($args), $args);
		$q = $this->whereYearMonth($q, $args);
		if(empty($args['id_user']) == false) {
			$q->where('_.creator_id', $args['id_user']);
		}
		return $q;
	}

	function getSent($args) {
		$q = $this->baseQuerySent($args);
		$result = $q->get()->result_array();
		$arr = array();
		if(is_array($result)){
			foreach($result as $val){				
				if($val['tgl_multi']!=""){					
					$days = explode(',',$val['tgl_multi']);
					foreach($days as $v){
						$v = str_replace(' ','',$v);
						$d = substr($v,0,2);
						$m = str_pad(($args['month']), 2, '0', STR_PAD_LEFT);
						$y = substr($args['year'],-2);
						//print_r(str_pad($args['month'], 2, '0', STR_PAD_LEFT).'-'.substr($args['year'],-2) .'-----'. substr($v,-5).'<br />');
						if($m.'-'.$y == substr($v,-5)){
							$val['start'] = $args['year'].'-'.$m.'-'.$d;
							$val['end'] = $args['year'].'-'.$m.'-'.$d;
							$arr[] = $val;
						}
					}
				}
				else $arr[] = $val;
			}
		}
		return $arr;
	}

	function baseQueryReceived_old($args = []) {
		$q = myquery($this->baseQueryAnnouncement($args), $args);
		$q = $this->whereYearMonth($q, $args);
		
		$this->load->model('m_receiver');
		$tReceiver = $this->m_receiver->tableName();

		if(empty($args['id_user']) == false) {
			$q->join("$tReceiver rcv", "rcv.id_fk = _.id and rcv.table_fk = 'announcement'", 'left')
				->where('rcv.id_receiver', $args['id_user'])
				->where('rcv.closed_at', null);
		}
		
		return $q;
	}
	
	function baseQueryReceived($args = []) {
		$q = myquery($this->baseQueryAnnouncementReceived($args), $args);
		$q = $this->whereYearMonthReceived($q, $args);
		
		$this->load->model('m_receiver');
		$tReceiver = $this->m_receiver->tableName();

		if(empty($args['id_user']) == false) {
			$q->where('rc.to_id', $args['id_user']);
				//->where('rc.closed_at', null);
		}
		
		return $q;
	}

	function getReceived($args) {
		$q = $this->baseQueryReceived($args);
		// echo $q->get_compiled_select();
		// exit();
		$result = $q->get()->result_array();
		$arr = array();
		if(is_array($result)){
			foreach($result as $val){				
				$o = json_decode($val['data']);
				if (isset($o->id_announcement)) {
					
					$val['id'] = $o->id_announcement;
					$val['creator_id'] = $o->creator_id;
					$val['start'] = $o->start;
					$val['end'] =  $o->end;//date("Y-m-d", strtotime("+1 month", strtotime($o->end)));
					$val['tgl_multi'] = (isset($o->tgl_multi)?$o->tgl_multi:"");
					$val['branch_id'] = $o->branch_id;
					$val['note'] = $o->note;
					$val['event_type'] = $o->event_type;
					$val['title'] = $o->title;
					$val['gcalendar_id'] = $o->gcalendar_id;
					$val['branch_id'] = $o->branch_id;
					$val['repeat_type'] = $o->repeat_type;
					$val['time_repeat'] = $o->time_repeat;
					$val['deleted_at'] = $o->deleted_at;
					$val['created_at'] = $o->created_at;
					$val['last_update'] = $o->last_update;
					$val['start_end'] = date("d M Y", strtotime($o->start)). ' - '. date("d M Y", strtotime($o->end));//date("d M Y", strtotime("+1 month", strtotime($o->end)));//'%d %b %Y'
					
					if($val['deleted_at']==""){
						//unset($val['data']);
						if($val['tgl_multi']!=""){					
							if(strpos((str_pad($args['month'], 2, '0', STR_PAD_LEFT)).'-'.substr($args['year'],-2), $val['tgl_multi']) !== false)
							{	
								$days = explode(',',$val['tgl_multi']);
								foreach($days as $v){
									$v = str_replace(' ','',$v);
									$d = substr($v,0,2);
									$m = str_pad(($args['month']), 2, '0', STR_PAD_LEFT);
									$y = substr($args['year'],-2);
									//print_r(str_pad($args['month'], 2, '0', STR_PAD_LEFT).'-'.substr($args['year'],-2) .'-----'. substr($v,-5).'<br />');
									if($m.'-'.$y == substr($v,-5)){
										$val['start'] = $args['year'].'-'.$m.'-'.$d;
										$val['end'] = $args['year'].'-'.$m.'-'.$d;
										$arr[] = $val;
									}
								}
							}
						}
						else $arr[] = $val;
					}
				}
			}
		}
		return $arr;
	}

	function one_announcement($args) {
		// $r = myquery($this->baseQueryAnnouncement($args), $args)->get()->row();
		if($args != null) {
			$args['limit'] = [ 1 ];
		}
		$r = $this->many_announcement($args);
		if(count($r) > 0){
			return $r[0];
		}
		return null;
	}

	function add_announcement($data) {
		$receivers = [];
		if($data['creator_id']!=0) {
			if(empty($data['creator_id'])) {
				return ['status' => false, 'message' => 'Creator ID is not valid'];
			}
		}
		$this->load->model('restapi/user_model');
		if(array_key_exists('receivers', $data)) {
			$receivers = $data['receivers'];
			unset($data['receivers']);

			if($receivers == null) {
				$receivers = [];
			}
			else if(is_array($receivers) == false) {
				$receivers = [ $receivers ];
			}
		}

		$bagian = [];
		if(array_key_exists('bagian', $data)) {
			$bagian = $data['bagian'];

			if(is_array($bagian) == false) {
				$bagian = [$bagian];
			}

			unset($data['bagian']);

			if(count($bagian) > 0){
				$user_bagian = $this->user_model->many_user(['where_in' => ['bg.id' => $bagian]]);

				foreach ($user_bagian as $key => $usr) {
					$receivers[] = $usr['id_user'];
				}
			}
		}

		$department = [];
		if(array_key_exists('department', $data)) {
			$department = $data['department'];

			if(is_array($department) == false) {
				$department = [$department];
			}

			unset($data['department']);

			if(count($department) > 0){
				$user_department = $this->user_model->many_user(['where_in' => ['dep.id_department' => $department]]);

				foreach ($user_department as $key => $usr) {
					$receivers[] = $usr['id_user'];
				}
			}
		}

		$temp = [];
		foreach ($receivers as $key => $id_user) {
			$id_user = trim($id_user);
			if(in_array($id_user, $temp)) {
				continue;
			}
			if(empty($id_user)) {
				continue;
			}
			if($data['creator_id'] == $id_user){
				continue;
			}
			$temp[] = $id_user;
		}

		$receivers = $temp;
		
		$is_repeat = $data['repeat_type']; //ADD REPEAT
		$time_repeat = $data['time_repeat']; //ADD REPEAT
		
		if(count($receivers) == 0 && $time_repeat=="") { //ADD REPEAT
			return ['status' => false, 'message' => 'Tidak ada receiver dipilih', 'receiver' => $receivers, 'bagian' => $bagian];
		}
		
		$this->load->model('m_receiver');
		$id="";
		if($data['id_announcement']=="")
		{
			unset($data['id_announcement']);
			$this->db->insert('announcements', $data);
			$id = $this->db->insert_id();
		}
		else{
			$id = $data['id_announcement'];
			$this->db->where('id', $id);
			unset($data['id_announcement']);
			$this->db->update('announcements', $data);
			if($time_repeat=="") $this->db->where(['id_fk' => $id, 'table_fk' => 'announcement'])->delete('receivers');
		}

		if(empty($id)) {
			return ['status' => false, 'message' => 'ID hasil simpan tidak valid'];
		}

		
		$this->m_receiver->add_receiver($id, 'announcement', $receivers);

		$data = $this->one_announcement(['where' => ['_.id' => $id]]);

		if(empty($data)) {
			return [
				'status' => false,
				'message' => 'Terjadi kesalahan saat menyimpan data'
			];
		}
		
		if($time_repeat=="") { //ADD REPEAT
		
			$this->load->model('restapi/user_model');
			$sender = $this->user_model->one_user(['where' => ['_.id_user' => $data['creator_id']]]);
			$sender_name = '';
			$position_name = '';
			$branch_name = '';
			if($sender != null) {
				$sender_name = $sender['name'];
				$position_name = $sender['position_name'];
				$branch_name = $sender['branch_name'];
			}
			
			$this->load->library('Notify_lib');
			$nl = new Notify_lib();
			$nl->send(
				'New Announcement',
				//$data['creator_name'] . ' has created new announcement',
				$sender_name . ' - '.$position_name.' ('.$branch_name.') has created new announcement',
				$data['id'],
				$receivers,
				$data,
				'announcement',
				true
			);
		}
		return [
			'status' => true, 
			'message' => 'Data berhasil disimpan',
			'id' => $id,
			'data' => $data
		];
	}

	function add_attachment($data) {
		$this->load->model('m_attachment');
		$data['table_fk'] = 'announcement';
		$r = $this->m_attachment->add_attachment($data);

		return $r;
	}

	function dt_inbox($args = []) {
		$q = $this->baseQueryReceived($args);
		$sql = $q->get_compiled_select();
		//print_r($sql);
		$this->load->helper('dt');
		
		$dt = getDataTable([
			'sql' => $sql,
			'order_subs' => [
				'start_end' => 'time'
			],
			'default_order' => [
				'time' => 'desc'
			]
		]);
		
		$result = $dt['data'];
		$arr = array();
		if(is_array($result)){
			foreach($result as $val){				
				$o = json_decode($val['data']);
				if (isset($o->id_announcement)) {
					
					$v_read = $this->db->where('rc.table_fk', 'announcement')->where('rc.id_fk', $o->id_announcement)->get('v_receivers_read_count rc')->row();
					$val['read_count'] = $v_read->dibaca. '/'. $v_read->jumlah_rcv;
					$val['id'] = $o->id_announcement;
					$val['creator_id'] = $o->creator_id;
					$val['start'] = $o->start;
					$val['end'] = $o->end; //strtotime("+1 month", strtotime($o->end))
					$val['tgl_multi'] = (isset($o->tgl_multi)?$o->tgl_multi:"");
					$val['branch_id'] = $o->branch_id;
					$val['note'] = $o->note;
					$val['event_type'] = $o->event_type;
					$val['title'] = $o->title;
					$val['gcalendar_id'] = $o->gcalendar_id;
					$val['branch_id'] = $o->branch_id;
					$val['repeat_type'] = $o->repeat_type;
					$val['time_repeat'] = $o->time_repeat;
					$val['deleted_at'] = $o->deleted_at;
					$val['created_at'] = $o->created_at;
					$val['last_update'] = $o->last_update;
					$val['start_end'] = date("d M Y", strtotime($o->start)). ' - '. date("d M Y", strtotime($o->end));//date("d M Y", strtotime("+1 month", strtotime($o->end)));//'%d %b %Y'
					
					if($val['deleted_at']==""){
						//unset($val['data']);
						if($val['tgl_multi']!=""){					
							if(strpos((str_pad($args['month'], 2, '0', STR_PAD_LEFT)).'-'.substr($args['year'],-2), $val['tgl_multi']) !== false)
							{	
								$days = explode(',',$val['tgl_multi']);
								foreach($days as $v){
									$v = str_replace(' ','',$v);
									$d = substr($v,0,2);
									$m = str_pad(($args['month']), 2, '0', STR_PAD_LEFT);
									$y = substr($args['year'],-2);
									//print_r(str_pad($args['month'], 2, '0', STR_PAD_LEFT).'-'.substr($args['year'],-2) .'-----'. substr($v,-5).'<br />');
									if($m.'-'.$y == substr($v,-5)){
										$val['start'] = $args['year'].'-'.$m.'-'.$d;
										$val['end'] = $args['year'].'-'.$m.'-'.$d;
										$arr[] = $val;
									}
								}
							}
						}
						else $arr[] = $val;
					}
				}
			}
		}
		$dt['data'] = $arr;
		return $dt;
		
		//return $dt;
		//'start' => 'desc',
		//'end' => 'desc'
				
	}

	function dt_sent($args = []) {
		if(empty($args['where'])) {
			$args['where'] = [];
		}
		$args['where']['deleted_at'] = null;
		$q = $this->baseQuerySent($args);
		$sql = $q->get_compiled_select();
		$this->load->helper('dt');
		return getDataTable([
			'sql' => $sql,
			'order_subs' => [
				'start_end' => 'start'
			],
			'default_order' => [
				'start' => 'desc',
				'end' => 'desc'
			]
		]);
	}

	function simpleUpdate($where, $data) {
		$this->db->like('data', '"notif_type":"announcement"')
		->like('data', '{"id":"'.$where['id'].'"')
		->delete('notifications');
		
		$this->db->where($where)->update('announcements', $data);
		
		return ['status' => true, 'message' => "Update success"];
	}

	function removeReceiver($args) {
		$this->load->model('m_receiver');
		$now = date('Y-m-d');
		
		$this->db->where(['to_id' => $args['id_user']])
		->like('data', '"notif_type":"announcement"')
		->like('data', '{"id":"'.$args['id'].'"')->delete('notifications');
        
		
		$this->m_receiver->simpleUpdate(['id_receiver' => $args['id_user'], 'id_fk' => $args['id'], 'table_fk' => 'announcement'], ['closed_at' => $now]);
		return ['status' => true, 'message' => 'Update success'];
	}

	function getReceivers_old($id){
		$this->load->model('m_receiver');
		$r = $this->m_receiver->many_receiver([
			'where' => [
				'_.id_fk'=> $id,
				'_.table_fk' => 'announcement'
			]
		]);
		return $r;
	}
	
	function getReceivers($id,$tgl=null){
		$this->load->model('m_receiver');
		$arr = array();
		if($tgl == null){
			$arr = [
				'where' => [
					'_.id_fk'=> $id,
					'_.table_fk' => 'announcement'
				]
			];
		}
		else
		{
			$arr = [
				'where' => [
					'_.id_fk'=> $id,
					'_.table_fk' => 'announcement',
					'DATE(_.updated_at)' => $tgl
				]
			];
		}
		$r = $this->m_receiver->many_receiver($arr);
		return $r;
	}

	function getAttachments($id) {
		$this->load->model('m_attachment');
		$r = $this->m_attachment->many_attachment([
			'where' => [
				'_.table_fk' => 'announcement',
				'_.id_fk' => $id
			]
		]);
		return $r;
	}

	function rcv_mark_read($id, $user_id) {
		$this->load->model('m_receiver');
		$now = date('Y-m-d H:i:s');
		$this->m_receiver->simpleUpdate(['table_fk' => 'announcement', 'id_fk' => $id, 'id_receiver' => $user_id], ['view_at' => $now, 'view_comment_at' => $now]);
	}
		
    public function get_notification($id){
        return $this->db->from("notifications")
            ->where([
                "to_id" => $id,
				"data like '%id_announcement%'" => null,
                "cuti_flag" => 0
            ])
            /*->group_start()
                ->or_where(["read" => null])
                ->or_where(["read" => 0])
            ->group_end()*/
			
            ->select('notifications.*,(select image from users where id_user = notifications.from_id) as photo_profile,(select name from users where id_user = notifications.from_id) as creator_name')
			->limit(20)
			
			// ->where(["anime" => "naruto"])
			->order_by('id', 'desc')
            ->get();
        
        // $this->db->select('*');
        // $this->db->from('notifications');
        // $this->db->where('to_id', $id);
        // $this->db->where('read', 0);
        // $this->db->where('read <>', 1);
        // $this->db->order_by('id', 'desc');
        
        // return $this->db->get();
    }
	
	public function no_read_notification($id){
        return $this->db->from("notifications")
            ->where([
                "to_id" => $id,
				"data like '%id_announcement%'" => null,
                "cuti_flag" => 0
            ])
            ->select('id')
			
			->where(["read" => "0"])
			->order_by('id', 'desc')
            ->get();
        
        // $this->db->select('*');
        // $this->db->from('notifications');
        // $this->db->where('to_id', $id);
        // $this->db->where('read', 0);
        // $this->db->where('read <>', 1);
        // $this->db->order_by('id', 'desc');
        
        // return $this->db->get();
    }

}