<?php

class M_cuti extends CI_Model {
	
	
    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

	//web
	public function get_data_cuti($params=[], $rows=0, $offset=0,$all=0){
	   	$this->db->select(['users.id_user','users.name','users.last_name','users.id_position','cuti.*','branch.branch_name','position.name_position','(select is_hrd from cuti_receiver where id_cuti=cuti.id_cuti and id_receiver = '.$this->user_id.' limit 1) as is_hrd','users.id_position','users.id_bagian']);
	   	$this->db->from('cuti');

		if (isset($params['in_param'])) {
	     	foreach ($params['in_param'] as $key => $value) {
				if (strpos($value, '(') !== false)
					$this->db->where_in($key,$value,false);
				else
					$this->db->where_in($key,$value);
	     	}
	     	unset($params['in_param']);
	   	}

	   if (isset($params['not_in_param'])) {
	      	foreach ($params['not_in_param'] as $k => $v) {
	          $this->db->where_not_in($k,$v);
	      	}
	      	unset($params['not_in_param']);
	    }

      	if (isset($params['or_param'])) {
	      foreach ($params['or_param'] as $k => $v) {
	          $this->db->or_where([$k => $v]);
	      }
	      unset($params['or_param']);
	    }
	   	$this->db->where($params);
		//$this->db->or_where('year(tgl_request)','2021');
		$this->db->where('izin','0');
	   	if ($all==0) {
	     	$this->db->limit($rows, $offset);
	   	}
	   $this->db->order_by('cuti.date_created', 'DESC');
	   $this->db->join('branch', 'cuti.branch_id = branch.branch_id','left');
	   $this->db->join('users', 'users.id_user = cuti.id_user','left');
	   $this->db->join('position', 'users.id_position = position.id_position','left');
	  
	   $query = $this->db->get();
	   return $query->result_array();
	}

//web
	public function get_data_last_cuti($params=[], $rows=0, $offset=0,$all=0){
		$this->db->select(['users.id_user','users.name','users.last_name','users.id_position','cuti.*','branch.branch_name','position.name_position','(select is_hrd from cuti_receiver where id_cuti=cuti.id_cuti and id_receiver = '.'"'.$this->session->userdata('id_user').'"'.' limit 1) as is_hrd','users.id_position','users.id_bagian']);
		$this->db->from('cuti');

	 if (isset($params['in_param'])) {
		  foreach ($params['in_param'] as $key => $value) {
			 if (strpos($value, '(') !== false)
				 $this->db->where_in($key,$value,false);
			 else
				 $this->db->where_in($key,$value);
		  }
		  unset($params['in_param']);
		}

	if (isset($params['not_in_param'])) {
		   foreach ($params['not_in_param'] as $k => $v) {
		   $this->db->where_not_in($k,$v);
		   }
		   unset($params['not_in_param']);
	 }

	   if (isset($params['or_param'])) {
	   foreach ($params['or_param'] as $k => $v) {
		   $this->db->or_where([$k => $v]);
	   }
	   unset($params['or_param']);
	 }
		$this->db->where($params);
	    $this->db->where('izin_reject','0');
		if ($all==0) {
		  $this->db->limit($rows, $offset);
		}
	$this->db->order_by('cuti.date_created', 'DESC');
	$this->db->join('branch', 'cuti.branch_id = branch.branch_id','left');
	$this->db->join('users', 'users.id_user = cuti.id_user','left');
	$this->db->join('position', 'users.id_position = position.id_position','left');
   
	$query = $this->db->get();
	return $query->result_array();
 }

	//mobile
	public function get_data_cuti_new($params=[], $rows=0, $offset=0,$all=0,$current_user){
		$this->db->select(['users.id_user','users.name','users.last_name','users.id_position','cuti.*','branch.branch_name','position.name_position','(select is_hrd from cuti_receiver where id_cuti=cuti.id_cuti and id_receiver = '.$current_user.' limit 1) as is_hrd','users.id_position','users.id_bagian']);
		$this->db->from('cuti');

	 if (isset($params['in_param'])) {
		  foreach ($params['in_param'] as $key => $value) {
			 if (strpos($value, '(') !== false)
				 $this->db->where_in($key,$value,false);
			 else
				 $this->db->where_in($key,$value);
		  }
		  unset($params['in_param']);
		}

	if (isset($params['not_in_param'])) {
		   foreach ($params['not_in_param'] as $k => $v) {
		   $this->db->where_not_in($k,$v);
		   }
		   unset($params['not_in_param']);
	 }

	   if (isset($params['or_param'])) {
	   foreach ($params['or_param'] as $k => $v) {
		   $this->db->or_where([$k => $v]);
	   }
	   unset($params['or_param']);
	 }

		$this->db->where($params);
		$this->db->where('izin_reject','0');
		if ($all==0) {
		  $this->db->limit($rows, $offset);
		}
	$this->db->order_by('cuti.date_created', 'DESC');
	$this->db->join('branch', 'cuti.branch_id = branch.branch_id','left');
	$this->db->join('users', 'users.id_user = cuti.id_user','left');
	$this->db->join('position', 'users.id_position = position.id_position','left');
	
	$query = $this->db->get();
	return $query->result_array();
 }

//mobile
 public function get_data_last_cuti_new($params=[], $rows=0, $offset=0,$all=0,$current_user){
	$this->db->select(['users.id_user','users.name','users.last_name','users.id_position','cuti.*','branch.branch_name','position.name_position','(select is_hrd from cuti_receiver where id_cuti=cuti.id_cuti and id_receiver = '.$current_user.' limit 1) as is_hrd','users.id_position','users.id_bagian']);
	$this->db->from('cuti');

 if (isset($params['in_param'])) {
	  foreach ($params['in_param'] as $key => $value) {
		 if (strpos($value, '(') !== false)
			 $this->db->where_in($key,$value,false);
		 else
			 $this->db->where_in($key,$value);
	  }
	  unset($params['in_param']);
	}

if (isset($params['not_in_param'])) {
	   foreach ($params['not_in_param'] as $k => $v) {
	   $this->db->where_not_in($k,$v);
	   }
	   unset($params['not_in_param']);
 }

   if (isset($params['or_param'])) {
   foreach ($params['or_param'] as $k => $v) {
	   $this->db->or_where([$k => $v]);
   }
   unset($params['or_param']);
 }
	$this->db->where($params);
	if ($all==0) {
	  $this->db->limit($rows, $offset);
	}
$this->db->order_by('cuti.date_created', 'DESC');
$this->db->join('branch', 'cuti.branch_id = branch.branch_id','left');
$this->db->join('users', 'users.id_user = cuti.id_user','left');
$this->db->join('position', 'users.id_position = position.id_position','left');

$query = $this->db->get();
return $query->result_array();
}


	public function get_last_id(){
		$this->db->select('cuti.id_cuti');
	   	$this->db->from('cuti');
	   	$this->db->order_by('cuti.id_cuti','DESC');
	   	$this->db->limit(1);
		$query = $this->db->get();
	   	return $query->result_array();
	}
    public function get_karyawan($params=[], $rows=0, $offset=10,$all=0){
	   $this->db->select(['users.id_user','users.name','users.last_name','users.CUTI','users.cuti_keseluruhan','users.branch_id','branch.branch_name','bagian.name as nama_bagian','position.name_position','users.STATUS','users.TGLMASUK','users.id_position','users.id_bagian','users.last_reset_cuti','users.masabakti','users.masabakti_keseluruhan','users.last_reset_masabakti']);
	   $this->db->from('users');
	   if (isset($params['in_param'])) {
	     foreach ($params['in_param'] as $key => $value) {
	         $this->db->where_in($key,$value);
	     }
	     unset($params['in_param']);
	   }

	   if (isset($params['not_in_param'])) {
        foreach ($params['not_in_param'] as $k => $v) {
            $this->db->where_not_in($k,$v);
        }
        unset($params['not_in_param']);
      }

	   $this->db->where($params);
	   if ($all==0) {
	     $this->db->limit($rows, $offset);
	   }
	   $this->db->order_by('name', 'ASC');
	   $this->db->join('branch', 'users.branch_id = branch.branch_id','left');
	   $this->db->join('bagian', 'users.id_bagian = bagian.id','left');
	   $this->db->join('position', 'users.id_position = position.id_position','left');
	   $query = $this->db->get();
	   return $query->result_array();
	}

	public function save_cuti($data){
		$this->db->insert('cuti', $data);
		$id_cuti = $this->db->insert_id();
		return $id_cuti;
	}

	public function save_cuti_receiver($data){
		$save = $this->db->insert('cuti_receiver', $data);
		return $save;
	}

	public function getCutiReceiver($params = []){
		$this->db->select('*');
	   	$this->db->from('cuti_receiver');
	   	$this->db->where($params);

	   	$query = $this->db->get();
	   	return $query->result_array();
	}

	public function get_user_read($params=[])
    {
        $this->db->select('users.name,users.last_name as last_name, name_position, position.id_position, id_receiver, is_view, is_view, is_hrd, is_acc');
        $this->db->from('cuti_receiver');
        $this->db->join('users', 'cuti_receiver.id_receiver = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');

        if (isset($params['in_param'])) {
	     	foreach ($params['in_param'] as $key => $value) {
	        	$this->db->where_in($key,$value);
	     	}
	     	unset($params['in_param']);
	   	}
        $this->db->where($params);

        $query = $this->db->get();
	   	return $query->result_array();

    }

    public function delete_cuti($id_cuti,$type_delete='soft'){
		$this->db->where('id_cuti', $id_cuti);
		if ($type_delete == 'soft') {
			$data['deleted'] = 1;
	    	$q = $this->db->update('cuti', $data);
		}else{
			/*hard way*/
			$this->db->where('id_cuti', $id_cuti);
			$q = $this->db->delete('cuti');
		}

	    return $q;
	}

	public function delete_cuti_receiver($id_cuti){
		$this->db->where('id_cuti', $id_cuti);
	    $q = $this->db->delete('cuti_receiver');
	    return $q;
	}

	public function delete_notification($id_cuti){
		/*by id_receiver*/
		$param = [
			'cuti_flag' => 1,
			'data like ' => '%{"id":'.$id_cuti.',"notif_type":"reqcuti","id_cuti":'.$id_cuti.'}%'
		];
		$this->db->where($param);
	    $q = $this->db->delete('notifications');
	    return $q;
	}

	public function get_cuti_notification($id_cuti)
	{
		return $this->db->get_where('cuti_notif', ['id_cuti'=>$id_cuti])->result();
	}

	public function update_cuti_receiver($data,$param){
	    $this->db->where($param);
	    $q = $this->db->update('cuti_receiver', $data);
	    return $q;
	}

	public function update_cuti($data,$param){
	    $this->db->where($param);
	    $q = $this->db->update('cuti', $data);
		$q = $this->db->update('admin_mis.cuti', $data);
	    return $q;
	}

	public function update_notification($data,$param){
	    $this->db->where($param);
	    $q = $this->db->update('notifications', $data);
	    return $q;
	}

	public function restore_cuti($id_cuti){
	    $param['id_cuti'] = $id_cuti;
	    $data = [
	    	'deleted' => 0
	    ];
	    $this->db->where($param);
	    $q = $this->db->update('cuti', $data);
	    return $q;
	}

	public function get_notification($id){
        return $this->db->from("notifications")
            ->where([
                "to_id" => $id,
                "cuti_flag" => 1
            ])
            
			/*->group_start()
                ->or_where(["read" => null])
                ->or_where(["read" => 0])
            ->group_end()*/
			
			->select('notifications.*,(select image from users where id_user = notifications.from_id) as photo_profile,(select name from users where id_user = notifications.from_id) as creator_name')
			->limit(20)
			
			->order_by('id', 'desc')
            ->get();
    }
	
	
	public function no_read_notification($id)
    {
        return $this->db->from("notifications")
            ->where([
				"to_id" => $id,
                "cuti_flag" => 1
            ])
            ->select('id')
			
			->where(["read" => "0"])
			->order_by('id', 'desc')
            ->get();
        
        // $this->db->select('*');
        // $this->db->from('notifications');
        // $this->db->where('to_id', $id);
        // $this->db->where('read', 0);
        // $this->db->where('read <>', 1);
        // $this->db->order_by('id', 'desc');
        
        // return $this->db->get();
    }


}
