<?php

defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 * Model untuk Modul Pembelian di tab Pembelian
 * Pada Url .../admin/logistk
 */
class M_Pembelian extends CI_model
{   

    function __construct()
    {
        parent::__construct();
        $this->tb_distributor                   = "admin_logistik.supplier"; // tabel distributor
        $this->tb_kurs                          = "admin_logistik.kurs";
        $this->tb_harga_reagent                 = "admin_logistik.harga_reagent";
        $this->tb_harga_non_reagent             = "admin_logistik.harga_non_reagent";
        $this->tb_barang                        = "admin_logistik.msbarang";
        $this->tb_supplier                      = "admin_logistik.supplier";
        $this->tb_retur_distributor             = "admin_logistik.retur";
        $this->tb_retur_distributor_detail      = "admin_logistik.returdetail";
        $this->tb_purchase_order                = "admin_logistik.po";
        $this->tb_purchase_order_detail         = "admin_logistik.podetail";
        $this->tb_proforma_purchase_order       = "admin_logistik.pr";
        $this->tb_proforma_purchase_order_detail= "admin_logistik.prdetail";
        $this->tb_account_payable               = "admin_logistik.ap";
        $this->tb_pricing                       = "admin_logistik.pricing";
        $this->tb_purchase_request              = "admin_logistik.pr";
        $this->tb_purchase_request_detail       = "admin_logistik.prdetail";

        $this->tb_evaluasi                      = "admin_logistik.supplier_rating";
    }

    function selectDistributor($id = null){                

        // Jika data ID tidak kosong , maka ambil item berdasarkan ID        
        $where=array();
        if ($id != null){
            $this->db->where("SupplierID", $id);
            $where['id']    = $id;
        }

        $this->db->where($where);
        return $this->db->get($this->tb_distributor);
        
    }

    function selectDistributorWithFilter($where = null){
        if($where != null) {
            $t = '';
            foreach($where as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            //print_r($t);                        
            $this->db->where($t);
        }
        
        return $this->db->get($this->tb_distributor);
    }

    function _generateSupplierID(){

        $this->db
            ->from($this->tb_distributor)
            ->order_by('SupplierID','desc');

        $data = $this->db->get()->row_array();

        $SupplierID = "DS0001";

        if($data){
            
            $preCode = 'DS';
            $currInc = intval(str_replace($preCode, '', $data['SupplierID']));

            $preNum = "";
            switch (strlen($currInc)) {
                case 1:
                    $preNum = "000";
                    break;
                case 2:
                    $preNum = "00";
                    break;
                case 3:
                    $preNum = "0";
                    break;
                case 4:
                    $preNum = "";
                    break;
                default:
                    break;
            }

            $SupplierID = $preCode+$preNum+$currInc;

        }

        return $SupplierID;

    }

    function insertDistributor($data){

        $data['SupplierID'] = $this->_generateSupplierID();

        $this->db->insert($this->tb_distributor, $data);
        return ($this->db->affected_rows() == 0) ? false : true;

    }

    function updateDistributor($id, $data){
        $this->db->where("SupplierID", $id);
        $this->db->update($this->tb_distributor, $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    function deleteDistributor($id){
        $where = array(
            "SupplierID"    => $id
        );

        $this->db->delete($this->tb_distributor,$where);
        return ($this->db->affected_rows() == 0) ? false : true;        
    }

    function deletePr($NOBUKTI){

        $where = array(
            "NOBUKTI"    => $NOBUKTI
        );

       $this->db->delete($this->tb_proforma_purchase_order,$where);
       $this->db->delete($this->tb_proforma_purchase_order_detail,$where);
       
       return ($this->db->affected_rows() == 0) ? false : true;        

    }

    function deletePo($NOBUKTI){

        $where = array(
            "NOBUKTI"    => $NOBUKTI
        );

       $this->db->delete($this->tb_purchase_order,$where);
       $this->db->delete($this->tb_purchase_order_detail,$where);
       
       return ($this->db->affected_rows() == 0) ? false : true;        

    }

    function disableDistributor($id){
        $this->db->where("SupplierID", $id);
        $data['IsActive']  = 0;
        $this->db->update($this->tb_distributor, $data);
        return ($this->db->affected_rows() == 0) ? false : true;        
    }
    
    function selectKurs($KursID = null){                

        $where = array();

        // Jika data ID tidak kosong , maka ambil item berdasarkan KursID
        if ($KursID != null){
            $where['KursID']    = $KursID;
        }

        $this->db->where($where);
        return $this->db->get($this->tb_kurs);
    }

    function insertKurs($data){

        $this->db->insert($this->tb_kurs, $data);
        return ($this->db->affected_rows() == 0) ? false : true;

    }

    function updateKurs($KursID, $data){

        $where = array(
            "KursID"    => $KursID
        );

        $this->db->update($this->tb_kurs, $data, $where);
        return ($this->db->affected_rows() != 1) ? false : true;

    }

    function deleteKurs($KursID){

        $where = array(
            "KursID"    => $KursID
        );

        $this->db->delete($this->tb_kurs,$where);
        return ($this->db->affected_rows() == 0) ? false : true;        

    }


    function selectHargaReagentWithFilter($where = null){
        if($where != null) {
            $t = '';
            foreach($where as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            //print_r($t);
            
            $where = $t." AND ".$this->tb_barang.'.JenisRG <> "REAGENT"';                        
        }
        else{
            $where[$this->tb_barang.'.JenisRG <>']    = 'REAGENT';
        }
    
        return $this->hargaReagentQuery($where);

    }    

    function selectHargaReagent($KodeBarang = null){                

        $where = array();

        $where[$this->tb_barang.'.JenisRG']    = 'REAGENT';

        // Jika data ID tidak kosong , maka ambil item berdasarkan KursID
        if ($KodeBarang != null){
            $where[$this->tb_pricing.'.KodeBarang'] = $KodeBarang;
        }

        return $this->hargaReagentQuery($where);

    }

    function hargaReagentQuery($where){
        $this->db
            ->select([
                $this->tb_pricing.'.KodeBarang',
                $this->tb_pricing.'.Harga',
                $this->tb_pricing.'.Disc',
                $this->tb_pricing.'.Tanggal',
                $this->tb_barang.'.NamaBarang',
                $this->tb_barang.'.NoKatalog',
                $this->tb_barang.'.Satuan',
                $this->tb_barang.'.KURSID',
                $this->tb_supplier.'.SupplierID',
                $this->tb_supplier.'.NamaPT',
            ])
            ->from($this->tb_pricing)
            ->join($this->tb_barang,$this->tb_barang.'.KodeBarang='.$this->tb_pricing.'.KodeBarang')
            ->join($this->tb_supplier,$this->tb_supplier.'.SupplierID='.$this->tb_barang.'.SupplierID')
            ->where($where)
            ->order_by($this->tb_pricing.'.Tanggal','DESC');

        $tbl = $this->db->get_compiled_select();

        $this->db
            ->from("($tbl) as tbl")
            ->group_by("tbl.KodeBarang");

        $data = $this->db->get();

        return $data;
    }

    function insertHargaReagent($data){

        $this->db->insert($this->tb_pricing, $data);
        return ($this->db->affected_rows() == 0) ? false : true;

    }

    function updateHargaReagent($KodeBarang, $data){

        $where = array(
            "KodeBarang"    => $KodeBarang
        );

        $this->db->update($this->tb_pricing, $data, $where);
        return ($this->db->affected_rows() != 1) ? false : true;

    }

    function deleteHargaReagent($KodeBarang){

        $where = array(
            "KodeBarang"    => $KodeBarang
        );

        $this->db->delete($this->tb_pricing,$where);
        return ($this->db->affected_rows() == 0) ? false : true;

    }

    function selectHargaNonReagentWithFilter($where = null){
        if($where != null) {
            $t = '';
            foreach($where as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            //print_r($t);
            
            $where = $t." AND ".$this->tb_barang.'.JenisRG <> "REAGENT"';                        
        }
        else{
            $where[$this->tb_barang.'.JenisRG <>']    = 'REAGENT';
        }
    
        return $this->hargaNonReagentQuery($where);

    }

    function selectHargaNonReagent($KodeBarang = null){                

        $where = array();

        $where[$this->tb_barang.'.JenisRG <>']    = 'REAGENT';

        // Jika data ID tidak kosong , maka ambil item berdasarkan KursID
        if ($KodeBarang != null){
            $where[$this->tb_pricing.'.KodeBarang'] = $KodeBarang;
        }

        return $this->hargaNonReagentQuery($where);
    }

    function hargaNonReagentQuery($where){
        $this->db
            ->select([
                $this->tb_pricing.'.KodeBarang',
                $this->tb_pricing.'.Harga',
                $this->tb_pricing.'.Disc',
                $this->tb_pricing.'.Tanggal',
                $this->tb_barang.'.NamaBarang',
                $this->tb_barang.'.NoKatalog',
                $this->tb_barang.'.Satuan',
                $this->tb_barang.'.KURSID',
                $this->tb_supplier.'.SupplierID',
                $this->tb_supplier.'.NamaPT',
            ])
            ->from($this->tb_pricing)
            ->join($this->tb_barang,$this->tb_barang.'.KodeBarang='.$this->tb_pricing.'.KodeBarang')
            ->join($this->tb_supplier,$this->tb_supplier.'.SupplierID='.$this->tb_barang.'.SupplierID')
            ->where($where)
            ->order_by($this->tb_pricing.'.Tanggal','DESC');

        $tbl = $this->db->get_compiled_select();

        $this->db
            ->from("($tbl) as tbl")
            ->group_by("tbl.KodeBarang");

        $data = $this->db->get();

        return $data;        
    }

    function insertHargaNonReagent($data){

        $this->db->insert($this->tb_pricing, $data);
        return ($this->db->affected_rows() == 0) ? false : true;

    }

    function updateHargaNonReagent($KodeBarang, $data){

        $where = array(
            "KodeBarang"    => $KodeBarang
        );

        $this->db->update($this->tb_pricing, $data, $where);
        return ($this->db->affected_rows() != 1) ? false : true;

    }

    function deleteHargaNonReagent($KodeBarang){

        $where = array(
            "KodeBarang" => $KodeBarang
        );

        $this->db->delete($this->tb_pricing,$where);
        return ($this->db->affected_rows() == 0) ? false : true;        

    }

    function selectBarang($KodeBarang=NULL,$where=array()){

        // Jika data ID tidak kosong , maka ambil item berdasarkan id
        if ($KodeBarang != null){
            $where['KodeBarang']    = $KodeBarang;
        }

        $this->db->select(['KodeBarang as id','NamaBarang as text']);
        $this->db->where($where);
        return $this->db->get($this->tb_barang);

    }

    function selectSupplier($id = NULL,$term=""){

        $where = array();

        // Jika data ID tidak kosong , maka ambil item berdasarkan id
        if ($id != null){
            $where['id']    = $id;
        }

        // 
        if ($term){
            $this->db->like('NamaPT',$term);
        }

        $this->db->select(['id as id','NamaPT as text']);


        $this->db->where($where);
        return $this->db->get($this->tb_supplier);

    }

    function selectReturDistributorWithFilter($where = null){
        if($where != null) {
            $t = '';
            foreach($where as $v){
                $t = ($t=='') ? $v : $v.' AND '.$t;
            }
            // echo $t;
            $this->db->where($t);
        }        
        
        return $this->db->get($this->tb_retur_distributor);
    }

    function selectReturDistributor($NOBUKTI = null){                

        $where = array();

        // Jika data ID tidak kosong , maka ambil item berdasarkan KursID
        if ($NOBUKTI != null){
            $where['NOBUKTI']    = $NOBUKTI;
        }

        $this->db->where($where);
        return $this->db->get($this->tb_retur_distributor);

    }

    function insertReturDistributor($data){

        $PO = $this->db->get_where($this->tb_purchase_order,['NOBUKTI'=>$data['NOPO']])->row_array();
        if($PO){

            $this->db->trans_start();

            $dataInsert = array(
                "NOBUKTI"       => $data['NOBUKTI'],
                "TANGGAL"       => $data['TANGGAL'],
                "KETERANGAN"    => $data['KETERANGAN'],
                "USERID"        => $data['USERID'],
                "STATUS"        => $data["STATUS"],
                "SUPPLIERID"    => $PO['SUPPLIERID'],
                "NOPO"          => $PO['NOBUKTI'],
                "ALAMAT"        => $PO['ALAMAT'],
                "TELPON"        => $PO['TELPON'],
                "FAX"           => $PO['FAX'],
                "CONTACT"       => $PO['CONTACT'],
                "CONTACTHP"     => $PO['CONTACTHP'],
                "UP"            => $PO['UP'],
                "NPWP"          => $PO['NPWP'],
                "KURSID"        => $PO['KURSID'],
                "KURSVALUE"     => $PO['KURSVALUE'],
                "PPN"           => $PO['P_PPN'],
            );

            $this->db->insert($this->tb_retur_distributor, $dataInsert);

            $query = "
                INSERT INTO ".$this->tb_retur_distributor_detail."(NOBUKTI,KODEBARANG,QTY,EXPDATE,HARGA,DISC,HEADER,FOOTER)
                SELECT '".$data['NOBUKTI']."',KODEBARANG,QTY,EXPDATE,HARGA,DISC,HEADER,FOOTER FROM ".$this->tb_purchase_order_detail." WHERE NOBUKTI='".$data['NOPO']."'";

            $this->db->query($query);

            $this->db->trans_complete();
            return ($this->db->trans_status() === FALSE) ? false : true;

        }

        return false;

    }

    function updateReturDistributor($NOBUKTI,$data){

        $PO = $this->db->get_where($this->tb_purchase_order,['NOBUKTI'=>$data['NOPO']])->row_array();
        if($PO){

            $this->db->trans_start();

            $dataUpdate = array(
                "NOBUKTI"       => $data['NOBUKTI'],
                "TANGGAL"       => $data['TANGGAL'],
                "KETERANGAN"    => $data['KETERANGAN'],
                "USERID"        => $data['USERID'],
                "STATUS"        => $data["STATUS"],
                "SUPPLIERID"    => $PO['SUPPLIERID'],
                "NOPO"          => $PO['NOBUKTI'],
                "ALAMAT"        => $PO['ALAMAT'],
                "TELPON"        => $PO['TELPON'],
                "FAX"           => $PO['FAX'],
                "CONTACT"       => $PO['CONTACT'],
                "CONTACTHP"     => $PO['CONTACTHP'],
                "UP"            => $PO['UP'],
                "NPWP"          => $PO['NPWP'],
                "KURSID"        => $PO['KURSID'],
                "KURSVALUE"     => $PO['KURSVALUE'],
                "PPN"           => $PO['P_PPN'],
            );

            $this->db->where('NOBUKTI',$data['NOBUKTI']);
            $this->db->update($this->tb_retur_distributor, $dataUpdate);

            $this->db->where('NOBUKTI',$data['NOBUKTI']);
            $this->db->delete($this->tb_retur_distributor_detail);

            $query = "INSERT INTO ".$this->tb_retur_distributor_detail."(NOBUKTI,KODEBARANG,QTY,EXPDATE,HARGA,DISC,HEADER,FOOTER) SELECT '".$data['NOBUKTI']."',KODEBARANG,QTY,EXPDATE,HARGA,DISC,HEADER,FOOTER FROM ".$this->tb_purchase_order_detail." WHERE NOBUKTI='".$data['NOPO']."'";
            $this->db->query($query);

            $this->db->trans_complete();
            return ($this->db->trans_status() === FALSE) ? false : true;

        }

        return false;

    }

    function deleteReturDistributor($NOBUKTI){

        $where = array(
            "NOBUKTI"    => $NOBUKTI
        );

        $this->db->trans_start();
        $this->db->delete($this->tb_retur_distributor,$where);
        $this->db->delete($this->tb_retur_distributor_detail,$where);
        $this->db->trans_complete();

        return ($this->db->trans_status() == 0) ? false : true;        

    }


    function selectReturDistributorItem($NOBUKTI){                

        $where = array();

        // Jika data ID tidak kosong , maka ambil item berdasarkan KursID
        if ($NOBUKTI != null){
            $where['NOBUKTI']    = $NOBUKTI;
        }

        $this->db
            ->select([
                $this->tb_barang.".NamaBarang",
                $this->tb_barang.".Satuan",
                $this->tb_retur_distributor_detail.".NOBUKTI",
                $this->tb_retur_distributor_detail.".KODEBARANG",
                $this->tb_retur_distributor_detail.".NOLOT",
                $this->tb_retur_distributor_detail.".EXPDATE",
                $this->tb_retur_distributor_detail.".QTY",
                $this->tb_retur_distributor_detail.".QTYRETUR",
            ])
            ->from($this->tb_retur_distributor_detail)
            ->join($this->tb_barang,$this->tb_barang.'.KodeBarang='.$this->tb_retur_distributor_detail.'.KODEBARANG')
            ->where($where);

        return $this->db->get();

    }

    function updateReturDistributorItem($NOBUKTI, $KODEBARANG, $data){

        $this->db->trans_start();

        $where = array(
            "NOBUKTI"       => $NOBUKTI,
            "KODEBARANG"    => $KODEBARANG
        );

        $this->db->set("TOTAL","HARGA*".$data['QTYRETUR'],false);
        $this->db->update($this->tb_retur_distributor_detail, $data, $where);

        $query = "UPDATE ".$this->tb_retur_distributor." SET SUBTOTAL=(SELECT SUM(TOTAL) FROM ".$this->tb_retur_distributor_detail." WHERE NOBUKTI='".$NOBUKTI."') WHERE NOBUKTI='".$NOBUKTI."'";
        $this->db->query($query);

        $this->db->trans_complete();

        return ($this->db->trans_status() === FALSE) ? false : true;

    }

    function selectPurchaseOrderWithFilter($where = null){
        if($where != null) {
            $t = '';
            foreach($where as $v){
                $t = ($t=='') ? $v : $v.' AND '.$t;
            }
            // echo $t;
            $this->db->where($t);
        }

        return $this->purchaseOrderQuery($where);
    }

    function selectPurchaseOrder($NOBUKTI = null){                

        $where = array();

        // Jika data ID tidak kosong , maka ambil item berdasarkan KursID
        if ($NOBUKTI != null){
            $where['po.NOBUKTI']    = $NOBUKTI;
        }

        return $this->purchaseOrderQuery($where);

    }

    function purchaseOrderQuery($where){
        $this->db
            ->select([
                $this->tb_purchase_order.".NOBUKTI",
                $this->tb_purchase_order.".TANGGAL",
                $this->tb_purchase_order.".NOPR",
                $this->tb_purchase_order.".STATUS",
                $this->tb_purchase_order.".KURSID",
                $this->tb_purchase_order.".TOTAL",
                $this->tb_supplier.".NamaPT"
            ])
            ->from($this->tb_purchase_order.' po')
            ->join($this->tb_supplier , $this->tb_supplier.'.SupplierID = '.$this->tb_purchase_order.'.SUPPLIERID')
            ->where($where);       

        return $this->db->get();        
    }

    function selectPurchaseOrderDetail($NOBUKTI){                

        $where = array();

        // Jika data ID tidak kosong , maka ambil item berdasarkan KursID
        if ($NOBUKTI != null){
            $where['NOBUKTI']    = $NOBUKTI;
        }

        $this->db
            ->select([
                "b.NamaBarang",
                "b.Satuan",
                "pod.QTY",
                "pod.HARGA"
            ])
            ->from($this->tb_purchase_order_detail.' pod')
            ->join($this->tb_barang.' b','b.KodeBarang=pod.KODEBARANG')
            ->where($where);

        return $this->db->get();
 
    }

    function selectPurchaseOrderDetailWithFilter($whereFilter,$NOBUKTI){                

        // $whereFilter = array();

        // Jika data ID tidak kosong , maka ambil item berdasarkan KursID
        if ($NOBUKTI != null){
            $whereFilter['NOBUKTI']    = $NOBUKTI;
        }

        // var_dump($whereFilter);
       
        $this->db
            ->select([
                "b.NamaBarang",
                "b.Satuan",
                "pod.QTY",
                "pod.HARGA"
            ])
            ->from($this->tb_purchase_order_detail.' pod')
            ->join($this->tb_barang.' b','b.KodeBarang=pod.KODEBARANG')
            ->where($whereFilter);

        return $this->db->get();

    }

    //model proforma purchase order 

    function insertProformaPurchaseOrder($data){

        $this->db->insert($this->tb_proforma_purchase_order, $data);
        return ($this->db->affected_rows() == 0) ? false : true;

    }

    function insertProformaPurchaseOrderDetail($data){

        $this->db->insert($this->tb_proforma_purchase_order_detail, $data);
        return ($this->db->affected_rows() == 0) ? false : true;

    }

    // model purchase order

    function insertPurchaseOrder($data){

        $this->db->insert($this->tb_purchase_order, $data);
        return ($this->db->affected_rows() == 0) ? false : true;

    }

    function insertPurchaseOrderDetail($data){

        $this->db->insert($this->tb_purchase_order_detail, $data);
        return ($this->db->affected_rows() == 0) ? false : true;

    }

    function insertEvaluasi($data){

        $this->db->insert($this->tb_evaluasi, $data);
        return ($this->db->affected_rows() == 0) ? false : true;

    }
    function selectPrCabangWithFilter($where = null){
        if($where != null) {
            $t = '';
            foreach($where as $v){
                $t = ($t=='') ? $v : $v.' AND '.$t;
            }
            // echo $t;
            $this->db->where($t);
        }

        return $this->prCabangQuery($where);
    }

    function selectPrCabang($NOBUKTI = null){                

        $where = array();

        // Jika data ID tidak kosong , maka ambil item berdasarkan KursID
        if ($NOBUKTI != null){
            $where['po.NOBUKTI']    = $NOBUKTI;
        }

        return $this->prCabangQuery($where);

    }

    function prCabangQuery($where){
        $this->db
            ->select([
                $this->tb_proforma_purchase_order.".id",
                $this->tb_proforma_purchase_order.".NOBUKTI",
                $this->tb_proforma_purchase_order.".TANGGAL",
                $this->tb_proforma_purchase_order.".STATUS",
                $this->tb_proforma_purchase_order.".KURSID",
                $this->tb_proforma_purchase_order.".GRANDTOTAL",
                $this->tb_proforma_purchase_order.".KETERANGAN",
                $this->tb_proforma_purchase_order.".USERID",
            ])
            ->from($this->tb_proforma_purchase_order.' pr')
            ->order_by('pr.TANGGAL', 'DESC')
            ->where("SUPPLIERID",NULL) 
            ->where($where);       

        return $this->db->get();        
    }

    function selectProformaPurchaseOrderWithFilter($where = null){
        if($where != null) {
            $t = '';
            foreach($where as $v){
                $t = ($t=='') ? $v : $v.' AND '.$t;
            }
            // echo $t;
            $this->db->where($t);
        }

        return $this->proformaPurchaseOrderQuery($where);
    }

    function selectProformaPurchaseOrder($NOBUKTI = null){                

        $where = array();

        // Jika data ID tidak kosong , maka ambil item berdasarkan KursID
        if ($NOBUKTI != null){
            $where['po.NOBUKTI']    = $NOBUKTI;
        }

        return $this->proformaPurchaseOrderQuery($where);

    }

    function proformaPurchaseOrderQuery($where){
        $this->db
            ->select([
                $this->tb_proforma_purchase_order.".id",
                $this->tb_proforma_purchase_order.".NOBUKTI",
                $this->tb_proforma_purchase_order.".TANGGAL",
                $this->tb_proforma_purchase_order.".STATUS",
                $this->tb_proforma_purchase_order.".KURSID",
                $this->tb_proforma_purchase_order.".GRANDTOTAL",
                $this->tb_proforma_purchase_order.".KETERANGAN",
                $this->tb_proforma_purchase_order.".USERID",
                $this->tb_supplier.".NamaPT"
            ])
            ->from($this->tb_proforma_purchase_order.' pr')
            ->join($this->tb_supplier , $this->tb_supplier.'.SupplierID = pr.SUPPLIERID')
            ->order_by('pr.TANGGAL', 'DESC')
            ->where($where);       

        return $this->db->get();        
    }

    function selectProformaPurchaseOrderDetail($NOBUKTI){                 

        $where = array();

        // Jika data ID tidak kosong , maka ambil item berdasarkan KursID
        if ($NOBUKTI != null){
            $where['NOBUKTI']    = $NOBUKTI;
        }

        $this->db
            ->select([
                "b.NamaBarang",
                "b.Satuan",
                "pod.QTYPesan",
                "pod.HARGA"
            ])
            ->from($this->tb_proforma_purchase_order_detail.' pod')
            ->join($this->tb_barang.' b','b.KodeBarang=pod.KODEBARANG')
            ->where($where);

        return $this->db->get();
 
    }


    function selectProformaPurchaseOrderDetailWithFilter($whereFilter,$NOBUKTI){                

        // $whereFilter = array();

        // Jika data ID tidak kosong , maka ambil item berdasarkan KursID
        if ($NOBUKTI != null){
            $whereFilter['NOBUKTI']    = $NOBUKTI;
        }

        // var_dump($whereFilter);
       
        $this->db
            ->select([
                "b.NamaBarang",
                "b.Satuan",
                "pod.QTYPesan",
                "pod.HARGA"
            ])
            ->from($this->tb_proforma_purchase_order_detail.' pod')
            ->join($this->tb_barang.' b','b.KodeBarang=pod.KODEBARANG')
            ->where($whereFilter);

        return $this->db->get();

    }

    function selectAccountPayableWithFilter($where = null){
        if($where != null) {
            $t = '';
            foreach($where as $v){
                $t = ($t=='') ? $v : $v.' AND '.$t;
            }
            // echo $t;
            $this->db->where($t);
        }        

        return $this->accountPayableQuery($where);
    }

    function selectAccountPayable($SUPPLIERID = null){                

        $where = array();

        // Jika data ID tidak kosong , maka ambil item berdasarkan KursID
        if ($SUPPLIERID != null){
            $where['ap.SUPPLIERID']    = $SUPPLIERID;
        }
        return $this->accountPayableQuery($where);
    }

    function accountPayableQuery($where){
        $this->db
            ->select([
                "DISTINCT(".$this->tb_account_payable.".SUPPLIERID) as SUPPLIERID",
                $this->tb_supplier.".NamaPT"
            ])
            ->from($this->tb_account_payable)
            ->join($this->tb_supplier,$this->tb_supplier.'.SupplierID='.$this->tb_account_payable.'.SUPPLIERID')
            ->where($where);

        return $this->db->get();        
    }

    function selectAccountPayableDetail($SUPPLIERID){                

        $where = array();

        // Jika data ID tidak kosong , maka ambil item berdasarkan KursID
        if ($SUPPLIERID != null){
            $where['SUPPLIERID']    = $SUPPLIERID;
        }

        return $this->db->get_where($this->tb_account_payable,$where);

    }

    function _paginate(){

        $page = $this->input->post('page');
        $rows = $this->input->post('rows');

        if($page && $rows){
            $this->db->offset(($page-1)*$rows);
            $this->db->limit($rows);
        }


    }

    function getSelectPurchaseOrder($where=array(),$type=""){

        if($type != "all"){
            $this->db->where('NOBUKTI NOT IN (SELECT NOPO FROM '.$this->tb_retur_distributor.')',null,false);
        }

        $this->db
            ->select([
                "NOBUKTI as id","NOBUKTI as text"
            ])
            ->from($this->tb_purchase_order)
            ->where($where);

        return $this->db->get();        

    }

    function getSelectPurchaseRequest($where=array()){

        $this->db
            ->select([
                "NOBUKTI as id","NOBUKTI as text"
            ])
            ->from($this->tb_purchase_request)
            ->where($where);

        return $this->db->get();        

    }

}
