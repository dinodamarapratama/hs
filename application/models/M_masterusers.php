<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

class M_masterusers extends CI_model{	
	public function get_all_users(){
		$this->db->select('users.*,branch.branch_name as branch_name,departments.name as name_department,position.name_position as name_position,bagian.name as name_bagian');
		$this->db->from('users');
		$this->db->join('branch', 'users.branch_id = branch.branch_id');
		$this->db->join('departments', 'users.id_department = departments.id_department');
		$this->db->join('position', 'users.id_position = position.id_position');
		$this->db->join('bagian', 'users.id_bagian = bagian.id');
		$this->db->where('users.STATUS !=','KELUAR');
		return $this->db->get();
	}

	public function get_all_branch(){
		$this->db->select('users.*,branch.branch_name as branch_name,departments.name as name_department,position.name_position as name_position,bagian.name as name_bagian');
		$this->db->from('users');
		$this->db->join('branch', 'users.branch_id = branch.branch_id');
		$this->db->join('departments', 'users.id_department = departments.id_department');
		$this->db->join('position', 'users.id_position = position.id_position');
		$this->db->join('bagian', 'users.id_bagian = bagian.id');
		return $this->db->get();
	}
	public function insert_data($data, $table)
	{
	$this->db->insert($table, $data);
	return $this->db->insert_id();
	}

	public function update_data($where, $data, $table)
	{
	$this->db->where($where);
	$this->db->update($table, $data);
	}
	public function edit_data_masterusers($where, $table){
		$this->db->select('users.*');
		$this->db->from('users');
		$this->db->where($where);
		return $this->db->get();
	}

	public function delete_data($where, $table){
		$this->db->where($where);
		$result = $this->db->delete($table);
		return $result;
	}

	// public function update_data($where, $data, $table){
	// 	$this->db->where($where);
	// 	$this->db->update($table, $data);
	// }

	function load_data_count($params=[]){
		$this->db->select(['users.*','branch.branch_name', 'departments.name as department_name',
		'bagian.name as bagian_name']);
		$this->db->from('users');

		if (isset($params['in_param'])) {
			foreach ($params['in_param'] as $key => $value) {
				$this->db->where_in($key,$value);  
			}
			unset($params['in_param']);
		}
		
		if (isset($params['not_in_param'])) {
			foreach ($params['not_in_param'] as $k => $v) {
				$this->db->where_not_in($k,$v);  
			}
			unset($params['not_in_param']);
		}

		if (isset($params['or_param'])) {
			foreach ($params['or_param'] as $k => $v) {
				$this->db->or_where($k,$v);  
			}
			unset($params['or_param']);
		}
		$this->db->where($params);
		$this->db->order_by('name', 'ASC');
		$this->db->join('branch', 'users.branch_id = branch.branch_id','left');
		$this->db->join('departments', 'users.id_department = departments.id_department','left');
		$this->db->join('bagian', 'users.id_bagian = bagian.id','left');
		$query = $this->db->get();
		return $query->result_array();
	}

	function load_data($params=[], $rows=0, $offset=10,$all=0,$orders=[]){
		$this->db->select(['users.*','branch.branch_name', 'departments.name as department_name',
		'bagian.name as bagian_name','position.name_position as name_position']);
		$this->db->from('users');
		if (isset($params['in_param'])) {
			foreach ($params['in_param'] as $key => $value) {
				$this->db->where_in($key,$value);  
			}
			unset($params['in_param']);
		}
		
		if (isset($params['not_in_param'])) {
			foreach ($params['not_in_param'] as $k => $v) {
				$this->db->where_not_in($k,$v);  
			}
			unset($params['not_in_param']);
		}

		if (isset($params['or_param'])) {
			foreach ($params['or_param'] as $k => $v) {
				$this->db->or_where($k,$v);  
			}
			unset($params['or_param']);
		}

		$this->db->where($params);
		if (!$all) {
			$this->db->limit($rows, $offset);  
		}
		// $this->db->order_by('name', 'ASC');
		$this->db->join('branch', 'users.branch_id = branch.branch_id','left');
		$this->db->join('departments', 'users.id_department = departments.id_department','left');
		$this->db->join('bagian', 'users.id_bagian = bagian.id','left');
		$this->db->join('position', 'users.id_position = position.id_position','left');
		/*order*/
		if (!empty($orders)) {
			$column = $orders['column'];
			$dir = $orders['dir'];
			$this->db->order_by($column,$dir);
		}
		/**/
		$query = $this->db->get();
		return $query->result_array();
	}

	function insert($data){
		$this->db->insert('users', $data);
		$this->db->insert('admin_mis.users', $data);
	}

	function update($data, $id_user){
		$this->db->where('id_user', $id_user);
		$this->db->update('users', $data);
		$this->db->update('admin_mis.users', $data);
	}

	function delete($id_user){

		$this->db->where('id_user', $id_user);
        $this->db->where_in('id_user', $id_user);
		$q = $this->db->delete('users');

		$this->db->where('id_user', $id_user);
        $this->db->where_in('id_user', $id_user);
		$q = $this->db->delete('admin_homeservice.users');

		return $q;
	}

	function getLastFingerID(){
		$this->db->select('CAST(FINGERID as SIGNED) as FINGERID');
		$this->db->where(['nip !=' => '']);
		$this->db->order_by('FINGERID','DESC');
		$this->db->limit(1);
		$res = $this->db->get('users');

		return $res->result_array();
	}

	function get_all_relations($table){
		return $this->db->get($table);
	}

	function getListByID($table,$param=[],$field=[]){
		$this->db->where($param);
		$this->db->select($field);
		$res = $this->db->get($table)->result_array();

		return $res;
	}   


	// public function get_all_finger($table, $params=[], $rows=0, $offset=10,$all=0){
	//   $this->db->select(['id','FingerId','Date','Cabang','min(Recorded) as jam_datang','max(Recorded) as jam_pulang']);
	//   $this->db->where($params);
	//   if (!$all) {
	//     $this->db->limit($rows, $offset);  
	//   }
	//   $this->db->group_by(['Date','FingerId']);
	//   return $this->db->get($table);
	// }


	// public function get_all_finger($params=[]){
	// 	$this->db->select(['e.id as id','e.UserID as FingerId','e.DateLog','ed.Cabang as Cabang','min(e.TimeLog) as jam_datang','max(e.TimeLog) as jam_pulang']);
	// 	$this->db->join('EventLogDevice ed','ed.DeviceID = e.DeviceID');
	// 	$this->db->where($params);
	// 	$this->db->group_by(['DateLog','FingerId']);
	// 	return $this->db->get('EventLog e');
	// }
	
	public function get_all_finger($params=[], $rows=0, $offset=10,$all=0,$orders=[],$search){
		$this->db->select(['e.id as id','e.UserID as FingerId','e.DateLog','e.DeviceID',
		'min(e.TimeLog) as jam_datang',
		'max(e.TimeLog) as jam_pulang',
		])->from("EventLog e");
		$this->db->where($params);
		
		if ($search!='') {
			$this->db->group_start();
			$this->db->like('(SELECT position_name from v_user u where e.UserID = u.FINGERID)',$search);
			$this->db->or_like('(SELECT name from users u where e.UserID = u.FINGERID)',$search);
			$this->db->group_end();
		}
		
		if (!empty($orders)) {
			$column = $orders['column'];
			$dir = $orders['dir'];
			$this->db->order_by($column,$dir);
		}
		if (!$all) {
			$this->db->limit($rows, $offset);  
		}
		$str = $this->db->group_by(['`DateLog`','`UserID`'])->get_compiled_select();
		
		
		
		//print_r($str);
		$str2 =  $this->db
		->select("d.*, 
			(select branch_name from branch b join users u on u.branch_id = b.branch_id where d.FINGERID = u.FINGERID) as Cabang,
			(select `ed`.`Cabang` from EventLogDevice ed where `ed`.`DeviceID` = `d`.`DeviceID`) as Cabangf, 
			(select TimeLog from EventLog where UserID = d.FingerId and DateLog=d.DateLog and FKMode=4 order by id desc limit 1) as jam_istirahat, 
			(select TimeLog from EventLog where UserID = d.FingerId and DateLog=d.DateLog and FKMode=5 order by id desc limit 1) as jam_masuk_istirahat", false)
		->from('('.$str.') as d')
		->get_compiled_select();
		
		
		$str2 = str_replace("`", "", $str2);
		
		return $this->db->query($str2);
	}
	
	public function get_all_finger_count($params=[],$search){
		$this->db->select(['count(e.id) as jml']);
		//$this->db->join('EventLogDevice ed','ed.DeviceID = e.DeviceID');
		//$this->db->join('users u','e.UserID = u.FINGERID', 'left');
		//$this->db->join('branch b','u.branch_id = b.branch_id', 'left');
		$this->db->where($params);
		
		if ($search!='') {
			$this->db->group_start();
			$this->db->like('(SELECT position_name from v_user u where e.UserID = u.FINGERID)',$search);
			$this->db->or_like('(SELECT name from users u where e.UserID = u.FINGERID)',$search);
			$this->db->group_end();
		}
		
		$this->db->group_by(['DateLog','UserID']);
		
		$str = $this->db->from('EventLog e')->get_compiled_select();
		
		$str = str_replace("`", "", $str);
		
		//print_r($str);
		
		return $this->db->query($str);
		//get('EventLog e');
	}

	function getFingerIdName(){
		$this->db->select(['name','FINGERID']);
		return $this->db->get('users');
	}

	function getFingerIdLastName(){
		$this->db->select(['last_name','FINGERID']);
		return $this->db->get('users');
	}

	function getFingerIdPosisi(){
		$this->db->select(['position_name','FINGERID']);
		return $this->db->get('v_user');
	}

	function dateRangeFingerOld($startDate, $endDate){
		return $this->db->query("SELECT f.* FROM v_finger f LEFT JOIN users u on f.FingerId = u.FINGERID WHERE  f.Date >='$startDate' AND f.date <='$endDate'");
	}

	function dateRangeFinger($params=[]){
		//throw new \Exception(print_r($startDate,true));		
		$this->db->distinct();
		$this->db->select(['e.id as id','e.UserID as finger_id',
		'e.DateLog as DateLog', 'u.nip as nip',
		'u.name as name', 'u.last_name AS last_name',
		'ed.Cabang as cabang_finger', 'b.branch_name as user_cabang']);
		$this->db->from('EventLog e');
		$this->db->join('EventLogDevice ed','ed.DeviceID = e.DeviceID', 'left');
		$this->db->join('users u','e.UserID = u.FINGERID', 'left');
		$this->db->join('branch b','u.branch_id = b.branch_id', 'left');
		$this->db->where($params);
		$this->db->group_by(['e.UserID']);
		return $this->db->get();
	
		
		/*
		$this->db->distinct();
		$this->db->select(['e.id as id','e.UserID as finger_id','e.DateLog','e.DeviceID'])->from("EventLog e");
		$this->db->where($params);
		$str = $this->db->group_by(['`UserID`'])->get_compiled_select();
		
		$str2 =  $this->db
		->select("d.*, 
			(select branch_name from branch b join users u on u.branch_id = b.branch_id where d.finger_id = u.FINGERID) as user_cabang,
			(select ed.Cabang from EventLogDevice ed where ed.DeviceID = d.DeviceID) as cabang_finger,
			(select u.nip from users u where d.finger_id = u.FINGERID) as nip,
			(select u.name from users u where d.finger_id = u.FINGERID) as name,
			(select u.last_name from users u where d.finger_id = u.FINGERID) as last_name", false)
		->from('('.$str.') as d')
		->get_compiled_select();
		
		
		$str2 = str_replace("`", "", $str2);
		return $this->db->query($str2);*/
	}

	function dateRangeFingerSunday($params=[]){
		//throw new \Exception(print_r($startDate,true));		
		$this->db->distinct();
		$this->db->select(['e.id as id','e.UserID as finger_id',
		'e.DateLog as DateLog', 'u.nip as nip',
		'u.name as name', 'u.last_name AS last_name',
		'ed.Cabang as cabang_finger', 'b.branch_name as user_cabang']);
		$this->db->from('EventLog e');
		$this->db->join('EventLogDevice ed','ed.DeviceID = e.DeviceID', 'left');
		$this->db->join('users u','e.UserID = u.FINGERID', 'left');
		$this->db->join('branch b','u.branch_id = b.branch_id', 'left');
		$this->db->where($params);
		$this->db->where('weekday(DateLog)','6');
		$this->db->group_by(['e.UserID']);
		return $this->db->get();
		
		
		
		/*
		$this->db->distinct();
		$this->db->select(['e.id as id','e.UserID as finger_id','e.DateLog','e.DeviceID'])->from("EventLog e");
		$this->db->where($params);
		$this->db->where('weekday(DateLog)','6');
		$str = $this->db->group_by(['`UserID`'])->get_compiled_select();
		
		$str2 =  $this->db
		->select("d.*, 
			(select branch_name from branch b join users u on u.branch_id = b.branch_id where d.finger_id = u.FINGERID) as user_cabang,
			(select ed.Cabang from EventLogDevice ed where ed.DeviceID = d.DeviceID) as cabang_finger,
			(select u.nip from users u where d.finger_id = u.FINGERID) as nip,
			(select u.name from users u where d.finger_id = u.FINGERID) as name,
			(select u.last_name from users u where d.finger_id = u.FINGERID) as last_name", false)
		->from('('.$str.') as d')
		->get_compiled_select();
		
		
		$str2 = str_replace("`", "", $str2);
		return $this->db->query($str2);*/
		
	}

	public function get_all_finger_absen($params=[]){
		/*$this->db->select(['e.id as id','e.UserID as FingerId','e.DateLog as DateLog',
		'b.branch_name as Cabang',
		'min(e.TimeLog) as jam_datang','max(e.TimeLog) as jam_pulang',
		'(select TimeLog from EventLog where UserID = e.UserID and DateLog=e.DateLog and FKMode=3 order by id asc limit 1) as jam_istirahat',
		'(select TimeLog from EventLog where UserID = e.UserID and DateLog=e.DateLog and FKMode=3 order by id desc limit 1) as jam_masuk_istirahat']);
		$this->db->from('EventLog e');
		$this->db->join('EventLogDevice ed','ed.DeviceID = e.DeviceID');
		$this->db->join('users u','e.UserID = u.FINGERID', 'left');
		$this->db->join('branch b','u.branch_id = b.branch_id', 'left');
		$this->db->where($params);
		$this->db->group_by(['DateLog','FingerId']);
		return $this->db->get();*/
		
		
		
		
		
		
		$this->db->select(['e.id as id','e.UserID as FingerId','e.DateLog',
		'min(e.TimeLog) as jam_datang',
		'max(e.TimeLog) as jam_pulang',
		'b.branch_name as cabang_finger'
		])->from("EventLog e");
		$this->db->join('branch b','b.branch_id = e.DeviceID');
		$this->db->where($params);
		$str = $this->db->group_by(['`DateLog`','`UserID`'])->get_compiled_select();
		
		$str2 =  $this->db
		->select("d.*, 
			(select branch_name from branch b join users u on u.branch_id = b.branch_id where d.FINGERID = u.FINGERID limit 1) as Cabang,
			(select max(TimeLog) from EventLog where UserID = d.FingerId and DateLog=d.DateLog and FKMode=4) as jam_istirahat, 
			(select max(TimeLog) from EventLog where UserID = d.FingerId and DateLog=d.DateLog and FKMode=5) as jam_masuk_istirahat", false)
		->from('('.$str.') as d')
		->get_compiled_select();
		
		
		$str2 = str_replace("`", "", $str2);
		return $this->db->query($str2);
		
	}

	public function get_all_finger_absen_sun($params=[]){
		/*$this->db->select(['e.id as id','e.UserID as FingerId','e.DateLog as DateLog',
		'b.branch_name as Cabang',
		'min(e.TimeLog) as jam_datang','max(e.TimeLog) as jam_pulang',
		'(select TimeLog from EventLog where UserID = e.UserID and DateLog=e.DateLog and FKMode=3 order by id asc limit 1) as jam_istirahat',
		'(select TimeLog from EventLog where UserID = e.UserID and DateLog=e.DateLog and FKMode=3 order by id desc limit 1) as jam_masuk_istirahat']);
		$this->db->from('EventLog e');
		$this->db->join('EventLogDevice ed','ed.DeviceID = e.DeviceID');
		$this->db->join('users u','e.UserID = u.FINGERID', 'left');
		$this->db->join('branch b','u.branch_id = b.branch_id', 'left');
		$this->db->where($params);
		$this->db->where('weekday(DateLog)','6');
		$this->db->group_by(['DateLog','FingerId']);
		return $this->db->get();*/
		
		$this->db->select(['e.id as id','e.UserID as FingerId','e.DateLog',
		'min(e.TimeLog) as jam_datang',
		'max(e.TimeLog) as jam_pulang',
		'b.branch_name as cabang_finger',
		])->from("EventLog e");
		$this->db->join('branch b','b.branch_id = e.DeviceID');
		$this->db->where($params);
		$this->db->where('weekday(DateLog)','6');
		$str = $this->db->group_by(['`DateLog`','`UserID`'])->get_compiled_select();
		
		$str2 =  $this->db
		->select("d.*, 
			(select branch_name from branch b join users u on u.branch_id = b.branch_id where d.FINGERID = u.FINGERID) as Cabang,
			(select max(TimeLog) from EventLog where UserID = d.FingerId and DateLog=d.DateLog and FKMode=4) as jam_istirahat, 
			(select max(TimeLog) from EventLog where UserID = d.FingerId and DateLog=d.DateLog and FKMode=5) as jam_masuk_istirahat", false)
		->from('('.$str.') as d')
		->get_compiled_select();
		
		
		$str2 = str_replace("`", "", $str2);
		return $this->db->query($str2);
		
	}


	public function get_survey(){
		$this->db->select('survey_kepuasan.*,users.*,branch.branch_name as branch_name,departments.name as name_department,position.name_position as name_position,bagian.name as name_bagian');
		$this->db->from('survey_kepuasan');
		$this->db->join('users', 'users.id_user = survey_kepuasan.id_user');
		$this->db->join('branch', 'survey_kepuasan.id_branch = branch.branch_id');
		$this->db->join('departments', 'users.id_department = departments.id_department');
		$this->db->join('position', 'users.id_position = position.id_position');
		$this->db->join('bagian', 'users.id_bagian = bagian.id');
		
		return $this->db->get();
	}
}
