<?php

class M_cuti_api extends CI_Model {

	public function get_data_cuti($params=[], $rows=0, $offset=0,$all=0){
	   	$this->db->select(['users.id_user','users.name','users.call_name','cuti.*','branch.branch_name','users.id_position','users.id_bagian']);
	   	$this->db->from('cuti');
	   	if (isset($params['in_param'])) {
	     	foreach ($params['in_param'] as $key => $value) {
	         $this->db->where_in($key,$value);
	     	}
	     	unset($params['in_param']);
	   	}

	   if (isset($params['not_in_param'])) {
	      	foreach ($params['not_in_param'] as $k => $v) {
	          $this->db->where_not_in($k,$v);
	      	}
	      	unset($params['not_in_param']);
	    }

      	if (isset($params['or_param'])) {
	      foreach ($params['or_param'] as $k => $v) {
	          $this->db->or_where([$k => $v]);
	      }
	      unset($params['or_param']);
	    }

	   	$this->db->where($params);
	   	if ($all==0) {
	     	$this->db->limit($rows, $offset);
	   	}
	   $this->db->order_by('cuti.date_created', 'DESC');
	   $this->db->join('branch', 'cuti.branch_id = branch.branch_id','left');
	   $this->db->join('users', 'users.id_user = cuti.id_user','left');
	   $this->db->where_not_in('status_pengajuan', 'Dibatalkan');
	   $query = $this->db->get();
	   return $query->result_array();
	}

	public function get_last_id(){
		$this->db->select('cuti.id_cuti');
	   	$this->db->from('cuti');
	   	$this->db->order_by('cuti.id_cuti','DESC');
	   	$this->db->limit(1);
		$query = $this->db->get();
	   	return $query->result_array();
	}

	public function get_last_id_2(){
		$this->db->select('cuti.id_cuti');
	   	$this->db->from('cuti');
	   	$this->db->order_by('cuti.id_cuti','DESC');
	   	$this->db->limit(1);
		$query = $this->db->get();
	   	return $query->row();
	}

    public function get_karyawan($params=[], $rows=0, $offset=10,$all=0){
	   $this->db->select(['users.id_user','users.name','users.call_name','users.last_name','users.CUTI','users.cuti_keseluruhan','branch.branch_name','bagian.name as nama_bagian','position.name_position','users.STATUS','users.TGLMASUK','users.id_position','users.id_bagian','users.branch_id','users.last_reset_cuti','users.masabakti','users.masabakti_keseluruhan','users.last_reset_masabakti']);
	   $this->db->from('users');
	   if (isset($params['in_param'])) {
	     foreach ($params['in_param'] as $key => $value) {
	         $this->db->where_in($key,$value);
	     }
	     unset($params['in_param']);
	   }

	   if (isset($params['not_in_param'])) {
        foreach ($params['not_in_param'] as $k => $v) {
            $this->db->where_not_in($k,$v);
        }
        unset($params['not_in_param']);
      }

	   $this->db->where($params);
	   if ($all==0) {
	     $this->db->limit($rows, $offset);
	   }
	   $this->db->order_by('name', 'ASC');
	   $this->db->join('branch', 'users.branch_id = branch.branch_id','left');
	   $this->db->join('bagian', 'users.id_bagian = bagian.id','left');
	   $this->db->join('position', 'users.id_position = position.id_position','left');
	   $query = $this->db->get();
	   return $query->result_array();
	}

	public function save_cuti($data){
		$this->db->insert('cuti', $data);
		$id_cuti = $this->db->insert_id();
		return $id_cuti;
	}

	public function getCutiReceiverDetail($idUser, $idCuti){
		$receivers = [];
		$getCuti = $this->db->select('c.id_user, cr.id_receiver')
						->from('cuti c')
						->join('cuti_receiver cr', 'cr.id_cuti = c.id_cuti')
						->where('c.id_cuti',$idCuti)
						// ->where('cr.id_receiver','!=',$idUser)
						->where('cr.is_hrd',0)
						->where('cr.is_acc',0)
						->get()->result_array();
		if (count($getCuti) > 0):
			foreach($getCuti as $receiver):
				array_push($receivers, $receiver['id_receiver']);
			endforeach;
		else:
			$getReceiver = $this->db->select('c.id_user, cr.id_receiver')
							->from('cuti c')
							->join('cuti_receiver cr', 'cr.id_cuti = c.id_cuti')
							->where('c.id_cuti',$idCuti)
							->where('cr.is_hrd',1)
							->get()->result_array();
			if (count($getReceiver) > 0):
				foreach($getReceiver as $receiver):
					array_push($receivers, $receiver['id_receiver']);
				endforeach;
			endif;
		endif;
		return [$receiver['id_user'],$receivers];
	}

	public function approveCuti($user,$idCuti){
		if ($user->id_bagian == 22){
			$getCuti =  $this->db->select('c.*, cr.is_acc')
							->from('cuti c')
							->join('cuti_receiver cr', 'cr.id_cuti = c.id_cuti')
							->where('c.id_cuti',$idCuti)
							->where('cr.is_hrd',1)
							->get()->row_array();
			if ($getCuti){
				//$data['status_pengajuan'] = 'Disetujui HRD';
				$data['status_pengajuan'] = 'Disetujui';
				$updateCuti = $this->db->update('cuti', $data, ['id_cuti' => intval($idCuti)]);
				if ($updateCuti){
					$data2['is_acc'] = 1;
					$updateReciver = $this->db->update('cuti_receiver',$data2, ['id_cuti' => intval($idCuti), 'id_receiver' => $user->id_user]);
					return $updateReciver;
				}else{
					return false;
				}
			}
		}else{
			$getCuti =  $this->db->select('c.*, cr.is_acc')
							->from('cuti c')
							->join('cuti_receiver cr', 'cr.id_cuti = c.id_cuti')
							->where('c.id_cuti',$idCuti)
							->where('cr.id_receiver',$user->id_user)
							->get()->row_array();
			if ($getCuti){
				//$data['status_pengajuan'] = 'Disetujui '.$user->name;
				$data['status_pengajuan'] = 'Disetujui';
				$updateCuti = $this->db->update('cuti', $data, ['id_cuti' => intval($idCuti)]);
				if ($updateCuti){
					$data2['is_acc'] = 1;
					$updateReciver = $this->db->update('cuti_receiver',$data2, ['id_cuti' => intval($idCuti), 'id_receiver' => $user->id_user]);
					return $updateReciver;
				}else{
					return false;
				}
			}
		}
	}

	public function rejectCuti($user,$idCuti, $alasan){
		/*if ($user->id_bagian == 22):
			$data['status_pengajuan'] = 'DiTolak HRD';
		else:
			$data['status_pengajuan'] = 'Ditolak Atasan';
		endif;*/
		
		$data['status_pengajuan'] = 'Ditolak!';
		
		$data['reject_by'] = $user->id_user;
		$data['reject_reason'] = $alasan;
		$data['deleted'] = 1;
		$updateCuti = $this->db->update('cuti', $data, ['id_cuti' => intval($idCuti)]);
		$data2['is_closed'] = 1;
		$updateCutiReceiver = $this->db->update('cuti_receiver', $data2, ['id_cuti' => intval($idCuti)]);
		
		$cuti =  $this->db->select('*')
							->from('cuti')
							->where('id_cuti',$idCuti)
							->get()->row_array();
		$getUserCuti = $this->db->select('*')->from('users')->where('id_user',$cuti['id_user'])->get()->row_array();
		
		if ($cuti['status_cuti']=='tahunan') {
			$data3['CUTI'] = (intval($getUserCuti['CUTI'])+intval($cuti['request_cuti']));
			$updateCutiUser = $this->db->update('users', $data3, ['id_user' => intval($getUserCuti['id_user'])]);
		}
		else if ($cuti['status_cuti']=='masabakti') {
			$data3['masabakti'] = (intval($getUserCuti['masabakti'])+intval($cuti['request_cuti']));
			$updateCutiUser = $this->db->update('users', $data3, ['id_user' => intval($getUserCuti['id_user'])]);
		}
		return $updateCuti;
	}

	public function cancelCuti($user,$idCuti, $alasan){
		$data['status_pengajuan'] = 'Dibatalkan';
		$data['reject_by'] = $user->id_user;
		$data['reject_reason'] = $alasan;
	//	$data['jatah_diambil'] = 0;
		$data['deleted'] = 1;
		$updateCuti = $this->db->update('cuti', $data, ['id_cuti' => intval($idCuti)]);
		$data2['is_closed'] = 1;
		$updateCutiReceiver = $this->db->update('cuti_receiver', $data2, ['id_cuti' => intval($idCuti)]);

		$cuti =  $this->db->select('*')
							->from('cuti')
							->where('id_cuti',$idCuti)
							->get()->row_array();
		$getUserCuti = $this->db->select('*')->from('users')->where('id_user',$cuti['id_user'])->get()->row_array();
		
		if ($cuti['status_cuti']=='tahunan') {
			$data3['CUTI'] = (intval($getUserCuti['CUTI'])+intval($cuti['request_cuti']));
			$updateCutiUser = $this->db->update('users', $data3, ['id_user' => intval($getUserCuti['id_user'])]);
		}
		else if ($cuti['status_cuti']=='masabakti') {
			$data3['masabakti'] = (intval($getUserCuti['masabakti'])+intval($cuti['request_cuti']));
			$updateCutiUser = $this->db->update('users', $data3, ['id_user' => intval($getUserCuti['id_user'])]);
		}

		return $updateCuti;
	}
	public function UpdateStatusCuti($id){
			$cuti =  $this->db->select('*')
							->from('cuti')
							->where('id_cuti',$id)
							->get()->row_array();
			$data['deleted'] = '1';
			$data['status_pengajuan'] = 'Disetujui';
			$data['jatah_diambil'] = $cuti['request_cuti'];
			$data['sisa_jatah'] = (intval($cuti['sisa_jatah'])-intval($cuti['request_cuti']));
			$data['sisa_cuti'] = (intval($cuti['sisa_cuti'])-intval($cuti['request_cuti']));
			$updateCuti = $this->db->update('cuti', $data, ['id_cuti' => intval($id)]);
			/*$getUserCuti = $this->db->select('*')->from('users')->where('id_user',$cuti['id_user'])->get()->row_array();
			if ($getUserCuti):
				$data2['CUTI'] = (intval($getUserCuti['CUTI'])-intval($cuti['request_cuti']));
				$updateCutiUser = $this->db->update('users', $data2, ['id_user' => intval($getUserCuti['id_user'])]);
			endif;*/
			$data3['is_closed'] = 1;
			$updateCutiReceiver = $this->db->update('cuti_receiver', $data3, ['id_cuti' => intval($id)]);
			return $updateCuti;
	}

	public function UpdateStatusCuti_ACCAtasan($id){
		$cuti =  $this->db->select('*')
						->from('cuti')
						->where('id_cuti',$id)
						->get()->row_array();
		$data['status_pengajuan'] = 'Sedang diproses!';
		$updateCuti = $this->db->update('cuti', $data, ['id_cuti' => intval($id)]);
		return $updateCuti;
}

	public function save_cuti_receiver($data){
		$save = $this->db->insert('cuti_receiver', $data);
		return $save;
	}

	public function updateCutiAsRead($userId, $cutiId){
		$updateCuti = $this->db->update('cuti_receiver',
			['is_view' => 1],
			[
				'id_cuti' => intval($cutiId),
				'id_receiver' => intval($userId)
			]
		);
		return $updateCuti;
	}

	public function getReceiver($cutiId){
		$getReceiver = $this->db->select('cr.*,u.name,u.username,u.image,p.name_position')
						->from('cuti_receiver cr')
						->join('users u', 'cr.id_receiver=u.id_user')
						->join('position p', 'u.id_position=p.id_position')
						->where('cr.id_cuti',$cutiId)
						->get();
		return $getReceiver->result_array();
	}

	public function getCutiReceiver($params = []){
		$this->db->select('*');
	   	$this->db->from('cuti_receiver');
	   	$this->db->where($params);

	   	$query = $this->db->get();
	   	return $query->result_array();
	}

	public function get_user_read($params=[])
    {
        $this->db->select('name, name_position, position.id_position, id_receiver, is_view');
        $this->db->from('cuti_receiver');
        $this->db->join('users', 'cuti_receiver.id_receiver = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');

        if (isset($params['in_param'])) {
	     	foreach ($params['in_param'] as $key => $value) {
	        	$this->db->where_in($key,$value);
	     	}
	     	unset($params['in_param']);
	   	}
        $this->db->where($params);

        $query = $this->db->get();
	   	return $query->result_array();

    }

    public function delete_cuti($id_cuti,$type_delete='soft'){
		$this->db->where('id_cuti', $id_cuti);
		if ($type_delete == 'soft') {
			$data['deleted'] = 1;
	    	$q = $this->db->update('cuti', $data);
		}else{
			/*hard way*/
			$this->db->where('id_cuti', $id_cuti);
			$q = $this->db->delete('cuti');
		}

	    return $q;
	}

	public function delete_cuti_receiver($id_cuti){
		$this->db->where('id_cuti', $id_cuti);
	    $q = $this->db->delete('cuti_receiver');
	    return $q;
	}

	public function delete_notification($id_cuti){
		/*by id_receiver*/
		$param = [
			'cuti_flag' => 1,
			'data like ' => '%{"id":'.$id_cuti.',"notif_type":"reqcuti","id_cuti":'.$id_cuti.'}%'
		];
		$this->db->where($param);
	    $q = $this->db->delete('notifications');
	    return $q;
	}

	public function update_cuti_receiver($data,$param){
	    $this->db->where($param);
	    $q = $this->db->update('cuti_receiver', $data);
	    return $q;
	}

	public function update_cuti($data,$param){
	    $this->db->where($param);
	    $q = $this->db->update('cuti', $data);
	    return $q;
	}

	public function update_notification($data,$param){
	    $this->db->where($param);
	    $q = $this->db->update('notifications', $data);
	    return $q;
	}

	public function restore_cuti($id_cuti){
	    $param['id_cuti'] = $id_cuti;
	    $data = [
	    	'deleted' => 0
	    ];
	    $this->db->where($param);
	    $q = $this->db->update('cuti', $data);
	    return $q;
	}

	public function getSent($user,$param){
		$query = $this->db->select('ct.*,ct.id_cuti AS id,ct.date_created AS date,usr.name,usr.call_name,usr.image,br.branch_name,pos.name_position')
		 				->from('cuti ct')
						->join('branch br', 'br.branch_id = ct.branch_id', 'left')
						->join('users usr', 'usr.id_user = ct.id_user', 'left')
						->join('position pos', 'pos.id_position = usr.id_position', 'left')
						->where('ct.id_user',$user)
						->where('ct.izin',0)
						->group_start()
						->or_like($param['like'])
						->group_end()
						->where($param['where'])
						//->where_not_in('ct.status_pengajuan',['Sudah Disetujui!'])
						->where('ct.deleted',0)
						->order_by('ct.id_cuti','desc')
						->get();
		return $query->result();
	}

	// archived bukan rekap
	public function getArchived($user,$param){
		$query = $this->db->select('ct.*,ct.id_cuti AS id,ct.date_created AS date,usr.name,usr.call_name,usr.image,br.branch_name,pos.name_position')
		 				->from('cuti ct')
						->join('branch br', 'br.branch_id = ct.branch_id', 'left')
						->join('users usr', 'usr.id_user = ct.id_user', 'left')
						->join('position pos', 'pos.id_position = usr.id_position', 'left')
						->where('ct.id_user',$user)
						->where('ct.izin',0)
						->group_start()
						->or_like($param['like'])
						->group_end()
						->where($param['where'])
						//->where_not_in('ct.status_pengajuan',['Sudah Disetujui!'])
						->where('ct.deleted',1)
						->order_by('ct.id_cuti','desc')
						->get();
		return $query->result();
	}

	public function getInbox($user,$param){
		$this->db->select('ct.*,ct.id_cuti AS id,ct.date_created AS date,usr.name,usr.call_name,usr.image,br.branch_name,pos.name_position')
 				->from('cuti ct')
 				->join('cuti_receiver ctr', 'ctr.id_cuti = ct.id_cuti','left')
				->join('branch br', 'br.branch_id = ct.branch_id', 'left')
				->join('users usr', 'usr.id_user = ct.id_user', 'left')
				->join('position pos', 'pos.id_position = usr.id_position', 'left')
				->where('(ct.id_cuti in (select id_cuti from cuti_receiver where id_receiver ='.$user->id_user.')) and ct.deleted=0',null)
				->group_start()
				->or_like($param['like'])
				->group_end()
				->where($param['where'])
				->where('ctr.is_acc',0)
				->where('ctr.id_receiver',$user->id_user)
				->where('ctr.is_asigned',1)
				->order_by('ct.id_cuti','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function getInboxHR($user,$param){
		$this->db->select('ct.*,ct.id_cuti AS id,ct.date_created AS date,usr.name,usr.call_name,usr.image,br.branch_name,pos.name_position')
 				->from('cuti ct')
 				// ->join('cuti_receiver ctr', 'ctr.id_cuti = ct.id_cuti','left')
				->join('branch br', 'br.branch_id = ct.branch_id', 'left')
				->join('users usr', 'usr.id_user = ct.id_user', 'left')
				->join('position pos', 'pos.id_position = usr.id_position', 'left')
				->where('ct.status_pengajuan not like "%setujui%" and ct.status_pengajuan not like "%tolak%" and ct.status_pengajuan not like "%batal%" and ct.deleted=0',null)
				->where('ct.izin',0)
				->group_start()
				->or_like($param['like'])
				->group_end()
				->where($param['where'])
				// ->where('ctr.is_acc',0)
				//->where('ctr.id_receiver',$user->id_user)
				->order_by('ct.id_cuti','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function getRekap($user,$param){
		$query = $this->db->select('ct.*,ct.id_cuti AS id,ct.date_created AS date,usr.name,usr.call_name,usr.image,br.branch_name,pos.name_position')
		 				->from('cuti ct')
						->join('branch br', 'br.branch_id = ct.branch_id', 'left')
						->join('users usr', 'usr.id_user = ct.id_user', 'left')
						->join('position pos', 'pos.id_position = usr.id_position', 'left')
						//->where('ct.id_user',$user)
						//->where('ct.status_pengajuan','!=','')
						->where('(ct.id_user ='.$user.' or ct.id_cuti in (select id_cuti from cuti_receiver where id_receiver ='.$user.' and is_asigned=1)) and ct.deleted=1',null)
						->group_start()
						->or_like($param['like'])
						->group_end()
						->where($param['where'])
						->order_by('ct.id_cuti','desc')
						->get();
		return $query->result();
	}

	public function getRekapHR($user,$param){
		$query = $this->db->select('ct.*,ct.id_cuti AS id,ct.date_created AS date,usr.name,usr.call_name,usr.image,br.branch_name,pos.name_position')
		 				->from('cuti ct')
						->join('branch br', 'br.branch_id = ct.branch_id', 'left')
						->join('users usr', 'usr.id_user = ct.id_user', 'left')
						->join('position pos', 'pos.id_position = usr.id_position', 'left')
						//->where('ct.id_user',$user)
						//->where('ct.status_pengajuan','!=','')
						// ->where('(ct.id_user ='.$user.' or ct.id_cuti in (select id_cuti from cuti_receiver where is_asigned=1))',null)
						->where('ct.izin',0)
						->group_start()
						->or_like($param['like'])
						->group_end()
						->where($param['where'])
						->order_by('ct.id_cuti','desc')
						->get();
		return $query->result();
	}

	function detail($id){
		if($id) {
			$query = $this->db->select('ct.*,ct.id_cuti AS id,ct.date_created AS date,usr.name,usr.call_name,usr.image,usr.TGLMASUK as tglmasuk,br.branch_name,pos.name_position,usr.cuti_keseluruhan,usr.CUTI,usr.masabakti_keseluruhan')
 				->from('cuti ct')
				->join('branch br', 'br.branch_id = ct.branch_id', 'left')
				->join('users usr', 'usr.id_user = ct.id_user', 'left')
				->join('position pos', 'pos.id_position = usr.id_position', 'left')
				->where('ct.id_cuti',$id)
				->get();
		}

		return $query->row();
	}

	public function get_notification($id)
    {
        return $this->db->from("notifications")
            ->where([
                "to_id" => $id,
                "cuti_flag" => 1
            ])
            ->group_start()
                ->or_where(["read" => null])
                ->or_where(["read" => 0])
            ->group_end()
            ->get();
    }

}
