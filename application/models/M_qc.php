<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_qc extends CI_model
{

    public function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $result = $this->db->delete($table);
        return $result;
    }

     public function get_all_masterqc()
    {
        $this->db->select('master_qc.*,users.*');
        $this->db->from('master_qc');
        $this->db->join('users','users.id_user = master_qc.creator_id');
        return $this->db->get();
    }

    public function edit_data_masterqc($where, $table)
    {
        $this->db->select('master_qc.*');
        $this->db->from('master_qc');
        $this->db->where($where);
        return $this->db->get();
    }


    public function get_all_relations($table)
    {
        return $this->db->get($table);
    }


    /* start input qc */

    function dt_input_qc($args = []){
        $sql = $this->baseQueryInputQC($args)->where($args['where'])->get_compiled_select();
        $this->load->helper("dt");
        $p = [
            "sql" => $sql,
        ];
        if (isset($args['disable_paging'])) {
            $p['disable_paging'] = 1;
        }
        return getDataTable($p);
    }
    
    // mobile
    function dt_input_qc_m($args = []){
        $this->load->helper('myquery');
        $q = myquery($this->baseQueryInputQC($args), $args);
        $sql = $q->get()->result();
        return $sql;
    }


    function select_lot_ctrl($args = [],$group_by=[]){
        $sql = $this->baseQueryLotCtrl($args)->where($args['where'])->get_compiled_select();
        if (!empty($group_by)) {
            $sql = $this->baseQueryLotCtrl($args)->where($args['where'])->group_by($group_by)->get_compiled_select();
        }
        $this->load->helper("dt");
        $p = [
            "sql" => $sql,
        ];
        if (isset($args['disable_paging'])) {
            $p['disable_paging'] = 1;
        }
        return getDataTable($p);
    }

    function select_lot_ctrl_m($args = [],$group_by=[]){
        $this->load->helper('myquery');
        $q = myquery($this->baseQueryLotCtrl($args), $args);
        $sql = $q->where($args['where'])->group_by($group_by)->get()->result();
        return $sql;
    }

    function baseQueryLotCtrl($args = []) {
        if($args == null) {
            $args = [];
        }
        
        $q = $this->db->from("qc_input _")
            ->join("master_test mtest", "mtest.test_id = _.id_test", "left")
            ->join("master_test_group tgroup", "tgroup.group_id = mtest.group_id", "test")
            ->join("master_qc_lot_number ln", "ln.id_lot_number = _.id_lot_number", "left")
            ->join("master_control ctrl", "ctrl.id_control = ln.id_control", "left")
            ->join("master_qc_alat alat", "alat.id_alat = _.id_alat", "left")
            ->join("branch b1", "b1.branch_id = _.branch_id", "left")
            ->join("users c", "c.id_user = _.creator_id", "left")
            ->select([
                "_.*",
                "alat.alat_name",
                "mtest.test_name",
                "ln.lot_number",
                "ctrl.control_name",
                "b1.branch_name",
                'c.username as creator_username',
                'c.name as creator_name'
            ]);
        return $q;
    }


    function baseQueryInputQC($args = []) {
        if($args == null) {
            $args = [];
        }
        if(isset($args["join_detail"]) == false) {
            $args["join_detail"] = true;
        }
        if(empty($args['include_info_hasil']) == true) {
            $args['include_info_hasil'] = false;
        }
        $sqlDetail = "";
        if($args["join_detail"] == true) {
            $q = $this->db->from("qc_input_detail qid")
                ->join("master_qc_level lvl", "lvl.id_level = qid.id_level", "left")
                ->select([
                    "GROUP_CONCAT(qid.id_level SEPARATOR '-----') as list_id_level",
                    "GROUP_CONCAT(lvl.level_name SEPARATOR '-----') as list_level_name",
                    "GROUP_CONCAT(qid.mean SEPARATOR '-----') as list_mean",
                    "GROUP_CONCAT(qid.sd SEPARATOR '-----') as list_sd",
					"GROUP_CONCAT(qid.range1 SEPARATOR '-----') as range1",
					"GROUP_CONCAT(qid.range2 SEPARATOR '-----') as range2",
                    "id_qc_input"
                ])
                ->group_by("id_qc_input");
            $sqlDetail = $q->get_compiled_select();
        }
        $qInfoHasil = '';
        if($args['include_info_hasil'] == true) {
            $q = $this->db->from('qc_hasil _')
                    ->group_by("_.id_qc_input")
                    ->select([
                        "_.id_qc_input",
                        "count(*) as jumlah"
                    ]);
            $qInfoHasil = $q->get_compiled_select();
        }
        $q = $this->db->from("qc_input _")
            ->join("users usr", "usr.id_user = _.creator_id", "left")
            ->join("master_test mtest", "mtest.test_id = _.id_test", "left")
            ->join("master_test_group tgroup", "tgroup.group_id = mtest.group_id", "test")
            ->join("master_qc_lot_number ln", "ln.id_lot_number = _.id_lot_number", "left")
            ->join("master_control ctrl", "ctrl.id_control = ln.id_control", "left")
            ->join("master_qc_alat alat", "alat.id_alat = _.id_alat", "left")
            ->join("branch b1", "b1.branch_id = _.branch_id", "left")
            ->select([
                "_.*",
                "usr.name as creator_name",
                "usr.username as creator_username",
                "alat.alat_name",
                "mtest.test_name",
                "ln.lot_number",
                "CONCAT(ln.lot_number, ' - ', ctrl.control_name) as lot_control_name",
                "tgroup.group_id as id_test_group",
                "b1.branch_name"
            ]);
        if($args["join_detail"] == true) {
            $q->join("($sqlDetail) dtl", "dtl.id_qc_input = _.id_qc_input", "left")
                ->select([
                    "dtl.list_id_level",
                    "dtl.list_level_name",
                    "dtl.list_mean",
                    "dtl.list_sd",
					"dtl.range1",
					"dtl.range2"
                ]);
        }
        if($args['include_info_hasil'] == true) {
            $q->join("($qInfoHasil) iqh", 'iqh.id_qc_input = _.id_qc_input', 'left')
                ->select([
                    'coalesce(iqh.jumlah, 0) as jumlah_iqh'
                ]);
        }
        return $q;
    }

    function add_input_qc($data) {
        $id_levels = [];
        $means = [];
        $sds = [];
		$range1 = [];
		$range2 = [];
        if(array_key_exists("id_level", $data)) {
            $id_levels = $data["id_level"];
            unset($data["id_level"]);
            if(is_array($id_levels) == false) {
                return ["status" => false, "message" => "Field level diperlukan"];
            }
        }
        if(array_key_exists("range1", $data)) {
            $range1 = $data["range1"];
            unset($data["range1"]);
        }
		if(array_key_exists("range2", $data)) {
            $range2 = $data["range2"];
            unset($data["range2"]);
        }
		if(array_key_exists("mean", $data)) {
            $means = $data["mean"];
            unset($data["mean"]);
            if(is_array($means) == false) {
                return ["status" => false, "message" => "Field mean diperlukan"];
            }
        }
        if(array_key_exists("sd", $data)) {
            $sds = $data["sd"];
            unset($data["sd"]);
            if(is_array($sds) == false) {
                return ["status" => false, "message" => "Field SD diperlukan"];
            }
        }
        $countIdLevel = count($id_levels);
        $countMean = count($means);
        $countSd = count($sds);
        if(($countIdLevel == $countMean && $countMean == $countSd) == false) {
            return ["status" => false, "message" => "Data level tidak valid"];
        }
        if($countIdLevel <= 0) {
            return ["status" => false, "message" => "Data level harus diisi"];
        }
        $this->db->insert("qc_input", $data);
        $id = $this->db->insert_id();
        $r = $this->one_input_qc(["where" => ["_.id_qc_input" => $id]]);
       
        if($id == null || $r == null) {
            return ["status" => false, "message" => "Terjadi kesalahan"];
        }

        // if($id == null) {
        //     return ["status" => false, "message" => "Terjadi kesalahan"];
        // }

        /* simpan detail */
        for($i = 0; $i < $countIdLevel; $i++) {
            $this->db->insert("qc_input_detail", [
                "id_qc_input" => $id,
                "id_level" => $id_levels[$i],
                "range1" => floatval($range1[$i]),
				"range2" => floatval($range2[$i]),
				"mean" => floatval($means[$i]),
                "sd" => floatval($sds[$i])
            ]);
        }

        return ["status" => true, "message" => "Success"];
    }

    function one_input_qc($args = []) {
        $this->load->helper("myquery");
        $r = myquery($this->baseQueryInputQC($args), $args)->get()->result_array();
        if(count($r) == 0) {
            return null;
        }
        $r1 = $r[0];
        if(isset($args['join_lot_number']) == true && $args['join_lot_number'] == true) {
            $this->load->model("m_masterqc");
            $r1["lot_number"] = $this->m_masterqc->one_lot_number(["where" => ["_.id_lot_number" => $r1["id_lot_number"]]]);
        }

        // $r1["detail"] = $this->db->from("qc_input_detail qcid")
        //                     ->join('master_qc_level lvl', 'lvl.id_level = qcid.id_level', 'left')
        //                     ->where(["id_qc_input" => $r1["id_qc_input"]])
        //                     ->select([
        //                         'qcid.*',
        //                         'lvl.level_name'
        //                     ])
        //                     ->get()->result_array();
        $r1['detail'] = $this->many_detail_qc_input(['where' => ['_.id_qc_input' => $r1['id_qc_input']]]);
        return $r1;
    }

    function many_input_qc($args = []) {
        // return $this->baseQueryInputQC($args)->where($args["where"])->get()->result_array();
        $this->load->helper("myquery");
        $q = myquery($this->baseQueryInputQC($args), $args);
        // echo $q->get_compiled_select();
        return $q->get()->result_array();
    }

    function update_input_qc($where, $data) {
        $list_id_level = [];
        $list_mean = [];
        $list_sd = [];
		$range1 = [];
		$range2 = [];

        if(array_key_exists("id_level", $data)) {
            $list_id_level = $data["id_level"];
            unset($data["id_level"]);

            if(is_array($list_id_level) == false) {
                $list_id_level = [];
            }
        }
		if(array_key_exists("range1", $data)) {
            $range1 = $data["range1"];
            unset($data["range1"]);
        }
		if(array_key_exists("range2", $data)) {
            $range2 = $data["range2"];
            unset($data["range2"]);
        }
        if(array_key_exists("sd", $data)) {
            $list_sd = $data["sd"];
            unset($data["sd"]);

            if(is_array($list_sd) == false) {
                $list_sd = [];
            }
        }
        if(array_key_exists("mean", $data)) {
            $list_mean = $data["mean"];
            unset($data["mean"]);

            if(is_array($list_mean) == false) {
                $list_mean = [];
            }
        }
        $countIdLevel = count($list_id_level);
        $countMean = count($list_mean);
        $countSd = count($list_sd);
        if(($countIdLevel == $countMean && $countMean == $countSd ) == false){
            return ["status" => false, "message" => "Data level tidak valid"];
        }
        if($countIdLevel <= 0) {
            return ["status" => false, "message" => "Data level harus diisi"];
        }
        $target = $this->one_input_qc(["where" => $where]);
        if($target == null) {
            return ["status" => false, "message" => "Data tidak ditemukan"];
        }
        $this->db->where(["id_qc_input" => $target["id_qc_input"]])->update("qc_input", $data);

        $oldDetail = $this->db->from("qc_input_detail")
                        ->where(["id_qc_input" => $target["id_qc_input"]])
                        ->get()->result_array();
        $keep = [];
        for($i = 0; $i < $countIdLevel; $i++) {
            $exist = false;
            foreach ($oldDetail as $key => $detail) {
                if($detail["id_level"] == $list_id_level[$i]) {
                    $exist = true;
                    $keep[] = $detail["id"];
                    $this->db->where( [ "id" => $detail["id"] ] )->update( "qc_input_detail", [
                        "mean" => floatval($list_mean[$i]),
                        "sd" => floatval($list_sd[$i]),
						"range1" => floatval($range1[$i]),
						"range2" => floatval($range2[$i])
                    ]);
                    break;
                }
            }
            if($exist == false) {
                $this->db->insert("qc_input_detail", [
                    "id_level" => $list_id_level[$i],
					"range1" => floatval($range1[$i]),
					"range2" => floatval($range2[$i]),
                    "mean" => floatval($list_mean[$i]),
                    "sd" => floatval($list_sd[$i]),
                    "id_qc_input" => $target["id_qc_input"]
                ]);
            }
        }
        foreach ($oldDetail as $key => $detail) {
            if(in_array($detail["id"], $keep) == false) {
                $this->db->where(["id" => $detail["id"]])->delete("qc_input_detail");
            }
        }
        return [
            "status" => true,
            "message" => "Success",
            "keep" => $keep,
            "oldDetail" => $oldDetail,
            "level" => $list_id_level,
            "mean" => $list_mean,
            "sd" => $list_sd
        ];
    }

    function delete_input_qc($where) {
        $targets = $this->many_input_qc(["where" => $where]);
        if(count($targets) == 0) {
            return ["status" => false, "message" => "Data tidak ditemukan"];
        }
        foreach ($targets as $key => $target) {
            $this->db->where(["id_qc_input" => $target["id_qc_input"]])->delete("qc_input_detail");
            $this->db->where(["id_qc_input" => $target["id_qc_input"]])->delete("qc_input");
        }
        return ["status" => true, "message" => "Success"];
    }

    function get_available_form_qc_result($id_category, $id_control, $date, $branch ,$alat=null) {
        $qc = $this->db->from('master_control c')
            ->join('master_qc_control_category cat', 'cat.id_control_category = c.id_category', 'left')
            ->join('master_qc_lot_number ln', 'ln.id_control = c.id_control')
            ->join('qc_input qi', 'qi.id_lot_number = ln.id_lot_number')
            ->join('master_test t', 't.test_id = qi.id_test')
            ->where("ln.expired_date >= str_to_date('$date', '%Y-%m-%d')")
			->where("ln.active ='1'")
            ->select([
                'c.*',
                'qi.id_qc_input',
                'qi.id_alat',
                'ln.lot_number',
                't.test_name'
            ]);
        if(empty($alat) == false) {
            $qc->where(['qi.id_alat' => $alat]);
        } 
		if(empty($id_control) == false) {
            $qc->where(['c.id_control' => $id_control]);
        } else {
            $qc->where(['cat.id_control_category' => $id_category]);
        }
        if(empty($branch) == false) {
            $qc->where(['ln.branch_id' => $branch]);
        }
        $qc = $qc->get()->result();
        foreach ($qc as $index => $_qc) {
            $detail = $this->many_detail_qc_input(['where' => ['_.id_qc_input' => $_qc->id_qc_input]]);
            $qc[$index]->detail = $detail;
        }
		
		foreach ($qc as $index => $_qc) {
            $detail_hasil = $this->many_detail_hasil(['where' => ['hasil.id_qc_input' => $_qc->id_qc_input,'hasil.tanggal' => $date]]);
            $qc[$index]->detail_hasil = $detail_hasil;
        }
		

        $lvl = [];
        return [
            'list_qc' => $qc,
            'levels' => $lvl
        ];
    }

    function baseQueryDetailQcInput($args = []) {
        $q = $this->db->from('qc_input_detail _')
            ->join("master_qc_level lvl", "lvl.id_level = _.id_level")
            ->select([
                "_.*",
                "lvl.level_name"
            ]);
        return $q;
    }

    function many_detail_qc_input($args = []){
        $this->load->helper('myquery');
        return myquery($this->baseQueryDetailQcInput($args), $args)->get()->result();
    }

    /* end input qc */


    /* start hasil*/

    function dt_hasil($args = []) {
        $sql = $this->baseQueryHasil($args)->where($args['where'])->get_compiled_select();
        $this->load->helper("dt");
        return getDataTable([
            "sql" => $sql,
            'default_order' => [
                'tanggal' => 'desc',
                'input_qc_text' => 'asc',
                'branch_name' => 'asc'
            ],

            'custom_filter' => isset($args['filter_where'])?$args['filter_where']:[],
            'rest_paging' => isset($args['rest_paging'])?$args['rest_paging']:[]
            // 'disable_paging' => (isset($args['disable_paging'])&&$args['disable_paging'])?1:0
        ]);
    }

	function dt_hasil_mobile($args = []){
        $this->load->helper('myquery');
        // var_dump($this->baseQueryHasil($args)->where($args['where'])->get_compiled_select());
        return $this->baseQueryHasil($args)->where($args['where'])->order_by('tanggal','desc')->get()->result();
    }
    function baseQueryHasil($args = []) {
        if($args == null) {
            $args = [];
        }
        if(isset($args["join_detail"]) == false) {
            $args["join_detail"] = true;
        }
        if(isset($args['join_input_qc']) == false) {
            $args['join_input_qc'] = true;
        }

        $joinDetail = "";
        if($args["join_detail"] == true) {
            $q = $this->db->from("qc_hasil_detail qhd")
                    ->join("master_qc_level lvl", "lvl.id_level = qhd.id_level", "left")

                    ->select([
                        "qhd.id_hasil",
                        "GROUP_CONCAT(qhd.id_level order by lvl.level_name, qhd.id SEPARATOR '-----') as list_id_level",
                        "GROUP_CONCAT(lvl.level_name order by lvl.level_name, qhd.id SEPARATOR '-----') as list_level_name",
                        "GROUP_CONCAT(qhd.hasil order by lvl.level_name, qhd.id SEPARATOR '-----') as list_hasil"
                    ])
                    ->group_by("qhd.id_hasil");
            $joinDetail = $q->get_compiled_select();
        }
        $joinInputQc = '';
        if($args['join_input_qc'] == true) {
            $q = $this->db->from('qc_input_detail _')
                ->join("master_qc_level lvl", "lvl.id_level = _.id_level", "left")
                ->select("GROUP_CONCAT(_.id_level order by lvl.level_name SEPARATOR '-----') as list_id_level")
                ->select("GROUP_CONCAT(_.mean order by lvl.level_name SEPARATOR '-----') as list_mean")
                ->select("GROUP_CONCAT(_.sd order by lvl.level_name SEPARATOR '-----') as list_sd")
                ->select("GROUP_CONCAT(lvl.level_name order by lvl.level_name SEPARATOR '-----') as list_level_name")
                ->select(["_.id_qc_input"])
                ->group_by('_.id_qc_input');
            $joinInputQc = $q->get_compiled_select();
        }
        $q = $this->db->from("qc_hasil _")
            ->join("users usr", "usr.id_user = _.creator_id", "left")
            ->join("qc_input qci", "qci.id_qc_input = _.id_qc_input", "left")
            ->join("master_test mtest", "mtest.test_id = qci.id_test", "left")
            ->join('master_test_group gtest', 'gtest.group_id = mtest.group_id', 'left')
            ->join("master_qc_lot_number ln", "ln.id_lot_number = qci.id_lot_number", "left")
            ->join("master_control mc", "mc.id_control = ln.id_control", "left")
            ->join("branch b1", "b1.branch_id = _.branch_id", "left")
            ->join("master_qc_alat Alat", "Alat.id_alat = qci.id_alat","left")
            ->join("branch b2", "b2.branch_id = Alat.branch_id", "left")
            ->select([
                "_.*",
                "usr.name as creator_name",
                "usr.username as creator_username",
                "mtest.test_name as test_name",
                "ln.lot_number as lot_number",
                "mc.control_name as control_name",
                "CONCAT(qci.id_qc_input, ' (test: ', mtest.test_name, ', lot: ', ln.lot_number, ')') as input_qc_text",
                "b1.branch_name",
                'gtest.group_name as group_test_name',
                'Alat.alat_name as list_alat',
                'Alat.branch_id as cabang_alat',
                'b2.branch_name as cabang_alat_name'
            ]);
        if($args["join_detail"] == true) {
            $q->join("($joinDetail) dtl", "dtl.id_hasil = _.id_hasil", "left")
                ->select([
                    "dtl.list_id_level",
                    "dtl.list_level_name",
                    "dtl.list_hasil",
                ]);
        }
        if($args['join_input_qc'] == true) {
            $q->join("($joinInputQc) iqc", 'iqc.id_qc_input = _.id_qc_input', 'left')
                ->select([
                    'iqc.list_id_level as list_id_level_input',
                    'iqc.list_sd',
                    'iqc.list_mean',
                    'iqc.list_level_name as list_level_name_input',
                ]);
        }
        return $q;

    }
	
	function dt_hasil_export($args = []) {
        $sql = $this->baseQueryHasil_export($args)->where($args['where'])->get_compiled_select();
        $this->load->helper("dt");
        return getDataTable([
            "sql" => $sql,
            'default_order' => [
                'id_alat' => 'asc',
                'id_test' => 'asc',
                //'qh.tanggal' => 'asc'
            ],

            'custom_filter' => isset($args['filter_where'])?$args['filter_where']:[],
            'rest_paging' => isset($args['rest_paging'])?$args['rest_paging']:[],
            'disable_paging' => 1
        ]);
    }
	function baseQueryHasil_export($args = []) {
        if($args == null) {
            $args = [];
        }
        if(isset($args["join_detail"]) == false) {
            //$args["join_detail"] = true;
        }
        if(isset($args['join_input_qc']) == false) {
            //$args['join_input_qc'] = true;
        }

        $joinDetail = "";
        if($args["join_detail"] == true) {
            $q = $this->db->from("qc_hasil_detail qhd")
                    ->join("master_qc_level lvl", "lvl.id_level = qhd.id_level", "left")

                    ->select([
                        "qhd.id_hasil",
                        "GROUP_CONCAT(qhd.id_level order by lvl.level_name, qhd.id SEPARATOR '-----') as list_id_level",
                        "GROUP_CONCAT(lvl.level_name order by lvl.level_name, qhd.id SEPARATOR '-----') as list_level_name",
                        "GROUP_CONCAT(qhd.hasil order by lvl.level_name, qhd.id SEPARATOR '-----') as list_hasil"
                    ])
                    ->group_by("qhd.id_hasil");
            $joinDetail = $q->get_compiled_select();
        }
        $joinInputQc = '';
        if($args['join_input_qc'] == true) {
            $q = $this->db->from('qc_input_detail _')
                ->join("master_qc_level lvl", "lvl.id_level = _.id_level", "left")
                ->select("GROUP_CONCAT(_.id_level order by lvl.level_name SEPARATOR '-----') as list_id_level")
                ->select("GROUP_CONCAT(_.mean order by lvl.level_name SEPARATOR '-----') as list_mean")
                ->select("GROUP_CONCAT(_.sd order by lvl.level_name SEPARATOR '-----') as list_sd")
                ->select("GROUP_CONCAT(lvl.level_name order by lvl.level_name SEPARATOR '-----') as list_level_name")
                ->select(["_.id_qc_input"])
                ->group_by('_.id_qc_input');
            $joinInputQc = $q->get_compiled_select();
        }
        $q = $this->db->from("qc_input _")
            ->join("qc_hasil qh", "_.id_qc_input = qh.id_qc_input", "left")
            ->join("qc_hasil_detail qhd", "qhd.id_hasil = qh.id_hasil", "left")
            ->join("master_test mtest", "mtest.test_id = _.id_test", "left")
            ->select([
                //"_.*",
                "id_alat",
                "id_test",
                "mtest.test_name as test_name",
				"(select max(id_level) from qc_hasil_detail where id_hasil=qhd.id_hasil) as max_level",
                "qh.tanggal",
                "id_level",
                "qhd.hasil",
            ]);
        if($args["join_detail"] == true) {
            $q->join("($joinDetail) dtl", "dtl.id_hasil = _.id_hasil", "left")
                ->select([
                    "dtl.list_id_level",
                    "dtl.list_level_name",
                    "dtl.list_hasil",
                ]);
        }
        if($args['join_input_qc'] == true) {
            $q->join("($joinInputQc) iqc", 'iqc.id_qc_input = _.id_qc_input', 'left')
                ->select([
                    'iqc.list_id_level as list_id_level_input',
                    'iqc.list_sd',
                    'iqc.list_mean',
                    'iqc.list_level_name as list_level_name_input',
                ]);
        }
        return $q;

    }
	
	
    // function add_hasil($data) {
    //     $list_id_level = [];
    //     $list_hasil = [];

    //     if(array_key_exists("id_level", $data)) {
    //         $list_id_level = $data["id_level"];
    //         unset($data["id_level"]);

    //         if(is_array($list_id_level) == false) {
    //             $list_id_level = [];
    //         }
    //     }
    //     if(array_key_exists("hasil", $data)) {
    //         $list_hasil = $data["hasil"];
    //         unset($data["hasil"]);

    //         if(is_array($list_hasil) == false) {
    //             $list_hasil = [];
    //         }
    //     }

    //     $countIdLevel = count($list_id_level);
    //     $countHasil = count($list_hasil);
    //     if(($countIdLevel == $countHasil) == false){
    //         return ["status" => false, "message" => "Data hasil tidak valid"];
    //     }
    //     if($countIdLevel <= 0) {
    //         return ["status" => false, "message" => "Data hasil harus diisi", 'msg' => 'count id level not valid'];
    //     }
    //     if(isset($data['id_qc_input']) == false) {
    //         return ['status' => false, 'message' => 'Input QC harus diisi'];
    //     }
    //     $qcInput = $this->one_input_qc(['where' => ['_.id_qc_input' => $data['id_qc_input']]]);
    //     if($qcInput == null) {
    //         return ['status' => false, 'message' => 'Data Input QC tidak dapat ditemukan'];
    //     }
    //     // print_r($qcInput);
    //     // exit();
    //     $inputDetails = isset($qcInput['detail']) ? $qcInput['detail'] : [];
    //     if(count($inputDetails) != $countHasil) {
    //         return ['status' => false, 'message' => 'Data yg dikirim tidak sesuai dengan detail input qc'];
    //     }
    //     $this->db->insert("qc_hasil", $data);
    //     $id = $this->db->insert_id();
    //     $r = $this->one_hasil(["where" => ["_.id_hasil" => $id]]);
    //     if($id == null || $r == null) {
    //         return ["status" => false, "message" => "Terjadi kesalahan"];
    //     }
    //     $anyOutOfMaxSD = false;
    //     for($i = 0; $i < $countIdLevel; $i++) {
    //         $hasil = floatval($list_hasil[$i]);
    //         $id_level = $list_id_level[$i];

    //         // $existInQcInput = false;
    //         // foreach ($inputDetails as $key => $iDetail) {
    //         //     if($iDetail->id_level == $id_level) {
    //         //         $existInQcInput = true;
    //         //         $sd = floatval($iDetail->sd);
    //         //         if($sd > 0) {
    //         //             $h = ($hasil - floatval($iDetail->mean) ) / $sd;

    //         //             if($h > 3 || $h < -3) {
    //         //                 $anyOutOfMaxSD = true;
    //         //             }
    //         //         }
    //         //         break;
    //         //     }
    //         // }

    //         $this->db->insert("qc_hasil_detail", [
    //             "hasil" => floatval($list_hasil[$i]),
    //             "id_hasil" => $id,
    //             "id_level" => $id_level
    //         ]);
    //     }

    //     $notif = null;

    //     return ["status" => true, "message" => "Success", "id" => $id, "data" => $r, 'notif' => $notif];
    // }

    function notifyIfOut($id_hasil,$id_qc_input) {
        $month = date('n');
		$year = date('Y');
		$hasil = $this->one_hasil(['where' => ['_.id_hasil' => $id_hasil]]);
        if($hasil == null) {
            return ['status' => false, 'message' => 'hasil not found'];
        }
        $input_qc = $this->one_input_qc(['where' => ['_.id_qc_input' => $hasil['id_qc_input']]]);
        if($input_qc == null) {
            return ['status' => false, 'message' => 'input not found'];
        }
        $detailHasil = $hasil['detail'];
        $detailInput = $input_qc['detail'];
        $hasOut = false;

        foreach ($detailHasil as $key => $dh) {
            foreach ($detailInput as $key => $di) {
                if($di->id_level == $dh['id_level']) {
                    $mean = floatval($di->mean);
                    $sd = floatval($di->sd);
                    $result = floatval($dh['hasil']);

                    if($sd > 0 || $sd < 0) {
                        $r = ($result - $mean) / $sd;
                        if($r > 2 || $r < -2) {
                            $hasOut = true;
                            break;
                        }
                    }
                }
            }
            if($hasOut == true) {
                break;
            }
        }
//$hasOut = true;

		$dateinput = date("d-M-y", strtotime($hasil['tanggal']));
		$datenow = date("d-M-y");
		
        if($hasOut == true && $dateinput == $datenow) {
            $listIdReceiver = [];
			$userReceivers = $this->db->from("v_user usr")
                                ->where_in('usr.id_user', [30,62,70,58,71,23])
                                ->select(['usr.id_user'])
                                ->get()->result();
			foreach ($userReceivers as $key => $user) {
                $listIdReceiver[] = $user->id_user;
            }



            $creator = $this->db->from('users')->where(['id_user' => $hasil['creator_id']])->get()->row();

			$this->load->model('restapi/user_model');
			$userReceivers = $this->user_model->many_user([
                'where_in' => ['_.id_position' => "4"],
                'where' => [
                    '_.STATUS !=' => 'KELUAR'
                ]
            ]);
			$userReceivers = json_decode(json_encode($userReceivers));
            foreach ($userReceivers as $key => $user) {
                if (strpos($user->many_branch, "'".$hasil['branch_id']."'") !== false)
					$listIdReceiver[] = $user->id_user;
            }
			$userReceivers = $this->user_model->many_user([
                'where_in' => ['_.branch_id' => $hasil['branch_id'],'_.id_position' => [3,5,6,7]],
                'where' => [
                    '_.STATUS !=' => 'KELUAR'
                ]
            ]);
			$userReceivers = json_decode(json_encode($userReceivers));
            foreach ($userReceivers as $key => $user) {
                $listIdReceiver[] = $user->id_user;
            }
				
			$sender;	
            $this->load->model('restapi/user_model');
            if($this->session->userdata('id_user')){
                $sender = $this->user_model->one_user(['where' => ['_.id_user' => $this->session->userdata('id_user')]]);
            }else{
                $sender = $this->user_model->one_user(['where' => ['_.id_user' => $hasil['creator_id']]]);
            }
            
            $sender_name = '';
            $position_name = '';
            $branch_name = '';
            if($sender != null) {
                $sender_name = $sender['name'];
                $position_name = $sender['position_name'];
                $branch_name = $sender['branch_name'];
            }


            // $hasil['id_qc_input']
             // stay
        // buat fungsi mengetahui detail pada $hasil['id_qc_input']

        $detail_qc = $this->db->from('qc_input')->where(['id_qc_input' => $hasil['id_qc_input']])->get()->row();
        $detail_test = $this->db->from('master_test')->where(['test_id' => $detail_qc->id_test])->get()->row();
        // var_dump($detail_test->test_name);

			$this->load->library('Notify_lib');
			$nl = new Notify_lib();
			$notif = $nl->send(
			  'QC Out',
			  //$creator->name . ' (@' . $creator->username . ') has submit new Out QC Result',
			  //   $sender_name . ' - '.$position_name.' ('.$branch_name.') has submit new Out QC Result '.$dateinput,
              '('.$branch_name.') has submit new Out QC Result '.' - '. $detail_test->test_name . ' - '.$dateinput,
              
              $creator->id_user,
			  $listIdReceiver,
			  ['id_hasil' => $hasil['id_hasil'], 'id' => $hasil['id_qc_input'], 'id_qc_input' => $hasil['id_qc_input']],
			  'qc_result',
			  true
			);
				
          return ['status' => true, 'message' => 'out', 'notif' => $notif];
       } else {
            return ['status' => true, 'message' => 'not out'];
        }
    }

    function one_hasil($args = []) {
        $this->load->helper("myquery");
        if(is_array($args) == false) {
            $args = [];
        }
        $args['join_detail'] = false;
        $r1 = myquery($this->baseQueryHasil($args), $args)->get()->row_array();
        if($r1 == null) {
            return $r1;
        }
        // $r1["detail"] = $this->db->from("qc_hasil_detail d")
        //     ->join('master_qc_level lvl', 'lvl.id_level = d.id_level', 'left')
        //     ->where(["id_hasil" => $r1["id_hasil"]])
        //     ->select([ 'd.*', 'lvl.level_name' ])
        //     ->get()->result_array();
        $r1['detail'] = $this->many_detail_hasil([
            'where' => ['_.id_hasil' => $r1['id_hasil']],
            'order' => [
                ['lvl.level_name', 'asc'],
                ['_.id', 'asc']
            ]
        ]);
        $r1['qc_input'] = $this->one_input_qc(['where' => ['_.id_qc_input' => $r1['id_qc_input']]]);
        return $r1;
    }

    function hasil($args = []) {

    }

    function detail_from_test($args = []) {
        $r = $this->dt_hasil_mobile($args);

        if(count($r) == 0) {
            return null;
        }
        $r1 = $r[0];
        return $this->m_qc->one_input_qc(['join_lot_number'=>true, 'where' => ['_.id_qc_input' => $r1->id_qc_input]]);

    }

    function many_hasil($args = []) {
        $this->load->helper("myquery");
        $res = myquery($this->baseQueryHasil($args), $args)->get()->result_array();

        if(isset($args['join_detail_v2']) == true && $args['join_detail_v2'] == true) {
            foreach ($res as $key => $r) {
                $r['detail'] = $this->many_detail_hasil([
                    'where' => [
                        '_.id_hasil' => $r['id_hasil']
                    ],
                    'order' => [
                        ['lvl.level_name'],
                        ['_.id']
                    ]
                ]);
                $r['qc_input'] = $this->one_input_qc([
                    'where' => [
                        '_.id_qc_input' => $r['id_qc_input']
                    ],
                    'join_detail' => false,
                    'join_lot_number' => isset($args['join_lot_number']) ? $args['join_lot_number'] : false
                ]);
                $res[$key] = $r;
            }
        }
        return $res;
    }

    function delete_hasil($where) {
        $targets = $this->many_hasil(["where" => $where]);
        if(count($targets) == 0) {
            return ["status" => false, "message" => "Data tidak ditemukan"];
        }
        foreach ($targets as $key => $t) {
            $this->db->where(["id_hasil" => $t["id_hasil"]])->delete("qc_hasil_detail");
            $this->db->where(["id_hasil" => $t["id_hasil"]])->delete("qc_hasil");
        }
        return ["status" => true, "message" => "Success"];
    }

    function update_many_hasil_v3($data) {
        if($data == null || is_array($data) == false) {
            return ['status' => false, 'message' => 'Argumen fungsi update tidak valid', 'data' => $data];
        }
        if(isset($data['tanggal']) == false) {
            return ['status' => false, 'message' => 'field tanggal diperlukan'];
        }
        if(isset($data['data']) == false) {
            return ['status' => false, 'message' => 'field data diperlukan'];
        }
        $newData = $data['data'];
        $tanggal = $data['tanggal'];
        $tanggals = explode('-', $tanggal);
        if(count($tanggals) < 3) {
            return ['status' => false, 'message' => 'format tanggal tidak valid'];
        }
        $oldHasils = $this->many_hasil([
            'where' => [
                'year(_.tanggal)' => $tanggals[0],
                'month(_.tanggal)' => $tanggals[1],
                'day(_.tanggal)' => $tanggals[2]
            ],
            'join_detail' => false,
            'join_input_qc' => false,
            'join_detail_v2' => true
        ]);

        $rr = [];
        foreach ($newData as $key => $_data) {
            $oldHasil = null;
            /* mencari hasil yg sudah ada untuk diupdate,
            jika tidak ditemukan, tambahkan hasil baru */
            foreach ($oldHasils as $key2 => $oh) {
                if($oh['id_qc_input'] == $_data['qc']) {
                    $oldHasil = $oh;
                    $rr[] = $oldHasil;
                    break;
                }
            }

            $_detail = $this->as_new_detail_hasil($_data['res']);

            /* hasil lama tidak ditemukan, buat hasil baru */
            if($oldHasil == null) {
                //print_r('NEW <br />');
				$rr[] = $_data;
                $r = $this->add_hasil_v3([
                    'tanggal' => $tanggal,
                    'creator_id' => $data['id_user'],
                    'branch_id' => $data['branch_id'],
                    'id_qc_input' => $_data['qc'],
                    'detail' => $_detail
                ]);
            }
            /* hasil lama ditemukan, update data */
            else {
				//print_r('OLD <br />');
                $rr[] = $this->update_hasil_v3([
                    'id_hasil' => $oldHasil['id_hasil'],
                    'tanggal' => $tanggal,
					
					'creator_id' => $data['id_user'],
                    'branch_id' => $data['branch_id'],
					'id_qc_input' => $_data['qc'],

                    'detail' => $_detail
                ]);
                $this->notifyIfOut($oldHasil['id_hasil'],null);
            }
        }
        return [
            'status' => true,
            'message' => 'Data berhasil diperbarui',
            // 'old_hasil' => $oldHasils,
            // 'rr' => $rr
        ];
    }

    function as_new_detail_hasil($data) {
        $result = [];
        if($data == null || is_array($data) == false) {
            return $result;
        }
        foreach ($data as $key => $d1) {
            $r = [];
            if(is_array($d1)) {
                if(count($d1) > 1) {
                    $r['id_level'] = $d1[0];
                    $r['hasil'] = floatval($d1[1]);
                }
                if(count($d1) > 2) {
                    $r['comment'] = $d1[2];
                }
            }
            $result[] = $r;
        }
        return $result;
    }
 
    function simulate_qc($data) {
        if($data == null || is_array($data) == false) {
            return ['status' => false, 'message' => 'Argumen fungsi update tidak valid', 'data' => $data];
        }
        if(isset($data['tanggal']) == false) {
            return ['status' => false, 'message' => 'field tanggal diperlukan'];
        }
        if(isset($data['data']) == false) {
            return ['status' => false, 'message' => 'field data diperlukan'];
        }
        $newData = $data['data'];
        $tanggal = $data['tanggal'];
        $tanggals = explode('-', $tanggal);
        if(count($tanggals) < 3) {
            return ['status' => false, 'message' => 'format tanggal tidak valid'];
        }
        $oldHasils = $this->many_hasil([
            'where' => [
                'year(_.tanggal)' => $tanggals[0],
                'month(_.tanggal)' => $tanggals[1],
                'day(_.tanggal)' => $tanggals[2]
            ],
            'join_detail' => false,
            'join_input_qc' => false,
            'join_detail_v2' => true
        ]);

        $rr = [];
		$r = [];
        foreach ($newData as $key => $_data) {
            $oldHasil = null;
            /* mencari hasil yg sudah ada untuk diupdate,
            jika tidak ditemukan, tambahkan hasil baru */
            foreach ($oldHasils as $key2 => $oh) {
                if($oh['id_qc_input'] == $_data['qc']) {
                    $oldHasil = $oh;
                    $rr[] = $oldHasil;
                    break;
                }
            }

            $_detail = $this->as_new_detail_hasil($_data['res']);

            /* hasil lama tidak ditemukan, buat hasil baru */
            //if($oldHasil == null) {
                //print_r('NEW <br />');
				/*$rr[] = $_data;
                $r = $this->add_hasil_v3([
                    'tanggal' => $tanggal,
                    'creator_id' => $data['id_user'],
                    'branch_id' => $data['branch_id'],
                    'id_qc_input' => $_data['qc'],
                    'detail' => $_detail
                ]);*/
				
				$rr[] = $_data;
                $r[] = $this->is_out([
                    'tanggal' => $tanggal,
                    'creator_id' => $data['id_user'],
                    'branch_id' => $data['branch_id'],
                    'id_qc_input' => $_data['qc'],
                    'detail' => $_detail
                ]);
				
            //}
            /* hasil lama ditemukan, update data */
            //else {
				//print_r('OLD <br />');
                /*$rr[] = $this->update_hasil_v3([
                    'id_hasil' => $oldHasil['id_hasil'],
                    'tanggal' => $tanggal,
					
					'creator_id' => $data['id_user'],
                    'branch_id' => $data['branch_id'],
					'id_qc_input' => $_data['qc'],

                    'detail' => $_detail
                ]);
                $this->notifyIfOut($oldHasil['id_hasil']);*/
            //}
        }
        return [
            'status' => true,
            'message' => 'Data berhasil diperbarui',
            // 'old_hasil' => $oldHasils,
            'data' => $r
        ];
    }
   

    // function update_hasil($where, $data) {
    //     $list_level = [];
    //     $list_hasil = [];

    //     if(array_key_exists("id_level", $data)) {
    //         $list_level = $data["id_level"];
    //         unset($data["id_level"]);

    //         if(is_array($list_level) == false) {
    //             $list_level = [];
    //         }
    //     }
    //     if(array_key_exists("hasil", $data)) {
    //         $list_hasil = $data["hasil"];
    //         unset($data["hasil"]);

    //         if(is_array($list_hasil) == false) {
    //             $list_hasil = [];
    //         }
    //     }

    //     $countLevel = count($list_level);
    //     $countHasil = count($list_hasil);

    //     if(($countLevel == $countHasil) == false) {
    //         return ["status" => false, "message" => "Hasil tidak valid"];
    //     }
    //     if($countLevel <= 0) {
    //         return ["status" => false, "message" => "Hasil harap diisi"];
    //     }

    //     $target = $this->one_hasil(["where" => $where]);
    //     if($target == null) {
    //         return ["status" => false, "message" => "Data tidak ditemukan"];
    //     }
    //     $this->db->where(["id_hasil" => $target["id_hasil"]])->update("qc_hasil", $data);

    //     $keep = [];
    //     $oldDetail = $this->db->from("qc_hasil_detail qhd")
    //                     ->where(["id_hasil" => $target["id_hasil"]])
    //                     ->get()->result_array();
    //     for($i = 0; $i < $countLevel; $i++) {
    //         $exist = false;
    //         foreach ($oldDetail as $key => $dtl) {
    //             if($dtl["id_level"] == $list_level[$i]) {
    //                 $exist = true;
    //                 $keep[] = $dtl["id"];
    //                 $this->db->where(["id" => $dtl["id"]])->update("qc_hasil_detail", [
    //                     "hasil" => floatval($list_hasil[$i])
    //                 ]);
    //                 break;
    //             }
    //         }

    //         if($exist == false) {
    //             $this->db->insert("qc_hasil_detail", [
    //                 "id_level" => $list_level[$i],
    //                 "id_hasil" => $target["id_hasil"],
    //                 "hasil" => floatval($list_hasil[$i])
    //             ]);
    //         }
    //     }
    //     foreach ($oldDetail as $key => $dtl) {
    //         if(in_array($dtl["id"], $keep) == false) {
    //             $this->db->where(["id" => $dtl["id"]])->delete("qc_hasil_detail");
    //         }
    //     }
    //     return ["status" => true, "message" => "Success"];
    // }

    // function add_many_qc_hasil($data) {
    //     if(empty($data['tanggal'])) {
    //         return ['status' => false, 'message' => 'field tanggal diperlukan'];
    //     }
    //     // if(empty($data['id_control'])) {
    //     //     return ['status' => false, 'message' => 'field id control diperlukan'];
    //     // }
    //     $tanggal = $data['tanggal'];
    //     $id_control = $data['id_control'];
    //     $list_hasil = empty($data['list_hasil']) ? [] : $data['list_hasil'];
    //     $list_id_detail_input_qc = empty($data['list_id_detail_input_qc']) ? [] : $data['list_id_detail_input_qc'];
    //     $list_id_input_qc = empty($data['list_id_input_qc']) ? [] : $data['list_id_input_qc'];
    //     $list_include_qc = empty($data['list_include_qc']) ? [] : $data['list_include_qc'];

    //     if(is_array($list_include_qc) == false) {
    //         $list_include_qc = [];
    //     }
    //     if(is_array($list_id_input_qc) == false) {
    //         $list_id_input_qc = [];
    //     }
    //     /* hanya qc yg dicentang yg disimpan, default 1
    //     yg sekarang semua di flag untuk disimpan */
    //     $temp = [];
    //     foreach ($list_include_qc as $index => $include_qc) {
    //         if($include_qc == 1) {
    //             if(count($list_id_input_qc) > $index) {
    //                 $temp[] = $list_id_input_qc[$index];
    //             }
    //         }
    //     }
    //     $list_id_input_qc = $temp;

    //     /* get detail input qc untuk masing-masing qc */
    //     $list_qc_input = [];
    //     foreach ($list_id_input_qc as $key => $id_input_qc) {
    //         $input_qc = $this->one_input_qc(['where' => ['_.id_qc_input' => $id_input_qc]]);
    //         $list_qc_input[] = $input_qc;
    //     }

    //     /* membuat data dengan format 1 qc dapat berisi hasil untuk banyak level */
    //     $saveCounter = 0;
    //     foreach ($list_qc_input as $key => $qc_input) {
    //         $detail = $qc_input['detail'];
    //         $list_id_level = [];
    //         $list_hasil_2 = [];

    //         /* matching dengan detail input qc */
    //         $counterDetail = 0;
    //         $counterHasil = 0;
    //         foreach ($detail as $key => $_d) {
    //             $counterDetail++;
    //             foreach ($list_id_detail_input_qc as $key10 => $id_detail_input_qc) {
    //                 /* hasil[] tidak diisi */
    //                 if(empty($list_hasil[$key10])){
    //                     continue;
    //                 }
    //                 if($id_detail_input_qc == $_d->id) {
    //                     $counterHasil++;
    //                     $list_id_level[] = $_d->id_level;
    //                     $list_hasil_2[] = $list_hasil[$key10];
    //                     break;
    //                 }
    //             }
    //         }

    //         /* jika jumlah hasil yang disubmit tidak sama dengan jumlah level yg didefine dalam input qc, maka batal disimpan */
    //         if($counterHasil != $counterDetail) {
    //             continue;
    //         }
    //         $d1 = [
    //             'id_level' => $list_id_level,
    //             'hasil' => $list_hasil_2,
    //             'id_qc_input' => $qc_input['id_qc_input'],
    //             'creator_id' => $data['creator_id'],
    //             'tanggal' => $data['tanggal'],
    //             'branch_id' => $data['branch_id']
    //         ];
    //         $r = $this->add_hasil($d1);
    //         if($r['status'] == false) {
    //             $r['d1'] = $d1;
    //             return $r;
    //         }
    //         $saveCounter++;
    //     }

    //     if($saveCounter == 0) {
    //         return ['status' => false, 'message' => 'Tidak ada data tersimpan'];
    //     }
    //     // exit();
    //     return ['status' => true, 'message' => 'ok', 'data' => $data, 'qc' => $list_qc_input];
    // }

    function add_many_hasil_v3($args) {
        if(empty($args)) {
            return ['status' => false, 'message' => 'tidak ada data diterima'];
        }
        if(empty($args->id_user)) {
            return ['status' => false, 'message' => 'id user diperlukan'];
        }
        if(empty($args->branch_id)) {
            return ['status' => false, 'message' => 'branch id diperlukan'];
        }
        if(empty($args->data)) {
            return ['status' => false, 'message' => 'harap mengisi setidaknya 1 form'];
        }
        if(empty($args->tanggal)) {
            return ['status' => false, 'message' => 'tanggal diperlukan'];
        }
        $qcs = $args->data;
        $counterSave = 0;
        foreach ($qcs as $key => $_qc) {
            $idQc = $_qc->qc;
            $res = $_qc->res;
            // $qc = $this->one_input_qc(['where' => ['_.id_qc_input' => $idQc]]);
            // if($qc == null) {
            //     continue;
            // }
            // $detailQc = $qc['detail'];
            // $result = [];
            // foreach ($res as $key => $r1) {
            //     // print_r($r1);
            //     $idDetail = $r1->d;
            //     $val = $r1->v;
            //     if(empty($val)){
            //         continue;
            //     }
            //     $detail = null;
            //     foreach ($detailQc as $key => $d) {
            //         if($d->id == $idDetail) {
            //             $detail = $d;
            //             break;
            //         }
            //     }
            //     if($detail == null) {
            //         continue;
            //     }
            //     $result[] = [
            //         'id_level' => $detail->id_level,
            //         'hasil' => $val
            //     ];
            // }

            $r = $this->add_hasil_v3([
                'tanggal' => $args->tanggal,
                'creator_id' => $args->id_user,
                'branch_id' => $args->branch_id,
                'id_qc_input' => $idQc,
                'detail' => $this->as_new_detail_hasil($res)
            ]);
            if($r['status'] == true) {
                $counterSave++;
            }
        }
        if($counterSave == 0) {
            return ['status' => false, 'message' => 'tidak ada data disimpan'];
        }
        if($counterSave == count($qcs)) {
            return ['status' => true, 'message' => 'Semua data berhasil disimpan'];
        }

        return ['status' => true, 'message' => 'beberapa data tidak disimpan'];
    }

    function add_hasil_v3($data) {
        if(empty($data['tanggal'])) {
            return ['status' => false, 'message' => 'tanggal diperlukan'];
        }
        if(empty($data['creator_id'])) {
            return ['status' => false, 'message' => 'creator id diperlukan'];
        }
        if(empty($data['branch_id'])) {
            return ['status' => false, 'message' => 'branch id diperlukan'];
        }
        if(empty($data['id_qc_input'])) {
            return ['status' => false, 'message' => 'id qc input diperlukan'];
        }
        if(empty($data['detail'])) {
            return ['status' => false, 'message' => 'detail diperlukan'];
        }

        $detail = $data['detail'];
        unset($data['detail']);

        if(is_string($detail)){
            $detail = json_decode($detail);
        }

        if(empty($detail)) {
            return ['status' => false, 'message' => 'detail kosong'];
        }

        $qc = $this->one_input_qc(['where' => ['_.id_qc_input' => $data['id_qc_input']]]);
        if($qc == null) {
            return ['status' => false, 'message' => 'qc tidak dapat ditemukan'];
        }

        $detailqc = $qc['detail'];
        $counterDetail = 0;
        $listDetail = [];
        foreach ($detail as $key => $d) {
            // if(isset($d['id_level']) == false || isset($d['hasil']) == false) {
            //     continue;
            // }
            // if(empty($d['hasil'])) {
            //     continue;
            // }
            $exist = false;
            foreach ($detailqc as $key => $d2) {
                if($d['id_level'] == $d2->id_level) {
                    $exist = true;
                }
            }
            if($exist) {
                $counterDetail++;
                $listDetail[] = [
                    'id_level' => $d['id_level'],
                    'hasil' => floatval($d['hasil']),
                    'comment' => empty($d['comment']) ? '' : $d['comment']
                ];
            }
        }
        if($counterDetail == 0) {
            return ['status' => false, 'message' => 'data tidak disimpan, data hasil tidak cocok dengan detail qc'];
        }

        $this->db->insert('qc_hasil', $data);
        $id = $this->db->insert_id();
	
		if($id == null) {
            return ['status' => false, 'message' => 'terjadi kesalahan saat menyimpan ke dalam database'];
        }

        foreach ($listDetail as $key => $detail) {
            $detail['id_hasil'] = $id;
            $this->db->insert('qc_hasil_detail', $detail);
        }
		
		$is_reanalyze = $this->db->from("qc_hasil_detail")
                                ->where_in('id_hasil', [$id])
								->group_by('id_level')
								->having('COUNT(*) > 1')
                                ->select(['id_level'])
                                ->get()->num_rows();
		
		//print_r($is_reanalyze.'__'.$id.'<br />');
		
        //if ($this->db->get_where('qc_hasil', ['id_qc_input' => $data['id_qc_input'], 'tanggal' => $data['tanggal']])->num_rows() > 1) {
        if ($is_reanalyze > 0) {
			$listIdReceiver = [];
			$userReceivers = $this->db->from("v_user usr")
                                ->where_in('usr.id_user', [30,62,70,58,71,23])
                                ->select(['usr.id_user'])
                                ->get()->result();
			foreach ($userReceivers as $key => $user) {
                $listIdReceiver[] = $user->id_user;
            }



			$creator = $this->db->from('users')->where(['id_user' => $data['creator_id']])->get()->row();

			$this->load->model('restapi/user_model');
			$userReceivers = $this->user_model->many_user([
                'where_in' => ['_.id_position' => "4"],
                'where' => [
                    '_.STATUS !=' => 'KELUAR'
                ]
            ]);
			$userReceivers = json_decode(json_encode($userReceivers));
            foreach ($userReceivers as $key => $user) {
                if (strpos($user->many_branch, "'".$data['branch_id']."'") !== false)
					$listIdReceiver[] = $user->id_user;
            }
			$userReceivers = $this->user_model->many_user([
                'where_in' => ['_.branch_id' => $data['branch_id'],'_.id_position' => [3,5,6,7]],
                'where' => [
                    '_.STATUS !=' => 'KELUAR'
                ]
            ]);
			$userReceivers = json_decode(json_encode($userReceivers));
            foreach ($userReceivers as $key => $user) {
                $listIdReceiver[] = $user->id_user;
            }


            $detail_qc = $this->db->from('qc_input')->where(['id_qc_input' => $data['id_qc_input']])->get()->row();
            $detail_test = $this->db->from('master_test')->where(['test_id' => $detail_qc->id_test])->get()->row();
			
			$dateinput = date("d-M-y", strtotime($data['tanggal']));
			$datenow = date("d-M-y");
			if($dateinput == $datenow){
                $sender;	
                $this->load->model('restapi/user_model');
                if($this->session->userdata('id_user')){
                    $sender = $this->user_model->one_user(['where' => ['_.id_user' => $this->session->userdata('id_user')]]);
                }else{
                    $sender = $this->user_model->one_user(['where' => ['_.id_user' => $data['creator_id']]]);
                }
                
                $sender_name = '';
                $position_name = '';
                $branch_name = '';
                if($sender != null) {
                    $sender_name = $sender['name'];
                    $position_name = $sender['position_name'];
                    $branch_name = $sender['branch_name'];
                }

				$this->load->library('Notify_lib');
				$nl = new Notify_lib();
				$notif = $nl->send(
						'Reanalyze QC Result',
						//$creator->name . ' (@' . $creator->username . ') Reanalyze QC Result',
						// $sender_name . ' - '.$position_name.' ('.$branch_name.') Reanalyze QC Result '.$dateinput,
						'('.$branch_name.') Reanalyze QC Result '.' - '. $detail_test->test_name . ' - '.$dateinput,
                        $creator->id_user,
						$listIdReceiver,
						['id' => $id, 'id_qc_input' => $data['id_qc_input']],
						'qc_base_result',
						true
				  );
			}
        }

        

        $this->notifyIfOut($id,null);

        return ['status' => true, 'message' => 'Data berhasil disimpan'];
    }
	
	function update_hasil_v3($data) {
        if(isset($data['id_hasil']) == false) {
            return ['status' => false, 'message' => 'field id hasil diperlukan'];
        }
        if(isset($data['tanggal']) == false) {
            return ['status' => false, 'message' => 'field tanggal diperlukan'];
        }
        if(isset($data['detail']) == false) {
            return ['status' => false, 'message' => 'field detail diperlukan'];
        }
        $detail = $data['detail'];
        if(is_array($detail) == false) {
            $detail = json_decode($detail);
        }
        if(is_array($detail) == false) {
            return ['status' => false, 'message' => 'format detail tidak valid'];
        }
        $target = $this->one_hasil(['where' => ['_.id_hasil' => $data['id_hasil']]]);
        if($target == null) {
            return ['status' => false, 'message' => 'data tidak ditemukan'];
        }
        // $this->db->where(['id_hasil' => $target['id_hasil']])->update('qc_hasil', [
        //     'tanggal' => $data['tanggal']
        // ]);
        // print_r($target);
        $qc = $target['qc_input'];
        $detailQc = $qc['detail'];
        $detailHasil = $target['detail'];
		
		$temp_rea_send = [];
		
        foreach ($detail as $key => $d) {
            // if(empty($d['id_level']) || empty($d['hasil'])) {
            //     continue;
            // }

            if(empty($d['id_level'])) {
                continue;
            }

            $dqc = null;
            foreach ($detailQc as $key2 => $d2) {
                if($d2->id_level == $d['id_level']) {
                    $dqc = $d2;
                    break;
                }
            }
            /* detail hasil yg baru tidak cocok dengan detail qc input */
            if($dqc == null) {
                continue;
            }

            $dhasil = null;
            foreach ($detailHasil as $key2 => $d2) {
                if($d2['id_level'] == $d['id_level']) {
                    $dhasil = $d2;
                    unset($detailHasil[$key2]);
                    break;
                }
            }

            /* detail hasil lama tidak ditemukan, tambahkan yg baru */
            if($dhasil == null) {
                
				
				$is_reanalyze = $this->db->from("qc_hasil_detail")
                                ->where_in('id_hasil', [$target['id_hasil']])
								->where_in('id_level', [$d['id_level']])
								//->having('COUNT(*) > 1')
                                ->select(['id_level'])
                                ->get()->num_rows();
				
				//print_r($is_reanalyze.'_isrea_<br />');
				
				$t = 0;
				foreach($detail as $f => $g){
					if($g['id_level']==$d['id_level']) $t++;
				}
				
				//print_r($t.'_ISREA_<br />');
				
				if ($is_reanalyze!=0 && $is_reanalyze != $t && !in_array($target['id_hasil'], $temp_rea_send)) {
					$temp_rea_send[] = $target['id_hasil'];
				}
				
				
				$this->db->insert('qc_hasil_detail', [
                    'id_hasil' => $target['id_hasil'],
                    'id_level' => $d['id_level'],
                    'hasil' => floatval($d['hasil']),
                    'comment' => empty($d['comment']) ? '' : $d['comment']
                ]);
                continue;
            }

            /* detail hasil lama ditemukan, update data yg lama */
            $this->db->where([
                'id' => $dhasil['id']
            ])->update('qc_hasil_detail', [
                'hasil' => floatval($d['hasil']),
                'comment' => empty($d['comment']) ? '' : $d['comment']
            ]);
        }
		
		foreach($temp_rea_send as $q){
			$listIdReceiver = [];
			$userReceivers = $this->db->from("v_user usr")
								// ->where_in('usr.id_user', [30,62,70,58,71,23])
                                ->where_in('usr.id_user', [30,62,70,58,23])
								->select(['usr.id_user'])
								->get()->result();
			foreach ($userReceivers as $key => $user) {
				$listIdReceiver[] = $user->id_user;
			}



			$creator = $this->db->from('users')->where(['id_user' => $data['creator_id']])->get()->row();

			$this->load->model('restapi/user_model');
			$userReceivers = $this->user_model->many_user([
                'where_in' => ['_.id_position' => "4"],
                'where' => [
                    '_.STATUS !=' => 'KELUAR'
                ]
            ]);
			$userReceivers = json_decode(json_encode($userReceivers));
			foreach ($userReceivers as $key => $user) {
				if (strpos($user->many_branch, "'".$data['branch_id']."'") !== false)
					$listIdReceiver[] = $user->id_user;
			}
			$userReceivers = $this->user_model->many_user([
                'where_in' => ['_.branch_id' => $data['branch_id'],'_.id_position' => [3,5,6,7]],
                'where' => [
                    '_.STATUS !=' => 'KELUAR'
                ]
            ]);
			$userReceivers = json_decode(json_encode($userReceivers));
			foreach ($userReceivers as $key => $user) {
				$listIdReceiver[] = $user->id_user;
			}

            $detail_qc = $this->db->from('qc_input')->where(['id_qc_input' => $data['id_qc_input']])->get()->row();
            $detail_test = $this->db->from('master_test')->where(['test_id' => $detail_qc->id_test])->get()->row();

			
			$dateinput = date("d-M-y", strtotime($target['tanggal']));
			$datenow = date("d-M-y");
			if($dateinput == $datenow){
			

                $sender;
                $this->load->model('restapi/user_model');
                if($this->session->userdata('id_user')){
                    $sender = $this->user_model->one_user(['where' => ['_.id_user' => $this->session->userdata('id_user')]]);
                }else{
                    $sender = $this->user_model->one_user(['where' => ['_.id_user' => $data['creator_id']]]);
                }
				
				$sender_name = '';
				$position_name = '';
				$branch_name = '';
				if($sender != null) {
					$sender_name = $sender['name'];
					$position_name = $sender['position_name'];
					$branch_name = $sender['branch_name'];
				}

				$this->load->library('Notify_lib');
				$nl = new Notify_lib();
				$notif = $nl->send(
					'Reanalyze QC Result',
					//$creator->name . ' (@' . $creator->username . ') Reanalyze QC Result',
					// $sender_name . ' - '.$position_name.' ('.$branch_name.') Reanalyze QC Result '.$dateinput,
					'('.$branch_name.') Reanalyze QC Result '.' - '. $detail_test->test_name . ' - '.$dateinput,
                    $creator->id_user,
					$listIdReceiver,
					['id' => $q, 'id_qc_input' => $data['id_qc_input']],
					'qc_base_result',
					true
				);		
			}			
		}
		
        return ['status' => true, 'message' => 'Data berhasil diupdate'];
    }
	
    // function add_qc_hasil_v2($args) {
    //     if(empty($args)) {
    //         return ['status' => false, 'message' => 'data is null'];
    //     }
    //     if(empty($args->id_user)) {
    //         return ['status' => false, 'message' => 'id user diperlukan'];
    //     }
    //     if(empty($args->branch_id)) {
    //         return ['status' => false, 'message' => 'branch id diperlukan'];
    //     }
    //     if(empty($args->data)) {
    //         return ['status' => false, 'message' => 'tidak ada data diterima'];
    //     }
    //     if(empty($args->tanggal)) {
    //         return ['status' => false, 'message' => 'tanggal diperlukan'];
    //     }
    //     $qcs = $args->data;
    //     $counterQc = 0;
    //     $listqc = [];
    //     $counterSave = 0;
    //     $listError = [];
    //     foreach ($qcs as $key => $_qc) {
    //         if(empty($listqc[$_qc->qc])){
    //             $qc = $this->one_input_qc(['where' => ['_.id_qc_input' => $_qc->qc]]);
    //             if($qc == null) {
    //                 continue;
    //             }
    //             $listqc[$qc['id_qc_input']] = $qc;
    //         }
    //         $counterQc++;
    //         $qc = $listqc[$_qc->qc];
    //         $hasil_from_input = $_qc->hasil;
    //         $hasil = [];
    //         $id_level = [];
    //         $detailQc = $qc['detail'];
    //         foreach ($detailQc as $key => $dqc) {
    //             foreach ($hasil_from_input as $key => $hinput) {
    //                 /* jika user tidak mengisi field hasil */
    //                 if(empty($hinput->val)) {
    //                     continue;
    //                 }
    //                 if($hinput->detail == $dqc->id) {
    //                     $id_level[] = $dqc->id_level;
    //                     $hasil[] = $hinput->val;
    //                     break;
    //                 }
    //             }
    //         }
    //         $r = $this->add_hasil([
    //             'tanggal' => $args->tanggal,
    //             'creator_id' => $args->id_user,
    //             'branch_id' => $args->branch_id,
    //             'id_qc_input' => $qc['id_qc_input'],
    //             'id_level' => $id_level,
    //             'hasil' => $hasil
    //         ]);
    //         if($r['status'] == false) {
    //             $r['iq_qc'] = $qc['id_qc_input'];
    //             $listError[] = $r;
    //         } else {
    //             $counterSave++;
    //         }
    //     }
    //     if($counterSave == 0) {
    //         return [
    //             'status' => false,
    //             'message' => 'Tidak ada data disimpan, pastikan semua field terisi',
    //             'error' => $listError
    //         ];
    //     }
    //     if($counterSave < $counterQc) {
    //         return [
    //             'status' => true,
    //             'message' => 'Data berhasil disimpan, namun ada beberapa data tidak valid',
    //             'error' => $listError
    //         ];
    //     }
    //     return [
    //         'status' => true,
    //         'message' => '',
    //         'qcs' => $listqc,
    //         // 'error' => $listError
    //     ];
    // }

    /* end hasil*/

    /* start detail hasil */

    function baseQueryDetailHasil($args = []) {
        $r = $this->db->from("qc_hasil_detail _")
            ->join("qc_hasil hasil", "hasil.id_hasil = _.id_hasil", "left")
            ->join('master_qc_level lvl', 'lvl.id_level = _.id_level', 'left')
            ->select([
                "_.*",
                "hasil.tanggal",
                'lvl.level_name'
            ]);
        return $r;
    }

    function many_detail_hasil($args = []) {
        $this->load->helper("myquery");
        return myquery($this->baseQueryDetailHasil($args), $args)->get()->result_array();
    }
    function one_detail_hasil($args = []) {
        $this->load->helper("myquery");
        return myquery($this->baseQueryDetailHasil($args), $args)->get()->row_array();
    }
    /* start detail hasil */

    /* start chart */
    function get_data_chart($id_qc_input, $id_hasil) {
        $result = [
            "qc_input" => null,
            "list_hasil" => []
        ];
        // var_dump($id_qc_input);
        // var_dump($id_hasil);
        // exit();
        if(empty($id_qc_input)) {
            $hasil = $this->one_hasil(["where" => ["_.id_hasil" => $id_hasil]]);
            // print_r($hasil);
            if($hasil != null) {
                $id_qc_input = $hasil["id_qc_input"];
            }
        }

        $qc_input = $this->one_input_qc([
            "where" => ["_.id_qc_input" => $id_qc_input],
            'join_lot_number' => true
        ]);
        $result["qc_input"] = $qc_input;

        $list_hasil = [];
        $list_level_input = [];
        $list_chart = [];
        $input_mean = [];
        $input_sd = [];
        $list_label = [];
        if($qc_input != null) {
            $lot_number = null;
            if(isset($qc_input["lot_number"])) {
                $lot_number = $qc_input["lot_number"];
            }
            $control = null;
            if($lot_number != null) {
                if(isset($lot_number["control"])) {
                    $control = $lot_number["control"];
                }
            }
            $group_level = null;
            if($control != null) {
                if(isset($control["group_level"])) {
                    $group_level = $control["group_level"];
                }
            }
            if($group_level != null) {
                if(isset($group_level["list_member"])) {
                    $list_level_input = $group_level["list_member"];
                }
            }

            foreach ($list_level_input as $key => $lvl) {
                $list_chart [ $lvl["id_level"] ] = [];
                $input_mean [ $lvl["id_level"] ] = 0;
                $input_sd   [ $lvl["id_level"] ] = 0;
                $list_label [ $lvl["id_level"] ] = [];
            }

            if(isset($qc_input["detail"])) {
                $details = $qc_input["detail"];
                foreach ($details as $key => $detail) {
                    $id_level = $detail->id_level;
                    if(isset($input_mean[$id_level])) {
                        $input_mean[$id_level] = floatval($detail->mean);
                    }
                    if(isset($input_sd[$id_level])) {
                        $input_sd[$id_level] = floatval($detail->sd);
                    }
                }
            }

            $list_detail_hasil = $this->many_detail_hasil([
                "where" => [
                    "hasil.id_qc_input" => $qc_input["id_qc_input"]
                ],
                "order" => [ ["hasil.tanggal", "asc"], ['hasil.id_hasil', 'asc'], ['_.id', 'asc'] ]
            ]);
            $result["list_detail_hasil"] = $list_detail_hasil;

            foreach ($list_detail_hasil as $key => $dh) {
                $id_level = $dh["id_level"];
                if( isset( $list_chart[ $id_level ] ) ) {
                    $list_label[$id_level] [] = $dh["tanggal"];
                    $sd = floatval( $input_sd[ $id_level ] );
                    $hasil = 0;
                    if ( $sd > 0 ) {
                        $hasil = ( floatval( $dh[ "hasil" ] ) - floatval( $input_mean[ $id_level ] ) ) / $sd ;
                    }
                    $hasil = floatval( number_format( $hasil, 5, '.', '') );
                    $list_chart[ $id_level ] [] = $hasil;
                }
            }
        }

        $result["charts"] = $list_chart;
        $result["levels"] = $list_level_input;
        $result["labels"] = $list_label;

        return $result;
    }
    /* end chart */

    /* start chart */
    function get_data_chart_komparasi($id_qc_input, $id_hasil, $start_date, $end_date) {
        $result = [
            "qc_input" => null,
            "list_hasil" => []
        ];
        // var_dump($id_qc_input);
        // var_dump($id_hasil);
        // exit();
        if(empty($id_qc_input)) {
            $hasil = $this->one_hasil(["where" => ["_.id_hasil" => $id_hasil]]);
            // print_r($hasil);
            if($hasil != null) {
                $id_qc_input = $hasil["id_qc_input"];
            }
        }

        $qc_input = $this->one_input_qc([
            "where" => ["_.id_qc_input" => $id_qc_input],
            'join_lot_number' => true
        ]);
        $result["qc_input"] = $qc_input;

        $list_hasil = [];
        $list_level_input = [];
        $list_chart = [];
        $input_mean = [];
        $input_sd = [];
        $list_label = [];
        if($qc_input != null) {
            $lot_number = null;
            if(isset($qc_input["lot_number"])) {
                $lot_number = $qc_input["lot_number"];
            }
            $control = null;
            if($lot_number != null) {
                if(isset($lot_number["control"])) {
                    $control = $lot_number["control"];
                }
            }
            $group_level = null;
            if($control != null) {
                if(isset($control["group_level"])) {
                    $group_level = $control["group_level"];
                }
            }
            if($group_level != null) {
                if(isset($group_level["list_member"])) {
                    $list_level_input = $group_level["list_member"];
                }
            }

            foreach ($list_level_input as $key => $lvl) {
                $list_chart [ $lvl["id_level"] ] = [];
                $input_mean [ $lvl["id_level"] ] = 0;
                $input_sd   [ $lvl["id_level"] ] = 0;
                $list_label [ $lvl["id_level"] ] = [];
            }

            if(isset($qc_input["detail"])) {
                $details = $qc_input["detail"];
                foreach ($details as $key => $detail) {
                    $id_level = $detail->id_level;
                    if(isset($input_mean[$id_level])) {
                        $input_mean[$id_level] = floatval($detail->mean);
                    }
                    if(isset($input_sd[$id_level])) {
                        $input_sd[$id_level] = floatval($detail->sd);
                    }
                }
            }

            $list_detail_hasil = $this->many_detail_hasil([
                "where" => [
                    "hasil.id_qc_input" => $qc_input["id_qc_input"],
                    "hasil.tanggal >=" => $start_date,
                    "hasil.tanggal <=" => $end_date,
                ],
                "order" => [ ["hasil.tanggal", "asc"], ['hasil.id_hasil', 'asc'], ['_.id', 'asc'] ]
            ]);
            $result["list_detail_hasil"] = $list_detail_hasil;

            // echo "<pre>",print_r($list_detail_hasil),"</pre>";

            foreach ($list_detail_hasil as $key => $dh) {
                $id_level = $dh["id_level"];
                if( isset( $list_chart[ $id_level ] ) ) {
                    $list_label[$id_level] [] = $dh["tanggal"];
                    $sd = floatval( $input_sd[ $id_level ] );
                    $hasil = 0;
                    if ( $sd > 0 ) {
                        $hasil = ( floatval( $dh[ "hasil" ] ) - floatval( $input_mean[ $id_level ] ) ) / $sd ;
                    }
                    $hasil = floatval( number_format( $hasil, 5, '.', '') );
                    $list_chart[ $id_level ] [] = $hasil;
                }
            }
        }

        $result["charts"] = $list_chart;
        $result["levels"] = $list_level_input;
        $result["labels"] = $list_label;

        return $result;
    }
    /* end chart */

	 /* start chart */
    function get_data_chart_export($id_qc_input, $id_hasil, $month, $year) {
        $result = [
            "qc_input" => null,
            "list_hasil" => []
        ];
        // var_dump($id_qc_input);
        // var_dump($id_hasil);
        // exit();
        if(empty($id_qc_input)) {
            $hasil = $this->one_hasil(["where" => ["_.id_hasil" => $id_hasil]]);
            // print_r($hasil);
            if($hasil != null) {
                $id_qc_input = $hasil["id_qc_input"];
            }
        }

        $qc_input = $this->one_input_qc([
            "where" => ["_.id_qc_input" => $id_qc_input],
            'join_lot_number' => true
        ]);
        $result["qc_input"] = $qc_input;

        $list_hasil = [];
        $list_level_input = [];
        $list_chart = [];
        $input_mean = [];
        $input_sd = [];
        $list_label = [];
        if($qc_input != null) {
            $lot_number = null;
            if(isset($qc_input["lot_number"])) {
                $lot_number = $qc_input["lot_number"];
            }
            $control = null;
            if($lot_number != null) {
                if(isset($lot_number["control"])) {
                    $control = $lot_number["control"];
                }
            }
            $group_level = null;
            if($control != null) {
                if(isset($control["group_level"])) {
                    $group_level = $control["group_level"];
                }
            }
            if($group_level != null) {
                if(isset($group_level["list_member"])) {
                    $list_level_input = $group_level["list_member"];
                }
            }

            foreach ($list_level_input as $key => $lvl) {
                $list_chart [ $lvl["id_level"] ] = [];
                $input_mean [ $lvl["id_level"] ] = 0;
                $input_sd   [ $lvl["id_level"] ] = 0;
                $list_label [ $lvl["id_level"] ] = [];
            }

            if(isset($qc_input["detail"])) {
                $details = $qc_input["detail"];
                foreach ($details as $key => $detail) {
                    $id_level = $detail->id_level;
                    if(isset($input_mean[$id_level])) {
                        $input_mean[$id_level] = floatval($detail->mean);
                    }
                    if(isset($input_sd[$id_level])) {
                        $input_sd[$id_level] = floatval($detail->sd);
                    }
                }
            }

            $list_detail_hasil = $this->many_detail_hasil([
                "where" => [
                    "hasil.id_qc_input" => $qc_input["id_qc_input"],
					"month(hasil.tanggal)" => $month,
                    "year(hasil.tanggal)" => $year,
                ],
                "order" => [ ["hasil.tanggal", "asc"], ['hasil.id_hasil', 'asc'], ['_.id', 'asc'] ]
            ]);
            $result["list_detail_hasil"] = $list_detail_hasil;

            foreach ($list_detail_hasil as $key => $dh) {
                $id_level = $dh["id_level"];
                if( isset( $list_chart[ $id_level ] ) ) {
                    $list_label[$id_level] [] = $dh["tanggal"];
                    $sd = floatval( $input_sd[ $id_level ] );
                    $hasil = 0;
                    if ( $sd > 0 ) {
                        $hasil = ( floatval( $dh[ "hasil" ] ) - floatval( $input_mean[ $id_level ] ) ) / $sd ;
                    }
                    $hasil = floatval( number_format( $hasil, 5, '.', '') );
                    $list_chart[ $id_level ] [] = $hasil;
                }
            }
        }

        $result["charts"] = $list_chart;
        $result["levels"] = $list_level_input;
        $result["labels"] = $list_label;

        return $result;
    }
    /* end chart */
	
    public function getDataAlat($cabang) {

        // SQL Syntax WHERE
        // WHERE b1.branch_id = {int} OR b1.branch_id = {int}
        $where = '';
        for ($i=0; $i < count($cabang); $i++) {
            if ($i == 0){
                $where = "b1.branch_id = ".$cabang[$i];
            } else {
                $where = $where." OR b1.branch_id = ".$cabang[$i];
            }
        }

        $q = $this->db->from("qc_hasil _")
            ->join("qc_input qci", "qci.id_qc_input = _.id_qc_input", "left")
            ->join("master_test mtest", "mtest.test_id = qci.id_test", "left")
            ->join("branch b1", "b1.branch_id = _.branch_id", "left")
            ->join("master_qc_alat Alat", "Alat.id_alat = qci.id_alat")
            ->select('distinct(alat.id_alat), alat.alat_name')
            ->where($where)
            ->order_by('alat.alat_name', 'ASC');

        $query = $q->get_compiled_select();

        return $this->db->query($query);

        /*
        SELECT DISTINCT Alat.id_alat, Alat.alat_name
        FROM 'qc_hasil' '_'
                LEFT JOIN 'qc_input' AS 'qci' ON 'qci'.'id_qc_input' = '_'.'id_qc_input'
                LEFT JOIN 'master_test' AS 'mtest' ON 'mtest'.'test_id' = 'qci'.'id_test'
                LEFT JOIN 'branch' AS 'b1' ON 'b1'.'branch_id' = '_'.'branch_id'
                LEFT JOIN 'master_qc_alat' AS 'Alat' ON 'Alat'.'id_alat' = 'qci'.'id_alat'
        WHERE
        b1.branch_id = 2 OR b1.branch_id = 15
        ORDER BY Alat.alat_name ASC;
        */


        // $this->db->select('distinct(master_qc_alat.id_alat), master_qc_alat.alat_name');
        // $this->db->from('qc_hasil');
        // $this->db->join('qc_input', 'qc_hasil.id_qc_input = qc_input.id_qc_input');
        // $this->db->join('master_qc_alat', 'master_qc_alat.id_alat = qc_input.id_alat');
        // $this->db->where_in('qc_hasil.branch_id', $cabang);
        // return $this->db->get();
    }

	public function getDataTanggal($cabang, $alat)
	{
		$this->db->select('distinct(qc_hasil.tanggal)');
		$this->db->from('qc_hasil');
		$this->db->join('qc_input', 'qc_hasil.id_qc_input = qc_input.id_qc_input');
		$this->db->where_in('qc_input.branch_id', $cabang);
		$this->db->where_in('qc_input.id_alat', $alat);
		$this->db->order_by(1, 'ASC');
		return $this->db->get();
	}

    public function getDataTest($cabang, $alat)
    {
        // SQL Syntax WHERE
        // WHERE Alat.id_alat = {INT} AND (b1.branch_id = {int} OR b1.branch_id = {int})
        $where = 'Alat.id_alat = '.$alat.' AND ';
        $countcabang = count($cabang);
        for ($i=0; $i < $countcabang; $i++) {
            if ($i == 0){
                $where = $where."(b1.branch_id = ".$cabang[$i];
            } else {
                $where = $where." OR b1.branch_id = ".$cabang[$i];
            }

            if ($i == ($countcabang-1)) {
                $where = $where.")";
            }
        }
        // echo $where;

        $q = $this->db->from("qc_hasil _")
            ->join("qc_input qci", "qci.id_qc_input = _.id_qc_input", "left")
            ->join("master_test mtest", "mtest.test_id = qci.id_test")
            ->join("branch b1", "b1.branch_id = _.branch_id", "left")
            ->join("master_qc_alat Alat", "Alat.id_alat = qci.id_alat", "left")
            ->select('distinct(mtest.test_id), mtest.test_name')
            ->where($where)
            ->order_by('mtest.test_name', 'ASC');

        $query = $q->get_compiled_select();
        return $this->db->query($query);


        // $this->db->select('distinct(master_test.test_id), master_test.test_name');
        // $this->db->from('qc_hasil');
        // $this->db->join('qc_input', 'qc_hasil.id_qc_input = qc_input.id_qc_input');
        // $this->db->join('master_test', 'master_test.test_id = qc_input.id_test');
        // $this->db->where_in('qc_input.branch_id', $cabang);
        // $this->db->where_in('qc_input.id_alat', $alat);
        // $this->db->where("qc_hasil.tanggal >=", $format_tgl_awal);
        // $this->db->where("qc_hasil.tanggal <=", $format_tgl_akhir);
        // $this->db->order_by(1, 'ASC');
        // return $this->db->get();
    }

	public function getTestByAlat($alat)
	{
		$this->db->select('distinct(master_test.test_id) as id, master_test.test_name as text');
		$this->db->from('qc_hasil');
		$this->db->join('qc_input', 'qc_hasil.id_qc_input = qc_input.id_qc_input');
		$this->db->join('master_test', 'master_test.test_id = qc_input.id_test');
		$this->db->where_in('qc_input.id_alat', $alat);
		$this->db->order_by(1, 'ASC');
		return $this->db->get();
	}

    public function getKomparasiItem($cabang, $alat, $tanggal, $test)
    {
        $start_date  = date_format(date_create($tanggal[0]), "Y-m-d");
        $end_date    = date_format(date_create($tanggal[1]), "Y-m-d");

        $q = $this->db->from('qc_input_detail _')
            ->join("master_qc_level lvl", "lvl.id_level = _.id_level", "left")
            ->select("GROUP_CONCAT(_.id_level order by lvl.level_name SEPARATOR '-----') as list_id_level")
            ->select("GROUP_CONCAT(_.mean order by lvl.level_name SEPARATOR '-----') as list_mean")
            ->select("GROUP_CONCAT(_.sd order by lvl.level_name SEPARATOR '-----') as list_sd")
            ->select("GROUP_CONCAT(lvl.level_name order by lvl.level_name SEPARATOR '-----') as list_level_name")
            ->select(["_.id_qc_input"])
            ->group_by('_.id_qc_input');
        $joinInputQc = $q->get_compiled_select();

        $q = $this->db->from("qc_hasil_detail qhd")
            ->join("master_qc_level lvl", "lvl.id_level = qhd.id_level", "left")

            ->select([
                "qhd.id_hasil",
                "GROUP_CONCAT(qhd.id_level order by lvl.level_name, qhd.id SEPARATOR '-----') as list_id_level",
                "GROUP_CONCAT(lvl.level_name order by lvl.level_name, qhd.id SEPARATOR '-----') as list_level_name",
                "GROUP_CONCAT(qhd.hasil order by lvl.level_name, qhd.id SEPARATOR '-----') as list_hasil"
            ])
            ->group_by("qhd.id_hasil");
        $joinDetail = $q->get_compiled_select();


        $q = $this->db->from("qc_hasil _")
            ->join("users usr", "usr.id_user = _.creator_id", "left")
            ->join("qc_input qci", "qci.id_qc_input = _.id_qc_input", "left")
            ->join("master_test mtest", "mtest.test_id = qci.id_test", "left")
            ->join('master_test_group gtest', 'gtest.group_id = mtest.group_id', 'left')
            ->join("master_qc_lot_number ln", "ln.id_lot_number = qci.id_lot_number", "left")
            ->join("branch b1", "b1.branch_id = _.branch_id", "left")
            ->join("master_qc_alat Alat", "Alat.id_alat = qci.id_alat","left")
            ->select([
                "_.*",
                "usr.name as creator_name",
                "usr.username as creator_username",
                "mtest.test_name as test_name",
                "ln.lot_number as lot_number",
                "CONCAT(qci.id_qc_input, ' (test: ', mtest.test_name, ', lot: ', ln.lot_number, ')') as input_qc_text",
                "b1.branch_name",
                'gtest.group_name as group_test_name',
                'Alat.alat_name as list_alat'
            ]);

        $q->join("($joinDetail) dtl", "dtl.id_hasil = _.id_hasil", "left")
            ->select([
                "dtl.list_id_level",
                "dtl.list_level_name",
                "dtl.list_hasil",
            ]);

        $q->join("($joinInputQc) iqc", 'iqc.id_qc_input = _.id_qc_input', 'left')
            ->select([
                'iqc.list_id_level as list_id_level_input',
                'iqc.list_sd',
                'iqc.list_mean',
                'iqc.list_level_name as list_level_name_input',
            ]);


        $args['where']['Alat.id_alat'] = $alat;
        $args['where']['b1.branch_id'] = $cabang;
        $args['where']['_.tanggal >='] = $start_date;
        $args['where']['_.tanggal <='] = $end_date;

        $query = $q->where($args['where'])->get_compiled_select();

        return $this->db->query($query);

        // $q = $this->db->from('qc_input_detail _')
        // ->join("master_qc_level lvl", "lvl.id_level = _.id_level", "left")
        // ->select("GROUP_CONCAT(_.id_level order by lvl.level_name SEPARATOR '-----') as list_id_level")
        // ->select("GROUP_CONCAT(_.mean order by lvl.level_name SEPARATOR '-----') as list_mean")
        // ->select("GROUP_CONCAT(_.sd order by lvl.level_name SEPARATOR '-----') as list_sd")
        // ->select("GROUP_CONCAT(lvl.level_name order by lvl.level_name SEPARATOR '-----') as list_level_name")
        // ->select(["_.id_qc_input"])
        // ->group_by('_.id_qc_input');

        // $joinInputQc = $q->get_compiled_select();

        // $q->join("($joinInputQc) iqc", 'iqc.id_qc_input = _.id_qc_input', 'left')
        // ->select([
        //     'iqc.list_id_level as list_id_level_input',
        //     'iqc.list_sd',
        //     'iqc.list_mean',
        //     'iqc.list_level_name as list_level_name_input',
        // ]);

        // return $joinHasilQc."<p>".$joinInputQc;

        // $this->db->select('qc_hasil.tanggal, qc_input.branch_id, branch.branch_name, AVG(qc_input_detail.sd) as sd');
        // $this->db->from('qc_input');
        // $this->db->join('qc_hasil', 'qc_hasil.id_qc_input = qc_input.id_qc_input');
        // $this->db->join('qc_input_detail', 'qc_input_detail.id_qc_input =  qc_input.id_qc_input');
        // $this->db->join('branch', 'branch.branch_id =  qc_input.branch_id');
        // $this->db->where_in('qc_input.branch_id', $cabang);
        // $this->db->where_in('qc_input.id_alat', $alat);
        // $this->db->where("qc_hasil.tanggal >=", $format_tgl_awal);
        // $this->db->where("qc_hasil.tanggal <=", $format_tgl_akhir);
        // $this->db->where('qc_input.id_test', $test);
        // $this->db->group_by([1,2]);
        // $this->db->order_by(1, 'ASC');
        // return $this->db->get();
    }



	function many_urinalisa_qc($args = []) {
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
		if(isset($args["order"]) == false) {
            $args["order"] = [];
        }
        return $this->baseQueryUrinalisaQc($args)->where($args["where"])->order_by('gtest.id_unit','asc')->get()->result_array();
    }
	
	function baseQueryUrinalisaQc($args = []) {
        return $this->db->from("qc_urinalisa _")
            ->join("users usr", "usr.id_user = _.creator_id", "left")
            ->join("branch b1", "b1.branch_id = _.branch_id", "left")
			
			->join("qc_urinalisa_detail b", "b.id_urinalisa = _.id_urinalisa", "left")
			
            ->join("master_unit gtest", "gtest.id_unit = b.id_unit", "left")
            ->select([
                "_.*",
                "usr.name as creator_name",
                "gtest.unit_name as unit_name",
                "b1.branch_name",
				"b.id as id_detail",
				"b.tanggal",
				"b.id_unit",
				"b.berat_jenis",
				"b.id_urin",
            ]);
    }

    function pdf_urin($bulan,$tahun,$cabang,$level,$lot,$alat)
    {
        $urin = $this->db->query("select u.* from qc_urinalisa u where u.bulan = '$bulan' and u.tahun = '$tahun' and u.branch_id = '$cabang' and u.id_level = '$level' and u.id_lot_number = '$lot' and u.id_alat = '$alat' ")->row();

        $urin->details = $this->db->query("select u.*,ud.*,mu.unit_name,b.branch_id,u.id_urinalisa as urinalisa_id from qc_urinalisa u left join qc_urinalisa_detail ud on ud.id_urinalisa = u.id_urinalisa left join master_unit mu on mu.id_unit = ud.id_unit left join branch b on b.branch_id = u.branch_id where u.bulan = '$bulan' and u.tahun = '$tahun' and u.branch_id = '$cabang' and u.id_level = '$level' and u.id_lot_number = '$lot' and u.id_alat = '$alat' group by ud.tanggal ")->result();

        $urin->alat = $this->db->query("select u.*,malat.alat_name from qc_urinalisa u left join master_qc_alat malat on malat.id_alat = u.id_alat ")->row();

        $urin->cabang = $this->db->query("select u.*,b.branch_name from qc_urinalisa u left join branch b on b.branch_id = u.branch_id ")->row();

        $urin->lot = $this->db->query("select u.*,lot.lot_number from qc_urinalisa u left join master_qc_lot_number lot on lot.id_lot_number = u.id_lot_number where u.id_lot_number =  '$lot' ")->row();

        $urin->unit = $this->db->query("select u.*,ud.*,mu.unit_name,b.branch_id from qc_urinalisa u left join qc_urinalisa_detail ud on ud.id_urinalisa = u.id_urinalisa left join master_unit mu on mu.id_unit = ud.id_unit left join branch b on b.branch_id = u.branch_id where u.bulan = '$bulan' and u.tahun = '$tahun' and u.branch_id = '$cabang' and u.id_level = '$level' and u.id_lot_number = '$lot' and u.id_alat = '$alat' group by ud.id_unit")->result();

        foreach ($urin->details as $key) {
            # code...
       
         $urin->master = $this->db->query("select m.* , d.*,red.is_red as redd  from master_qc_urinalisa m left join qc_urinalisa_detail d on d.id_unit = m.id_unit left join qc_urinalisa_is_red red on red.id_urin = d.id_urin where d.id_unit = d.id_unit and d.tanggal = '$key->tanggal' and d.id_urinalisa = '$key->urinalisa_id' ")->result();

          }


        return $urin;
    }
	
	function as_new_detail_urinalisa($tgl, $brt, $data) {
        $result = [];
        if($data == null || is_array($data) == false) {
            return $result;
        }
        foreach ($data as $key => $d1) {
            $r = [];
            if(is_array($d1)) {
                if(count($d1) > 1) {
					$tmp = $d1;
					
                    $r['tanggal'] = $tgl;
					$r['berat_jenis'] = $brt;
					$r['id_urin'] = floatval($tmp[1]);
					$r['id_unit'] = $tmp[0];
                }
            }
            $result[] = $r;
        }
        return $result;
    }
		
    function add_urinalisa($data) {
        
        if(empty($data['detail'])) {
            return ['status' => false, 'message' => 'detail diperlukan'];
        }

        $detail = $data['detail'];
        unset($data['detail']);

        if(is_string($detail)){
            $detail = json_decode($detail);
        }

        if(empty($detail)) {
            return ['status' => false, 'message' => 'detail kosong'];
        }

        /*$qc = $this->one_input_qc(['where' => ['_.id_qc_input' => $data['id_qc_input']]]);
        if($qc == null) {
            return ['status' => false, 'message' => 'qc tidak dapat ditemukan'];
        }

        $detailqc = $qc['detail'];        
		$counterDetail = 0;*/
        $listDetail = [];
        foreach ($detail as $key => $d) {
			
            /*if(isset($d['id_level']) == false || isset($d['hasil']) == false) {
                continue;
            }
            if(empty($d['hasil'])) {
                continue;
            }
            $exist = false;
            foreach ($detailqc as $key => $d2) {
                if($d['id_level'] == $d2->id_level) {
                    $exist = true;
                }
            }
            if($exist) {
                $counterDetail++;*/
                $listDetail[] = [
                    'tanggal' => floatval($d['tanggal']),
                    'berat_jenis' => floatval($d['berat_jenis']),
                    'id_urin' => floatval($d['id_urin']),
					'id_unit' => floatval($d['id_unit']),
                ];
            //}
        }
        /*if($counterDetail == 0) {
            return ['status' => false, 'message' => 'data tidak disimpan, data hasil tidak cocok dengan detail qc'];
        }*/
		
		

        foreach ($listDetail as $key => $detail) {
            $detail['id_urinalisa'] = $data['id_urinalisa'];
            $this->db->insert('qc_urinalisa_detail', $detail);
        }

        

        return ['status' => true, 'message' => 'Data berhasil disimpan'];
    }
	
    function update_urinalisa($data) {
        if(isset($data['id_urinalisa']) == false) {
            return ['status' => false, 'message' => 'field id hasil diperlukan'];
        }
        

        if(isset($data['detail']) == false) {
            return ['status' => false, 'message' => 'field detail diperlukan'];
        }
        $detail = $data['detail'];
        if(is_array($detail) == false) {
            $detail = json_decode($detail);
        }
        if(is_array($detail) == false) {
            return ['status' => false, 'message' => 'format detail tidak valid'];
        }
		
		
		
        $target = $this->many_urinalisa_qc(['where' => ['_.id_urinalisa' => $data['id_urinalisa']]]);
        if($target == null) {
            return ['status' => false, 'message' => 'data tidak ditemukan'];
        }
        // $this->db->where(['id_hasil' => $target['id_hasil']])->update('qc_hasil', [
        //     'tanggal' => $data['tanggal']
        // ]);
        //print_r($target);
        //$qc = $target['qc_input'];
        //$detailQc = $qc['detail'];
        //$detailHasil = $target['detail'];
        foreach ($detail as $key => $d) {
            /*if(empty($d['id_level']) || empty($d['hasil'])) {
                continue;
            }

            $dqc = null;
            foreach ($detailQc as $key2 => $d2) {
                if($d2->id_level == $d['id_level']) {
                    $dqc = $d2;
                    break;
                }
            }
            
            if($dqc == null) {
                continue;
            }*/

            $dhasil = null;
            foreach ($target as $key2 => $d2) {
                if($d2['id_urin'] == $d['id_urin'] && $d2['tanggal'] == $d['tanggal'] && $d2['id_unit'] == $d['id_unit']) {
                    $dhasil = $d2;
                    unset($target[$key2]);
                    break;
                }
            }

            
            if($dhasil == null) {
                $this->db->insert('qc_urinalisa_detail', [
                    'id_urinalisa' => $target['id_urinalisa'],
                    'tanggal' => floatval($d['tanggal']),
                    'berat_jenis' => floatval($d['berat_jenis']),
                    'id_urin' => floatval($d['id_urin']),
					'id_unit' => floatval($d['id_unit']),
                ]);
                continue;
            }

            
            $this->db->where([
                'id' => $dhasil['id_detail']
            ])->update('qc_urinalisa_detail', [
                'tanggal' => floatval($d['tanggal']),
				'berat_jenis' => floatval($d['berat_jenis']),
				'id_urin' => floatval($d['id_urin']),
				'id_unit' => floatval($d['id_unit']),
            ]);
        }
		
        return ['status' => true, 'message' => 'Data berhasil diupdate'];
    }

	
	function many_qc_urinalisa_is_red($args = []) {
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
		if(isset($args["order"]) == false) {
            $args["order"] = [];
        }
        return $this->baseQueryQcUrinalisaIsRed($args)->where($args["where"])->order_by('gtest.id_unit','asc')->get()->result_array();
    }
	
	function baseQueryQcUrinalisaIsRed($args = []) {
        return $this->db->from("qc_urinalisa_is_red _")
            //->join("users usr", "usr.id_user = _.creator_id", "left")
            //->join("branch b1", "b1.branch_id = _.branch_id", "left")			
			//->join("qc_urinalisa_detail b", "b.id_urinalisa = _.id_urinalisa", "left")
            //->join("master_unit gtest", "gtest.id_unit = b.id_unit", "left")
            ->select([
                "_.*",
            ]);
    }

    function pdf($id_hasil)
    {
       
       $qc = $this->db->where('id_hasil', $id_hasil)->get('qc_hasil')->row();
       $qc->detail =  $this->get_pdf_detail($qc->id_qc_input);
       //$qc->input = $this->get_input($qc->id_qc_input);
       //$qc->input_detail = $this->get_input_detail($qc->id_qc_input);
       //$qc->hasil = $this->db->where('id_qc_input', $id_qc_input)->get('qc_hasil')->row();
       $qc->chart = $this->get_data_chart($qc->id_qc_input,$qc->id_hasil);

        return $qc;
    }

    function get_pdf_detail($id_qc_input) {
        $q = $this->db->from('qc_hasil _')
            ->join('branch br', 'br.branch_id = _.branch_id', 'left')
            ->join('qc_input inpt', 'inpt.id_qc_input = _.id_qc_input', 'left')
            ->join('master_qc_alat alat', 'alat.id_alat = inpt.id_alat', 'left')
            ->join('master_test test', 'test.test_id = inpt.id_test', 'left')
            ->join('master_test_group group', 'group.group_id = test.group_id', 'left')
            ->join('master_qc_lot_number lot', 'lot.id_lot_number = inpt.id_lot_number', 'left')
            ->where(['_.id_qc_input' => $id_qc_input])
            ->limit(1)
            ->select([
                '_.*',
                'inpt.*',
                'alat.alat_name',
                'br.branch_name',
                'test.test_name',
                'group.group_name',
                'lot.lot_number',

            ]);
        return $q->get()->result();
    }

    function get_hasil_detail($id_hasil) {
        $q = $this->db->from('qc_hasil_detail _')
            ->join('qc_hasil hsl', 'hsl.id_hasil = _.id_hasil', 'left')
            ->where(['_.id_hasil' => $id_hasil])
            ->select([
                '_.hasil as hasil_qc',
                'hsl.*'
            ]);
        return $q->get()->result();
    }

    function get_input_detail($id_qc_input) {
        $q = $this->db->from('qc_input_detail _')
            ->join('qc_hasil hsl', 'hsl.id_qc_input = _.id_qc_input', 'left')
            ->where(['_.id_qc_input' => $id_qc_input])
            ->select([
                '_.*',
                'hsl.*',
            ]);
        return $q->get()->result();
    }

    function get_input($id_qc_input) {
        $q = $this->db->from('qc_input _')
            ->join('branch br', 'br.branch_id = _.branch_id', 'left')
            ->join('master_test test', 'test.test_id = _.id_test', 'left')
            ->join('master_test_group group', 'group.group_id = test.group_id', 'left')
            ->join('master_qc_lot_number lot', 'lot.id_lot_number = _.id_lot_number', 'left')
            ->join('master_qc_alat alat', 'alat.id_alat = _.id_alat', 'left')
            ->where(['_.id_qc_input' => $id_qc_input])
            ->select([
                '_.*',
                'test.*',
                'lot.*',
                'alat.*',
                'group.group_name',
                'br.branch_name',
            ]);
        return $q->get()->result();
    }
	
	
	function is_out($data){
		
		if(empty($data['tanggal'])) {
            return ['status' => false, 'message' => 'tanggal diperlukan'];
        }
        if(empty($data['creator_id'])) {
            return ['status' => false, 'message' => 'creator id diperlukan'];
        }
        if(empty($data['branch_id'])) {
            return ['status' => false, 'message' => 'branch id diperlukan'];
        }
        if(empty($data['id_qc_input'])) {
            return ['status' => false, 'message' => 'id qc input diperlukan'];
        }
        if(empty($data['detail'])) {
            return ['status' => false, 'message' => 'detail diperlukan'];
        }

        $detail = $data['detail'];
        unset($data['detail']);

        if(is_string($detail)){
            $detail = json_decode($detail);
        }

        if(empty($detail)) {
            return ['status' => false, 'message' => 'detail kosong'];
        }

        $qc = $this->one_input_qc(['where' => ['_.id_qc_input' => $data['id_qc_input']]]);
        if($qc == null) {
            return ['status' => false, 'message' => 'qc tidak dapat ditemukan'];
        }

        $detailqc = $qc['detail'];
        $counterDetail = 0;
        $listDetail = [];
        foreach ($detail as $key => $d) {
            if(isset($d['id_level']) == false || isset($d['hasil']) == false) {
                continue;
            }
            if(empty($d['hasil'])) {
                continue;
            }
            $exist = false;
            foreach ($detailqc as $key => $d2) {
                if($d['id_level'] == $d2->id_level) {
                    $exist = true;
                }
            }
            if($exist) {
                $counterDetail++;
                $listDetail[] = [
                    'id_level' => $d['id_level'],
                    'hasil' => floatval($d['hasil']),
                    'comment' => empty($d['comment']) ? '' : $d['comment']
                ];
            }
        }
        // if($counterDetail == 0) {
        //     return ['status' => false, 'message' => 'data tidak disimpan, data hasil tidak cocok dengan detail qc'];
        // }
		
		$input_qc = $this->one_input_qc(['where' => ['_.id_qc_input' => $data['id_qc_input']]]);
        if($input_qc == null) {
            return ['status' => false, 'message' => 'input not found'];
        }
        $detailInput = $input_qc['detail'];
		
		$hasOut = 0;
		$resultSD= [];
        foreach ($detail as $keyX => $dh) {
            $resultSD['SD'][$keyX]= 0;
			$resultSD['IsTrue'][$keyX]= 0;
			$hasOut = 0;
			foreach ($detailInput as $keyY => $di) {
                if($di->id_level == $dh['id_level']) {
                    $mean = floatval($di->mean);
                    $sd = floatval($di->sd);
                    $result = floatval($dh['hasil']);

                    if($sd > 0 || $sd < 0) {
                        $r = ($result - $mean) / $sd;
						$resultSD['SD'][$keyX]= round($r,2);
                        if($r > 2 || $r < -2) {
							$hasOut = 1;
							$resultSD['IsTrue'][$keyX]= $hasOut;
                            break;
                        }
                    }
                }
            }
            // if($hasOut == 1) {
            //     break;
            // }
            if($hasOut == 1) {
                // break;
            }
        }
		
		//print_r($detail);	
		//rint_r('<br />AAAAAAAA');	
		//print_r($detailInput);	
		//print_r('<br />HASIL<br />');	
		return $resultSD;
			
        /*$this->db->insert('qc_hasil', $data);
        $id = $this->db->insert_id();
	
		if($id == null) {
            return ['status' => false, 'message' => 'terjadi kesalahan saat menyimpan ke dalam database'];
        }

        foreach ($listDetail as $key => $detail) {
            $detail['id_hasil'] = $id;
            $this->db->insert('qc_hasil_detail', $detail);
        }*/
		
		/*$hasil = $this->one_hasil(['where' => ['_.id_hasil' => $id_hasil]]);
        if($hasil == null) {
            return ['status' => false, 'message' => 'hasil not found'];
        }
        $input_qc = $this->one_input_qc(['where' => ['_.id_qc_input' => $hasil['id_qc_input']]]);
        if($input_qc == null) {
            return ['status' => false, 'message' => 'input not found'];
        }
        $detailHasil = $hasil['detail'];
        $detailInput = $input_qc['detail'];
        $hasOut = false;

        foreach ($detailHasil as $key => $dh) {
            foreach ($detailInput as $key => $di) {
                if($di->id_level == $dh['id_level']) {
                    $mean = floatval($di->mean);
                    $sd = floatval($di->sd);
                    $result = floatval($dh['hasil']);

                    if($sd > 0 || $sd < 0) {
                        $r = ($result - $mean) / $sd;
                        if($r > 2 || $r < -2) {
                            $hasOut = true;
                            break;
                        }
                    }
                }
            }
            if($hasOut == true) {
                break;
            }
        }
		print_r($detailInput);*/
	}
	
}
