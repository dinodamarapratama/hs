<?php

class M_attachment extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->helper('myquery');
	}

	function add_attachment($data) {
		$this->db->insert('attachments', $data);
		$id = $this->db->insert_id();

		return ['status' => true, 'id' => $id];
	}

	function baseQuery($args = []) {
		$q = $this->db->from('attachments _')
			->select([
				'_.*'
			]);
		return $q;
	}

	function many_attachment($args = []) {
		$q = myquery($this->baseQuery($args), $args);
		return $q->get()->result_array();
	}

	function one_attachment($args) {
		if($args == null) {
			$args = [];
		}

		$args['limit'] = [1];

		$r = $this->many_attachment($args);

		if(count($r) > 0) {
			return $r[0];
		}
		return null;
	}
}