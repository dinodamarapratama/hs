<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_masterusers extends CI_model
{


public function get_all_users()
    {
        $this->db->select('users.*,branch.branch_name as branch_name,departments.name as name_department,position.name_position as name_position,bagian.name as name_bagian');
        $this->db->from('users');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        $this->db->join('departments', 'users.id_department = departments.id_department');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('bagian', 'users.id_bagian = bagian.id');
        return $this->db->get();
    }

   public function edit_data_masterusers($where, $table)
    {
        $this->db->select('users.*');
        $this->db->from('users');
        $this->db->where($where);
        return $this->db->get();
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $result = $this->db->delete($table);
        return $result;
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

function load_data($params=[], $rows=0, $offset=10,$all=0)
   {
      $this->db->select(['users.*','branch.branch_name']);
      $this->db->from('users');
      if (isset($params['in_param'])) {
        foreach ($params['in_param'] as $key => $value) {
            $this->db->where_in($key,$value);  
        }
        unset($params['in_param']);
      }
      
      if (isset($params['not_in_param'])) {
        foreach ($params['not_in_param'] as $k => $v) {
            $this->db->where_not_in($k,$v);  
        }
        unset($params['not_in_param']);
      }

      if (isset($params['or_param'])) {
        foreach ($params['or_param'] as $k => $v) {
            $this->db->or_where($k,$v);  
        }
        unset($params['or_param']);
      }

      $this->db->where($params);
      if (!$all) {
        $this->db->limit($rows, $offset);  
      }
      $this->db->order_by('name', 'ASC');
      $this->db->join('branch', 'users.branch_id = branch.branch_id');
      $query = $this->db->get();
      return $query->result_array();
   }

function insert($data)
  {
    $this->db->insert('users', $data);
  }

function update($data, $id_user)
  {
    $this->db->where('id_user', $id_user);
    $this->db->update('users', $data);

  }

function delete($id_user)
  {
    $this->db->where('id_user', $id_user);
    $q = $this->db->delete('users');
    return $q;
  }

function getLastFingerID(){
    $this->db->select('CAST(FINGERID as SIGNED) as FINGERID');
    $this->db->where(['nip !=' => '']);
    $this->db->order_by('FINGERID','DESC');
    $this->db->limit(1);
    $res = $this->db->get('users');

    return $res->result_array();
}

function get_all_relations($table)
  {
      return $this->db->get($table);
  }

function getListByID($table,$param=[],$field=[]){
  $this->db->where($param);
  $this->db->select($field);
  $res = $this->db->get($table)->result_array();

  return $res;
}   


// public function get_all_finger($table, $params=[], $rows=0, $offset=10,$all=0)
// {

//   $this->db->select(['id','FingerId','Date','Cabang','min(Recorded) as jam_datang','max(Recorded) as jam_pulang']);
//   $this->db->where($params);
//   if (!$all) {
//     $this->db->limit($rows, $offset);  
//   }
//   $this->db->group_by(['Date','FingerId']);
//   return $this->db->get($table);

// }


public function get_all_finger($params=[])
{

  $this->db->select(['e.id as id','e.UserID as FingerId','e.DateLog','ed.Cabang as Cabang','min(e.TimeLog) as jam_datang','max(e.TimeLog) as jam_pulang']);
  $this->db->join('EventLogDevice ed','ed.DeviceID = e.DeviceID');
  $this->db->where($params);
  $this->db->group_by(['DateLog','FingerId']);
  return $this->db->get('EventLog e');

}

function getFingerIdName(){
  $this->db->select(['name','FINGERID']);
  return $this->db->get('users');
}

function getFingerIdLastName(){
  $this->db->select(['last_name','FINGERID']);
  return $this->db->get('users');
}

function getFingerIdPosisi(){
  $this->db->select(['position_name','FINGERID']);
  return $this->db->get('v_user');
}

function dateRangeFingerOld($startDate, $endDate)
  {
    return $this->db->query("SELECT f.* FROM v_finger f LEFT JOIN users u on f.FingerId = u.FINGERID WHERE  f.Date >='$startDate' AND f.date <='$endDate'");
  }

  function dateRangeFinger($startDate, $endDate)
  {
    return $this->db->query("SELECT f.* FROM v_finger f LEFT JOIN users u on f.FingerId = u.FINGERID WHERE  f.Date >='$startDate' AND f.date <='$endDate'");
  }

  




}
