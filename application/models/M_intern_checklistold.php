<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_intern_checklist extends CI_model
{
	public function get_all_checklist_input($params=[]){
        $this->db->select('m_a.id as mia_id,m_a.branch_id,m_a.keterangan,mqc_a.alat_name,b.branch_name, ic.periode,ic.checklist_name, ic.id as ic_id,ic_in.*,users.name as creator,users.call_name,users.username,ic.periode,ic.id_intern_alat');
        $this->db->from('intern_checklist_input ic_in');
        $this->db->join('intern_checklist ic','ic.id=ic_in.id_intern_checklist','left');
        $this->db->join('master_intern_alat m_a','ic.id_intern_alat=m_a.id','left');
        $this->db->join('branch b','m_a.branch_id=b.branch_id','left');
        $this->db->join('master_qc_alat mqc_a','m_a.id_alat=mqc_a.id_alat','left');
        $this->db->join('users','users.id_user=ic_in.creator','left');
        $this->db->group_by('ic.id_intern_alat');
        $this->db->where($params);
        $query = $this->db->get();
        return $query;
    }

    public function get_all($id,$cabang){
        $this->db->select('mia.*,mqa.*,ic.*,ici.*,u.name as creator,b.branch_name');
        $this->db->from('master_intern_alat mia');
        $this->db->join('master_qc_alat mqa','mqa.id_alat = mia.id_alat','left');
        $this->db->join('intern_checklist ic','ic.id_intern_alat = mia.id','left');
        $this->db->join('intern_checklist_input ici','ici.id_intern_checklist = ic.id','left');
        $this->db->join('users u','u.id_user = ici.creator','left');
        $this->db->join('branch b','b.branch_id = ici.branch_id','left');
        $this->db->group_by('periode');
        $this->db->where('ic.id_intern_alat',$id);
        $this->db->where('ici.branch_id',$cabang);
        $query = $this->db->get();
        return $query;
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function save_data($data=[]){
        $this->db->insert('intern_checklist_input', $data);
        $saved_id = $this->db->insert_id();
        return $saved_id;
    }

    public function update_data($key=0,$data=[]){
    	$this->db->where(['id' => $key]);
        $update = $this->db->update('intern_checklist_input', $data);
        return $update;
    }

    public function hapus_data($param=[]){
        $del = $this->db->delete('intern_checklist_input', $param);
        return $del;
    }

    public function detail($id){
        $this->db->select([
            'm_a.id as mia_id',
            'm_a.branch_id',
            'm_a.keterangan',
            'mqc_a.alat_name',
            'b.branch_name', 
            'ic.periode',
            'ic.checklist_name', 
            'ic.id as ic_id',
            'ic.id_intern_alat',
            'ic_in.*',
            'users.name as creator',
            'users.call_name',
            'users.username',
            'ic.periode'
        ])
                ->from('intern_checklist_input ic_in')
                ->join('intern_checklist ic','ic.id=ic_in.id_intern_checklist','left')
                ->join('master_intern_alat m_a','ic.id_intern_alat=m_a.id','left')
                ->join('branch b','m_a.branch_id=b.branch_id','left')
                ->join('master_qc_alat mqc_a','m_a.id_alat=mqc_a.id_alat','left')
                ->join('users','users.id_user=ic_in.creator','left')
              //  ->group_by('ic.id_intern_alat')
               ->where('ic.id_intern_alat',$id);
        $getDetail = $this->db->get();
        return $getDetail;
    }

}