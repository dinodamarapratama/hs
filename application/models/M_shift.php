<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_shift extends CI_model
{


    public function insert($table,$data=[]){
        $this->db->insert($table, $data);
        $saved_id = $this->db->insert_id();
        return $saved_id;
    }

    public function get_data_shift($params=[]){
        $this->db->select(['paket_shift.nama_paket','branch.branch_name','detail_shift.*']);
        $this->db->from('paket_shift');
        $this->db->join('branch', 'paket_shift.branch_id = branch.branch_id','left');
        $this->db->join('detail_shift', 'paket_shift.id_paket_shift = detail_shift.id_paket_shift','left');
        $this->db->where($params);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_paket_shift($params=[]){
        $this->db->select('paket_shift.*,branch.branch_name');
        $this->db->from('paket_shift');
        $this->db->join('branch', 'paket_shift.branch_id = branch.branch_id','left');
        $this->db->where($params);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_branch($params=[]){
        $this->db->select('*');
        $this->db->from('branch');
        $this->db->where($params);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function delete_detail_shift($param=[]){
        $this->db->where($param);
        $q = $this->db->delete('detail_shift');
        return $q;
    }

    public function delete_shift($param=[],$relations=0){
        $this->db->where($param);
        $q = $this->db->delete('paket_shift');
        if ($q && $relations) {
            $this->delete_detail_shift($param);
        }

        return $q;
    }

    public function update_shift($data=[],$param=[]){
        $this->db->where($param);
        $q = $this->db->update('paket_shift',$data);
        return $q;
    }

    /*---------------------------------- jadwal -------------------------*/


    public function get_data_jadwal($params=[]){
        $this->db->select(['jadwal.*','branch.branch_name','branch.branch_id','users.name']);
        $this->db->from('jadwal');
        $this->db->join('users', 'users.id_user = jadwal.user_id','left');
        $this->db->join('branch', 'branch.branch_id = users.branch_id','left');
        $this->db->where($params);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function delete_jadwal($param=[]){
        $this->db->where($param);
        $q = $this->db->delete('jadwal');

        return $q;
    }

    public function update_jadwal($data=[],$param=[]){
        $this->db->where($param);
        $q = $this->db->update('jadwal',$data);
        return $q;
    }

}
