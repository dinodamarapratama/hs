<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_hs extends CI_model
{

    public function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function get_data($table,$order = '')
    {
        if ($order):
            $this->db->order_by($order,'ASC');
        endif;
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

	public function get_where($where, $table) {
        $this->db->where($where);
        return $this->db->get($table);
	}

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $result = $this->db->delete($table);
        return $result;
    }

    // START HS

     public function get_all($date)
    {
        return $this->db->query("SELECT a.*, UPPER(nama) as namacaps , nama_petugas,
            nama_petugas_hs,
            a.ptgshs_id,
            a.petugas_id,
            a.time_id,
            a.branch_id,
            hs_time.time_name,
            branch.branch_name,
            hs_payment.pay_name,
            users.name as name_creator
            FROM homeservice a
            JOIN (SELECT c.id_user, c.name as nama_petugas FROM users c) t1
                ON t1.id_user = a.petugas_id
            LEFT JOIN (SELECT d.ptgshs_id, c.name as nama_petugas_hs FROM hs_initial_petugas_hs d JOIN users c ON c.id_user = d.id_user) t2
                ON t2.ptgshs_id = a.ptgshs_id
            JOIN hs_time ON hs_time.time_id = a.time_id
            JOIN hs_payment ON hs_payment.pay_id = a.pay_id
            JOIN branch ON branch.branch_id = a.branch_id
            JOIN users ON users.id_user = a.creator_id
            WHERE a.date='$date' order by a.ptgshs_id,a.time_id,a.id
            ");
    }


    public function get_data_by_branch($date, $branch_id)
    {
    return $this->db->query("SELECT a.*, UPPER(nama) as namacaps , nama_petugas,
                            nama_petugas_hs,
                            abbr_hs,
                            a.ptgshs_id,
                            a.petugas_id,
                            a.time_id,
                            hs_time.time_name,
                            branch.branch_name,
                            hs_payment.pay_name,
                            users.image as image,
                            users.name as name_creator
                            FROM homeservice a
                            JOIN (SELECT c.id_user, c.name as nama_petugas FROM users c) t1
                            ON t1.id_user = a.petugas_id
                            LEFT JOIN (SELECT d.ptgshs_id, c.name as nama_petugas_hs, d.abbr_hs as abbr_hs FROM hs_initial_petugas_hs d JOIN users c ON c.id_user = d.id_user) t2
                            ON t2.ptgshs_id = a.ptgshs_id
                            JOIN hs_time ON hs_time.time_id = a.time_id
                            JOIN hs_payment ON hs_payment.pay_id = a.pay_id
                            JOIN branch ON branch.branch_id = a.branch_id
                            JOIN users ON users.id_user = a.creator_id
                            WHERE a.date='$date'
                            AND a.branch_id=$branch_id
                            ");
                            }


    public function get_by_id($id)
    {
        return $this->db->query("SELECT a.*, UPPER(nama) as namacaps , nama_petugas,
            nama_petugas_hs,
            a.ptgshs_id,
            a.petugas_id,
            a.time_id,
            hs_time.time_name,
            branch.branch_name,
            hs_payment.pay_name,
            users.name as name_creator
            FROM homeservice a
            JOIN (SELECT c.id_user, c.name as nama_petugas FROM users c) t1
                ON t1.id_user = a.petugas_id
            LEFT JOIN (SELECT d.ptgshs_id, c.name as nama_petugas_hs FROM hs_initial_petugas_hs d JOIN users c ON c.id_user = d.id_user) t2
                ON t2.ptgshs_id = a.ptgshs_id
            JOIN hs_time ON hs_time.time_id = a.time_id
            JOIN hs_payment ON hs_payment.pay_id = a.pay_id
            JOIN branch ON branch.branch_id = a.branch_id
            JOIN users ON users.id_user = a.creator_id
            WHERE a.id='$id'
            ");
    }

    public function get_data_hs_initial_petugas($branch)
    {   
        $this->db->select('*, users.id_user AS petugas_id')->from('users');
        // $this->db->where("branch_id = $branch");
        // $this->db->where("users.id_position = 2");
        // $this->db->or_where("users.id_position = 3");
        $st="branch_id='$branch' AND (id_position='2' OR id_position='3')";
        $this->db->where($st, NULL, FALSE);  
        // $this->db->where("status LIKE  '%TETAP%'");
        return $this->db->get();
    }

    public function get_data_hs_initial_petugas_hs($branch, $date) {
        $this->db->select('hs_initial_petugas_hs.*,(select users.name from users where users.id_user = hs_initial_petugas_hs.id_user) as name,
		(select users.call_name from users where users.id_user = hs_initial_petugas_hs.id_user) as call_name');
        $this->db->from('hs_initial_petugas_hs');
        //$this->db->join('users','users.id_user = hs_initial_petugas_hs.id_user');
        // $this->db->where_not_in("hs_initial_petugas_hs.abbr_hs", "SELECT abbr_hs FROM hs_initial_petugas_hs WHERE abbr_hs LIKE '%Julita%' AND DAYOFWEEK('$date') IN (3,4,6,7)", false);
        $this->db->where("hs_initial_petugas_hs.branch_id = $branch");
        $this->db->where("hs_initial_petugas_hs.end_date >=",$date);
        $this->db->where("hs_initial_petugas_hs.start_date <=", $date);
        return $this->db->get();
    }

    public function edit_hs($where, $table)
    {
        $this->db->select('homeservice.*');
        $this->db->from('homeservice');
        $this->db->where($where);
        return $this->db->get();
    }

    public function get_available_time($date, $ptgshs_id, $branch) {

        $res = $this->get_branch_time($branch)->first_row();
        if ($res != null && $res->time_ids != null) {
            return $this->db->query("SELECT * FROM hs_time where time_id IN ($res->time_ids) AND `time_id` NOT IN (SELECT `time_id` FROM homeservice WHERE date='$date' AND ptgshs_id='$ptgshs_id' AND status != 'Cancel' and time_id is not null) ");
        } else {
            $this->db->select('*')->from('hs_time');
            $this->db->where("`time_id` NOT IN (SELECT `time_id` FROM homeservice WHERE date='$date' AND ptgshs_id='$ptgshs_id' AND status != 'Cancel' and time_id is not null) ");
            return $this->db->get();
        }
    }

    public function get_branch_time($branch) {
        $this->db->select('time_ids')->from('branch_time');
        $this->db->where('branch_id', $branch);
        return $this->db->get();
    }

    public function get_branch_hs_time($branch) {
        $res = $this->get_branch_time($branch)->first_row();
        if ($res != null && $res->time_ids != null) {
            return $this->db->query("SELECT * FROM hs_time where time_id IN ($res->time_ids)");
        } else {
            return $this->get_data('hs_time');
        }
    }

    // get time
    public function getTimeHomeService($cabang, $dari, $sampai) {
        $this->db->select('DISTINCT(hs_time.time_name)');
        $this->db->from('homeservice');
        $this->db->where("homeservice.branch_id = $cabang");
        $this->db->where("homeservice.date >=",$dari);
        $this->db->where("homeservice.date <=", $sampai);
        $this->db->where("homeservice.alamat !=", 'Autoblock');
        $this->db->join('hs_time','hs_time.time_id = homeservice.time_id');
        $this->db->order_by(1, 'ASC');
        return $this->db->get();
    }

    // get data hs
    public function getDataHomeService($cabang, $dari, $sampai) {
        $this->db->select('
          homeservice.date,
          hs_time.time_name,
          COUNT(homeservice.pid) AS totalpasien,
          hs_initial_petugas_hs.abbr_hs,
          homeservice.nama,
          homeservice.reason,
          homeservice.time_selesai
        ');
        $this->db->from('homeservice');
        $this->db->where("homeservice.branch_id = $cabang");
        $this->db->where("homeservice.date >=",$dari);
        $this->db->where("homeservice.date <=", $sampai);
        $this->db->where("homeservice.alamat !=", 'Autoblock');
        $this->db->join('hs_initial_petugas_hs','hs_initial_petugas_hs.ptgshs_id = homeservice.ptgshs_id');
        $this->db->join('hs_time','hs_time.time_id = homeservice.time_id');
        $this->db->group_by(array(1,2,4));
        $this->db->order_by(1, 'ASC');
        $this->db->order_by(4, 'ASC');
        $this->db->order_by(2, 'ASC');
        return $this->db->get();
    }

    public function getDataHomeServiceForExportV2($cabang, $dari, $sampai) {
        $this->db->select('
          homeservice.date,
          hs_time.time_name,
          homeservice.jumlah_pasien,
          hs_initial_petugas_hs.abbr_hs,
          homeservice.nama,
          homeservice.reason,
          homeservice.catatan,
          homeservice.time_selesai,
          homeservice.ptgshs_id
        ');
        $this->db->from('homeservice');
        $this->db->where("homeservice.branch_id = $cabang");
        $this->db->where("homeservice.date >=",$dari);
        $this->db->where("homeservice.date <=", $sampai);
        $this->db->where("homeservice.alamat !=", 'Autoblock');
        $this->db->join('hs_initial_petugas_hs','hs_initial_petugas_hs.ptgshs_id = homeservice.ptgshs_id');
        $this->db->join('hs_time','hs_time.time_id = homeservice.time_id');
        $this->db->group_by(array(1,2,4));
        $this->db->order_by(1, 'ASC');
        $this->db->order_by(4, 'ASC');
        $this->db->order_by(2, 'ASC');
        return $this->db->get();
    }

    public function detail($id){
        $getDetail = $this->db->query("SELECT a.*, UPPER(nama) as namacaps , nama_petugas,
            nama_petugas_hs,
            abbr_hs,
            a.ptgshs_id,
            a.petugas_id,
            a.time_id,
            hs_time.time_name,
            branch.branch_name,
            hs_payment.pay_name,
            users.image as image,
            users.name as name_creator
            FROM homeservice a
            JOIN (SELECT c.id_user, c.name as nama_petugas FROM users c) t1
            ON t1.id_user = a.petugas_id
            JOIN (SELECT d.ptgshs_id, c.name as nama_petugas_hs, d.abbr_hs as abbr_hs FROM hs_initial_petugas_hs d JOIN users c ON c.id_user = d.id_user) t2
            ON t2.ptgshs_id = a.ptgshs_id
            JOIN hs_time ON hs_time.time_id = a.time_id
            JOIN hs_payment ON hs_payment.pay_id = a.pay_id
            JOIN branch ON branch.branch_id = a.branch_id
            JOIN users ON users.id_user = a.creator_id
            WHERE a.id='$id'
        ");
        return $getDetail->row();
    }
	
	public function get_all_masterabbr($branch_id = null)
	{
	$this->db->select('abbr.*,br.*,usr.name as fullname,abbr.abbr_id,abbr.created_at');
	$this->db->from('hs_abbr abbr');
	$this->db->join('users usr','usr.id_user = abbr.creator_id');
	$this->db->join('branch br','br.branch_id = abbr.branch_id');
	if(!empty($branch_id)){
	$this->db->where('abbr.branch_id', $branch_id);
	}

	return $this->db->get();
	}
}
