<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_techreports extends CI_model
{

    public function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $this->db->where($table);
    }

    public function insert_batch($table, $data)
    {
        $this->db->insert_batch($table, $data);
    }

    public function get_data_like($where, $colum, $table)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->like($colum, $where);
        return $this->db->get();
    }

    public function get_data_join()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('position', 'users.id_position = position.id');
        return $this->db->get();
    }

    public function get_sent_to($id)
    {
        $this->db->select('name, (select name_position from position where users.id_position = position.id_position) as name_position, id_receiver');
        $this->db->from('techreports_receiver');
        $this->db->join('users', 'techreports_receiver.id_receiver = users.id_user');
        //$this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('techreports_receiver.id_receiver', $id);

        return $this->db->get();

    }
	
	public function get_all_sent_to($id)
    {
        $this->db->select('name, (select name_position from position where users.id_position = position.id_position) as name_position, id_receiver');
        $this->db->from('techreports_receiver');
        $this->db->join('users', 'techreports_receiver.id_receiver = users.id_user');
        //$this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('techreports_receiver.id_techreports', $id);

        return $this->db->get();

    }
	
    public function get_sent_count($id)
    {
        $this->db->from('techreports_receiver');
        $this->db->where('techreports_receiver.id_techreports', $id);

        return $this->db->get()->num_rows();
    }

    public function get_read_count($id)
    {
        $this->db->from('techreports_receiver');
        $this->db->where('techreports_receiver.id_techreports', $id);
        $this->db->where('techreports_receiver.is_view', '1');

        return $this->db->get()->num_rows();
    }

    public function get_user_read($id)
    {
        $this->db->select('name, name_position, id_receiver, is_view');
        $this->db->from('techreports_receiver');
        $this->db->join('users', 'techreports_receiver.id_receiver = users.id_user','left');
        $this->db->join('position', 'users.id_position = position.id_position','left');
        $this->db->where('techreports_receiver.id_techreports', $id);

        return $this->db->get();

    }

    public function get_data_inventaris()
    {
        $this->db->select('master_inventaris_ga.*,br.*');
        $this->db->from('master_inventaris_ga');
        $this->db->join('branch br', 'br.branch_id = master_inventaris_ga.branch_id');

        return $this->db->get();

    }
		
	public function total_data_received($id /*id user*/, $id_techreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('techreports.id');
        $this->db->from('techreports');
        //$this->db->join('techreports_receiver', 'techreports_receiver.id_techreports = techreports.id');
        $this->db->join('users', 'techreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch to_branch', 'to_branch.branch_id = techreports.to_branch');


        if (!empty($id_techreports)) {
            //$this->db->where('techreports_receiver.id_techreports', $id_techreports);
			$this->db->where('techreports.id', $id_techreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
			$this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('techreports.kategori',$search,'both');
            $this->db->or_like('techreports.title',$search,'both');
            $this->db->or_like('techreports.date',$search,'both');
            
			$this->db->or_like('techreports.lmp_des',$search,'both');
            $this->db->or_like('techreports.kf_des',$search,'both');
            $this->db->or_like('techreports.lift_des',$search,'both');
            $this->db->or_like('techreports.ef_des',$search,'both');
            $this->db->or_like('techreports.ups_des',$search,'both');
            $this->db->or_like('techreports.aq_des',$search,'both');
            $this->db->or_like('techreports.pmp_des',$search,'both');
            $this->db->or_like('techreports.pabx_des',$search,'both');
            $this->db->or_like('techreports.wtp_des',$search,'both');
            $this->db->or_like('techreports.stp_ipal_des',$search,'both');
            $this->db->or_like('techreports.ge_des',$search,'both');
            $this->db->or_like('techreports.ac_des',$search,'both');
            $this->db->or_like('techreports.lain_des',$search,'both');
            //$this->db->limit(1);
			$this->db->group_end();

        }
		$this->db->where('techreports.id in (select techreports_receiver.id_techreports from techreports_receiver where id_receiver = '.$id.' and techreports_receiver.is_closed<>"1")', null);
        //$this->db->where('techreports_receiver.id_receiver', $id);
        //$this->db->where('techreports_receiver.is_closed <>', '1');
        $this->db->where('upper(techreports.status)', 'OPEN');
		return $this->db->get();
    }

    public function total_data_sent($id /*id user*/, $id_techreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('techreports.id');
        $this->db->from('techreports');
        //$this->db->join('techreports_receiver', 'techreports_receiver.id_techreports = techreports.id', 'left');
        // $this->db->join('techreports_receiver', 'techreports_receiver.id_techreports = techreports.id', 'left');
        $this->db->join('users', 'techreports.from_id = users.id_user','left');
        // $this->db->join('users', 'techreports.from_id = users.id_user', 'left');
        $this->db->join('position', 'users.id_position = position.id_position', 'left');
        // $this->db->join('position', 'users.id_position = position.id_position', 'left');
        $this->db->join('branch to_branch', 'to_branch.branch_id = techreports.to_branch', 'left');
        // $this->db->join('branch to_branch', 'to_branch.branch_id = techreports.to_branch', 'left');

        if (!empty($id_techreports)) {
            // $this->db->where('techreports_receiver.id_techreports', $id_techreports);
            $this->db->where('techreports.id', $id_techreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->group_start();
			$this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('techreports.kategori',$search,'both');
            $this->db->or_like('techreports.title',$search,'both');
            $this->db->or_like('techreports.date',$search,'both');

            $this->db->or_like('techreports.lmp_des',$search,'both');
            $this->db->or_like('techreports.kf_des',$search,'both');
            $this->db->or_like('techreports.lift_des',$search,'both');
            $this->db->or_like('techreports.ef_des',$search,'both');
            $this->db->or_like('techreports.ups_des',$search,'both');
            $this->db->or_like('techreports.aq_des',$search,'both');
            $this->db->or_like('techreports.pmp_des',$search,'both');
            $this->db->or_like('techreports.pabx_des',$search,'both');
            $this->db->or_like('techreports.wtp_des',$search,'both');
            $this->db->or_like('techreports.stp_ipal_des',$search,'both');
            $this->db->or_like('techreports.ge_des',$search,'both');
            $this->db->or_like('techreports.ac_des',$search,'both');
            $this->db->or_like('techreports.lain_des',$search,'both');
            //$this->db->limit(1);
			$this->db->group_end();
        }
        $this->db->where('techreports.from_id', $id);
        $this->db->where('techreports.id_draft', '0');
        $this->db->where('upper(techreports.status)', 'OPEN');
        /* data belum dihapus oleh creator */
        $this->db->where_in('techreports.is_view', [0,1]);
        /* data belum dihapus oleh receiver */
        //$this->db->where('techreports_receiver.is_closed', '0');
		
		$this->db->where('techreports.id in (select techreports_receiver.id_techreports from techreports_receiver where techreports_receiver.is_closed="0")', null);
		
		return $this->db->get();
    }
	
	public function total_data_archived($id /*id user*/, $id_techreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('techreports.id');
        $this->db->from('techreports');
        //$this->db->join('techreports_receiver', 'techreports_receiver.id_techreports = techreports.id');
        $this->db->join('users', 'techreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch to_branch', 'to_branch.branch_id = techreports.to_branch');
        if (!empty($id_techreports)) {
            //$this->db->where('techreports_receiver.id_techreports', $id_techreports);
			$this->db->where('techreports.id', $id_techreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('techreports.kategori',$search,'both');
            $this->db->or_like('techreports.title',$search,'both');
            $this->db->or_like('techreports.date',$search,'both');

            $this->db->or_like('techreports.lmp_des',$search,'both');
            $this->db->or_like('techreports.kf_des',$search,'both');
            $this->db->or_like('techreports.lift_des',$search,'both');
            $this->db->or_like('techreports.ef_des',$search,'both');
            $this->db->or_like('techreports.ups_des',$search,'both');
            $this->db->or_like('techreports.aq_des',$search,'both');
            $this->db->or_like('techreports.pmp_des',$search,'both');
            $this->db->or_like('techreports.pabx_des',$search,'both');
            $this->db->or_like('techreports.wtp_des',$search,'both');
            $this->db->or_like('techreports.stp_ipal_des',$search,'both');
            $this->db->or_like('techreports.ge_des',$search,'both');
            $this->db->or_like('techreports.ac_des',$search,'both');
            $this->db->or_like('techreports.lain_des',$search,'both');
            //$this->db->limit(1);
            $this->db->group_end();
        }
        $this->db->where('upper(techreports.status)', 'Close');
        // $this->db->where('techreports.status <>', 'Open');
        // $this->db->where('techreports.is_view <>', '1');
        $this->db->group_start();
            $this->db->or_group_start();
                $this->db->where('techreports.from_id', $id);
                /* data belum dihapus oleh creator*/
                $this->db->where_in('techreports.is_view', [0,1]);
            $this->db->group_end();
            $this->db->or_group_start();
				
				$this->db->where('techreports.id in (select techreports_receiver.id_techreports from techreports_receiver where id_receiver = '.$id.' and techreports_receiver.is_closed="0")', null);
                //$this->db->where('techreports_receiver.id_receiver', $id);
                /* data belum dihapus oleh receiver*/
                //$this->db->where('techreports_receiver.is_closed', '0');
            $this->db->group_end();
        $this->db->group_end();
		return $this->db->get();
    }

	public function total_data_draft($id /*id user*/, $id_techreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('techreports.id');
        $this->db->from('techreports');
        //$this->db->join('techreports_receiver', 'techreports_receiver.id_techreports = techreports.id');
        $this->db->join('users', 'techreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch to_branch', 'to_branch.branch_id = techreports.to_branch');
        if (!empty($id_techreports)) {
            //$this->db->where('techreports_receiver.id_techreports', $id_techreports);
			$this->db->where('techreports.id', $id_techreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
			$this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('techreports.kategori',$search,'both');
            $this->db->or_like('techreports.title',$search,'both');
            $this->db->or_like('techreports.date',$search,'both');

            $this->db->or_like('techreports.lmp_des',$search,'both');
            $this->db->or_like('techreports.kf_des',$search,'both');
            $this->db->or_like('techreports.lift_des',$search,'both');
            $this->db->or_like('techreports.ef_des',$search,'both');
            $this->db->or_like('techreports.ups_des',$search,'both');
            $this->db->or_like('techreports.aq_des',$search,'both');
            $this->db->or_like('techreports.pmp_des',$search,'both');
            $this->db->or_like('techreports.pabx_des',$search,'both');
            $this->db->or_like('techreports.wtp_des',$search,'both');
            $this->db->or_like('techreports.stp_ipal_des',$search,'both');
            $this->db->or_like('techreports.ge_des',$search,'both');
            $this->db->or_like('techreports.ac_des',$search,'both');
            $this->db->or_like('techreports.lain_des',$search,'both');
            //$this->db->limit(1);
			$this->db->group_end();
        }
        $this->db->where('techreports.from_id', $id);
        // $this->db->where('techreports.is_view <>', '1');
        // $this->db->where('techreports.id_draft <>', '0');
        $this->db->where('techreports.id_draft', '1');
		return $this->db->get();
    }
	
    public function get_data_received($id /*id user*/, $id_techreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('techreports.*,techreports.status as status,users.*,position.*,to_branch.*,
            (select techreports_receiver.is_view from techreports_receiver where techreports_receiver.id_techreports = techreports.id and techreports_receiver.id_receiver = '.$id.' group by techreports_receiver.id_receiver) as `read`,
			techreports.status as status');//techreports_receiver.id_techreports,techreports_receiver.id_receiver,techreports_receiver.is_view as read,
            
        $this->db->from('techreports');
        //$this->db->join('techreports_receiver', 'techreports_receiver.id_techreports = techreports.id');
        $this->db->join('users', 'techreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch to_branch', 'to_branch.branch_id = techreports.to_branch');


        if (!empty($id_techreports)) {
            //$this->db->where('techreports_receiver.id_techreports', $id_techreports);
			$this->db->where('techreports.id', $id_techreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
			$this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('techreports.kategori',$search,'both');
            $this->db->or_like('techreports.title',$search,'both');
            $this->db->or_like('techreports.date',$search,'both');
            
			$this->db->or_like('techreports.lmp_des',$search,'both');
            $this->db->or_like('techreports.kf_des',$search,'both');
            $this->db->or_like('techreports.lift_des',$search,'both');
            $this->db->or_like('techreports.ef_des',$search,'both');
            $this->db->or_like('techreports.ups_des',$search,'both');
            $this->db->or_like('techreports.aq_des',$search,'both');
            $this->db->or_like('techreports.pmp_des',$search,'both');
            $this->db->or_like('techreports.pabx_des',$search,'both');
            $this->db->or_like('techreports.wtp_des',$search,'both');
            $this->db->or_like('techreports.stp_ipal_des',$search,'both');
            $this->db->or_like('techreports.ge_des',$search,'both');
            $this->db->or_like('techreports.ac_des',$search,'both');
            $this->db->or_like('techreports.lain_des',$search,'both');
            //$this->db->limit(1);
			$this->db->group_end();

        }
		$this->db->where('techreports.id in (select techreports_receiver.id_techreports from techreports_receiver where id_receiver = '.$id.' and techreports_receiver.is_closed<>"1")', null);
        //$this->db->where('techreports_receiver.id_receiver', $id);
        //$this->db->where('techreports_receiver.is_closed <>', '1');
        $this->db->where('upper(techreports.status)', 'OPEN');
        // $this->db->where('techreports.status <>', 'Close');
        //$this->db->order_by('techreports_receiver.id', 'desc');
		$this->db->order_by('techreports.id', 'desc');
		
		$limit = 10;
		$offset = 0;
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		if(isset($args['offset'])) {
			$offset = $args['offset'];
			unset($args['offset']);
		}
		
		
		$this->db->limit($limit,$offset);
        return $this->db->get();
    }

    public function get_data_sent($id, $id_techreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('techreports.*,users.*,position.*,to_branch.*,techreports.status as status');//techreports_receiver.id_techreports,techreports_receiver.id_receiver
        $this->db->from('techreports');
        //$this->db->join('techreports_receiver', 'techreports_receiver.id_techreports = techreports.id', 'left');
        // $this->db->join('techreports_receiver', 'techreports_receiver.id_techreports = techreports.id', 'left');
        $this->db->join('users', 'techreports.from_id = users.id_user','left');
        // $this->db->join('users', 'techreports.from_id = users.id_user', 'left');
        $this->db->join('position', 'users.id_position = position.id_position', 'left');
        // $this->db->join('position', 'users.id_position = position.id_position', 'left');
        $this->db->join('branch to_branch', 'to_branch.branch_id = techreports.to_branch', 'left');
        // $this->db->join('branch to_branch', 'to_branch.branch_id = techreports.to_branch', 'left');

        if (!empty($id_techreports)) {
            // $this->db->where('techreports_receiver.id_techreports', $id_techreports);
            $this->db->where('techreports.id', $id_techreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->group_start();
			$this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('techreports.kategori',$search,'both');
            $this->db->or_like('techreports.title',$search,'both');
            $this->db->or_like('techreports.date',$search,'both');

            $this->db->or_like('techreports.lmp_des',$search,'both');
            $this->db->or_like('techreports.kf_des',$search,'both');
            $this->db->or_like('techreports.lift_des',$search,'both');
            $this->db->or_like('techreports.ef_des',$search,'both');
            $this->db->or_like('techreports.ups_des',$search,'both');
            $this->db->or_like('techreports.aq_des',$search,'both');
            $this->db->or_like('techreports.pmp_des',$search,'both');
            $this->db->or_like('techreports.pabx_des',$search,'both');
            $this->db->or_like('techreports.wtp_des',$search,'both');
            $this->db->or_like('techreports.stp_ipal_des',$search,'both');
            $this->db->or_like('techreports.ge_des',$search,'both');
            $this->db->or_like('techreports.ac_des',$search,'both');
            $this->db->or_like('techreports.lain_des',$search,'both');
            //$this->db->limit(1);
			$this->db->group_end();
        }
        $this->db->where('techreports.from_id', $id);
        $this->db->where('techreports.id_draft', '0');
        $this->db->where('upper(techreports.status)', 'OPEN');
        /* data belum dihapus oleh creator */
        $this->db->where_in('techreports.is_view', [0,1]);
        /* data belum dihapus oleh receiver */
        //$this->db->where('techreports_receiver.is_closed', '0');
		
		$this->db->where('techreports.id in (select techreports_receiver.id_techreports from techreports_receiver where techreports_receiver.is_closed="0")', null);
		
        // $this->db->where('techreports.status <>', 'Close');
        // $this->db->where('techreports.id_draft <>', '1');
        // $this->db->where('techreports.is_view <>', '1');
        $this->db->order_by('techreports.id', 'desc');
        // echo $this->db->get_compiled_select();
        // exit();
        return $this->db->get();
    }

    public function get_data_archived($id, $id_techreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('techreports.*,users.*,position.*,to_branch.*,techreports.status as status');//techreports_receiver.id_techreports,techreports_receiver.id_receiver
        $this->db->from('techreports');
        //$this->db->join('techreports_receiver', 'techreports_receiver.id_techreports = techreports.id');
        $this->db->join('users', 'techreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch to_branch', 'to_branch.branch_id = techreports.to_branch');
        if (!empty($id_techreports)) {
            //$this->db->where('techreports_receiver.id_techreports', $id_techreports);
			$this->db->where('techreports.id', $id_techreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('techreports.kategori',$search,'both');
            $this->db->or_like('techreports.title',$search,'both');
            $this->db->or_like('techreports.date',$search,'both');

            $this->db->or_like('techreports.lmp_des',$search,'both');
            $this->db->or_like('techreports.kf_des',$search,'both');
            $this->db->or_like('techreports.lift_des',$search,'both');
            $this->db->or_like('techreports.ef_des',$search,'both');
            $this->db->or_like('techreports.ups_des',$search,'both');
            $this->db->or_like('techreports.aq_des',$search,'both');
            $this->db->or_like('techreports.pmp_des',$search,'both');
            $this->db->or_like('techreports.pabx_des',$search,'both');
            $this->db->or_like('techreports.wtp_des',$search,'both');
            $this->db->or_like('techreports.stp_ipal_des',$search,'both');
            $this->db->or_like('techreports.ge_des',$search,'both');
            $this->db->or_like('techreports.ac_des',$search,'both');
            $this->db->or_like('techreports.lain_des',$search,'both');
            //$this->db->limit(1);
            $this->db->group_end();
        }
        $this->db->where('upper(techreports.status)', 'Close');
        // $this->db->where('techreports.status <>', 'Open');
        // $this->db->where('techreports.is_view <>', '1');
        $this->db->group_start();
            $this->db->or_group_start();
                $this->db->where('techreports.from_id', $id);
                /* data belum dihapus oleh creator*/
                $this->db->where_in('techreports.is_view', [0,1]);
            $this->db->group_end();
            $this->db->or_group_start();
				
				$this->db->where('techreports.id in (select techreports_receiver.id_techreports from techreports_receiver where id_receiver = '.$id.' and techreports_receiver.is_closed="0")', null);
                //$this->db->where('techreports_receiver.id_receiver', $id);
                /* data belum dihapus oleh receiver*/
                //$this->db->where('techreports_receiver.is_closed', '0');
            $this->db->group_end();
        $this->db->group_end();
        $this->db->order_by('techreports.id', 'desc');
        return $this->db->get();
    }

    public function get_data_draft($id, $id_techreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('techreports.*,users.*,position.*,to_branch.*,techreports.status as status');//techreports_receiver.id_techreports,techreports_receiver.id_receiver,
        $this->db->from('techreports');
        //$this->db->join('techreports_receiver', 'techreports_receiver.id_techreports = techreports.id');
        $this->db->join('users', 'techreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch to_branch', 'to_branch.branch_id = techreports.to_branch');
        if (!empty($id_techreports)) {
            //$this->db->where('techreports_receiver.id_techreports', $id_techreports);
			$this->db->where('techreports.id', $id_techreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
			$this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('techreports.kategori',$search,'both');
            $this->db->or_like('techreports.title',$search,'both');
            $this->db->or_like('techreports.date',$search,'both');

            $this->db->or_like('techreports.lmp_des',$search,'both');
            $this->db->or_like('techreports.kf_des',$search,'both');
            $this->db->or_like('techreports.lift_des',$search,'both');
            $this->db->or_like('techreports.ef_des',$search,'both');
            $this->db->or_like('techreports.ups_des',$search,'both');
            $this->db->or_like('techreports.aq_des',$search,'both');
            $this->db->or_like('techreports.pmp_des',$search,'both');
            $this->db->or_like('techreports.pabx_des',$search,'both');
            $this->db->or_like('techreports.wtp_des',$search,'both');
            $this->db->or_like('techreports.stp_ipal_des',$search,'both');
            $this->db->or_like('techreports.ge_des',$search,'both');
            $this->db->or_like('techreports.ac_des',$search,'both');
            $this->db->or_like('techreports.lain_des',$search,'both');
            //$this->db->limit(1);
			$this->db->group_end();
        }
        $this->db->where('techreports.from_id', $id);
        // $this->db->where('techreports.is_view <>', '1');
        // $this->db->where('techreports.id_draft <>', '0');
        $this->db->where('techreports.id_draft', '1');
        $this->db->order_by('techreports.id', 'desc');
        return $this->db->get();
    }

    public function get_user_replay($id)
    {
        $this->db->select('*');
        $this->db->from('techreports');
        $this->db->join('techreports_reply', 'techreports.id = techreports_reply.id_techreports');
        $this->db->join('users', 'techreports_reply.sender = users.id_user');
        $this->db->join('branch', 'branch.branch_id = users.branch_id');
        $this->db->join('position', 'position.id_position = users.id_position');
        $this->db->where('techreports.id', $id);

        return $this->db->get();
    }

    public function get_to_id($id,$user_id)
    {
        $this->db->select('*');
        $this->db->from('techreports_receiver');
        $this->db->join('techreports', 'techreports.id = techreports_receiver.id_techreports');
        $this->db->where('techreports.id', $id);
        $this->db->where('techreports_receiver.id_receiver', $user_id);

        return $this->db->get();
    }

    public function get_from_id($id)
    {
        $this->db->select('*');
        $this->db->from('techreports_receiver');
        $this->db->join('techreports', 'techreports.id = techreports_receiver.id_techreports');
        $this->db->where('techreports.id', $id);

        return $this->db->get();
    }

    public function get_notification($id)
    {
        return $this->db->from("notifications")
            ->where([
                "to_id" => $id
            ])
            ->group_start()
                ->or_where(["read" => null])
                ->or_where(["read" => 0])
            ->group_end()
            // ->where(["anime" => "naruto"])
            ->get();

        // $this->db->select('*');
        // $this->db->from('notifications');
        // $this->db->where('to_id', $id);
        // $this->db->where('read', 0);
        // $this->db->where('read <>', 1);
        // $this->db->order_by('id', 'desc');

        // return $this->db->get();
    }


    public function get_user_hrd()
    {
        $this->db->select('id_user,name');
        $this->db->from('users');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('name_position','STAFF HRD');

        return $this->db->get();
    }

    public function testing($array)
    {
        return print("<pre>" . print_r($array, true) . "</pre>");
    }

    function getCountReceived($id_user, $args) {
        $args['where']['rcv.id_receiver'] = $id_user;
        $args['where']['upper(_.status)'] = 'OPEN';
        $q = $this->queryReceived($args);
        $sql = "select count(*) as jumlah from ($q) s1";
        return $this->db->query($sql)->row();
    }

    function getCountSent($id_user, $args) {
        $args['where']['_.from_id'] = $id_user;
        $args['where']['upper(_.status)'] = 'OPEN';
        $q = $this->querySent($args);
        $sql = "select count(*) as jumlah from ($q) s1";
        return $this->db->query($sql)->row();
    }

    function getCountArchived($id_user, $args) {
        $args['where']['upper(_.status)'] = 'CLOSE';
        $args['join_receiver'] = true;
        $sql = $this->queryArchived($id_user, $args);
        $sql = "select count(*) as jumlah from ($sql) s1";
        return $this->db->query($sql)->row();
    }

    function queryReceived($args) {
        $this->load->helper('myquery');
        $args['join_receiver'] = true;
        $q = myquery($this->baseQuery($args), $args);
        $q->select('case when (rcv.is_view = 0 or rcv.is_view_comment = 0) then _.last_update else _.created_at end as last_update2', false);
        /* data belum dihapus oleh creator */
        $q->where_in('_.is_view', [0,1]);
        /* data belum dihapus oleh receiver */
        $q->where('rcv.is_closed', '0');
        // return $q->get()->result();
        $sql = $q->get_compiled_select();
        return $sql;
    }

    function querySent($args) {
        $this->load->helper('myquery');
        $args['join_receiver'] = false;
        $q = myquery($this->baseQuery($args), $args);
        // return $q->get()->result();
        $q  ->select('0 as is_view_rcv')
            ->select('case when _.is_view = 0 then _.last_update else _.created_at end as last_update2', false);
        /* data belum dihapus oleh creator */
        $q->where_in('_.is_view', [0,1]);
        $sql = $q->get_compiled_select();
        return $sql;
    }

    function queryArchived($id_user, $args) {
        $argsReceived = $args;
        $argsReceived['where']['rcv.id_receiver'] = $id_user;

        $argsSent = $args;
        $argsSent['where']['_.from_id'] = $id_user;

        $sqlReceived = $this->queryReceived($argsReceived);
        $sqlSent = $this->querySent($argsSent);

        $sql = "$sqlReceived  \n\n\nunion  \n\n\n$sqlSent";

        return $sql;
    }

    function baseQuery($args=[]){
        if(isset($args['join_receiver']) == false) {
            $args['join_receiver'] = false;
        }
        if(isset($args['join_reply']) == false) {
            $args['join_reply'] = true;
        }

        $qReply = '';
        if($args['join_reply'] == true) {
            $q = $this->db->from('techreports_reply _')
                ->select('count(*) as jumlah', false)
                ->select('GROUP_CONCAT(_.message_reply) as reply_all', false)
                ->select([
                    'id_techreports'
                ])
                ->group_by('id_techreports');
            $qReply = $q->get_compiled_select();
        }
        $r = $this->db->from("techreports _")
            ->join('users usr', 'usr.id_user = _.from_id', 'left')
            ->join('position ps', 'ps.id_position = usr.id_position', 'left')
            ->join('bagian bg', 'bg.id = usr.id_bagian')
            ->join('branch b', 'b.branch_id = _.to_branch', 'left')
            ->join('v_master_inventaris ilmp', 'ilmp.id = _.lmp_inventaris', 'left')
            ->join('v_master_inventaris ikf', 'ikf.id = _.kf_inventaris', 'left')
            ->join('v_master_inventaris ilift', 'ilift.id = _.lift_inventaris', 'left')
            ->join('v_master_inventaris ief', 'ief.id = _.ef_inventaris', 'left')
            ->join('v_master_inventaris iups', 'iups.id = _.ups_inventaris', 'left')
            ->join('v_master_inventaris iaq', 'iaq.id = _.aq_inventaris', 'left')
            ->join('v_master_inventaris ipmp', 'ipmp.id = _.pmp_inventaris', 'left')
            ->join('v_master_inventaris ipabx', 'ipabx.id = _.pabx_inventaris', 'left')
            ->join('v_master_inventaris iwtp', 'iwtp.id = _.wtp_inventaris', 'left')
            ->join('v_master_inventaris istp', 'istp.id = _.stp_ipal_inventaris', 'left')
            ->join('v_master_inventaris ige', 'ige.id = _.ge_inventaris', 'left')
            ->join('v_master_inventaris iac', 'iac.id = _.ac_inventaris', 'left')
            // ->join('v_master_inventaris iprfgb', 'iprfgb.id = _.prfgb_inventaris', 'left')
            ->join('v_master_inventaris ilain', 'ilain.id = _.lain_inventaris', 'left')
            ->join('v_master_inventaris ivr', 'ivr.id = _.vr_inventaris', 'left')
            ->join('v_master_inventaris ivs', 'ivs.id = _.vs_inventaris', 'left')
            ->join('v_master_inventaris ivt', 'ivt.id = _.vt_inventaris', 'left')
            ->join('v_master_inventaris igr', 'igr.id = _.gr_inventaris', 'left')
            ->select([
                '_.*',
                'usr.name',
                'usr.last_name',
                'usr.call_name',
                'usr.username',
                'usr.image',
                'ps.name_position',
                'bg.name as bagian',
                'b.branch_name as to_branch_name',
                "CONCAT(ilmp.branch_name, ' ', ilmp.lokasi, ' ', ilmp.nama_ruang, ' ', ilmp.inventaris_check) as lmp_inventaris_text",
                "CONCAT(ikf.branch_name, ' ', ikf.lokasi, ' ', ikf.nama_ruang, ' ', ikf.inventaris_check) as kf_inventaris_text",
                "CONCAT(ilift.branch_name, ' ', ilift.lokasi, ' ', ilift.nama_ruang, ' ', ilift.inventaris_check) as lift_inventaris_text",
                "CONCAT(ief.branch_name, ' ', ief.lokasi, ' ', ief.nama_ruang, ' ', ief.inventaris_check) as ef_inventaris_text",
                "CONCAT(iups.branch_name, ' ', iups.lokasi, ' ', iups.nama_ruang, ' ', iups.inventaris_check) as ups_inventaris_text",
                "CONCAT(iaq.branch_name, ' ', iaq.lokasi, ' ', iaq.nama_ruang, ' ', iaq.inventaris_check) as aq_inventaris_text",
                "CONCAT(ipmp.branch_name, ' ', ipmp.lokasi, ' ', ipmp.nama_ruang, ' ', ipmp.inventaris_check) as pmp_inventaris_text",
                "CONCAT(ipabx.branch_name, ' ', ipabx.lokasi, ' ', ipabx.nama_ruang, ' ', ipabx.inventaris_check) as pabx_inventaris_text",
                "CONCAT(iwtp.branch_name, ' ', iwtp.lokasi, ' ', iwtp.nama_ruang, ' ', iwtp.inventaris_check) as wtp_inventaris_text",
                "CONCAT(istp.branch_name, ' ', istp.lokasi, ' ', istp.nama_ruang, ' ', istp.inventaris_check) as stp_ipal_inventaris_text",
                "CONCAT(ige.branch_name, ' ', ige.lokasi, ' ', ige.nama_ruang, ' ', ige.inventaris_check) as ge_inventaris_text",
                "CONCAT(iac.branch_name, ' ', iac.lokasi, ' ', iac.nama_ruang, ' ', iac.inventaris_check) as ac_inventaris_text",
                // "CONCAT(iprfgb.branch_name, ' ', iprfgb.lokasi, ' ', iprfgb.nama_ruang, ' ', iprfgb.inventaris_check) as prfgb_inventaris_text",
                "CONCAT(ilain.branch_name, ' ', ilain.lokasi, ' ', ilain.nama_ruang, ' ', ilain.inventaris_check) as lain_inventaris_text",
                "CONCAT(ivr.branch_name, ' ', ivr.lokasi, ' ', ivr.nama_ruang, ' ', ivr.inventaris_check) as vr_inventaris_text",
                "CONCAT(ivt.branch_name, ' ', ivt.lokasi, ' ', ivt.nama_ruang, ' ', ivt.inventaris_check) as vt_inventaris_text",
                "CONCAT(ivs.branch_name, ' ', ivs.lokasi, ' ', ivs.nama_ruang, ' ', ivs.inventaris_check) as vs_inventaris_text",
                "CONCAT(igr.branch_name, ' ', igr.lokasi, ' ', igr.nama_ruang, ' ', igr.inventaris_check) as gr_inventaris_text",
            ]);
        if($args['join_reply'] == true) {
            $r->join("($qReply) rply", 'rply.id_techreports = _.id', 'left')
                ->select([
                    'coalesce(rply.jumlah, 0) as jumlah_reply'
                ]);
        }

        if($args['join_receiver'] == true) {
            $r->join('techreports_receiver rcv', 'rcv.id_techreports = _.id', 'left')
                ->select('case when (rcv.is_view = 0 or rcv.is_view_comment = 0) then 0 else 1 end as is_view_rcv', false);
        }
        return $r;
    }

    function getReceived($id_user, $args) {
        $args['where']['rcv.id_receiver'] = $id_user;
        $args['where']['upper(_.status)'] = 'OPEN';
        $args['join_receiver'] = true;
        $limit = [];
        if(isset($args['limit'])) {
            $limit = $args['limit'];
            unset($args['limit']);
        }
        $sql = $this->queryReceived($args);
        $q = myquery($this->db->from("($sql) s1"), ['limit' => $limit, 'order' => [['last_update2', 'desc'], ['id', 'desc']]]);
        return $q->get()->result();
        // echo $q->get_compiled_select();
    }

    function getSent($id_user, $args) {
        $args['where']['_.from_id'] = $id_user;
        $args['where']['upper(_.status)'] = 'OPEN';
        $limit = [];
        if(isset($args['limit'])) {
            $limit = $args['limit'];
            unset($args['limit']);
        }

        $sql = $this->querySent($args);
        $q = myquery($this->db->from("($sql) s1"), ['limit' => $limit, 'order' => [['last_update2', 'desc'], ['id', 'desc']]]);
        return $q->get()->result();
        // echo $q->get_compiled_select();
    }

    function getArchived($id_user, $args) {
        $id_user = intval($id_user);
        $args['where']['upper(_.status)'] = 'CLOSE';
        $args['join_receiver'] = true;
        $limit = [];
        if(isset($args['limit'])) {
            $limit = $args['limit'];
            unset($args['limit']);
        }

        $sql = $this->queryArchived($id_user, $args);
        $q = myquery($this->db->from("($sql) s1"), ['limit' => $limit, 'order' => [ ['last_update2', 'desc'], ['id', 'desc'] ]]);
        $sql = $q->get_compiled_select();

        // echo $sql . "\n\n\n";
        return $this->db->query($sql)->result();
    }

    function save($data) {
        $receivers = [];
        if(array_key_exists('receivers', $data)) {
            $receivers = $data['receivers'];
            unset($data['receivers']);

            if(is_array($receivers) == false) {
                $receivers = [];
            }
        }

        $departments = [];
        if(array_key_exists('departments', $data)) {
            $departments = $data['departments'];
            unset($data['departments']);

            if(is_array($departments) == false) {
                $departments = [];
            }
        }

        $this->load->model('restapi/user_model');

        if(count($departments) > 0) {
            $users = $this->user_model->many_user(['where_in' => ['dep.id_department' => $departments]]);
            foreach ($users as $key => $user) {
                $receivers[] = $user['id_user'];
            }
        }

        if(array_key_exists('bagian', $data)) {
            $bagian = $data['bagian'];
            unset($data['bagian']);
            if(is_array($bagian) == false) {
                $bagian = [$bagian];
            }
            if(count($bagian) > 0) {
                $users = $this->user_model->many_user(['where_in' => ['bg.id' => $bagian]]);
                foreach ($users as $key => $usr) {
                    $receivers[] = $usr['id_user'];
                }
            }
        }

        $temp = [];
        foreach ($receivers as $key => $id_user) {
            $id_user = trim($id_user);
            if(empty($id_user)){
                continue;
            }
            if(in_array($id_user, $temp)){
                continue;
            }
            $temp[]=$id_user;
        }
        $receivers=$temp;

        if(count($receivers) == 0 ) {
            return ['status' => false, 'message' => 'Penerima kosong'];
        }
        $this->db->insert('techreports', $data);
        $id = $this->db->insert_id();

        $report = $this->one_report(['where' => ['_.id' => $id]]);
        if($report == null) {
            return ['status' => false, 'message' => 'Terjadi kesalahan menyimpan report', 'id' => $id, 'report' => $report];
        }

        $added = [];
        foreach ($receivers as $key => $rcv) {
            if(in_array($rcv, $added)) {
                continue;
            }

            $added[] = $rcv;

            $this->db->insert('techreports_receiver', [
                'id_techreports' => $id,
                'id_receiver' => $rcv,
                'is_view' => 0,
                'is_view_comment' => 0,
                'is_closed' => 0
            ]);
        }

		$this->load->model('restapi/user_model');
		$sender = $this->user_model->one_user(['where' => ['_.id_user' => $report->from_id]]);
		$sender_name = '';
		$position_name = '';
		$branch_name = '';
		if($sender != null) {
			$sender_name = $sender['name'];
			$position_name = $sender['position_name'];
			$branch_name = $sender['branch_name'];
		}

        $this->load->library('Notify_lib');
        $nl = new Notify_lib();
        $nl->send(
            'New Tech Report',
            //$report->author->name . ' has sent new Tech Report',
			$sender_name . ' - '.$position_name.' ('.$branch_name.') has sent new Tech Report',
            $report->from_id,
            $receivers,
            ['id' => $report->id],
            'techreports',
            true
        );

        return ['status' => true, 'message' => 'Data techreports berhasil disimpan', 'id' => $id];
    }

    function save_attachment($data) {
        $this->db->insert('techreports_attachment', $data);
        $id = $this->db->insert_id();
        return ['status' => true, 'id' => $id, 'message' => 'Suksess'];
    }

    function many_report($args) {
        $this->load->helper('myquery');
        return myquery($this->baseQuery($args), $args)->get()->result();
    }

    function one_report($args) {
        $this->load->helper('myquery');
        $report = myquery($this->baseQuery($args), $args)->get()->row();

        if($report != null) {
            $report->receivers = $this->getReceivers($report->id);
            $report->author = $this->getAuthor($report->from_id);
            $report->attachments = $this->getAttachments($report->id);
            $report->replies = $this->getReplies($report->id);
        }

        return $report;
    }

    function update_report($where, $data) {
        $target = $this->one_report(['where' => $where]);

        if($target == null) {
            return ['status' => false, 'message' => 'Report tidak ditemukan'];
        }

        $this->db->where(['id' => $target->id])->update('techreports', $data);

        return ['status' => true, 'message' => 'Report berhasil diupdate', 'id' => $target->id];
    }

    function baseQueryReply($args = []) {
        $q = $this->db->from('techreports_reply _')
            ->join('users usr', 'usr.id_user = _.sender')
            ->join('position pos', 'pos.id_position = usr.id_position')
            ->join('bagian bg', 'bg.id = usr.id_bagian')
            ->select([
                '_.*',
                'usr.call_name',
                'usr.name',
                'usr.image',
                'usr.username',
                'pos.name_position as position',
                'bg.name as bagian'
            ]);
        return $q;
    }

    function getReplies($id_report) {
        $q = $this->baseQueryReply()
            ->where(['_.id_techreports' => $id_report]);
        $res = $q->get()->result();

        foreach ($res as $key => $r) {
            $r->attachments = $this->get_reply_attachment($r->id);

            $res[$key] = $r;
        }

        return $res;
    }

    function getAttachments($id_report) {
        $q = $this->db->from('techreports_attachment')
            ->where(['techreports_id' => $id_report]);
        return $q->get()->result();
    }

    function getAuthor($id_user) {
        $q = $this->db->from('users')
            ->where(['id_user' => $id_user]);
        return $q->get()->row();
    }

    

    function getReceivers($id_generalreports)
	{
		$q = $this->db->from("techreports_receiver dc")
			->join("users usr", "usr.id_user = dc.id_receiver", "left")
			->where([
				"dc.id_techreports" => $id_generalreports
			])
			->select([
				"dc.is_view",
				'dc.is_view_comment',
				'usr.name',
				'usr.username',
				'usr.call_name',
				'usr.image',
				'usr.id_user'
			])
			->get()->result_array();

		return $q;
	}


    function hapus_report($where) {
        $targets = $this->many_report(['where' => $where]);

        if(count($targets) == 0) {
            return ['status' => false, 'message' => 'Data tidak ditemukan'];
        }

        foreach ($targets as $key => $report) {
            /* data dihapus dengan flag is_view=2*/
            $this->db->where(['id' => $report->id])->update('techreports', ['is_view' => 2]);
            // $this->db->where(['techreports_id' => $report->id])->delete('techreports_attachment');
            // $this->db->where(['id_techreports' => $report->id])->delete('techreports_receiver');
            // $this->db->where(['id_techreports' => $report->id])->delete('techreports_reply');
            // $this->db->where(['id_techreports' => $report->id])->delete('techreports_reply_attachment');
            // $this->hapus_reply(['_.id_techreports' => $report->id]);
            // $this->db->where(['id' => $report->id])->delete('techreports');
        }

        return ['status' => true, 'message' => 'Data berhasil dihapus'];
    }

    function save_reply($data) {
        if(isset($data['id_techreports']) == false) {
            return ['status' => false, 'message' => 'id report di perlukan'];
        }
        if(isset($data['sender']) == false) {
            return ['status' => false, 'message' => 'sender diperlukan'];
        }
        $report = $this->one_report(['where' => ['_.id' => $data['id_techreports']]]);
        if($report == null) {
            return ['status' => false, 'message' => 'report tidak dapat ditemukan'];
        }
        $this->db->insert('techreports_reply', $data);
        $id = $this->db->insert_id();

        if($id == null) {
            return ['status' => false, 'message' => 'Terjadi kesalahan menyimpan reply'];
        }

        $reply = $this->one_reply(['where' => ['_.id' => $id]]);
        if($reply == null) {
            return ['status' => false, 'message' => 'Terjadi kesalahan menyimpan reply'];
        }

        $receiversReport = $this->getReceivers($report->id);

        $receiversNotif = [];
        if($data['sender'] == $report->from_id) {

        } else {
            $receiversNotif[] = $report->from_id;
        }

        foreach ($receiversReport as $key => $rcv) {
            if($rcv->id_receiver == $data['sender']) {
                continue;
            }
            $receiversNotif[] = $rcv->id_receiver;
        }

		$this->load->model('restapi/user_model');
		$sender = $this->user_model->one_user(['where' => ['_.id_user' => $reply->sender]]);
		$sender_name = '';
		$position_name = '';
		$branch_name = '';
		if($sender != null) {
			$sender_name = $sender['name'];
			$position_name = $sender['position_name'];
			$branch_name = $sender['branch_name'];
		}

        /* kirim notifikasi */
        $this->load->library('Notify_lib');
        $nl = new Notify_lib();
        $nl->send(
            'New Techreport Reply',
            //$reply->name . ' has send new Techreport reply',
			$sender_name . ' - '.$position_name.' ('.$branch_name.') has send new Techreport reply',
            $reply->sender,
            $receiversNotif,
            ['id' => $report->id],
            'techreports',
            true
        );
        return ['status' => true, 'message' => 'Reply disimpan', 'id' => $id];
    }

    function update_reply($where, $data) {
        $target = $this->one_reply(['where' => $where]);
        if($target == null) {
            return ['status' => false, 'message' => 'Reply tidak ditemukan'];
        }

        $this->db->where(['id' => $target->id])->update('techreports_reply', $data);

        return ['status' => true, 'message' => 'Reply berhasil diupdate', 'id' => $target->id];
    }

    function many_reply($args) {
        $this->load->helper('myquery');
        return myquery($this->baseQueryReply($args), $args)->get()->result();
    }

    function one_reply($args) {
        $this->load->helper('myquery');
        return myquery($this->baseQueryReply($args), $args)->get()->row();
    }

    function hapus_reply($where) {
        $targets = $this->many_reply(['where' => $where]);
        if(count($targets) == 0) {
            return ['status' => false, 'message' => 'Reply tidak ditemukan'];
        }

        foreach ($targets as $key => $reply) {
            $this->db->where(['id_techreports_reply' => $reply->id])->delete('techreports_reply_attachment');
            $this->db->where(['id' => $reply->id])->delete('techreports_reply');
        }

        return ['status' => true, 'message' => 'Reply berhasil dihapus'];
    }

    function save_reply_attachment($data) {
        $this->db->insert('techreports_reply_attachment', $data);
        $id = $this->db->insert_id();

        return ['status' => true, 'message' => '', 'id' => $id];
    }

    function get_reply_attachment($id_reply) {
        $q = $this->db->from('techreports_reply_attachment')
            ->where(['id_techreports_reply' => $id_reply]);
        return $q->get()->result();
    }

    function mark_read($id_report, $id_user, $report = null) {
        if($report == null) {
            $report = $this->one_report(['where' => ['_.id' => $id_report]]);
        }

        if($report == null) {
            return ['status' => false, 'message' => 'Report tidak ditemukan'];
        }

        if($report->from_id == $id_user) {
            $this->db->where(['id' => $report->id])->where_in('is_view', [0,1])->update('techreports', ['is_view' => 1]);
        }
        // else {
        $this->db->where(['id_techreports' => $report->id, 'id_receiver' => $id_user, 'is_closed' => 0])
            ->update('techreports_receiver', ['is_view' => 1, 'is_view_comment' => 1]);
        // }
        $this->load->library('Notify_lib');
        $nl = new Notify_lib();
        $nl->mark_read($id_user, 'techreports', $report->id);

        return ['status' => true, 'message' => 'Report berhasil ditandai sudah dibaca'];
    }

    function auto_close($id_report, $report = null) {
        if($report == null) {
            $report = $this->one_report(['where' => ['_.id' => $id_report]]);
        }
        if($report == null) {
            return ['status' => false, 'message' => 'report is null'];
        }
        if(strtolower($report->status) == 'close'){
            return ['status' => false, 'message' => 'report already closed'];
        }
        $everythingOk = $report->lmp_des == '2' && $report->kf_des == '3'
                        && $report->lift_des == '4' && $report->ef_des == '5' && $report->ups_des == '6'
                        && $report->aq_des == '7' && $report->pmp_des == '8' && $report->pabx_des == '9'
                        && $report->wtp_des == '10' && $report->stp_ipal_des == '11'
                        && $report->ge_des == '12' && $report->ac_des == '13' && $report->lain_des == '14';
        if($everythingOk == false) {
            return ['status' => false, 'message' => 'Not closed, there is some issues'];
        }
        $allHaveRead = true;
        foreach ($report->receivers as $key => $rcv) {
            if($rcv->is_view == '0' || $rcv->is_view_comment == '0') {
                $allHaveRead = false;
                break;
            }
        }

        if($allHaveRead == false) {
            return ['status' => false, 'message' => 'Some receivers have not read'];
        }

        $this->update_report(['_.id' => $report->id], ['status' => 'Close']);
        return ['status' => true, 'message' => 'Report has been closed'];
    }

    function recent_report($id_user) {
        $id_user = intval($id_user);
        $type = "Tech Report";
        $sql = "    SELECT r.id, '$type Reply' as type_name, r.from_id as sender_id, r.last_update as last_update,
                        r.is_view
                    from techreports r
                    where r.from_id = '$id_user' and r.is_view = 0 and UPPER(r.status) = 'OPEN'

                    union

                    Select r.id, '$type' as type_name, r.from_id as sender_id, r.last_update as last_update,
                        case when rcv.is_view = 0 or rcv.is_view_comment = 0 then 0 else 1 end as is_view
                    from techreports_receiver rcv
                    join techreports r on rcv.id_techreports = r.id
                    where rcv.id_receiver = '$id_user' and UPPER(r.status) = 'OPEN' ";
        return $sql;
    }

}
