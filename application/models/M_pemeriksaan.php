<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_pemeriksaan extends CI_model
{

    function many_pemeriksaan($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        else{
            $t = '';
            foreach($args["where"] as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            //print_r($t);
            
            $args["where"] = $t;
        }
        if(isset($args["limit"]) == false) {
            $args["limit"] = [];
        }
        if(isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if(isset($args["order"]) == false) {
            $args["order"] = [];
        }
        
        $result = array();
        $row = $this->baseQueryPemeriksaan($args)->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();    
        $result = array_merge($result, ['rows' => $row]);
        $result['total'] = $this->baseQueryPemeriksaan($args)->where($args["where"])->get()->num_rows();
        
        return $result;
        
    }

    function baseQueryPemeriksaan($args = []) {
        return $this->db->from("pemeriksaan _")
            ->order_by($args["sort"],$args["order"])
            ->select([
                "_.*", 
            ]);
    }


    function many_history($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }

        if(isset($args["order"]) == false) {
            $args["order"] = [];
        }

        return $this->baseQueryHistory($args)->where($args["where"])->get()->result_array();
    }

    function baseQueryHistory($args = []) {
        return $this->db->from("pemeriksaan_history _")
            ->join("pemeriksaan p","p.id_pemeriksaan = _.id_pemeriksaan")
            ->join("users usr","usr.id_user = _.creator_id")
             ->order_by($args["sort"],$args["order"])
            ->select([
                "p.id_pemeriksaan", 
                "_.*",
                "DATE_FORMAT(_.created_at,'%d-%m-%y %h:%i:%s') AS created_at",
                "usr.name as creator_name",  
            ]);
    }

     function many_approve($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        return $this->baseQueryApprove($args)->where($args["where"])->get()->result_array();
    }

    function baseQueryApprove($args = []) {
        return $this->db->from("pemeriksaan_approve _")
            ->join("pemeriksaan p","p.id_pemeriksaan = _.id_pemeriksaan")
            ->join("users usr","usr.id_user = _.creator_id")
             ->order_by($args["sort"],$args["order"])
            ->select([
                "p.id_pemeriksaan", 
                "_.*",
                "DATE_FORMAT(_.created_at,'%d-%m-%y %h:%i:%s') AS created_at",
                "usr.name as creator_name",  
            ]);
    }

    function getDataPemeriksaan($params=[]){
        $this->db->from("pemeriksaan");
        $this->db->select("*");
        $this->db->where($params);
        $query = $this->db->get();
        return $query;
    }

    function add_input_pemeriksaan($data) {
        
        $this->db->insert("pemeriksaan", $data);
        $id = $this->db->insert_id();
        

        return ["status" => true, "message" => "Success"];
    }

    function add_input_history($data) {
        
        $this->db->insert("pemeriksaan_history", $data);
        $id = $this->db->insert_id();
        

        return ["status" => true, "message" => "Success"];
    }


    function update_input_pemeriksaan($where, $data) {
        $this->db->where($where)->update("pemeriksaan", $data);
        return ["status" => true, "message" => "Success"];
    }

    function update_input_pemeriksaan2($where, $data) {
        $this->db->where($where)->insert("pemeriksaan_history", $data);
        return ["status" => true, "message" => "Success"];
    }

    function delete_input_pemeriksaan($where){
        $del = $this->db->delete('pemeriksaan', $where);
        return ["status" => true, "message" => "Success"];
    }

    function delete_history_pemeriksaan($where){
        $del = $this->db->delete('pemeriksaan_history', $where);
        return ["status" => true, "message" => "Success"];
    }

    function insert_input_approve($where, $data) {
        $this->db->where($where)->insert("pemeriksaan_approve", $data);
        return ["status" => true, "message" => "Success"];
    }

	public function getGruopTest()
	{
		$this->db->select('distinct (group_test)');
		$this->db->from('pemeriksaan');
		return $this->db->get();
	}

	// export pemeriksaan
	public function export($group)
	{
		$this->db->select('*');
		$this->db->from('pemeriksaan');
		$this->db->where('group_test', $group);
		return $this->db->get();
	}

}
