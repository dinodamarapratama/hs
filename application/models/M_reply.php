<?php

class M_reply extends CI_Model {

    function baseQuery($args = []) {
        $q = $this->db->from('replies _')
            ->select([
                '_.*'
            ]);
        return $q;
    }

    function many($args = []) {
        $this->load->helper('myquery');
        return myquery($this->baseQuery($args), $args)->get()->result_array();
    }

    function one($args) {
        if(is_array($args) == false) {
            $args = [];
        }
        $args['limit'] = [1];
        $q = $this->many($args);
        if(count($q) > 0) {
            return $q[0];
        }
        return null;
    }

    function save_reply($sender, $table_fk, $id_fk, $message) {
        // echo "m reply:\n";
        // echo "sender: $sender\n";
        // echo "table: $table_fk\n";
        // echo "id: $id_fk\n";
        // echo "message: $message\n";
        // exit();
        $now = date('Y-m-d H:i:s');
        $this->db->insert('replies', [
            'id_fk' => $id_fk,
            'message' => $message,
            'table_fk' => $table_fk,
            'sender' => $sender,
            'created_at' => $now,
            'updated_at' => $now
        ]);
        $id = $this->db->insert_id();
        if(empty($id) ) {
            return ['status' => false, 'message' => 'ID hasil simpan tidak valid'];
        }
        return ['status' => true, 'message' => 'Reply berhasil disimpan'];
    }
}