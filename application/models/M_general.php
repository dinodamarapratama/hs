<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_general extends CI_model
{

    public function last_query_db(){
        return $this->db->last_query();
    }
    
    public function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    public function insert_batch($table, $data)
    {
        $this->db->insert_batch($table, $data);
    }

    public function get_data_like($where, $colum, $table)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->like($colum, $where);
        return $this->db->get();
    }

    public function get_data_join()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('position', 'users.id_position = position.id');
        return $this->db->get();
    }

    public function get_sent_to($id)
    {
        $this->db->select('name, (select name_position from position where users.id_position = position.id_position) as name_position, id_receiver');
        $this->db->from('generalreports_receiver');
        $this->db->join('users', 'generalreports_receiver.id_receiver = users.id_user');
        //$this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('generalreports_receiver.id_receiver', $id);

        return $this->db->get();
		
    }
	
	public function get_all_sent_to($id)
    {
        $this->db->select('name, (select name_position from position where users.id_position = position.id_position) as name_position, id_receiver');
        $this->db->from('generalreports_receiver');
        $this->db->join('users', 'generalreports_receiver.id_receiver = users.id_user');
        //$this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('generalreports_receiver.id_generalreports', $id);

        return $this->db->get();

    }

	public function get_sent_to_by_id($id)
    {
        $this->db->select('name, (select name_position from position where users.id_position = position.id_position) as name_position, id_receiver');
        $this->db->from('generalreports_receiver');
        $this->db->join('users', 'generalreports_receiver.id_receiver = users.id_user');
        //$this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('generalreports_receiver.id_generalreports', $id);

        return $this->db->get();
		
    }

    public function get_sent_count($id)
    {
        $this->db->from('generalreports_receiver');
        $this->db->where('generalreports_receiver.id_generalreports', $id);

        return $this->db->get()->num_rows();
    }

    public function get_read_count($id)
    {
        $this->db->from('generalreports_receiver');
        $this->db->where('generalreports_receiver.id_generalreports', $id);
        $this->db->where('generalreports_receiver.is_view', '1');

        return $this->db->get()->num_rows();
    }

    public function get_user_read($id)
    {
        $this->db->select('name, (select name_position from position where users.id_position = position.id_position) as name_position, id_receiver, is_view');
        $this->db->from('generalreports_receiver');
        $this->db->join('users', 'generalreports_receiver.id_receiver = users.id_user');
        //$this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('generalreports_receiver.id_generalreports', $id);

        return $this->db->get();

    }

    public function total_data_received($id, $id_generalreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('generalreports.id');
        $this->db->from('generalreports');
        //$this->db->join('generalreports_receiver', 'generalreports_receiver.id_generalreports = generalreports.id');
        $this->db->join('users', 'generalreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($id_generalreports)) {
            //$this->db->where('generalreports_receiver.id_generalreports', $id_generalreports);
			$this->db->where('generalreports.id', $id_generalreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
			$this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('generalreports.kategori',$search,'both');
            $this->db->or_like('generalreports.title',$search,'both');
            $this->db->or_like('generalreports.date',$search,'both');
            $this->db->or_like('generalreports.description',$search,'both');
            //$this->db->limit(1);
			$this->db->group_end();
        }
		$this->db->where('generalreports.id in (select generalreports_receiver.id_generalreports from generalreports_receiver where id_receiver = '.$id.' and generalreports_receiver.is_closed="0")', null);
        //$this->db->where('generalreports_receiver.id_receiver', $id);
        /* data belum dihapus oleh receiver */
        //$this->db->where('generalreports_receiver.is_closed', '0');
        $this->db->where('upper(generalreports.status)', 'OPEN');
        /* data belum dihapus oleh creator */
        $this->db->where_in('generalreports.is_view', [0,1]);
        return $this->db->get();
    }
	
    public function total_data_sent($id, $id_generalreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('generalreports.id');
        $this->db->from('generalreports');
        //$this->db->join('generalreports_receiver', 'generalreports_receiver.id_generalreports = generalreports.id');
        $this->db->join('users', 'generalreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position', 'left');
        $this->db->join('branch', 'users.branch_id = branch.branch_id', 'left');
        if (!empty($id_generalreports)) {
            //$this->db->where('generalreports_receiver.id_generalreports', $id_generalreports);
			$this->db->where('generalreports.id', $id_generalreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
			$this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('generalreports.kategori',$search,'both');
            $this->db->or_like('generalreports.title',$search,'both');
            $this->db->or_like('generalreports.date',$search,'both');
            $this->db->or_like('generalreports.description',$search,'both');
            //$this->db->limit(1);
			$this->db->group_end();
        }
        $this->db->where('generalreports.from_id', $id);
        $this->db->where('generalreports.id_draft', '0');
        $this->db->where('upper(generalreports.status)', 'OPEN');
        /* data belum dihapus oleh creator */
        $this->db->where_in('generalreports.is_view', [0,1]);
        return $this->db->get();
    }

    public function total_data_archived($id, $id_generalreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('generalreports.id');
        $this->db->from('generalreports');
        //$this->db->join('generalreports_receiver', 'generalreports_receiver.id_generalreports = generalreports.id');
        $this->db->join('users', 'generalreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($id_generalreports)) {
            //$this->db->where('generalreports_receiver.id_generalreports', $id_generalreports);
			$this->db->where('generalreports.id', $id_generalreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('generalreports.kategori',$search,'both');
            $this->db->or_like('generalreports.title',$search,'both');
            $this->db->or_like('generalreports.date',$search,'both');
            $this->db->or_like('generalreports.description',$search,'both');
            //$this->db->limit(1);
            $this->db->group_end(); 
        }
        
         $this->db->where('upper(generalreports.status)', 'CLOSE');
        // $this->db->where('generalreports.status <>', 'Open');
        // $this->db->where('generalreports.is_view <>', '1');
        $this->db->group_start();
            $this->db->or_group_start();
                $this->db->where('generalreports.from_id', $id);
                /* data belum dihapus oleh creator */
                $this->db->where_in('generalreports.is_view', [0,1]);
            $this->db->group_end();
            /* data belum dihapus oleh receiver */
            /*$this->db->or_group_start();
                $this->db->where('generalreports_receiver.id_receiver', $id);
                $this->db->where('generalreports_receiver.is_closed', '0');
            $this->db->group_end();*/
			
			$this->db->or_group_start();
                $this->db->where('generalreports.id in (select id_generalreports from generalreports_receiver where generalreports_receiver.id_generalreports = generalreports.id and generalreports_receiver.id_receiver = '.$id.' and generalreports_receiver.is_closed="0") ', null);
            $this->db->group_end();
			
        $this->db->group_end();
        return $this->db->get();
    }

    public function total_data_draft($id, $id_generalreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('generalreports.id');
        $this->db->from('generalreports');
        //$this->db->join('generalreports_receiver', 'generalreports_receiver.id_generalreports = generalreports.id','left');
        $this->db->join('users', 'generalreports.from_id = users.id_user','left');
        $this->db->join('position', 'users.id_position = position.id_position','left');
        $this->db->join('branch', 'users.branch_id = branch.branch_id','left');
        if (!empty($id_generalreports)) {
            //$this->db->where('generalreports_receiver.id_generalreports', $id_generalreports);
             $this->db->where('generalreports.id', $id_generalreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->group_start();
			$this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('generalreports.kategori',$search,'both');
            $this->db->or_like('generalreports.title',$search,'both');
            $this->db->or_like('generalreports.date',$search,'both');
            $this->db->or_like('generalreports.description',$search,'both');
            //$this->db->limit(1);
            $this->db->group_end();
        }
        $this->db->where('generalreports.from_id', $id);
        // $this->db->where('generalreports.is_view <>', '1');
        $this->db->where('generalreports.id_draft <>', '0');
        $this->db->where('generalreports.id_draft', '1');
        return $this->db->get();
    }

    public function get_data_received($id, $id_generalreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('generalreports.*,generalreports.status as status,users.*,position.*,branch.*,
            (select generalreports_receiver.is_view from generalreports_receiver where generalreports_receiver.id_generalreports = generalreports.id and generalreports_receiver.id_receiver = '.$id.' group by generalreports_receiver.id_receiver) as `read`,
			generalreports.status as status');//generalreports_receiver.id_generalreports,generalreports_receiver.id_receiver,generalreports_receiver.is_view as read,
        $this->db->from('generalreports');
        //$this->db->join('generalreports_receiver', 'generalreports_receiver.id_generalreports = generalreports.id');
        $this->db->join('users', 'generalreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($id_generalreports)) {
            //$this->db->where('generalreports_receiver.id_generalreports', $id_generalreports);
			$this->db->where('generalreports.id', $id_generalreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
			$this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('generalreports.kategori',$search,'both');
            $this->db->or_like('generalreports.title',$search,'both');
            $this->db->or_like('generalreports.date',$search,'both');
            $this->db->or_like('generalreports.description',$search,'both');
            //$this->db->limit(1);
			$this->db->group_end();
        }
		
		$this->db->where('generalreports.id in (select generalreports_receiver.id_generalreports from generalreports_receiver where id_receiver = '.$id.' and generalreports_receiver.is_closed="0")', null);
        //$this->db->where('generalreports_receiver.id_receiver', $id);
        /* data belum dihapus oleh receiver */
        //$this->db->where('generalreports_receiver.is_closed', '0');
        $this->db->where('upper(generalreports.status)', 'OPEN');
        /* data belum dihapus oleh creator */
        $this->db->where_in('generalreports.is_view', [0,1]);
        // $this->db->where('generalreports.status <>', 'Close');
        //$this->db->order_by('generalreports_receiver.id', 'desc');
		$this->db->order_by('generalreports.id', 'desc');
		
		$limit = 10;
		$offset = 0;
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		if(isset($args['offset'])) {
			$offset = $args['offset'];
			unset($args['offset']);
		}
		
		
		$this->db->limit($limit,$offset);
        return $this->db->get();
    }

    public function get_data_sent_old($id, $id_generalreports = null, $position = null, $search = null)
    {
        $this->db->select('generalreports.*,usr.id_user,usr.name,usr.username,usr.image,usr.position_name,usr.id_position,usr.branch_name,usr.bagian_name,usr.id_bagian,
		(select department_name from v_user where id_user=generalreports.from_id limit 1) as department_name,
		(select id_department from v_user where id_user=generalreports.from_id limit 1) as id_department,
            generalreports_receiver.id_generalreports,generalreports_receiver.id_receiver,generalreports.status as status');
        $this->db->from('generalreports');
        $this->db->join('generalreports_receiver', 'generalreports_receiver.id_generalreports = generalreports.id', 'left');
        $this->db->join('v_user usr', 'generalreports.from_id = usr.id_user', 'left');
		
		$this->db->group_by("generalreports_receiver.id_receiver");
		
        if (!empty($id_generalreports)) {
            $this->db->where('generalreports.id', $id_generalreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->like('usr.name',$search,'both');
            $this->db->or_like('usr.username',$search,'both');
            $this->db->or_like('generalreports.kategori',$search,'both');
            $this->db->or_like('generalreports.title',$search,'both');
            $this->db->or_like('generalreports.date',$search,'both');
            $this->db->or_like('generalreports.description',$search,'both');
            $this->db->limit(1);
        }
        $this->db->where('generalreports.from_id', $id);
        $this->db->where('generalreports.id_draft', '0');
        $this->db->where('upper(generalreports.status)', 'OPEN');
      
        /* data belum dihapus oleh creator */
        $this->db->where_in('generalreports.is_view', [0,1]);
        $this->db->order_by('generalreports.id', 'desc');
        // echo $this->db->get_compiled_select();
        // exit();
        return $this->db->get();
    }

     public function get_data_sent($id, $id_generalreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('generalreports.*,users.*,position.*,branch.*,generalreports.status as status,generalreports.id as id_generalreports');//generalreports_receiver.id_generalreports,generalreports_receiver.id_receiver,
        $this->db->from('generalreports');
        //$this->db->join('generalreports_receiver', 'generalreports_receiver.id_generalreports = generalreports.id');
        $this->db->join('users', 'generalreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position', 'left');
        $this->db->join('branch', 'users.branch_id = branch.branch_id', 'left');
        if (!empty($id_generalreports)) {
            //$this->db->where('generalreports_receiver.id_generalreports', $id_generalreports);
			$this->db->where('generalreports.id', $id_generalreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
			$this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('generalreports.kategori',$search,'both');
            $this->db->or_like('generalreports.title',$search,'both');
            $this->db->or_like('generalreports.date',$search,'both');
            $this->db->or_like('generalreports.description',$search,'both');
            //$this->db->limit(1);
			$this->db->group_end();
        }
        $this->db->where('generalreports.from_id', $id);
        $this->db->where('generalreports.id_draft', '0');
        $this->db->where('upper(generalreports.status)', 'OPEN');
        /* data belum dihapus oleh creator */
        $this->db->where_in('generalreports.is_view', [0,1]);
        // $this->db->where('generalreports.status <>', 'Close');
        // $this->db->where('generalreports.id_draft <>', '1');
        // $this->db->where('generalreports.is_view <>', '1');
        $this->db->order_by('generalreports.id', 'desc');
		
		$limit = 10;
		$offset = 0;
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		if(isset($args['offset'])) {
			$offset = $args['offset'];
			unset($args['offset']);
		}
		
		
		$this->db->limit($limit,$offset);
        return $this->db->get();
    }

    public function get_data_archived($id, $id_generalreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('generalreports.*,users.*,position.*,branch.*,
            generalreports.status as status,generalreports.id as id_generalreports');//generalreports_receiver.id_generalreports,generalreports_receiver.id_receiver,
        $this->db->from('generalreports');
        //$this->db->join('generalreports_receiver', 'generalreports_receiver.id_generalreports = generalreports.id');
        $this->db->join('users', 'generalreports.from_id = users.id_user');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->join('branch', 'users.branch_id = branch.branch_id');
        if (!empty($id_generalreports)) {
            //$this->db->where('generalreports_receiver.id_generalreports', $id_generalreports);
			$this->db->where('generalreports.id', $id_generalreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
            $this->db->group_start();
            $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('generalreports.kategori',$search,'both');
            $this->db->or_like('generalreports.title',$search,'both');
            $this->db->or_like('generalreports.date',$search,'both');
            $this->db->or_like('generalreports.description',$search,'both');
            //$this->db->limit(1);
            $this->db->group_end(); 
        }
        
         $this->db->where('upper(generalreports.status)', 'CLOSE');
        // $this->db->where('generalreports.status <>', 'Open');
        // $this->db->where('generalreports.is_view <>', '1');
        $this->db->group_start();
            $this->db->or_group_start();
                $this->db->where('generalreports.from_id', $id);
                /* data belum dihapus oleh creator */
                $this->db->where_in('generalreports.is_view', [0,1]);
            $this->db->group_end();
           /* data belum dihapus oleh receiver */
            /*$this->db->or_group_start();
                $this->db->where('generalreports_receiver.id_receiver', $id);
                $this->db->where('generalreports_receiver.is_closed', '0');
            $this->db->group_end();*/
			
			$this->db->or_group_start();
                $this->db->where('generalreports.id in (select id_generalreports from generalreports_receiver where generalreports_receiver.id_generalreports = generalreports.id and generalreports_receiver.id_receiver = '.$id.' and generalreports_receiver.is_closed="0") ', null);
            $this->db->group_end();
			
        $this->db->group_end();
        $this->db->order_by('generalreports.id', 'desc');
		
		$limit = 10;
		$offset = 0;
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		if(isset($args['offset'])) {
			$offset = $args['offset'];
			unset($args['offset']);
		}
		
		
		$this->db->limit($limit,$offset);
        return $this->db->get();
    }

    public function get_data_draft($id, $id_generalreports = null, $position = null, $search = null, $args)
    {
        $this->db->select('generalreports.*,users.*,position.*,branch.*,
            generalreports.id as id_generalreports,generalreports.status as status');//generalreports_receiver.id_generalreports,generalreports_receiver.id_receiver
        $this->db->from('generalreports');
        //$this->db->join('generalreports_receiver', 'generalreports_receiver.id_generalreports = generalreports.id','left');
        $this->db->join('users', 'generalreports.from_id = users.id_user','left');
        $this->db->join('position', 'users.id_position = position.id_position','left');
        $this->db->join('branch', 'users.branch_id = branch.branch_id','left');
        if (!empty($id_generalreports)) {
            //$this->db->where('generalreports_receiver.id_generalreports', $id_generalreports);
             $this->db->where('generalreports.id', $id_generalreports);
        }
        if (!empty($position)) {
            $this->db->where('position.id_position',$position);
        }
        if (!empty($search)) {
			$this->db->group_start();
           $this->db->like('users.name',$search,'both');
            $this->db->or_like('users.username',$search,'both');
            $this->db->or_like('generalreports.kategori',$search,'both');
            $this->db->or_like('generalreports.title',$search,'both');
            $this->db->or_like('generalreports.date',$search,'both');
            $this->db->or_like('generalreports.description',$search,'both');
            //$this->db->limit(1);
            $this->db->group_end();
        }
        $this->db->where('generalreports.from_id', $id);
        // $this->db->where('generalreports.is_view <>', '1');
        $this->db->where('generalreports.id_draft <>', '0');
        $this->db->where('generalreports.id_draft', '1');
        $this->db->order_by('generalreports.id', 'desc');
		
		$limit = 10;
		$offset = 0;
		if(isset($args['limit'])) {
			$limit = $args['limit'];
			unset($args['limit']);
		}
		if(isset($args['offset'])) {
			$offset = $args['offset'];
			unset($args['offset']);
		}
		
		
		$this->db->limit($limit,$offset);
        return $this->db->get();
    }

    public function get_user_replay($id)
    {
        $this->db->select('*');
        $this->db->from('generalreports');
        $this->db->join('generalreports_reply', 'generalreports.id = generalreports_reply.id_generalreports');
        $this->db->join('users', 'generalreports_reply.sender = users.id_user');
        $this->db->join('branch', 'branch.branch_id = users.branch_id');
        $this->db->join('position', 'position.id_position = users.id_position');
        $this->db->where('generalreports.id', $id);

        return $this->db->get();
    }

    public function get_to_id($id,$user_id)
    {
        $this->db->select('*');
        $this->db->from('generalreports_receiver');
        $this->db->join('generalreports', 'generalreports.id = generalreports_receiver.id_generalreports');
        $this->db->where('generalreports.id', $id);
        $this->db->where('generalreports_receiver.id_receiver', $user_id);

        return $this->db->get();
    }

    public function get_from_id($id)
    {
        $this->db->select('*');
        $this->db->from('generalreports_receiver');
        $this->db->join('generalreports', 'generalreports.id = generalreports_receiver.id_generalreports');
        $this->db->where('generalreports.id', $id);

        return $this->db->get();
    }

    public function get_notification($id)
    {
        return $this->db->from("notifications")
            ->where([
                "to_id" => $id
            ])
            ->group_start()
                ->or_where(["read" => null])
                ->or_where(["read" => 0])
            ->group_end()
            // ->where(["anime" => "naruto"])
            ->get();
        
        // $this->db->select('*');
        // $this->db->from('notifications');
        // $this->db->where('to_id', $id);
        // $this->db->where('read', 0);
        // $this->db->where('read <>', 1);
        // $this->db->order_by('id', 'desc');
        
        // return $this->db->get();
    }


    public function get_user_hrd()
    {
        $this->db->select('id_user,name');
        $this->db->from('users');
        $this->db->join('position', 'users.id_position = position.id_position');
        $this->db->where('name_position','STAFF HRD');

        return $this->db->get();
    } 

    public function testing($array)
    {
        return print("<pre>" . print_r($array, true) . "</pre>");
    }
	
	function load_data($params=[], $rows=0, $offset=10,$all=0,$table,$id,$order="desc")
   {
      $this->db->select(['*']);
      $this->db->from($table);
      if (isset($params['in_param'])) {
        foreach ($params['in_param'] as $key => $value) {
            $this->db->where_in($key,$value);  
        }
        unset($params['in_param']);
      }
      $this->db->where($params);
      if (!$all) {
        $this->db->limit($rows, $offset);  
      }
      $this->db->order_by($id, $order);
      //$this->db->join('branch', 'users.branch_id = branch.branch_id');
      $query = $this->db->get();
      return $query->result_array();
   }
	
	function total_load_data($params=[],$table)
   {
      $this->db->select(['id']);
      $this->db->from($table);
      if (isset($params['in_param'])) {
        foreach ($params['in_param'] as $key => $value) {
            $this->db->where_in($key,$value);  
        }
        unset($params['in_param']);
      }
      $this->db->where($params);
      $query = $this->db->get();
      return $query->num_rows();
   }


   function detail($id)
    {
        $generalreports = $this->db->where('id', $id)->get('generalreports')->row();

        $this->db->select('users.*, position.name_position as position, position.id_department,branch.*,branch.branch_name as branch');
        $this->db->join('position', 'position.id_position=users.id_position', 'left');
        $this->db->join('branch', 'branch.branch_id=users.branch_id');

        $generalreports->author = $this->db->where('users.id_user', $generalreports->from_id)->get('users')->row();


     //   $attributes = $this->db->where('complaint_id', $complaint->id)->get('complaint_attributes')->result();
     //   $complaint->attr = new stdClass();
     //   foreach ($attributes as $value) {
     //       $complaint->attr->{$value->key} = $value->value;
     //   }
        
        $generalreports->receivers = $this->getReceivers($complaint->id);

        $this->db->select('users.*');
        $this->db->select('position.name_position as position, position.id_department');
        $this->db->select('bagian.name as bagian');
        $this->db->select('generalreports_reply.*');
        $this->db->select('users.name as reply_name');
        $this->db->join('users', 'generalreports_reply.sender=users.id_user');
        $this->db->join('position', 'position.id_position=users.id_position', 'left');
        $this->db->join('bagian', 'bagian.id=users.id_bagian', 'left');
        $this->db->where('generalreports_reply.id_generalreports', $generalreports->id);
        $generalreports->reply = $this->db->get('generalreports_reply')->result();

      //  $complaint->attachments = $this->db->where('complaint_id', $id)->get('complaint_attachment')->result();
        return $generalreports;
    }

    function getReceivers($id) {
        $q = $this->db->from('generalreports_receiver _')
            ->join('users usr', 'usr.id_user = _.id_receiver', 'left')
            ->join('position p', 'p.id_position = usr.id_position', 'left')
            ->join('departments dep', 'dep.id_department = p.id_department', 'left')
            ->where(['_.id_generalreports' => $id])
            ->select([
                '_.is_view',
                'usr.name',
                'usr.username',
                'p.name_position as position',
                'dep.name as department',
                'usr.id_user'
            ]);
        return $q->get()->result();
    }

    function excel_it($startDate, $endDate)
    {
        $this->db->select('*,usr.name,usr.last_name,br.branch_name as branch');
        $this->db->from('generalreports _');
        $this->db->join('users usr', 'usr.id_user = _.from_id', 'left');
        $this->db->join('branch br', 'br.branch_id = usr.branch_id', 'left');
        $this->db->where('kategori ="IT"');
        $this->db->where('date >=', $startDate);
        $this->db->where('date <=', $endDate);
        return $this->db->get()->result();
    }
	
}