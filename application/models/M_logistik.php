<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_logistik extends CI_model
{
	function __construct()
    {
        parent::__construct();

        $this->tb_suratJalan                = "admin_logistik.sj";
        $this->tb_unit                      = "admin_logistik.unit";
    }

    function many_sb($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        else{
            $t = '';
            foreach($args["where"] as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            //print_r($t);

            $args["where"] = $t;
        }
        if(isset($args["limit"]) == false) {
            $args["limit"] = [];
        }
        if(isset($args["start"]) == false) {
            $args["start"] = [];
        }
        if(isset($args["order"]) == false) {
            $args["order"] = [];
        }

        $result = array();
        $row = $this->baseQuerySb($args)->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();
        $result = array_merge($result, ['rows' => $row]);
        $result['total'] = $this->baseQuerySb($args)->where($args["where"])->get()->num_rows();

        return $result;

    }

    function baseQuerySb($args = []) {
        return $this->db->from("admin_logistik.parameter _")
            ->where("PARAMGROUP","SATUAN")
            ->order_by("_.id","desc")
            ->select([
                "_.*",
            ]);
    }



    function add_input_sb($data) {

        $this->db->insert("admin_logistik.parameter", $data);
        $id = $this->db->insert_id();


        return ["status" => true, "message" => "Success"];
    }



    public function update_input_sb($data_received, $where)
    {
        $data['PARAMGROUP']=$data_received['PARAMGROUP'];
        $data['PARAMNAME']=$data_received['PARAMNAME'];
        $data['PARAMVALUE1']=$data_received['PARAMVALUE1'];
        $data['PARAMVALUE2']=$data_received['PARAMVALUE2'];


        $this->db->where($where);
        $result = $this->db->update("admin_logistik.parameter", $data);

        $response = [
            'success' => true,
            'message' => '',
        ];

        if($result){
            $response['success'] = true;
            $response['message'] = "Data Diubah";
        }else{
            $response['success'] = false;
            $response['message'] = "Gagal";
        }
        return $response;
    }

    function selectUnitWithFilter($where = null){
        if($where != null) {
            $t = '';
            foreach($where as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            //print_r($t);                        
            $this->db->where($t);
        }
        
        return $this->db->get('unit');
    }
 //   function del_sb($where) {
 //       $targets = $this->many_sb(["where" => $where]);
 //       if(count($targets) == 0) {
 //           return ["status" => false, "message" => "Data tidak ditemukan"];
 //       }
 //       foreach ($targets as $key => $t) {
 //           $this->db->where(["id" => $t["id"]])->delete("admin_logistik.parameter");
 //       }
 //       return ["status" => true, "message" => "Success"];
 //   }

    public function del_sb($where)
    {
        $response = [
            'success' => true,
            'message' => '',
        ];

        $this->db->where($where);
        $result = $this->db->delete("admin_logistik.parameter");

        if($result){
            $response['success'] = true;
            $response['message'] = "Data dihapus";
        }else{
            $response['success'] = false;
            $response['message'] = "Gagal";
        }

        return $response;
    }

    function many_dr($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        } else {
            $t = '';
            foreach($args["where"] as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            $args["where"] = $t;
        }

        if(isset($args["limit"]) == false) {
            $args["limit"] = [];
        }
        if(isset($args["start"]) == false) {
            $args["start"] = [];
        }
        return $this->baseQueryDr($args)->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();
    }

    function baseQueryDr($args = []) {
        return $this->db->from("admin_logistik.supplier s")
            ->join("admin_logistik.suppliergroup sgp", "sgp.SUPPLIERID = s.SupplierID", "left")
            ->group_by("s.SupplierID")
            ->select([
                "s.*",
                "GROUP_CONCAT(sgp.KELOMPOK) KELOMPOK"
            ]);
    }

    function many_pdf_dr($args = []) {
       // $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }

        return $this->baseQueryPdfDr()->get()->result();
	}

	function baseQueryPdfDr($args = []) {
        return $this->db->from("admin_logistik.supplier _")
            ->join("(SELECT SUPPLIERID, GROUP_CONCAT(KELOMPOK) KELOMPOK FROM admin_logistik.suppliergroup GROUP BY SUPPLIERID ) sgp", "sgp.SUPPLIERID = _.SupplierID", "left")
            ->where("_.isActive","1")
            ->select([
                "_.SupplierID",
                "_.NamaPT",
                "_.Alamat",
                "_.NPWP",
                "_.CONTACT1",
                "_.CONTACT2",
                "_.NoTelp",
                "_.NOHP",
                "_.NoFax",
                "_.Email",
                "sgp.KELOMPOK as KELOMPOK"

            ]);
    }
	

	//konversi
	function many_pdf_ks($args = []) {
		// $this->load->helper("myquery");
		 if($args == null) {
			 $args = [];
		 }
 
		 return $this->baseQueryPdfKs()->get()->result();
	 }

	 function baseQueryPdfKs($args = []) {
		return $this->db->from("admin_logistik.konversi k")
		->join("admin_logistik.msbarang b", "b.KodeBarang = k.KodeBesar")
		->join("admin_logistik.msbarang l", "l.KodeBarang = k.KodeKecil")
		->select('b.KodeBarang as KodeBarangBesar,b.NamaBarang as NamaBarangBesar,b.NoKatalog as NoKatalogBesar,b.Satuan as SatuanBesar,b.MinStock as MinStockBesar
		,l.KodeBarang as KodeBarangKecil,l.NamaBarang as NamaBarangKecil,l.NoKatalog as NoKatalogKecil,l.Satuan as SatuanKecil,k.Konversi as Konversi');
    }


   

    public function get_master_gudang($args = [])
	{
		$this->load->helper("myquery");
		if($args == null) {
			$args = [];
		}
		if(isset($args["where"]) == false) {
			$args["where"] = [];
        } else {
            $t = '';
            foreach($args["where"] as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            //print_r($t);

            $args["where"] = $t;
        }


		if(isset($args["limit"]) == false) {
			$args["limit"] = [];
		}
		if(isset($args["start"]) == false) {
			$args["start"] = [];
		}

		return $this->baseQueryMG($args)
			->where($args["where"])
			->order_by($args["sort"], $args["order"])
			->limit($args["limit"], $args["start"])
			->get()->result_array();
    }

    public function simpan_master_gudang($data_received)
	{
		$data['KODEUNIT']=$data_received['KODEUNIT'];
		$data['NAMAUNIT']=$data_received['NAMAUNIT'];
		$data['KODEINDUK']=$data_received['KODEINDUK'];
		$data['UNITLEVEL']=$data_received['UNITLEVEL'];
		$data['SINGKATAN']=$data_received['SINGKATAN'];
		$data['AKTIF']="b'1'";
		$data['PIC']=$data_received['PIC'];
		$data['CODE1']=$data_received['CODE1'];
		$data['CODE2']=$data_received['CODE2'];
		$data['FIXLEVEL']=$data_received['FIXLEVEL'];

		$result = $this->db->insert("admin_logistik.unit", $data);

		$response = [
			'success' => true,
			'message' => '',
		];

		if($result){
			$response['success'] = true;
			$response['message'] = "Data ditambahkan";
		}else{
			$response['success'] = false;
			$response['message'] = "Gagal";
		}
		return $response;
    }

    public function update_master_gudang($data_received, $where)
	{
		$data['KODEUNIT']=$data_received['KODEUNIT'];
		$data['NAMAUNIT']=$data_received['NAMAUNIT'];
		$data['KODEINDUK']=$data_received['KODEINDUK'];
		$data['UNITLEVEL']=$data_received['UNITLEVEL'];
		$data['SINGKATAN']=$data_received['SINGKATAN'];
		$data['AKTIF']="b'1'";
		$data['PIC']=$data_received['PIC'];
		$data['CODE1']=$data_received['CODE1'];
		$data['CODE2']=$data_received['CODE2'];
		$data['FIXLEVEL']=$data_received['FIXLEVEL'];

		$this->db->where($where);
		$result = $this->db->update("admin_logistik.unit", $data);

		$response = [
			'success' => true,
			'message' => '',
		];

		if($result){
			$response['success'] = true;
			$response['message'] = "Data Diubah";
		}else{
			$response['success'] = false;
			$response['message'] = "Gagal";
		}
		return $response;
	}

    public function update_aktif_master_gudang($data_received, $where)
	{
		$data['AKTIF']=$data_received['AKTIF'];

		$this->db->where($where);
		$result = $this->db->update("admin_logistik.unit", $data);
        // print_r($this->db->last_query());
		$response = [
			'success' => true,
			'message' => '',
		];

		if($result){
			$response['success'] = true;
			$response['message'] = "Data Diubah";
		}else{
			$response['success'] = false;
			$response['message'] = "Gagal";
		}
		return $response;
	}

	public function hapus_master_gudang($where)
	{
		$response = [
			'success' => true,
			'message' => '',
		];

		$this->db->where($where);
		$result = $this->db->delete("admin_logistik.unit");

		if($result){
			$response['success'] = true;
			$response['message'] = "Data dihapus";
		}else{
			$response['success'] = false;
			$response['message'] = "Gagal";
		}

		return $response;
	}

	public function get_master_barang($args = [], $getdata)
	{
		$this->load->helper("myquery");
		if($args == null) {
			$args = [];
		}
		if(isset($args["where"]) == false) {
			$args["where"] = [];
        } else {
            $t = '';
            foreach($args["where"] as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            //print_r($t);

            $args["where"] = $t;
        }


		if(isset($args["limit"]) == false) {
			$args["limit"] = [];
		}
		if(isset($args["start"]) == false) {
			$args["start"] = [];
		}

        if($getdata == 1){
		return $this->baseQueryMB($args)
			->where($args["where"])
			->order_by($args["sort"], $args["order"])
			->limit($args["limit"], $args["start"])
             ->get()->result_array();
        }elseif($getdata == 0){
            return $this->baseQueryMB($args)
			->where($args["where"])
			->order_by($args["sort"], $args["order"])
            ->get()->num_rows();
        }
    }


    public function get_master_barang_filter($args = [],$getdata)
	{
		$this->load->helper("myquery");
		if($args == null) {
			$args = [];
        }
        
        $kodeBarang = $args['katid'];
        $likename   = $args['likename'];
		
        if($getdata == 1){
		return $this->baseQueryMB($args)
			 ->where(array("m.KodeBarang LIKE"=> $kodeBarang.'%',"m.NamaBarang LIKE"=>'%'.$likename.'%'))
			 ->get()->result_array();
        }elseif($getdata == 0){
            return $this->baseQueryMB($args)
            ->where(array("m.KodeBarang LIKE"=> $kodeBarang.'%',"m.NamaBarang LIKE"=>'%'.$likename.'%'))
			->get()->num_rows();
        }
    }
    
   

	public function simpan_master_barang($data_received)
	{
		$data['KodeBarang']=$data_received['KodeBarang'];
		$data['NamaBarang']=$data_received['NamaBarang'];
		$data['NoKatalog']=$data_received['NoKatalog'];
		$data['Satuan']=$data_received['Satuan'];
		$data['SupplierID']=$data_received['distributor'];
		$data['BAGIAN']=$data_received['BAGIAN'];

		$result = $this->db->insert("admin_logistik.msbarang", $data);

		$response = [
			'success' => true,
			'message' => '',
		];

		if($result){
			$response['success'] = true;
			$response['message'] = "Data ditambahkan";
		}else{
			$response['success'] = false;
			$response['message'] = "Gagal";
		}
		return $response;
	}

	public function update_master_barang($data_received, $where)
	{
		$data['KodeBarang']=$data_received['KodeBarang'];
		$data['NamaBarang']=$data_received['NamaBarang'];
		$data['NoKatalog']=$data_received['NoKatalog'];
		$data['Satuan']=$data_received['Satuan'];
		$data['SupplierID']=$data_received['distributor'];
		$data['BAGIAN']=$data_received['BAGIAN'];

		$this->db->where($where);
		$result = $this->db->update("admin_logistik.msbarang", $data);

		$response = [
			'success' => true,
			'message' => '',
		];

		if($result){
			$response['success'] = true;
			$response['message'] = "Data Diubah";
		}else{
			$response['success'] = false;
			$response['message'] = "Gagal";
		}
		return $response;
	}

	public function hapus_master_barang($where)
	{
		$response = [
			'success' => true,
			'message' => '',
		];

		$this->db->where($where);
		$result = $this->db->delete("admin_logistik.msbarang");

		if($result){
			$response['success'] = true;
			$response['message'] = "Data dihapus";
		}else{
			$response['success'] = false;
			$response['message'] = "Gagal";
		}

		return $response;
	}

	function baseQueryMB($args = [])
	{
		return $this->db->from("admin_logistik.msbarang m")
			->join("admin_logistik.supplier s", "s.SupplierID = m.SupplierID")
			->select('m.*, s.NamaPT, CONCAT(KodeBarang," - ",NamaBarang) as combobox');
    }

    public function get_master_biaya_jasa($args = [])
	{
		$this->load->helper("myquery");
		if($args == null) {
			$args = [];
		}
		if(isset($args["where"]) == false) {
			$args["where"] = [];
        } else {
            $t = '';
            foreach($args["where"] as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            //print_r($t);

            $args["where"] = $t;
        }


		if(isset($args["limit"]) == false) {
			$args["limit"] = [];
		}
		if(isset($args["start"]) == false) {
			$args["start"] = [];
		}

		return $this->baseQueryBJ($args)
			->where($args["where"])
			->order_by($args["sort"], $args["order"])
			->limit($args["limit"], $args["start"])
			->get()->result_array();
    }

    public function simpan_master_biaya_jasa($data_received)
	{
        $kode = $this->db->select('KodeBarang')->from("admin_logistik.msbarang m")->where('JenisRG','BIAYA DAN JASA')->order_by('KodeBarang',"desc")->limit(1)->get()->row()->KodeBarang;
        $kode = substr($kode, 2) + 1;
        if (strlen($kode) == 1) {
            $kode = 'JS000'.$kode;
        }elseif (strlen($kode) == 2){
            $kode = 'JS00'.$kode;
        }elseif (strlen($kode) == 3){
            $kode = 'JS0'.$kode;
        }elseif (strlen($kode) == 4){
            $kode = 'JS'.$kode;
        }
		$data['KodeBarang']=$kode;
		$data['NamaBarang']=$data_received['NamaBarang'];
		$data['Satuan']=$data_received['Satuan'];
		$data['JenisRG']=$data_received['JenisRG'];

		$result = $this->db->insert("admin_logistik.msbarang", $data);

		$response = [
			'success' => true,
			'message' => '',
		];

		if($result){
			$response['success'] = true;
			$response['message'] = "Data ditambahkan";
		}else{
			$response['success'] = false;
			$response['message'] = "Gagal";
		}
		return $response;
    }

    public function update_master_biaya_jasa($data_received, $where)
	{
		$data['NamaBarang']=$data_received['NamaBarang'];
		$data['Satuan']=$data_received['Satuan'];

		$this->db->where($where);
		$result = $this->db->update("admin_logistik.msbarang", $data);

		$response = [
			'success' => true,
			'message' => '',
		];

		if($result){
			$response['success'] = true;
			$response['message'] = "Data Diubah";
		}else{
			$response['success'] = false;
			$response['message'] = "Gagal";
		}
		return $response;
    }

    public function update_aktif_master_biaya_jasa($data_received, $where)
	{
		$data['Active']=$data_received['AKTIF'];

		$this->db->where($where);
		$result = $this->db->update("admin_logistik.msbarang", $data);
        // print_r($this->db->last_query());
		$response = [
			'success' => true,
			'message' => '',
		];

		if($result){
			$response['success'] = true;
			$response['message'] = "Data Diubah";
		}else{
			$response['success'] = false;
			$response['message'] = "Gagal";
		}
		return $response;
	}

    function baseQueryBJ($args = [])
	{
		return $this->db->from("admin_logistik.msbarang m")->where('JenisRG', 'BIAYA DAN JASA')->select('*');
    }

    function baseQueryMG($args = [])
	{
		return $this->db->from("admin_logistik.unit m")->select('*');
	}


	function many_pr($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
		else{
			$t = '';
			foreach($args["where"] as $v){
				$t = ($t=='')?$v:$v.' AND '.$t;
			}
			//print_r($t);

			$args["where"] = $t;
		}
		if(isset($args["limit"]) == false) {
            $args["limit"] = [];
        }
		if(isset($args["start"]) == false) {
            $args["start"] = [];
        }
		if(isset($args["order"]) == false) {
            $args["order"] = [];
        }

		$result = array();
		$row = $this->baseQueryPr($args)->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();
        $result = array_merge($result, ['rows' => $row]);
        $result['total'] = $this->baseQueryPr($args)->where($args["where"])->get()->num_rows();

		return $result;

        //return $this->baseQueryPr($args)->where($args["where"])->limit($args["limit"], $args["start"])->get()->result_array();

    }

    function baseQueryPr($args = []) {
        return $this->db->from("admin_logistik.pr _")
            //->order_by('_.TANGGAL', 'desc')
			->join("admin_logistik.po po", "po.NOPR = _.NOBUKTI", "left")
			->join("admin_logistik.supplier supplier", "supplier.SupplierID = po.SUPPLIERID", "left")
            ->select([
                "_.*",
                "po.NOBUKTI as po",
				"supplier.NamaPT as distributor",
				"po.KETERANGAN as ket",
            ])->order_by($args["sort"],$args["order"]);
    }

	function many_pr_detail($args = []) {
        $this->load->helper("myquery");
        if($args == null) {
            $args = [];
        }
        if(isset($args["where"]) == false) {
            $args["where"] = [];
        }
        return $this->baseQueryPrDetail($args)->where($args["where"])->get()->result_array();
    }

    function baseQueryPrDetail($args = []) {
        return $this->db->from("admin_logistik.prdetail _")
            ->join("admin_logistik.msbarang msbarang", "msbarang.KodeBarang = _.KODEBARANG", "left")
            ->select([
                "_.*",
                "msbarang.NamaBarang as namabarang",
				"msbarang.Satuan as satuan",
				"_.QTYPesan as pesan",
				"_.QtyPR as tersedia",
            ]);
    }

	function add_input_pr($data) {
        /*$id_levels = [];
        $means = [];
        $sds = [];
        if(array_key_exists("id_level", $data)) {
            $id_levels = $data["id_level"];
            unset($data["id_level"]);
            if(is_array($id_levels) == false) {
                return ["status" => false, "message" => "Field level diperlukan"];
            }
        }
        if(array_key_exists("mean", $data)) {
            $means = $data["mean"];
            unset($data["mean"]);
            if(is_array($means) == false) {
                return ["status" => false, "message" => "Field mean diperlukan"];
            }
        }
        if(array_key_exists("sd", $data)) {
            $sds = $data["sd"];
            unset($data["sd"]);
            if(is_array($sds) == false) {
                return ["status" => false, "message" => "Field SD diperlukan"];
            }
        }
        $countIdLevel = count($id_levels);
        $countMean = count($means);
        $countSd = count($sds);
        if(($countIdLevel == $countMean && $countMean == $countSd) == false) {
            return ["status" => false, "message" => "Data level tidak valid"];
        }
        if($countIdLevel <= 0) {
            return ["status" => false, "message" => "Data level harus diisi"];
        }*/
        $this->db->insert("admin_logistik.pr", $data);
        $id = $this->db->insert_id();

        /*$r = $this->one_input_qc(["where" => ["_.id_qc_input" => $id]]);
        if($id == null || $r == null) {
            return ["status" => false, "message" => "Terjadi kesalahan"];
        }

        // simpan detail
        for($i = 0; $i < $countIdLevel; $i++) {
            $this->db->insert("qc_input_detail", [
                "id_qc_input" => $id,
                "id_level" => $id_levels[$i],
                "mean" => floatval($means[$i]),
                "sd" => floatval($sds[$i])
            ]);
        }*/

        return ["status" => true, "message" => "Success"];
    }

	function update_input_pr($where, $data) {
        $this->db->where($where)->update("admin_logistik.pr", $data);
        return ["status" => true, "message" => "Success"];
    }

	function delete_input_pr($where){
		$del = $this->db->delete('admin_logistik.pr', $where);
		return ["status" => true, "message" => "Success"];
	}

	// konversi satuan
	public function get_konversi_satuan($args = [])
	{
		$this->load->helper("myquery");
		if($args == null) {
			$args = [];
		}
		if(isset($args["where"]) == false) {
			$args["where"] = [];
		} else {
            $t = '';
            foreach($args["where"] as $v){
				
                $t = ($t=='')?$v:$v.' AND '.$t;
				// echo $v;
            }
            $args["where"] = $t;
        }

		if(isset($args["limit"]) == false) {
			$args["limit"] = [];
		}
		if(isset($args["start"]) == false) {
			$args["start"] = [];
		}

		return $this->baseQueryKs($args)
			->where($args["where"])
			->order_by($args["sort"], $args["order"])
			->limit($args["limit"], $args["start"])
			->get()->result_array();
	}

	function add_konversi_satuan($data) {

        $this->db->insert("admin_logistik.konversi", $data);
        $id = $this->db->insert_id();


        return ["success" => true, "message" => "Di Simpan"];
    }



    public function update_konversi_satuan($data_received, $where)
    {
        $data['KodeBesar']=$data_received['KodeBesar'];
        $data['KodeKecil']=$data_received['KodeKecil'];
        $data['Konversi']=$data_received['Konversi'];

        $this->db->where($where);
        $result = $this->db->update("admin_logistik.konversi", $data);

        $response = [
            'success' => true,
            'message' => '',
        ];

        if($result){
            $response['success'] = true;
            $response['message'] = "Data Diubah";
        }else{
            $response['success'] = false;
            $response['message'] = "Gagal";
        }
        return $response;
    }

	public function hapus_konversi_satuan($where)
	{
		$response = [
			'success' => true,
			'message' => '',
		];

		$this->db->where($where);
		$result = $this->db->delete("admin_logistik.konversi");

		if($result){
			$response['success'] = true;
			$response['message'] = "Data dihapus";
		}else{
			$response['success'] = false;
			$response['message'] = "Gagal";
		}

		return $response;
	}

	

	function baseQueryKs($args = [])
	{
		return $this->db->from("admin_logistik.konversi k")
			->join("admin_logistik.msbarang b", "b.KodeBarang = k.KodeBesar")
			->join("admin_logistik.msbarang l", "l.KodeBarang = k.KodeKecil")
			->select('b.KodeBarang as KodeBarangBesar,b.NamaBarang as NamaBarangBesar,b.NoKatalog as NoKatalogBesar,b.Satuan as SatuanBesar,b.MinStock as MinStockBesar
			,l.KodeBarang as KodeBarangKecil,l.NamaBarang as NamaBarangKecil,l.NoKatalog as NoKatalogKecil,l.Satuan as SatuanKecil,k.Konversi as Konversi');
	}

	function _paginate(){

        $page = $this->input->post('page');
        $rows = $this->input->post('rows');

        if($page && $rows){
            $this->db->offset(($page-1)*$rows);
            $this->db->limit($rows);
        }

    }

    function selectSuratJalanWithFilter($where = null){
        if($where != null) {
            $t = '';
            foreach($where as $v){
                $t = ($t=='') ? $v : $v.' AND '.$t;
            }
            // echo $t;
            $this->db->where($t);
        }

        return $this->suratJalanQuery($where);
    }

    function selectSuratJalan($NOBUKTI = null){                

        $where = array();

        // Jika data ID tidak kosong , maka ambil item berdasarkan KursID
        if ($NOBUKTI != null){
            $where['sj.NOBUKTI']    = $NOBUKTI;
        }

        return $this->suratJalanQuery($where);

    }

	function suratJalanQuery($where){
        $this->db
            ->select([
                $this->tb_suratJalan.".NOBUKTI",
                $this->tb_suratJalan.".TANGGAL",
                $this->tb_unit.".NAMAUNIT",
                $this->tb_suratJalan.".STATUS",
                $this->tb_suratJalan.".USERID"
            ])
            ->from($this->tb_suratJalan)
            ->join($this->tb_unit , $this->tb_unit.'.KODEUNIT = '.$this->tb_suratJalan.'.CABANG2')
            ->where($where);       

        return $this->db->get();        
    }

    // remove SJ
    function deleteSJ($NOBUKTI){

        $where = array(
            "NOBUKTI"    => $NOBUKTI
        );

       $this->db->delete('admin_logistik.sj',$where);
       $this->db->delete('admin_logistik.sjdetail',$where);
       
       return ($this->db->affected_rows() == 0) ? false : true;        

    }

    // insert SJ
    function insertSuratJalan($data){

        $this->db->insert('admin_logistik.sj', $data);
        return ($this->db->affected_rows() == 0) ? false : true;

    }

    function insertSuratJalanDetail($data){

        $this->db->insert('admin_logistik.sjdetail', $data);
        return ($this->db->affected_rows() == 0) ? false : true;

    }

	// item cabang
	public function get_item_cabang($args = [])
	{
		$this->load->helper("myquery");
		if($args == null) {
			$args = [];
		}
		if(isset($args["where"]) == false) {
			$args["where"] = [];
        } else {
            $t = '';
            foreach($args["where"] as $v){
                $t = ($t=='')?$v:$v.' AND '.$t;
            }
            $args["where"] = $t;
        }

		if(isset($args["limit"]) == false) {
			$args["limit"] = [];
		}
		if(isset($args["start"]) == false) {
			$args["start"] = [];
		}

		return $this->baseQueryIC($args)
			->where($args["where"])
			->order_by($args["sort"], $args["order"])
			->limit($args["limit"], $args["start"])
			->get()->result_array();
	}

	function baseQueryIC($args = [])
	{
		return $this->db->from("admin_logistik.itemcabang ic")
			->join("admin_logistik.msbarang m", "m.KodeBarang = ic.KodeBarang")
			->select('m.*');
	}

	function selectSuratJalanDetailWithFilter($whereFilter,$NOBUKTI){                

        // $whereFilter = array();

        // Jika data ID tidak kosong , maka ambil item berdasarkan KursID
        if ($NOBUKTI != null){
            $whereFilter['NOBUKTI']    = $NOBUKTI;
        }

        // var_dump($whereFilter);
       
        $this->db
            ->select([
                "b.NamaBarang",
                "b.Satuan",
                "pod.QTY",
                "pod.HARGA"
            ])
            ->from('admin_logistik.sjdetail'.' pod')
            ->join('admin_logistik.msbarang'.' b','b.KodeBarang=pod.KODEBARANG')
            ->where($whereFilter);

        return $this->db->get();

    }

     function selectSuratJalanDetail($NOBUKTI){                

        $where = array();

        if ($NOBUKTI != null){
            $where['NOBUKTI']    = $NOBUKTI;
        }

        $this->db
            ->select([
                "b.NamaBarang",
                "b.Satuan",
                "pod.QTY",
                "pod.HARGA"
            ])
            ->from('admin_logistik.sjdetail pod')
            ->join('admin_logistik.msbarang b','b.KodeBarang=pod.KODEBARANG')
            ->where($where);

        return $this->db->get();
 
    }

    public function get_karyawan($params=[], $rows=0, $offset=10,$all=0){
        $this->db->select(['users.id_user','users.name','users.last_name','users.CUTI','users.cuti_keseluruhan','users.branch_id','branch.branch_name','bagian.name as nama_bagian','position.name_position','users.STATUS','users.TGLMASUK','users.id_position','users.id_bagian','users.last_reset_cuti','users.masabakti','users.masabakti_keseluruhan','users.last_reset_masabakti']);
        $this->db->from('users');
        if (isset($params['in_param'])) {
          foreach ($params['in_param'] as $key => $value) {
              $this->db->where_in($key,$value);
          }
          unset($params['in_param']);
        }
 
        if (isset($params['not_in_param'])) {
         foreach ($params['not_in_param'] as $k => $v) {
             $this->db->where_not_in($k,$v);
         }
         unset($params['not_in_param']);
       }
 
        $this->db->where($params);
        if ($all==0) {
          $this->db->limit($rows, $offset);
        }
        $this->db->order_by('name', 'ASC');
        $this->db->join('branch', 'users.branch_id = branch.branch_id','left');
        $this->db->join('bagian', 'users.id_bagian = bagian.id','left');
        $this->db->join('position', 'users.id_position = position.id_position','left');
        $query = $this->db->get();
        return $query->result_array();
     }



     public function get_master_barang_sp($args = [])
     {
         
         $this->load->helper("myquery");
         if($args == null) {
             $args = [];
         }
         if(isset($args["where"]) == false) {
             $args["where"] = [];
         } else {
             $t = '';
             foreach($args["where"] as $v){
                 $t = ($t=='')?$v:$v.' AND '.$t;
             }
             $args["where"] = $t;
         }
 
         if(isset($args["limit"]) == false) {
             $args["limit"] = [];
         }
         if(isset($args["start"]) == false) {
             $args["start"] = [];
         }
 
    

             return $this->baseQueryMasterBarangSP($args)->where($args["where"])->get()->result_array();
     }
 
     function baseQueryMasterBarangSP($args = []) {
        return $this->db->from("admin_logistik.msbarang m")
            // ->join("supplier s", "s.SupplierID = m.SupplierID", "left")
            ->select([
                "m.*",
            ]);
    }
}
