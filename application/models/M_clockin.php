<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class M_clockin extends CI_model
{

    public function get_data($table,$where)
    {
        $this->db->where($where);
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $save = $this->db->update($table, $data);
        return $save;
    }

    public function delete_data($where, $table)
    {
        $this->db->where($where);
        $result = $this->db->delete($table);
        return $result;
    }

    /*get data (R)*/
    public function get_all_data($params=[],$source=[]){

        /*
            structure example
            $source = [
                query_select => '',
                master_table => '',
                relations => [
                    'table1' => [
                        'conditions' => '',
                        'join_type' => ''
                    ],
                    'table2' => [
                        'conditions' => '',
                        'join_type' => ''
                    ],
                ]
            ]
        */
        $this->db->select($source['query_select']);
        $this->db->from($source['master_table']);

        if (!empty($source['relations'])) {
            foreach ($source['relations'] as $table => $opts) {
                $this->db->join($table,$opts['conditions'],$opts['join_type']);     
            }
            
        }
        
        $this->db->where($params);

        if (isset($source['order']) && !empty($source['order'])) {
            $this->db->order_by($source['order']);            
        }

        $query = $this->db->get();
        return $query;
    }
    
}
