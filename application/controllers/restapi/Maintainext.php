<?php

class Maintainext extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->restapikey= $this->config->config['restapikey'];
        if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
		$this->user_id = $this->input->get_post('user_id');
		$this->load->model('restapi/user_model');
        $this->me = $this->user_model->one_user(["where" => ["_.id_user" => $this->user_id]]);
        $this->model = 'maintain_ext';
		if($this->me == null) {
			echo json_encode(['status' => false, 'message' => 'Invalid user']);
			exit();
		}
		$this->load->model('m_maintain_ext', 'cmodel');
	}

	function get(){
		$month = intval($this->input->post('month'));
		$year = intval($this->input->post('year'));
        $branch_id = $this->me['branch_id'];
		if ($year == 0 && $month == 0){
			$r = $this->cmodel->getAllData($branch_id)->result();
		}else{
			$r = $this->cmodel->get($branch_id, $month, $year)->result();
		}
		echo json_encode($r);
	}

	function get_alat() {
		$r = $this->cmodel->get_data('master_qc_alat')->result();
		echo json_encode($r);
	}

	function save(){
        $result['status'] = false;
        $result['message'] = 'Data gagal disimpan.';


		$data['id_alat'] = $this->input->post('id_alat');
		$data['tanggal'] = date('Y-m-d',strtotime($this->input->post('tanggal')));
		$data['remind_before_day'] = $this->input->post('remind_before_day');
		$data['note'] = $this->input->post('note');
		$data['periode_type'] = $this->input->post('period');
		$data['branch_id'] = $this->me['branch_id'];
		$data['user_id'] = $this->input->post('user_id');
		if (! empty($this->input->post('id'))):
			$where['id_maintain_ex'] = $this->input->post('id');
			$r =  $this->cmodel->update_data($where, $data, $this->model);
		else:
            $data['created_at'] = date("Y-m-d H:i:s");
			$r = $this->cmodel->insert_data($data, $this->model);
		endif;
        if ($r) {
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        }
        echo json_encode($result);
	}

	function setSelesai() {
        $result['status'] = false;
        $result['message'] = 'Data gagal disimpan.';

        $data['status'] = 'Selesai';

        $id_maintain_ex = $this->input->post('id');

        if (!empty($id_maintain_ex)) {
            $where['id_maintain_ex'] = $this->input->post('id');
            $id_maintain_ex = $this->cmodel->update_data($where, $data, $this->model);
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        } else {
            $id_maintain_ex = $this->cmodel->insert_data($data, $this->model);
        }
        if ($id_maintain_ex) {
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        }
        echo json_encode($result);
	}

	function setCancel() {
        $result['status'] = false;
        $result['message'] = 'Data gagal disimpan.';

        $data['status'] = 'Cancel';

        $id_maintain_ex = $this->input->post('id');

        if (!empty($id_maintain_ex)) {
            $where['id_maintain_ex'] = $this->input->post('id');
            $id_maintain_ex = $this->cmodel->update_data($where, $data, $this->model);
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        } else {
            $id_maintain_ex = $this->cmodel->insert_data($data, $this->model);
        }
        if ($id_maintain_ex) {
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        }
        echo json_encode($result);
	}

	function delete(){
        $result['status'] = false;
        $result['message'] = 'Data gagal disimpan.';


		$data['id_maintain_ex'] = $this->input->post('id');
		$r = $this->cmodel->delete_data($data, $this->model);

        if ($r) {
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        }
		echo json_encode($result);
	}
}
