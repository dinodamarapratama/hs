<?php
use Illuminate\Http\Request;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');

class Drug extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->restapikey= $this->config->config['restapikey'];
        if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        $this->user_id = $this->input->get_post('user_id');
        $this->load->model('restapi/user_model');
        $this->me = $this->user_model->one_user(['where' => ['_.id_user' => $this->user_id]]);
        if($this->me == null) {
            echo json_encode(['status' => false, 'message' => 'Invalid user']);
            exit();
        }
		$this->load->model('m_drugs');
        $this->load->library('misc');
        $karyawan = $this->m_drugs->get_karyawan(['id_user' => $this->user_id],0,0,1);
        if (in_array($karyawan[0]['id_position'],[4,17,20,13])) {
            $this->area_manager = 1;
        }else{
            $this->area_manager = 0;
        }

	}

	public function index(){
        $args = [];
        $args['sort'] = 'drug_name';
        $args['order'] = 'ASC';
        $args['start'] = 0;
        $args['limit'] = 100;
        $search = $this->input->post('search');
        if (!empty($search)) {
            $args['where']['drug_name like '] ='%'.$search.'%';
        }
        $getDataDrug = $this->m_drugs->get_all_drugs($args);
        $data_drug = [];
        if ($getDataDrug):
            foreach($getDataDrug as $dataDrug):
                array_push($data_drug,[
                    'id' => $dataDrug['id'],
                    'drug_name' => $dataDrug['drug_name'],
                    'branch_name' => $dataDrug['branch_name'],
                    'unit' => $dataDrug['unit'],
                    'stock' => $dataDrug['stock'],
                    'exp_date' => date('Y-m-d',strtotime($dataDrug['exp_date']))
                ]);
            endforeach;
        endif;
        echo json_encode($data_drug);
    }

    public function response($data, $code = 200)
    {
        $json = json_encode($data);
        return $this
            ->output
            ->set_content_type('application/json')
            ->set_status_header($code)->set_output($json);
    }

    function isAm()
    {
        echo json_encode($this->area_manager);
    }


    function list_branch()
    {
        $list_branch = $this->db->query("select branch_name as name , branch_id as id from branch")->result();
		return $this->response($list_branch);
    }

    public function save_data(){

        $parse_data = [];
        $parse_data = $this->input->post();

        if ($this->area_manager == 1){
            $parse_data['branch_id'] =  $parse_data['branch_id'];
        }else{
            $parse_data['branch_id'] =  $parse_data['branch'];
        }

        unset($parse_data['auth_key']);
        unset($parse_data['user_id']);
        unset($parse_data['branch']);
        
        // $parse_data['exp_date'] = $this->misc->dateFormat($parse_data['exp_date']);
        if (isset($parse_data['id']) && !empty($parse_data['id']) && $parse_data['id']>0) {
            $key_update = $parse_data['id'];
            unset($parse_data['id']);
            $id = $this->m_drugs->update_data(['id' => $key_update],$parse_data,'drugs');
        }else{
            $id = $this->m_drugs->insert_data($parse_data,'drugs');
        }
        
        if ($id > 0) {
            /*saved*/
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        }else{
            $result['status'] = true;
            $result['message'] = 'Data gagal disimpan.';
        }
        echo json_encode($result); die;
    }

    public function get_drug(){
        $parse_data = $this->input->post();
        $id_drug = $parse_data['id'];
        $args = [];
        $args['where']['id'] = $id_drug;
        $result = $this->m_drugs->get_drugs($args);
        echo json_encode($result);
    }

}