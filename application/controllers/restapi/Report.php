<?php

class Report extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->restapikey= $this->config->config['restapikey'];
        if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        $this->user_id = $this->input->post('user_id');
        $this->load->model('restapi/user_model');
        $user = $this->user_model->one_user(['where' => ['_.id_user' => $this->user_id]]);
        if(empty($user)) {
            echo json_encode(['status' => false, 'message' => 'User is invalid']);
            exit();
        }
    }

    function save(){
        $data = [
            'title' => $this->input->post('subject'),
            'status' => $this->input->post('status'),
            'kategori' => $this->input->post('category'),
            'from_id' => $this->user_id
        ];

        $receivers = $this->input->post('receivers');
        $depts = $this->input->post('departments');

        $general_desc = $this->input->post('general_des');

        $data_general = $data;
        $data_general['description'] = empty($general_desc) ? '' : $general_desc;

        $this->load->model('restapi/generalreports_model');
        $gr = $this->generalreports_model->save($data_general, $receivers, $depts, 'file_general_');

        if($gr['status'] == true) {
            $id = $gr['id'];
        }

        $data_daily = $data;
        $data_daily['pelayanan_des'] = empty(($v = $this->input->post('pelayanan_des'))) ? 8 : $v;
        $data_daily['absensi_des'] = empty(($v = $this->input->post('absensi_des'))) ? 1 : $v;
        $data_daily['reg_des'] = empty(($v = $this->input->post('reg_des'))) ? 2 : $v;
        $data_daily['sampling_des'] = empty(($v = $this->input->post('sampling_des'))) ? 3 : $v;
        $data_daily['qc_des'] = empty(($v = $this->input->post('qc_des'))) ? 4 : $v;
        $data_daily['alat_des'] = empty(($v = $this->input->post('alat_des'))) ? 5 : $v;
        $data_daily['janji_des'] = empty(($v = $this->input->post('janji_des'))) ? 6 : $v;
        $data_daily['lain_des'] = empty(($v = $this->input->post('lain_des'))) ? 7 : $v;

        $this->load->model('restapi/dailyreport_model');

        $dr = $this->dailyreport_model->save($data_daily, $receivers, $depts, 'file_daily_');

        $r = ['status' => true, 'message' => 'Report berhasil disimpan'];
        echo json_encode($r);
    }
}