<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class suhu extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->restapikey= $this->config->config['restapikey'];
        if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        $this->load->model('restapi/suhu_model', 'suhu');
        $this->load->model('restapi/user_model');
        $this->current_user = $this->input->post('user_id');
        $this->me = $this->user_model->one_user(['where' => ['_.id_user' => $this->current_user]]);
        if ($this->me == null) {
            echo json_encode(['status' => false, 'message' => 'Invalid user']);
            exit();
        }
    }

    public function index(){
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $search = $this->input->post('search');

        $args = ['where' => []];

        if ($search) :
            $args['search'] = $search;
        endif;

        $args['where']['_.user_type'] = 1;

        if (isset($tanggal) && !empty($tanggal)) :
            $args['where']['_.recog_time'] = date('Y-m-d', strtotime($tanggal));
        endif;
        // if (isset($user) && !empty($user)) :
        //     $args['where']['from_id'] = $user;
        // endif;

        if ($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
            $args['start'] = $start;
            $args['length'] = $length;
        }

        $r = $this->suhu->getData($args);
        return $this->response($r);
    }

    public function detail(){
        $id = $this->input->post('id');
        $detailSuhu = $this->suhu->detail($id);
        return $this->response($detailSuhu);
    }

    public function response($data, $code = 200){
        $json = json_encode($data);
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header($code)
            ->set_output($json);
    }
}
