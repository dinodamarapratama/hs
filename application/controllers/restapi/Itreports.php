<?php
class Itreports extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->restapikey= $this->config->config['restapikey'];
        if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        $this->user_id = $this
            ->input
            ->post('user_id');
        $this
            ->load
            ->model('m_itreports', 'cmodel');
        $this
            ->load
            ->model('m_masterinventaris');
    }

    function count_report()
    {
        $search = $this
            ->input
            ->post('search');
        $user = $this
            ->input
            ->post('user');
        $bagian_id = $this
            ->input
            ->post('bagian_id');
        $tanggal = $this
            ->input
            ->post('tanggal');
        $status = $this
            ->input
            ->post('status');
        $args = [];
        if (isset($tanggal) && !empty($tanggal)):
            $args['where']['_.date'] = date('Y-m-d',strtotime($tanggal));
        endif;
        if (isset($user) && !empty($user)):
            $args['where']['_.from_id'] = $user;
        endif;

        if (empty($search) == false)
        {
            $args['like'] = [['search' => $search, 'cols' => ['_.title', '_.description', '_.kategori', 'rply.reply_all', 'usr.name', 'usr.username']]];
        }

        $r1 = $this
            ->cmodel
            ->getCountReceived($this->user_id, $args);
        $r2 = $this
            ->cmodel
            ->getCountSent($this->user_id, $args);
        $r3 = $this
            ->cmodel
            ->getCountArchived($this->user_id, $args);
        $r = ['received' => $r1->jumlah, 'sent' => $r2->jumlah, 'archived' => $r3->jumlah];
        echo json_encode($r);
    }

    function many_report()
    {
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $user = $this->input->post('user');
        $bagian_id = $this->input->post('bagian_id');
        $tanggal = $this->input->post('tanggal');
        $search = $this->input->post('search');
        $status = $this->input->post('status');

        $args = ['where' => [],
        // 'order' => [ ['_.id', 'desc'] ]
        ];
        if (isset($tanggal) && !empty($tanggal)):
            $args['where']['_.date'] = date('Y-m-d',strtotime($tanggal));
        endif;
        if (isset($user) && !empty($user)):
            $args['where']['_.from_id'] = $user;
        endif;

        if ($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0)
        {
            $args['limit'] = [$start, $length];
        }

        if (empty($search) == false)
        {
            $args['like'] = [['search' => $search, 'cols' => ['_.title', '_.description', '_.kategori', 'rply.reply_all', 'usr.name', 'usr.username']]];
        }

        $r = [];
        if ($status == 'received')
        {
            $r = $this
                ->cmodel
                ->getReceived($this->user_id, $args);
        }
        else if ($status == 'sent')
        {
            $r = $this
                ->cmodel
                ->getSent($this->user_id, $args);
        }
        else if ($status == 'archived')
        {
            $r = $this
                ->cmodel
                ->getArchived($this->user_id, $args);
        }
        echo json_encode($r);
    }

    function save()
    {
        $id_report = $this
            ->input
            ->post('id');
        $data = [];
        if (empty($id_report))
        {
            $data = ['from_id' => $this->user_id, 'status' => $this
                ->input
                ->post('status') , 'kategori' => $this
                ->input
                ->post('category') , 'title' => $this
                ->input
                ->post('subject') , 'is_view' => 1, 'created_at' => date('Y-m-d H:i:s') , 'last_update' => date('Y-m-d H:i:s') , 'date' => date('Y-m-d', strtotime($this
                ->input
                ->post('date'))) , 'id_draft' => 0, 'receivers' => $this
                ->input
                ->post('receivers') , 'departments' => $this
                ->input
                ->post('department') , 'to_branch' => $this
                ->input
                ->post('to_branch') , 'bagian' => $this
                ->input
                ->post('bagian') , ];
        }
        else
        {
            $data = [
            // 'from_id' => $this->user_id,
            'status' => $this
                ->input
                ->post('status') , 'kategori' => $this
                ->input
                ->post('category') , 'title' => $this
                ->input
                ->post('subject') ,
            // 'is_view' => 1,
            // 'created_at' => date('Y-m-d H:i:s'),
            // 'last_update' => date('Y-m-d H:i:s'),
            // 'date' => date('Y-m-d'),
            // 'id_draft' => 0,
            // 'receivers' => $this->input->post('receivers'),
            'to_branch' => $this
                ->input
                ->post('to_branch') , ];
        }

        $bodys = ['pc_des', 'printer_des', 'jaringan_des', 'server_des', 'monitor_des', 'dll'];
        for ($i = 0;$i < 6;$i++)
        {
            $key = 'body';
            if ($i > 0)
            {
                $key = 'body' . $i;
            }
            $body = $this
                ->input
                ->post($key);
            if ($body != null)
            {
                $data[$bodys[$i]] = $body;
            }
            else
            {
                // $data[$bodys[$i]] = $i + 1;
                if ($bodys[$i] == 'pc_des'){
                    $data[$bodys[$i]] = 1;
                } else if ($bodys[$i] == 'printer_des'){
                    $data[$bodys[$i]] = 2;
                } else if ($bodys[$i] == 'jaringan_des'){
                    $data[$bodys[$i]] = 4;
                } else if ($bodys[$i] == 'server_des'){
                    $data[$bodys[$i]] = 5;
                } else if ($bodys[$i] == 'monitor_des'){
                    $data[$bodys[$i]] = 9;
                } else if ($bodys[$i] == 'dll'){
                    $data[$bodys[$i]] = 8;
                }
            }
        }
        $r = [];
        if (empty($id_report))
        {
            $r = $this
                ->cmodel
                ->save($data);

        // stay
                $id_itreports = $r['id'];

                $invt = $this->input->post('invt_master');
                $data = json_decode($invt, 1);

                if(count($data) > 1) {

                    foreach($data as $k => $v) 
                    {
                        $new_data = array();
                        $new_data = [
                        'id_inventaris' => $data[$k]["id_inventaris"],
                        'id_itreports' => $r['id'],
                        'id_subkategori' => $data[$k]["id_subkategori"],
                        'tanggal' => date('Y-m-d H:i:s'),
                        'creator_id' => $this->user_id,
                        'detail_perawatan' => $data[$k]["detail_perawatan"]
                        ];
    
                        $this->m_masterinventaris->insert_data($new_data, 'master_inventaris_it_history');
                    }
        
    
                } else{

                    foreach($data as $k => $v) 
                    {
                        $new_data = array();
                        $new_data = [
                        'id_inventaris' => $data[$k]["id_inventaris"],
                        'id_itreports' => $r['id'],
                        'id_subkategori' => $data[$k]["id_subkategori"],
                        'tanggal' => date('Y-m-d H:i:s'),
                        'creator_id' => $this->user_id,
                        'detail_perawatan' => $data[$k]["detail_perawatan"]
                        ];
                    }

                    $this->m_masterinventaris->insert_data($new_data, 'master_inventaris_it_history');

                }



        }
        else
        {
            $r = $this
                ->cmodel
                ->update_report(['_.id' => $id_report], $data);
        }

        if ($r['status'] == true)
        {
            $counter = - 1;
            $list_upload = [];
            $attachment_descriptions = $this
                ->input
                ->post('attachments');
            $attachment_descriptions_length = is_array($attachment_descriptions) ? count($attachment_descriptions) : 0;
            while (true)
            {
                $counter++;
                $key = 'file_' . $counter;
                if (isset($_FILES[$key]) == false)
                {
                    break;
                }
                if ($counter > 100)
                {
                    break;
                }
                $config = ['upload_path' => 'uploads/attachments/itreports/', 'allowed_types' => '*', 'encrypt_name' => true];

                if (file_exists($config['upload_path']) == false)
                {
                    $old = umask(0);
                    mkdir($config['upload_path'], 0777, true);
                    umask($old);
                }

                $this
                    ->upload
                    ->initialize($config);

                if ($this
                    ->upload
                    ->do_upload($key))
                {
                    $uploaded = $this
                        ->upload
                        ->data();
                    $list_upload[] = $uploaded;
                    $desc = $this
                        ->input
                        ->post('file_desc_' . $counter);
                    $title = $this
                        ->input
                        ->post('file_title_' . $counter);
                    // if($counter < $attachment_descriptions_length) {
                    // 	$d = $attachment_descriptions[$counter];
                    // 	$desc = $d['description'];
                    // 	$title = $d['title'];
                    // }
                    $this
                        ->cmodel
                        ->save_attachment(['itreports_id' => $r['id'], 'title' => strlen($title) > 0 ? $title : $uploaded['orig_name'], 'description' => $desc, 'path' => $config['upload_path'] . $uploaded['file_name'], 'filetype' => $uploaded['file_type'], 'filesize' => $uploaded['file_size'], 'name' => $uploaded['orig_name'], ]);
                }
            }
            $r['upload'] = $list_upload;
        }
        echo json_encode($r);
    }

    function delete()
    {
        $id = $this
            ->input
            ->get_post('id');
        $r = $this
            ->cmodel
            ->hapus_report(['_.id' => $id]);
        echo json_encode($r);
    }

    function detail()
    {
        $id = $this
            ->input
            ->get_post('id');
        $r = $this
            ->cmodel
            ->one_report(['where' => ['_.id' => $id]]);
        if ($r != null)
        {
            $this
                ->cmodel
                ->mark_read($r->id, $this->user_id, $r);
            $this
                ->cmodel
                ->auto_close($r->id, $r);
        }

         
       
   
        if(!empty($r->pc_des)){
                $myCaption1 = strip_tags($r->pc_des);
                $myCaptionEncoded1 = htmlspecialchars_decode($myCaption1);
                $r->pc_des = html_entity_decode($r->pc_des);
                $r->pc_des = str_replace(['<p>','&lt;p&gt;','&lt;em&gt;&lt;strong&gt;&amolnbsp;','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>','<br','</br>','&quot;','&nbsp;'], '',$myCaptionEncoded1);
            }

        if(!empty($r->printer_des)){
                $myCaption1 = strip_tags($r->printer_des);
                $myCaptionEncoded1 = htmlspecialchars_decode($myCaption1);
                $r->printer_des = str_replace(['<p>','&lt;p&gt;','&lt;em&gt;&lt;strong&gt;&amolnbsp;','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>','<br','</br>','&quot;','&nbsp;'], '',$myCaptionEncoded1);
                $r->printer_des = html_entity_decode($r->printer_des);
            }

        // if(!empty($r->alat_des)){
        //         $myCaption1 = strip_tags($r->alat_des);
        //         $myCaptionEncoded1 = htmlspecialchars_decode($myCaption1);
        //         $r->alat_des = str_replace(['<p>','&lt;p&gt;','&lt;em&gt;&lt;strong&gt;&amolnbsp;','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>','<br','</br>','&quot;','&nbsp;'], '',$myCaptionEncoded1);
        //         $r->alat_des = html_entity_decode($r->alat_des);
        //     }

        if(!empty($r->jaringan_des)){
                $myCaption1 = strip_tags($r->jaringan_des);
                $myCaptionEncoded1 = htmlspecialchars_decode($myCaption1);
                $r->jaringan_des = str_replace(['<p>','&lt;p&gt;','&lt;em&gt;&lt;strong&gt;&amolnbsp;','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>','<br','</br>','&quot;','&nbsp;'], '',$myCaptionEncoded1);
                $r->jaringan_des = html_entity_decode($r->jaringan_des);
            }

        if(!empty($r->server_des)){
                $myCaption1 = strip_tags($r->server_des);
                $myCaptionEncoded1 = htmlspecialchars_decode($myCaption1);
                $r->server_des = str_replace(['<p>','&lt;p&gt;','&lt;em&gt;&lt;strong&gt;&amolnbsp;','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>','<br','</br>','&quot;','&nbsp;'], '',$myCaptionEncoded1);
                $r->server_des = html_entity_decode($r->server_des);
            }

        // if(!empty($r->prog_des)){
        //         $myCaption1 = strip_tags($r->prog_des);
        //         $myCaptionEncoded1 = htmlspecialchars_decode($myCaption1);
        //         $r->prog_des = str_replace(['<p>','&lt;p&gt;','&lt;em&gt;&lt;strong&gt;&amolnbsp;','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>','<br','</br>','&quot;','&nbsp;'], '',$myCaptionEncoded1);
        //         $r->prog_des = html_entity_decode($r->prog_des);

        //     }

        // if(!empty($r->backup_des)){
        //         $myCaption1 = strip_tags($r->backup_des);
        //         $myCaptionEncoded1 = htmlspecialchars_decode($myCaption1);
        //         $r->backup_des = str_replace(['<p>','&lt;p&gt;','&lt;em&gt;&lt;strong&gt;&amolnbsp;','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>','<br','</br>','&quot;','&nbsp;'], '',$myCaptionEncoded1);
        //         $r->backup_des = html_entity_decode($r->backup_des);
        //     }

            if(!empty($r->monitor_des)){
                $myCaption1 = strip_tags($r->monitor_des);
                $myCaptionEncoded1 = htmlspecialchars_decode($myCaption1);
                $r->monitor_des = str_replace(['<p>','&lt;p&gt;','&lt;em&gt;&lt;strong&gt;&amolnbsp;','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>','<br','</br>','&quot;','&nbsp;'], '',$myCaptionEncoded1);
                $r->monitor_des = html_entity_decode($r->monitor_des);
            }

        if(!empty($r->dll)){
                $myCaption1 = strip_tags($r->dll);
                $myCaptionEncoded1 = htmlspecialchars_decode($myCaption1);
                $r->dll = str_replace(['<p>','&lt;p&gt;','&lt;em&gt;&lt;strong&gt;&amolnbsp;','</p>','<strong>','</strong>','<em>','</em>','<s>','</s>','--&gt','&nbsp;','<ul>','</ul>','<li>','</li>','<br','</br>','&quot;','&nbsp;'], '',$myCaptionEncoded1);
                $r->dll = html_entity_decode($r->dll);
            }


        // stay
        $args = [];
        $args['limit'] = 10;
        $args['offset'] = 0;
        $sort = 'id_itreports,creator_name,tanggal,detail_perawatan';
        $order = 'desc';
        $args['sort'] = '_.'.$sort;
        $args['order'] = $order;
        $args['where'][] = 'id_itreports =  '.intval($id).' and mit.id = id_inventaris'; 
        $x = $this->m_masterinventaris->many_invenit_report($args); 
        $r->inventaris = $x['rows'];
        
        //$data = html_entity_decode($r);
        echo json_encode($r);
    }

    function reply()
    {
        $id_reply = $this
            ->input
            ->post('reply_id');
        $r = [];
        if ($id_reply == null)
        {
            $data = ['id_itreports' => $this
                ->input
                ->post('id') , 'message_reply' => $this
                ->input
                ->post('message') , 'sender' => $this->user_id, 'date_added' => date('Y-m-d H:i:s') ];
            $r = $this
                ->cmodel
                ->save_reply($data);
        }
        else
        {
            $data = ['message_reply' => $this
                ->input
                ->post('message') ];
            $r = $this
                ->cmodel
                ->update_reply(['_.id' => $id_reply], $data);
        }

        if ($r['status'] == true)
        {
            $counter = - 1;
            $list_uploaded = [];
            $config = ['upload_path' => 'uploads/attachments/itreports/', 'allowed_types' => '*', 'encrypt_name' => true];

            $old = umask(0);
            if (file_exists($config['upload_path']) == false)
            {
                mkdir($config['upload_path'], 0777, true);
            }
            umask($old);

            while (true)
            {
                $counter++;
                $key = 'file_' . $counter;
                if (isset($_FILES[$key]) == false)
                {
                    break;
                }
                if ($counter > 100)
                {
                    break;
                }
                $this
                    ->upload
                    ->initialize($config);
                if ($this
                    ->upload
                    ->do_upload($key))
                {
                    $uploaded = $this
                        ->upload
                        ->data();
                    $list_uploaded[] = $uploaded;

                    $this
                        ->cmodel
                        ->save_reply_attachment(['id_itreports_reply' => $r['id'], 'path' => $config['upload_path'] . $uploaded['file_name'], 'name' => $uploaded['orig_name'], 'created_at' => date('Y-m-d H:i:s') , 'filetype' => $uploaded['file_type'], 'filesize' => $uploaded['file_size']]);
                }
                else
                {
                    $list_uploaded[] = 'error ' . $key;
                }
            }
            $r['upload'] = $list_uploaded;
        }

        echo json_encode($r);
    }

    function get_replies()
    {
        $id = $this
            ->input
            ->post('id');
        $r = $this
            ->cmodel
            ->getReplies($id);
        echo json_encode($r);
    }

    function hapus_reply()
    {
        $id_reply = $this
            ->input
            ->post('id_reply');
        $r = $this
            ->cmodel
            ->hapus_reply(['_.id' => $id_reply]);
        echo json_encode($r);
    }

    function update_status()
    {
        $id = $this
            ->input
            ->post('id');
        $status = $this
            ->input
            ->post('status');
        $r = $this
            ->cmodel
            ->update_report(['_.id' => $id], ['status' => $status]);
        echo json_encode($r);
    }


    function get_inven_it($branch_id){
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'nomer_inventaris,nama_inventaris,os,ip,harga,branch_name,created_at';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'asc'; 
        $args = array();
        $kategori = $this->input->post("kategori");
        // $subkategori = $this->input->post("subkategori");
        // if ($subkategori == 0){
        //     $subkategori = null;
        // }
        $args['sort'] = '_.'.$sort;
        $args['order'] = $order;
        // $args['where'][] = 'br.branch_id = '.$branch_id.' and _.kategori like \''.$kategori.'%\''.' and sub_kategori = '.$subkategori;  
        $args['where'][] = 'br.branch_id = '.$branch_id.' and _.kategori like \''.$kategori.'%\''; 
        $r = $this->m_masterinventaris->many_invenitCategory($args);     
        $r['sort'] = '_.'.$sort;
        $r['order'] = $order;
        // echo $kategori;
        echo json_encode($r);
    }

    function get_sub_kategori(){
        $kategori = $this->input->post("kategori");
        $r = $this->m_masterinventaris->get_where(['kategori' => $kategori],'master_inventaris_it_subkategori')->result();    
        echo json_encode($r);
    }
    
	function check_inventIT()
    {
        $inventIT_form = $this
        ->input
        ->post('inventIT_form');
        $r = $this
            ->db
			->query("select a.*,b.branch_name from master_inventaris_it a join branch b on (a.branch_id = b.branch_id) where nomer_inventaris = '$inventIT_form'")
            ->result();
        // return $this->response($r);
        echo json_encode($r);
    }


}

