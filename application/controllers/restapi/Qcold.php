<?php

class Qc extends CI_Controller {

	function __construct(){
		parent::__construct();
		// header('Content-Type: application/json');
        $this->restapikey= $this->config->config['restapikey'];
		if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
		$this->user_id = $this->input->post("user_id");

        $this->load->model("restapi/user_model");
        $user = $this->user_model->one_user(["where" => ["_.id_user" => $this->user_id]]);
        if($user == null) {
        	echo json_encode(["status" => false, "message" => "user is invalid"]);
        	exit();
        }
        $this->load->model("m_masterqc");
        $this->load->model("m_qc");
	}

	/* start input qc */
	function many_input_qc(){
		$args = [
			"order" => [ ["id_qc_input", "desc"] ],
			'join_detail' => false,
			'include_info_hasil' => true
		];
		$search = $this->input->post('search');
		$branch = $this->input->post('branch_id');
		if(empty($search) == false) {
			$args['like'] = [
				[
					'cols' => ['b1.branch_name', 'usr.name', 'usr.username', 'mtest.test_name', 'ctrl.control_name'],
					'search' => $search
				]
			];
		}
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		// if(empty($start) == false && empty($length) == false) {
		if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
			$args['limit'] = [ intval($start), intval($length) ];
		}
		$id_test = $this->input->post('test');
		if(empty($id_test) == false) {
			$args["where"] = ['_.id_test' => $id_test];
		}
		if(empty($branch) == false) {
			$args['where']['_.branch_id'] = $branch ;
		}

		$r = $this->m_qc->many_input_qc($args);
		echo json_encode($r);
	}

	function one_input_qc(){
		$id = $this->input->post("id");
		$r = $this->m_qc->one_input_qc(['join_lot_number'=>true, 'where' => ['_.id_qc_input' => $id]]);
		echo json_encode($r);
	}
	/* end input qc */

	function many_hasil(){
		$user = $this->current_user();
		$search = $this->input->post('search');
		$id_qc_input = $this->input->post('qc_input');
		$branch = $user->branch_id;
		$id_alat = $this->input->post('id_alat');

		$test = $this->input->post('test');
		if (!empty($test)) {
			if(empty($branch) == false) {
				$args['where']['b1.branch_id'] = $branch;
			}
			if(empty($test) == false) {
				$args['where']['mtest.test_id'] = $test;
			}
			$args['where']['b1.branch_id'] = $branch;

			$r = $this->m_qc->dt_hasil_mobile($args);

			echo json_encode($r);
		} else {
				$args = [
					"order" => [ ["_.tanggal", "desc"], [ "_.id_hasil", 'desc' ] ],
				];
				if(strlen($search) > 0) {
					$args = [
						'where' => [],
						'join_detail_v2' => true,
						'join_detail' => false,
						'join_input_qc' => false
					];
						$args['like'] = [
							[
								'cols' => ['b1.branch_name'],
								'search' => $search
							]
						];
				}
				if(empty($id_qc_input) == false) {
					$args['where']['_.id_qc_input'] = $id_qc_input;
				}

				if(!empty($id_alat)) {
					$args['where']['Alat.id_alat'] = $id_alat;
				}

			$start = $this->input->post('start');
			$length = $this->input->post('length');
			// if($start != null && $length != null) {
			if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
				$args['limit'] = [ $start, $length ];
			}
			$r = $this->m_qc->many_hasil($args);
			echo json_encode($r);
		}
	}

	function detail_from_test(){
		$search = $this->input->post('search');
		$id_qc_input = $this->input->post('qc_input');
		$branch = $this->input->post('branch_id');
		$test = $this->input->post('test');
		if (!empty($test)) {
			if(empty($branch) == false) {
				$args['where']['b1.branch_id'] = $branch;
			}
			if(empty($test) == false) {
				$args['where']['mtest.test_id'] = $test;
			}
			$args['where']['b1.branch_id'] = $branch;

			$r = $this->m_qc->detail_from_test($args);

			echo json_encode($r);
		} else {
		if(strlen($search) > 0) {
		$args = [
			"order" => [ ["_.tanggal", "desc"], [ "_.id_hasil", 'desc' ] ],
			'where' => [],
			'join_detail_v2' => true,
			'join_detail' => false,
			'join_input_qc' => false
		];
			$args['like'] = [
				[
					'cols' => ['b1.branch_name'],
					'search' => $search
				]
			];
		}
		if(empty($id_qc_input) == false) {
			$args['where']['_.id_qc_input'] = $id_qc_input;
		}

		$start = $this->input->post('start');
		$length = $this->input->post('length');
		// if($start != null && $length != null) {
		if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
			$args['limit'] = [ $start, $length ];
		}
		$r = $this->m_qc->detail_from_test($args);
		echo json_encode($r);
	}
	}

	function many_group_test(){
		$args = [];
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$branch = $this->input->post('branch');
		$alat = $this->input->post('alat');
		$search = $this->input->post('search');
		// if($start != null && $length != null) {
		if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
			$args['limit'] = [ intval($start), intval($length) ];
		}
		if(empty($branch) == false) {
			// $args['where'] = [ '_.branch_id' => $branch ];
		}
		if(!empty($alat)) {
			// $args['where'] = [ '_.branch_id' => $alat ];
		}
		if(empty($search) == false) {
			$args['like'] = [
				[
					'cols' => ['_.group_name', 'usr.name', 'usr.username', 'b1.branch_name'],
					'search' => $search
				]
			];
		}
		$r = $this->m_masterqc->many_group_test($args);
		echo json_encode($r);
	}

	function many_test(){
		$args = [
			'include_info_qc' => true,
			'order' => [
				['urutan', 'asc'],
				['jumlah_iqc', 'desc'],
				['test_name', 'asc'],
			],
			'where' => []
		];
		$id_group_test = $this->input->post('id_group_test');
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search');
		$branch = $this->input->post('branch_id');

		if(empty($id_group_test) == false){
			$args['where'] = ['_.group_id' => $id_group_test];
		}
		if(empty($branch) == false) {
			$args['branch'] = $branch;
		}
		// if(empty($start)==false && empty($length)==false) {
		if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
			$args['limit'] = [intval($start), intval($length)];
		}
		if(empty($search) == false) {
			$args['like'] = [
				[
					'cols' => ['b1.branch_name', 'usr.name', 'usr.username', '_.test_name'],
					'search' => $search
				]
			];
		}
		$r = $this->m_masterqc->many_test($args);
		echo json_encode($r);
	}

	public function many_alat(){
		$user = $this->current_user();
		if ($user->branch_id == 16):
			$list_alat = $this->m_masterqc->many_alat(['order' => [ ['_.alat_name'] ] ]);
		else:
			$list_alat = $this->m_masterqc->many_alat(['order' => [ ['_.alat_name'] ] ],
                [
                    'where' => [
                        '_.branch_id' => $user->branch_id
                    ]
                ]
            );
		endif;
		echo json_encode($list_alat);
	}

	function many_lot_number(){
		$args = [];
		$r = $this->m_masterqc->many_lot_number($args);
		echo json_encode($r);
	}

	function data_chart(){
		$id_qc_input = $this->input->post("id_qc_input");
        $id_hasil = $this->input->post("id_hasil");
        $r = $this->m_qc->get_data_chart($id_qc_input, $id_hasil);
        echo json_encode($r);
	}

	public function current_user(){
        $query = $this->db->query("Select a.*,b.branch_name,c.name_position
            from users a
            join branch b on (a.branch_id = b.branch_id)
            join position c on (a.id_position = c.id_position)
            where id_user='$this->user_id'
        ");
        return $query->first_row();
    }

    function filter_lot_ctrl() {
        $user = $this->current_user();
        $args = [
            'where' => [
            	'b1.branch_id' => $user->branch_id
            ],
            'disable_paging' => 1 /*auto set*/
        ];

        /*
        	params
        	- branch_id
			- alat_name
			- test_name
        */
        $post_params = [
        	'branch_id' => $this->input->post('branch_id'),
        	'alat_name' => $this->input->post('alat_name'),
        	'test_name' => $this->input->post('test_name')
        ];

        if($user->branch_id == 16) {
            unset($args['where']['b1.branch_id']);
        }

        foreach ($post_params as $field => $value) {
        	if (!empty($value)) {
        		if ($field=='branch_id') {
        			$args['where']['b1.branch_id'] = $value;
        		}else{
        			$args['where'][$field. '  like '] = '%'.$value.'%';
        		}
        	}
        }


        $filter_group = $this->input->post('group');
        $r = $this->m_qc->select_lot_ctrl($args,$filter_group);

        $result['data'] = (isset($r['data'])&&!empty($r['data']))?$r['data']:[];
        $result['rows'] = count($result['data']);
        // $result['sql'] = $this->db->last_query();

        echo json_encode($result);
    }

    function data_input_qc($id = 0) {
        $user = $this->current_user();
        $args = [
            'where' => [
            	'b1.branch_id' => $user->branch_id
            ],
            'disable_paging' => 1 /*auto set*/
        ];

        if($user->branch_id == 16) {
            unset($args['where']['b1.branch_id']);
        }

        if ($id > 0) {
            $args['where']['_.id_qc_input'] = $id;
        }

        /*
        	params
        	- alat_name
			- test_name
			- lot_number
			- list_mean
			- list_sd
			- list_level_name
			- creator_name
			- branch_id
        */
        $post_params = [
        	'alat_name' => $this->input->post('alat_name'),
			'test_name' => $this->input->post('test_name'),
			'lot_number' => $this->input->post('lot_number'),
			'list_mean' => $this->input->post('list_mean'),
			'list_sd' => $this->input->post('list_sd'),
			'list_level_name' => $this->input->post('list_level_name'),
			'creator_name' => $this->input->post('creator_name'),
			'branch_id' => $this->input->post('branch_id')
        ];

		foreach ($post_params as $field => $value) {
        	if (!empty($value)) {
        		if ($field=='branch_id') {
        			$args['where']['b1.branch_id'] = $value;
        		}else{
        			$args['where'][$field. '  like '] = '%'.$value.'%';
        		}
        	}
        }

        $r = $this->m_qc->dt_input_qc($args);

        /*formatter*/
        $tmp_ = $r['data'];
        unset($r['data']);
        $id_qc_input = 0;
        foreach ($tmp_ as $key => $value) {
            $id_qc_input = $value['id_qc_input'];
            foreach ($value as $k => $v) {
                $r['data'][$key][$k] = $v;
                if ($k=='list_mean') {
                    if (!empty($v)) {
                        $mean_tmp = explode('-----', $v);
                        $r['data'][$key][$k] = '';
                        foreach ($mean_tmp as $im => $vm) {
                            $r['data'][$key][$k] .= '<br>'.($im+1).'). '.$vm;
                        }
                    }else{
                        $r['data'][$key][$k] = '-';
                    }

                }

                if ($k=='list_sd') {
                    if (!empty($v)) {
                        $sd_tmp = explode('-----', $v);
                        $r['data'][$key][$k] = '';
                        foreach ($sd_tmp as $is => $vs) {
                            $r['data'][$key][$k] .= '<br>'.($is+1).'). '.$vs;
                        }
                    }else{
                        $r['data'][$key][$k] = '-';
                    }

                }

                if ($k=='list_level_name') {
                    if (!empty($v)) {
                        $lvl_tmp = explode('-----', $v);
                        $r['data'][$key][$k] = '';
                        $level_caption = ['nol','satu','dua','tiga','empat','lima','enam','tujuh','delapan','sembilan','sepuluh'];
                        foreach ($lvl_tmp as $ilv => $vlv) {
                            $r['data'][$key][$k] .= '<br>'.$vlv.' ('.$level_caption[$vlv].')';
                        }
                    }else{
                        $r['data'][$key][$k] = '-';
                    }

                }

            }

        }
        /**/

        $result['data'] = (isset($r['data'])&&!empty($r['data']))?$r['data']:[];
        $result['rows'] = count($result['data']);
        // $result['sql'] = $this->db->last_query();
        echo json_encode($result);
    }

    function get_data_result() {
        $p_rows = !empty($this->input->post('limit'))?$this->input->post('limit'):10;
        $p_page = $this->input->post('page');

        $args = [
            'where' => [],
            'rest_paging' => [
            	'rows' => !empty($p_rows)?$p_rows:10,
            	'page' => !empty($p_page)?$p_page:1,
            ]
        ];

        $alat_test = $this->input->post('id_alat');
        $filter_param = [
            'tanggal' => $this->input->post('tanggal'),
			'group_test_name' => $this->input->post('group_test_name'),
			'test_name' => $this->input->post('test_name'),
			'lot_number' => $this->input->post('lot_number'),
			'list_mean' => $this->input->post('list_mean'),
			'list_sd' => $this->input->post('list_sd'),
			'list_hasil' => $this->input->post('list_hasil'),
			'list_level_name' => $this->input->post('list_level_name'),
			'periode' => $this->input->post('periode'),
			'branch_id' => $this->input->post('branch_id'),
        ];
        // default filter is this years data

        if(empty($alat_test) == false) {
            /*get alat name*/
            $alatObj = $this->m_qc->edit_data(['id_alat' => $alat_test],'master_qc_alat')->result();
            $alat_name = $alatObj[0]->alat_name;
            $args['where']['Alat.alat_name like '] = '%'.$alat_name.'%';
        }

        if (!empty($filter_param) && !empty($filter_param['periode'])) {
            $key = $filter_param['periode'];

            switch ($key) {
                case 'today':
                    // $args['disable_paging'] = 1;
                    $args['where']['tanggal'] = date('Y-m-d');
                    break;
                case 'week':
                    // $args['disable_paging'] = 1;
                    $date = date('Y-m-d');
                    $date = strtotime($date);
                    $this_week = date("W", $date)-1; /*selisih dgn mysql week*/
                    $args['where']['week(tanggal)'] = $this_week;
                    break;
                case 'month':
                    // $args['disable_paging'] = 0;
                    $args['where']['month(tanggal)'] = date('m');
                    $args['where']['year(tanggal)'] = date('Y');
                    break;
                case 'year':
                    unset($args['where']['month(tanggal)']);
                    $args['where']['year(tanggal)'] = date('Y');
                    // $args['disable_paging'] = 0;
                    break;
                case 'all':
                    // all data, no filter
                    // $args['disable_paging'] = 0;
                    $tmp_where['where']['Alat.alat_name like '] = $args['where']['Alat.alat_name like '];
                    unset($args);
                    unset($filter_param);
                    $args = $tmp_where;
                    break;
                default:
                    /**/
                    break;
            }

            if (isset($filter_param['periode'])) {
                unset($filter_param['periode']);
            }
        }

        $user = $this->current_user();
		if($user->branch_id == 16) {
            unset($args['where']['b1.branch_id']);
        }else{
        	$args['where']['b1.branch_id'] = $user->branch_id;
        }


		$i = 1;
        foreach ($filter_param as $field => $value) {
            if (!empty($value)) {
                if ($field=='branch_id') {
                    $args['where']['b1.branch_id'] = $value;
                }else{
                    $args['filter_where'][$i] = $value;
                }
            }
        }


    	$r = $this->m_qc->dt_hasil($args);

        $this->load->library('misc');
        /*formating date*/
        foreach ($r['data'] as $key => $value) {
            $r['data'][$key]['tanggal'] = $this->misc->dateFormat($value['tanggal']);
        }
        /**/

        $result['rows'] = count($r['data']);
        $result['page'] = $p_page;
        $result['page_total'] = ceil($r['recordsTotal']/$p_rows);
        $result['record_total'] = $r['recordsTotal'];
        $result['data'] = $r['data'];
    	echo json_encode($result);
    }

    public function get_branch()
    {
        $data = array();
        $data_branch = $this->m_qc->get_data('branch')->result();

        if (!empty($data_branch)) {
            $i = 0;
            foreach ($data_branch as $key => $value) {
                $data['data'][$i]['no'] = $i+1;
                $data['data'][$i]['branch_id'] = $value->branch_id;
                $data['data'][$i]['branch_name'] = $value->branch_name;
                $i++;
            }
        }
        $data['rows'] = count($data['data']);
        echo json_encode($data);
	}
	
	public function get_lot() {
		$this->db->select('*');
		$this->db->from('master_qc_lot_number');
		$this->db->join('master_control', 'master_control.id_control = master_qc_lot_number.id_control','left');
		$this->db->join('master_qc_group_level', 'master_qc_group_level.id_group_level = master_qc_group_level.id_group_level','left');
		$data = $this->db->get();
		$data = $data->first_row();
        echo json_encode($data);
	}
}
