<?php

class Homeservice extends CI_Controller {
    public $model = 'homeservice';

    public $model1 = 'hs_initial_petugas_hs';
    function __construct()
    {
        parent::__construct();
		header('Content-Type: application/json');
        $this->restapikey= $this->config->config['restapikey'];
        if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        $this->user_id = $this->input->post('user_id');
        $this->load->model("restapi/user_model");
        $user = $this->user_model->one_user(["where" => ["_.id_user" => $this->user_id]]);
        if($user == null) {
            echo json_encode(["status" => false, "message" => "user is invalid"]);
            exit();
        }

        $this->branch_id = $user['branch_id'];
    }


    public function get_hs()
    {
        $this->branch_id = $this->input->post('branch_id');
        $date = $this->input->post('date');
        $date = empty($date) ? date('Y-m-d') : $date;
        $data = array();
        $data_hs = $this->m_hs->get_data_by_branch($date, $this->input->post('branch_id'))->result();
        $data['hs_time'] = $this->m_hs->get_data('hs_time')->result();
        $data['hs_initial_petugas_hs'] = $this->m_hs->get_data_hs_initial_petugas_hs($this->branch_id, $date)->result();
        $data['data'] = [];
        if (!empty($data_hs)) {
            $i = 0;
            foreach ($data_hs as $key => $value) {
                $data['data'][$i]['id'] = $value->id;
                $data['data'][$i]['no'] = $i + 1;
                $data['data'][$i]['date'] = $value->date;
                $data['data'][$i]['pid'] = $value->pid;
                $time = substr($value->time_name, 0, 8);
                $data['data'][$i]['time_name'] = $time;
                $data['data'][$i]['petugas_id'] = $value->petugas_id;
                $data['data'][$i]['time_id'] = $value->time_id;
                $data['data'][$i]['ptgshs_id'] = $value->ptgshs_id;
                $data['data'][$i]['nama'] = $value->namacaps;
                $data['data'][$i]['alamat'] = $value->alamat;
                $data['data'][$i]['no_telp'] = $value->no_telp;
                $data['data'][$i]['dokter'] = $value->dokter;
                $data['data'][$i]['jumlah_pasien'] = $value->jumlah_pasien;
                $data['data'][$i]['pemeriksaan'] = $value->pemeriksaan;
                $data['data'][$i]['pay_id'] = $value->pay_id;
                $data['data'][$i]['pay_name'] = $value->pay_name;
                $data['data'][$i]['nama_petugas'] = $value->nama_petugas;
                $data['data'][$i]['image'] = $value->image;
                $data['data'][$i]['nama_petugas_hs'] = $value->nama_petugas_hs;
                $data['data'][$i]['ttd_pasien'] = $value->ttd_pasien;
                $data['data'][$i]['branch'] = $value->branch_name;
                $data['data'][$i]['branch_id'] = $value->branch_id;
                $data['data'][$i]['status'] = $value->status;
                $data['data'][$i]['catatan'] = $value->catatan;
                $data['data'][$i]['reason'] = $value->reason;
                $data['data'][$i]['time_selesai'] = $value->time_selesai;
                $data['data'][$i]['abbr_hs'] = $value->abbr_hs;
                $data['data'][$i]['creator'] = $value->name_creator;
                $data['data'][$i]['created_date'] = $value->created_date;

                $i++;
            }
        }
        echo json_encode($data);
    }

    public function setSelesai()
    {
        $result['status'] = false;
        $result['message'] = 'Data gagal disimpan.';

        $data['status'] = 'Selesai';
        $data['time_selesai'] = date('Y-m-d H:i:s');

        $id_hs = $this->input->post('id');

        if (!empty($id_hs)) {
            $where['id'] = $this->input->post('id');
            $id_hs = $this->m_hs->update_data($where, $data, $this->model);
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        } else {
            $id_hs = $this->m_hs->insert_data($data, $this->model);
        }
        if ($id_hs) {
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        }
        echo json_encode($result);
    }

    public function delete_hs()
    {
        $result['status'] = false;
        $result['message'] = 'Data gagal dihapus.';

        $data['status'] = 'Cancel';
        $data['reason'] = $this->input->post('reason');
        $where['id'] = $this->input->post('id');
        $id_hs = $this->m_hs->update_data($where, $data, $this->model);

        if ($id_hs) {
            $result['status'] = true;
            $result['message'] = 'Home service di cancel.';
        }

        echo json_encode($result);
    }

    public function editPasien(){
        $result['status'] = false;
        $result['message'] = 'Data gagal disimpan.';

        $data['jumlah_pasien'] = $this->input->post('jml_pasien');

        $id_hs = $this->input->post('id');

        if (!empty($id_hs)) {
            $where['id'] = $id_hs;
            $id_hs = $this->m_hs->update_data($where, $data, $this->model);
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        } 
        echo json_encode($result);
    }
    
    public function get_branch()
    {
        $data_branch = $this->m_hs->get_data('branch','branch_name')->result();
        echo json_encode($data_branch);
    }

    public function get_user() {
        $data = array();
        $this->db->select('id_user AS id,id_user,name');
        $this->db->from('users');
        $this->db->where("branch_id =".$this->input->post('branch_id'));
        // $this->db->where_in("id_position",[2,3,14]);
        // $this->db->where("hs_initial_petugas_hs.start_date <=", $date);
        // $this->db->where_not_in("hs_initial_petugas_hs.abbr_hs", "SELECT abbr_hs FROM hs_initial_petugas_hs WHERE abbr_hs LIKE '%Julita%' AND DAYOFWEEK('$date') IN (3,4,6,7)", false);
        // $this->db->join('users','users.id_user = hs_initial_petugas_hs.id_user');
        $getUser = $this->db->get();

        // $data_phs =$this->m_masterhs->get_all_relations('users')->result();

        // if (!empty($data_phs)) {
        //     $i = 0;
        //     foreach ($data_phs as $key => $value) {
        //         $data['data'][$i]['no'] = $i+1;
        //         $data['data'][$i]['id_user'] = $value->id_user;
        //         $data['data'][$i]['name'] = $value->name;
        //         $i++;
        //     }
        // }
        echo json_encode($getUser->result());
    }

public function get_phs_master(){
    $data = array();
    $branch_id = $this->input->post('branch_id');
    if ($branch_id){
        $branch = $branch_id;
    }else{
        $branch = $this->branch_id;
    }
    $data_phs = $this->m_masterhs->get_all_masterphs($branch)->result();

    if (!empty($data_phs)) {
        $i = 0;
        foreach ($data_phs as $key => $value) {
            $data['data'][$i]['no'] = $i+1;
            $data['data'][$i]['ptgshs_id'] = $value->ptgshs_id;
            $data['data'][$i]['id_user'] = $value->id_user;
            $data['data'][$i]['petugas_hs'] = $value->fullname;
            $data['data'][$i]['abbr_hs'] = $value->abbr_hs;
            $start_date = date('Y-m-d', strtotime($value->start_date) );
            $data['data'][$i]['start_date'] = $start_date;
            $end_date = date('Y-m-d', strtotime($value->end_date) );
            $data['data'][$i]['end_date'] = $end_date;
            $data['data'][$i]['branch_id'] = $value->branch_name;
            $data['data'][$i]['creator_id'] = $value->namefull;
            $created_at = date('d-M-y H:i:s', strtotime($value->created_at) );
            $data['data'][$i]['created_at'] = $created_at;$i++;
        }
    }
    echo json_encode($data);
}


public function get_phs(){
    $data = array();
    $branch_id = $this->input->post('branch_id');
    if ($branch_id){
        $branch = $branch_id;
    }else{
        $branch = $this->branch_id;
    }
    $date = $this->input->post('date');
    $date = empty($date) ? date('Y-m-d') : $date;
    $data_phs = $this->m_hs->get_data_hs_initial_petugas_hs($branch,$date)->result();

    if (!empty($data_phs)) {
        $i = 0;
        foreach ($data_phs as $key => $value) {
            $data['data'][$i]['no'] = $i+1;
            $data['data'][$i]['ptgshs_id'] = $value->ptgshs_id;
            // $data['data'][$i]['id_user'] = $value->id_user;
            $data['data'][$i]['petugas_hs'] = $value->name;
            // $data['data'][$i]['abbr_hs'] = $value->abbr_hs;
            // $start_date = date('Y-m-d', strtotime($value->start_date) );
            // $data['data'][$i]['start_date'] = $start_date;
            // $end_date = date('Y-m-d', strtotime($value->end_date) );
            // $data['data'][$i]['end_date'] = $end_date;
            // $data['data'][$i]['branch_id'] = $value->branch_name;
            // $data['data'][$i]['creator_id'] = $value->namefull;
            // $created_at = date('d-M-y H:i:s', strtotime($value->created_at) );
            // $data['data'][$i]['created_at'] = $created_at;$i++;
        }
    }
    echo json_encode($data);
}

    public function get_pay(){
        $data = array();
        $data_pay = $this->m_masterhs->get_all_masterpayment()->result();
        if (!empty($data_pay)) {
            $i = 0;
            foreach ($data_pay as $key => $value) {
                $data['data'][$i]['no'] = $i+1;
                $data['data'][$i]['pay_id'] = $value->pay_id;
                $data['data'][$i]['pay_name'] = $value->pay_name;
                $data['data'][$i]['creator_id'] = $value->fullname;
                $created_at = date('d-M-y H:i:s', strtotime($value->created_at) );
                $data['data'][$i]['created_at'] = $created_at;
                $i++;
            }
        }
        echo json_encode($data);
    }

    public function get_time(){
        $data = array();
        $branch_id = $this->input->post('branch_id');
        if ($branch_id){
            $branch = $branch_id;
        }else{
            $branch = $this->branch_id;
        }
        $data_time = $this->m_hs->get_branch_hs_time($branch)->result();

        if (!empty($data_time)) {
            $i = 0;
            foreach ($data_time as $key => $value) {
                $data['data'][$i]['no'] = $i+1;
                $data['data'][$i]['time_id'] = $value->time_id;
                $data['data'][$i]['time_name'] = $value->time_name;
                // $data['data'][$i]['branch_id'] = $value->branch_name;
                // $data['data'][$i]['creator_id'] = $value->fullname;
                $created_at = date('d-M-y H:i:s', strtotime($value->created_at) );
                $data['data'][$i]['created_at'] = $created_at;
                $i++;
            }
        }
        echo json_encode($data);
    }

    public function get_abbr(){
        $data = array();
        $data_abbr = $this->m_masterhs->get_all_masterabbr($this->branch_id)->result();

        if (!empty($data_abbr)) {
            $i = 0;
            foreach ($data_abbr as $key => $value) {
                $data['data'][$i]['no'] = $i+1;
                $data['data'][$i]['abbr_id'] = $value->abbr_id;
                $data['data'][$i]['abbr_name'] = $value->abbr_name;
                $data['data'][$i]['lokasi'] = $value->lokasi;
                $data['data'][$i]['branch_id'] = $value->branch_name;
                $data['data'][$i]['creator_id'] = $value->fullname;
                $created_at = date('d-M-y H:i:s', strtotime($value->created_at) );
                $data['data'][$i]['created_at'] = $created_at;
                $i++;
            }
        }
        echo json_encode($data);
    }

    public function get_block(){
        $data = array();
        $data_block = $this->m_masterhs->get_all_masterblock($this->branch_id)->result();

        if (!empty($data_block)) {
            $i = 0;
            foreach ($data_block as $key => $value) {
                $data['data'][$i]['no'] = $i+1;
                $data['data'][$i]['block_id'] = $value->block_id;
                $data['data'][$i]['ptgs_id'] = $value->abbr_hs;
                $data['data'][$i]['ptgs'] = $value->ptgs;
                $data['data'][$i]['nama'] = $value->nama;
                $data['data'][$i]['hari_id'] = $value->hari_id;
                $data['data'][$i]['time_id'] = $value->time_id;
                $data['data'][$i]['branch_id'] = $value->branch_name;
                $data['data'][$i]['creator_id'] = $value->fullname;
                $created_at = date('d-M-y H:i:s', strtotime($value->created_at) );
                $data['data'][$i]['created_at'] = $created_at;
                $i++;
            }
        }
        echo json_encode($data);
    }

    public function save_phs(){
        $result['status'] = false;
        $result['message'] = 'Data gagal disimpan.';

        $data['id_user'] = $this->input->post('id_user');
        $data['abbr_hs'] = $this->input->post('abbr_hs');
        $data['start_date'] = $this->input->post('start_date');
        $data['end_date'] = $this->input->post('end_date');
        $data['branch_id'] = $this->branch_id;
        $data['creator_id'] = $this->user_id;
        $data['created_at'] = date("Y-m-d H:i:s");

        $phs_id = $this->input->post('ptgshs_id');

        if (!empty($phs_id)) {
            $where['ptgshs_id'] = $this->input->post('ptgshs_id');
            $phs_id = $this->m_masterhs->update_data($where, $data, $this->model1);
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        } else {
            $phs_id = $this->m_masterhs->insert_data($data, $this->model1);
        }
        if ($phs_id) {
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        }

        echo json_encode($result);
    }

    public function save_hs(){
        $result['status'] = false;
        $result['message'] = 'Data gagal disimpan.';

        $data['date'] = date('Y-m-d',strtotime($this->input->post('date')));
        $data['pid'] = $this->input->post('pid');
        $data['time_id'] = $this->input->post('time_id');
        $time_id2 = $this->input->post('time_id2');
        $data['nama'] = $this->input->post('nama');
        $data['alamat'] = $this->input->post('alamat');
        $data['no_telp'] = $this->input->post('no_telp');
        $data['dokter'] = $this->input->post('dokter');
        $data['jumlah_pasien'] = $this->input->post('jumlah_pasien');
        $data['pemeriksaan'] = $this->input->post('pemeriksaan');
        $data['pay_id'] = $this->input->post('pay_id');
        $data['petugas_id'] = $this->user_id; //$this->input->post('petugas_id');
        $data['ptgshs_id'] = $this->input->post('ptgshs_id');
        $data['catatan'] = $this->input->post('catatan');
        $data['ttd_pasien'] = $this->input->post('ttd_pasien');
        $data['branch_id'] = $this->input->post('branch_id'); //$this->branch_id;
        $data['creator_id'] = $this->user_id;
        $data['created_date'] = date('Y-m-d H:i:s');

        $hs_id = $this->input->post('hs_id');

        if (!empty($hs_id)) {
            $where['id'] = $hs_id;
            $phs_id = $this->m_masterhs->update_data($where, $data, $this->model);
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        } else {
            $data['status'] = 'Diproses';
            if ($time_id2 && $time_id2 > $data['time_id']):
                for ($i = $data['time_id']; $i <= $time_id2; $i++):
                    $getTime = $this->m_hs->get_where(['time_id' => $i, 'ptgshs_id' => $data['ptgshs_id'], 'date' => $data['date'] ], $this->model)->num_rows();
                    if($getTime == 0):
                        $data['time_id'] = $i;
                        $phs_id = $this->m_masterhs->insert_data($data, $this->model);
                    endif;
                endfor;
            else:
                $phs_id = $this->m_masterhs->insert_data($data, $this->model);
            endif;
        }
		
		$data_ptgs = $this->m_hs->edit_data(array('ptgshs_id' => $data['ptgshs_id']),'hs_initial_petugas_hs')->row();
		$data_time1 = $this->m_hs->edit_data(array('time_id' => $this->input->post('time_id')),'hs_time')->row();
		
		$jam = $data_time1->time_name;
		if($this->input->post('time_id_sampai')!=''){
			$data_time2 = $this->m_hs->edit_data(array('time_id' => $this->input->post('time_id_sampai')),'hs_time')->row();
			$jam = $data_time1->time_name.' '.($data_time1->time_name!=$data_time2->time_name?' - '.$data_time2->time_name:"");
		}
		
		$this->load->model('restapi/user_model');
		$sender = $this->user_model->one_user(['where' => ['_.id_user' => $data_ptgs->id_user]]);
		$idReceivers[] = $sender['id_user'];
		if($sender == null) {
			echo json_encode(['status' => false, 'message' => 'Invalid Sender']);
			exit();
		}
		else{
			$this->load->library('Notify_lib');
			$ntf = new Notify_lib();
			$ntf->send(
					'New Home Service',
					$sender['name'] . ' - '.$sender['position_name'].' ('.$sender['branch_name'].') anda mendapatkan tugas atas nama pasien '.$data['nama'].' dan jam '.$jam,
					$this->user_id,
					$idReceivers,
					['id' => $phs_id,'date' => $data['date'],'branch_id' => $data['branch_id']],
					'homeservice',
					true
				);
		}
		
        if ($phs_id) {
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        }

        echo json_encode($result);
    }

    public function detail(){
        $id = $this->input->post('id');
        $detail = $this->m_hs->detail($id);
        echo json_encode($detail);
    }
}
