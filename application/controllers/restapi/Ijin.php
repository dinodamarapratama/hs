<?php

class Ijin extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->restapikey= $this->config->config['restapikey'];
		if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);

		$this->user_id = $this->input->get_post('user_id');
		$this->load->model('restapi/user_model');
        $this->me = $this->user_model->one_user(["where" => ["_.id_user" => $this->user_id]]);
        $this->model = 'maintain_ext';
		if($this->me == null) {
			echo json_encode(['status' => false, 'message' => 'Invalid user']);
			exit();
		}
		$this->load->model('m_ijin', 'cmodel');
	}

	function get(){
		$branch_id = intval($this->input->post('branch_id'));
		$start = $this->input->post('start');
        $length = $this->input->post('length');
        
		$r = $this->cmodel->get_all_ijin(null, $branch_id, null, $start, $length)->result();
		echo json_encode($r);
	}

	function get_by_id(){
		$ijin_id = intval($this->input->post('id'));
        
		$r = $this->cmodel->get_one_ijin($ijin_id)->first_row();
		echo json_encode($r);
	}
}