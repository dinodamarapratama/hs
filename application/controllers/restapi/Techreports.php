<?php

class Techreports extends CI_Controller {

    function __construct(){
        parent::__construct();

        $this->restapikey= $this->config->config['restapikey'];
        if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        $this->user_id = $this->input->post('user_id');
        $this->load->model('m_techreports', 'cmodel');
    }

    function get_data_inven(){

       $data = $this->cmodel->get_data_inventaris()->result();
       echo json_encode($data);

    }

    function count_report(){
        $search = $this->input->post('search');
        $status = $this->input->post('status');
        $user = $this->input->post('user');
        $bagian_id = $this->input->post('bagian_id');
        $tanggal = $this->input->post('tanggal');
        $args = [];
        if (!empty($tanggal)):
            $args['where']['date'] = date('Y-m-d',strtotime($tanggal));
        endif;
        if(empty($search) == false) {
            $args['like'] = [
                [
                    'search' => $search,
                    'cols' => [
                        '_.title', '_.kategori', 'rply.reply_all', 'usr.name', 'usr.username'
                    ]
                ]
            ];
        }

        $r1 = $this->cmodel->getCountReceived($this->user_id, $args);
        $r2 = $this->cmodel->getCountSent($this->user_id, $args);
        $r3 = $this->cmodel->getCountArchived($this->user_id, $args);
        $r = [
            'received' => $r1->jumlah,
            'sent' => $r2->jumlah,
            'archived' => $r3->jumlah
        ];
        echo json_encode($r);
    }

    function many_report(){
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $user = $this->input->post('user');
        $bagian_id = $this->input->post('bagian_id');
        $tanggal = $this->input->post('tanggal');
        $search = $this->input->post('search');
        $status = $this->input->post('status');

        $args = [
            'where' => [],
            // 'order' => [ ['_.id', 'desc'] ]
        ];

        if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
            $args['limit'] = [ $start, $length ];
        }

        if (isset($tanggal) && !empty($tanggal)):
            $args['where']['_.date'] = date('Y-m-d',strtotime($tanggal));
        endif;
        if (isset($user) && !empty($user)):
            $args['where']['_.from_id'] = $user;
        endif;
        if(empty($search) == false) {
            $args['like'] = [
                [
                    'search' => $search,
                    'cols' => [
                        '_.title', '_.kategori', 'rply.reply_all', 'usr.name', 'usr.username'
                    ]
                ]
            ];
        }

        $r = [];
        if($status == 'received') {
            $r = $this->cmodel->getReceived($this->user_id, $args);
        } else if($status == 'sent') {
            $r = $this->cmodel->getSent($this->user_id, $args);
        } else if($status == 'archived') {
            $r = $this->cmodel->getArchived($this->user_id, $args);
        }
        echo json_encode($r);
    }

    function save(){
        $id_report = $this->input->post('id');
        $data = [];
        if(empty($id_report)) {
            $data = [
                'from_id' => $this->user_id,
                'status' => $this->input->post('status'),
                'kategori' => $this->input->post('category'),
                'title' => $this->input->post('subject'),
                'is_view' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'last_update' => date('Y-m-d H:i:s'),
                'date' => date('Y-m-d'),
                'id_draft' => 0,
                'receivers' => $this->input->post('receivers'),
                'departments' => $this->input->post('department'),
                'to_branch' => $this->input->post('to_branch'),
                'bagian' => $this->input->post('bagian'),
                'vr_des' => $this->input->post('vr_des'),
                'vr_inventaris' => $this->input->post('vr_inventaris'),
                'vs_des' => $this->input->post('vs_des'),
                'vs_inventaris' => $this->input->post('vs_inventaris'),
                'vt_des' => $this->input->post('vt_des'),
                'vt_inventaris' => $this->input->post('vt_inventaris'),
                'gr_des' => $this->input->post('gr_des'),
                'gr_inventaris' => $this->input->post('gr_inventaris'),
            ];
        } else {
            $data = [
                // 'from_id' => $this->user_id,
                'status' => $this->input->post('status'),
                'kategori' => $this->input->post('category'),
                'title' => $this->input->post('subject'),
                // 'is_view' => 1,
                // 'created_at' => date('Y-m-d H:i:s'),
                // 'last_update' => date('Y-m-d H:i:s'),
                // 'date' => date('Y-m-d'),
                // 'id_draft' => 0,
                // 'receivers' => $this->input->post('receivers'),
                'to_branch' => $this->input->post('to_branch'),
            ];
        }
        
        $bodys = [
            'lmp_des', 'kf_des', 'lift_des', 'ef_des', 'ups_des', 'aq_des', 'pmp_des', 'pabx_des', 'wtp_des',
            'stp_ipal_des', 'ge_des', 'ac_des', 'lain_des'
        ];
        $inventaris = [
            'kg_inventaris', 'lmp_inventaris', 'kf_inventaris', 'lift_inventaris', 'ef_inventaris', 'ups_inventaris',
            'aq_inventaris', 'pmp_inventaris', 'pabx_inventaris', 'wtp_inventaris', 'stp_ipal_inventaris', 
            'ge_inventaris', 'ac_inventaris','lain_inventaris'
        ];
        for($i = 1, $i2 = count($bodys); $i <= $i2; $i++) {
            $key = 'body';
            if($i > 0) {
                $key = 'body' . $i;
            }
            $body = $this->input->post($key);
            if(empty($body) == false) {
                $data[$bodys[$i - 1]] = $body;
            } else {
                $data[$bodys[$i - 1]] = $i + 1;
            }
        }
        for($i = 0, $i2 = count($inventaris); $i < $i2; $i++) {
            /* ambil post dengan key inv_0, inv_1, inv_2 dst */
            $key = 'inv_' . $i;
            $inv = $this->input->post($key);
            if(empty($inv) == false) {
                $data[$inventaris[$i]] = $inv;
            }
        }
        $r = [];
        if(empty($id_report)) {
            array_push($data['receivers'], 75, 48);
            $r = $this->cmodel->save($data);
        } else {
            $r = $this->cmodel->update_report(['_.id' => $id_report], $data);
        }
       
        if($r['status'] == true) {
            $counter = -1;
            $list_upload = [];
            $attachment_descriptions = $this->input->post('attachments');
            $attachment_descriptions_length = is_array($attachment_descriptions) ? count($attachment_descriptions) : 0;
            while(true) {
                $counter++;
                $key = 'file_' . $counter;
                if(isset($_FILES[$key]) == false) {
                    break;
                }
                if($counter > 100) {
                    break;
                }
                $config = [
                    'upload_path' => 'uploads/attachments/monthlyreports/',
                    'allowed_types' => '*',
                    'encrypt_name' => true
                ];

                if(file_exists($config['upload_path']) == false) {
                    $old = umask(0);
                    mkdir($config['upload_path'], 0777, true);
                    umask($old);
                }

                $this->upload->initialize($config);

                if($this->upload->do_upload($key)) {
                    $uploaded = $this->upload->data();
                    $list_upload[] = $uploaded;
                    $desc = $this->input->post('file_desc_' . $counter);
                    $title = $this->input->post('file_title_' . $counter);
                    // if($counter < $attachment_descriptions_length) {
                    //     $d = $attachment_descriptions[$counter];
                    //     $desc = $d['description'];
                    //     $title = $d['title'];
                    // }
                    $this->cmodel->save_attachment([
                        'techreports_id' => $r['id'],
                        'title' => strlen($title) > 0 ? $title : $uploaded['orig_name'],
                        'description' => $desc,
                        'path' => $config['upload_path'] . $uploaded['file_name'],
                        'filetype' => $uploaded['file_type'],
                        'filesize' => $uploaded['file_size'],
                        'name' => $uploaded['orig_name'],
                    ]);
                }
            }
            $r['upload'] = $list_upload;
        }
        echo json_encode($r);
    }

    function delete(){
        $id = $this->input->get_post('id');
        $r = $this->cmodel->hapus_report(['_.id' => $id]);
        echo json_encode($r);
    }

    function detail(){
        $id = $this->input->get_post('id');
        $r = $this->cmodel->one_report(['where' => ['_.id' => $id]]);
        if($r != null) {
            $this->cmodel->mark_read($r->id, $this->user_id, $r);
            $this->cmodel->auto_close($r->id, $r);
        }
        echo json_encode($r);
    }

    function reply(){
        $id_reply = $this->input->post('reply_id');
        $r = [];
        if($id_reply == null) {
            $data = [
                'id_techreports' => $this->input->post('id'),
                'message_reply' => $this->input->post('message'),
                'sender' => $this->user_id,
                'date_added' => date('Y-m-d H:i:s')
            ];
            $r = $this->cmodel->save_reply($data);
        } else {
            $data = [
                'message_reply' => $this->input->post('message')
            ];
            $r = $this->cmodel->update_reply(['_.id' => $id_reply], $data);
        }
        
        if($r['status'] == true) {
            $counter = -1;
            $list_uploaded = [];
            $config = [
                'upload_path' => 'uploads/attachments/techreports/',
                'allowed_types' => '*',
                'encrypt_name' => true
            ];

            $old = umask(0);
            if(file_exists($config['upload_path']) == false) {
                mkdir($config['upload_path'], 0777, true);
            }
            umask($old);

            while(true) {
                $counter++;
                $key = 'file_' . $counter;
                if(isset($_FILES[$key]) == false) {
                    break;
                }
                if($counter > 100) {
                    break;
                }
                $this->upload->initialize($config);
                if($this->upload->do_upload($key)) {
                    $uploaded = $this->upload->data();
                    $list_uploaded[] = $uploaded;

                    $this->cmodel->save_reply_attachment([
                        'id_techreports_reply' => $r['id'],
                        'path' => $config['upload_path'] . $uploaded['file_name'],
                        'name' => $uploaded['orig_name'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'filetype' => $uploaded['file_type'],
                        'filesize' => $uploaded['file_size']
                    ]);
                } else {
                    $list_uploaded[] = 'error ' . $key;
                }
            }
            $r['upload'] = $list_uploaded;
        }

        echo json_encode($r);
    }

    function get_replies(){
        $id = $this->input->post('id');
        $r = $this->cmodel->getReplies($id);
        echo json_encode($r);
    }

    function hapus_reply(){
        $id_reply = $this->input->post('id_reply');
        $r = $this->cmodel->hapus_reply(['_.id' => $id_reply]);
        echo json_encode($r);
    }

    function update_status(){
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        $r = $this->cmodel->update_report(['_.id' => $id], ['status' => $status]);
        echo json_encode($r);
    }

}