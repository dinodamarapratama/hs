<?php

class Doc extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->uri->segment(3) !== "pdf"){
			$this->restapikey= $this->config->config['restapikey'];
			if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
			$this->user_id = $this->input->get_post('user_id');
			$this->load->model('restapi/user_model');
			$this->me = $this->user_model->one_user(['where' => ['_.id_user' => $this->user_id]]);
			if($this->me == null) {
				echo json_encode(['status' => false, 'message' => 'Invalid user']);
				exit();
			}
		}
		$this->load->model('m_masterdoc', 'cmodel');
	}

	public function index(){
		$search = $this->input->post('search');
		$data_doc = $this->m_masterdoc->get_all_masterdoc($search)->result();
        echo json_encode($data_doc);
	}
	
	public function get_doc_by_id()
    {
        $result['status'] = false;
        $result['message'] = 'Data tidak ditemukan.';

		
		$where['doc_id'] = $this->input->post('id');
		
		$result['data'] =0;
		
		$result['status'] = true;
		$result['message'] = 'Data ditemukan';
		$edit_data = $this->m_masterdoc->edit_data($where, 'master_doc')->row();
		if (!empty($edit_data)) {
			$result['data'] = (array)$edit_data;
			
			//WATERMARK
			//print_r(APPPATH.'../assets/logo/logo_daun.jpg');	
			$pdf = new \Mpdf\Mpdf();
			$pdf->showWatermarkImage = true;
			//$pdf->SetImportUse();
			$pagecount = $pdf->SetSourceFile(APPPATH.'../'.$result['data']['doc_pdf']);
			for ($i=1; $i<=($pagecount); $i++) {
				$pdf->AddPage();
				$pdf->WriteHTML(
				  '<watermarkimage src="'.APPPATH.'../assets/logo/watermark.jpg" alpha="0.2" size="70,70" position="140,200" />'
				);
				$import_page = $pdf->ImportPage($i);
				$pdf->UseTemplate($import_page);
			}
			$file = str_replace(".pdf","___.pdf",$result['data']['doc_pdf']);
			
			$pdf->Output(APPPATH.'../'.$file,'F');
			$result['data']['doc_pdf'] = $file;
			//WATERMARK
			
			
		}
	
		echo json_encode($result);
	}
	
	function pdf($id){
		$where['doc_id'] = $id;
		$edit_data = $this->m_masterdoc->edit_data($where, 'master_doc')->row_array();
		if (!empty($edit_data)) {
			$result['data'] = (array)$edit_data;
			
			//WATERMARK
			$pdf = new \Mpdf\Mpdf();
			$pdf->showWatermarkImage = true;
			$pagecount = $pdf->SetSourceFile($_SERVER['DOCUMENT_ROOT'].'/'.$result['data']['doc_pdf']);
			for ($i=1; $i<=($pagecount); $i++) {
				$pdf->AddPage();
				$pdf->WriteHTML(
					'<watermarkimage src="'.APPPATH.'../assets/logo/watermark.jpg" alpha="0.2" size="70,70" position="140,200" />'
				);
				$import_page = $pdf->ImportPage($i);
				$pdf->UseTemplate($import_page);
			}
			$file = str_replace(".pdf","___.pdf",$result['data']['doc_pdf']);
			
			$pdf->Output(APPPATH.'../'.$file,'I');		
		}
    }

	public function cek_password()
    {
		$password = $this->input->post('password');
		
		$cek = $this->m_masterdoc->edit_data(array('password'=>strtoupper(sha1('Bio Medika'.$password)),'id_user'=>$this->user_id), 'users')->row();
		if (!empty($cek)) {
			echo '1';
		}
		else
			echo '0';
    }
	
}
