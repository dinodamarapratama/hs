<?php

class Announcement extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->restapikey= $this->config->config['restapikey'];
		if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);

		$this->user_id = $this->input->get_post('user_id');
		$this->load->model('restapi/user_model');
		$this->me = $this->user_model->one_user(['where' => ['_.id_user' => $this->user_id]]);
		if($this->me == null) {
			echo json_encode(['status' => false, 'message' => 'Invalid user']);
			exit();
		}
		$this->load->model('m_announcement', 'cmodel');
	}

	function my_announcement(){
		$args = [
			'where' => [],
			// 'order' => [ ['_.created_at', 'asc'] ]
		];

		$args['year'] = intval($this->input->post('year'));
		$args['month'] = intval($this->input->post('month'));
		$args['id_user'] = $this->user_id;

		$sent = $this->cmodel->getSent($args);
		$received = $this->cmodel->getReceived($args);
		
		$r = [
			'sent' => $sent,
			'received' => $received
		];

		echo json_encode($r);
	}
	
	function detail()
    {
        $id = $this->input->post('id');
		$announcement = $this->cmodel->detail($id, $this->user_id);
        echo json_encode($announcement);
    }

	function save_announcement(){
		$d = $this->input->post('data');
		// var_dump($d);
		$d = json_decode($d, true);

		// print_r($d);
		// exit();

		$data = [
			'start' => $d['start'],
			'end' => $d['end'],
			'created_at' => date('Y-m-d H:i:s'),
			'note' => $d['notes'],
			'title' => $d['title'],
			'event_type' => $d['event_type'] == 0 ? 'EVENT' : 'NON EVENT',
			'receivers' => $d['receivers'],
			'creator_id' => $this->user_id,
			'bagian' => $d['bagian'],
			'repeat_type' => (isset($d['repeat_type'])?$d['repeat_type']:"0"),
            'time_repeat' => (isset($d['time_repeat'])?$d['time_repeat']:""),
		];

		$r = $this->cmodel->add_announcement($data);

		if($r['status'] == true) {
			$counter = -1;
			$config = [
				'upload_path' => './uploads/attachments/announcement/' . $r['id'] . '/',
				'allowed_types' => '*',
				'encrypt_name' => true
			];

			if(file_exists($config['upload_path']) == false) {
				$old = umask(0);
				mkdir($config['upload_path'], 0777, true);
				umask($old);
			}

			while(true) {
				$counter++;
				$key = 'file_' . $counter;
				if(isset($_FILES[$key]) == false) {
					break;
				}

				$this->upload->initialize($config);

				if($this->upload->do_upload($key)) {
					$d = $this->upload->data();
					$this->cmodel->add_attachment([
						'id_fk' => $r['id'],
						'filesize' => $d['file_size'],
						'filetype' => $d['file_type'],
						'path' => $config['upload_path'] . $d['file_name'],
						'name' => $d['orig_name'],
						'created_at' => date('Y-m-d H:i:s')
					]);
				}
			}
			$data_parse = [
				"id" => $r['id'],
				"notif_type" => "announcement",
				"id_announcement" => $r['id']
			];
			/*$this->notify_lib->send(
				$d['title'], //title
				$d['notes'],        //message
				$this->user_id,     //sender
				$d['receivers'],             //list receiver
				$data_parse,        //attachment
				"announcement",          //type notifikasi
				true               //kirim ke onesignal
			);*/
		}
		echo json_encode($r);
	}
}
