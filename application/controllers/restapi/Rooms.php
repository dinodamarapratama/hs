<?php

class Rooms extends CI_Controller {

    function __construct(){
        parent::__construct();
        // header('Content-Type: application/json');
        $this->restapikey= $this->config->config['restapikey'];
        if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        $this->user_id = $this->input->post("user_id");

        $this->load->model("restapi/user_model");
        $user = $this->user_model->one_user(["where" => ["_.id_user" => $this->user_id]]);
        if($user == null) {
            echo json_encode(["status" => false, "message" => "user is invalid"]);
            exit();
        }

        $this->branch_id = $user['branch_id'];
        $this->load->model(['m_rooms','m_ijin']);
        $this->load->library('misc');
    }


    /* -- master data -- */
    public function get_master_room(){

        $param['b.branch_id'] = $this->branch_id;
        if ($this->branch_id == 16) {
            unset($param['b.branch_id']);
        }

        $id_room = $this->input->post('id_room');
        if (!empty($id_room)&& $id_room > 0) {
            $param = ['m_r.id_room' =>  $id_room];
        }

        $source = [
            'query_select' => 'm_r.*,b.branch_name,u.name',
            'master_table' => 'master_rooms m_r',
            'relations' => [
                'branch b' => [
                    'conditions' => 'm_r.branch_id = b.branch_id',
                    'join_type' => 'left'
                ],
                'users u' => [
                    'conditions' => 'm_r.creator_id = u.id_user',
                    'join_type' => 'left'
                ]
            ],
        ];
        $data = $this->m_rooms->get_all_data($param,$source)->result();
        
        $new_data = [];
        
        $new_data['data'] = [];
        foreach ($data as $key => $value) {
            $new_data['data'][] = [ 
                            'id' => $value->id_room,
                            'no' => ($key+1),
                            'room_name' => $value->room_name,
                            'nama_branch' => $value->branch_name,
                            'keterangan' => $value->keterangan,
                            'creator_name' => $value->name
                        ];
        }

        $new_data['rows'] = count($new_data['data']);
        echo json_encode($new_data);
    }

    public function save_room(){

        $data = $this->input->post();
        $data['branch_id'] = $this->branch_id;
        $data['creator_id'] = $this->user_id;
        $key_update = $data['id_room'];
        unset($data['id_room']);
        $res['status'] = false;
        $edit = 0;

        if ($key_update > 0) {
            $edit = 1;
        }

        if ($edit) {

            $res['message'] = 'update data gagal';
            $save = $this->m_rooms->update_data(['id_room' => $key_update],$data,'master_rooms');
            if ($save) {
                $res['status'] = true;
                $res['message'] = 'update data berhasil';
            }
        }else{

            $res['message'] = 'simpan data gagal';
            $save = $this->m_rooms->save_data($data,'master_rooms');
            if ($save) {
                $res['status'] = true;
                $res['message'] = 'simpan data berhasil';
            }
        }

        echo json_encode($res);
    }

    public function hapus_data(){
        $result['status'] = false;
        $result['message'] = 'Data gagal dihapus.';

        $key = $this->input->post('id_room');
        $delete = $this->m_rooms->hapus_data(['id_room' => $key],'master_rooms');

        if($delete){
            $result['status'] = true;
            $result['message'] = 'Data berhasil dihapus.';
        }

        echo json_encode($result);
    }

    /* -- end master data -- */

    /* -- schedules ---*/
    public function save_schedules(){

        $hari = $this->misc->dayEnToId();
        $data = $this->input->post();
        $data['creator_id'] = $this->user_id;
        $key_update = $data['schedule_id'];
        $data['id_room'] = $data['room_id'];
        $data['day'] = $hari[date('D')];
        unset($data['schedule_id']);
        unset($data['room_id']);
        $res['status'] = false;
        $edit = 0;

        if ($data['id_room'] > 0) {
            $edit = 1;
        }

        if ($edit) {

            $res['message'] = 'update data gagal';
             $source = [
                'query_select' => 'r_s.*',
                'master_table' => 'room_schedules r_s',
                'relations' => [],
            ];
            $param = [
                        'r_s.id_room' =>  $data['id_room'],
                        'r_s.date_input' =>  $data['date_input'],  
                        'r_s.start_time <=' =>  $data['start_time'], //start time beetween saved start_time & end_time
                        'r_s.end_time >=' =>  $data['start_time'],
                    ];
            $cek_data = $this->m_rooms->get_all_data($param,$source)->result();
            
            if (empty($cek_data)) {
                /*get data origin before update*/
                $_origin_data = $this->m_rooms->get_all_data(['r_s.id'=>$key_update],$source)->result();
                /**/
                switch ($data['status']) {
                    case 'weekly':
                        # save weekly by date input on same month
                        # mingguan sebelum tgl input & sesudah tgl input
                        $jml_hari = date('t');
                        $hari_mulai = 1;
                        $weeks_by_date = [];
                        $weeks_by_date[] = $data['date_input'];
                        /**/
                        $weeks_by_date_ori = [];
                        $weeks_by_date_ori[] = $_origin_data[0]->date_input;

                        $tgl = date_format(date_create($data['date_input']),'d');
                        $tgl_search = date_format(date_create($_origin_data[0]->date_input),'d');

                        for ($i=1; $i <= 5 ; $i++) { 
                            $tgl_ = $tgl - 7;
                            $tgl_origin = $tgl_search - 7;

                            if ($tgl_ >= $hari_mulai && $tgl_ <= $jml_hari) {
                                $weeks_by_date[] = date('Y').'-'.date('m').'-'.$tgl_;
                                $tgl = $tgl_;      
                            }

                            if ($tgl_origin >= $hari_mulai && $tgl_origin <= $jml_hari) {
                                $weeks_by_date_ori[] = date('Y').'-'.date('m').'-'.$tgl_origin;
                                $tgl_search = $tgl_origin;      
                            }

                        }

                        $tgl = date_format(date_create($data['date_input']),'d');
                        $tgl_search = date_format(date_create($_origin_data[0]->date_input),'d');
                        for ($i=1; $i <= 5 ; $i++) { 
                            $tgl__ = $tgl + 7;
                            $tgl__origin = $tgl_search + 7;

                            if ($tgl__ >= $hari_mulai && $tgl__ <= $jml_hari) {
                                $weeks_by_date[] = date('Y').'-'.date('m').'-'.$tgl__;
                                $tgl = $tgl__;
                            }
                            if ($tgl__origin >= $hari_mulai && $tgl__origin <= $jml_hari) {
                                $weeks_by_date_ori[] = date('Y').'-'.date('m').'-'.$tgl__origin;
                                $tgl_search = $tgl__origin;
                            }
                        }

                        /*delete old data*/
                        foreach ($weeks_by_date_ori as $key => $value) {
                            $param = [
                                        'r_s.id_room' =>  $_origin_data[0]->id_room,
                                        'r_s.date_input' =>  isset($weeks_by_date_ori[$key])?$weeks_by_date_ori[$key]:'',  
                                        'r_s.start_time <=' => $_origin_data[0]->start_time,
                                        'r_s.end_time >=' =>  $_origin_data[0]->start_time,
                                    ];
                            $_data = $this->m_rooms->get_all_data($param,$source)->result();
                            $key_delete = $_data[0]->id;
                            $this->m_rooms->hapus_data(['id'=>$key_delete],'room_schedules');
                        }
                        /**/
                        foreach ($weeks_by_date as $key => $dateToSave) {
                            $data['date_input'] = $dateToSave;
                            $tgl_day = date_format(date_create($data['date_input']),'D');
                            $data['day'] = $hari[$tgl_day];
                            $save = $this->m_rooms->save_data($data,'room_schedules');
                        }
                        die;
                        break;

                    case 'monthly':
                        # save monthly by date input on 1 years
                        $bulan = $this->misc->getBulan();
                        $cur_date = $data['date_input'];
                        $ori_date = $_origin_data[0]->date_input;
                        foreach ($bulan as $key => $value) {
                            $month = $key;
                            if ($key < 10) {
                                $month = '0'.$key; 
                            }
                            $date_day = date_format(date_create($cur_date),'d');
                            $data['date_input'] = date('Y').'-'.$month.'-'.$date_day;
                            $date_day_search = date_format(date_create($ori_date),'d');
                            $date_search =  date('Y').'-'.$month.'-'.$date_day_search;
                            /*pengecualian utk bulan 2*/
                            if ($key == 2) {
                                $date_def = date('Y').'-'.$month.'-01';
                                $dd = date_format(date_create($date_def),'t');
                                if ($date_day > $dd) {
                                    $date_day = $dd;
                                }
                                $data['date_input'] = date('Y').'-'.$month.'-'.$date_day;

                                if ($date_day_search > $dd) {
                                    $date_search =  date('Y').'-'.$month.'-'.$dd;
                                }

                                
                            }
                            /**/
                            $tgl_day = date_format(date_create($data['date_input']),'D');
                            $data['day'] = $hari[$tgl_day];
                            /*get id off data first*/
                            $param = [
                                        'r_s.id_room' =>  $_origin_data[0]->id_room,
                                        'r_s.date_input' =>  $date_search,  
                                        'r_s.start_time <=' => $_origin_data[0]->start_time,
                                        'r_s.end_time >=' =>  $_origin_data[0]->start_time,
                                    ];
                            $_data = $this->m_rooms->get_all_data($param,$source)->result();
                            $key_update = $_data[0]->id;
                            /**/
                            $save = $this->m_rooms->update_data(['id' => $key_update],$data,'room_schedules');
                        }
                        break;
                    
                    case 'single':
                    case 'daily':
                    default:
                        # single save data
                        $save = $this->m_rooms->update_data(['id' => $key_update],$data,'room_schedules');
                        break;
                }
                $res['save'] = $save;
                if ($save) {
                    $res['status'] = true;
                    $res['message'] = 'update data berhasil';
                }    
            }else{
                $res['status'] = false;
                $res['message'] = 'update data gagal, jadwal sudah ada!';
            }
        }else{

            $res['message'] = 'simpan data gagal';
            $source = [
                'query_select' => 'r_s.*',
                'master_table' => 'room_schedules r_s',
                'relations' => [],
            ];
            $param = [
                        'r_s.id_room' =>  $data['id_room'],
                        'r_s.date_input' =>  $data['date_input'],  
                        'r_s.start_time <=' =>  $data['start_time'], //start time beetween saved start_time & end_time
                        'r_s.end_time >=' =>  $data['start_time'],
                    ];
            $cek_data = $this->m_rooms->get_all_data($param,$source)->result();

            if (empty($cek_data)) {
                switch ($data['status']) {
                    case 'daily':
                        # save daily by date input on 1 month
                        # setiap hari dalam 1 bulan, tgl sebelum & sesudah input dalam bulan yg sama
                        $jml_hari = date('t');
                        for ($i=1; $i <= $jml_hari ; $i++) { 
                            $day = $i;
                            if ($i < 10) {
                                $day = '0'.$i; 
                            }
                            $data['date_input'] = date('Y-m-').$day;
                            $tgl_day = date_format(date_create($data['date_input']),'D');
                            $data['day'] = $hari[$tgl_day];
                            $save = $this->m_rooms->save_data($data,'room_schedules');
                        }
                        break;

                    case 'weekly':
                        # save weekly by date input on same month
                        # mingguan sebelum tgl input & sesudah tgl input
                        $jml_hari = date('t');
                        $hari_mulai = 1;
                        $weeks_by_date = [];
                        $weeks_by_date[] = $data['date_input'];
                        $tgl = date_format(date_create($data['date_input']),'d');
                        for ($i=1; $i <= 5 ; $i++) { 
                            $tgl_ = $tgl - 7;

                            if ($tgl_ >= $hari_mulai && $tgl_ <= $jml_hari) {
                                $weeks_by_date[] = date('Y').'-'.date('m').'-'.$tgl_;
                                $tgl = $tgl_;      
                            }

                        }

                        $tgl = date_format(date_create($data['date_input']),'d');
                        for ($i=1; $i <= 5 ; $i++) { 
                            $tgl__ = $tgl + 7;

                            if ($tgl__ >= $hari_mulai && $tgl__ <= $jml_hari) {
                                $weeks_by_date[] = date('Y').'-'.date('m').'-'.$tgl__;
                                $tgl = $tgl__;
                            }
                        }


                        foreach ($weeks_by_date as $key => $dateToSave) {
                            $data['date_input'] = $dateToSave;
                            $tgl_day = date_format(date_create($data['date_input']),'D');
                            $data['day'] = $hari[$tgl_day];
                            $save = $this->m_rooms->save_data($data,'room_schedules');
                        }

                        break;

                    case 'monthly':
                        # save monthly by date input on 1 years
                        $bulan = $this->misc->getBulan();
                        $cur_date = $data['date_input'];
                        foreach ($bulan as $key => $value) {
                            $month = $key;
                            if ($key < 10) {
                                $month = '0'.$key; 
                            }
                            $date_day = date_format(date_create($cur_date),'d');
                            $data['date_input'] = date('Y').'-'.$month.'-'.$date_day;
                            /*pengecualian utk bulan 2*/
                            if ($key == 2) {
                                $date_def = date('Y').'-'.$month.'-01';
                                $dd = date_format(date_create($date_def),'t');
                                if ($date_day > $dd) {
                                    $date_day = $dd;
                                }
                                $data['date_input'] = date('Y').'-'.$month.'-'.$date_day;
                            }
                            /**/
                            $tgl_day = date_format(date_create($data['date_input']),'D');
                            $data['day'] = $hari[$tgl_day];
                            $save = $this->m_rooms->save_data($data,'room_schedules');
                        }
                        break;
                    
                    case 'single':
                    default:
                        # single save data
                        $save = $this->m_rooms->save_data($data,'room_schedules');
                        break;
                }
                $res['save'] = $save;
                if ($save) {
                    $res['status'] = true;
                    $res['message'] = 'simpan data berhasil';
                }    
            }else{
                $res['status'] = false;
                $res['message'] = 'simpan data gagal, jadwal sudah ada!';
            }
            
        }

        echo json_encode($res);
    }

    public function get_schedules(){
        $param['m_r.branch_id'] = $this->branch_id;
        if ($this->branch_id == 16) {
            unset($param['m_r.branch_id']);
        }

        $rs_id = $this->input->post('schedule_id');
        if (!empty($rs_id) && $rs_id > 0) {
            $param = ['r_s.id' =>  $rs_id];    
        }

        $source = [
            'query_select' => 'r_s.*,b.branch_name,m_r.room_name,u.name',
            'master_table' => 'room_schedules r_s',
            'relations' => [
                'master_rooms m_r' => [
                    'conditions' => 'm_r.id_room = r_s.id_room',
                    'join_type' => 'left'
                ],
                'branch b' => [
                    'conditions' => 'm_r.branch_id = b.branch_id',
                    'join_type' => 'left'
                ],
                'users u' => [
                    'conditions' => 'm_r.creator_id = u.id_user',
                    'join_type' => 'left'
                ]
            ],
            'order' => 'r_s.date_input'
        ];
        $data = $this->m_rooms->get_all_data($param,$source)->result();
        
        $new_data = [];
        
        foreach ($data as $key => $value) {
            $new_data['data'][] = [ 
                            'id' => $value->id,
                            'no' => ($key+1),
                            'room_name' => $value->room_name,
                            'branch_name' => $value->branch_name,
                            'notes' => $value->notes,
                            'date_input' => $this->misc->dateFormat($value->date_input),
                            'start_time' => $value->start_time,
                            'end_time' => $value->end_time,
                            'status' => $value->status,
                            'day' => $value->day,
                            'creator_name' => $value->name
                        ];
        }

        $new_data['rows'] = count($new_data['data']);
        echo json_encode($new_data);
    }

    function hapus_schedules(){
        $result['status'] = false;
        $result['message'] = 'Data gagal dihapus.';

        $key = $this->input->post('schedule_id');
        $delete = $this->m_rooms->hapus_data(['id' => $key],'room_schedules');

        if($delete){
            $result['status'] = true;
            $result['message'] = 'Data berhasil dihapus.';
        }

        echo json_encode($result);
    }

    /* -- end schdules --*/

    /* event calender*/
    public function get_event(){
        $params=[];
        $today = $this->input->post('today');
        $tanggal = $this->input->post('tanggal');
        if ($today) {
            $params['r_s.date_input'] = date('Y-m-d');    
        }

        if (!empty($tanggal)) {
            $params['r_s.date_input'] = $tanggal; /*assume format Y-m-d*/   
        }
        
        $source = [
            'query_select' => 'r_s.*,b.branch_name,m_r.room_name',
            'master_table' => 'room_schedules r_s',
            'relations' => [
                'master_rooms m_r' => [
                    'conditions' => 'm_r.id_room = r_s.id_room',
                    'join_type' => 'left'
                ],
                'branch b' => [
                    'conditions' => 'm_r.branch_id = b.branch_id',
                    'join_type' => 'left'
                ]
            ],
        ];
        $data = $this->m_rooms->get_all_data($params,$source)->result();

        $new_format = [];
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $new_format[$key] = [
                    'title' => $value->notes.' ('.$value->room_name.')',
                    'start' => $value->date_input.' '.$value->start_time,
                    'end' => $value->date_input.' '.$value->end_time,
                    'room_id' => $value->id_room,
                    'id' => $value->id 
                ];
            }

            $result['data'] = $new_format;
        }
        
        $result['rows'] = count($result['data']);
        echo json_encode($result);
        
    }

    public function get_data_jam(){
        $data_jam = $this->misc->getOfficeHour(8,17,30);
        echo json_encode($data_jam);
    }
    /**/

}