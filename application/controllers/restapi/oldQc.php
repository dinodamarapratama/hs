<?php

class Qc extends CI_Controller {

	function __construct(){
		parent::__construct();
		// header('Content-Type: application/json');
        $this->restapikey= $this->config->config['restapikey'];
		if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
		$this->user_id = $this->input->post("user_id");

        $this->load->model("restapi/user_model");
        $user = $this->user_model->one_user(["where" => ["_.id_user" => $this->user_id]]);
        if($user == null) {
        	echo json_encode(["status" => false, "message" => "user is invalid"]);
        	exit();
        }
        $this->load->model("m_masterqc");
        $this->load->model("m_qc");
	}

	/* start input qc */
	function many_input_qc(){
		$args = [
			"order" => [ ["id_qc_input", "desc"] ],
			'join_detail' => false,
			'include_info_hasil' => true
		];
		$search = $this->input->post('search');
		$branch = $this->input->post('branch_id');
		if(empty($search) == false) {
			$args['like'] = [
				[
					'cols' => ['b1.branch_name', 'usr.name', 'usr.username', 'mtest.test_name', 'ctrl.control_name'],
					'search' => $search
				]
			];
		}
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		// if(empty($start) == false && empty($length) == false) {
		if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
			$args['limit'] = [ intval($start), intval($length) ];
		}
		$id_test = $this->input->post('test');
		if(empty($id_test) == false) {
			$args["where"] = ['_.id_test' => $id_test];
		}
		if(empty($branch) == false) {
			$args['where']['_.branch_id'] = $branch ;
		}

		$r = $this->m_qc->many_input_qc($args);
		echo json_encode($r);
	}

	function one_input_qc(){
		$id = $this->input->post("id");
		$r = $this->m_qc->one_input_qc(['join_lot_number'=>true, 'where' => ['_.id_qc_input' => $id]]);
		echo json_encode($r);
	}
	/* end input qc */

	function many_hasil(){
		$user = $this->current_user();
		$search = $this->input->post('search');
		$id_qc_input = $this->input->post('qc_input');
		$branch = $user->branch_id;
		$id_alat = $this->input->post('id_alat');

		$test = $this->input->post('test');
		if (!empty($test)) {
			if(empty($branch) == false) {
				$args['where']['b1.branch_id'] = $branch;
			}
			if(empty($test) == false) {
				$args['where']['mtest.test_id'] = $test;
			}
			$args['where']['b1.branch_id'] = $branch;

			$r = $this->m_qc->dt_hasil_mobile($args);

			echo json_encode($r);
		} else {
				$args = [
					"order" => [ ["_.tanggal", "desc"], [ "_.id_hasil", 'desc' ] ],
				];
				if(strlen($search) > 0) {
					$args = [
						'where' => [],
						'join_detail_v2' => true,
						'join_detail' => false,
						'join_input_qc' => false
					];
						$args['like'] = [
							[
								'cols' => ['b1.branch_name'],
								'search' => $search
							]
						];
				}
				if(empty($id_qc_input) == false) {
					$args['where']['_.id_qc_input'] = $id_qc_input;
				}

				if(!empty($id_alat)) {
					$args['where']['Alat.id_alat'] = $id_alat;
				}

			$start = $this->input->post('start');
			$length = $this->input->post('length');
			// if($start != null && $length != null) {
			if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
				$args['limit'] = [ $start, $length ];
			}
			$r = $this->m_qc->many_hasil($args);
			echo json_encode($r);
		}
	}

	function detail_from_test(){
		$search = $this->input->post('search');
		$id_qc_input = $this->input->post('qc_input');
		$branch = $this->input->post('branch_id');
		$test = $this->input->post('test');
		if (!empty($test)) {
			if(empty($branch) == false) {
				$args['where']['b1.branch_id'] = $branch;
			}
			if(empty($test) == false) {
				$args['where']['mtest.test_id'] = $test;
			}
			$args['where']['b1.branch_id'] = $branch;

			$r = $this->m_qc->detail_from_test($args);

			echo json_encode($r);
		} else {
		if(strlen($search) > 0) {
		$args = [
			"order" => [ ["_.tanggal", "desc"], [ "_.id_hasil", 'desc' ] ],
			'where' => [],
			'join_detail_v2' => true,
			'join_detail' => false,
			'join_input_qc' => false
		];
			$args['like'] = [
				[
					'cols' => ['b1.branch_name'],
					'search' => $search
				]
			];
		}
		if(empty($id_qc_input) == false) {
			$args['where']['_.id_qc_input'] = $id_qc_input;
		}

		$start = $this->input->post('start');
		$length = $this->input->post('length');
		// if($start != null && $length != null) {
		if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
			$args['limit'] = [ $start, $length ];
		}
		$r = $this->m_qc->detail_from_test($args);
		echo json_encode($r);
	}
	}

	function many_group_test(){
		$args = [];
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$branch = $this->input->post('branch');
		$alat = $this->input->post('alat');
		$search = $this->input->post('search');
		// if($start != null && $length != null) {
		if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
			$args['limit'] = [ intval($start), intval($length) ];
		}
		if(empty($branch) == false) {
			// $args['where'] = [ '_.branch_id' => $branch ];
		}
		if(!empty($alat)) {
			// $args['where'] = [ '_.branch_id' => $alat ];
		}
		if(empty($search) == false) {
			$args['like'] = [
				[
					'cols' => ['_.group_name', 'usr.name', 'usr.username', 'b1.branch_name'],
					'search' => $search
				]
			];
		}
		$r = $this->m_masterqc->many_group_test($args);
		echo json_encode($r);
	}

	function many_test(){
		$args = [
			'include_info_qc' => true,
			'order' => [
				['urutan', 'asc'],
				['jumlah_iqc', 'desc'],
				['test_name', 'asc'],
			],
			'where' => []
		];
		$id_group_test = $this->input->post('id_group_test');
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search');
		$branch = $this->input->post('branch_id');

		if(empty($id_group_test) == false){
			$args['where'] = ['_.group_id' => $id_group_test];
		}
		if(empty($branch) == false) {
			$args['branch'] = $branch;
		}
		// if(empty($start)==false && empty($length)==false) {
		if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
			$args['limit'] = [intval($start), intval($length)];
		}
		if(empty($search) == false) {
			$args['like'] = [
				[
					'cols' => ['b1.branch_name', 'usr.name', 'usr.username', '_.test_name'],
					'search' => $search
				]
			];
		}
		$r = $this->m_masterqc->many_test($args);
		echo json_encode($r);
	}

	public function many_alat(){
		$user = $this->current_user();
		if ($user->branch_id == 16):
			$list_alat = $this->m_masterqc->many_alat(['order' => [ ['_.alat_name'] ] ]);
		else:
			$list_alat = $this->m_masterqc->many_alat(['order' => [ ['_.alat_name'] ] ],
                [
                    'where' => [
                        '_.branch_id' => $user->branch_id
                    ]
                ]
            );
		endif;
		echo json_encode($list_alat);
	}

	function many_lot_number(){
		$args = [];
		$r = $this->m_masterqc->many_lot_number($args);
		echo json_encode($r);
	}

	function data_chart(){
		$id_qc_input = $this->input->post("id_qc_input");
        $id_hasil = $this->input->post("id_hasil");
        $r = $this->m_qc->get_data_chart($id_qc_input, $id_hasil);
        echo json_encode($r);
	}

	public function current_user(){
        $query = $this->db->query("Select a.*,b.branch_name,c.name_position
            from users a
            join branch b on (a.branch_id = b.branch_id)
            join position c on (a.id_position = c.id_position)
            where id_user='$this->user_id'
        ");
        return $query->first_row();
    }

    function filter_lot_ctrl() {
        $user = $this->current_user();
        $args = [
            'where' => [
            	'b1.branch_id' => $user->branch_id
            ],
            'disable_paging' => 1 /*auto set*/
        ];

        /*
        	params
        	- branch_id
			- alat_name
			- test_name
        */
        $post_params = [
        	'branch_id' => $this->input->post('branch_id'),
        	'alat_name' => $this->input->post('alat_name'),
        	'test_name' => $this->input->post('test_name')
        ];

        if($user->branch_id == 16) {
            unset($args['where']['b1.branch_id']);
        }

        foreach ($post_params as $field => $value) {
        	if (!empty($value)) {
        		if ($field=='branch_id') {
        			$args['where']['b1.branch_id'] = $value;
        		}else{
        			$args['where'][$field. '  like '] = '%'.$value.'%';
        		}
        	}
        }


        $filter_group = $this->input->post('group');
        $r = $this->m_qc->select_lot_ctrl($args,$filter_group);

        $result['data'] = (isset($r['data'])&&!empty($r['data']))?$r['data']:[];
        $result['rows'] = count($result['data']);
        // $result['sql'] = $this->db->last_query();

        echo json_encode($result);
    }

    function data_input_qc($id = 0) {
        $user = $this->current_user();
        $args = [
            'where' => [
            	'b1.branch_id' => $user->branch_id
            ],
            'disable_paging' => 1 /*auto set*/
        ];

        if($user->branch_id == 16) {
            unset($args['where']['b1.branch_id']);
        }

        if ($id > 0) {
            $args['where']['_.id_qc_input'] = $id;
        }

        /*
        	params
        	- alat_name
			- test_name
			- lot_number
			- list_mean
			- list_sd
			- list_level_name
			- creator_name
			- branch_id
        */
        $post_params = [
        	'alat_name' => $this->input->post('alat_name'),
			'test_name' => $this->input->post('test_name'),
			'lot_number' => $this->input->post('lot_number'),
			'list_mean' => $this->input->post('list_mean'),
			'list_sd' => $this->input->post('list_sd'),
			'list_level_name' => $this->input->post('list_level_name'),
			'creator_name' => $this->input->post('creator_name'),
			'branch_id' => $this->input->post('branch_id')
        ];

		foreach ($post_params as $field => $value) {
        	if (!empty($value)) {
        		if ($field=='branch_id') {
        			$args['where']['b1.branch_id'] = $value;
        		}else{
        			$args['where'][$field. '  like '] = '%'.$value.'%';
        		}
        	}
        }

        $r = $this->m_qc->dt_input_qc($args);

        /*formatter*/
        $tmp_ = $r['data'];
        unset($r['data']);
        $id_qc_input = 0;
        foreach ($tmp_ as $key => $value) {
            $id_qc_input = $value['id_qc_input'];
            foreach ($value as $k => $v) {
                $r['data'][$key][$k] = $v;
                if ($k=='list_mean') {
                    if (!empty($v)) {
                        $mean_tmp = explode('-----', $v);
                        $r['data'][$key][$k] = '';
                        foreach ($mean_tmp as $im => $vm) {
                            $r['data'][$key][$k] .= '<br>'.($im+1).'). '.$vm;
                        }
                    }else{
                        $r['data'][$key][$k] = '-';
                    }

                }

                if ($k=='list_sd') {
                    if (!empty($v)) {
                        $sd_tmp = explode('-----', $v);
                        $r['data'][$key][$k] = '';
                        foreach ($sd_tmp as $is => $vs) {
                            $r['data'][$key][$k] .= '<br>'.($is+1).'). '.$vs;
                        }
                    }else{
                        $r['data'][$key][$k] = '-';
                    }

                }

                if ($k=='list_level_name') {
                    if (!empty($v)) {
                        $lvl_tmp = explode('-----', $v);
                        $r['data'][$key][$k] = '';
                        $level_caption = ['nol','satu','dua','tiga','empat','lima','enam','tujuh','delapan','sembilan','sepuluh'];
                        foreach ($lvl_tmp as $ilv => $vlv) {
                            $r['data'][$key][$k] .= '<br>'.$vlv.' ('.$level_caption[$vlv].')';
                        }
                    }else{
                        $r['data'][$key][$k] = '-';
                    }

                }

            }

        }
        /**/

        $result['data'] = (isset($r['data'])&&!empty($r['data']))?$r['data']:[];
        $result['rows'] = count($result['data']);
        // $result['sql'] = $this->db->last_query();
        echo json_encode($result);
    }

    function get_hasil(){

		$user = $this->current_user();
		$alat = $this->input->post("id_alat");

		if ($user->branch_id == 16):
		//	$list_hasil = $this->m_qc->dt_hasil($args1);
		$list_hasil = $this->db->query("select qcd.id_hasil,qc.id_qc_input,b.branch_name , qc.tanggal, t.test_name , tg.group_name , qcd.hasil , level.level_name, lot.lot_number , qid.mean ,qid.sd,qci.id_alat,lot.active from qc_hasil qc left join qc_hasil_detail qcd on qcd.id_hasil = qc.id_hasil left join branch b on b.branch_id = qc.branch_id left join qc_input qci on qci.id_qc_input = qc.id_qc_input left join qc_input_detail qid on qid.id_qc_input = qci.id_qc_input left join master_test t on t.test_id = qci.id_test left join master_test_group tg on tg.group_id = t.group_id left join master_qc_lot_number lot on lot.id_lot_number = qci.id_lot_number left join master_qc_level level on level.id_level = qcd.id_level where qci.id_alat ='$alat' and lot.active = '1' group by qcd.id_hasil order by tanggal desc")->result();
		else:
		$list_hasil = $this->db->query("select qcd.id_hasil,qc.id_qc_input,b.branch_name , qc.tanggal, t.test_name , tg.group_name , qcd.hasil , level.level_name, lot.lot_number , qid.mean ,qid.sd,qci.id_alat,lot.active from qc_hasil qc left join qc_hasil_detail qcd on qcd.id_hasil = qc.id_hasil left join branch b on b.branch_id = qc.branch_id left join qc_input qci on qci.id_qc_input = qc.id_qc_input left join qc_input_detail qid on qid.id_qc_input = qci.id_qc_input left join master_test t on t.test_id = qci.id_test left join master_test_group tg on tg.group_id = t.group_id left join master_qc_lot_number lot on lot.id_lot_number = qci.id_lot_number left join master_qc_level level on level.id_level = qcd.id_level where qci.id_alat ='$alat' and lot.active = '1' and qc.branch_id = '$user->branch_id' group by qcd.id_hasil order by tanggal desc")->result();
		//$list_hasil = $this->m_qc->dt_hasil($args2);
		endif;
		echo json_encode($list_hasil);

    }

	public function response($data, $code = 200)
    {
        $json = json_encode($data);
        return $this
            ->output
            ->set_content_type('application/json')
            ->set_status_header($code)->set_output($json);
    }

	public function save_control(){
        $input = $this->input->post();
		
		// $data = [
		// 	'lot_number' =>  $input['lot_number'] ,
		// 	'creator_id' => $input['user_id'],
		// 	'id_control' =>  $input['id_control'],
		// 	'expired_Date' => date('Y-m-d',strtotime($input['expired_date'])),
		// 	'branch_id' => $input['branch_id'],
		// ];
		
		// 	$save = $this->m_masterqc->insert_data($data, 'master_qc_lot_number');


			$data = [
				"creator_id" => $input['user_id'],
				"branch_id" => $input['branch_id'],
				"lot_number" => $input['lot_number'],
				"id_control" => $input['id_control'],
				'expired_Date' => date('Y-m-d',strtotime($input['expired_date'])),
				"active" => 1,
			];
			$save = $this->m_masterqc->add_lot_number($data);

		
			if ($save) :
				$this->response([
					'msg' => "success",
					'data' => $save
				]);
			else :
				$this->response([
					'msg' => "gagal",
					'data' => $save
				]);
			endif;
	
	}

	public function save_inputQC(){
        $input = $this->input->post();

		// $this->response([
		// 		'msg' => "gagal",
		// 		'data' => $input 
		// 	]);

			$data = [
				"creator_id" => $input['user_id'],
				"branch_id" => $input['branch_id'],
				"id_test" => $input['id_test'],
				"id_lot_number" => $input['id_lot_number'],
				"mean" => json_decode($input['mean']),
				"sd" => json_decode($input['sd']),
				"id_level" => json_decode($input['id_level']),
				"id_alat" => $input['id_alat'],
				"range1" => json_decode($input['range1']),
				"range2" => json_decode($input['range2']),
			];

			$save = $this->m_qc->add_input_qc($data);
			
			if ($save) :
				$this->response([
					'msg' => "success",
					'data' => $save,
					'input' => $data,
				]);
			else :
				$this->response([
					'msg' => "gagal",
					'data' => $save
				]);
			endif;
	
	}

	function get_control_master(){
		$user = $this->current_user();
		if ($user->branch_id == 16):
		$list_control = $this->db->query("select CONCAT(c.control_name,' - ', mlot.lot_number) as name , mlot.id_lot_number as id from master_qc_lot_number mlot left join master_control c on c.id_control = mlot.id_control where control_name is not null")->result();
		else:
		$list_control = $this->db->query("select CONCAT(c.control_name,' - ', mlot.lot_number) as name , mlot.id_lot_number as id from master_qc_lot_number mlot left join master_control c on c.id_control = mlot.id_control where control_name is not null and mlot.branch_id = '$user->branch_id'  group by control_name")->result();
		endif;
		return $this->response($list_control);
	}

	function get_group_master(){
		$user = $this->current_user();
		if ($user->branch_id == 16):
		$list_group = $this->db->query("select group_name as name , group_id as id  from master_test_group")->result();
		else:
		$list_group = $this->db->query("select group_name as name , group_id as id  from master_test_group")->result();
		endif;
		return $this->response($list_group);
	}

	function get_test_master(){
		$user = $this->current_user();
		$group_id = $this->input->post('group_id');
		if ($user->branch_id == 16):
		$list_test = $this->db->query("select test_name as name , test_id as id , master_test_group.group_id  from master_test left join master_test_group on master_test_group.group_id = master_test.group_id")->result();
		else:
		$list_test = $this->db->query("select test_name as name , test_id as id ,master_test_group.group_id from master_test left join master_test_group on master_test_group.group_id = master_test.group_id")->result();
		endif;
		return $this->response($list_test);
	}

	function get_alat_master(){
		$user = $this->current_user();
		if ($user->branch_id == 16):
		$list_alat = $this->db->query("select alat_name as name , id_alat as id from master_qc_alat")->result();
		else:
		$list_alat = $this->db->query("select alat_name as name , id_alat as id from master_qc_alat where branch_id = '$user->branch_id'")->result();
		endif;
		return $this->response($list_alat);
	}

	function reqOneLotNum(){
		$user = $this->current_user();
		$id_lot_number = $this->input->post("id_lot_number");
		// $r = $this->m_masterqc->one_lot_number(["where" => ["id_lot_number" => $id_lot_number]]);
		$where = [
            "id_lot_number" => $this->input->post("id_lot_number")
        ];
        $r = $this->m_masterqc->one_lot_number(["where" => $where]);
		echo json_encode($r);
	}

	function check_alat()
    {
        $branch_id = $this
        ->input
        ->post('branch_id');
        $alat_form = $this
            ->input
            ->post('alat_form');
        $r = $this
            ->db
            // ->query("select * from master_qc_alat where branch_id = '$branch_id' and alat_name = '$alat_form'")
			->query("select * from master_qc_alat where alat_name = '$alat_form'")
            ->result();
        return $this->response($r);
    }

	function list_branch()
    {
        $list_branch = $this->db->query("select branch_name as name , branch_id as id from branch")->result();
		return $this->response($list_branch);
    }


	function list_category()
    {
        $list_category= $this->db->query("select category_name as name , id_control_category as id from master_qc_control_category")->result();
		return $this->response($list_category);
    }

	function get_many_control(){
		$id_category = $this->input->post('id_category');
		$list_control = $this->db->query("select control_name as name , id_control as id from master_control where id_category  = '$id_category'")->result();
		return $this->response($list_control);
	}

	function get_select_lot_ctrl(){

		$user = $this->current_user();
		$args = [
            'where' => [],
            'disable_paging' => 1,
        ];

		if ($user->branch_id != 16){
			$args['where']['b1.branch_id'] = $user->branch_id;
		}else{
			unset($args['where']['b1.branch_id']);
		}
		$filter_group = json_decode('{"group_by":"id_alat"}');
        $r = $this->m_qc->select_lot_ctrl_m($args,$filter_group);
		echo json_encode($r);
	}

	function get_data_hasil(){
		$dataEdit = [];
		
		$y = $this->input->get_post('y');
		$m = $this->input->get_post('m');
		$d = $this->input->get_post('d');

		if(empty($y) == false && empty($m) == false && empty($d) == false) {
			$tahun_sekarang = $y;
			$bulan_sekarang = intval($m);
			$tanggal_sekarang = $d;

			$dataEdit = $this->m_qc->many_hasil([
				'where' => [
					'year(_.tanggal)' => $tahun_sekarang,
					'month(_.tanggal)' => $bulan_sekarang,
					'day(_.tanggal)' => $tanggal_sekarang,
					// 'b1.branch_id' => '2',
					// 'Alat.id_alat' => '12',
				],
				'join_detail' => false,
				'join_input_qc' => false,
				'join_detail_v2' => true
			]);
		}
		echo json_encode($dataEdit);
	}

	function get_available_form_qc_result(){
        $id_control = $this->input->post('control');
        $id_category = $this->input->post('category');
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        $day = $this->input->post('day');
		$alat = $this->input->post('alat');
		$branch_id = $this->input->post('branch_id');
		
        $date = "$year-$month-$day";
		
		if($branch_id=='16') $branch_id = null;
        $r = $this->m_qc->get_available_form_qc_result($id_category, $id_control, $date, $branch_id,$alat);
        echo json_encode($r);
    }


	function simulate_qc(){
        $data = $this->input->post('data');
        $data = json_decode($data, true);
        if($data != null) {
            $data['id_user'] = $this->user_id;
            $data['branch_id'] = 16;
        }
        $r = $this->m_qc->simulate_qc($data);
        echo json_encode($r);
    }



	function save_qc_hasil_v3(){
        $data = $this->input->post('data');
        $data_ = json_decode($data,true);
		$data = $data_[0];
		// echo json_encode($data);
        if($data != null) {
            $data['id_user'] = $this->input->post('user_id');
            $data['branch_id'] = 16;
        }
		

		// ADD VALIDATE DOUBLE INPUT
		$date = $data['tanggal'];
		$var = explode("-",$date);
		$oldHasils = $this->m_qc->many_hasil([
			'where' => [
				'year(_.tanggal)' => $var[0],
				'month(_.tanggal)' => $var[1],
				'day(_.tanggal)' => $var[2]
			],
			'join_detail' => false,
			'join_input_qc' => false,
			'join_detail_v2' => true
		]);
		$newData = $data['data'];
		$rr = [];
		$r = [];
        foreach ($newData as $key => $_data) {
            $oldHasil = null;
            /* mencari hasil yg sudah ada untuk diupdate,
            jika tidak ditemukan, tambahkan hasil baru */
            foreach ($oldHasils as $key2 => $oh) {
                if($oh['id_qc_input'] == $_data['qc'] && $oh['branch_id'] == $data['branch_id']) {
                    $oldHasil = $oh;
                    $rr[] = $oldHasil;
                    break;
                }
            }


            $_detail = $this->m_qc->as_new_detail_hasil($_data['res']);

            /* hasil lama tidak ditemukan, buat hasil baru */
            if($oldHasil == null) {
                //$r = $this->m_qc->add_many_hasil_v3($data);
				$r = $this->m_qc->add_hasil_v3([
                    'tanggal' => $date,
                    'creator_id' => $this->input->post('user_id'),
                    'branch_id' => $data['branch_id'],
                    'id_qc_input' => $_data['qc'],
                    'detail' => $_detail
                ]);
            }
            /* hasil lama ditemukan, update data */
            else {
                $rr[] = $this->m_qc->update_hasil_v3([
                    'id_hasil' => $oldHasil['id_hasil'],
                    'tanggal' => $date,
                    'detail' => $_detail,
					'creator_id' => $this->input->post('user_id'),
                    'branch_id' => $data['branch_id'],
                    'id_qc_input' => $_data['qc'],
                ]);

				// stay
                $this->m_qc->notifyIfOut($oldHasil['id_hasil']);
				echo json_encode($this->m_qc->notifyIfOut($oldHasil['id_hasil']));
				$r = [
					'status' => true, 
					'message' => 'Data berhasil diperbarui'
				];
            }
        }
		
		echo json_encode($r);
		
		
		//$r = $this->m_qc->add_many_hasil_v3($data);
        //echo json_encode($r);
    }


	//  stay
	function get_control(){
		$user = $this->current_user();

		$args = [
            'where' => []
        ];

		$args["order"] = [['_.id_lot_number', 'desc'],['_.active', 'desc']];

		if ($user->branch_id != 16){
			$args['where']['b1.branch_id'] = $user->branch_id;
		}else{
			unset($args['where']['b1.branch_id']);
		}
		
		$r = $this->m_masterqc->dt_lot_number_m($args);


		// if ($user->branch_id == 16):
		// //	$list_hasil = $this->m_qc->dt_hasil($args1);
		// $list_control = $this->db->query("select master_qc_lot_number.*,users.name,branch.branch_name,master_control.*,master_qc_group_level.* from master_qc_lot_number left join users on users.id_user = master_qc_lot_number.creator_id left join branch on branch.branch_id = master_qc_lot_number.branch_id left join master_control on master_control.id_control = master_qc_lot_number.id_control left join master_qc_group_level on master_qc_group_level.id_group_level = master_qc_group_level.id_group_level where master_control.control_name IS NOT NULL order by master_qc_lot_number.id_lot_number desc")->result();
		// else:
		// $list_control = $this->db->query("select master_qc_lot_number.*,users.name,branch.branch_name,master_control.*,master_qc_group_level.* from master_qc_lot_number left join users on users.id_user = master_qc_lot_number.creator_id left join branch on branch.branch_id = master_qc_lot_number.branch_id left join master_control on master_control.id_control = master_qc_lot_number.id_control left join master_qc_group_level on master_qc_group_level.id_group_level = master_qc_group_level.id_group_level where master_control.control_name  IS NOT NULL and master_qc_lot_number.branch_id = '$user->branch_id' order by master_qc_lot_number.id_lot_number desc")->result();
		// //$list_hasil = $this->m_qc->dt_hasil($args2);
		// endif;
		// echo json_encode($list_control);

		echo json_encode($r);

    }


	function master_control(){
		$user = $this->current_user();
		 if ($user->branch_id == 16):
		
		$list_control = $this->db->query("select mc.id_control as id , mc.control_name as name from master_control mc")->result();
		 else:
		 $list_control = $this->db->query("select mc.id_control as id , mc.control_name as name from master_control mc")->result();
		
		endif;
		echo json_encode($list_control);

	

    }


	// stay
	function get_input_qc(){

		$user = $this->current_user();
		// if ($user->branch_id == 16):
		// $list_input = $this->db->query("select b.* , lot.*, test.*, alat.alat_name , qid.*, input.*, GROUP_CONCAT(qid.id_level SEPARATOR ' - ') as list_id_level, GROUP_CONCAT(lvl.level_name SEPARATOR ' - ') as list_level_name , GROUP_CONCAT(qid.mean SEPARATOR ' - ') as list_mean , GROUP_CONCAT(qid.sd SEPARATOR ' - ') as list_sd , GROUP_CONCAT(qid.range1 SEPARATOR ' - ') as range1 , GROUP_CONCAT(qid.range2 SEPARATOR ' - ') as range2 from qc_input_detail qid left join qc_input input on input.id_qc_input = qid.id_qc_input left join master_qc_alat alat on alat.id_alat = input.id_alat left join branch b on b.branch_id = input.branch_id left join master_qc_level lvl on lvl.id_level = qid.id_level left join master_test test on test.test_id = input.id_test left join master_qc_lot_number lot on lot.id_lot_number = input.id_lot_number group by qid.id_qc_input")->result();
		// else:
		// $list_input = $this->db->query("select b.* , lot.*, test.*, alat.alat_name , qid.*, input.*, GROUP_CONCAT(qid.id_level SEPARATOR ' - ') as list_id_level, GROUP_CONCAT(lvl.level_name SEPARATOR ' - ') as list_level_name , GROUP_CONCAT(qid.mean SEPARATOR ' - ') as list_mean , GROUP_CONCAT(qid.sd SEPARATOR ' - ') as list_sd , GROUP_CONCAT(qid.range1 SEPARATOR ' - ') as range1 , GROUP_CONCAT(qid.range2 SEPARATOR ' - ') as range2 from qc_input_detail qid left join qc_input input on input.id_qc_input = qid.id_qc_input left join master_qc_alat alat on alat.id_alat = input.id_alat left join branch b on b.branch_id = input.branch_id left join master_qc_level lvl on lvl.id_level = qid.id_level left join master_test test on test.test_id = input.id_test left join master_qc_lot_number lot on lot.id_lot_number = input.id_lot_number where input.branch_id = '$user->branch_id' group by qid.id_qc_input")->result();
		// endif;
		// echo json_encode($list_input);


		$args = [
            'where' => []
        ];

		$args["order"] = [['_.id_qc_input', 'desc']];

		if ($user->branch_id != 16){
			$args['where']['b1.branch_id'] = $user->branch_id;
		}else{
			unset($args['where']['b1.branch_id']);
		}
		
		// $r = $this->m_masterqc->dt_lot_number_m($args);
		$r = $this->m_qc->dt_input_qc_m($args);

		echo json_encode($r);

    }

    function get_data_result() {
        $p_rows = !empty($this->input->post('limit'))?$this->input->post('limit'):10;
        $p_page = $this->input->post('page');

        $args = [
            'where' => [],
            'rest_paging' => [
            	'rows' => !empty($p_rows)?$p_rows:10,
            	'page' => !empty($p_page)?$p_page:1,
            ]
        ];

        $alat_test = $this->input->post('id_alat');
        $filter_param = [
            'tanggal' => $this->input->post('tanggal'),
			'group_test_name' => $this->input->post('group_test_name'),
			'test_name' => $this->input->post('test_name'),
			'lot_number' => $this->input->post('lot_number'),
			'list_mean' => $this->input->post('list_mean'),
			'list_sd' => $this->input->post('list_sd'),
			'list_hasil' => $this->input->post('list_hasil'),
			'list_level_name' => $this->input->post('list_level_name'),
			'periode' => $this->input->post('periode'),
			'branch_id' => $this->input->post('branch_id'),
        ];
        // default filter is this years data

        if(empty($alat_test) == false) {
            /*get alat name*/
            $alatObj = $this->m_qc->edit_data(['id_alat' => $alat_test],'master_qc_alat')->result();
            $alat_name = $alatObj[0]->alat_name;
            $args['where']['Alat.alat_name like '] = '%'.$alat_name.'%';
        }

        if (!empty($filter_param) && !empty($filter_param['periode'])) {
            $key = $filter_param['periode'];

            switch ($key) {
                case 'today':
                    // $args['disable_paging'] = 1;
                    $args['where']['tanggal'] = date('Y-m-d');
                    break;
                case 'week':
                    // $args['disable_paging'] = 1;
                    $date = date('Y-m-d');
                    $date = strtotime($date);
                    $this_week = date("W", $date)-1; /*selisih dgn mysql week*/
                    $args['where']['week(tanggal)'] = $this_week;
                    break;
                case 'month':
                    // $args['disable_paging'] = 0;
                    $args['where']['month(tanggal)'] = date('m');
                    $args['where']['year(tanggal)'] = date('Y');
                    break;
                case 'year':
                    unset($args['where']['month(tanggal)']);
                    $args['where']['year(tanggal)'] = date('Y');
                    // $args['disable_paging'] = 0;
                    break;
                case 'all':
                    // all data, no filter
                    // $args['disable_paging'] = 0;
                    $tmp_where['where']['Alat.alat_name like '] = $args['where']['Alat.alat_name like '];
                    unset($args);
                    unset($filter_param);
                    $args = $tmp_where;
                    break;
                default:
                    /**/
                    break;
            }

            if (isset($filter_param['periode'])) {
                unset($filter_param['periode']);
            }
        }

        $user = $this->current_user();
		if($user->branch_id == 16) {
            unset($args['where']['b1.branch_id']);
        }else{
        	$args['where']['b1.branch_id'] = $user->branch_id;
        }


		$i = 1;
        foreach ($filter_param as $field => $value) {
            if (!empty($value)) {
                if ($field=='branch_id') {
                    $args['where']['b1.branch_id'] = $value;
                }else{
                    $args['filter_where'][$i] = $value;
                }
            }
        }


    	$r = $this->m_qc->dt_hasil($args);

        $this->load->library('misc');
        /*formating date*/
        foreach ($r['data'] as $key => $value) {
            $r['data'][$key]['tanggal'] = $this->misc->dateFormat($value['tanggal']);
        }
        /**/

        $result['rows'] = count($r['data']);
        $result['page'] = $p_page;
        $result['page_total'] = ceil($r['recordsTotal']/$p_rows);
        $result['record_total'] = $r['recordsTotal'];
        $result['data'] = $r['data'];
    	echo json_encode($result);
    }

    public function get_branch()
    {
        $data = array();
        $data_branch = $this->m_qc->get_data('branch')->result();

        if (!empty($data_branch)) {
            $i = 0;
            foreach ($data_branch as $key => $value) {
                $data['data'][$i]['no'] = $i+1;
                $data['data'][$i]['branch_id'] = $value->branch_id;
                $data['data'][$i]['branch_name'] = $value->branch_name;
                $i++;
            }
        }
        $data['rows'] = count($data['data']);
        echo json_encode($data);
	}
	
	public function get_lot() {
		$this->db->select('*');
		$this->db->from('master_qc_lot_number');
		$this->db->join('master_control', 'master_control.id_control = master_qc_lot_number.id_control','left');
		$this->db->join('master_qc_group_level', 'master_qc_group_level.id_group_level = master_qc_group_level.id_group_level','left');
		$data = $this->db->get();
		$data = $data->first_row();
        echo json_encode($data);
	}
}
