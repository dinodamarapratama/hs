<?php

class Master_data extends CI_Controller {

	function __construct(){
		parent::__construct();
		// header('Content-Type: application/json');
        $this->restapikey= $this->config->config['restapikey'];
		if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        $this->user_id = $this->input->post("user_id");
        $this->load->model("restapi/user_model");
        $user = $this->user_model->one_user(["where" => ["_.id_user" => $this->user_id]]);
        if($user == null) {
        	echo json_encode(["status" => false, "message" => "user is invalid"]);
        	exit();
        }
        $this->load->model('m_master_data');
	}

	function many_branch(){
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search');

		$args = [
			'order' => [ ['_.branch_name', 'asc'] ]
		];
        
		if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
        // if(empty($start) == false && empty($length) == false ) {
			$args['limit'] = [intval($start), intval($length)];
		}
		if(empty($search) == false) {
			$args['like'] = [
				[
					'cols' => ['branch_name'],
					'search' => $search
				]
			];
		}
		$r = $this->m_master_data->many_branch($args);
		echo json_encode($r);
	}

	function many_department() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search');

		$args = [
			'order' => [ ['_.name', 'asc'] ]
		];
        
		if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
        // if(empty($start) == false && empty($length) == false ) {
			$args['limit'] = [intval($start), intval($length)];
		}
		if(empty($search) == false) {
			$args['like'] = [
				[
					'cols' => ['_.name'],
					'search' => $search
				]
			];
		}
		$r = $this->m_master_data->many_department($args);
		echo json_encode($r);
	}

	function many_bagian(){
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search');

		$args = [
			'order' => [ ['_.name', 'asc'] ]
		];
        
		if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
        // if(empty($start) == false && empty($length) == false ) {
			$args['limit'] = [intval($start), intval($length)];
		}
		if(empty($search) == false) {
			$args['like'] = [
				[
					'cols' => ['_.name'],
					'search' => $search
				]
			];
		}
		$r = $this->m_master_data->many_bagian($args);
		echo json_encode($r);
	}
}