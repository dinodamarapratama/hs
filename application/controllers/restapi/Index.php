<?php

class Index extends CI_Controller{
    function __construct(){
        parent::__construct();
        header('Content-Type: application/json');
        $this->restapikey= $this->config->config['restapikey'];
		if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
    }

    public function index(){
        $getApi = $this->db->get_where('app_version')->row();
        echo json_encode($getApi);
    }
}
