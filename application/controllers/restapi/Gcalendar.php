<?php

class Gcalendar extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('gcalendar');
	}

	function config(){
		// gc_get_client();
		// gc_test();
		$r = gc_config();
		if($r['status'] == 1) {
			// redirect($r['url']);
			echo 'Konfigurasi salah. Lakukan konfigurasi dengan membuka halaman <a href="' . $r['url'] . '">ini</a>';
		}
		else if($r['status'] == 0) {
			echo "Konfigurasi sudah benar. untuk reset, hapus file <b>" . $r['conf_file'] . "</b>";
		}
		else if($r['status'] == 2) {
			echo "Tidak dapat terhubung ke internet";
		}
	}

	function set_token(){
		$token = $this->input->get('code');
		echo $token;
		// exit();
		
		$r = gc_set_token($token);

		// echo json_encode($r);
		if($r['status'] == 0) {
			// echo 'Konfigurasi telah disimpan';
			redirect(base_url() . 'restapi/gcalendar/config');
		}
		else {
			echo 'Terjadi kesalahan menyimpan konfigurasi';
		}
	}
}