<?php

class Qcreports extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        if($this->uri->segment(3) !== "pdf"){
            header('Content-Type: application/json');
        $this->restapikey= $this->config->config['restapikey'];
        if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        }

        $this->load->model('restapi/qcreport_model', 'mqcreport');
        $this->load->model('restapi/user_model');
        $this->user_id = $this->input->post('user_id');
        $this->user = $this->user_model->one_user(['where' => ['_.id_user' => $this->user_id]]);

        if($this->user == null) {
            echo json_encode(['status' => false, 'message' => 'User is invalid']);
            exit();
        }

        $this->id_position = $this->user['id_position'];
        $this->id_bagian = $this->user['id_bagian'];
    }

    function detail()
    {
        $id = $this->input->post('id');
        $id_reply = $this->input->post('id_reply');
        $qcreport = $this->mqcreport->detail($id, $id_reply, $this->user_id);
        // print_r($qcreport);

        echo json_encode($qcreport);
    }

    function get_replies(){
        $id = $this->input->get_post('id');
        $r = $this->mqcreport->getReplies($id);
        echo json_encode($r);
    }

    function pdf($id)
    {
        error_reporting(0);
        $qcreport = $this->mqcreport->detail($id);
        $attr = $qcreport->attr;
        // echo '<pre>';
        // print_r($qcreport);
        // exit();
        $this->load->library('MyPDF');
        $mypdf = $this->mypdf;
        $mypdf->AddPage();
        $mypdf->SetTextColor(40,40,40);
        $mypdf->Image('logo.png', 8, 10, 40);
        $mypdf->SetFont('FiraMono-Regular', null, 10);
        $mypdf->SetX(0);
        $mypdf->Cell( 0, 12, 'No. 123/KDY/456', 0, 0, 'R' );
        $mypdf->SetFont('FiraMono-Bold', null, 14);
        $mypdf->SetX(0);
        $mypdf->Ln();
        $mypdf->SetDrawColor(210,210,210);
        $mypdf->SetFillColor(235,235,235);
        $mypdf->Ln();
        $mypdf->Cell( 0, 0, 'Formulir Komplain', 0, 50, 'C', false );
        $mypdf->Ln(10);

        $mypdf->Heading("Informasi Utama");
        $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );
        $mypdf->Row("Pengaju Komplain", $qcreport->pengaju_komplain);
        $mypdf->Row("Nama Pasien", $qcreport->nama_pasien);
        $mypdf->Row("ID Pasien", $qcreport->id_pasien);
        $mypdf->Row("Nomor Registrasi", $qcreport->nomor_registrasi);
        $mypdf->Row("Jenis Komplain", $qcreport->jenis_komplain);
        $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );

        $mypdf->Heading("Data Pasien Tambahan");
        $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );
        $mypdf->Row("Pengirim", $attr->pengirim);
        // $mypdf->Row("Tanggal / Jam Pasien Tiba di Laboraturiom", $attr->waktu);
        $mypdf->Row("Petugas Registrasi dan Jam", $attr->petugas_registrasi.' ('.date('d M Y, H:i', strtotime($attr->waktu_registrasi)).')');
        $mypdf->Row("Petugas Phlebotomy/Radiografi dan Jam", $attr->petugas_phlebotomy.' ('.date('d M Y, H:i', strtotime($attr->waktu_radiografi)).')');
        if($attr->home_service === 'Home Service'){
            $mypdf->Row("Jika Home Service", false);
            $mypdf->Row("   Nama Petugas", $attr->petugas_home_service);
            $mypdf->Row("   Tanggal/Jam Petugas Tiba di Rumah Pasien", $attr->waktu_tiba_dirumah);
            $mypdf->Row("   Jam Pengambilan Sampel", $attr->waktu_ambil_sample);
        }
        $mypdf->Row("Tanggal/Jam Sampel Naik", date('d F Y, H:i', strtotime($attr->waktu_sample_naik)));
        $mypdf->Row("Tanggal/Jam Sampel Running", date('d F Y, H:i', strtotime($attr->waktu_sample_running)));
        $mypdf->Row("Analis Yang Mengerjakan", $attr->analis_yang_mengerjakan);
        $mypdf->Row("Analis Yang Verifikasi Hasil dan Tanggal", $attr->analis_yang_memverifikasi_hasil .' ('. date('d F Y, H:i', strtotime($attr->waktu_verifikasi_hasil)).')');
        $mypdf->Row("Autorisasi Hasil dan Tanggal/Jam", $attr->autorisasi_hasil . ' (' . date('d M Y, H:i', strtotime($attr->waktu_autorisasi_hasil)).')');
        $mypdf->Row("Tanggal/Jam Print Hasil", date('d F Y, H:i', strtotime($attr->waktu_hasil_print)));
        $mypdf->Row("Tanggal/Jam Kirim Email ke Dokter/Pasien", date('d F Y, H:i', strtotime($attr->waktu_kirim_email)));
        $mypdf->Row("Tanggal/Jam Hasil Tiba di Rumah Pasien/Dokter", date('d F Y, H:i', strtotime($attr->waktu_hasil_tiba)));
        $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );

        $mypdf->Heading("Deskripsi Masalah");
        $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );
        $mypdf->SetFont('FiraMono-Regular', null, 7);
        $mypdf->Cell( 0, 3, "  ".$attr->deskripsi, 'LR', 1, 'L', false );
        $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );

        $mypdf->Heading("Kronologis");
        $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );
        $mypdf->SetFont('FiraMono-Regular', null, 7);
        $mypdf->Cell( 0, 3, "  ".$attr->kronologis, 'LR', 1, 'L', false );
        $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );

        $mypdf->Heading("Akar Masalah");
        $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );
        $mypdf->SetFont('FiraMono-Regular', null, 7);
        $mypdf->Cell( 0, 3, "  ".$attr->akar_masalah, 'LR', 1, 'L', false );
        $mypdf->Cell( 0, 3, "", 'LRB', 1, 'L', false );

        $mypdf->AddPage();
        $mypdf->Ln(5);

        $mypdf->Heading("Penyelesaian Masalah");
        $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );
        foreach ($qcreport->actions as $action) {
            if($action->type === 'Penyelesaian Masalah'){
                $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );
                $mypdf->SetFont('FiraMono-Bold', null, 8);
                $mypdf->Cell( 0, 3, "  ".ucwords($action->name).", ".date('d F Y, H:i', strtotime($action->action_time)), 'LR', 1, 'L', false );
                $mypdf->SetFont('FiraMono-Regular', null, 7);
                $mypdf->Cell( 0, 6, "  ".$action->content, 'LR', 1, 'L', false );
                $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );
            }
        }
        $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );

        $mypdf->Heading("Tindakan Perbaikan");
        $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );
        foreach ($qcreport->actions as $action) {
            if($action->type === 'Tindakan Perbaikan'){
                $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );
                $mypdf->SetFont('FiraMono-Bold', null, 8);
                $mypdf->Cell( 0, 3, "  ".ucwords($action->name).", ".date('d F Y, H:i', strtotime($action->action_time)), 'LR', 1, 'L', false );
                $mypdf->SetFont('FiraMono-Regular', null, 7);
                $mypdf->Cell( 0, 6, "  ".$action->content, 'LR', 1, 'L', false );
                $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );
            }
        }
        $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );

        $mypdf->Heading("Kesimpulan Penyelesaian Masalah");
        $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );
        foreach ($qcreport->actions as $action) {
            if($action->type === 'Kesimpulan Penyelesaian Komplain'){
                $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );
                $mypdf->SetFont('FiraMono-Bold', null, 8);
                $mypdf->Cell( 0, 3, "  ".ucwords($action->name).", ".date('d F Y, H:i', strtotime($action->action_time)), 'LR', 1, 'L', false );
                $mypdf->SetFont('FiraMono-Regular', null, 7);
                $mypdf->Cell( 0, 6, "  ".$action->content, 'LR', 1, 'L', false );
                $mypdf->Cell( 0, 3, "", 'LR', 1, 'L', false );
            }
        }
        $mypdf->Cell( 0, 3, "", 'LRB', 1, 'L', false );

        $mypdf->Ln(20);

        $mypdf->SetFont('FiraMono-Regular', null, 7);
        $mypdf->SetMargins(46, 0, 0);
        $mypdf->Cell( 0, 0, "Laporan Komplain", '', 1, 'L', false );
        $mypdf->SetMargins(75, 0, 0);
        $mypdf->Cell( 0, 0, "Area Manager", '', 1, 'L', false );
        $mypdf->SetMargins(120, 0, 0);
        $mypdf->Cell( 0, 0, "Dokter Penanggung Jawab", '', 1, 'L', false );
        $mypdf->SetMargins(164, 0, 0);
        $mypdf->Cell( 0, 0, "Manager Coordinator Medic", '', 1, 'L', false );
        $mypdf->SetMargins(10, 0, 0);
        $mypdf->Cell( 0, 0, "GM Operasional/GM CM", '', 1, 'L', false );

        $mypdf->Ln(6);

        $mypdf->SetFont('FiraMono-Bold', null, 9);
        $mypdf->SetMargins(46, 0, 0);
        $mypdf->Cell( 0, 0, ucwords($qcreport->author->name), '', 1, 'L', false );
        $mypdf->SetMargins(75, 0, 0);
        $mypdf->Cell( 0, 0, "", '', 1, 'L', false );
        $mypdf->SetMargins(120, 0, 0);
        $mypdf->Cell( 0, 0, "", '', 1, 'L', false );
        $mypdf->SetMargins(164, 0, 0);
        $mypdf->Cell( 0, 0, "", '', 1, 'L', false );
        $mypdf->SetMargins(10, 0, 0);
        $mypdf->Cell( 0, 0, "", '', 1, 'L', false );

        $mypdf->Ln(4);

        $mypdf->SetFont('FiraMono-Medium', null, 7);
        $mypdf->SetMargins(46, 0, 0);
        $mypdf->Cell( 0, 0, sprintf('%s %s', $qcreport->author->dept_name, $qcreport->author->dept_desc), '', 1, 'L', false );
        $mypdf->SetMargins(75, 0, 0);
        $mypdf->Cell( 0, 0, "", '', 1, 'L', false );
        $mypdf->SetMargins(120, 0, 0);
        $mypdf->Cell( 0, 0, "", '', 1, 'L', false );
        $mypdf->SetMargins(164, 0, 0);
        $mypdf->Cell( 0, 0, "", '', 1, 'L', false );
        $mypdf->SetMargins(10, 0, 0);
        $mypdf->Cell( 0, 0, "", '', 1, 'L', false );

        $mypdf->Output('qcreport.pdf', $this->input->get('download') ? 'D' : 'I', true);
        // $mypdf->Output();
    }

    public function save()
    {
        $data = [
            'title' => $this->input->post('subject'),
            'id_alat' => 0,
            'branch_id' => $this->input->post('branch_id'),
            'kategori' => $this->input->post('category'),
            'date' => $this->input->post('date'),
            'from_id' => $this->user_id,
            'bagian' => $this->input->post('bagian'),
            'receivers' => $this->input->post('receivers'),
            'bagian' => $this->input->post('bagian'),
            'status' => $this->input->post('status'),
            'reagen_des' => $this->input->post('reagen_des'),
            'kontrol_des' => $this->input->post('kontrol_des'),
            'reg_des' => $this->input->post('reg_des'),
            'kalibrasi_des' => $this->input->post('kalibrasi_des'),
            'qc_des' => $this->input->post('qc_des'),
            'is_edit' => false,
            'id' => ''
        ];
        // echo json_encode($_POST);
        // exit();
        //ob_start();
        //print_r($_POST);
        //print_r($_FILES);
        //file_put_contents("/applications/xampp/htdocs/mis/post-qcreport.log", ob_get_clean());
        $qcreport = $this->mqcreport->save($data, [], [], 'file_', 'seq');
        echo json_encode($qcreport);
    }

    public function action()
    {
        $qcreport = $this->mqcreport->action();
        echo json_encode($qcreport);
    }

    public function mine()
    {
        $query = $this->input->post('query');
        $qcreport = $this->mqcreport->mine($this->user_id, $query);
        echo json_encode($qcreport);
    }

    function recent_report(){
        $r = $this->mqcreport->recent_report($this->user_id);
        echo json_encode($r);
    }

    function reply()
    {
        $data = [
            "id" => $this->input->post("id"),
            "user_id" => $this->input->post("user_id"),
            "message" => $this->input->post("message"),
            'reply_id' => $this->input->post('reply_id')
        ];
        $r = $this->mqcreport->reply($data);
        if($r['status'] && isset($_FILES['attachment'])) {
            $this->load->library('upload');
            $upload_path = './uploads/attachments/qcreport/';
            $this->upload->initialize([
                'upload_path' => $upload_path,
                'encrypt_name' => true,
                'allowed_types' => '*'
            ]);
            $ru = [];
            $cu = count($_FILES['attachment']['name']);
            for($i = 0; $i < $cu; $i++) {
                $_FILES['t'] = [
                    'name' => $_FILES['attachment']['name'][$i],
                    'type' => $_FILES['attachment']['type'][$i],
                    'tmp_name' => $_FILES['attachment']['tmp_name'][$i],
                    'size' => $_FILES['attachment']['size'][$i],
                    'error' => $_FILES['attachment']['error'][$i],
                ];

                if($this->upload->do_upload('t')) {
                    $ru[] = $u = $this->upload->data();
                    $this->mqcreport->save_reply_attachment([
                        'id_qcreport_reply' => $r['id'],
                        'name' => $u['orig_name'],
                        'path' => $upload_path . $u['file_name'],
                        'file_type' => $u['file_type']
                    ]);
                } else {
                    $ru[] = $this->upload->display_errors();
                }
            }
            $r['uploads'] = $ru;
        }
        echo json_encode($r);
    }

    function hapus_reply(){
        $id = $this->input->post('id_reply');
        $r = $this->mqcreport->hapus_reply($id, $this->user_id);
        echo json_encode($r);
    }

    function hapus_attachment(){
        $id = $this->input->post('id');
        $r = $this->mqcreport->hapus_attachment($id);
        echo json_encode($r);
    }

    public function delete()
    {
        $result = $this->mqcreport->delete();
        echo json_encode($result);
    }

    function update_status(){
        $id = $this->input->post('id_qcreport');
        $status = $this->input->post('status');
        if(in_array($status, ['Open', 'Close']) == false) {
            $status = 'Close';
        }
        $target = $this->mqcreport->one(['where'=>['_.id'=>$id]]);
        if($target != null && $target->from_id != $this->user_id){
            echo json_encode(['status'=>false,'message'=>'Anda tidak berhak mengakses milik orang lain']);
            exit();
        }
        $r = $this->mqcreport->update(['_.id' => $id], ['status' => $status]);
        echo json_encode($r);
    }

    function many_report(){
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $search = $this->input->post('search');
        $status = $this->input->post('status');

        $args = [
            'where' => [],
            'join_reply' => false
            // 'order' => [ ['_.id', 'desc'] ]
        ];

        if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
            $args['limit'] = [ $start, $length ];
        }

        if(empty($search) == false) {
            $args['join_reply'] = true;
            $args['like'] = [
                [
                    'search' => $search,
                    'cols' => [
                        '_.title', '_.description', '_.reagen_des', '_.kontrol_des',
                        '_.kalibrasi_des', '_.qc_des', 'rply.reply_all', 'usr.name', 'usr.username'
                    ]
                ]
            ];
        }

        $r = [];
        if($status == 'received') {
            $r = $this->mqcreport->getReceived($this->user_id, $args);
        } else if($status == 'sent') {
            $r = $this->mqcreport->getSent($this->user_id, $args);
        } else if($status == 'archived') {
            $r = $this->mqcreport->getArchived($this->user_id, $args);
        }
        echo json_encode($r);
    }

    function count_report(){
        $search = $this->input->post('search');
        $status = $this->input->post('status');
        $args = [];
        if(empty($search) == false) {
            $args['like'] = [
                [
                    'search' => $search,
                    'cols' => [
                        '_.title', '_.description', '_.reagen_des', '_.kontrol_des',
                        '_.kalibrasi_des', '_.qc_des', '_.kategori', 'rply.reply_all', 'usr.name', 'usr.username'
                    ]
                ]
            ];
        }

        $r1 = $this->mqcreport->getCountReceived($this->user_id, $args);
        $r2 = $this->mqcreport->getCountSent($this->user_id, $args);
        $r3 = $this->mqcreport->getCountArchived($this->user_id, $args);
        $r = [
            'received' => $r1->jumlah,
            'sent' => $r2->jumlah,
            'archived' => $r3->jumlah
        ];
        echo json_encode($r);
    }
}
