<?php
class Maintainint extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->restapikey = $this
            ->config
            ->config['restapikey'];
        if ($this
            ->input
            ->post('auth_key') != $this->restapikey) die( /*Silent is gold*/);
        $this->user_id = $this
            ->input
            ->get_post('user_id');
        $this->current_user = $this
            ->input
            ->post('user_id');
        $this->branch_id = $this
            ->input
            ->post('branch_id');
        $this
            ->load
            ->model('restapi/user_model');
        $this->me = $this
            ->user_model
            ->one_user(["where" => ["_.id_user" => $this->user_id]]);
        $this->model = 'm_intern_checklist';
        if ($this->me == null)
        {
            echo json_encode(['status' => false, 'message' => 'Invalid user']);
            exit();
        }
        $this
            ->load
            ->model('m_intern_checklist', 'cmodel');
    }

    function get()
    {

        $id_intern_alat = $this
            ->input
            ->post('id_intern_alat');
        $cabang = $this
            ->input
            ->post('branch_id');

        $r = $this
            ->cmodel
            ->get_all($id_intern_alat, $cabang)->result();

        echo json_encode($r);
    }

    public function detail()
    {
        $id = $this
            ->input
            ->post('id_intern_alat');
        $detail = $this
            ->cmodel
            ->detail($id)->row_array();
        return $this->response($detail);
    }

    function get_alat()
    {
        $r = $this
            ->db
            ->query('select alat.* , alat.id as id_intern_alat, ialat.* from master_intern_alat alat left join master_qc_alat ialat on ialat.id_alat = alat.id_alat')
            ->result();
        return $this->response($r);
    }

    function test_saja()
    {
        // $r = $this->db->query('select * from intern_checklist_input where value = 0')->result();
        $timezone = time() + (60 * 60 * 7);
        $mytimezone = gmdate('d-m-Y H:i:s', $timezone);
        $date = date("Ymd his", strtotime($mytimezone));
        $r = $this
            ->db
            ->query("INSERT into 
            notifications 
            (`from_id`, `to_id`, `description`, `data`, `read`, `time`, `cuti_flag`) 
            select
            1,
            0,
            intern_checklist.checklist_name, 
            json_object('id', intern_checklist_input.id, 'id_intern_checklist', intern_checklist_input.id_intern_checklist,'notif_type','intern_checklist','hrd',0),
            0,
            STR_TO_DATE('$date','%Y%m%d %h%i%s'),
            0
            from intern_checklist_input 
            left join intern_checklist 
            on intern_checklist.id = intern_checklist_input.id_intern_checklist
            where value = 0")->result();
        return $this->response($r);
    }

    function get_ic()
    {
        $r = $this
            ->db
            ->query('select 
            ici.* , ici.id as id_intern_checklist_input, 
            ic.*
            from intern_checklist_input ici 
            left join intern_checklist ic
            on ici.id_intern_checklist = ic.id')
            ->result();
        return $this->response($r);
    }

    function get_bm()
    {
        // $r = $this->db->query('select * FROM users')->result();
        $r = $this
            ->db
            ->query('select * FROM users where id_position = 22')
            ->result();
        return $this->response($r);
    }

    function post_ntf()
    {
        $timezone = time() + (60 * 60 * 7);
        $mytimezone = gmdate('d-m-Y H:i:s', $timezone);
        $date = date("Ymd his", strtotime($mytimezone));
        $data = json_decode($this
            ->input
            ->post('data'));
        foreach ($data as $mydata)
        {
            $this
            ->db
            ->query("INSERT into 
            notifications 
            (`from_id`, `to_id`, `description`, `data`, `read`, `time`, `cuti_flag`) 
            values
            (1,
            '$mydata->id_user',
            'ok', 
            json_object('id','$mydata->id_intern_checklist','id_intern_checklist','$mydata->id_intern_checklist','notif_type','intern_checklist','hrd',0),
            1,
            STR_TO_DATE('$date','%Y%m%d %h%i%s'),
            0)");

        }

        // print_r($data);
    }

    function get_alat_cabang()
    {
        $branch_id = $this
            ->input
            ->post('branch_id');
        $r = $this
            ->db
            ->query("select alat.* , alat.id as id_intern_alat, alat.branch_id as id_branch_alat, ialat.* from master_intern_alat alat left join master_qc_alat ialat on ialat.id_alat = alat.id_alat where alat.branch_id = '$branch_id'")->result();
        return $this->response($r);
    }

    function get_cabang()
    {
        $r = $this
            ->db
            ->query('select b.* from branch b')
            ->result();
        return $this->response($r);
    }

    public function get_checklist()
    {
        $periode = $this
            ->input
            ->post('periode');
        $id_intern_alat = $this
            ->input
            ->post('id_intern_alat');
        $detail = $this
            ->db
            ->query("select ic.* from intern_checklist ic where ic.periode = '$periode' and ic.id_intern_alat = '$id_intern_alat' order by ic.id")->result();
        return $this->response($detail);
    }

    public function get_checklist_detail()
    {
        $periode = $this
            ->input
            ->post('periode');
        $id_intern_alat = $this
            ->input
            ->post('id_intern_alat');
        $detail = $this
            ->db
            ->query("select ic.*,ici.* from intern_checklist ic left join intern_checklist_input ici on ici.id_intern_checklist  = ic.id  where ic.periode = '$periode' and ic.id_intern_alat = '$id_intern_alat' order by ic.id")->result();
        return $this->response($detail);
    }

    function save()
    {
        $result['status'] = false;
        $result['message'] = 'Data gagal disimpan.';

        // is array id_intern_checklist
        if (is_array($this
            ->input
            ->post('id_intern_checklist')))
        {
            foreach ($this
                ->input
                ->post('id_intern_checklist') as $id_intern_checklist)
            {
                if (in_array($id_intern_checklist, $this
                    ->input
                    ->post('id_checked')))
                {
                    $data = array(
                        'id_intern_checklist' => $id_intern_checklist,
                        'branch_id' => $this
                            ->input
                            ->post('branch_id') ,
                        'tanggal' => date('Y-m-d') ,
                        'value' => 1,
                        'creator' => $this->current_user
                    );
                }
                else
                {
                    $data = array(
                        'id_intern_checklist' => $id_intern_checklist,
                        'branch_id' => $this
                            ->input
                            ->post('branch_id') ,
                        'tanggal' => date('Y-m-d') ,
                        'value' => 0,
                        'creator' => $this->current_user
                    );
                }

                $this
                    ->m_mis
                    ->insert_data($data, 'intern_checklist_input');
            }
            $result['status'] = true;
            $result['msg'] = 'success';
            $result['message'] = 'Data berhasil disimpan.';
        }

        echo json_encode($result);
    }

    function delete()
    {
        $result['status'] = false;
        $result['message'] = 'Data gagal disimpan.';

        $data['id_maintain_ex'] = $this
            ->input
            ->post('id');
        $r = $this
            ->cmodel
            ->delete_data($data, $this->model);

        if ($r)
        {
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        }
        echo json_encode($result);
    }

    public function response($data, $code = 200)
    {
        $json = json_encode($data);
        return $this
            ->output
            ->set_content_type('application/json')
            ->set_status_header($code)->set_output($json);
    }
}

