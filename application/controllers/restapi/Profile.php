<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
*
*/
class Profile extends CI_Controller
{

public $model = 'users';

public function __construct()
{
		parent::__construct();
        // if($this->uri->segment(3) !== "pdf"){
        header('Content-Type: application/json');
        $this->restapikey= $this->config->config['restapikey'];
		if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        $this->load->model('restapi/notification_model', 'mnotification');
        $this->load->model('restapi/user_model');
        $this->user_id = $this->input->post('user_id');
        $this->user = $this->user_model->one_user(['where' => ['_.id_user' => $this->user_id]]);

        if($this->user == null) {
            echo json_encode(['status' => false, 'message' => 'User tidak valid']);
            exit();
        }
}

public function getUser() {
	$users = $this->m_profile->get_all_profile($this->user_id)->first_row();
	echo json_encode($users);
}

public function delete_account() {
	$id = $this->input->post('id');
	$update = $this->db->query("UPDATE users SET STATUS='KELUAR' where id_user='$id'");
	return $this->response([
		'msg' => "success",
		'data' => $update
	]);
}

public function change()
{
	$id = $this->user_id;
	$data = array(
		'username'=>$this->input->post('username'),
		'name'=>$this->input->post('name'),
		'last_name'=>$this->input->post('last_name'),
		'inisial'=>$this->input->post('inisial'),
		'call_name'=>$this->input->post('call_name'),
		'email'=>$this->input->post('email'),
		'GOLDARAH'=>$this->input->post('GOLDARAH'),
		'STATUSKAWIN'=>$this->input->post('STATUSKAWIN'),
		'PENDIDIKAN'=>$this->input->post('PENDIDIKAN'),
		'TINGGI'=>$this->input->post('TINGGI'),
		'BERAT'=>$this->input->post('BERAT'),
		'JMLANAK'=>$this->input->post('JMLANAK'),
		'TGLLAHIR'=>$this->input->post('TGLLAHIR'),
		'TGLMASUK'=>$this->input->post('TGLMASUK'),
		'ALAMAT_DOMO'=>$this->input->post('ALAMAT_DOMO'),
		'PROPINSI_DOMO'=>$this->input->post('PROPINSI_DOMO'),
		'KECAMATAN_DOMO'=>$this->input->post('KECAMATAN_DOMO'),
		'RTRW_DOMO'=>$this->input->post('RTRW_DOMO'),
		'KELDESA_DOMO'=>$this->input->post('KELDESA_DOMO'),
		'KABUPATEN_DOMO'=>$this->input->post('KABUPATEN_DOMO'),
		'KODEPOS_DOMO'=>$this->input->post('KODEPOS_DOMO'),
		/*alamat ktp*/
		'ALAMAT'=>$this->input->post('ALAMAT'),
		'PROPINSI'=>$this->input->post('PROPINSI'),
		'KECAMATAN'=>$this->input->post('KECAMATAN'),
		'RTRW'=>$this->input->post('RTRW'),
		'KELDESA'=>$this->input->post('KELDESA'),
		'KABUPATEN'=>$this->input->post('KABUPATEN'),
		'KODEPOS'=>$this->input->post('KODEPOS'),
		/**/
		'TELPONRUMAH_DOMO'=>$this->input->post('TELPONRUMAH_DOMO'),
		'TELPONHP_DOMO'=>$this->input->post('TELPONHP_DOMO'),
	);

		$this->db->where('id_user', $this->input->post('id_user'));
	
	// $data = $this->input->post();
	// unset($data['auth_key']);
	// unset($data['user_id']);
	// unset($data['avatar']);
	// unset($data['id']);
	
	$r = $this->db->update('users',$data);

	$result['status'] = false;
	$result['message'] = "Gagal Menyimpan";
	if (!empty($r)) {
		$result['status'] = true;
	$result['message'] = "Berhasil Menyimpan";
	}
	echo json_encode($result);

}

public function getKabupaten($idPropinsi=0,$id=0){
	$id_propinsi = ($idPropinsi==0)?$this->input->post('id_propinsi'):$idPropinsi;
	$param = ['id_propinsi' => $id_propinsi];
	if ($id>0) {
		$param=['id_kabkota' => $id];
	}
	$data = $this->m_profile->get_list_select('m_ikabkota',$param);
    	$new_format=[];
    	foreach ($data as $key => $value) {
			$new_format[$key+1]['id'] = $value->id_kabkota;
			$new_format[$key+1]['text'] = $value->nama_kabkota;
		}
		$new_format[0]['id']=0;
		$new_format[0]['text']='--Pilih--';
		asort($new_format);
    	echo json_encode($new_format);
}

public function getKecamatan($idKabkota=0,$id=0){
	$id_kabkota = ($idKabkota==0)?$this->input->post('id_kabkota'):$idKabkota;
	$param = ['id_kabkota' => $id_kabkota];
	if ($id>0) {
		$param=['id_kecamatan' => $id];
	}

	$data = $this->m_profile->get_list_select('m_ikecamatan',$param);
    	$new_format=[];
		foreach ($data as $key => $value) {
			$new_format[$key+1]['id'] = $value->id_kecamatan;
			$new_format[$key+1]['text'] = $value->nama_kecamatan;
		}
		$new_format[0]['id']=0;
		$new_format[0]['text']='--Pilih--';
		asort($new_format);
    	echo json_encode($new_format);
    
}

public function getKelurahan($idKecamatan=0,$id=0){
	$id_kecamatan = ($idKecamatan==0)?$this->input->post('id_kecamatan'):$idKecamatan;
	$param = ['id_kecamatan' => $id_kecamatan];
	if ($id>0) {
		$param=['id_kelurahan' => $id];
	}
	$data = $this->m_profile->get_list_select('m_ikelurahan',$param);
	
    	$new_format=[];
		foreach ($data as $key => $value) {
			$new_format[$key+1]['id'] = $value->id_kelurahan;
			$new_format[$key+1]['text'] = $value->nama_kelurahan;
		}
		$new_format[0]['id']=0;
		$new_format[0]['text']='--Pilih--';
		asort($new_format);
    	echo json_encode($new_format);
    
}

public function getPropinsi(){
	$data = $this->m_profile->get_list_select('m_ipropinsi',[]);
    	$new_format=[];
		foreach ($data as $key => $value) {
			$new_format[$key+1]['id'] = $value->id_propinsi;
			$new_format[$key+1]['text'] = $value->nama_propinsi;
		}
		$new_format[0]['id']=0;
		$new_format[0]['text']='--Pilih--';
		asort($new_format);
    	echo json_encode($new_format);
}

}