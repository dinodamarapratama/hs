<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class generalreports extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if($this->uri->segment(3) !== "pdf"){
            header('Content-Type: application/json');
            $this->restapikey= $this->config->config['restapikey'];
        if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        }

        $this->load->model('restapi/generalreports_model', 'mgeneralreports');
        $this->user_id = $this->input->post('user_id');
    }

    function detail()
    {
        $id = $this->input->post('id');
        $id_reply = $this->input->post('id_reply');
        $generalreports = $this->mgeneralreports->detail($id, $id_reply, $this->user_id);
        echo json_encode($generalreports);
    }

    function get_replies(){
        $id = $this->input->get_post('id');
        $r = $this->mgeneralreports->getReplies($id);
        echo json_encode($r);
    }


    public function save()
    {
        // echo json_encode($_POST);
        // exit();
        //ob_start();
        //print_r($_POST);
        //print_r($_FILES);
        //file_put_contents("/applications/xampp/htdocs/mis/post-generalreports.log", ob_get_clean());
        $generalreports = $this->mgeneralreports->save();
        echo json_encode($generalreports);
    }

    public function action()
    {
        $generalreports = $this->mgeneralreports->action();
        echo json_encode($generalreports);
    }

    public function mine()
    {
        $query = $this->input->post('query');
        $generalreports = $this->mgeneralreports->mine($this->user_id, $query);
        echo json_encode($generalreports);
    }

    function recent_report(){
        $r = $this->mgeneralreports->recent_report($this->user_id);
        echo json_encode($r);
    }

    function reply()
    {
        $data = [
            "id" => $this->input->post("id"),
            "user_id" => $this->input->post("user_id"),
            "message" => $this->input->post("message"),
            'reply_id' => $this->input->post('reply_id')
        ];
        $r = $this->mgeneralreports->reply($data);
        if($r['status'] == true) {
            $this->load->library('upload');
            $upload_path = './uploads/attachments/generalreports/';

            $config = [
                'upload_path' => $upload_path,
                'encrypt_name' => true,
                'allowed_types' => '*'
            ];

            $this->upload->initialize([
                'upload_path' => $upload_path,
                'encrypt_name' => true,
                'allowed_types' => '*'
            ]);

            if(file_exists($config['upload_path']) == false) {
                $old = umask(0);
                mkdir($config['upload_path'], 0777, true);
                umask($old);
            }

            $ru = [];

            $counter = -1;
            while (true) {
                $counter++;
                if($counter > 100) {
                    break;
                }
                $key = 'file_' . $counter;
                if(isset($_FILES[$key]) == false) {
                    break;
                }
                $this->upload->initialize($config);

                if($this->upload->do_upload($key)) {
                    $ru[] = $u = $this->upload->data();
                    $this->mgeneralreports->save_reply_attachment([
                        'id_generalreports_reply' => $r['id'],
                        'name' => $u['orig_name'],
                        'path' => $upload_path . $u['file_name'],
                        'file_type' => $u['file_type'],
                        'file_size' => $u['file_size']
                    ]);
                } else {
                    $ru[] = $this->upload->display_errors();
                }
            }
            // $cu = count($_FILES['attachment']['name']);
            // for($i = 0; $i < $cu; $i++) {
            //     $_FILES['t'] = [
            //         'name' => $_FILES['attachment']['name'][$i],
            //         'type' => $_FILES['attachment']['type'][$i],
            //         'tmp_name' => $_FILES['attachment']['tmp_name'][$i],
            //         'size' => $_FILES['attachment']['size'][$i],
            //         'error' => $_FILES['attachment']['error'][$i],
            //     ];

            //     if($this->upload->do_upload('t')) {
            //         $ru[] = $u = $this->upload->data();
            //         $this->mgeneralreports->save_reply_attachment([
            //             'id_generalreports_reply' => $r['id'],
            //             'name' => $u['orig_name'],
            //             'path' => $upload_path . $u['file_name'],
            //             'file_type' => $u['file_type']
            //         ]);
            //     } else {
            //         $ru[] = $this->upload->display_errors();
            //     }
            // }
            $r['uploads'] = $ru;
        }
        echo json_encode($r);
    }

    function hapus_reply(){
        $id = $this->input->post('id_reply');
        $r = $this->mgeneralreports->hapus_reply($id, $this->user_id);
        echo json_encode($r);
    }

    function hapus_attachment(){
        $id = $this->input->post('id');
        $r = $this->mgeneralreports->hapus_attachment($id);
        echo json_encode($r);
    }

    public function delete()
    {
        $result = $this->mgeneralreports->delete();
        echo json_encode($result);
    }

    function update_status(){
        $id = $this->input->post('id_generalreports');
        $status = $this->input->post('status');
        if(in_array($status, ['Open', 'Close']) == false) {
            $status = 'Close';
        }
        $target = $this->mgeneralreports->one(['where'=>['_.id'=>$id]]);
        if($target != null && $target->from_id != $this->user_id){
            echo json_encode(['status'=>false,'message'=>'Anda tidak berhak mengakses milik orang lain']);
            exit();
        }
        $r = $this->mgeneralreports->update(['_.id' => $id], ['status' => $status]);
        echo json_encode($r);
    }

    function count_report(){
        $search = $this->input->post('search');
        $status = $this->input->post('status');
        $user = $this->input->post('user');
        $bagian_id = $this->input->post('bagian_id');
        $tanggal = $this->input->post('tanggal');
        $args = [];
        if (isset($tanggal) && !empty($tanggal)):
            $args['where']['_.date'] = date('Y-m-d',strtotime($tanggal));
        endif;
        if (isset($user) && !empty($user)):
            $args['where']['_.from_id'] = $user;
        endif;
        if(empty($search) == false) {
            $args['like'] = [
                [
                    'search' => $search,
                    'cols' => [
                        '_.title', '_.description', '_.kategori', 'rply.reply_all', 'usr.name', 'usr.username'
                    ]
                ]
            ];
        }

        $r1 = $this->mgeneralreports->getCountReceived($this->user_id, $args);
        $r2 = $this->mgeneralreports->getCountSent($this->user_id, $args);
        $r3 = $this->mgeneralreports->getCountArchived($this->user_id, $args);
        $r = [
            'received' => $r1->jumlah,
            'sent' => $r2->jumlah,
            'archived' => $r3->jumlah
        ];
        echo json_encode($r);
    }

    function many_report(){
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $user = $this->input->post('user');
        $bagian_id = $this->input->post('bagian_id');
        $tanggal = $this->input->post('tanggal');
        $search = $this->input->post('search');
        $status = $this->input->post('status');

        if (isset($tanggal) && !empty($tanggal)):
            $args['where']['_.date'] = date('Y-m-d',strtotime($tanggal));
        endif;
        if (isset($user) && !empty($user)):
            $args['where']['_.from_id'] = $user;
        endif;
        $args = [
            'where' => [],
            // 'order' => [ ['_.id', 'desc'] ]
        ];

        if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
            $args['limit'] = [ $start, $length ];
        }

        if(empty($search) == false) {
            $args['like'] = [
                [
                    'search' => $search,
                    'cols' => [
                        '_.title', '_.description', '_.kategori', 'rply.reply_all', 'usr.name', 'usr.username'
                    ]
                ]
            ];
        }

        $r = [];
        if($status == 'received') {
            $r = $this->mgeneralreports->getReceived($this->user_id, $args);
        } else if($status == 'sent') {
            $r = $this->mgeneralreports->getSent($this->user_id, $args);
        } else if($status == 'archived') {
            $r = $this->mgeneralreports->getArchived($this->user_id, $args);
        }
        echo json_encode($r);
    }

}
