<?php

class Intern_checklist extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->restapikey= $this->config->config['restapikey'];
        if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        $this->user_id = $this->input->post("user_id");
        $this->load->model("restapi/user_model");

        $user = $this->user_model->one_user(["where" => ["_.id_user" => $this->user_id]]);
        if($user == null) {
            echo json_encode(["status" => false, "message" => "user is invalid"]);
            exit();
        }
        
        $this->branch_id = $user['branch_id'];

        $this->load->model('m_intern_checklist','m_ic');
        $this->load->model('m_master_intern_alat','m_mia');
        $this->load->library('misc');

    }

    /* master cheklist*/
    public function get_periode_checklist(){
        $periode = [
            'harian' => 'Harian',
            'mingguan' => 'Mingguan',
            'bulanan' => 'Bulanan',
            'tri_month' => '3 Bulanan',
            'six_month' => '6 Bulanan',
            'as_needed' => 'As needed'
        ];

        $result['data'] = $periode;
        $result['rows'] = count($periode);
        echo json_encode($result);
    }
    public function get_master_checklist($ic_id=0){
        $param = [
            'where' => []
        ];

        $param['where']['b.branch_id'] = $this->branch_id;
        if ($this->branch_id == 16) {
            unset($param['where']['b.branch_id']);
        }

        /*
            params
            - nama_alat
            - nama_checklist
            - nama_branch
            - creator
            - note
            - periode
        */
        $post_params = [
            'nama_alat' => $this->input->post('nama_alat'),
            'nama_checklist' => $this->input->post('nama_checklist'),
            'nama_branch' => $this->input->post('nama_branch'),
            'creator' => $this->input->post('creator'),
            'note' => $this->input->post('note'),
            'periode' => $this->input->post('periode'),
            'branch_id' => $this->input->post('branch_id'),
        ]; 
        
        foreach ($post_params as $field => $value) {
            if (!empty($value)) {
                if ($field=='branch_id') {
                    $param['where']['b.branch_id'] = $value;
                }else{
                    $param['where'][$field. '  like '] = '%'.$value.'%';
                }
            }
        }

        if ($ic_id > 0) {
            $param['where']['id'] = $ic_id;
        }

        // print_r('<pre>');
        // print_r($param);
        // print_r('<pre>');die;
        $data = $this->m_mia->get_all_checklist($param)->result();
        $new_data = [];
        foreach ($data as $key => $value) {
            $new_data[] = [ 
                            'id' => $value->id,
                            'no' => ($key+1),
                            'nama_alat' => $value->alat_name,
                            'nama_checklist' => $value->checklist_name,
                            'nama_branch' => $value->branch_name,
                            'creator' => $value->creator,
                            'note' => $value->note,
                            'periode' => $value->periode
                        ];
        }

        $result = [];
        $result['data'] = $new_data;
        $result['rows'] = count($new_data);

        echo json_encode($result);
    }

    public function save_master_checklist(){
        $data = $this->input->post();
        $data['id'] = (isset($data['ic_id'])&&!empty($data['ic_id']))?$data['ic_id']:0;
        unset($data['ic_id']);
        $res['status'] = false;
        $edit = 0;

        if ($data['id'] > 0) {
            $edit = 1;
        }
        
        if ($edit) {

            $res['message'] = 'update data gagal';
            $save = $this->m_mia->update_checklist($data['id'],$data);
            if ($save) {
                $res['status'] = true;
                $res['message'] = 'update data berhasil';
            }
        }else{

            $res['message'] = 'simpan data gagal';
            $save = $this->m_mia->save_checklist($data);
            if ($save) {
                $res['status'] = true;
                $res['message'] = 'simpan data berhasil';
            }
        }

        echo json_encode($res);
    }

    public function delete_master_checklist(){
        $result['status'] = false;
        $result['message'] = 'Data gagal dihapus.';

        $key = $this->input->post('ic_id');
        $delete = $this->m_mia->hapus_checklist(['id' => $key]);

        if($delete){
            $result['status'] = true;
            $result['message'] = 'Data berhasil dihapus.';
        }

        echo json_encode($result);
    }

    /* master alat for checklist*/
    public function get_master_alat($mia_id=0){
        $param = [
            'where' => []
        ];

        $param['where']['b.branch_id'] = $this->branch_id;
        if ($this->branch_id == 16) {
            unset($param['where']['b.branch_id']);
        }

       /*
            params
            - nama_alat
            - serial_number
            - nama_branch
            - keterangan
            - creator
            - branch_id
        */
        $post_params = [
            'nama_alat' => $this->input->post('nama_alat'),
            'serial_number' => $this->input->post('serial_number'),
            'nama_branch' => $this->input->post('nama_branch'),
            'keterangan' => $this->input->post('keterangan'),
            'creator' => $this->input->post('creator'),
            'branch_id' => $this->input->post('branch_id'),
        ]; 
        
        foreach ($post_params as $field => $value) {
            if (!empty($value)) {
                if ($field=='branch_id') {
                    $param['where']['b.branch_id'] = $value;
                }else{
                    $param['where'][$field. '  like '] = '%'.$value.'%';
                }
            }
        }

        if ($mia_id>0) {
            $param['where']['id'] = $mia_id;
        }

        $data = $this->m_mia->get_all_data($param)->result();
        $new_data = [];
        foreach ($data as $key => $value) {
            $new_data[] = [ 
                            'id' => $value->id,
                            'no' => ($key+1),
                            'nama_alat' => $value->alat_name,
                            'serial_number' => $value->serial_number,
                            'nama_branch' => $value->branch_name,
                            'keterangan' => $value->keterangan,
                            'creator' => $value->creator
                        ];
        }

        $result = [];
        $result['data'] = $new_data;
        $result['rows'] = count($new_data);

        echo json_encode($result);
    }

    public function save_master_alat(){
        $param['branch_id'] = $this->branch_id;
        if ($this->branch_id == 16) {
            unset($param['where']['b.branch_id']);
        }

        $data = $this->input->post();
        $data['branch_id'] = $this->branch_id;
        $data['creator'] = $this->user_id;
        $data['id'] = (isset($data['mia_id'])&&!empty($data['mia_id']))?$data['mia_id']:'';
        unset($data['mia_id']);
        $res['status'] = false;
        
        $edit = 0;
        if ($data['id']>0) {
            $edit = 1;
        }

        if ($edit) {
            $res['message'] = 'update data gagal';
            $save = $this->m_mia->update_data($data['id'],$data);
            if ($save) {
                $res['status'] = true;
                $res['message'] = 'update data berhasil';
            }
        }else{

            $res['message'] = 'simpan data gagal';
            $save = $this->m_mia->save_data($data);
            if ($save) {
                $res['status'] = true;
                $res['message'] = 'simpan data berhasil';
            }
        }

        echo json_encode($res);
    }

    public function delete_master_alat(){
        $result['status'] = false;
        $result['message'] = 'Data gagal dihapus.';

        $key = $this->input->post('mia_id');
        $delete = $this->m_mia->hapus_data(['id' => $key]);

        if($delete){
            $result['status'] = true;
            $result['message'] = 'Data berhasil dihapus.';
        }

        echo json_encode($result);
    }

    /**/

    /*intern checklist input*/
    public function get_bulan(){
        $bulan = $this->misc->getBulan();
        echo json_encode($bulan);
    }

    public function get_tahun(){
        $tahun = $this->misc->getTahun();
        echo json_encode($tahun);
    }

    public function get_checklist(){
        $param = [
            'ic.periode' => $this->input->post('periode'),
            'ic.id_intern_alat' => $this->input->post('id_intern_alat')
        ];
        $ic = $this->m_mia->get_all_checklist($param)->result();
        
        /* -check apakah data sudah diinputkan pada bulan & thn yg sama */
        $p = [
            'ic.periode' => $this->input->post('periode'),
            'ic.id_intern_alat' => $this->input->post('id_intern_alat'),
            'month(ic_in.tanggal)' => date('m'),
            'year(ic_in.tanggal)' => date('Y')
        ];
        $saved_ic = $this->m_ic->get_all_checklist_input($p)->result();
        
        if (!empty($saved_ic)) {
            foreach ($saved_ic as $key => $value) {
                foreach ($ic as $k => $v) {
                    if ($v->id == $value->ic_id) {
                        $ic[$k]->value = $value->value;
                        $ic[$k]->update = 1;
                        $ic[$k]->ic_id = $value->id;
                    }
                }
            }  
        }
        /* end */
        $result = [
            'code' => !empty($ic)?200:400,
            'data' => $ic,
        ];

        echo json_encode($result);
    }

    public function get_checklist_input(){

        $param['b.branch_id'] = $this->branch_id;
        if ($this->branch_id == 16) {
            unset($param['b.branch_id']);
        }

        $data = $this->m_ic->get_all_checklist_input($param)->result();
        $new_data = [];
        $new_data['code'] = 200;
        foreach ($data as $key => $value) {
            $new_data['data'][] = [ 
                            'id' => $value->id,
                            'no' => ($key+1),
                            'periode' => $value->periode,
                            'tanggal' => $value->tanggal,
                            'alat_name' => $value->alat_name,
                            'checklist_name' => $value->checklist_name,
                            'nama_branch' => $value->branch_name,
                            'creator' => $value->creator,
                            'value' => $value->value
                        ];
        }

        $new_data['rows'] = count($data);
        echo json_encode($new_data);
    }

    public function get_cheklist_form(){
        $param = [
            'ic.id_intern_alat' => $this->input->post('id_intern_alat')
        ];
       
        $ic = $this->m_mia->get_all_checklist($param)->result();
        
        /* -check apakah data sudah diinputkan pada bulan & thn yg sama */
        $p = [
            'ic.id_intern_alat' => $this->input->post('id_intern_alat'),
            'month(ic_in.tanggal)' => $this->input->post('bulan'),
            'year(ic_in.tanggal)' => $this->input->post('tahun')
        ];
        
        $saved_ic = $this->m_ic->get_all_checklist_input($p)->result();
        
        $periode = [
            'harian' => 'Harian',
            'mingguan' => 'Mingguan',
            'bulanan' => 'Bulanan',
            'tri_month' => '3 Bulanan',
            'six_month' => '6 Bulanan',
            'as_needed' => 'As needed'
        ];

        $ic_data = [];
        foreach ($periode as $key => $value) {
            $ic_data[$key] = [];

        }

        foreach ($ic as $k => $v) {
            /*set default value*/
            switch ($v->periode) {
                case 'harian':
                    for ($i=1; $i <= 31; $i++) { 
                        $ic_data[$v->periode][$v->id][$i] = [];
                    }
                    break;
                
                case 'mingguan':
                    for ($i=1; $i < 5; $i++) { 
                        $ic_data[$v->periode][$v->id][$i] = [];
                    }
                    break;

                case 'bulanan':
                    for ($i=1; $i <= 12; $i++) { 
                        $ic_data[$v->periode][$v->id][$i] = [];
                    }
                    break;

                case 'as_needed':
                    for ($i=1; $i <= 31 ; $i++) { 
                        $ic_data[$v->periode][$v->id][$i] = [];
                    }
                    break;

                case 'tri_month':
                    for ($i=1; $i <= 4 ; $i++) { 
                        $ic_data[$v->periode][$v->id][$i] = [];
                    }
                    break;

                case 'six_month':
                    for ($i=1; $i <= 2 ; $i++) { 
                        $ic_data[$v->periode][$v->id][$i] = [];
                    }
                    break;
                    
                default:
                    # code...
                    break;
            }
        }

        /*checklist_name*/
        $checklist_name = [];
        foreach ($ic as $k => $v) {
            $checklist_name[$v->id] = $v->checklist_name;
        }

        /*param*/
        $mnth = $this->input->post('bulan');
        $yrs = $this->input->post('tahun');
        $mnth = ($mnth>9)?$mnth:'0'.$mnth;
        /*formating list harian*/
        if (!empty($ic_data['harian'])) {
            $list_ = $ic_data['harian'];
            unset($ic_data['harian']);
            foreach ($list_ as $icid => $value) {
                foreach ($value as $k => $v) {
                    $ic_data['harian'][$icid][$k] = [
                        'ic_id' => $icid,
                        'ic_in_id' => 0,
                        'tgl' => $yrs.'-'.$mnth.'-'.(($i>9)?$k:'0'.$k),
                        'nilai' => 0,
                        'nama_cheklist' => $checklist_name[$icid]
                    ];
                    /*set the value if has inputed*/
                    foreach ($saved_ic as $sk => $sv) {
                        if ($sv->ic_id == $icid) {
                            $tgl = intval(substr($sv->tanggal,8));
                            if ($k == $tgl) {
                                 $ic_data['harian'][$icid][$k]['nilai'] = 1;
                                 $ic_data['harian'][$icid][$k]['ic_in_id'] = $sv->id;
                            }
                        }
                    }
                }
            }

        }
        /**/

        /*formating list mingguan*/
        if (!empty($ic_data['mingguan'])) {
            unset($list_);
            $list_ = $ic_data['mingguan'];
            unset($ic_data['mingguan']);
            foreach ($list_ as $icid => $value) {
                foreach ($value as $k => $v) {
                    $ic_data['mingguan'][$icid][$k] = [
                        'ic_id' => $icid,
                        'ic_in_id' => 0,
                        'weeks' => $k,
                        'tgl' => ''/*kosong*/,
                        'nilai' => 0,
                        'nama_cheklist' => $checklist_name[$icid]
                    ];
                    /*set the value if has inputed*/
                    foreach ($saved_ic as $sk => $sv) {
                        if ($sv->ic_id == $icid) {
                            $week = $this->misc->weekOfMonth($sv->tanggal);
                            if ($k == $week) {
                                $ic_data['mingguan'][$icid][$k]['nilai'] = 1;
                                $ic_data['mingguan'][$icid][$k]['ic_in_id'] = $sv->id;
                                $ic_data['mingguan'][$icid][$k]['tgl'] = $sv->tanggal;
                            }
                        }
                    }
                }
            }

        }
        /**/

        /*formating list bulanan*/
        if (!empty($ic_data['bulanan'])) {
            $list_ = $ic_data['bulanan'];
            unset($ic_data['bulanan']);
            foreach ($list_ as $icid => $value) {
                foreach ($value as $k => $v) {
                    $ic_data['bulanan'][$icid][$k] = [
                        'ic_id' => $icid,
                        'ic_in_id' => 0,
                        'bulan' => $k,
                        'tgl' => ''/*kosong*/,
                        'nilai' => 0,
                        'nama_cheklist' => $checklist_name[$icid]
                    ];
                    /*set the value if has inputed*/
                    foreach ($saved_ic as $sk => $sv) {
                        if ($sv->ic_id == $icid) {
                            $bln = substr($sv->tanggal,5,2);
                            if ($k == $bln) {
                                $ic_data['bulanan'][$icid][$k]['nilai'] = 1;
                                $ic_data['bulanan'][$icid][$k]['ic_in_id'] = $sv->id;
                                $ic_data['bulanan'][$icid][$k]['tgl'] = $sv->tanggal;
                            }
                        }
                    }
                }
            }

        }
        /**/

        /*formating list as_needed*/
        if (!empty($ic_data['as_needed'])) {
            $list_ = $ic_data['as_needed'];
            unset($ic_data['as_needed']);
            foreach ($list_ as $icid => $value) {
                foreach ($value as $k => $v) {
                    $ic_data['as_needed'][$icid][$k] = [
                        'ic_id' => $icid,
                        'ic_in_id' => 0,
                        'tgl' => $yrs.'-'.$mnth.'-'.(($i>9)?$k:'0'.$k),
                        'nilai' => 0,
                        'nama_cheklist' => $checklist_name[$icid]
                    ];
                    /*set the value if has inputed*/
                    foreach ($saved_ic as $sk => $sv) {
                        if ($sv->ic_id == $icid) {
                            $tgl = intval(substr($sv->tanggal,8));
                            if ($k == $tgl) {
                                 $ic_data['as_needed'][$icid][$k]['nilai'] = 1;
                                 $ic_data['as_needed'][$icid][$k]['ic_in_id'] = $sv->id;
                            }
                        }
                    }
                }
            } 

        }
        /**/

        /*formating list 3 bulanan*/
        if (!empty($ic_data['tri_month'])) {
            $list_ = $ic_data['tri_month'];
            unset($ic_data['tri_month']);
            for ($i=1; $i <= 4 ; $i++) { 
                foreach ($list_ as $icid => $value) {
                    foreach ($value as $k => $v) {
                        $ic_data['tri_month'][$icid][$k] = [
                            'ic_id' => $icid,
                            'ic_in_id' => 0,
                            'tgl' => ''/*kosong*/,
                            'nilai' => 0,
                            'nama_cheklist' => $checklist_name[$icid]
                        ];
                        /*set the value if has inputed*/
                        foreach ($saved_ic as $sk => $sv) {
                            if ($sv->ic_id == $icid) {
                                $tgl = intval($this->misc->triwulanOfMonth($sv->tanggal));
                                if ($k == $tgl) {
                                     $ic_data['tri_month'][$icid][$k]['nilai'] = 1;
                                     $ic_data['tri_month'][$icid][$k]['ic_in_id'] = $sv->id;
                                     $ic_data['tri_month'][$icid][$k]['tgl'] = $sv->tanggal;
                                }
                            }
                        }
                    }
                }

            } 

        }
        /**/

        /*formating list 6 bulanan*/
        if (!empty($ic_data['six_month'])) {
            $list_ = $ic_data['six_month'];
            unset($ic_data['six_month']);
            for ($i=1; $i <= 2 ; $i++) { 
                foreach ($list_ as $icid => $value) {
                    foreach ($value as $k => $v) {
                        $ic_data['six_month'][$icid][$k] = [
                            'ic_id' => $icid,
                            'ic_in_id' => 0,
                            'tgl' => ''/*kosong*/,
                            'nilai' => 0,
                            'nama_cheklist' => $checklist_name[$icid]
                        ];
                        /*set the value if has inputed*/
                        foreach ($saved_ic as $sk => $sv) {
                            if ($sv->ic_id == $icid) {
                                $tgl = intval($this->misc->semesterOfMonth($sv->tanggal));
                                if ($k == $tgl) {
                                     $ic_data['six_month'][$icid][$k]['nilai'] = 1;
                                     $ic_data['six_month'][$icid][$k]['ic_in_id'] = $sv->id;
                                     $ic_data['six_month'][$icid][$k]['tgl'] = $sv->tanggal;
                                }
                            }
                        }
                    }
                }

            } 

        }


        /* end */
        $result = [
            'code' => !empty($ic)?200:400,
            'data' => $ic_data,
            'checklist_name' => $checklist_name
        ];

        echo json_encode($result);
    }

    public function get_cheklist_report(){
        $param = [
            'month(ic_in.tanggal)' => $this->input->post('bulan'),
            'year(ic_in.tanggal)' => $this->input->post('tahun'),
            'ic.id_intern_alat' => $this->input->post('id_intern_alat')
        ];

        $periode = [
            'harian' => 'Harian',
            'mingguan' => 'Mingguan',
            'bulanan' => 'Bulanan',
            'tri_month' => '3 Bulanan',
            'six_month' => '6 Bulanan',
            'as_needed' => 'As needed'
        ];

        $data = [];

        foreach ($periode as $key => $value) {
            $param['ic.periode'] = strtolower($key);
            $data[$key] = $this->m_ic->get_all_checklist_input($param)->result();
            unset($param['ic.periode']);
        }

        /*formating list harian*/
        if (!empty($data['harian'])) {
            $list_ = $data['harian'];
            unset($data['harian']);
            
            for ($i=1; $i <= 31 ; $i++) { 
                foreach ($list_ as $key => $value) {
                    $tgl = intval(substr($value->tanggal,8));
                    if ($i==$tgl) {
                        $data['harian'][$value->checklist_name][$i] = '<i class="fa fa-lg fa-check-circle text-success"></i><br>('.$value->creator.')';
                    }else{
                        $data['harian'][$value->checklist_name][$i] = '<i class="fa fa-lg fa-times-circle text-danger"></i>';
                    }
                }

            }

        }
        /**/

        /*formating list mingguan*/
        if (!empty($data['mingguan'])) {
            $list_ = $data['mingguan'];
            unset($data['mingguan']);
            /*assume 1 month has 4 weeks*/
            for ($i=1; $i < 5; $i++) { 
                foreach ($list_ as $key => $value) {
                    $data['mingguan'][$value->checklist_name][$i] = '';
                    $weekFromDate = $this->misc->weekOfMonth($value->tanggal);
                    if ($weekFromDate == $i) {
                        $data['mingguan'][$value->checklist_name][$i] = '<i class="fa fa-check-circle fa-lg text-success"></i><br>('.$value->creator.' : '.$this->misc->dateFormat($value->tanggal).')';    
                    }else{
                        $data['mingguan'][$value->checklist_name][$i] = '<i class="fa fa-lg fa-times-circle text-danger"></i>';
                    }
                    
                }
            }

        }
        /**/

        /*formating list bulanan*/
        if (!empty($data['bulanan'])) {
            $list_ = $data['bulanan'];
            unset($data['bulanan']);
            
            for ($i=1; $i <= 12; $i++) { 
                foreach ($list_ as $key => $value) {
                    $data['bulanan'][$value->checklist_name][$i] = '';
                    $getBulan = intval(substr($value->tanggal,5,2));
                    if ($i==intval($getBulan)) {
                        $data['bulanan'][$value->checklist_name][$i] = '<i class="fa fa-check-circle fa-lg text-success"></i><br>('.$value->creator.' : '.$this->misc->dateFormat($value->tanggal).')';    
                    }else{
                        $data['bulanan'][$value->checklist_name][$i] = '<i class="fa fa-lg fa-times-circle text-danger"></i>'; 
                    }
                }
            }

        }
        /**/

        /*formating list as_needed*/
        if (!empty($data['as_needed'])) {
            $list_ = $data['as_needed'];
            
            unset($data['as_needed']);
            for ($i=1; $i <= 31 ; $i++) { 
                foreach ($list_ as $key => $value) {
                    $tgl = intval(substr($value->tanggal,8));
                    if ($i==$tgl) {
                        $data['as_needed'][$value->checklist_name][$i] = '<i class="fa fa-lg fa-check-circle text-success"></i><br>('.$value->creator.')';
                    }else{
                        $data['as_needed'][$value->checklist_name][$i] = '<i class="fa fa-lg fa-times-circle text-danger"></i>';
                    }
                }

            }

        }
        /**/

        /*formating list 3 bulanan*/
        if (!empty($data['tri_month'])) {
            $list_ = $data['tri_month'];
            unset($data['tri_month']);
            for ($i=1; $i <= 4 ; $i++) { 
                foreach ($list_ as $key => $value) {
                    $key = $this->misc->triwulanOfMonth($value->tanggal);
                    if ($i==$key) {
                        $data['tri_month'][$value->checklist_name][$i] = '<i class="fa fa-check-circle fa-lg text-success"></i><br>('.$value->creator.' : '.$this->misc->dateFormat($value->tanggal).')';
                    }else{
                        $data['tri_month'][$value->checklist_name][$i] = '<i class="fa fa-lg fa-times-circle text-danger"></i>';
                    }
                }

            } 

        }
        /**/

        /*formating list 6 bulanan*/
        if (!empty($data['six_month'])) {
            $list_ = $data['six_month'];
            unset($data['six_month']);
            for ($i=1; $i <= 2 ; $i++) { 
                foreach ($list_ as $key => $value) {
                    $key = $this->misc->semesterOfMonth($value->tanggal);
                    if ($i==$key) {
                        $data['six_month'][$value->checklist_name][$i] = '<i class="fa fa-check-circle fa-lg text-success"></i><br>('.$value->creator.' : '.$this->misc->dateFormat($value->tanggal).')';
                    }else{
                        $data['six_month'][$value->checklist_name][$i] = '<i class="fa fa-lg fa-times-circle text-danger"></i>';
                    }
                }

            } 

        }
        /**/
        
        echo json_encode($data);
    }

    /**/
    // public function save_data(){

    //     $data = $this->input->post();
    //     $save_data = [];
    //     if (!empty($data['id_intern_checklist'])) {
    //         /*save*/
    //         foreach ($data['id_intern_checklist'] as $key => $value) {
    //             if (!empty($value)) {
    //                 $save_data = [
    //                     'id_intern_checklist' => $key,
    //                     'value' => $value,
    //                     'tanggal' => date('Y-m-d'),
    //                     'creator' => $this->user_id
    //                 ];

    //                 if ($data['id_update'][$key] > 0) {
    //                     /*update_data*/
    //                     $save = $this->m_ic->update_data($data['id_update'][$key],$save_data);
    //                 }else{
    //                     /*save*/
    //                     $save = $this->m_ic->save_data($save_data);
    //                 }
                    
    //             }
                
    //         }
            
    //     }
       
        
    //     $result = [
    //         'code' => $save?200:304,
    //         'msg' =>  $save?'data berhasil disimpan!':'data gagal disimpan!',
    //     ];

    //     echo json_encode($result);
    // }

    public function update_ic_input(){
        $data = $this->input->post();
        $save_data = [
            'id_intern_checklist' => $data['ic_id'],
            'value' => $data['value'],
            'tanggal' => (isset($data['tgl'])&&!empty($data['tgl']))?$data['tgl']:date('Y-m-d'),
            'creator' => $this->user_id
        ];

        if ($data['ic_in_id'] > 0) {
            /*update*/
            $save = $this->m_ic->update_data($data['ic_in_id'],$save_data);
        }else{
            /*create*/
            $save = $this->m_ic->save_data($save_data);
        }

        $result = [
            'code' => $save?200:304,
            'msg' =>  $save?'data berhasil disimpan!':'data gagal disimpan!',
        ];

        echo json_encode($result);
    }

    // public function hapus_checklist_input()
    // {
    //     $result['status'] = false;
    //     $result['message'] = 'Data gagal dihapus.';

    //     $key = $this->input->post('key');
    //     $delete = $this->m_ic->hapus_data(['id' => $key]);

    //     if($delete){
    //         $result['status'] = true;
    //         $result['message'] = 'Data berhasil dihapus.';
    //     }

    //     echo json_encode($result);
    // }
    /**/
}