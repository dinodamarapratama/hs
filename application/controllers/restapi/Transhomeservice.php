<?php

class Transhomeservice extends CI_Controller
{

    public $model = 'hs_payment';
    public $model1 = 'hs_initial_petugas_hs';
    public $model2 = 'hs_time';
    public $model3 = 'hs_abbr';
    public $model4 = 'hs_block';
    public $model5 = 'master_fs';

    function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json');
        $this->restapikey= $this->config->config['restapikey'];
        if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        $this->user_id = $this->input->post('user_id');
        $this->load->model("restapi/user_model");
        $user = $this->user_model->one_user(["where" => ["_.id_user" => $this->user_id]]);
        if ($user == null) {
            echo json_encode(["status" => false, "message" => "user is invalid"]);
            exit();
        }
        $this->load->model('restapi/ths_model');
        $this->load->model('m_hs');
        $this->branch_id = $user['branch_id'];
        $this->load->library('Notify_lib');
    }

    public function getHomeServices()
    {
        $search = $this->input->post('search');

        if ($search) {
            $where['homeservice.nama like'] = '%' . $search . '%';
        }
        // $where['petugas_id'] = $this->user_id;
        $where['id_user'] = $this->user_id;
        $where['homeservice.date'] = date("Y-m-d");
        $where['status'] = 'Diproses';

        $process = $this->ths_model->getHs($where)->result();

        $where['status'] = 'Selesai';
        $done = $this->ths_model->getHs($where)->result();

        $data = [
            'process' => $process,
            'done' => $done
        ];

        echo json_encode($data);
    }

    public function createTransaction()
    {
        $list = $this->input->post('list');
        $id_hs = $this->input->post('id_hs');
        $total = $this->input->post('total_harga');
        $disc = $this->input->post('disc');
        $subtotal = $this->input->post('subtotal');

        $this->db->trans_start();

        $data['id_hs'] = $id_hs;
        $data['tgl_ths'] = date("Y-m-d H:i:s");
        $data['total_harga'] = $total;
        $data['disc'] = $disc;
        $data['creator_id'] = $this->user_id;
        $data['subtotal'] = $subtotal;

        $this->db->insert('ths', $data);
        $id_ths = $this->db->insert_id();

        $insert_list = array();

        foreach ($list as $i => $ls) {
            $insert_list[$i]['id_ths'] = $id_ths;
            $insert_list[$i]['id_price_list'] = $ls;
        }

        $this->db->insert_batch('ths_price', $insert_list);

        $this->db->update('homeservice', ['status' => 'Selesai'], ['id' => $id_hs]);

        $this->db->trans_complete();

        echo json_encode(['id_ths' => $id_ths]);
    }

    public function Transfer()
    {
            $id_hs = $this->input->post('id_hs');
            $user_id = $this->input->post('$user_id');
            $hs = $this->ths_model->getHs(['id' => $id_hs])->row();
            // $id_notif = $this->input->post('id_notif');
            $data['id_user'] = $this->input->post('id_petugas_new');
            $data['abbr_hs'] = $hs->abbr_hs;
            $data['start_date'] = $hs->start_date;
            $data['end_date'] = $hs->end_date;
            $data['branch_id'] = $hs->branch_id;
            $data['creator_id'] = $hs->creator_id;
            $data['created_at'] = date("Y-m-d H:i:s");

            $this->m_masterhs->insert_data($data, $this->model1);
            $id_petugas_new = $this->db->insert_id();
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan';

            
    
            $this->db->where('id', $id_hs);
            $this->db->update('homeservice', ['ptgshs_id' => $id_petugas_new]);

        // echo ($hs->time_id);
		$data_time1 = $this->m_hs->edit_data(array('time_id' => $hs->time_id),'hs_time')->row();
		$jam = $data_time1->time_name;

            
            $this->load->model('restapi/user_model');
            $sender = $this->user_model->one_user(['where' => ['_.id_user' => $this->input->post('id_petugas_new')]]);
            $idReceivers[] = $sender['id_user'];

            $this->load->library('Notify_lib');
			$ntf = new Notify_lib();
			$ntf->send(
					'New Home Service',
					$sender['name'] . ' - '.$sender['position_name'].' ('.$sender['branch_name'].') anda mendapatkan tugas atas nama pasien '.$hs->nama.' dan jam '.$jam,
					$this->user_id,
					$idReceivers,
					['id' => $this->input->post('id_petugas_new'),'date' => $hs->start_date,'branch_id' => $hs->branch_id],
					'homeservice',
					true
				);

    
    echo json_encode($result);
    
    }
    


    public function getHomeService($id_hs)
    {
        $data = $this->ths_model->getHs(['id' => $id_hs])->row();
        $ths = $this->db->get_where('ths', ['id_hs' => $id_hs])->row();
        if ($ths) {
            $data->ths = $ths;
            $childs = $this->ths_model->getChild($ths->id_ths)->result();
            $teschilds = [];
            foreach($childs as $child){
                $teschild = $this->ths_model->getTesByThs(null, $child);
                foreach ($teschild as $ts) {
                    array_push($teschilds, $ts);
                }
            }
            $data->tes = $this->ths_model->getTesByThs($ths, null);
            $data->child = $childs;
            $data->teschild = $teschilds;

            
        } else {
            $data->child = array();
        }
        
        echo json_encode($data);
    }

    public function getPriceList()
    {
        $page = $this->input->get('page') ? $this->input->get('page') : 1;
        $perPage = $this->input->get('perPage') ? $this->input->get('perPage') : 5;
        $search = $this->input->get('search');
        $offset = ($page - 1) * $perPage;

        $this->db->select('*');
        $this->db->from('ths_master_pricelist');
        if ($search) {
            $this->db->like('test_name', $search);
        }
        $this->db->limit($perPage, $offset);
        $data = $this->db->get()->result();

        $result = [
            'page' => (int)$page,
            'perPage' => (int)$perPage,
            'data' => $data
        ];

        echo json_encode($result);
    }

    public function getPriceList10()
    {
        $page = $this->input->get('page') ? $this->input->get('page') : 1;
        $perPage = $this->input->get('perPage') ? $this->input->get('perPage') : 5;
        $search = $this->input->get('search');
        $offset = ($page - 1) * $perPage;

        $this->db->select('*');
        $this->db->from('ths_master_pricelist_10');
        if ($search) {
            $this->db->like('test_name', $search);
        }
        $this->db->limit($perPage, $offset);
        $data = $this->db->get()->result();

        $result = [
            'page' => (int)$page,
            'perPage' => (int)$perPage,
            'data' => $data
        ];

        echo json_encode($result);
    }

    public function getPriceListLoyalty()
    {
        $page = $this->input->get('page') ? $this->input->get('page') : 1;
        $perPage = $this->input->get('perPage') ? $this->input->get('perPage') : 5;
        $search = $this->input->get('search');
        $offset = ($page - 1) * $perPage;

        $this->db->select('*');
        $this->db->from('ths_master_pricelist_loyalty');
        if ($search) {
            $this->db->like('test_name', $search);
        }
        $this->db->limit($perPage, $offset);
        $data = $this->db->get()->result();

        $result = [
            'page' => (int)$page,
            'perPage' => (int)$perPage,
            'data' => $data
        ];

        echo json_encode($result);
    }

    public function createTransactionChild()
    {
        $list = $this->input->post('list');
        $id_ths = $this->input->post('id_ths');
        $total = $this->input->post('total');
        $disc = $this->input->post('disc');
        $subtotal = $this->input->post('subtotal');
        $name = $this->input->post('name');
        // $id_relation = $this->input->post('id_relation');
        $relation = $this->input->post('relation_status');

        $this->db->trans_start();

        //input ke tabel relation
        $rel['relation_name'] = $name;
        $rel['relation_status'] = (int)$relation;
        $this->db->insert('ths_relation', $rel);
        $id_rel = $this->db->insert_id();

        $data['id_ths'] = $id_ths;
        $data['name'] = $name;
        $data['id_relation'] = $id_rel;
        $data['date'] = date("Y-m-d H:i:s");
        $data['subtotal'] = $subtotal;
        $data['disc'] = $disc;
        $data['total'] = $total;
        $data['creator_id'] = $this->user_id;

        $this->db->insert('ths_child', $data);
        $id_ths = $this->db->insert_id();


        $insert_list = array();
        if (isset($list)) {
            foreach ($list as $i => $ls) {
                $insert_list[$i]['id_child_ths'] = $id_ths;
                $insert_list[$i]['id_price_list'] = $ls;
            }

            $this->db->insert_batch('ths_price_child', $insert_list);
        }

        $this->db->trans_complete();

        

        echo json_encode(['id_ths' => $id_ths]);
    }

    public function dataPrint($id_hs)
    {
        $this->db->trans_start();

        $ths = $this->db->get_where('ths', ['id_hs' => $id_hs])->row();
        $ths->hs = $this->db->get_where('homeservice', ['id' => $ths->id_hs])->row();
        $ths->items = $this->db->get_where('ths_price', ['id_ths' => $ths->id_ths])->result();

        foreach ($ths->items as $i => $v) {
            $ths->items[$i] = $this->db->get_where('ths_master_pricelist', ['id_pricelist' => $v->id_price_list])->row();
        }

        $ths->child = $this->db->get_where('ths_child', ['id_ths' => $ths->id_ths])->result();

        foreach ($ths->child as $j => $w) {
            $ths->child[$j]->items = $this->db->get_where('ths_price_child', ['id_child_ths' => $w->id_child_ths])->result();
            foreach ($ths->child[$j]->items as $k => $z) {
                $ths->child[$j]->items[$k] = $this->db->get_where('ths_master_pricelist', ['id_pricelist' => $z->id_price_list])->row();
            }
        }
        $this->db->trans_complete();

        echo json_encode($ths);
    }

    public function getRelation()
    {
        $data = $this->db->get('ths_relation')->result();

        $result = [
            'data' => $data
        ];

        echo json_encode($result);
    }


}
