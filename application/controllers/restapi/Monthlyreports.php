<?php

class Monthlyreports extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->restapikey= $this->config->config['restapikey'];
        if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        $this->user_id = $this->input->post("user_id");
        $this->load->model("restapi/user_model");
        $user = $this->user_model->one_user(["where" => ["_.id_user" => $this->user_id]]);
        if($user == null) {
            echo json_encode(["status" => false, "message" => "user is invalid"]);
            exit();
        }
        $this->load->model('m_monthlyreports', 'cmodel');
    }

    function count_report(){
        $search = $this->input->post('search');
        $status = $this->input->post('status');
        $args = [];
        if(empty($search) == false) {
            $args['like'] = [
                [
                    'search' => $search,
                    'cols' => [
                        '_.title', '_.description', '_.kategori', 'rply.reply_all', 'usr.name', 'usr.username'
                    ]
                ]
            ];
        }

        $r1 = $this->cmodel->getCountReceived($this->user_id, $args);
        $r2 = $this->cmodel->getCountSent($this->user_id, $args);
        $r3 = $this->cmodel->getCountArchived($this->user_id, $args);
        $r = [
            'received' => $r1->jumlah,
            'sent' => $r2->jumlah,
            'archived' => $r3->jumlah
        ];
        echo json_encode($r);
    }

    function many_report(){
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $search = $this->input->post('search');
        $status = $this->input->post('status');

        $args = [
            'where' => [],
            // 'order' => [ ['_.id', 'desc'] ]
        ];

        if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
            $args['limit'] = [ $start, $length ];
        }

        if(empty($search) == false) {
            $args['like'] = [
                [
                    'search' => $search,
                    'cols' => [
                        '_.title', '_.description', '_.kategori', 'rply.reply_all', 'usr.name', 'usr.username'
                    ]
                ]
            ];
        }

        $r = [];
        if($status == 'received') {
            $r = $this->cmodel->getReceived($this->user_id, $args);
        } else if($status == 'sent') {
            $r = $this->cmodel->getSent($this->user_id, $args);
        } else if($status == 'archived') {
            $r = $this->cmodel->getArchived($this->user_id, $args);
        }
        echo json_encode($r);
    }

    function save(){
        $id_report = $this->input->post('id');
        $r = [];
        if(empty($id_report)){
            $data = [
                'from_id' => $this->user_id,
                'status' => $this->input->post('status'),
                'kategori' => $this->input->post('category'),
                'title' => $this->input->post('subject'),
                'description' => $this->input->post('body'),
                'is_view' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'last_update' => date('Y-m-d H:i:s'),
                'date' => date('Y-m-d'),
                'id_draft' => 0,
                'receivers' => $this->input->post('receivers'),
                'departments' => $this->input->post('department'),
                'bagian' => $this->input->post('bagian')
            ];
            $r = $this->cmodel->save($data);
        } else {
            $data = [
                // 'from_id' => $this->user_id,
                'status' => $this->input->post('status'),
                'kategori' => $this->input->post('category'),
                'title' => $this->input->post('subject'),
                'description' => $this->input->post('body'),
                // 'is_view' => 1,
                // 'created_at' => date('Y-m-d H:i:s'),
                // 'last_update' => date('Y-m-d H:i:s'),
                // 'date' => date('Y-m-d'),
                // 'id_draft' => 0,
                // 'receivers' => $this->input->post('receivers'),
            ];
            $r = $this->cmodel->update_report(['_.id' => $id_report], $data);
        }
        
        if($r['status'] == true) {
            $counter = -1;
            $list_upload = [];
            $attachment_descriptions = $this->input->post('attachments');
            $attachment_descriptions_length = is_array($attachment_descriptions) ? count($attachment_descriptions) : 0;
            while(true) {
                $counter++;
                $key = 'file_' . $counter;
                if(isset($_FILES[$key]) == false) {
                    break;
                }
                if($counter > 100) {
                    break;
                }
                $config = [
                    'upload_path' => 'uploads/attachments/monthlyreports/',
                    'allowed_types' => '*',
                    'encrypt_name' => true
                ];

                if(file_exists($config['upload_path']) == false) {
                    $old = umask(0);
                    mkdir($config['upload_path'], 0777, true);
                    umask($old);
                }

                $this->upload->initialize($config);

                if($this->upload->do_upload($key)) {
                    $uploaded = $this->upload->data();
                    $list_upload[] = $uploaded;
                    $desc = $this->input->post('file_desc_' . $counter);
                    $title = $this->input->post('file_title_' . $counter);
                    // if($counter < $attachment_descriptions_length) {
                    //     $d = $attachment_descriptions[$counter];
                    //     $desc = $d['description'];
                    //     $title = $d['title'];
                    // }
                    $this->cmodel->save_attachment([
                        'monthlyreports_id' => $r['id'],
                        'title' => strlen($title) > 0 ? $title : $uploaded['orig_name'],
                        'description' => $desc,
                        'path' => $config['upload_path'] . $uploaded['file_name'],
                        'filetype' => $uploaded['file_type'],
                        'filesize' => $uploaded['file_size'],
                        'name' => $uploaded['orig_name'],
                    ]);
                }
            }
            $r['upload'] = $list_upload;
        }
        echo json_encode($r);
    }

    function delete(){
        $id = $this->input->get_post('id');
        $r = $this->cmodel->hapus_report(['_.id' => $id]);
        echo json_encode($r);
    }

    function detail(){
        $id = $this->input->get_post('id');
        $r = $this->cmodel->one_report(['where' => ['_.id' => $id]]);

        if($r != null) {
            $this->cmodel->mark_read($r->id, $this->user_id, $r);
            // $this->cmodel->auto_close($r->id, $r);
        }
        echo json_encode($r);
    }

    function update_status(){
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        $r = $this->cmodel->update_report(['_.id' => $id], ['status' => $status]);
        echo json_encode($r);
    }

    function reply(){
        $id_reply = $this->input->post('reply_id');
        $r = ['status' => false, 'message' => 'Unknown'];
        if(empty($id_reply)){
            $data = [
                'id_monthlyreports' => $this->input->post('id'),
                'message_reply' => $this->input->post('message'),
                'sender' => $this->user_id,
                'date_added' => date('Y-m-d H:i:s')
            ];
            $r = $this->cmodel->save_reply($data);
        } else {
            $data = [
                // 'id_monthlyreports' => $this->input->post('id'),
                'message_reply' => $this->input->post('message'),
                // 'sender' => $this->user_id
            ];
            $r = $this->cmodel->update_reply(['_.id' => $id_reply], $data);
        }

        if($r['status'] == true) {
            $counter = -1;
            $list_upload = [];
            while(true) {
                $counter++;
                $key = 'file_' . $counter;
                if(isset($_FILES[$key]) == false) {
                    break;
                }
                if($counter > 100) {
                    break;
                }
                $config = [
                    'upload_path' => 'uploads/attachments/monthlyreports/',
                    'allowed_types' => '*',
                    'encrypt_name' => true
                ];

                $this->upload->initialize($config);

                if($this->upload->do_upload($key)) {
                    $uploaded = $this->upload->data();
                    $list_upload[] = $uploaded;

                    $this->cmodel->save_reply_attachment([
                        'id_monthlyreports_reply' => $r['id'],
                        'path' => $config['upload_path'] . $uploaded['file_name'],
                        'name' => $uploaded['orig_name'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'filetype' => $uploaded['file_type'],
                        'filesize' => $uploaded['file_size']
                    ]);
                }
            }
        }
        
        echo json_encode($r);
    }

    function get_replies(){
        $id_report = $this->input->get_post('id');
        $r = $this->cmodel->getReplies($id_report);
        echo json_encode($r);
    }

    function hapus_reply(){
        $id_reply = $this->input->post('id_reply');
        $r = $this->cmodel->hapus_reply(['_.id' => $id_reply]);
        echo json_encode($r);
    }

    function hapus_reply_attachment(){
        $id = $this->input->post('id');
        $r = $this->cmodel->hapus_reply_attachment(['id' => $id]);
        echo json_encode($r);
    }
}