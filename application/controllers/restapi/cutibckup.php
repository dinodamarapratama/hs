  <?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cuti extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->restapikey= $this->config->config['restapikey'];
        $this->restapikey= $this->config->config['restapikey'];
        if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        $this->load->model('restapi/user_model');
        $this->current_user = $this->input->post('user_id');
        $this->me = $this->user_model->one_user(['where' => ['_.id_user' => $this->current_user]]);
        if($this->me == null) {
            echo json_encode(['status' => false, 'message' => 'Invalid user']);
            exit();
        }
        $this->load->model('m_cuti_api');
        $this->load->model('m_cuti');
        $this->load->library('Notify_lib');
    }

    public function state()
    {
        $params['id_user'] = $this->current_user;

        $karyawan =  $this->db->query("select CUTI as jatah_cuti, TGLMASUK as tglmasuk
            from users a
            where id_user='$this->current_user'
        ")->first_row();

        $cuti_diambil = $this->getLastCuti($this->current_user);
        $karyawan->jatah_diambil = $cuti_diambil;
        $karyawan->jatah_cuti = $karyawan->jatah_cuti+$cuti_diambil;
        return $this->response([
          'msg'=> 'state',
          'data'=> $karyawan
        ]);
    }

    public function getLastCuti($id_user=0, $type = 'view')
    {
        $param['year(tgl_request)'] = date('Y');
        $param['users.id_user'] = $id_user;
        //$param['cuti.status_pengajuan'] = 'Sudah disetujui!';
        $param['(cuti.status_pengajuan = "Sudah disetujui!" or cuti.status_pengajuan = "Sudah Disetujui!" or cuti.status_pengajuan = "Disetujui")'] = null;
        /*$param['or_param'] = [
            'cuti.status_pengajuan' => 'Sudah disetujui!',
            'cuti.status_pengajuan' => 'Sudah Disetujui!',
            'cuti.status_pengajuan' => 'Disetujui'
        ];*/

        $data = $this->m_cuti_api->get_data_cuti($param, 0, 0, 1);

        $jml_cuti_lalu = 0;
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $jml_cuti_lalu = $jml_cuti_lalu+$value['request_cuti'];
            }
        }
        if ($type === 'json'):
            return $this->response($this->current_user()->CUTI);
        else:
            return $jml_cuti_lalu;
        endif;
    }

    public function atasan(){
        $user = $this->current_user();

        // $param=[
        //     'branch.branch_id' => $user->branch_id,
        //     'in_param' => [
        //         'STATUS' => [
        //             'TETAP',
        //             'KONTRAK'
        //         ],
        //     ],
        //     'not_in_param' => [
        //         'users.id_position' => [1,21] /*direktur tdk termasuk*/
        //     ]
        // ];
        // if (strtolower($user->branch_name) == 'all') {
        //     unset($param['branch.branch_id']);
        // }



        // $param['users.id_position'] = 7;/*branch manager*/
        // $data['branch_manager'] = $this->m_cuti_api->get_karyawan($param, 0, 0, 1);
        // unset($param['users.id_position']);
        // $param['id_bagian'] = 22;/*HR*/
        // $data['HR'] = $this->m_cuti_api->get_karyawan($param, 0, 0, 1);
        // unset($param['id_bagian']);
        // $param['users.id_position'] = 9;/*GM*/
        // unset($param['branch.branch_id']);
        // $data['manager'] = $this->m_cuti_api->get_karyawan($param, 0, 0, 1);
        //
        // unset($param);
        // $param = [
        //     'in_param' => [
        //         'users.id_position' => [1,21]
        //     ]
        // ];
        // $data['direktur'] = $this->m_cuti_api->get_karyawan($param, 0, 0, 1);
        if (in_array($user->id_position,[9,19])) 
        { // General Manager, Manager
            $param=[
                'in_param' => [
                    'users.id_position' => [
                        1
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            $data['show_branch_manager'] = false;
            $data['show_manager'] = false;
            $data['show_area_manager'] = false;
            $data['show_general_manager'] = false;
            $data['direktur'] = $this->m_cuti_api->get_karyawan($param, 0, 0, 1);
            $data['show_direktur'] = true;
        } 
        elseif (in_array($user->id_position,[4,17,20])) 
        { // Area Manager,supervisor, ass gm
            $paramGM=[
                'in_param' => [
                    'users.id_position' => [
                        9
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            $data['show_branch_manager'] = false;
            $data['show_manager'] = false;
            $data['show_area_manager'] = false;
            $data['general_manager'] = $this->m_cuti_api->get_karyawan($paramGM, 0, 0, 1);
            $data['show_general_manager'] = true;
            $data['show_direktur'] = false;
        } 
        elseif ($user->id_position == 13) 
        { // penanggung jawab
            $paramArea=[
                'in_param' => [
                    'users.id_position' => [
                        4
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            $data['show_branch_manager'] = false;
            $data['show_manager'] = false;
            $data['area_manager'] = $this->m_cuti_api->get_karyawan($paramArea, 0, 0, 1);
            $data['show_area_manager'] = true;
            $data['show_general_manager'] = false;
            $data['show_direktur'] = false;
        } 
        elseif ($user->id_position == 7) 
        { // Branch Manager
            $paramArea=[
                'in_param' => [
                    'users.id_position' => [
                        4
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            $paramGM=[
                'in_param' => [
                    'users.id_position' => [
                        9
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            $data['show_branch_manager'] = false;
            $data['show_manager'] = false;
            $data['area_manager'] = $this->m_cuti_api->get_karyawan($paramArea, 0, 0, 1);
            $data['show_area_manager'] = true;
            $data['general_manager'] = $this->m_cuti_api->get_karyawan($paramGM, 0, 0, 1);
            $data['show_general_manager'] = true;
            $data['show_direktur'] = false;
        } 
        elseif (in_array($user->id_position,[10,11])) 
        { // officer, staff
            $paramManager=[
                'in_param' => [
                    'users.id_position' => [
                        17,19
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            $paramGM=[
                'in_param' => [
                    'users.id_position' => [
                        9
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            // $param['users.id_user'] = 75;
            $data['show_branch_manager'] = false;
            $manager_list = $this->m_cuti_api->get_karyawan($paramManager, 0, 0, 1);
            $data['manager'] = (count($manager_list)>0?$manager_list:false);
            $data['show_manager'] = (count($manager_list)>0?true:false);
            $data['show_area_manager'] = false;
            $data['general_manager'] = $this->m_cuti_api->get_karyawan($paramGM, 0, 0, 1);
            $data['show_general_manager'] = true;
            $data['show_direktur'] = false;
        } 
        elseif (in_array($user->id_position,[2,3,6,14,16,12,23,22,15])) 
        { 
            // KELOMPOK 1 = ADM , ANALIS , PERAWAT , SECURITY , P. KEBERSIHAN , KURIR , EKSPEDISI , RADIOGRAPHER
            $paramArea=[
                'in_param' => [
                    'users.id_position' => [
                        4
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];

            $paramBranch=[
                'branch.branch_id' => $user->branch_id,
                'in_param' => [
                    'users.id_position' => [
                        7
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            
            
            $data['area_manager'] = $this->m_cuti_api->get_karyawan($paramArea, 0, 0, 1);
            $data['show_area_manager'] = true;
            $data['branch_manager'] = $this->m_cuti_api->get_karyawan($paramBranch, 0, 0, 1);
            $data['show_branch_manager'] = true;
            $data['show_direktur'] = false;
            $data['show_general_manager'] = false;
            $data['show_manager'] = false;

        } 

        else {
            $paramBranch=[
                'branch.branch_id' => $user->branch_id,
                'in_param' => [
                    'users.id_position' => [
                        7
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            $paramArea=[
                'branch.branch_id' => $user->branch_id,
                'in_param' => [
                    'users.id_position' => [
                        4
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            $data['branch_manager'] = $this->m_cuti_api->get_karyawan($paramBranch, 0, 0, 1);
            $data['show_branch_manager'] = true;
            $data['show_manager'] = false;
            $data['area_manager'] = $this->m_cuti_api->get_karyawan($paramArea, 0, 0, 1);
            $data['show_area_manager'] = true;
            $data['show_general_manager'] = false;
            $data['show_direktur'] = false;
        }
        return $this->response($data);
    }


    public function listCuti(){

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $type = $this->input->post('type');

        $search = $this->input->post('search');
        $filter = $this->input->post('filter');


        if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
            $args['limit'] = [ $start, $length ];
        }

        $args = [
            'where' => ['1'=>'1'],
            'like' => ['1'=>'1'],
            // 'order' => [ ['_.id', 'desc'] ]
        ];


        $r = [];
        if($type === 'sent') {
            if (!empty($search)) {
                $args = ['like' => ['usr.name' => $search, 'usr.last_name' => $search, 'branch_name' => $search, 'tgl_request' =>$search]];
            }

            $r = $this->m_cuti_api->getSent($this->current_user, $args);
        } else if($type === 'inbox') {
            if (!empty($search)) {
                $args = ['like' => ['usr.name' => $search, 'usr.last_name' => $search, 'branch_name' => $search, 'tgl_request' =>$search]];
            }

            $r = $this->m_cuti_api->getInbox($this->current_user(), $args);
        } else {
            if (!empty($filter)) {
                $args['where'] = ['status_cuti' => $filter];
            }
            if (!empty($search)) {
                $args['like'] = ['usr.name' => $search, 'usr.last_name' => $search, 'branch_name' => $search, 'tgl_request' =>$search];
            }

            $r = $this->m_cuti_api->getArchived($this->current_user, $args);
        }
        return $this->response($r);
    }

    public function getReceiver(){

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $search = $this->input->post('search');
        $id = $this->input->post('id');

        $args = [
            'where' => [],
            // 'order' => [ ['_.id', 'desc'] ]
        ];

        if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
            $args['limit'] = [ $start, $length ];
        }

        $r = $this->m_cuti_api->getReceiver($id);
        return $this->response($r);
    }

    public function markAsRead(){

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $search = $this->input->post('search');
        $id = $this->input->post('id');

        $args = [
            'where' => [],
            // 'order' => [ ['_.id', 'desc'] ]
        ];

        if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
            $args['limit'] = [ $start, $length ];
        }

        $r = $this->m_cuti_api->updateCutiAsRead($this->current_user, $id);
        return $this->response($r);
    }

    public function approval($id){
        if ($id):
            $approve = $this->m_cuti_api->approveCuti($this->current_user(),$id);
            if ($approve):
                $getCuti = $this->m_cuti_api->getCutiReceiverDetail($this->current_user,$id);
                $user = $this->current_user();
                if ($getCuti):
                    if ($user->id_bagian == 22):
                        $this->m_cuti_api->UpdateStatusCuti($id);

                        $dataCuti = $this->m_cuti_api->get_data_cuti(['id_cuti' => $id],0,0,1);
                        $receiver_ids = [];
                        if(count($dataCuti)>0){
                            $receiver_ids[] = $dataCuti[0]['id_user'];
                        }

                        $data_parse = [
                            "id" => $id,
                            "notif_type" => "reqcuti",
                            "id_cuti" => $id
                        ];

                        $sender = $this->user_model->one_user(['where' => ['_.id_user' => $this->current_user]]);
                        $sender_name = '';
                        $position_name = '';
                        $branch_name = '';
                        if($sender != null) {
                            $sender_name = $sender['name'];
                            $position_name = $sender['position_name'];
                            $branch_name = $sender['branch_name'];
                        }
                        $description = $sender_name.' ('.$position_name.') mensetujui permintaan cuti';

                        $this->notify_lib->send(
                            'Permintaan Cuti Sudah Disetujui', //title
                            $description, //'Permintaan Cuti Sudah Disetujui',        //message
                            $this->current_user,     //sender
                            //$getCuti[0],     //list receiver
                            $receiver_ids,             //list receiver
                            $data_parse,        //attachment
                            "reqcuti",          //type notifikasi
                            true               //kirim ke onesignal
                        );
                    else:

                        $receiver_ids = [];
                        $cek =  $this->m_cuti->getCutiReceiver(['id_cuti'=>$id,'is_acc' => 0,'is_hrd' => 0]);
                        if(count($cek)>0)
                        {
                            $x = 0;
                            foreach($cek as $val){
                                if($x==0){
                                    $receiver_ids[] = $val['id_receiver'];

                                    $paramx['cuti_receiver.id_cuti'] = $id;
                                    $paramx['cuti_receiver.id_receiver'] = $val['id_receiver'];
                                    $this->m_cuti->update_cuti_receiver([
                                        'is_asigned' => 1
                                    ],$paramx);
                                }
                                $x++;
                            }
                        }
                        else
                        {
                            $get_hrd = $this->m_cuti->getCutiReceiver([
                                'cuti_receiver.id_cuti' => $id,
                                'is_hrd' => 1,
                                'id_receiver <>' => $this->current_user
                            ]);

                            foreach($get_hrd as $val){
                                $id_hrd = isset($val['id_receiver'])?$val['id_receiver']:0;
                                $receiver_ids[] = $id_hrd;

                                $paramx['cuti_receiver.id_cuti'] = $id;
                                $paramx['cuti_receiver.id_receiver'] = $id_hrd;
                                $this->m_cuti->update_cuti_receiver([
                                    'is_asigned' => 1
                                ],$paramx);

                            }
                        }

                        $data_parse = [
                            "id" => $id,
                            "notif_type" => "reqcuti",
                            "id_cuti" => $id
                        ];

                        $dataCutix = $this->m_cuti_api->get_data_cuti(['id_cuti' => $id],0,0,1);
                        $req_sender_name = '';
                        $req_position_name = '';
                        $req_branch_name = '';
                        if(count($dataCutix)>0){
                            $req_sender = $this->user_model->one_user(['where' => ['_.id_user' => $dataCutix[0]['id_user']]]);
                            if($req_sender != null) {
                                $req_sender_name = $req_sender['name'];
                                $req_position_name = $req_sender['position_name'];
                                $req_branch_name = $req_sender['branch_name'];
                            }
                        }
                        $description = $req_sender_name. ' ('.$req_position_name.') Mengajukan Cuti';

                        $this->notify_lib->send(
                            'Permintaan Cuti', //title
                            $description, //'Permintaan Cuti',        //message
                            $getCuti[0],     //sender
                            $receiver_ids,             //list receiver
                            //$getCuti[1],             //list receiver
                            $data_parse,        //attachment
                            "reqcuti",          //type notifikasi
                            true               //kirim ke onesignal
                        );
                    endif;
                endif;
                return $this->response($getCuti);
            endif;
        else:
            return $this->response(false);
        endif;
    }

    public function rejected($id){
        if ($id){
            $reject = $this->m_cuti_api->rejectCuti($this->current_user(),$id, $this->input->post('alasan'));

            $user = $this->current_user();
            $sender = $this->user_model->one_user(['where' => ['_.id_user' => $user->id_user]]);
            $sender_name = '';
            $position_name = '';
            $branch_name = '';
            if($sender != null) {
                $sender_name = $sender['name'];
                $position_name = $sender['position_name'];
                $branch_name = $sender['branch_name'];
            }
            $receiver_ids = [];
            $dataCuti = $this->m_cuti_api->get_data_cuti(['id_cuti' => $id],0,0,1);
            $receiver_ids[] = (isset($dataCuti[0]['id_user'])?$dataCuti[0]['id_user']:'');
            $description = $sender_name.' ('.$position_name.') menolak permintaan cuti';

            $data_parse = [
                "id" => $id,
                "notif_type" => "reqcuti",
                "id_cuti" => $id
            ];

            $this->notify_lib->send(
                "Permintaan Cuti", //title
                $description,        //message
                $user->id_user,     //sender
                $receiver_ids,             //list receiver
                $data_parse,        //attachment
                "reqcuti",          //type notifikasi
                true               //kirim ke onesignal
            );

            return $this->response($reject);
        }else{
            return $this->response(false);
        }
    }

    public function canceled($id){
        if ($id){
            $cancel = $this->m_cuti_api->cancelCuti($this->current_user(),$id, $this->input->post('alasan'));

            $user = $this->current_user();
            $sender = $this->user_model->one_user(['where' => ['_.id_user' => $user->id_user]]);
            $sender_name = '';
            $position_name = '';
            $branch_name = '';
            if($sender != null) {
                $sender_name = $sender['name'];
                $position_name = $sender['position_name'];
                $branch_name = $sender['branch_name'];
            }
            $receiver_ids = [];
            $dataCuti = $this->m_cuti_api->get_data_cuti(['id_cuti' => $id],0,0,1);
            $receiver_ids[] = (isset($dataCuti[0]['id_user'])?$dataCuti[0]['id_user']:'');
            $description = $sender_name.' ('.$position_name.') membatalkan permintaan cuti';

            $data_parse = [
                "id" => $id,
                "notif_type" => "reqcuti",
                "id_cuti" => $id
            ];

            $this->notify_lib->send(
                "Permintaan Cuti", //title
                $description,        //message
                $user->id_user,     //sender
                $receiver_ids,             //list receiver
                $data_parse,        //attachment
                "reqcuti",          //type notifikasi
                true               //kirim ke onesignal
            );

            return $this->response($cancel);
        }else{
            return $this->response(false);
        }
    }

    public function current_user()
    {
        $query = $this->db->query("Select a.*,b.branch_name,c.name_position,a.CUTI as total_jatah_cuti
            from users a
            join branch b on (a.branch_id = b.branch_id)
            join position c on (a.id_position = c.id_position)
            where id_user='".$this->current_user."'
        ");
        return $query->first_row();
    }

    public function save(){
        $input = $this->input->post();
        $user = $this->current_user();
        $id_cuti = 0;
        $data = $this->m_cuti_api->get_last_id();
        $docNum = isset($data[0]['id_cuti'])?($data[0]['id_cuti']):0;
        $new_number = $docNum+1;
        $cuti_data = [
          'creator_id' => $this->current_user,
          'branch_id' => $this->current_user()->branch_id,
          'id_user' =>  $this->current_user,
          'tgl_request' => date('Y-m-d'),
          'tgl_ambil_cuti' => $input['tgl_ambil_cuti'],
          'jml_hari' => $input['jml_hari'],
          'status_at_cuti' => $this->current_user()->STATUS,
          'no_dokumen' => $new_number,
          'no_telp' => $input['telp'],
          'jatah_cuti' => $this->current_user()->total_jatah_cuti,
          'jatah_diambil' => $this->getLastCuti($this->current_user),
          'sisa_jatah' => $input['jenis_cuti'] === 'melahirkan' ? $this->current_user()->total_jatah_cuti : $this->current_user()->total_jatah_cuti - $this->getLastCuti($this->current_user), // sisa jatah skrg
          'request_cuti' => $input['jenis_cuti'] === 'melahirkan'? 0 : $input['jml_hari'],
          'sisa_cuti' => $input['jenis_cuti'] === 'melahirkan' ? $this->getLastCuti($this->current_user) : $this->current_user()->total_jatah_cuti - $this->getLastCuti($this->current_user) - $input['jml_hari'], // sisa jatah skrg setelah dikurangi request
          'status_cuti' => $input['jenis_cuti']
        ];

        $id_cuti = $this->m_cuti_api->save_cuti($cuti_data);
        $receiverNotif = [];
        $receivers = [];
        $receiverHr = [];
        if (isset($input['branch_manager_id']) && !empty($input['branch_manager_id'])):
            if (count($receiverNotif) == 0):
                array_push($receiverNotif, $input['branch_manager_id']);
            endif;
            array_push($receivers,$input['branch_manager_id']);
        endif;
        if (isset($input['manager_id']) && !empty($input['manager_id'])){
            if (count($receiverNotif) == 0):
                array_push($receiverNotif,$input['manager_id']);
            endif;
            array_push($receivers,$input['manager_id']);
        }
        if (isset($input['area_manager_id']) && !empty($input['area_manager_id'])){
            if (count($receiverNotif) == 0):
                array_push($receiverNotif,$input['area_manager_id']);
            endif;
            array_push($receivers,$input['area_manager_id']);
        }
        if (isset($input['general_manager_id']) && !empty($input['general_manager_id'])){
            if (count($receiverNotif) == 0):
                array_push($receiverNotif,$input['general_manager_id']);
            endif;
            array_push($receivers,$input['general_manager_id']);
        }
        if (isset($input['direktur_id']) && !empty($input['direktur_id'])){
            if (count($receiverNotif) == 0):
                array_push($receiverNotif,$input['direktur_id']);
            endif;
            array_push($receivers,$input['direktur_id']);
        }
        $paramHR=[
            'in_param' => [
                'users.id_bagian' => [
                    22
                ],
                'STATUS' => [
                    'TETAP',
                    'KONTRAK'
                ],
            ]
        ];
        $dataHr = $this->m_cuti_api->get_karyawan($paramHR, 0, 0, 1);
        if ($dataHr):
            foreach($dataHr as $hr):
                array_push($receivers,$hr['id_user']);
                array_push($receiverHr,$hr['id_user']);
            endforeach;
        endif;
        // if ($user->name_position == "OPT") {
        //     /*OPT*/
        //     $receiver[0] = [
        //         'id_receiver' => $input['branch_manager_id'],
        //         'id_cuti' => $id_cuti
        //     ];
        //     $receiver[1] = [
        //         'id_receiver' => $input['manager_id'],
        //         'id_cuti' => $id_cuti
        //     ];
        //     $receiver[2] = [
        //         'id_receiver' => 68,
        //         'id_cuti' => $id_cuti,
        //         'is_hrd' => 1
        //     ];
        // } elseif ($user->name_position == "GENERAL MANAGER") {
        //     //GENERAL MANAGER
        //     $receiver[0] = [
        //         'id_receiver' => $input['direktur_id'],
        //         'id_cuti' => $id_cuti
        //     ];
        //     $receiver[1] = [
        //         'id_receiver' => 68,
        //         'id_cuti' => $id_cuti,
        //         'is_hrd' => 1
        //     ];
        // } else {
        //     $receiver[0] = [
        //         'id_receiver' => $input['manager_id'],
        //         'id_cuti' => $id_cuti
        //     ];
        //     $receiver[2] = [
        //         'id_receiver' => 68,
        //         'id_cuti' => $id_cuti,
        //         'is_hrd' => 1
        //     ];
        // }
        $x = 0;
        foreach ($receivers as $value) {
            $data_receiver['id_receiver'] = $value;
            $data_receiver['id_cuti'] = $id_cuti;
            if (in_array($value,$receiverHr)):
                $data_receiver['is_hrd'] = 1;
            endif;

            if($x == 0){
                $data_receiver['is_asigned'] = 1;
            }

            $this->m_cuti_api->save_cuti_receiver($data_receiver);
            // $receiver_ids[] = (isset($value['is_hrd'])&&$value['is_hrd']==1)?$data_receiver['id_receiver']:$data_receiver['id_receiver'];

            $x++;
        }
        $data_parse = [
            "id" => $id_cuti,
            "notif_type" => "reqcuti",
            "id_cuti" => $id_cuti
        ];

        $user = $this->m_cuti->get_karyawan(['id_user' => $this->current_user],0,0,1);
        $nama_user = $user[0]['name'];
        $position_name = $user[0]['name_position'];

        $description = $nama_user. ' ('.$position_name.') Mengajukan Cuti';

        $this->load->library("Notify_lib");
        $nl = new Notify_lib();
        $nl->send(
         'Permintaan Cuti Baru',
            $description, //'Permintaan Cuti Baru',
            //'Request Cuti', //title
            //'Request Cuti',        //message
            $this->current_user,     //sender
            $receiverNotif,             //list receiver
            $data_parse,        //attachment
            "reqcuti",          //type notifikasi
            true               //kirim ke onesignal                                            //kirim ke onesignal
        );

      

        $this->response([
          'msg' => "sukses",
          // 'data'=> $cuti_data
          'data'=> $receiverNotif
        ]);
    }

    public function detail(){
        $id = $this->input->post('id');
        $detailCuti = $this->m_cuti_api->detail($id);

        $karyawan =  $this->db->query("select TGLMASUK as tglmasuk
            from users a
            where id_user='$detailCuti->id_user'
        ")->first_row();

        $detailCuti->tglmasuk = $karyawan->tglmasuk;

        return $this->response($detailCuti);
    }

    public function response($data, $code = 200){
        $json =   json_encode($data);

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header($code)
            ->set_output($json);
    }

    public function unittest($id_user=0, $type = 'view'){
        /*$args = [
            'where' => [],
            // 'order' => [ ['_.id', 'desc'] ]
        ];
        $this->current_user = 47;
        print_r($this->m_cuti_api->getInbox($this->current_user(), $args));*/

        /*
        $param['year(tgl_request)'] = date('Y');
        $param['users.id_user'] = 47;
        $param['(cuti.status_pengajuan = "Sudah disetujui!" or cuti.status_pengajuan = "Sudah Disetujui!" or cuti.status_pengajuan = "Disetujui")'] = null;

        $data = $this->m_cuti_api->get_data_cuti($param, 0, 0, 1);

        $jml_cuti_lalu = 0;
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $jml_cuti_lalu = $jml_cuti_lalu+$value['request_cuti'];
            }
        }
        if ($type === 'json'):
            echo $this->response($this->current_user()->CUTI);
        else:
            echo $jml_cuti_lalu;
        endif;
        $this->current_user = 47;
        $detailCuti = $this->m_cuti_api->detail(43, $this->current_user);

        $karyawan =  $this->db->query("select CUTI as jatah_cuti, TGLMASUK as tglmasuk
            from users a
            where id_user='$this->current_user'
        ")->first_row();

        $cuti_diambil = $this->getLastCuti($this->current_user);
        $detailCuti->jatah_diambil = $cuti_diambil;
        $detailCuti->jatah_cuti = $karyawan->jatah_cuti+$cuti_diambil;
        $detailCuti->sisa_jatah = $karyawan->jatah_cuti;

        print_r($detailCuti);
        */


        $search = 'dini';
        $filter = 'tahunan';
        $this->current_user = 47;
        $type = 'inbox';

        $args = [
            'where' => ['1'=>'1'],
            'like' => ['1'=>'1'],
            // 'order' => [ ['_.id', 'desc'] ]
        ];


        $r = [];
        if($type === 'sent') {
            if (!empty($search)) {
                $args = ['like' => ['usr.name' => $search, 'usr.last_name' => $search, 'branch_name' => $search, 'tgl_request' =>$search]];
            }

            $r = $this->m_cuti_api->getSent($this->current_user, $args);
        } else if($type === 'inbox') {
            if (!empty($search)) {
                $args = ['like' => ['usr.name' => $search, 'usr.last_name' => $search, 'branch_name' => $search, 'tgl_request' =>$search]];
            }

            $r = $this->m_cuti_api->getInbox($this->current_user(), $args);
        } else {
            if (!empty($filter)) {
                $args['where'] = ['status_cuti' => $filter];
            }
            if (!empty($search)) {
                $args['like'] = ['usr.name' => $search, 'usr.last_name' => $search, 'branch_name' => $search, 'tgl_request' =>$search];
            }

            $r = $this->m_cuti_api->getArchived($this->current_user, $args);
        }
        print_r($r);

    }

}


  $this->notify_lib->send(
            'Permintaan Cuti Baru',
			$description, //'Permintaan Cuti Baru',
			//'Request Cuti', //title
            //'Request Cuti',        //message
            $this->current_user,     //sender
            $receiverNotif,             //list receiver
            $data_parse,        //attachment
            "reqcuti",          //type notifikasi
            true               //kirim ke onesignal
        );