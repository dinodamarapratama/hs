<?php

class Overtime extends CI_Controller
{

	public function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json');
        $this->restapikey= $this->config->config['restapikey'];
        if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        $this->user_id = $this->input->post('user_id');
        $this->load->model('restapi/user_model');
            $this->me = $this->user_model->one_user(["where" => ["_.id_user" => $this->user_id]]);

            $this->branch_id = $this->me['branch_id'];
            $this->first_name = $this->me['name'];
            $this->call_name = $this->me['call_name'];
            $this->last_name = $this->me['last_name'];
            $this->position_name = $this->me['position_name'];
            $this->branch_name = $this->me['branch_name'];
            $this->id_bagian = $this->me['id_bagian'];

            $this->model = 'maintain_ext';
        if($this->me == null) {
          echo json_encode(['status' => false, 'message' => 'Invalid user']);
          exit();
        }
        
        $this->load->model(['m_overtime','m_cuti_api']);
        $this->load->model('restapi/Lembur_model', 'lembur');
        $this->load->library(['upload','curl','misc']);

    }

    public function checkAuthorized(){
      $respon['code'] = 200;
      $respon['authorized_gm'] = 0;
      if ( $this->me['id_bagian'] == 22) {
        /*hrd*/
        $respon['authorized_gm'] = 2;
      }

      if ($this->me['id_position'] == 9) {
        /*GM*/
        $respon['authorized_gm'] = 1;
      }

      if ($this->me['id_position'] == 4) {
        /*area Manager*/
        $respon['authorized_gm'] = 2; 
      }

      if ($this->me['id_position'] == 7) {
        /*branch Manager*/
        $respon['authorized_gm'] = 2;
      }

      if ($this->me['id_position'] == 17 && $this->id_bagian == 19) {
        /*IT  Supervisor*/
        $respon['authorized_gm'] = 2;
      }
      if ($this->me['id_position'] == 19 && $this->id_bagian == 34) {
        /*MCU Manager*/
        $respon['authorized_gm'] = 2;
      }

      return $this->response($respon, $respon['code']);
    }


    // public function checkAuthorized2(){
    //   print "<h2>PHP is Fun!</h2>";
    //   print "Hello world!<br>";
    //   print "I'm about to learn PHP!";
    // }


    public function listOvertime(){
      $type = $this->input->post('type'); 
      $page = $this->input->post('page') ? $this->input->post('page') : 1;
      $perPage = $this->input->post('perpage') ? $this->input->post('perpage') : 6;
      $offset = ($page - 1) * $perPage;

      switch ($type) {
        case 'sent':
          $respon['code'] = 200;
          $respon['message'] = "success get data";
          $respon['data'] = $this->lembur->getOvertime($this->user_id, $perPage, $offset, $type)->result_array();
          break;
        
        default:
          $respon['code'] = 200;
          $respon['message'] = "success get data";
          $respon['data'] = $this->lembur->getOvertime($this->user_id, $perPage, $offset)->result_array();
          break;
      }


      // ini khusus inbox
      if($type == 'inbox'){
        $data = [];
        foreach ($respon['data'] as $key_sp => $data_sp) {
          // just inbox 
          if($data_sp['id_user'] == $this->user_id){
            $data_sp['just_read'] = 1;
            array_push($data, $data_sp);
          }else{
            // inbox and acc akses
            $mentioners= json_decode($data_sp['mentioner'], 1);
            $ids = [];
            foreach ($mentioners as $k => $v) {
              if (!is_array($v)) {
                $ids[] = $v;
              }else{
                foreach ($v as $x => $id) {
                  if (!in_array($id,$ids)) {
                    $ids[] = $id;  
                  }
                }
              }      
            }
            foreach ($ids as $id_mention) {
              if ($this->user_id == $id_mention) {
                $data_sp['just_read'] = 0;
                array_push($data, $data_sp);
              }
            }
          }
        }
  
        $respon['data'] = $data;
        if ($data) {
          return $this->response($respon, $respon['code']);
        }
      }
      return $this->response($respon, $respon['code']);
      

    }

    public function getDepartment($ajax=1){
  		$param=[
			];
			$config = [
				'fields' => [], /*leave empty if select all field*/ 
			  'relation' => [],
				'master' => 'departments'
			];
			
			$config = array_merge($config,$param);		

			$hrd = $this->m_overtime->get_related_data($config)->result();
			if (!empty($hrd)) {
					$result=[
						'code' => 200,
						'data' => json_decode(json_encode($hrd),1)
					];
			}else{
				$result=[
						'code' => 404,
						'data' => []
					];
			}

			if ($ajax==0) {
				return $hrd;
			}else{
				echo json_encode($result);	
			}
  	}

    public function getKaryawan($ajax=1,$filter=[]){
  		$parseFilter = $this->input->post();
      if (empty($filter['where'])) {
        $where = [
            'u.id_department' => !empty($parseFilter['id_department'])?$parseFilter['id_department']:'' 
          ];
      }else{
        $where = $filter['where'];
      }

      if (empty($filter['in_where'])) {
        $in_where = [
          'STATUS' => [
            'TETAP',
            'KONTRAK'
          ],
        ];
      }else{
        $in_where = $filter['in_where'];
      }

  		$param=[
				'in_where' => $in_where,
				'where' => $where
			];
			if (empty($parseFilter['id_department'])) {
				unset($param['where']);
			}
      if (!empty($parseFilter['id_user'])) {
        $param['where']['u.id_user'] = $parseFilter['id_user'];
      }

			$config = [
				'fields' => ['u.id_user','u.name','u.last_name','b.branch_name'], /*leave empty if select all field*/ 
			  'relation' => [
			        'bagian' => [
			          'table' => 'bagian bg',
			          'key' => 'u.id_bagian=bg.id',
			          'type' => 'left'
			        ],
			        'branch' => [
			          'table' => 'branch b',
			          'key' => 'b.branch_id=u.branch_id',
			          'type' => 'left'
			        ]
			      ],
				'master' => 'users u'
			];
			
			$config = array_merge($config,$param);		

			$kar = $this->m_overtime->get_related_data($config)->result();
			if (!empty($kar)) {
					
					$result=[
						'code' => 200,
						'data' => json_decode(json_encode($kar),1)
					];
				
			}else{
				$result=[
						'code' => 404,
						'data' => []
					];
			}

			if ($ajax==0) {
				return $kar;
			}else{
				echo json_encode($result);	
			}
  	}

    public function getHrd($ajax=1){
	  	$param=[
				'in_where' => [
					'STATUS' => [
						'TETAP',
						'KONTRAK'
					],
				],
				'where' => [
					'id_bagian' => 22 /*HR*/
				]
			];
			$config = [
				'fields' => ['u.id_user','u.name','u.last_name','b.branch_name'], /*leave empty if select all field*/ 
			  'relation' => [
			        'bagian' => [
			          'table' => 'bagian bg',
			          'key' => 'u.id_bagian=bg.id',
			          'type' => 'left'
			        ],
			        'branch' => [
			          'table' => 'branch b',
			          'key' => 'b.branch_id=u.branch_id',
			          'type' => 'left'
			        ]
			      ],
				'master' => 'users u'
			];
			
			$config = array_merge($config,$param);		

			$hrd = $this->m_overtime->get_related_data($config)->result();
			if (!empty($hrd)) {
					
					$result=[
						'code' => 200,
						'data' => json_decode(json_encode($hrd),1)
					];
				
			}else{
				$result=[
						'code' => 404,
						'data' => []
					];
			}

			if ($ajax==0) {
				return json_decode(json_encode($hrd),1);
			}else{
				echo json_encode($result);	
			}
  	}

    function setNotifReceiver(){
      /*cek if load from notification*/
      $notif_id = $this->input->post('notif');
      $data['notif_id'] = $data['set_notif_id'] = 0;
      $data['hrd']=0;
      $data['am']=0;
      $data['acc']=0;
      if (!empty($notif_id)) {
        // explode notif_id & id_set_notif
        $ids_notif = explode('_',$notif_id);
        $data['notif_id'] = $ids_notif[0];
        $data['set_notif_id'] = $ids_notif[1];
        
        /*update notification has read by user*/
        $update = ['read' => 1];
        $where = ['id' => $data['set_notif_id']];
        
        /**
          1. jika punya acc akses : flag acc harus 1 baru hilang notif
          2. jika punya hrd akses : flag tidak harus 1 namun hrd lain sudah set flag
          3. jika punta am akses :  flag tidak harus 1 namun am lain sudah set flag
        */
        
        $cek = $this->m_overtime->get_sp_receiver(['id_sp' => $data['notif_id'],'id_receiver' => $this->user_id]);
        if (!empty($cek)) {
          # not update notif until acc was set
          $as_acc = $cek[0]->as_acc;
          $set_acc = $cek[0]->acc_check;
          /* jika punyak hak akses acc (atasan & ka dept)*/
          if ($as_acc) {
            $data['acc'] = 1;
            if ($set_acc) {
              $this->m_overtime->update_data($where,$update,'notifications');  
            }
          }

          $as_hrd = $cek[0]->as_hrd;
          if ($as_hrd) {
            /*check status acc hrd lain dulu*/
            $cekHrd = $this->m_overtime->get_sp_receiver(['id_sp' => $data['notif_id'],'as_hrd' => 1, 'hrd_check'=>1]);
            
            if (!empty($cekHrd)) {
              $this->m_overtime->update_data($where,$update,'notifications');
            }

            $data['hrd'] = 1;
          }

          $as_am = $cek[0]->as_am;
          if ($as_am) {
            /*check status acc am lain*/
            $cekAm = $this->m_overtime->get_sp_receiver(['id_sp' => $data['notif_id'],'as_am' => 1, 'am_check'=>1]);
            if (!empty($cekAm)) {
              $this->m_overtime->update_notification($update,$where);
            }
            $data['am'] = 1;
          }
        }
        /*
          update set view receiver status
        */
        $this->m_overtime->update_data(
          [
            'id_sp' => $data['notif_id'],
            'id_receiver'=>$this->user_id
          ],
          [
            'is_view' => 1
          ],
          'sp_receiver');
      }

      return $data;
    }


    function setIsView(){
      $user_id = $this->input->post('user_id');
      $id_sp = $this->input->post('id_sp');
        /*
          update set view receiver status
        */
        $r = $this->m_overtime->update_data(
          [
            'id_sp' => $id_sp,
            'id_receiver'=> $user_id
          ],
          [
            'is_view' => 1
          ],
          'sp_receiver');
      return $r;
    }

    public function getDetail(){
      $id_Sp= $this->input->post('id_sp'); 
      $respon['code'] = 200;
      $respon['message'] = "success get data";
      $result = $this->lembur->getOvertime($this->user_id, $perPage = null, $offset = null, $type = null, $id_Sp)->row_array();
      
      if($result['id_user'] == $this->user_id){
        $result['just_read'] = 1;
      }else{
        // inbox and acc akses
        $mentioners= json_decode($result['mentioner'], 1);
        $ids = [];
        foreach ($mentioners as $k => $v) {
          if (!is_array($v)) {
            $ids[] = $v;
          }else{
            foreach ($v as $x => $id) {
              if (!in_array($id,$ids)) {
                $ids[] = $id;  
              }
            }
          }      
        }
        foreach ($ids as $id_mention) {
          if ($this->user_id == $id_mention) {
            $result['just_read'] = 0;
          }
        }
      }

      $respon['data'] = $result;

      $respon['data']['durasi'] =  intval($this->misc->selisih_waktu($respon['data']['jam_mulai'],$respon['data']['jam_selesai'])['jam']);  

      return $this->response($respon, $respon['code']);     

    }

    public function getAtasan($id_user=0,$ajax=1){
    	/*
			 - copied fuction of cuti/atasan (restapi)
    	 - add list manager department/ka department
       - ka department set as GM
      */
      $data = [];
      $param = [];
      $userKaryawan = ($id_user>0)?$id_user:$this->input->post('user');
      $user = $this->currentUser($userKaryawan);
      
      if ($userKaryawan == 34) 
			{ // penanggung jawab
            $paramArea=[
                'in_param' => [
                    'users.id_position' => [
                        4
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ],
                'users.id_department' => $user->id_department
            ];
            $paramGM=[
                'in_param' => [
                    'users.id_position' => [
                        9
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ],
                'users.id_department' => $user->id_department
            ];
            
            
            $data['area_manager'] = $this->m_cuti_api->get_karyawan($paramArea, 0, 0, 1);
            // $data['general_manager'] = $this->m_cuti_api->get_karyawan($paramGM, 0, 0, 1);
            // $data['level_atasan_langsung'] = 'area_manager';
		
      } 
			elseif (in_array($user->id_position,[9,19])) 
			{ // General Manager, Manager
            $param=[
                'in_param' => [
                    'users.id_position' => [
                        1
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ],
                'users.id_department' => $user->id_department
            ];
            
            $data['direktur'] = $this->m_cuti_api->get_karyawan($param, 0, 0, 1);
            // $data['level_atasan_langsung'] = 'direktur';
      } 
			elseif (in_array($user->id_position,[4,17,20,13])) 
			{ // Area Manager,supervisor, ass gm, penanggung jawab
            $paramGM=[
                'in_param' => [
                    'users.id_position' => [
                        9
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ],
                'users.id_department' => $user->id_department
            ];
            
            // $data['general_manager'] = $this->m_cuti_api->get_karyawan($paramGM, 0, 0, 1);
            // $data['level_atasan_langsung'] = 'general_manager';
      } 
			elseif ($user->id_position == 7) 
			{ // Branch Manager
            $paramArea=[
                'in_param' => [
                    'users.id_position' => [
                        4
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ],
                'users.id_department' => $user->id_department
            ];
            $paramGM=[
                'in_param' => [
                    'users.id_position' => [
                        9
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ],
                'users.id_department' => $user->id_department
            ];

            $data['area_manager'] = $this->m_cuti_api->get_karyawan($paramArea, 0, 0, 1);
            // $data['general_manager'] = $this->m_cuti_api->get_karyawan($paramGM, 0, 0, 1);
            // $data['level_atasan_langsung'] = 'area_manager';
      } 
			elseif (in_array($user->id_bagian,[20,21,22,25,29,31,32,1,2,3,28]) || in_array($user->id_position,[11])  || (in_array($user->id_position,[10]) && in_array($user->id_bagian,[17,30]))) 
		        { 
						// officer, staff tanpa manager dept
            $paramManager=[
                'in_param' => [
                    'users.id_position' => [
                        17,19
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ],
                'users.id_department' => $user->id_department
            ];
            $paramGM=[
                'in_param' => [
                    'users.id_position' => [
                        9
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ],
                'users.id_department' => $user->id_department
            ];
            
            // $data['general_manager'] = $this->m_cuti_api->get_karyawan($paramGM, 0, 0, 1);
            // $data['level_atasan_langsung'] = 'general_manager';
      } 
      elseif (in_array($userKaryawan,[47,79,78,73,828])) 
      { // officer, staff dengan manager dept
            $paramManager=[
                'in_param' => [
                    'users.id_position' => [
                        17,19
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ],
                'users.id_department' => $user->id_department
            ];
            // $paramGM=[
            //     'in_param' => [
            //         'users.id_position' => [
            //             9
            //         ],
            //         'STATUS' => [
            //             'TETAP',
            //             'KONTRAK'
            //         ],
            //     ],
            //     'users.id_department' => $user->id_department
            // ];
            
            $manager_list = $this->m_cuti_api->get_karyawan($paramManager, 0, 0, 1);
            $data['manager'] = (count($manager_list)>0?$manager_list:false);
            // $data['general_manager'] = $this->m_cuti_api->get_karyawan($paramGM, 0, 0, 1);
            $data['level_atasan_langsung'] = 'manager';
      } 
      elseif (in_array($user->id_bagian,[34])) 
      { 
            // OPT / MCU
            $paramArea=[
                'in_param' => [
                    'users.id_position' => [
                        4
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ],
                'users.id_department' => $user->id_department
            ];

            $paramBranch=[
                //'branch.branch_id' => $user->branch_id,
                'in_param' => [
                    'users.id_position' => [
                        7,19
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ],
                'users.id_department' => $user->id_department
            ];

          
            
            
            $data['area_manager'] = $this->m_cuti_api->get_karyawan($paramArea, 0, 0, 1);
            $data['branch_manager'] = $this->m_cuti_api->get_karyawan($paramBranch, 0, 0, 1);
            // $data['level_atasan_langsung'] = 'manager';

      } 
			elseif (in_array($user->id_position,[2,3,6,14,16,12,23,22,15])) 
      { 
            // KELOMPOK 1 = ADM , ANALIS , PERAWAT , SECURITY , P. KEBERSIHAN , KURIR , EKSPEDISI , RADIOGRAPHER
            $paramArea=[
                'in_param' => [
                    'users.id_position' => [
                        4
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ],
                'users.id_department' => $user->id_department
            ];

            $paramBranch=[
                'branch.branch_id' => $user->branch_id,
                'users.id_department' => $user->id_department,
                'in_param' => [
                    'users.id_position' => [
                        7,19
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];

          
            $data['area_manager'] = $this->m_cuti_api->get_karyawan($paramArea, 0, 0, 1);
            $data['branch_manager'] = $this->m_cuti_api->get_karyawan($paramBranch, 0, 0, 1);
            // $data['level_atasan_langsung'] = 'branch_manager';

      } 

			else {
            $paramBranch=[
                'branch.branch_id' => $user->branch_id,
                'in_param' => [
                    'users.id_position' => [
                        7
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                    'users.id_department' => $user->id_department
                ]
            ];
            $paramArea=[
                'branch.branch_id' => $user->branch_id,
                'in_param' => [
                    'users.id_position' => [
                        4
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ],
                'users.id_department' => $user->id_department
            ];
            
            $data['branch_manager'] = $this->m_cuti_api->get_karyawan($paramBranch, 0, 0, 1);
            $data['area_manager'] = $this->m_cuti_api->get_karyawan($paramArea, 0, 0, 1);
            
      }
   
      /*every department must be have GM*/
      $paramGM=[
          'in_param' => [
              'users.id_position' => [
                  9
              ],
              'STATUS' => [
                  'TETAP',
                  'KONTRAK'
              ],
          ],
          'users.id_department' => $user->id_department
      ];
      $data['general_manager'] = $this->m_cuti_api->get_karyawan($paramGM, 0, 0, 1);
      
      if ($ajax) {
        $respon['code'] = 200;
        $respon['data'] = $data;
        echo json_encode($respon);    
      }else{
        return $data;
      }
      
    }
    

  	public function currentUser($id_user)
    {
        $query = $this->m_overtime->get_current_user($id_user);
        return $query;
    }

    function actionOvertime($action){
      /*
        ** update status aksi overtime
        1:view/acc by hrd|
        2:dec by hrd| 
        3:acc by ka dept|
        4:decl by ka dept
        5:acc by bm/atasan
        6:decl by bm/atasan
        7:acc by am
        8:dec by am
        9:cancel by creator
        10: acc atasan
      */
      $parseData = $this->input->post();
      $where = [
        'id_sp' => $parseData['id_sp']
      ];
      $getStatus = $this->m_overtime->get_where($where,'sp_overtime')->result();
      $ket_status = $getStatus[0]->ket_status;
      if ($action==9) {
        $pia = $this->user_id; /*creator*/
      }
      $pia = $this->currentUser($parseData['pia']);
      $pia_name = $pia->name;
      $reason = $parseData['reason'];

      $cost = $parseData['cost'];
      $norek_transfer = $parseData['norek_transfer'];
      $total_cost = $parseData['total_cost'];
      $status_transfer = $parseData['status_transfer'];
      $id_user = $parseData['id_user'];
      switch ($action) {
        case 1:
          #HRD view/acc
          $label = '<span class="badge bg-success" style="color:#fff;">Mengetahui HRD ('.$pia_name.')</span><br>';
          $ket_status = str_replace($label,'',$ket_status);
          $data = [
            'status' => 1,
            'ket_status' => $ket_status.$label,
            'cost' => $cost,
            'norek_transfer' => $norek_transfer,
            'total_cost' => $total_cost,
            'status_transfer' => $status_transfer
          ];
          $dd = [
            'hrd_check' => 1,
            'is_act' => 1
          ];
          break;
        case 2:
          $label = '<span class="badge bg-danger" style="color:#fff;">Ditolak HRD ('.$pia_name.')</span><br>';
          $ket_status = str_replace($label,'',$ket_status);
          $data = [
            'status' => 2,
            'ket_status' => $ket_status.$label
          ];
          $dd = [
            'is_act' => 1
          ];
          break;
        case 3:
          $label = '<span class="badge bg-success" style="color:#fff;">Disetujui Kepala Department ('.$pia_name.')</span><br>';
          $ket_status = str_replace($label,'',$ket_status);
          $data = [
            'status' => 3,
            'ket_status' => $ket_status.$label
          ];
          $dd = [
            'acc_check' => 1,
            'is_act' => 1
          ];
          break;
        case 4:
          $label = '<span class="badge bg-danger" style="color:#fff;">Ditolak Kepala Department ('.$pia_name.')</span><br>';
          $ket_status = str_replace($label,'',$ket_status);
          $data = [
            'status' => 4,
            'ket_status' => $ket_status.$label
          ];
          $dd = [
            'is_act' => 1
          ];
          break;
        case 5:
          $label = '<span class="badge bg-success" style="color:#fff;">Disetujui Branch Manager ('.$pia_name.')</span><br>';
          $ket_status = str_replace($label,'',$ket_status);
          $data = [
            'status' => 5,
            'ket_status' => $ket_status.$label
          ];
          $dd = [
            'acc_check' => 1,
            'is_act' => 1
          ];
          break;
        case 6:
          $label = '<span class="badge bg-danger" style="color:#fff;">Ditolak Branch Manager ('.$pia_name.')</span><br>';
          $ket_status = str_replace($label,'',$ket_status);
          $data = [
            'status' => 6,
            'ket_status' => $ket_status.$label
          ];
          $dd = [
            'is_act' => 1
          ];
          break;
        case 7:
          $label = '<span class="badge bg-success" style="color:#fff;">Disetujui Area Manager ('.$pia_name.')</span><br>';
          $ket_status = str_replace($label,'',$ket_status);
          $data = [
            'status' => 7,
            'ket_status' => $ket_status.$label
          ];
          $dd = [
            'am_check' => 1,
            'is_act' => 1
          ];
          break;
        case 8:
          $label = '<span class="badge bg-danger" style="color:#fff;">Ditolak Area Manager ('.$pia_name.')</span><br>';
          $ket_status = str_replace($label,'',$ket_status);
          $data = [
            'status' => 8,
            'ket_status' => $ket_status.$label
          ];
          $dd = [
            'is_act' => 1
          ];
          break;
        case 9:
          $label = '<span class="badge bg-danger" style="color:#fff;">Dibatalkan ('.$pia_name.')</span><br>';
          $ket_status = str_replace($label,'',$ket_status);
          $data = [
            'status' => 9,
            'ket_status' => $ket_status.$label
          ];
          $dd = [
            'is_act' => 1
          ];
          break;
        case 10:
          #Atasan/acc (auto)
          $label = '<span class="badge bg-info" style="color:#fff;">Disetujui Atasan ('.$pia_name.')</span><br>';
          $ket_status = str_replace($label,'',$ket_status);
          $data = [
            'status' => 10,
            'ket_status' => $ket_status.$label
          ];
          $dd = [
            'is_act' => 1
          ];
          break;
          case 11:
            #Edi Laksana => general manager - supporting group
            $label = '<span class="badge bg-success" style="color:#fff;">Disetujui General Manager ('.$pia_name.')</span><br>';
            $ket_status = str_replace($label,'',$ket_status);
            $data = [
              'status' => 11,
              'ket_status' => $ket_status.$label
            ];
            $dd = [
              'is_act' => 1
            ];
            break;
            case 12:
              $label = '<span class="badge bg-danger" style="color:#fff;">Ditolak General Manager ('.$pia_name.')</span><br>';
              $ket_status = str_replace($label,'',$ket_status);
              $data = [
                'status' => 12,
                'ket_status' => $ket_status.$label
              ];
              $dd = [
                'is_act' => 1
              ];
              break;
        default:
          
          break;
      }
      
      /*set sp_receiver*/
      // $dd = [
      //   'hrd_check' => 1
      // ];

      // print_r($dd);

      $this->m_overtime->update_data(
        [
          'id_sp' => $parseData['id_sp'],
          'id_receiver'=> $parseData['pia']
        ],
          $dd
        ,
        'sp_receiver');

      // $this->m_overtime->update_data_receiver([
      //   'id_sp' => $parseData['id_sp'],
      //   'id_receiver' => $parseData['pia']
      // ],$dd,'sp_receiver');

      if ($this->id_bagian == 22) {
        /*hrd*/
        $this->m_overtime->update_data_overtime($where,$data,$reason,'sp_overtime',1);

        $dataSave = [
          'id_sp' => $parseData['id_sp'],
          'id_receiver' => $id_user,
        ];

        $user = $this->currentUser($id_user);  
        $userName = $user->name;

        $sp_receiver_id = $this->m_overtime->insert_data($dataSave,'sp_receiver'); 
        if ($sp_receiver_id) {
          $cfg = [
            'creator' => $this->user_id,
            'sp_id' => $parseData['id_sp'],
            'sp_receiver_id' => $sp_receiver_id,
            'receiver' => $id_user,
            'nama_creator' => $this->first_name
          ];
          $this->sendNotif($cfg,1,$userName);
        }

        // $financeArray=array(28,823,110);
        // foreach($financeArray as $value){
        
        //   $dataFinance = [
        //     'id_sp' => $parseData['id_sp'],
        //     'id_receiver' => $value,
        //     'as_finance' => 1
        //   ];

        // $sp_receiver_id_ = $this->m_overtime->insert_data($dataFinance,'sp_receiver');
        // if ($sp_receiver_id_) {
        //   $cfg = [
        //     'creator' => $this->user_id,
        //     'sp_id' => $parseData['id_sp'],
        //     'sp_receiver_id' => $sp_receiver_id_,
        //     'receiver' => $value,
        //     'nama_creator' => $this->first_name
        //   ];
        //   $this->sendNotif($cfg,2,$userName);
        // }  
        // }

      }else{
        $this->m_overtime->update_data_overtime($where,$data,$reason,'sp_overtime',0);
      }
      echo json_encode([
        'code' => 200,
        'msg' => 'data berhasil diproses'
      ]);
    }

    function validationForm($filedsCnf=[]){
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules($filedsCnf);
        /*formating the message error*/
        $msg = [];
        $status = $this->form_validation->run();
        foreach ($filedsCnf as $key => $obj) {
            $msg[$obj['field']] = trim(form_error($obj['field']),"<p></p>");
        }
        /**/
        $data=[
            'status' => ($status == FALSE)?'fail':'ok',
            'msg_error' => $msg
        ];
        return $data;
    }

    function setValidation($parseData){
      /*ubah rules & error msg jika menggunakan validasi khusus*/
      $valConfig = [];
      foreach ($parseData as $field => $value) {
          $valConfig[] = [
              'field' => $field,
              'label' => str_replace('_',' ',ucwords($field)),
              'rules' => 'required',
              'errors' => [
                  'required' => 'Field wajib diisi!',
              ]
          ];
      }
      return $valConfig;
    }

    function setValidation_($parseData){
     
      $valConfig = [];

      $tanggal = $parseData['tanggal']; //Invalid date
      $jenis_hari = $parseData['jenis_hari'];
      $id_department = $parseData['id_department'];
      $id_user = $parseData['nm_karyawan']; //0

      if ($tanggal == 'Invalid date') 
			{ 
        $valConfig[] = [
          'Tanggal'
      ];  
      } 

      if (strlen($jenis_hari) === 0) 
			{ 
        $valConfig[] = [
          'Jenis Hari'
      ];  
      } 

      if (strlen($id_department) === 0) 
			{ 
        $valConfig[] = [
          'Department'
      ];  
      } 


      $userKaryawan = ($id_user>0)?$id_user:$this->input->post('user');
      $user = $this->currentUser($userKaryawan);
      
      if ($id_user == 0) 
			{ 
        $valConfig[] = [
          'Menugaskan Kepada'
      ];  
      }
      
      else {

        if ($userKaryawan == 34) 
        { // penanggung jawab
          foreach ($parseData as $field => $value) {
            if (strlen($value) === 0 && $field != 'branch_manager'){
              $valConfig[] = [
                str_replace('_',' ',ucwords($field)),
            ];
            }
          }    
        } 
        elseif (in_array($user->id_position,[9,19])) 
        { // General Manager, Manager
          foreach ($parseData as $field => $value) {
            if (strlen($value) === 0 && $field != 'branch_manager' && $field != 'area_manager' ){
              $valConfig[] = [
                str_replace('_',' ',ucwords($field)),
            ];
            }
          } 
        } 
  
        elseif (in_array($user->id_position,[4,17,20,13])) 
        { // Area Manager,supervisor, ass gm, penanggung jawab
          foreach ($parseData as $field => $value) {
            if (strlen($value) === 0 && $field != 'branch_manager' && $field != 'area_manager' ){
              $valConfig[] = [
                str_replace('_',' ',ucwords($field)),
            ];
            }
          } 
        } 
        elseif ($user->id_position == 7) 
        { // Branch Manager
          foreach ($parseData as $field => $value) {
            if (strlen($value) === 0 && $field != 'branch_manager'){
              $valConfig[] = [
                str_replace('_',' ',ucwords($field)),
            ];
            }
          }   
        } 
        elseif (in_array($user->id_bagian,[20,21,22,25,29,31,32,1,2,3,28]) || in_array($user->id_position,[11])  || (in_array($user->id_position,[10]) && in_array($user->id_bagian,[17,30]))) 
              {
                foreach ($parseData as $field => $value) {
                  if (strlen($value) === 0 && $field != 'branch_manager' && $field != 'area_manager' ){
                    $valConfig[] = [
                      str_replace('_',' ',ucwords($field)),
                  ];
                  }
                } 
        } 
        elseif (in_array($userKaryawan,[47,79,78,73,828])) 
        { // officer, staff dengan manager dept
          foreach ($parseData as $field => $value) {
            if (strlen($value) === 0 && $field != 'branch_manager' && $field != 'area_manager' ){
              $valConfig[] = [
                str_replace('_',' ',ucwords($field)),
            ];
            }
          }
        } 
        elseif (in_array($user->id_bagian,[34])) 
        { 
          foreach ($parseData as $field => $value) {
            if (strlen($value) === 0){
              $valConfig[] = [
                str_replace('_',' ',ucwords($field)),
            ];
            }
          }
        } 
        elseif (in_array($user->id_position,[2,3,6,14,16,12,23,22,15])) 
        { 
          foreach ($parseData as $field => $value) {
            if (strlen($value) === 0){
              $valConfig[] = [
                str_replace('_',' ',ucwords($field)),
            ];
            }
          }
        } 
  
        else {
          foreach ($parseData as $field => $value) {
            if (strlen($value) === 0){
              $valConfig[] = [
                str_replace('_',' ',ucwords($field)),
            ];
            }
          }
        }


      }



      return $valConfig;
    }

    

    public function sendNotif($params=[]){
        $idReceivers[] = $params['receiver'];
        $msg = $params['nama_creator'] . ' telah membuat SP lembur';
        $nl = new Notify_lib();
        $nl->send('SP Lembur Report', //title
            $msg, //message
            $params['creator'], //sender
            $idReceivers, //list receiver
            [
              'sp_id' => $params['sp_id'],
              'sp_receiver_id' => $params['sp_receiver_id']
            ], //attachment
            "overtime", //type notifikasi
            true //kirim ke onesignal
        );

        return true;
    }

    function saveOvertime(){
      /*update
        - atasan = user creator
        - ka dept = gm
      */
        $parseData = $this->input->post();
        // echo "terminator";
        // echo json_encode($parseData);
        $response = [];
 
                    /*saving data*/
            $response['code'] = 200;
            $response['msg'] = 'berhasil disimpan';
            /* mentioned user */
            /**
              - atasan langsung
              - semua HRD
              - kepala department (GM)
            */
            /*switch to edit or save*/
            if ($parseData['id_sp']!='' && $parseData['id_sp']>0) {
              /*update*/
              $response['code'] = 201;
              $savedData = [
                'jenis_hari' => $parseData['jenis_hari'],
                'tgl_sp' => $parseData['tanggal'],
                'jam_mulai' => $parseData['jam_awal'],
                'jam_selesai' => $parseData['jam_akhir'],
                'ket_lembur' => $parseData['ket_lembur']
              ];
              $this->m_overtime->update_data(['id_sp' => $parseData['id_sp']],$savedData,'sp_overtime');
            }else{
              $user = $this->currentUser($parseData['nm_karyawan']);
              // $getAtasan = $this->getAtasan($parseData['nm_karyawan'],0);
              $getHrd = $this->getHrd(0);
              $listHrd = [];
              foreach ($getHrd as $key => $value) {
                $listHrd[] = $value['id_user'];
              }

              // $mentioner = [
              //   // 'atasan_langsung' => $parseData['atasan_langsung'],
              //   'ka_department' => $parseData['ka_department'],
              //   'hrd' => $listHrd
              // ];

              $mentioner = [];

              // #Edi Laksana => general manager - supporting group
              if ($parseData['user_id']==48){
                $mentioner['ka_department'] = $parseData['ka_department'];
                $mentioner['hrd'] = $listHrd;
              }

              // error
              // if ($parseData['user_id']!=48) {
              //   if($parseData['ka_department']==48){
              //     $mentioner['ka_department'] = $parseData['ka_department'];
              //     $mentioner['hrd'] = $listHrd;
              //   }
              //   if($parseData['ka_department']!=48 ) {
              //     $mentioner['ka_department'] = $parseData['ka_department'];
              //     $mentioner['gm_sg'] = '48';
              //     $mentioner['hrd'] = $listHrd;
              //   }
              // }


              if ($parseData['user_id']!=48) {
                  $mentioner['gm_sg'] = '48';
                  $mentioner['hrd'] = $listHrd;
              }

            
              if (in_array($parseData['user_id'],[37,55,57]) && $parseData['user_id'] != $parseData['area_manager']) {
                $mentioner['area_manager'] = $parseData['area_manager'];
                $mentioner['branch_manager'] = $parseData['branch_manager'];
              }

              if (in_array($parseData['user_id'],[37,55,57]) && $parseData['user_id'] === $parseData['area_manager']) {
                $mentioner['branch_manager'] = $parseData['branch_manager'];
              }
              

              // if ($parseData['id_department']==9 && !in_array($parseData['user_id'],[37,55,57]) && $parseData['user_id'] != $parseData['branch_manager']) {
                if ($parseData['id_department']==9 && !in_array($parseData['user_id'],[37,55,57])) {
                /*kondisi khusus notif OPT, tambah  branch & area manager*/
                // $bm = $this->getBranchManager($user->branch_id);
                // $atasan = $this->getAtasan($parseData['nm_karyawan'],0);
                // $listAm = [];
                // $listBm = [];
                // foreach ($atasan['area_manager'] as $key => $value) {
                //   $listAm[] = $value['id_user'];
                // }
                // foreach ($bm as $k => $v) {
                //   $listBm[] = $v['id_user']; 
                // }

                $mentioner['area_manager'] = $parseData['area_manager'];
                // $mentioner['branch_manager'] = $parseData['branch_manager'];
              }


              // if ($parseData['id_department']==9 && !in_array($parseData['user_id'],[37,55,57]) && $parseData['user_id'] === $parseData['branch_manager']) {
                if ($parseData['id_department']==9 && !in_array($parseData['user_id'],[37,55,57])) {
                $mentioner['area_manager'] = $parseData['area_manager'];
              }
              

              $savedData = [
                'branch_id' => $user->branch_id,
                'jenis_hari' => $parseData['jenis_hari'],
                'department_id' => $parseData['id_department'],
                'id_user' => $parseData['nm_karyawan'],
                'tgl_sp' => $parseData['tanggal'],
                'jam_mulai' => $parseData['jam_awal'],
                'jam_selesai' => $parseData['jam_akhir'],
                'mentioner' => json_encode($mentioner),
                'atasan' => $parseData['atasan_langsung'],
                'creator' => $this->user_id,
                'ket_lembur' => $parseData['ket_lembur'],
                'ket_status' => '<span class="badge bg-info" style="color:#fff;">Sedang Diproses!</span><br><span class="badge bg-info" style="color:#fff;">Disetujui Atasan ('.$this->first_name.')</span><br>',
                'status' => 10
              ];
              /*auto acc atasan on saving data first time*/

              $ids = [];
              foreach ($mentioner as $k => $v) {
                if (!is_array($v)) {
                  $ids[] = $v;
                }else{
                  foreach ($v as $x => $id) {
                    if (!in_array($id,$ids)) {
                      $ids[] = $id;
                    }
                    
                  }
                }
              }
              
              $sp_id = $this->m_overtime->insert_data($savedData,'sp_overtime');
              if ($sp_id) {
                /*save to notif & receiver*/
                foreach ($ids as $key => $receiver) {
                  /*sp_receiver*/
                  $dataSave = [
                    'id_sp' => $sp_id,
                    'id_receiver' => $receiver,
                  ];
                  /*cek hrd*/
                  if (in_array($receiver,$listHrd)) {
                    $dataSave['as_hrd'] = 1;
                  }

                  // if (in_array($receiver,[
                  //   $parseData['ka_department'],
                  // ])) {
                  //   $dataSave['as_acc'] = 1;
                  // }

                  if ($parseData['id_department']==9) {
                    /*bm as acc*/
                    // if (in_array($receiver,[
                    //   $parseData['branch_manager']
                    // ])) {
                    //   $dataSave['as_acc'] = 1;
                    // }
                    if (in_array($receiver,[
                      $parseData['area_manager']
                    ])) {
                      $dataSave['as_am'] = 1;
                    }
                  }

                  

                  $sp_receiver_id = $this->m_overtime->insert_data($dataSave,'sp_receiver');
                  if ($sp_receiver_id) {
                    $cfg = [
                      'creator' => $this->user_id,
                      'sp_id' => $sp_id,
                      'sp_receiver_id' => $sp_receiver_id,
                      'receiver' => $receiver,
                      'nama_creator' => $this->first_name
                    ];
                    // $user = $this->currentUser($sp_receiver_id);  
                    // $userName = $user->name;
                    $this->sendNotif($cfg,0,null);
                  }
                  
                }
                
              }
            }

        echo json_encode($response);
    }

    function deleteOvertime(){
      $NL = new Notify_lib();
      $idSp = $this->input->post('id_sp');
      $param=[
        'id_sp' => $idSp
      ];
      $exe = $this->m_overtime->delete_data($param,'sp_overtime');
      if ($exe) {
        $dt = $this->m_overtime->get_where([
          'id_sp' => $idSp
        ],'sp_receiver')->result();
        /*notification*/
        foreach ($dt as $key => $row) {
          $x = $NL->deleteNotifV1([
            [
              'flag' => 'sp_receiver_id',
              'value' => intval($row->id_sp)
            ],
          ],$row->id_receiver);
        }
        /*receiver*/
        $this->m_overtime->delete_data($param,'sp_receiver');
      }

      echo json_encode([
        'code' => 200,
        'msg' => 'data berhasil dihapus'
      ]);
    }

    function getMentioner($list=[]){
      // return $list;
      $where = [
        'in_where' => [
          'u.id_user' => $list 
        ]
      ];
      $dt = $this->getKaryawan(0,$where);
      $users = ''; /*str devide by ,(comma)*/
      if(!empty($dt)){
        foreach ($dt as $key => $obj) {
          $users .= ucwords($obj->name);
          $users .= ($key==(count($list)-1))?'':', ';
        }
      }

      return $users;
    }

    public function getOvertimeData($user=0){
        $filterTgl = $this->input->post('tgl');
        $filterCabang = $this->input->post('cabang');
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $offset = empty($start) ? 0 : $start;
        $limit = empty($length) ? 10 : $length;
        // $param['sp.id_user'] = $this->user_id;
        // if ($this->id_bagian == 22) {
        //     /*hanya hrd yg boleh lihat data semua user*/
        //     if ($user == 0) {
        //         unset($param['sp.id_user']);    
        //     }
        // }

        $param['sp.tgl_sp like'] = '%'.date('Y-m').'%';
        
        if (!empty($filterTgl)) {
            $param['sp.tgl_sp like'] = '%'.$filterTgl.'%';
        }

        if (!empty($filterCabang)) {
            $param['sp.branch_id'] = $filterCabang;
        }

        $config = [
          'fields' => ['sp.*','u.id_user','u.name','u.last_name','b.branch_name'], /*leave empty if select all field*/ 
          'relation' => [
                'users' => [
                  'table' => 'users u',
                  'key' => 'u.id_user=sp.id_user',
                  'type' => 'left'
                ],
                'branch' => [
                  'table' => 'branch b',
                  'key' => 'b.branch_id=u.branch_id',
                  'type' => 'left'
                ]
              ],
          'where' => $param,
          'master' => 'sp_overtime sp',
          'baseQuery'=> 1
        ];
        $data = [];
        $dt = $this->m_overtime->get_list($config,$limit,$offset)->result();
        /*formatting*/
        if (!empty($dt)) {
          foreach ($dt as $i => $obj) {
            $data[$i] = $obj;
            /*get id user*/
            $ids = [];
            foreach (json_decode($obj->mentioner,1) as $k => $v) {
              if (!is_array($v)) {
                $ids[] = $v;
              }else{
                foreach ($v as $x => $id) {
                  if (!in_array($i,$ids)) {
                    $ids[] = $id;  
                  }
                }
              }
              
            }
            $data[$i]->penerima = $this->getMentioner($ids);
          }
        }
        /**/
        $totalData = $this->m_overtime->count_list_all($config);
        $new_data = [
          'data' => []
        ];
        foreach ($data as $key => $value) {
            $status = $value->status;
            $new_data['data'][] = [ 
                          'id_sp' => $value->id_sp,
                          'no' => ($key+1),
                          'nama_karyawan' => $value->name.' '.$value->last_name,
                          'tanggal' => $this->misc->dateFormat($value->tgl_sp),
                          'penerima' => $value->penerima,
                          'ket_status' => $value->ket_status,
                          'cabang' => $value->branch_name,
                          'status' => $value->status
                        ];
        }

        $new_data['draw'] = isset($_REQUEST['draw'])?$_REQUEST['draw']:1;
        $new_data['recordsTotal'] = $totalData;
        $new_data['recordsFiltered'] = $totalData;
        echo json_encode($new_data);
    }
    
    function getUserRead() {
      $data = array();
      $id = $this->input->post('id_sp');
      $param = [
        'id_sp' => $id
      ];
      $cnfg = [
        'fields' => ['sp.*','u.name','u.last_name','p.name_position','p.id_position'], /*leave empty if select all field*/ 
          'relation' => [
                'users' => [
                  'table' => 'users u',
                  'key' => 'u.id_user=sp.id_receiver',
                  'type' => 'left'
                ],
                'position' => [
                  'table' => 'position p',
                  'key' => 'p.id_position=u.id_position',
                  'type' => 'left'
                ]
              ],
          'where' => $param,
          'master' => 'sp_receiver sp',
      ];
      $user_read = $this->m_overtime->get_related_data($cnfg)->result();
      // echo json_encode($user_read);
      if (!empty($user_read)) {
        foreach ($user_read as $key => $value) {
          $data[$key]['name'] = $value->name;
          $data[$key]['name_position'] = $value->name_position;
          $data[$key]['id_receiver'] = $value->id_receiver;
          $data[$key]['is_view'] = $value->is_view;
          $data[$key]['as_hrd'] = $value->as_hrd;
          $data[$key]['hrd_check'] = $value->hrd_check;
          $data[$key]['as_acc'] = $value->as_acc;
          $data[$key]['acc_check'] = $value->acc_check;
          $data[$key]['as_am'] = $value->as_am;
          $data[$key]['am_check'] = $value->am_check;
          $data[$key]['is_act'] = $value->is_act;
        }
      }

      echo json_encode($data);
    }

   
    public function response($data, $code = 200){
      $json =   json_encode($data);

      return $this->output
          ->set_content_type('application/json')
          ->set_status_header($code)
          ->set_output($json);
    }


}

