<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mcu extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->restapikey= $this->config->config['restapikey'];
		if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        $this->load->model('restapi/mcu_model', 'mcu');
        $this->load->model('restapi/user_model');
        $this->current_user = $this->input->post('user_id');
        $this->me = $this->user_model->one_user(['where' => ['_.id_user' => $this->current_user]]);
        if ($this->me == null) {
            echo json_encode(['status' => false, 'message' => 'Invalid user']);
            exit();
        }
    }

    public function index(){
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $search = $this->input->post('search');
        $type = $this->input->post('status');
        $tanggal = $this->input->post('tanggal');
        $lokasi = $this->input->post('lokasi');
		
        $args = [
            'where' => []
		];
		
		if ($search):
			$args['search'] = $search;
		endif;

		if (isset($tanggal) && !empty($tanggal)) :
			$args['tanggal'] = date('Y-m-d', strtotime($tanggal));
		endif;

		if (isset($lokasi) && !empty($lokasi)) :
			$args['where']['mcu_lokasi'] = $lokasi;
		endif;

		if (isset($user) && !empty($user)) :
			$args['where']['creator_id'] = $user;
		endif;

        // if ($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
        //     $args['start'] = $start;
        //     $args['length'] = $length;
        // }

        $r = $this->mcu->getData($type, $args);
        return $this->response($r);
	}
	
    public function count(){
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search');
		$type = $this->input->post('type');
		$filter = $this->input->post('filter');

		$args = [
			'where' => [],
			'order' => ['_.id', 'desc']
		];

		if ($search) :
			$args['search'] = $search;
		endif;

		if (isset($tanggal) && !empty($tanggal)) :
			$args['where']['_.date'] = date('Y-m-d', strtotime($tanggal));
		endif;
		if (isset($user) && !empty($user)) :
			$args['where']['_.from_id'] = $user;
		endif;

		if ($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
			$args['limit'] = [$start, $length];
		}

		$r1 = $this->mcu->getData('all', $args);
		$r2 = $this->mcu->getData('view2', $args);
		$r = [
			'view1' => count($r1),
			'view2' => count($r2),
			'all' => count($r1)
		];
        return $this->response($r);
    }

    public function save(){
        $input = $this->input->post();
		$user = $this->current_user;
		$data = [
			'branch_id' => isset($input['branch_id']) ? $input['branch_id'] : null ,
			'mcu_perusahaan' => $input['perusahaan'],
			'mcu_kode' =>  $input['kode'],
			'mcu_tgl_mulai' => date('Y-m-d',strtotime($input['tgl_mulai'])),
			'mcu_tgl_selesai' => date('Y-m-d', strtotime($input['tgl_selesai'])),
			'mcu_jumlah' => $input['jumlah'],
			'mcu_lokasi' => $input['lokasi'],
			'mcu_detail_lokasi' => $input['detail_lokasi'],
			'creator_id' => $user,
			'created' => date('Y-m-d H:i:s')
		];
		if (isset($input['branch_id']) && !empty($input['branch_id'])):
			$this->db->where_in('branch_id',$input['branch_id']);
			$getBranchName = $this->db->get('branch')->result();
			if ($getBranchName):
				foreach($getBranchName as $branchName):
					$branch[] = $branchName->branch_name;
				endforeach;
			else:
				$branch = [];
			endif;
			$data['branch_name'] = join(',',$branch);
		endif;
		if (isset($input['id_mcu'])):
			$id = $input['id_mcu'];
			$data['id_mcu'] = $input['id_mcu'];
		else:
			$id = 0;
		endif;
		if (!empty($input['kode']) && $this->checkDuplicate($input['kode'],$id)):
			$this->response(['msg' => "duplicate"]);
		else:
			$pemeriksaan = [];
			if (isset($input['id_tes'])) :
				$getDataTest = $this->db->where_in('id_tes', $input['id_tes'])->get('master_mcu_tes');
				$dataTest = $getDataTest->result();
				foreach ($dataTest as $tes) :
					$pemeriksaan[] = [
						'id_kat' => $tes->id_kat,
						'id_sub' => $tes->id_sub,
						'id_tes' => $tes->id_tes
					];
				endforeach;
				if (in_array('1',$input['id_kategori'])):
					$pemeriksaan[] = [
						'id_kat' => 1,
						'id_sub' => 0,
						'id_tes' => 0
					];
				endif;
			else :
				if (in_array('1', $input['id_kategori'])) :
					$pemeriksaan[] = [
						'id_kat' => 1,
						'id_sub' => 0,
						'id_tes' => 0
					];
				endif;
			endif;
			// print_r($pemeriksaan);
			$save = $this->mcu->save($data, $pemeriksaan);
			// print_r($save);
			if ($save) :
				$this->response([
					'msg' => "success",
					'data' => $save
				]);
			else :
				$this->response([
					'msg' => "gagal",
					'data' => $save
				]);
			endif;
		endif;
	}

	public function checkDuplicate($kode,$id = 0){
		if ($id !== 0) :
			$this->db->where('mcu_kode', $kode);
			$this->db->where('id_mcu !=', $id);
			$getUnique = $this->db->get('mcu');
			if ($getUnique->num_rows() > 0) :
				return TRUE;
			else:
				return FALSE;
			endif;
		else :
			$getUnique = $this->db->get_where('mcu', ['mcu_kode' => $kode]);
			if ($getUnique->num_rows() > 0) :
				return TRUE;
			else:
				return FALSE;
			endif;
		endif;
	}

	public function delete(){
        $result = $this->mcu->delete($this->input->post('id'));
        echo json_encode($result);
    }

	public function dataDropdown($table){
		if ($table === 'subs'):
			$table = 'master_mcu_sub';
			$order = 'sub_kat';
			$where = [];
		elseif($table === 'tests'):
			$idCat = $this->input->post('id_kategori'); 
			$idSub = $this->input->post('id_sub');
			$table = 'master_mcu_tes';
			$order = 'nama_tes';
			if (! empty($idCat) && !empty($idSub)):
				$where = ['id_kat' => $idCat,'id_sub' => $idSub];
			else:
				$where = [];
			endif;
		else: 
			$where = [];
			$order = 'branch_name';
		endif;
		$data_branch = $this->mcu->getDataDropdown($table, $order,$where)->result();
		// $data_branch = $this->mcu->getDataDropdown($table, $order,$where);
		echo json_encode($data_branch);
	}

    public function detail(){
        $id = $this->input->post('id');
        $detailMcu = $this->mcu->detail($id);
        return $this->response($detailMcu);
    }

    public function response($data, $code = 200){
        $json =   json_encode($data);
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header($code)
            ->set_output($json);
    }
}
