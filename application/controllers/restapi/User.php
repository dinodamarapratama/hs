<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        header('Content-Type: application/json');
        $this->restapikey= $this->config->config['restapikey'];
        if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        $this->load->model('restapi/user_model', 'muser');
        $this->user_id = $this->input->post('user_id');
    }

    public function all()
    {
        // $users = $this->muser->all();
        $user_id = $this->input->post('user_id');
        $users = $this->muser->many_user();

        $this->load->model('restapi/user_model');
        $branch_ = $this->user_model->many_user([
			'where_in' => ['_.id_user' => $user_id],
		]);
        $branch_id = $branch_[0]['branch_id'];
		
		// fungsi peniadaan filter dokter PJ freelance yang tidak sesuai cabang
		$blockUsers = [];
		$users = $this->user_model->many_user(['where_in' => ['_.id_position' => 13, '_.STATUS' => 'FREELANCE' ]]);
		foreach ($users as $key => $usr) {
			$input   = $usr['many_branch'];
			$numbers = str_split("',");
			$output  = str_replace($numbers,' ',$input);
			$numArray = explode(" ", $output);
			$final = (array_values(array_filter($numArray)));
			// if (in_array($this->branch_id, $final) == false){
			// 	$blockUsers[] = $usr['id_user'];
			// }
            if (in_array($branch_id, $final) == false){
				$blockUsers[] = $usr['id_user'];
			}
		}

        // echo json_encode($blockUsers);

		$users_ = $this->user_model->many_user([
			'where_not_in' => ['_.id_user' => $blockUsers],
		]);

        echo json_encode($users_);
    }

    public function save()
    {
            $user= [];
            $data = [
                'id_user' => $this->input->post('id_user'),
                'nip' => $this->input->post('nip'),
                'password' => $this->input->post('password')
            ];
            $user = $this->muser->save($data,'users');

        echo json_encode($user);
    }

    public function verify()
    {
        $username = $this->input->post('username');
        $salt = 'Bio Medika';
        $password = '123456';
        $where = array('name=$username OR nip=$username OR username=$username', 'password' => sha1($salt . $password));
        $data = $this->m_mis->edit_data($where, 'users');
        $d = $this->m_mis->edit_data($where, 'users')->row();
        echo json_encode($d);
    }

    public function getLoginDate(){
        $id = $this->input->post('user_id');
        $data = $this->muser->getLoginDate($id);
        echo json_encode($data);
    }

    public function signin(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');
       
       
        $user = $this->muser->signin($username, $password);
        // print_r($user);
        echo $user;
    }

    public function register(){
        $username = $this->input->post('username');
        $name = $this->input->post('name');
        $teleponhp = $this->input->post('hp');
        $password = $this->input->post('password');
        $data = [
            'name' => $name,
            'username' => $username,
            'password' => strtoupper(sha1('Bio Medika'.$password)),
            'TELPONHP' => $teleponhp,
            'STATUS'   => 'FREELANCE'
        ];
        $user = $this->muser->register($data);
        echo json_encode([
            "user" => $user
        ]);
    }

    public function store_onesignal()
    {
        $user_id = $this->input->post('user_id');
        $player_id = $this->input->post('player_id');
        $old_id = $this->input->post("old_id");
        $r = $this->muser->store_onesignal($this->user_id, $player_id, $old_id);
        echo json_encode($r);
    }

    function delete_onesignal(){
        $data = [
            "user_id" => $this->input->post("id_user"),
            "player_id" => $this->input->post("player_id")
        ];

        $r = $this->muser->delete_onesignal($data);
        
        echo json_encode($r);
    }

    function change_password()
    {
        $result = $this->muser->change_password();
        echo json_encode($result);
    }

    public function avatar()
    {
        // echo json_encode(['path' => FCPATH.'uploads/avatar/', 'is_dir' => is_dir(FCPATH.'uploads/avatar/')]);
        $config = array(
            'allowed_types'     => 'jpg|jpeg|gif|png', //only accept these file types
            'max_size'          => 2048 * 10, //2MB max
            // 'upload_path'       => FCPATH.'uploads/avatar/', //upload directory
            'upload_path'       => './uploads/avatar/', //upload directory
        );
        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->upload->do_upload('file');
        $image_data = $this->upload->data(); //upload the image
        // echo json_encode($image_data);
        // echo json_encode($this->upload->display_errors());
        $config1 = array(
            'source_image'      => $image_data['full_path'], //path to the uploaded image
            'new_image'         => FCPATH.'uploads/avatar/', //path to
            'maintain_ratio'    => true,
            'width'             => 512,
            'height'            => 512
        );
        $this->load->library('image_lib', $config1);
        // $this->image_lib->initialize($config);
        $this->image_lib->resize();

        $this->makeThumb($image_data['full_path'], 256);

        $user = $this->muser->change_avatar($image_data['file_name']);

        echo json_encode([
            'upload' => $image_data,
            'user' => $user
        ]);
    }

    private function makeThumb( $filename, $thumbSize = 100 ){
        if(in_array('png', explode('.', $filename))) 
            $source_img = imagecreatefrompng($filename);
        else
            $source_img = imagecreatefromjpeg($filename);

        $new_w = $thumbSize;
        $new_h = $thumbSize;
            
        $orig_w = imagesx($source_img);
        $orig_h = imagesy($source_img);
            
        $w_ratio = ($new_w / $orig_w);
        $h_ratio = ($new_h / $orig_h);
            
        if ($orig_w > $orig_h ) {//landscape
            $crop_w = round($orig_w * $h_ratio);
            $crop_h = $new_h;
        } elseif ($orig_w < $orig_h ) {//portrait
            $crop_h = round($orig_h * $w_ratio);
            $crop_w = $new_w;
        } else {//square
            $crop_w = $new_w;
            $crop_h = $new_h;
        }
        $dest_img = imagecreatetruecolor($new_w,$new_h);
        imagecopyresampled($dest_img, $source_img, 0 , 0 , 0, 0, $crop_w, $crop_h, $orig_w, $orig_h);
        imagejpeg($dest_img, $filename);
    }

    function my_menu(){
        $this->load->model('m_master_menu');
        $r = $this->m_master_menu->getMyMenu($this->user_id);
        echo json_encode($r);
    }

}