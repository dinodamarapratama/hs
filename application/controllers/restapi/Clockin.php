<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Clockin extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->restapikey= $this->config->config['restapikey'];
        if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        $this->load->model('restapi/clockin_model', 'clockin');
        $this->load->model('restapi/user_model');
        $this->current_user = $this->input->post('user_id');
        $this->me = $this->user_model->one_user(['where' => ['_.id_user' => $this->current_user]]);
        if ($this->me == null) {
            echo json_encode(['status' => false, 'message' => 'Invalid user']);
            exit();
        }
    }

    public function index(){
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $search = $this->input->post('search');
        $type = $this->input->post('type');
        $tanggal = $this->input->post('tanggal');

        $args = [
            'where' => []
        ];

        if ($search) :
            $args['search'] = $search;
        endif;

        if (isset($tanggal) && !empty($tanggal)) :
            $args['tanggal'] = date('Y-m-d', strtotime($tanggal));
        endif;
        if (isset($user) && !empty($user)) :
            $args['where']['from_id'] = $user;
        endif;

        if ($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
            $args['start'] = $start;
            $args['length'] = $length;
        }

        $r = $this->clockin->getData($type, $args);
        return $this->response($r);
    }

    public function save(){
        $input = $this->input->post();
        $data = [
            'id_user' => $this->current_user,
            'tgl_absen' => date('Y-m-d'),
            'jam_absen' => $input['waktu'],
            'kondisi_badan' => $input['kondisi'],
            'perjalanan_luar' => $input['perjalanan'],
            'tgl_perjalanan_luar' => $input['perjalanan'] == 1 ? date('Y-m-d', strtotime($input['tgl_perjalanan_luar'])) : "0000-00-00",
            'lokasi_perjalanan_luar' => $input['lokasi_perjalanan_luar'],
            'keramaian' => $input['keramaian'],
            'tgl_keramaian' => $input['keramaian'] == 1 ? date('Y-m-d', strtotime($input['tgl_keramaian'])) : "0000-00-00",
            'lokasi_keramaian' => $input['lokasi_keramaian'],
            'kontak_covid' => $input['kontak_covid'],
            'tgl_kontak' => $input['kontak_covid'] == 1 ? date('Y-m-d', strtotime($input['tgl_kontak'])) : "0000-00-00",
            'kontak_siapa' => $input['kontak_siapa'],
            'gejala_demam' => $input['gejala_demam'],
            'gejala_batuk' => $input['gejala_batuk'],
            'gejala_flu' => $input['gejala_flu'],
            'gejala_tenggorokan' => $input['gejala_tenggorokan'],
            'gejala_sesak' => $input['gejala_sesak'],
            'gejala_nyeri' => $input['gejala_nyeri'],
            'gejala_pusing' => $input['gejala_pusing'],
            'gejala_mata_merah' => $input['gejala_mata_merah'],
            'gejala_kulit_merah' => $input['gejala_kulit_merah'],
        ];
        if (isset($_FILES['lampiran_srt_dokter'])):
            $config['upload_path']          = 'uploads/surat_dokter/';
            $config['allowed_types']        = 'jpg|png|jpeg|pdf';
            $config['overwrite']            = true;
            $new_file_name                  = 'C0U'.$this->current_user.'_Clockin' . date('Y-m-d');
            $config['file_name']            = $new_file_name;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            @chmod(FCPATH . 'uploads/surat_dokter/', 0777);
            if ($this->upload->do_upload('lampiran_srt_dokter')):
                $data['lampiran_srt_dokter'] = $new_file_name . $this->upload->data('file_ext');
            endif;
        endif;
        if($input['kondisi'] == 0):
            $data['status'] = 'decline';
            $data['card_status'] = 'expired';
            $data['result_case'] = 'r2';
        elseif($input['kondisi'] == 1 && $input['kontak_covid'] == 1):
            $data['status'] = 'decline';
            $data['card_status'] = 'exired';
            $data['result_case'] = 'r3';
        else:
            $data['card_status'] = 'active';
            $data['status'] = 'accept';
            $data['result_case'] = 'r1';
        endif;
        // print_r($data);
        $save = $this->clockin->save($data);
        // print_r($save);
        if ($save):
            $this->load->library("Notify_lib");
            $nl = new Notify_lib();
            $idReceivers = [];
            if ($input['kondisi'] == 0):
                $users = $this->db->get_where('users', ['id_bagian' => 22])->result();
                foreach ($users as $value) {
                    $idReceivers[] = $value->id_user;
                }
                //print_r($idReceivers);
                $description = $input['nama'] . ' tidak diperbolehkan masuk pada tanggal ' . date('Y-m-d H:i');
                $nl->send(
                    'Clockin Report', //title
                    $description, //'Request Izin Sudah Disetujui',        //message
                    $this->current_user,     //sender
                    $idReceivers,             //list receiver
                    ['clockin_id' => $save],        //attachment
                    "clockin",          //type notifikasi
                    true               //kirim ke onesignal
                );
            else:
                /*delete : perubahan flow notif*/
                // $description = $input['nama'] . ' Silahkan mengisi Suhu di tanggal ' . date('Y-m-d H:i');
                // // $description = 'Anda sudah melakukan pengecekan suhu, silahkan lakukan verifikasi Clock In';
                // $nl->send(
                //     'Clock In Report', //title
                //     $description, //'Request Izin Sudah Disetujui',        //message
                //     1,     //sender
                //     [$this->current_user],             //list receiver
                //     ['clockin_id' => $save],        //attachment
                //     "clockin",          //type notifikasi
                //     true               //kirim ke onesignal
                // );
            endif;
            return $this->response([
                'msg' => "success",
                'data' => $save
            ]);
        else :
            return $this->response([
                'msg' => "gagal",
                'data' => $save
            ]);
        endif;
    }

    public function getClockinToday(){
        $getData = $this->db->where('tgl_absen',date('Y-m-d'))
                            ->where('id_user', $this->current_user)
                            ->from('clockin_user')->count_all_results();
        return $this->response([
            'msg' => 'success',
            'data' => $getData
        ]);
    }

    public function saveSuhu(){
        $id = $this->input->post('id');
        $suhu = $this->input->post('suhu');
        $suhu_id = $this->input->post('suhu_id');
        $update = $this->clockin->updateSuhu($id,$suhu,intval($suhu_id));
        return $this->response([
            'msg' => "success",
            'data' => $update
        ]);
    }

    public function delete(){
        $result = $this->clockin->delete($this->input->post('id'));
        return $this->response($result);
    }

    public function detail(){
        $id = $this->input->post('id');
        $detailMcu = $this->clockin->detail($id);
        return $this->response($detailMcu);
    }

    public function cekSuhu(){
        $id = $this->input->post('user_id');
        $tanggal = date('Y-m-d',strtotime($this->input->post('tanggal')));
        $cekSuhu = $this->clockin->cekSuhu($id,$tanggal);
        return $this->response($cekSuhu);
    }

    public function response($data, $code = 200){
        $json =   json_encode($data);
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header($code)
            ->set_output($json);
    }
}
