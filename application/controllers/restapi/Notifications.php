<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class notifications extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        // if($this->uri->segment(3) !== "pdf"){
        header('Content-Type: application/json');
        // if($this->input->post('auth_key') != "c0untingst4rs") die(/*Silent is gold*/);
        // }
        $this->load->model('restapi/notification_model', 'mnotification');
        $this->load->model('restapi/user_model');
        $this->user_id = $this->input->post('user_id');
        $this->user = $this->user_model->one_user(['where' => ['_.id_user' => $this->user_id]]);

        if($this->user == null) {
            echo json_encode(['status' => false, 'message' => 'User tidak valid']);
            exit();
        }
    }

    public function index()
    {
        $notifications = $this->mnotification->get(true);
        echo json_encode($notifications);
    }

    public function get_all()
    {
        $notifications = $this->mnotification->get(false);
        echo json_encode($notifications);
    }

    public function unread()
    {
        $notifications = $this->mnotification->get(true);
        echo json_encode(["count" => count($notifications)]);
    }

    public function read(){
        // $result = $this->mnotification->read(true);
        // echo json_encode(["result" => $result]);
        $id = $this->input->post('id');
		$this->db->where('id', $this->input->post('id'))->update('notifications', [
			'read' => 1
		]);
        $r = $this->mnotification->mark_read($id);
        echo json_encode($r);
    }

    public function read_all()
    {
        $r = $this->mnotification->mark_all_read($this->user_id);
        echo json_encode($r);
    }

    function recent_report(){
        $this->load->model([
            'restapi/dailyreport_model',
            'restapi/generalreports_model',
            // 'm_general',
            'm_itreports',
            'm_monthlyreports',
            'm_techreports'
        ]);

        $sql = $this->dailyreport_model->recent_report($this->user_id)
                . "\n\n\n union \n\n\n" .
                $this->generalreports_model->recent_report($this->user_id)
                . "\n\n\n union \n\n\n" .
                $this->m_itreports->recent_report($this->user_id)
                . "\n\n\n union \n\n\n" .
                $this->m_monthlyreports->recent_report($this->user_id)
                . "\n\n\n union \n\n\n" .
                $this->m_techreports->recent_report($this->user_id);

        $sql2 = "SELECT s1.*, usr.name, usr.username, usr.call_name,
                 usr.image
                 from ($sql) s1
                 join users usr on usr.id_user = s1.sender_id
                 order by last_update desc limit 3";

        $q = $this->db->query($sql2)->result();
        echo json_encode($q);
    }


    public function get_reminder() {
        $data_reminder = array();
        $r = array();

        $data_reminder['ijin'] = $this->get_ijin_reminder();
        $data_reminder['control'] = $this->get_control_reminder();
        $data_reminder['maintain_ext'] = $this->get_ext_reminder();
		
        if (!empty($data_reminder)) {
            foreach ($data_reminder as $key => $datas) {
                    if (empty($datas)) {
                        continue;
                    }
                foreach ($datas as $key2 => $value) {
                    if (empty($value)) {
                        continue;
                    }
                    $data['id'] = $value->id;
                    $data['description'] = $value->description;
                    $data['message'] = $value->message;
                    $data['data'] = $value->data;
                    $data['key'] = $key;

                    $time = date('d-M-y', strtotime($value->time));
                    $data['time'] = $time;
                    array_push($r, $data);
                }
            }
        }

        echo json_encode($r);
    }

	public function getCuti() {
		$getCuti = $this->mnotification->getCuti();
        echo json_encode($getCuti);
	}

	public function getCutiUnread(){
        $getCuti = $this->mnotification->getCuti(true);
        echo json_encode($getCuti);
    }

    public function get_ijin_reminder() {

        if (!in_array ( $this->user_id,[47,75,608,48])) {
            return [];
        }
            $tgl = date('Y-m-d');
            $bln = date('d');
        $q = "SELECT `ijin`.*, `reminder`.*, `ijin_code`.*, `branch`.*
                FROM `ijin`
                JOIN `reminder` ON `ijin`.`reminder_id` = `reminder`.`reminder_id`
                JOIN `ijin_code` ON `ijin`.`ijin_code_id` = `ijin_code`.`ijin_code_id`
                JOIN `branch` ON `ijin`.`branch_id` = `branch`.`branch_id`
                WHERE DATE_FORMAT(ijin.ijin_exp, '%Y-%m-%d') >= $tgl
                AND DATE_FORMAT(ijin.ijin_exp, '%d') = $bln
                AND (`status` IS NULL OR `status` = '')

                UNION

                SELECT `ijin_renew`.*, `reminder`.*, `ijin_code`.*, `branch`.*
                FROM `ijin_renew`
                JOIN `reminder` ON `ijin_renew`.`reminder_id` = `reminder`.`reminder_id`
                JOIN `ijin_code` ON `ijin_renew`.`ijin_code_id` = `ijin_code`.`ijin_code_id`
                JOIN `branch` ON `ijin_renew`.`branch_id` = `branch`.`branch_id`
                WHERE DATE_FORMAT(ijin_renew.ijin_exp, '%Y-%m-%d') >= $tgl
                AND DATE_FORMAT(ijin_renew.ijin_exp, '%d') = $bln
                AND (`status` IS NOT NULL OR `status` = 'RENEW')";
        $this->db->query($q);

        $datas['ijin'] =  $this->db->query($q)->result();

        $r = array();

        if(count($datas) && !empty($datas)){
            foreach ($datas as $k => $v) {
                if ($k == 'ijin') {
                    foreach ($v as $key => $data) {
                        // $tanggal = date("Y-m-d", strtotime(date('Y-m-d')."-4 day"));
                        $tanggal = date("Y-m-d");
                        $ijin_exp = date("Y-m-d", strtotime($data->ijin_exp));

                        $reminder = array();
                        for($i = 1; $i <= (int)$data->reminder_name; $i++) {
                            $reminder[$i] = date("Y-m-d", strtotime($ijin_exp."-".$i." month"));
                        }
                        $reminder[$i] = date("Y-m-d", strtotime($ijin_exp."-14 day"));
                        $reminder[$i+1] = date("Y-m-d", strtotime($ijin_exp."-7 day"));
                        $reminder[$i+2] = date("Y-m-d", strtotime($ijin_exp."-3 day"));

                        if(in_array($tanggal, $reminder)){
                            $dataa = new stdClass();
                $dataa->id = $data->ijin_id;
                $dataa->message = $data->branch_name . ' - '.$data->ijin_code_name .' - ' .$data->ijin_num  ;
                $dataa->description = "Ijin perlu diperbarui";
				$data->notif_type = "reqizin";
                $dataa->data = $data;
                // $dataa->data = $data;

                $dataa->time = $data->ijin_exp;
                array_push($r, $dataa);
                        }
                    }
                }
            }
        }

        return $r;
    }

    public function get_control_reminder() {

        if (!in_array ( $this->user_id,[47])) {
            return [];
        }

        $qc_list = $this->db->query("SELECT * FROM (SELECT `_`.*, `usr`.`name` as `creator_name`, `ctrl`.`control_name`, `b1`.`branch_name`, CONCAT(_.lot_number, ' - ', ctrl.control_name) AS lot_number_text FROM `master_qc_lot_number` `_`LEFT JOIN `users` `usr` ON `usr`.`id_user` = `_`.`creator_id`LEFT JOIN `master_control` `ctrl` ON `ctrl`.`id_control` = `_`.`id_control`LEFT JOIN `branch` `b1` ON `b1`.`branch_id` = `_`.`branch_id`) t1 WHERE expired_date = DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 7 DAY), '%Y-%m-%d') ORDER BY expired_date")->result();

        $r = array();

        foreach ($qc_list as $key => $data) {
                            $dataa = new stdClass();
                $dataa->id = $data->id_lot_number;
                $dataa->message = $data->lot_number_text;
                $dataa->description = "Control perlu diperbarui";
				$data->notif_type = 'control';
                $dataa->data = $data;

                $dataa->time = $data->expired_date;
                array_push($r, $dataa);

        }
        return $r;
    }

    public function get_ext_reminder() {
        $this->load->model('M_maintain_ext');
        $reminder_list = $this->M_maintain_ext->get_reminder($this->user['branch_id'])->result();

        $r = array();

        foreach ($reminder_list as $key => $data) {
                            $dataa = new stdClass();
                $dataa->id = $data->id_maintain_ex;
                $dataa->message = $data->alat_name;
                $data->notif_type = 'schedule';
                $dataa->description = "Reminder maintanance external";
                $dataa->data = $data;

                $dataa->time = $data->tanggal;
                array_push($r, $dataa);

        }
        return $r;
    }
	
}
