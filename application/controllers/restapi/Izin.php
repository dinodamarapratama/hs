<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

include_once (dirname(__FILE__) . "/Cuti.php");

class Izin extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->restapikey= $this->config->config['restapikey'];
        if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        $this->load->model('restapi/user_model');
        $this->current_user = $this->input->post('user_id');
		$this->me = $this->user_model->one_user(['where' => ['_.id_user' => $this->current_user]]);
		if($this->me == null) {
			echo json_encode(['status' => false, 'message' => 'Invalid user']);
			exit();
		}
        $this->load->model('restapi/Izin_model', 'izin');
        $this->load->model('m_cuti_api');
        $this->load->model('m_cuti');
		$this->load->model('m_izin');
    }

    public function state()
    {
        $params['id_user'] = $this->current_user;

        $karyawan =  $this->db->query("select CUTI as jatah_izin, TGLMASUK as tglmasuk
            from users a
            where id_user='$this->current_user'
        ")->first_row();

        $izin_diambil = $this->getLastIzin($this->current_user);
        $karyawan->jatah_diambil = $izin_diambil;
        return $this->response([
          'msg'=> 'state',
          'data'=> $karyawan
        ]);
    }

    public function getLastIzin($id_user=0, $type = 'view')
    {
        $param['year(tgl_request)'] = date('Y');
        $param['users.id_user'] = $id_user;
        $param['izin.status_pengajuan'] = 'Sudah disetujui!';
        $data = $this->izin->get_data_izin($param, 0, 0, 1);

        $jml_izin_lalu = 0;
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $jml_izin_lalu = $jml_izin_lalu+$value['request_izin'];
            }
        }
        if ($type === 'json'):
            return $this->response(14-$jml_izin_lalu);
        else:
            return $jml_izin_lalu;
        endif;
    }

    public function atasan(){
        $user = $this->current_user();

        // $param=[
        //     'branch.branch_id' => $user->branch_id,
        //     'in_param' => [
        //         'STATUS' => [
        //             'TETAP',
        //             'KONTRAK'
        //         ],
        //     ],
        //     'not_in_param' => [
        //         'users.id_position' => [1,21] /*direktur tdk termasuk*/
        //     ]
        // ];
        // if (strtolower($user->branch_name) == 'all') {
        //     unset($param['branch.branch_id']);
        // }



        // $param['users.id_position'] = 7;/*branch manager*/
        // $data['branch_manager'] = $this->m_cuti_api->get_karyawan($param, 0, 0, 1);
        // unset($param['users.id_position']);
        // $param['id_bagian'] = 22;/*HR*/
        // $data['HR'] = $this->m_cuti_api->get_karyawan($param, 0, 0, 1);
        // unset($param['id_bagian']);
        // $param['users.id_position'] = 9;/*GM*/
        // unset($param['branch.branch_id']);
        // $data['manager'] = $this->m_cuti_api->get_karyawan($param, 0, 0, 1);
        //
        // unset($param);
        // $param = [
        //     'in_param' => [
        //         'users.id_position' => [1,21]
        //     ]
        // ];
        // $data['direktur'] = $this->m_cuti_api->get_karyawan($param, 0, 0, 1);
        if ($this->current_user == 34) 
		{ // penanggung jawab
             $paramArea=[
                'in_param' => [
                    'users.id_position' => [
                        4
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            $paramGM=[
                'in_param' => [
                    'users.id_position' => [
                        9
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            $data['show_branch_manager'] = false;
            $data['show_manager'] = false;
            $data['area_manager'] = $this->m_cuti_api->get_karyawan($paramArea, 0, 0, 1);
            $data['show_area_manager'] = true;
            $data['general_manager'] = $this->m_cuti_api->get_karyawan($paramGM, 0, 0, 1);
            $data['show_general_manager'] = true;
            $data['show_direktur'] = false;
			/*$paramGM=[
                'in_param' => [
                    'users.id_position' => [
                        9
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            $data['show_branch_manager'] = false;
            $data['show_manager'] = false;
            $data['show_area_manager'] = false;
            $data['general_manager'] = $this->m_cuti_api->get_karyawan($paramGM, 0, 0, 1);
            $data['show_general_manager'] = true;
            $data['show_direktur'] = false;*/
        } 
		elseif (in_array($user->id_position,[9,19])) 
        { // General Manager, Manager
            $param=[
                'in_param' => [
                    'users.id_position' => [
                        1
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            $data['show_branch_manager'] = false;
            $data['show_manager'] = false;
            $data['show_area_manager'] = false;
            $data['show_general_manager'] = false;
            $data['direktur'] = $this->m_cuti_api->get_karyawan($param, 0, 0, 1);
            $data['show_direktur'] = true;
        } 
        elseif (in_array($user->id_position,[4,17,20,13])) 
        { // Area Manager,supervisor, ass gm, penanggung jawab
            $paramGM=[
                'in_param' => [
                    'users.id_position' => [
                        9
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            $data['show_branch_manager'] = false;
            $data['show_manager'] = false;
            $data['show_area_manager'] = false;
            $data['general_manager'] = $this->m_cuti_api->get_karyawan($paramGM, 0, 0, 1);
            $data['show_general_manager'] = true;
            $data['show_direktur'] = false;
        } 
        elseif ($user->id_position == 7) 
        { // Branch Manager
            $paramArea=[
                'in_param' => [
                    'users.id_position' => [
                        4
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            $paramGM=[
                'in_param' => [
                    'users.id_position' => [
                        9
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            $data['show_branch_manager'] = false;
            $data['show_manager'] = false;
            $data['area_manager'] = $this->m_cuti_api->get_karyawan($paramArea, 0, 0, 1);
            $data['show_area_manager'] = true;
            $data['general_manager'] = $this->m_cuti_api->get_karyawan($paramGM, 0, 0, 1);
            $data['show_general_manager'] = true;
            $data['show_direktur'] = false;
        } 
        elseif (in_array($user->id_bagian,[20,21,22,25,29,31,32,1,2,3,28]) || in_array($user->id_position,[11])  || (in_array($user->id_position,[10]) && in_array($user->id_bagian,[17,30]))) 
        { // officer
            $paramManager=[
                'in_param' => [
                    'users.id_position' => [
                        17,19
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            $paramGM=[
                'in_param' => [
                    'users.id_position' => [
                        9
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            // $param['users.id_user'] = 75;
            $data['show_branch_manager'] = false;
            $manager_list = $this->m_cuti_api->get_karyawan($paramManager, 0, 0, 1);
            //$data['manager'] = (count($manager_list)>0?$manager_list:false);
            //$data['show_manager'] = (count($manager_list)>0?true:false);
            $data['show_manager'] = false;
            $data['show_area_manager'] = false;
            $data['general_manager'] = $this->m_cuti_api->get_karyawan($paramGM, 0, 0, 1);
            $data['show_general_manager'] = true;
            $data['show_direktur'] = false;
        } 
         elseif (in_array($this->current_user,[47,79,78,73,828])) 
        { // officer, staff dengan manager dept
            $paramManager=[
                'in_param' => [
                    'users.id_position' => [
                        17,19
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            $paramManagerIT=[
                'in_param' => [
                    'users.id_user' => [
                        47
                    ],
                ]
            ];
            $paramGM=[
                'in_param' => [
                    'users.id_position' => [
                        9
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            // $param['users.id_user'] = 75;
            $data['show_branch_manager'] = false;
            $manager_list = $this->m_cuti_api->get_karyawan($paramManager, 0, 0, 1);
            $manager_list_ = $this->m_cuti_api->get_karyawan($paramManagerIT, 0, 0, 1);
            // $allManager = array_merge($manager_list,$manager_list_);
            $allManager = array_merge($manager_list_);
            // $data['manager'] = (count($allManager)>0?$allManager:false);
            // $data['show_manager'] = (count($manager_list)>0?true:false);
            $data['manager'] = $this->m_cuti_api->get_karyawan($paramManagerIT, 0, 0, 1);
            $data['show_manager'] =  true;
            //$data['show_manager'] = false;
            $data['show_area_manager'] = false;
            $data['general_manager'] = $this->m_cuti_api->get_karyawan($paramGM, 0, 0, 1);
            $data['show_general_manager'] = true;
            $data['show_direktur'] = false;
        } 
		elseif (in_array($user->id_bagian,[34])) 
        { 
            // OPT / MCU
            $paramArea=[
                'in_param' => [
                    'users.id_position' => [
                        4
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];

            $paramBranch=[
                //'branch.branch_id' => $user->branch_id,
                'in_param' => [
                    'users.id_position' => [
                        7,19
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];

          
            
            
            $data['area_manager'] = $this->m_cuti_api->get_karyawan($paramArea, 0, 0, 1);
            $data['show_area_manager'] = true;
            $data['manager'] = $this->m_cuti_api->get_karyawan($paramBranch, 0, 0, 1);
            $data['show_branch_manager'] = false;
            $data['show_direktur'] = false;
            $data['show_general_manager'] = false;
            $data['show_manager'] = true;

        } 
        elseif (in_array($user->id_position,[2,3,6,14,16,12,23,22,15])) 
        { 
            // KELOMPOK 1 = ADM , ANALIS , PERAWAT , SECURITY , P. KEBERSIHAN , KURIR , EKSPEDISI , RADIOGRAPHER
            $paramArea=[
                'in_param' => [
                    'users.id_position' => [
                        4
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];

            $paramBranch=[
                'branch.branch_id' => $user->branch_id,
                'in_param' => [
                    'users.id_position' => [
                        7,19
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            
            
            $data['area_manager'] = $this->m_cuti_api->get_karyawan($paramArea, 0, 0, 1);
            $data['show_area_manager'] = true;
            $data['branch_manager'] = $this->m_cuti_api->get_karyawan($paramBranch, 0, 0, 1);
            $data['show_branch_manager'] = true;
            $data['show_direktur'] = false;
            $data['show_general_manager'] = false;
            $data['show_manager'] = false;

        } 

        else {
            $paramBranch=[
                'branch.branch_id' => $user->branch_id,
                'in_param' => [
                    'users.id_position' => [
                        7
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            $paramArea=[
                'branch.branch_id' => $user->branch_id,
                'in_param' => [
                    'users.id_position' => [
                        4
                    ],
                    'STATUS' => [
                        'TETAP',
                        'KONTRAK'
                    ],
                ]
            ];
            $data['branch_manager'] = $this->m_cuti_api->get_karyawan($paramBranch, 0, 0, 1);
            $data['show_branch_manager'] = true;
            $data['show_manager'] = false;
            $data['area_manager'] = $this->m_cuti_api->get_karyawan($paramArea, 0, 0, 1);
            $data['show_area_manager'] = true;
            $data['show_general_manager'] = false;
            $data['show_direktur'] = false;
        }
        return $this->response($data);
    }


    public function listIzin(){

        $search = $this->input->post('search');
        $user = $this->input->post('user');
        $bagian_id = $this->input->post('bagian_id');
        $tanggal = $this->input->post('tanggal');
        $status = $this->input->post('status');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $type = $this->input->post('type');
		$filter = $this->input->post('filter');

        $user_id = $this->input->post('user_id');

        $args = [
            'where' => ['1'=>'1'],
			'like' => ['1'=>'1'],
            // 'order' => [ ['_.id', 'desc'] ]
        ];

        $hr = false;

        
            $list_hr = [];
            $users_hr = $this->user_model->many_user(['where_in' => ['bg.id' => '22']]);
            if ($users_hr):
                foreach ($users_hr as $key => $usr):
                    $list_hr[] = $usr['id_user'];
                endforeach;
            endif;

            if (in_array($user_id, $list_hr)):
                $hr = true;
            endif;

        
        if (isset($user) && !empty($user)):
            $args['where']['usr.id_user'] = $user;
        endif;

        if (isset($tanggal) && !empty($tanggal)):
            $args['where']['tgl_request'] = date('Y-m-d',strtotime($tanggal));
        endif;

        if (isset($bagian_id) && !empty($bagian_id)):
            $user_bagian = [];
            $users = $this->user_model->many_user(['where_in' => ['bg.id' => $bagian_id]]);
            if ($users):
                foreach ($users as $key => $usr):
                    $user_bagian[] = $usr['id_user'];
                endforeach;
            endif;
            $args['where_in']['id_user'] = $user_bagian;
        endif;


        if (!empty($search)) {
            $args = ['like' => ['usr.name' => $search, 'usr.last_name' => $search, 'branch_name' => $search, 'tgl_request' =>$search]];
        } 
        else{
            $args['like'] = ['1'=>'1'];
        }

        if (!empty($filter)) {
            $args['where'] = ['status_izin' => $filter];
        }
        else if(empty($filter) && !empty($search) ){
            $args['where'] = ['1'=>'1'];
        }else{}

        $r = [];
        if($type === 'sent') {
            $r = $this->izin->getSent($this->current_user, $args);
        } else if($type === 'inbox' && ($user_id == "48" || $hr == true)){
            $r = $this->izin->getInboxHR($this->current_user(), $args);
        } else if($type === 'inbox' && ($user_id != "48" || $hr == false)){
            $r = $this->izin->getInbox($this->current_user(), $args);
        } else if($type === 'archived'){
            $r = $this->izin->getArchived($this->current_user, $args);
        } else if($type === 'rekap' && ($user_id == "48" || $hr == true)){
            $r = $this->izin->getRekapHR($this->current_user, $args);
        } else if($type === 'rekap' && ($user_id != "48" || $hr == false)){
            $r = $this->izin->getRekap($this->current_user, $args);
        } else {}
        return $this->response($r);
    }

    public function getReceiver(){

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $search = $this->input->post('search');
        $id = $this->input->post('id');

        $args = [
            'where' => [],
            // 'order' => [ ['_.id', 'desc'] ]
        ];

        if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
            $args['limit'] = [ $start, $length ];
        }

        $r = $this->izin->getReceiver($id);
        return $this->response($r);
    }

    public function markAsRead(){

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $search = $this->input->post('search');
        $id = $this->input->post('id');

        $args = [
            'where' => [],
            // 'order' => [ ['_.id', 'desc'] ]
        ];

        if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
            $args['limit'] = [ $start, $length ];
        }

        $r = $this->izin->updateIzinAsRead($this->current_user, $id);
        return $this->response($r);
    }

    public function approval($id){
        if ($id):
            $approve = $this->izin->approveIzin($this->current_user(),$id);
            if ($approve):
                $getIzin = $this->izin->getIzinReceiverDetail($this->current_user,$id);
                $user = $this->current_user();
                if ($getIzin):
                    if ($user->id_bagian == 22):
                        $this->izin->UpdateStatusIzin($id);
						
						$dataIzin = $this->izin->get_data_izin(['id_izin' => $id],0,0,1);
						$receiver_ids = [];	
						if(count($dataIzin)>0){
							$receiver_ids[] = $dataIzin[0]['id_user'];
						}
						
                        $data_parse = [
                            "id" => $id,
                            "notif_type" => "reqizin",
                            "id_izin" => $id
                        ];
						
						$sender = $this->user_model->one_user(['where' => ['_.id_user' => $this->current_user]]);
						$sender_name = '';
						$position_name = '';
						$branch_name = '';
						if($sender != null) {
							$sender_name = $sender['name'];
							$position_name = $sender['position_name'];
							$branch_name = $sender['branch_name'];
						}
						$description = $sender_name.' ('.$position_name.') mensetujui permintaan izin';
						
						
						//JIKA REQUEST CUTI PENANGGUNG JAWAB DI ACC MAKA KIRIM NOTIF KE AM
						$karyawan = $this->m_izin->get_karyawan(['id_user' => $dataIzin[0]['id_user']],0,0,1);
						if(in_array($karyawan[0]['id_position'],[13])){
							$paramArea=[
								//'branch.branch_id' => $dataIzin[0]['branch_id'],
								'in_param' => [
									'users.id_position' => [
										4
									],
									'STATUS' => [
										'TETAP',
										'KONTRAK'
									],
								]
							];
							$area_manager = $this->m_izin->get_karyawan($paramArea, 0, 0, 1);
							if(is_array($area_manager)){
								foreach($area_manager as $v){
									$receiver_ids[] = $v['id_user'];
									
									$data_receiver = [
										'id_receiver' => $v['id_user'],
										'id_izin' => $id_izin,
										'is_hrd' => 0,
									];
									$this->m_izin->save_izin_receiver($data_receiver);
								}
							}
						}
						//JIKA REQUEST CUTI PENANGGUNG JAWAB DI ACC MAKA KIRIM NOTIF KE AM
						
                        $this->load->library("Notify_lib");
                        $nl = new Notify_lib();
                        $nl->send(
                            'Permintaan Izin Sudah Disetujui', //title
                            $description, //'Request Izin Sudah Disetujui',        //message
                            $this->current_user,     //sender
                            //$getIzin[0],     //list receiver
                            $receiver_ids,             //list receiver
                            $data_parse,        //attachment
                            "reqizin",          //type notifikasi
                            true               //kirim ke onesignal
                        );
						
                        
                    else:
					
						$receiver_ids = [];		
						$cek =  $this->m_izin->getIzinReceiver(['id_izin'=>$id,'is_acc' => 0,'is_hrd' => 0]); 
						if(count($cek)>0)
						{
							$x = 0;
							foreach($cek as $val){
								if($x==0){
									$receiver_ids[] = $val['id_receiver'];							
									
									$paramx['izin_receiver.id_izin'] = $id;
									$paramx['izin_receiver.id_receiver'] = $val['id_receiver'];
									$this->m_izin->update_izin_receiver([
										'is_asigned' => 1
									],$paramx);
								}
								$x++;
							}
						}
						else
						{
							$get_hrd = $this->m_izin->getIzinReceiver([
								'izin_receiver.id_izin' => $id,
								'is_hrd' => 1,
								'id_receiver <>' => $this->current_user
							]);
							
							foreach($get_hrd as $val){
								$id_hrd = isset($val['id_receiver'])?$val['id_receiver']:0;
								$receiver_ids[] = $id_hrd;
								
								$paramx['izin_receiver.id_izin'] = $id;
								$paramx['izin_receiver.id_receiver'] = $id_hrd;
								$this->m_izin->update_izin_receiver([
									'is_asigned' => 1
								],$paramx);
								
							}	
						}
					
                        $data_parse = [
                            "id" => $id,
                            "notif_type" => "reqizin",
                            "id_izin" => $id
                        ];
						
						$dataCutix = $this->izin->get_data_izin(['id_izin' => $id],0,0,1);
						$req_sender_name = '';
						$req_position_name = '';
						$req_branch_name = '';
						if(count($dataCutix)>0){
							$req_sender = $this->user_model->one_user(['where' => ['_.id_user' => $dataCutix[0]['id_user']]]);
							if($req_sender != null) {
								$req_sender_name = $req_sender['name'];
								$req_position_name = $req_sender['position_name'];
								$req_branch_name = $req_sender['branch_name'];
							}
						}
						$description = $req_sender_name. ' ('.$req_position_name.') Mengajukan Izin';

                        $this->load->library("Notify_lib");
                        $nl = new Notify_lib();
                        $nl->send(
                            'Permintaan Izin', //title
                            $description, //'Request Izin',        //message
                            $getIzin[0],     //sender
                            //$getIzin[1],             //list receiver
                            $receiver_ids,             //list receiver
                            $data_parse,        //attachment
                            "reqizin",          //type notifikasi
                            true               //kirim ke onesignal
                        );
						
                       
                    endif;
                endif;
                return $this->response($getIzin);
            endif;
        else:
            return $this->response(false);
        endif;
    }

    // stay
    public function rejected($id){
        if ($id){
            $reject = $this->izin->rejectIzin($this->current_user(),$id, $this->input->post('alasan'));
			
			$user = $this->current_user();
			$sender = $this->user_model->one_user(['where' => ['_.id_user' => $user->id_user]]);
			$sender_name = '';
			$position_name = '';
			$branch_name = '';
			if($sender != null) {
				$sender_name = $sender['name'];
				$position_name = $sender['position_name'];
				$branch_name = $sender['branch_name'];
			}
			$receiver_ids = [];		
			$dataCuti = $this->izin->get_data_izin(['id_izin' => $id],0,0,1);
			$receiver_ids[] = (isset($dataCuti[0]['id_user'])?$dataCuti[0]['id_user']:'');
			$description = $sender_name.' ('.$position_name.') menolak permintaan izin';
			
			$data_parse = [
				"id" => $id,
				"notif_type" => "reqizin",
				"id_izin" => $id
			];

            $this->load->library("Notify_lib");
            $nl = new Notify_lib();
            $nl->send(
                "Permintaan Izin", //title
                $description,        //message
                $user->id_user,     //sender
                $receiver_ids,             //list receiver
                $data_parse,        //attachment
                "reqizin",          //type notifikasi
                true               //kirim ke onesignal
            );

            $id_cuti = $this->input->post('id_cuti');

            if (isset($id_cuti) && !empty($id_cuti)) {
                $paramv['id_cuti'] = $id_cuti;
                $dataCuti = $this->m_cuti->get_data_last_cuti_new($paramv,0,0,1,$this->input->post('user_id'));
    
                    $paramx['users.id_user'] = $dataCuti[0]['id_user'];
                    $user = $this->m_cuti->get_karyawan($paramx,0,0,1)[0];
                    
                    $update_data_user = [
                        'CUTI' => $user['CUTI']+$dataCuti[0]['request_cuti']
                    ];
                    $this->m_mis->update_data(['id_user'=>$dataCuti[0]['id_user']],$update_data_user,'users');
                 
                    $update_data_cuti = [
                        'izin_reject' => 1
                    ];
                    // buat fungsi yang mengubah status cuti (berdasarkan cuti_id di izin menjadi apapun) setelah izin ditolak
                    $this->m_mis->update_data(['id_cuti'=>$id_cuti],$update_data_cuti,'cuti');
            }
 
			
            return $this->response($reject);
        }else{
            return $this->response(false);
        }
    }

    public function current_user()
    {
        $query = $this->db->query("Select a.*,b.branch_name,c.name_position,a.CUTI as total_jatah_cuti
            from users a
            join branch b on (a.branch_id = b.branch_id)
            join position c on (a.id_position = c.id_position)
            where id_user='$this->current_user'
        ");
        return $query->first_row();
    }

    public function save(){
        $input = $this->input->post();
        $user = $this->current_user();
        $id_izin = 0;
        $data = $this->izin->get_last_id();
        $data_cuti = $this->m_cuti_api->get_last_id_2();
        $docNum = isset($data[0]['id_izin'])?($data[0]['id_izin']):0;
        $new_number = $docNum+1;

        $izin_data = [
            'creator_id' => $this->current_user,
            'branch_id' => $this->current_user()->branch_id,
            'id_user' =>  $this->current_user,
            'tgl_request' => date('Y-m-d'),
            'tgl_ambil_izin' => $input['tgl_ambil_izin'],
            'jml_hari' => $input['jml_hari'],
            'status_at_izin' => $this->current_user()->STATUS,
            'no_dokumen' => $new_number,
            'jam_keluar' => $input['jam_keluar'],
            'jam_kembali' => $input['jam_kembali'],
            'jml_jam' => (intval($input['jam_keluar'])-intval($input['jam_kembali'])),
            'status_izin' => $input['duration'],
            'keperluan' => $input['keperluan'],
            'alasan' => $input['alasan'],
            'request_izin' => $input['jml_hari'],
            // 'cuti_id' => $data_cuti->id_cuti
            'cuti_id' => '0'
        ];
       //  if ($this->request_cuti($input,1) != 0) {
         //    $izin_data['cuti_id'] = $this->request_cuti($input,1);
        // }


        $id_izin = $this->izin->save_izin($izin_data);
        $receiver = [
            'branch_manager_id' => $input['branch_manager_id'],
            'manager_id' => $input['manager_id'],
            'area_manager_id' => $input['area_manager_id'],
            'general_manager_id' => $input['general_manager_id'],
            'direktur_id' => $input['direktur_id']
        ];

        // if (empty($_FILES)) {
        //      // potong cuti
        //      $paramx['users.id_user'] =  $input['user_id'];
        //      $user = $this->m_cuti_api->get_karyawan($paramx,0,0,1)[0];
             
        //      $update_data_user = [
        //          'CUTI' => $user['CUTI']-$input['jml_hari']
        //      ];
        //     $this->m_mis->update_data(['id_user'=>$input['user_id']],$update_data_user,'users');
        // }


        if ($input['keperluan'] == '1'  && $input['duration'] === 'tidak_masuk' && empty($_FILES)) {
            // potong cuti
            $paramx['users.id_user'] =  $input['user_id'];
            $user = $this->m_cuti_api->get_karyawan($paramx,0,0,1)[0];
            
            $update_data_user = [
                'CUTI' => $user['CUTI']-$input['jml_hari']
            ];
           $this->m_mis->update_data(['id_user'=>$input['user_id']],$update_data_user,'users');
       

           $datax = $this->m_cuti->get_last_id();
						$docNum = isset($datax[0]['id_cuti'])?($datax[0]['id_cuti']):0;
						$new_number_ = $docNum+1;

           $karyawan =  $this->db->query("select CUTI as jatah_cuti, TGLMASUK as tglmasuk, cuti_keseluruhan, STATUS, last_reset_cuti
							from users a
							where id_user='".$this->current_user."'
						")->first_row();

           $cuti_diambil = $this->getLastCuti($this->current_user, $karyawan->STATUS, $karyawan->last_reset_cuti);

           $store_data = [
            'creator_id' => $this->current_user,
            'branch_id' => $this->current_user()->branch_id,
            'id_user' => $this->current_user,
            'tgl_request' => date('Y-m-d'),
            'tgl_ambil_cuti' => $input['tgl_ambil_izin'],
            'jml_hari' => $input['jml_hari'],
            'status_at_cuti' => $this->current_user()->STATUS,
            'no_dokumen' => $new_number_,
            'no_telp' => '',
            'jatah_cuti' => $karyawan->cuti_keseluruhan,
            'jatah_diambil' => $cuti_diambil + $input['jml_hari'],
            'sisa_jatah' => $karyawan->cuti_keseluruhan - ($cuti_diambil + $input['jml_hari']),
            'request_cuti' => $input['jml_hari'],
            'sisa_cuti' => $karyawan->cuti_keseluruhan - ($cuti_diambil + $input['jml_hari']),
            'status_cuti' => 'tahunan',
            'status_pengajuan' => 'Disetujui',
            'deleted' => '1',
            'izin' => '1'
        ];

        $id_cuti = $this->m_cuti->save_cuti($store_data);

        $update_data_izin = [
            'cuti_id' => $id_cuti
        ];

        $this->m_mis->update_data(['id_izin'=>$id_izin],$update_data_izin,'izin');
       
        }

        // if($input['keperluan'] == '1' && $input['duration'] === 'tidak_masuk' && empty($_FILES)):
        //     if ($id_izin):
        //         $resp = $this->request_cuti(array_merge($izin_data,$receiver),1);
        //         if ($resp['id_cuti']>0) {
        //             $this->izin->update_izin(['cuti_id' => $resp['id_cuti']],['id_izin' => $id_izin]);
        //         }
        //     endif;
        // endif;

        if ($input['keperluan'] == '4'  && $input['duration'] === 'tidak_masuk') {
            // potong cuti
            $paramx['users.id_user'] =  $input['user_id'];
            $user = $this->m_cuti_api->get_karyawan($paramx,0,0,1)[0];
            
            $update_data_user = [
                'CUTI' => $user['CUTI']-$input['jml_hari']
            ];
           $this->m_mis->update_data(['id_user'=>$input['user_id']],$update_data_user,'users');
       
           
           $datax = $this->m_cuti->get_last_id();
						$docNum = isset($datax[0]['id_cuti'])?($datax[0]['id_cuti']):0;
						$new_number_ = $docNum+1;

           $karyawan =  $this->db->query("select CUTI as jatah_cuti, TGLMASUK as tglmasuk, cuti_keseluruhan, STATUS, last_reset_cuti
							from users a
							where id_user='".$this->current_user."'
						")->first_row();

           $cuti_diambil = $this->getLastCuti($this->current_user, $karyawan->STATUS, $karyawan->last_reset_cuti);

           $store_data = [
            'creator_id' => $this->current_user,
            'branch_id' => $this->current_user()->branch_id,
            'id_user' => $this->current_user,
            'tgl_request' => date('Y-m-d'),
            'tgl_ambil_cuti' => $input['tgl_ambil_izin'],
            'jml_hari' => $input['jml_hari'],
            'status_at_cuti' => $this->current_user()->STATUS,
            'no_dokumen' => $new_number_,
            'no_telp' => '',
            'jatah_cuti' => $karyawan->cuti_keseluruhan,
            'jatah_diambil' => $cuti_diambil + $input['jml_hari'],
            'sisa_jatah' => $karyawan->cuti_keseluruhan - ($cuti_diambil + $input['jml_hari']),
            'request_cuti' => $input['jml_hari'],
            'sisa_cuti' => $karyawan->cuti_keseluruhan - ($cuti_diambil + $input['jml_hari']),
            'status_cuti' => 'tahunan',
            'status_pengajuan' => 'Disetujui',
            'deleted' => '1',
            'izin' => '1'
        ];

        $id_cuti = $this->m_cuti->save_cuti($store_data);

        $update_data_izin = [
            'cuti_id' => $id_cuti
        ];

        $this->m_mis->update_data(['id_izin'=>$id_izin],$update_data_izin,'izin');
       
       
        }

        // if($input['keperluan'] == '4' && $input['duration'] === 'tidak_masuk'):
        //     if ($id_izin):
        //         $resp = $this->request_cuti(array_merge($izin_data,$receiver),1);
        //         if ($resp['id_cuti']>0) {
        //             $this->izin->update_izin(['cuti_id' => $resp['id_cuti']],['id_izin' => $id_izin]);
        //         }
        //     endif;
        // endif;

        

        // if ($input['duration'] === 'tidak_masuk' && !empty($input['jam_keluar'])):
        //     if ($id_izin):
        //         $resp = $this->request_cuti(array_merge($izin_data,$receiver),1);
        //         if ($resp['id_cuti']>0):
        //             $this->izin->update_izin(['cuti_id' => $resp['id_cuti']],['id_izin' => $id_izin]);
        //         endif;
        //     endif;
        // endif;

        

        $receiverNotif = [];
        $receivers = [];
        $receiverHr = [];
        if (isset($input['branch_manager_id']) && !empty($input['branch_manager_id'])):
            if (count($receiverNotif) == 0):
                array_push($receiverNotif, $input['branch_manager_id']);
            endif;
            array_push($receivers,$input['branch_manager_id']);
        endif;
        if (isset($input['manager_id']) && !empty($input['manager_id'])){
            if (count($receiverNotif) == 0):
                array_push($receiverNotif,$input['manager_id']);
            endif;
            array_push($receivers,$input['manager_id']);
        }
        if (isset($input['area_manager_id']) && !empty($input['area_manager_id'])){
            if (count($receiverNotif) == 0):
                array_push($receiverNotif,$input['area_manager_id']);
            endif;
            array_push($receivers,$input['area_manager_id']);
        }
        if (isset($input['general_manager_id']) && !empty($input['general_manager_id'])){
            if (count($receiverNotif) == 0):
                array_push($receiverNotif,$input['general_manager_id']);
            endif;
            array_push($receivers,$input['general_manager_id']);
        }
        if (isset($input['direktur_id']) && !empty($input['direktur_id'])){
            if (count($receiverNotif) == 0):
                array_push($receiverNotif,$input['direktur_id']);
            endif;
            array_push($receivers,$input['direktur_id']);
        }
        $paramHR=[
            'in_param' => [
                'users.id_bagian' => [
                    22
                ],
                'STATUS' => [
                    'TETAP',
                    'KONTRAK'
                ],
            ]
        ];
        $dataHr = $this->izin->get_karyawan($paramHR, 0, 0, 1);
        if ($dataHr):
            foreach($dataHr as $hr):
                array_push($receivers,$hr['id_user']);
                array_push($receiverHr,$hr['id_user']);
            endforeach;
        endif;
        // if ($user->name_position == "OPT") {
        //     /*OPT*/
        //     $receiver[0] = [
        //         'id_receiver' => $input['branch_manager_id'],
        //         'id_izin' => $id_izin
        //     ];
        //     $receiver[1] = [
        //         'id_receiver' => $input['manager_id'],
        //         'id_izin' => $id_izin
        //     ];
        //     $receiver[2] = [
        //         'id_receiver' => 68,
        //         'id_izin' => $id_izin,
        //         'is_hrd' => 1
        //     ];
        // } elseif ($user->name_position == "GENERAL MANAGER") {
        //     //GENERAL MANAGER
        //     $receiver[0] = [
        //         'id_receiver' => $input['direktur_id'],
        //         'id_izin' => $id_izin
        //     ];
        //     $receiver[1] = [
        //         'id_receiver' => 68,
        //         'id_izin' => $id_izin,
        //         'is_hrd' => 1
        //     ];
        // } else {
        //     $receiver[0] = [
        //         'id_receiver' => $input['manager_id'],
        //         'id_izin' => $id_izin
        //     ];
        //     $receiver[2] = [
        //         'id_receiver' => 68,
        //         'id_izin' => $id_izin,
        //         'is_hrd' => 1
        //     ];
        // }
		$x = 0;
        foreach ($receivers as $value) {
            $data_receiver['id_receiver'] = $value;
            $data_receiver['id_izin'] = $id_izin;
            if (in_array($value,$receiverHr)):
                $data_receiver['is_hrd'] = 1;
            endif;
			
			if($x == 0){
				$data_receiver['is_asigned'] = 1;
			}
			
            $this->izin->save_izin_receiver($data_receiver);
            // $receiver_ids[] = (isset($value['is_hrd'])&&$value['is_hrd']==1)?$data_receiver['id_receiver']:$data_receiver['id_receiver'];
			$x++;
        }

        $data_parse = [
            "id" => $id_izin,
            "notif_type" => "reqizin",
            "id_izin" => $id_izin
        ];
		
		$user = $this->m_izin->get_karyawan(['id_user' => $this->current_user],0,0,1);
		$nama_user = $user[0]['name'];
		$position_name = $user[0]['name_position'];
		
		$description = $nama_user. ' ('.$position_name.') Mengajukan Izin';

        $this->load->library("Notify_lib");
            $nl = new Notify_lib();
            $nl->send(
            'Permintaan Izin Baru', //title
            $description, //'Request Izin',        //message
            $this->current_user,     //sender
            $receiverNotif,             //list receiver
            $data_parse,        //attachment
            "reqizin",          //type notifikasi
            true               //kirim ke onesignal
        );
		

        $this->response([
          'msg' => "sukses",
          // 'data'=> $izin_data
          'data'=> $receiverNotif
        ]);
    }

    public function detail(){
        $id = $this->input->post('id');
        $detailIzin = $this->izin->detail($id, $this->current_user);
        return $this->response($detailIzin);
    }

    public function keperluan_izin($direct=0){
        $list = [
            1 => 'Izin Sakit',
            2 => 'Izin Pernikahan Karyawan Sendiri : 3 hari kerja',
            3 => 'Izin Anggota keluarga meninggal ( Kakak/Adik ) dan anggota keluarga yang tinggal satu rumah : 1 hari kerja',
            5 => 'Izin Pernikahan Anak Karyawan : 2 hari kerja',
            6 => 'Izin Khitanan / Pembabtisan anak karyawan : 2 hari kerja',
            7 => 'Izin Suami /istri/anak/orang tua/mertua karyawan Meninggal Dunia : 2 hari kerja',
            8 => 'Izin Istri Karyawan Melahirkan : 2 hari kerja',
            9 => 'Izin Kepolisian',
            4 => 'DLL'
        ];

        if ($direct) {
            return $list;
        }else{
            return $this->response($list);
        }
    }

    public function jatah_hari($direct=0){
        /* acuan jml hari berdasarkan keperluan izin */
        $list = [
            1 => 1,
            2 => 3,
            3 => 1,
            5 => 2,
            6 => 2,
            7 => 2,
            8 => 2,
            4 => 0 //tidak ditentukan
        ];

        if ($direct) {
            return $list;
        }else{
            return $this->response($list);
        }

    }

    public function request_cuti($data=[],$direct=0){
        /*
            - pengecekan izin apakah sesuai dengan jatah hari yang sudah ditentukan atau tidak.
            - jika jml hari ambil lebih dari jatah maka kurangi masukkan ke rquest cuti
            - accomodate hit as an API or method
        */
        if ($direct) {
            $parse_data = $data;
        }else{
            $parse_data = $this->input->post();
        }

        $jatah = $this->jatah_hari(1)[$parse_data['keperluan']];
        $ambil = $parse_data['jml_hari'];
        $tanggal_izin = $parse_data['tgl_ambil_izin'];

        /*olah tanggal cuti*/
        $tmp_ = explode(',',$tanggal_izin);
        $tgl_ambil_cuti = '';
        sort($tmp_);
        for ($i=0; $i < $jatah ; $i++) {
            unset($tmp_[$i]);
        }

        $i=0;
        foreach ($tmp_ as $key => $value) {
            if ($i == 0) {
                $tgl_ambil_cuti .= $value;
            }else{
                $tgl_ambil_cuti .= ', '.$value;
            }
            $i++;
        }
        /**/

        if ($ambil > $jatah) {
            /* save data ke request cuti */
            /* duplikasi dari restapi/cuti/save */
            $post = [
                'id_user' => $this->current_user,
                'tgl_ambil_cuti' => $tgl_ambil_cuti,
                'tgl_request' => $parse_data['tgl_request'],
                'jml_hari' =>count($tmp_),
                'jenis_cuti' => 'tahunan',
                'branch_manager_id' => $parse_data['branch_manager_id'],
                'manager_id' => $parse_data['manager_id'],
                'area_manager_id' => $parse_data['area_manager_id'],
                'general_manager_id' => $parse_data['general_manager_id'],
                'direktur_id' => $parse_data['direktur_id'],
                'no_telp' => '',
            ];

            /*------------------------------------------------*/
                $input = $post;
                $user = $this->current_user();
                $id_cuti = 0;
                $data = $this->m_cuti_api->get_last_id();
                $docNum = isset($data[0]['id_cuti'])?($data[0]['id_cuti']):0;
                $new_number = $docNum+1;
				
				$karyawan =  $this->db->query("select CUTI as jatah_cuti, TGLMASUK as tglmasuk, cuti_keseluruhan, STATUS, last_reset_cuti
					from users a
					where id_user='$this->current_user'
				")->first_row();

				$cuti_diambil = $this->getLastCuti($this->current_user, 'view', $karyawan->STATUS, $karyawan->last_reset_cuti);
				
                $cuti_data = [
                  'creator_id' => $this->current_user,
                  'branch_id' => $this->current_user()->branch_id,
                  'id_user' =>  $this->current_user,
                  'tgl_request' => date('Y-m-d'),
                  'tgl_ambil_cuti' => $input['tgl_ambil_cuti'],
                  'jml_hari' => $input['jml_hari'],
                  'status_at_cuti' => 'izin',
                  'no_dokumen' => $new_number,
                  'no_telp' => $input['no_telp'],
                  'jatah_cuti' => $this->current_user()->cuti_keseluruhan,
                  'jatah_diambil' => $cuti_diambil + $input['jml_hari'],
                  'sisa_jatah' => $input['jenis_cuti'] === 'melahirkan' ? $this->current_user()->cuti_keseluruhan - $cuti_diambil : $this->current_user()->cuti_keseluruhan - ($cuti_diambil + $input['jml_hari']), // sisa jatah skrg
                  'request_cuti' => $input['jenis_cuti'] === 'melahirkan'? 0 : $input['jml_hari'],
                  'sisa_cuti' => $input['jenis_cuti'] === 'melahirkan' ? $this->current_user()->cuti_keseluruhan - $cuti_diambil : $this->current_user()->cuti_keseluruhan - ($cuti_diambil + $input['jml_hari']), // sisa jatah skrg setelah dikurangi request
                  'status_cuti' => $input['jenis_cuti'],
                  'izin'=>'1'
                ];

                $id_cuti = $this->m_cuti_api->save_cuti($cuti_data);

                $receiverNotif = [];
                $receivers = [];
                $receiverHr = [];
                if (isset($input['branch_manager_id']) && !empty($input['branch_manager_id'])):
                    if (count($receiverNotif) == 0):
                        array_push($receiverNotif, $input['branch_manager_id']);
                    endif;
                    array_push($receivers,$input['branch_manager_id']);
                endif;
                if (isset($input['manager_id']) && !empty($input['manager_id'])){
                    if (count($receiverNotif) == 0):
                        array_push($receiverNotif,$input['manager_id']);
                    endif;
                    array_push($receivers,$input['manager_id']);
                }
                if (isset($input['area_manager_id']) && !empty($input['area_manager_id'])){
                    if (count($receiverNotif) == 0):
                        array_push($receiverNotif,$input['area_manager_id']);
                    endif;
                    array_push($receivers,$input['area_manager_id']);
                }
                if (isset($input['general_manager_id']) && !empty($input['general_manager_id'])){
                    if (count($receiverNotif) == 0):
                        array_push($receiverNotif,$input['general_manager_id']);
                    endif;
                    array_push($receivers,$input['general_manager_id']);
                }
                if (isset($input['direktur_id']) && !empty($input['direktur_id'])){
                    if (count($receiverNotif) == 0):
                        array_push($receiverNotif,$input['direktur_id']);
                    endif;
                    array_push($receivers,$input['direktur_id']);
                }
                $paramHR=[
                    'in_param' => [
                        'users.id_bagian' => [
                            22
                        ],
                        'STATUS' => [
                            'TETAP',
                            'KONTRAK'
                        ],
                    ]
                ];
                $dataHr = $this->m_cuti_api->get_karyawan($paramHR, 0, 0, 1);
                if ($dataHr):
                    foreach($dataHr as $hr):
                        array_push($receivers,$hr['id_user']);
                        array_push($receiverHr,$hr['id_user']);
                    endforeach;
                endif;

                foreach ($receivers as $value) {
                    $data_receiver['id_receiver'] = $value;
                    $data_receiver['id_cuti'] = $id_cuti;
                    if (in_array($value,$receiverHr)):
                        $data_receiver['is_hrd'] = 1;
                    endif;
                    $this->m_cuti_api->save_cuti_receiver($data_receiver);

                }
                $data_parse = [
                    "id" => $id_cuti,
                    "notif_type" => "reqcuti",
                    "id_cuti" => $id_cuti
                ];

            $this->load->library("Notify_lib");
            $nl = new Notify_lib();
            $nl->send(
            'Request Cuti', //title
            'Request Cuti',        //message
            $this->current_user,     //sender
            $receiverNotif,             //list receiver
            $data_parse,        //attachment
            "reqcuti",          //type notifikasi
            true               //kirim ke onesignal
            );

                // $this->response([
                //   'msg' => "sukses",
                //   // 'data'=> $cuti_data
                //   'data'=> $receiverNotif
                // ]);
            /*----------------------------------------------------------*/


            $rsp = [
                'msg' => 'request cuti berhsail ditambahkan.',
                'code' => 200,
                'id_cuti' => $id_cuti
            ];
        }else{
            $rsp = [
                'msg' => 'tidak perlu request cuti.',
                'code' => 200,
                'id_cuti' => 0
            ];

        }

        if ($direct) {
            return $rsp;
        }else{
            return $this->response($rsp);
        }

    }

    public function getLastCuti($id_user=0, $type = 'view',$status_karyawan='',$last_reset_cuti='')
    {
        $param['users.id_user'] = $id_user;
		
		if($status_karyawan=='TETAP'){
			$param['year(tgl_request)'] = date('Y');
		}
		else if($status_karyawan=='KONTRAK'){
			$param['(tgl_request>="'.$last_reset_cuti.'")'] = null;
		}
		
        $param['(cuti.status_pengajuan not like "%tolak%")'] = null;
		
		$data = $this->m_cuti_api->get_data_cuti($param, 0, 0, 1);

        $jml_cuti_lalu = 0;
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $jml_cuti_lalu = $jml_cuti_lalu+$value['request_cuti'];
            }
        }
        if ($type === 'json'):
            return $this->response($this->current_user()->CUTI);
        else:
            return $jml_cuti_lalu;
        endif;
    }

    public function response($data, $code = 200){
        $json =   json_encode($data);

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header($code)
            ->set_output($json);
    }

    function count_izin(){
        $search = $this->input->post('search');
        $user = $this->input->post('user');
        $bagian_id = $this->input->post('bagian_id');
        $tanggal = $this->input->post('tanggal');
        $status = $this->input->post('status');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $type = $this->input->post('type');
		$filter = $this->input->post('filter');
        $user_id = $this->input->post('user_id');

        // if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
        //     $args['limit'] = [ $start, $length ];
        // }

        $args = [
            'where' => ['1'=>'1'],
			'like' => ['1'=>'1'],
            // 'order' => [ ['_.id', 'desc'] ]
        ];

        if (isset($user) && !empty($user)):
            $args['where']['usr.id_user'] = $user;
        endif;

        if (isset($tanggal) && !empty($tanggal)):
            $args['where']['tgl_request'] = date('Y-m-d',strtotime($tanggal));
        endif;

        if (isset($bagian_id) && !empty($bagian_id)):
            $user_bagian = [];
            $users = $this->user_model->many_user(['where_in' => ['bg.id' => $bagian_id]]);
            if ($users):
                foreach ($users as $key => $usr):
                    $user_bagian[] = $usr['id_user'];
                endforeach;
            endif;
            $args['where_in']['usr.id_user'] = $user_bagian;
        endif;

        if (!empty($search)) {
            $args = ['like' => ['usr.name' => $search, 'usr.last_name' => $search, 'branch_name' => $search, 'tgl_request' =>$search]];
        } 
        else{
            $args['like'] = ['1'=>'1'];
        }

        if (!empty($filter)) {
            $args['where'] = ['status_izin' => $filter];
        }
        else if(empty($filter) && !empty($search) ){
            $args['where'] = ['1'=>'1'];
        }else{}

        $hr = false;

        $list_hr = [];
        $users_hr = $this->user_model->many_user(['where_in' => ['bg.id' => '22']]);
        if ($users_hr):
            foreach ($users_hr as $key => $usr):
                $list_hr[] = $usr['id_user'];
            endforeach;
        endif;

        if (in_array($user_id, $list_hr)):
            $hr = true;
        endif;


    if($user_id == "48" || $hr == true){
        $r1 = $this->izin->getInboxHR($this->current_user(), $args);
        $r4 = $this->izin->getRekapHR($this->current_user, $args);
    }else{
        $r1 = $this->izin->getInbox($this->current_user(), $args);
        $r4 = $this->izin->getRekap($this->current_user, $args);
    }
   
    $r2 = $this->izin->getSent($this->current_user, $args);
    $r3 = $this->izin->getArchived($this->current_user, $args);

        $r = [
            'received' => count($r1),
            'sent' => count($r2) ,
            'archived' => count($r3),
            'rekap' => count($r4)
        ];
        echo json_encode($r);
    }

}
