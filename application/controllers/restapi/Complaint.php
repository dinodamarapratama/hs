<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class complaint extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        if($this->uri->segment(3) !== "pdf"){
            header('Content-Type: application/json');
        $this->restapikey= $this->config->config['restapikey'];
        if($this->input->post('auth_key') != $this->restapikey) die(/*Silent is gold*/);
        }
        $this->load->model('restapi/complaint_model', 'mcomplaint');
    }

    function index(){
        $sql = "SELECT count(c.id) as jml_complaint, b.branch_name, c.jenis_komplain FROM complaints c LEFT JOIN branch b on b.branch_id=c.branch_id GROUP BY c.branch_id, c.jenis_komplain";

        $res = $this->db->query($sql)->result();

        $sql2 = "SELECT branch_name FROM branch ORDER BY branch_name";
        $res2 = $this->db->query($sql2)->result();

        $label = $total = $warna = [];
        $num = 0;
        $non_teknis = $teknis = $base_branch = [];
        foreach ($res as $key => $value) {
            if ($value->jenis_komplain == 'TEKNIS') {
                $teknis[$value->branch_name] = intval($value->jml_complaint);
            } else {
                $non_teknis[$value->branch_name] = intval($value->jml_complaint);
            }
        }

        $data = [];
        foreach ($res2 as $k => $v) {
            if (!in_array(strtolower($v->branch_name), ['all', 'office'])) {
                $base_branch[] = $v->branch_name;
            }
        }

        foreach ($base_branch as $key => $value) {
            array_push($data,[
                'name' => $value,
                'teknis' => isset($teknis[$value]) ? $teknis[$value] : 0, 
                'nonteknis' => isset($non_teknis[$value]) ? $non_teknis[$value] : 0
            ]);
            $label[] = $value;
            $total[$value]['teknis'] = isset($teknis[$value]) ? $teknis[$value] : 0;
            $warna['teknis'][] = $this->fixedRGB(2);
            $total[$value]['nonteknis'] = isset($non_teknis[$value]) ? $non_teknis[$value] : 0;
            $warna['non-teknis'][] = $this->fixedRGB(10);
        }
        /**/
        echo json_encode([
            'cabang' => $label,
            'data' => $data,
            'warna' => [$this->fixedRGB(2), $this->fixedRGB(10)]
        ]);
    }

    function fixedRGB($index = 0)
    {
        /*set 17 color*/
        $color = [
            "rgba(50, 168, 82)",
            "rgba(196, 50, 45)",
            "rgba(33, 191, 178)",
            "rgba(191, 191, 33)",
            "rgba(235, 56, 154)",
            "rgba(18, 50, 230)",
            "rgba(84, 4, 14)",
            "rgba(227, 160, 45)",
            "rgba(197, 151, 240)",
            "rgba(230, 20, 5)",
            "rgba(252, 157, 3)",
            "rgba(144, 250, 132)",
            "rgba(94, 93, 13)",
            "rgba(94, 54, 13)",
            "rgba(6, 91, 122)",
            "rgba(22, 242, 25)",
            "rgba(107, 11, 16)",
        ];

        return $color[$index];
    }

    function detail()
    {
        $this->load->helper("myhtml");
        $complaint = $this->mcomplaint->detail($this->input->post('id'));
        $complaint->deskripsi = __clean_text($complaint->deskripsi);
        if(isset($complaint->attr)) {
            $attr = $complaint->attr;
            foreach ($attr as $key => $value) {
                $value = html_entity_decode($value);
                // $value = __clean_text($value);
                $attr->{$key} = $value;
            }
            $complaint->attr = $attr;
        }
        echo json_encode($complaint);
    }

    function close(){
        $id = $this->input->post('id');
        $data = [
            'status' => 'CLOSED'
        ];

        $r = $this->mcomplaint->update(['_.id' => $id, 'upper(_.status)' => 'OPEN'], $data);

        echo json_encode($r);

    }

    function pdf($id){
        error_reporting(0);
        require FCPATH . "vendor/autoload.php";
        $complaint = $this->mcomplaint->detail($id);
        $dompdf = new Dompdf\Dompdf();
        $html = $this->load->view("admin/complaints/complaint_detail_pdf3", [
            "complaint" => $complaint
        ], true);
        $dompdf->loadHTML($html);
        $dompdf->render();
        $filename = "complaint-" . $complaint->pengaju_komplain . "-" . $complaint->id_pasien . "-" . $complaint->nomor_registrasi;
        $dompdf->stream($filename . '.pdf',array('Attachment'=>0));
    }

    function pdf_html2pdf($id) {
        // echo __DIR__;
        // echo "<br/>";
        // echo FCPATH;
        require FCPATH . "vendor/autoload.php";

        // use Spipu\Html2Pdf\Html2Pdf;

        $html2pdf = new Spipu\Html2Pdf\Html2Pdf();
        $fontDir = APPPATH . DIRECTORY_SEPARATOR . 'third_party' . DIRECTORY_SEPARATOR . 'fpdf' . DIRECTORY_SEPARATOR . 'font' . DIRECTORY_SEPARATOR;
        $assetFontDir = FCPATH . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'fonts' . DIRECTORY_SEPARATOR;
        $thirdPartyFontDir = APPPATH . DIRECTORY_SEPARATOR . 'third_party' . DIRECTORY_SEPARATOR . 'fonts' . DIRECTORY_SEPARATOR;
        
        $complaint = $this->mcomplaint->detail($id);

        $html = $this->load->view("admin/complaint_detail_pdf2", [
            "complaint" => $complaint
        ], true);
        // echo $html;
        $html2pdf->writeHTML($html);
        $html2pdf->output();
    }

    function pdf_old($id)
    {
        error_reporting(0);
        $complaint = $this->mcomplaint->detail($id);
        $attr = $complaint->attr;
        // echo '<pre>';
        // print_r($complaint);
        // exit();
        $this->load->library('MyPDF');

        $this->load->view("admin/complaint_detail_pdf", [
            "mypdf" => $this->mypdf,
            "complaint" => $complaint,
            "attr" => $attr
        ]);

    }

    public function create()
    {
        $complaint = $this->mcomplaint->create();
        echo json_encode($complaint);
    }

	public function status(){
		$id = $this->input->post('id');
		$value = $this->input->post('value');
		$complaint = $this->mcomplaint->setStatus($id, $value);
		echo json_encode($complaint);
	}

    public function action()
    {
        $complaint = $this->mcomplaint->action();
        echo json_encode($complaint);
    }

    public function mine()
    {
        $this->load->helper("myhtml");
        $complaint = $this->mcomplaint->mine();

        echo json_encode($complaint);
    }

    public function delete()
    {
        $result = $this->mcomplaint->delete();
        echo json_encode($result);
    }

    function update(){
        $id = $this->input->post('id');
        $data = [
            'pengaju_komplain' => $this->input->post('pengaju_komplain'),
            'id_pasien' => $this->input->post('id_pasien'),
            'nama_pasien' => $this->input->post('nama_pasien'),
            'nomor_registrasi' => $this->input->post('nomor_registrasi'),
            'jenis_komplain' => $this->input->post('jenis_komplain'),
            'waktu_komplain' => $this->input->post('waktu_komplain'),
            'waktu_respon' => $this->input->post('waktu_respon'),
        ];

        $attributes = $this->input->post('attributes');
        // print_r($attributes);

        if(strtolower($data['jenis_komplain']) == 'non teknis') {

        }
        else if(strtolower($data['jenis_komplain']) == 'teknis') {

        }

        $data['attributes'] = $attributes;

        $r = $this->mcomplaint->update(['_.id' => $id, 'upper(_.status)' => 'OPEN'], $data);

        if($r['status'] == true) {
            $counter = -1;
            $config = [
                'upload_path' => './uploads/attachments/complaint/',
                'ecnrypt_name' => true,
                'allowed_types' => '*'
            ];

            if(file_exists($config['upload_path']) == false) {
                $old = umask(0);
                mkdir($config['upload_path'], 0777, true);
                umask($old);
            }
            while (true) {
                $counter++;
                $key = 'file_' . $counter;
                if(isset($_FILES[$key]) == false) {
                    break;
                }

                $this->upload->initialize($config);

                if($this->upload->do_upload($key)){
                    $d = $this->upload->data();
                    $desc = $this->input->post('file_desc_' . $counter);
                    $title = $this->input->post('file_title_' . $counter);
                    $this->mcomplaint->add_attachment([
                        'complaint_id' => $id,
                        'title' => $title,
                        'name' => $d['orig_name'],
                        'description' => $desc,
                        'path' => $config['upload_path'] . $d['file_name'],
                        'filetype' => $d['file_type'],
                        'filesize' => $d['file_size']
                    ]);
                }
            }
        }

        echo json_encode($r);
    }

}
