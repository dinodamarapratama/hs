<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

class Notification extends CI_Controller{
	public $model = 'users';
	public function __construct(){
		parent::__construct();
		//cek login
		if ($this->session->userdata('status') != "login") {
			redirect(base_url() . 'welcome?pesan=belumlogin');
		}
		$this->user_id = $this->session->userdata('id_user');
		$this->first_name = $this->session->userdata('name');
		$this->last_name = $this->session->userdata('last_name');
		$this->position_name = $this->session->userdata('position_name');
		$this->branch_name = $this->session->userdata('branch_name');
	}

	public function index($call = ''){
		$data = [
			'mobile_menu' => $this->mysidebar->build_menu_mobile(),
			'sidebar_menu' => $this->mysidebar->asHtml([], true)
		];

		if (!empty($call)) {
			if ($call != 'js') {
				$id = $call;
				$this->user_id = $this->session->userdata('id_user',$id);
				$this->branch_id = $this->session->userdata('branch_id',$id);
				$this->id_bagian = $this->session->userdata('id_bagian',$id);
				$this->branch_id = $this->session->userdata('branch_id',$id);
				$this->first_name = $this->session->userdata('name',$id);
				$this->last_name = $this->session->userdata('last_name',$id);
				$this->position_name = $this->session->userdata('position_name',$id);
				$this->branch_name = $this->session->userdata('branch_name',$id);
			}
		}
		//$data['umur'] = $this->db->query("SELECT *, YEAR(CURDATE()) - YEAR(TGLLAHIR) AS age FROM users WHERE id_user=($this->user_id)")->result();
		//$data['propinsi'] = $this->m_profile->get_all_relations('m_ipropinsi')->result();
		//$data['users'] = $this->m_profile->get_all_profile($this->user_id)->result();
		$data['model'] = $this->model;

		if ($call == 'js') {
			$this->load->view('admin/view_notification', $data);
		}else{
			$this->load->view('admin/inc/v_header', $data);
			$this->load->view('admin/view_notification',$data);
			$this->load->view('admin/inc/v_footer', $data);
		}
	}

	function load_data($id_user=0,$direct=0){	
		if (!$this->input->is_ajax_request() && $direct==0) {
			exit('No direct script access allowed');
		}

		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$offset = empty($start) ? 0 : $start;
		$rows = empty($length) ? -1 : $length;

		$param = [];
		// if ($id_user) {
			$param['to_id'] = $this->session->userdata('id_user');
		// }
		
		$search = $this->input->post('search')['value'];
		//$status = $this->input->post('status');
		// $branch = $this->input->post('branch');
		if (!empty($search)) {
			$param['description like '] = '%'.$search.'%';
		}

		// if (!empty($branch)) {
		// 	$param['users.branch_id'] = $branch;
		// }else{
		// 	$param['users.branch_id'] = $this->branch_id;
		// 	if ($id_user) {
		// 		unset($param['users.branch_id']);
		// 	}
		// }

		// if (!empty($status)) {
		// 	$param['STATUS'] = $status ;
		// }else{
		// 	$param['STATUS !='] = 'KELUAR' ;
		// }
		
		$orderby = $this->input->post('order')[0]['column']<>null?$this->input->post('order')[0]['column']:"";
		$order = $this->input->post('order')[0]['dir']<>null?$this->input->post('order')[0]['dir']:"";
		
		if($orderby=="0")
			$orderby = "id";
		else if($orderby=="1")
			$orderby = "time";
		else if($orderby=="2")
			$orderby = "from_id";
		else if($orderby=="3")
			$orderby = "description";
		
		if (!$this->session->userdata('notifRecords')) {
			$sess = [
				'notifRecords' => count($this->m_general->load_data($param,$rows,$offset,1,'notifications',$orderby,$order))
			];
			$this->session->set_userdata($sess);
		}
		$data = $this->m_general->load_data($param,$rows,$offset,($rows==-1?1:$direct),'notifications',$orderby,$order);
		$totalData = $this->session->userdata('notifRecords');
		if (count($param) > 0) {
			$totalData = count($data);
		}
		$totalDataAll =  $this->m_general->total_load_data($param,'notifications');
		
		$newData = [];
		$i = 0;
		foreach ($data as $key => $value) {
			$newData['data'][$i]['no'] = $i+1;
			$newData['data'][$i]['id'] = $value['id'];
			$newData['data'][$i]['description'] = $value['description'];
			$newData['data'][$i]['time'] = $value['time'];
			$newData['data'][$i]['data'] = $value['data'];
			$newData['data'][$i]['read'] = $value['read'];
			
			if (!empty($value['from_id']) && intval($value['from_id'])) {
				$param = ['id_user' => $value['from_id']];
				$field = ['username','name'];
				$res = $this->m_masterusers->getListByID('users',$param,$field);
				$value['name']=isset($res[0])?$res[0]['name']:'';
				$value['username']=isset($res[0])?$res[0]['username']:'';
			}
			$newData['data'][$i]['name'] = (isset($value['name'])?$value['name']:'') .' ('.(isset($value['username'])?$value['username']:'').')';
			
			$newData['data'][$i]['dailyreports_id'] = "";
			if (isset($value['data'])) {
				$data_ = json_decode($value['data']);
				if (isset($data_->dailyreports_id)) {
					$newData['data'][$i]['dailyreports_id'] = $data_->dailyreports_id;
				}
			}

			$i++;
		}

		$newData['draw'] = isset($_REQUEST['draw'])?$_REQUEST['draw']:1;
		$newData['recordsTotal'] = $totalDataAll;
		$newData['recordsFiltered'] = $totalDataAll;

		if ($direct) {
			return json_encode($newData);
		}else{
			echo json_encode($newData);	
		}
	}


	public function get_notification(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$data = array();
		$data['count'] = "";
		$param['id'] = $this->input->get('id');
		$data_notification = $this->m_general->edit_data($param,'notifications')->result();
		if (!empty($data_notification)) {
			foreach ($data_notification as $key => $value) {
				$data[$key]['id'] = $value->id;
				$data[$key]['from_id'] = $value->from_id;
				$data[$key]['to_id'] = $value->to_id;
				$data[$key]['description'] = $value->description;
				$data[$key]['data'] = json_decode($value->data);
				// $data[$key]['dailyreports_id'] = json_decode($value->data)->dailyreports_id;
				// $data[$key]['data'] = json_decode($value->data);
				$data[$key]['data'] = null;
				$data[$key]['dailyreports_id'] = "";
				if (isset($value->data)) {
					$data_ = json_decode($value->data);
					$data[$key]['data'] = $data_;
					if (isset($data_->dailyreports_id)) {
						$data[$key]['dailyreports_id'] = $data_->dailyreports_id;
					}
				}
				// $data[$key]['dailyreports_id'] = json_decode($value->data)->dailyreports_id;
				$time = date('d-M-y H:i', strtotime($value->time));
				$data[$key]['time'] = $time;
			}
			$data['count'] = $this->m_general->edit_data($param,'notifications')->num_rows();

		}
		echo json_encode($data);
	}
}