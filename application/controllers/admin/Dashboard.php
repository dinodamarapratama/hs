<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');
class Dashboard extends CI_Controller
{

    public $model = '';

    public function __construct()
    {
        parent::__construct();
        //cek login
        if ($this->session->userdata('status') != "login") {

            redirect(base_url() . 'welcome?pesan=belumlogin');
        }
        $this->user_id = $this->session->userdata('id_user');
        $this->first_name = $this->session->userdata('name');
        $this->last_name = $this->session->userdata('last_name');
        $this->position_name = $this->session->userdata('position_name');
        $this->bagian_name = $this->session->userdata('bagian_name');
        $this->branch_name = $this->session->userdata('branch_name');
        $this->call_name = $this->session->userdata('call_name');
        $this->image = $this->session->userdata('image');
    }



    public function index($call='')
    {      
        helper_log("akses", "Akses Menu Dashboard");     
		
		$show_report_kepala_cabang = true;
        $data = [
            'mobile_menu' => $this->mysidebar->build_menu_mobile(),
            'sidebar_menu' => $this->mysidebar->asHtml_v3([], true),
            'belum_report' => [],
            'show_report_kepala_cabang' => $show_report_kepala_cabang
        ];

        if ($show_report_kepala_cabang == true) {
            $this->load->model('restapi/user_model');

            $user_kepala_cabang = $this->user_model->many_user([
                'where_in' => [
                    //'dep.id_department' => [9],   /* user yg berada di dep opt */
                    'pos.id_position' => [7],       /* position branch manager */
                ],
                'where_not_in' => [
                    '_.id_user' => [62]  /* exclude dr edi */
                ],
                'where' => ['_.STATUS !=' => 'KELUAR']
            ]);

            $today = date('Y-m-d');
            $ids = [];
            foreach ($user_kepala_cabang as $key => $user) {
                $ids[] = $user['id_user'];
            }

            if (empty($ids) == false) {
                /* get report yg telah dibuat hari ini oleh user kepala cabang */
                $this->load->model('restapi/dailyreport_model');
                $dr = $this->dailyreport_model->many([
                    'where_in' => [
                        '_.from_id' => $ids
                    ],
                    'where' => [
                        'date' => $today
                    ]
                ]);

                $ids2 = $ids;
                /* exclude kepala cabang yg telah membuat report */
                foreach ($dr as $key => $report) {
                    foreach ($ids2 as $key2 => $id_user) {
                        if ($id_user == $report->from_id) {
                            unset($ids2[$key2]);
                            break;
                        }
                    }
                }
                /* id user di dalam ids2 belum membuat daily report hari ini */
                /* get user yg belum membuat report */
                $user_belum_report = [];
                foreach ($user_kepala_cabang as $key => $user) {
                    if (in_array($user['id_user'], $ids2)) {
                        $user_belum_report[] = $user;
                    }
                }
                $data['belum_report'] = $user_belum_report;
            }
        }


        $this->load->model([
            'restapi/dailyreport_model',
            'restapi/generalreports_model',
            // 'm_general',
            'm_itreports',
            'm_monthlyreports',
            'm_techreports',
            'M_complaints'
        ]);

        $sql = $this->dailyreport_model->recent_report($this->user_id)
            . "\n\n\n union \n\n\n" .
            $this->generalreports_model->recent_report($this->user_id)
            . "\n\n\n union \n\n\n" .
            $this->m_itreports->recent_report($this->user_id)
            . "\n\n\n union \n\n\n" .
            $this->m_monthlyreports->recent_report($this->user_id)
            . "\n\n\n union \n\n\n" .
            $this->m_techreports->recent_report($this->user_id);

        $sql2 = "SELECT s1.*, usr.call_name, usr.username, usr.image from ($sql) s1 join users usr on usr.id_user = s1.sender_id  where s1.is_view = 0 order by last_update desc limit 3";

        $q = $this->db->query($sql2)->result();

        $data['recentreport'] = $q;


        $data['total_daily'] = $this->db->query("SELECT COUNT(id) AS total FROM dailyreports where status='Open' ")->result();
        $data['total_hs'] = $this->db->query("SELECT COUNT(id) AS total FROM homeservice where status='Diproses' ")->result();
        $data['total_qc'] = $this->db->query("SELECT COUNT(id) AS total FROM qc_hasil_detail where comment!='' ")->result();
        $data['total_complaints'] = $this->db->query("SELECT COUNT(id) AS total FROM complaints where status='OPEN' ")->result();
        $data['total_general'] = $this->db->query("SELECT COUNT(id) AS total FROM generalreports where status='Open' ")->result();
        $data['total_monthly'] = $this->db->query("SELECT COUNT(id) AS total FROM monthlyreports where status='Open' ")->result();
        $data['total_ijin'] = $this->db->query("SELECT COUNT(ijin_id) AS total FROM ijin where status='BATAL' ")->result();
        $data['total_tech'] = $this->db->query("SELECT COUNT(id) AS total FROM techreports where status='Open' ")->result();
        $data['total_it'] = $this->db->query("SELECT COUNT(id) AS total FROM itreports  where status='Open' ")->result();
        $data['model'] = $this->model;

        $data['complaints'] = $this->db->query("SELECT * FROM complaints comp JOIN users usr ON comp.creator_id=usr.id_user JOIN branch bh ON bh.branch_id =usr.branch_id WHERE comp.status='OPEN' limit 3")->result();
		
        $data['grafik'] = $this->grafik_complaint();
    	
		if ($call=='js') {
            $this->load->view('admin/dashboard/view_dashboard', $data);
        }else{
            $this->load->view('admin/inc/v_header', $data);
			$this->load->view('admin/dashboard/v_dashboard', $data);
			$this->load->view('admin/inc/v_footer', $data);			
        }
    }

    public function logout()
    {
        helper_log("logout", "Logout");
        $this->session->sess_destroy();
        redirect(base_url() . 'welcome?pesan=logout');
    }


    function grafik_complaint(){
        /*get count complaint base on branch*/
        /**/
        $sql = "SELECT count(c.id) as jml_complaint, b.branch_name, c.jenis_komplain FROM complaints c LEFT JOIN branch b on b.branch_id=c.branch_id GROUP BY c.branch_id, c.jenis_komplain";

        $res = $this->db->query($sql)->result();

        /*get only branch*/
        $sql2 = "SELECT branch_name FROM branch ORDER BY branch_name";
        $res2 = $this->db->query($sql2)->result();

        $label = $total = $warna = [];
        $num = 0;
        /*divide by jenis*/
        $non_teknis = $teknis = $base_branch = [];
        foreach ($res as $key => $value) {
            if ($value->jenis_komplain == 'TEKNIS') {
                // $label['teknis'][] = $value->branch_name;
                // $total['teknis'][] = intval($value->jml_complaint);
                // $warna['teknis'][] = $this->fixedRGB(1);
                 $teknis[$value->branch_name] = intval($value->jml_complaint);
            }else{
                // $label['non-teknis'][] = $value->branch_name;
                // $total['non-teknis'][] = intval($value->jml_complaint);
                // $warna['non-teknis'][] = $this->fixedRGB(5);
                $non_teknis[$value->branch_name] = intval($value->jml_complaint);
            }
        }

        foreach ($res2 as $k => $v) {
            /*kecuali all & office*/
            if (!in_array(strtolower($v->branch_name),['all','office'])) {
                $base_branch[] = $v->branch_name;
            }
        }

        foreach ($base_branch as $key => $value) {
            $label[] = $value;
            $total['teknis'][] = isset($teknis[$value])?$teknis[$value]:0;
            $warna['teknis'][] = $this->fixedRGB(2);
            $total['non-teknis'][] = isset($non_teknis[$value])?$non_teknis[$value]:0;
            $warna['non-teknis'][] = $this->fixedRGB(10);
        }
        /**/

        return [
            'cabang' => $label,
            'jml' => $total,
            'warna' => $warna
        ];
    }

    function randomRGB(){
        $rgbColor = array();
        foreach(array('r', 'g', 'b') as $color){
            //Generate a random number between 0 and 255.
            $rgbColor[$color] = mt_rand(0, 255);
        }
        return 'rgba('.implode(',',$rgbColor).')';
    }

    function fixedRGB($index=0){
        /*set 17 color*/
        $color = [
            "rgba(50, 168, 82)",
            "rgba(196, 50, 45)",
            "rgba(33, 191, 178)",
            "rgba(191, 191, 33)",
            "rgba(235, 56, 154)",
            "rgba(18, 50, 230)",
            "rgba(84, 4, 14)",
            "rgba(227, 160, 45)",
            "rgba(197, 151, 240)",
            "rgba(230, 20, 5)",
            "rgba(252, 157, 3)",
            "rgba(144, 250, 132)",
            "rgba(94, 93, 13)",
            "rgba(94, 54, 13)",
            "rgba(6, 91, 122)",
            "rgba(22, 242, 25)",
            "rgba(107, 11, 16)",
        ];

        return $color[$index];
    }
}