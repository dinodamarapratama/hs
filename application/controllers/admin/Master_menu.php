<?php

class Master_menu extends CI_Controller {

    function __construct(){
        parent::__construct();
        //cek login
        if ($this->session->userdata('status') != "login") {
            redirect(base_url() . 'welcome?pesan=belumlogin');
            exit();
        }
        $this->load->model('m_master_menu');
        $this->menus = $this->mysidebar->asHtml([], true);
        $this->user_id = $this->session->userdata('id_user');
        $this->first_name = $this->session->userdata('name');
        $this->last_name = $this->session->userdata('last_name');
        $this->position_name = $this->session->userdata('position_name');
        $this->branch_name = $this->session->userdata('branch_name');
        ini_set('display_errors',0);
        $cek_user = $this->db->query("select * from master_menu m left join role_menu_dept r on r.id_menu = m.id_menu left join role_menu_pos p on p.id_menu = m.id_menu left join role_menu_user u on u.id_menu = m.id_menu where u.id_user =  $this->user_id and m.url = 'admin/master_menu' group by m.nama")->row();
        if ($cek_user->id_menu < 1) {

        echo "<script>
        alert('Anda tidak mempunyai Akses');
        window.location.href='dashboard';
        </script>";
        }
        else {}
    }

    function index($call=''){
        $parent = intval($this->input->get('parent'));
        $parentMenu = null;
        if(empty($parent) == false) {
            $parentMenu = $this->m_master_menu->one_menu(['where' => ['_.id_menu' => $parent]]);
        }
        $data = [
            'mobile_menu' => $this->mysidebar->build_menu_mobile(),
            'parent' => empty($parent) ? 0 : $parent,
            'sidebar_menu' => $this->menus,
            'parent_menu' => $parentMenu
        ];
        // print_r($data);
        // exit();
        // $this->load->view('cpanel/inc/main');
        // $this->load->view('cpanel/v_master_menu',$data);
        // $this->load->view('cpanel/inc/sidebar');
        // $this->load->view('admin/inc/main',$data);
        helper_log("akses", "Akses Menu Master Menu");  
		
		if ($call=='js') {
            $this->load->view('admin/master/view_master_menu', $data);
        }else{
            $this->load->view('admin/inc/v_header', $data);
			$this->load->view('admin/master/view_master_menu', $data);
			$this->load->view('admin/inc/v_footer', $data);    
        }
    }

    function dt_menu(){
        $args = [
            'include_submenu_count' => true,
            'where' => [
                '_.parent' => $this->input->get_post('parent')
            ]
        ];
        $r = $this->m_master_menu->dt_menu($args);
        echo json_encode($r);
    }

    function save_menu(){
        $data = $this->input->post('data');
        $data = json_decode($data, true);
        $r = $this->m_master_menu->add_menu($data);
        echo json_encode($r);
    }

    function update_menu(){
        $data = $this->input->post('data');
        $data = json_decode($data, true);
        $where = ['id_menu' => null];
        if(isset($data['id_menu'])) {
            $where['id_menu'] = $data['id_menu'];
            unset($data['id_menu']);
        }
        $r = $this->m_master_menu->update_menu($where, $data);
        echo json_encode($r);
    }

    function hapus_menu() {
        $id = $this->input->post('id');
        $r = $this->m_master_menu->hapus_menu(['_.id_menu' => $id]);
        echo json_encode($r);
    }

    function list_user(){
        $this->load->model('restapi/user_model');
        $r = $this->user_model->many_user(['order' => [ ['_.name'] ]]);
        echo json_encode($r);
    }

    function add_role_user(){
        $data = $this->input->post('data');
        $data = json_decode($data, true);
        $r = $this->m_master_menu->add_role_user($data);
        echo json_encode($r);
    }

    function add_role_dept(){
        $data = $this->input->post('data');
        $data = json_decode($data, true);
        $r = $this->m_master_menu->add_role_dept($data);
        echo json_encode($r);
    }

    function add_role_pos(){
        $data = $this->input->post('data');
        $data = json_decode($data, true);
        $r = $this->m_master_menu->add_role_pos($data);
        echo json_encode($r);
    }

    function list_role_user(){
        $idMenu = $this->input->get_post('id_menu');
        $r = $this->m_master_menu->many_role_user(['where' => ['_.id_menu' => $idMenu], 'order' => [ ['usr.name', 'asc'] ] ]);
        echo json_encode($r);
    }

    function list_role_pos(){
        $idMenu = $this->input->get_post('id_menu');
        $r = $this->m_master_menu->many_role_pos(['where' => ['_.id_menu' => $idMenu], 'order' => [ ['pos.name_position', 'asc'] ] ]);
        echo json_encode($r);
    }

    function list_role_dept(){
        $idMenu = $this->input->get_post('id_menu');
        $r = $this->m_master_menu->many_role_dept(['where' => ['_.id_menu' => $idMenu], 'order' => [ ['dep.name'] ]]);
        echo json_encode($r);
    }

    function hapus_role_user(){
        $where = [
            '_.id_menu' => $this->input->get_post('id_menu'),
            '_.id_user' => $this->input->get_post('id_user')
        ];
        $r = $this->m_master_menu->hapus_role_user($where);
        echo json_encode($r);
    }

    function hapus_role_dept(){
        $where = [
            '_.id_menu' => $this->input->get_post('id_menu'),
            '_.id_dept' => $this->input->get_post('id_dept')
        ];
        $r = $this->m_master_menu->hapus_role_dept($where);
        echo json_encode($r);
    }

    function hapus_role_pos(){
        $where = [
            '_.id_menu' => $this->input->get_post('id_menu'),
            '_.id_pos' => $this->input->get_post('id_pos')
        ];
        $r = $this->m_master_menu->hapus_role_pos($where);
        echo json_encode($r);
    }

    function list_dept(){
        $this->load->model('m_master_data');
        $r = $this->m_master_data->many_department();
        echo json_encode($r);
    }

    function list_position(){
        $this->load->model('m_master_data');
        $r = $this->m_master_data->many_position();
        echo json_encode($r);
    }
}