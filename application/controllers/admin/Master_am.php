<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
*
*/
class Master_am extends CI_Controller
{
public $model = 'hs_payment';
public $model1 = 'hs_initial_petugas_hs';
public $model2 = 'hs_time';
public $model3 = 'hs_abbr';
public $model4 = 'hs_block';
public $model5 = 'master_fs';



public function __construct()
{
	parent::__construct();
	//cek login
	
	$this->load->library('upload');
	$this->load->helper('tgl_indo');
	$this->user_id = $this->session->userdata('id_user');
	$this->branch_id = $this->session->userdata('branch_id');
	$this->first_name = $this->session->userdata('name');
    $this->last_name = $this->session->userdata('last_name');
    $this->position_name = $this->session->userdata('position_name');
    $this->branch_name = $this->session->userdata('branch_name');
    $this->load->model('m_masterusers');
    $this->load->model('m_master_intern_alat','m_mia');
    $this->load->model('m_intern_checklist','m_ic');
	$this->load->library('misc');
	$this->load->library('curl');
    $this->API= $this->config->config['api_url_node'];
    ini_set('display_errors',0);
        $cek_user = $this->db->query("select * from master_menu m left join role_menu_dept r on r.id_menu = m.id_menu left join role_menu_pos p on p.id_menu = m.id_menu left join role_menu_user u on u.id_menu = m.id_menu where u.id_user =  $this->user_id and m.url = 'admin/master_am' group by m.nama")->row();
        if ($cek_user->id_menu < 1) {

        echo "<script>
        alert('Anda tidak mempunyai Akses');
        window.location.href='dashboard';
        </script>";
        }
        else {}
}


public function index($call = '')
{
	$data = [
	'mobile_menu' => $this->mysidebar->build_menu_mobile(),
	            'sidebar_menu' => $this->mysidebar->asHtml([], true)
	];

	helper_log("akses", "Akses Menu Master BM");  

	$data['model'] = $this->model;
	if (!empty($id)) {
	$this->session->set_userdata('selected_branch', $id);
	$this->branch_id = $id;
	}

	$data['hs_payment'] = $this->m_masterhs->get_all_relations('hs_payment')->result();
	$data['hs_initial_petugas_hs'] = $this->m_masterhs->get_all_masterphs($this->branch_id)->result();
	//$data['users'] = $this->m_masterhs->get_all_relations('users')->result();
	$data['branch'] = $this->m_masterhs->get_all_relations('branch')->result();
	$data['time'] = $this->m_masterhs->get_all_mastertime()->result();
	$data['abbr'] = $this->m_masterhs->get_all_masterabbr($this->branch_id)->result();
	$data['master_depart'] = $this->m_masterusers->get_all_relations('departments')->result();
	$data['master_position'] = $this->m_masterusers->get_all_relations('position')->result();
	$data['master_bagian'] = $this->m_masterusers->get_all_relations('bagian')->result();

	/*add*/
	$list_alat = $this->m_mia->get_alat()->result();
    $data['list_alat'] = $list_alat;

	$list_cabang = $this->m_mia->get_cabang()->result();
    $data['list_cabang'] = $list_cabang;
	
	
    /*get ic data*/
    $param['m_a.branch_id'] = $this->branch_id;
    if ($this->branch_id == 16) {
        unset($param['m_a.branch_id']);
		$data['users'] = $this->m_masterhs->get_all_relations('users')->result();
    }
    else
		$data['users'] = $this->m_masterhs->edit_data(array('branch_id'=>$this->branch_id),'users')->result();
	
	
    $ic = $this->m_mia->get_all_checklist($param)->result();
    $data['list_ic'] = $ic;

    /**/

    /*get mia data*/
    $mia = $this->m_mia->get_all_data($param)->result();
    $data['list_mia'] = $mia;
    /**/

    $bulan = $this->misc->getBulan();
    $tahun = $this->misc->getTahun();

    $data['list_bulan'] = $bulan;
    $data['list_tahun'] = $tahun;
    $data['nama_cabang'] = $this->branch_name;

	
	$this->load->view('admin/inc/v_header', $data);
	$this->load->view('admin/master/view_masteram', $data);
	$this->load->view('admin/inc/v_footer', $data);    
	
}

// START PAY
public function get_pay()
{

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data = array();
$data_pay = $this->m_masterhs->get_all_masterpayment()->result();
//$data_pay = json_decode($this->curl->simple_get($this->API.'hspayment'));


if (!empty($data_pay)) {
$i = 0;
foreach ($data_pay as $key => $value) {
$data['data'][$i]['no'] = $i+1;
$data['data'][$i]['pay_id'] = $value->pay_id;
$data['data'][$i]['pay_name'] = $value->pay_name;
$data['data'][$i]['creator_id'] = $value->fullname;
$created_at = date('d-M-y H:i:s', strtotime($value->created_at) );
$data['data'][$i]['created_at'] = $created_at;
$i++;
}
}
echo json_encode($data);
}

public function save_pay()
{
$result['status'] = false;
$result['message'] = 'Data gagal disimpan.';

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data['pay_name'] = $this->input->post('pay_name');
$data['creator_id'] = $this->user_id;
$data['created_at'] = date("Y-m-d H:i:s");

$pay_id = $this->input->post('pay_id');

if (!empty($pay_id)) {
$where['pay_id'] = $this->input->post('pay_id');
$pay_id = $this->m_masterhs->update_data($where, $data, $this->model);
$pay_id = $this->m_masterhs->update_data($where, $data, "admin_mis.hs_payment");
$result['status'] = true;
$result['message'] = 'Data berhasil disimpan.';
} else {
$pay_id = $this->m_masterhs->insert_data($data, $this->model);
$pay_id = $this->m_masterhs->insert_data($data, "admin_mis.hs_payment");
}
if ($pay_id) {
$result['status'] = true;
$result['message'] = 'Data berhasil disimpan.';
}

echo json_encode($result);
}

public function edit_pay()
{
$result['status'] = false;
$result['message'] = 'Data tidak ditemukan.';

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data['pay_id'] = $this->input->post('pay_id');


$result['status'] = true;
$result['message'] = 'Data ditemukan';
$edit_data = $this->m_masterhs->edit_data_masterpayment($data, $this->model)->row();
if (!empty($edit_data)) {
$result['data'] = (array) $edit_data;
}

echo json_encode($result);
}

public function delete_pay()
{
$result['status'] = false;
$result['message'] = 'Data gagal dihapus.';

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data['pay_id'] = $this->input->post('pay_id');


$delete = $this->m_masterhs->delete_data($data, $this->model);
$delete = $this->m_masterhs->delete_data($data, "admin_mis.hs_payments");

if ($delete) {
$result['status'] = true;
$result['message'] = 'Data berhasil dihapus.';
}

echo json_encode($result);
}

// END PAY

// START ABBR
public function get_abbr()
{

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data = array();
$data_abbr = $this->m_masterhs->get_all_masterabbr($this->branch_id)->result();

//$data_abbr = json_decode($this->curl->simple_get($this->API.'hsabbr?branch_id='.$this->branch_id));

if (!empty($data_abbr)) {
$i = 0;
foreach ($data_abbr as $key => $value) {
$data['data'][$i]['no'] = $i+1;
$data['data'][$i]['abbr_id'] = $value->abbr_id;
$data['data'][$i]['abbr_name'] = $value->abbr_name;
$data['data'][$i]['lokasi'] = $value->lokasi;
$data['data'][$i]['branch_id'] = $value->branch_name;
$data['data'][$i]['creator_id'] = $value->fullname;
$created_at = date('d-M-y H:i:s', strtotime($value->created_at) );
$data['data'][$i]['created_at'] = $created_at;
$i++;
}
}
echo json_encode($data);
}

public function save_abbr()
{
$result['status'] = false;
$result['message'] = 'Data gagal disimpan.';

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data['abbr_name'] = $this->input->post('abbr_name');
$data['lokasi'] = $this->input->post('lokasi');
$data['branch_id'] = $this->branch_id;
$data['creator_id'] = $this->user_id;
$data['created_at'] = date("Y-m-d H:i:s");

$abbr_id = $this->input->post('abbr_id');

if (!empty($abbr_id)) {
$where['abbr_id'] = $this->input->post('abbr_id');
$abbr_id = $this->m_masterhs->update_data($where, $data, $this->model3);
$abbr_id = $this->m_masterhs->update_data($where, $data, "admin_mis.hs_abbr");
$result['status'] = true;
$result['message'] = 'Data berhasil disimpan.';
} else {
$abbr_id = $this->m_masterhs->insert_data($data, $this->model3);
$abbr_id = $this->m_masterhs->insert_data($data, "admin_mis.hs_abbr");
}
if ($abbr_id) {
$result['status'] = true;
$result['message'] = 'Data berhasil disimpan.';
}

echo json_encode($result);
}

public function edit_abbr()
{
$result['status'] = false;
$result['message'] = 'Data tidak ditemukan.';

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data['abbr_id'] = $this->input->post('abbr_id');


$result['status'] = true;
$result['message'] = 'Data ditemukan';
$edit_data = $this->m_masterhs->edit_data_masterabbr($data, $this->model3)->row();
if (!empty($edit_data)) {
$result['data'] = (array) $edit_data;
}

echo json_encode($result);
}

public function delete_abbr()
{
$result['status'] = false;
$result['message'] = 'Data gagal dihapus.';

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data['abbr_id'] = $this->input->post('abbr_id');


$delete = $this->m_masterhs->delete_data($data, $this->model3);
$delete = $this->m_masterhs->delete_data($data, "admin_mis.hs_abbr");

if ($delete) {
$result['status'] = true;
$result['message'] = 'Data berhasil dihapus.';
}

echo json_encode($result);
}

// END ABBR

// START BLOCK
public function get_block()
{

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data = array();
$data_block = $this->m_masterhs->get_all_masterblock($this->branch_id)->result();

if (!empty($data_block)) {
$i = 0;
foreach ($data_block as $key => $value) {
$data['data'][$i]['no'] = $i+1;
$data['data'][$i]['block_id'] = $value->block_id;
$data['data'][$i]['ptgs_id'] = $value->abbr_hs;
$data['data'][$i]['ptgs'] = $value->ptgs;
$data['data'][$i]['nama'] = $value->nama;
$data['data'][$i]['hari_id'] = $value->hari_id;
$data['data'][$i]['time_name'] = $value->time_name;
$data['data'][$i]['branch_id'] = $value->branch_name;
$data['data'][$i]['creator_id'] = $value->fullname;
$created_at = date('d-M-y H:i:s', strtotime($value->created_at) );
$data['data'][$i]['created_at'] = $created_at;
$i++;
}
}
echo json_encode($data);
}

public function save_block()
{
$result['status'] = false;
$result['message'] = 'Data gagal disimpan.';

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data['ptgs_id'] = $this->input->post('ptgs_id');
$data['nama'] = $this->input->post('nama');
$data['hari_id'] = $this->input->post('hari_id');
//$data['time_id'] = implode(',', $this->input->post('time_id')); 
$arr_time = array();
		$arr_time_name = array();
		if(!empty($this->input->post('time_id'))){
			foreach($this->input->post('time_id') as $v){
				$time = $this->m_masterhs->edit_data(array('time_id'=>$v),'hs_time')->result();
				$arr_time[]=$v;
				$arr_time_name[]=$time[0]->time_name;
			}
		
			$data['time_id'] = implode(",", $arr_time);
			$data['time_name'] = implode(",", $arr_time_name);
		}
$data['branch_id'] = $this->branch_id;
$data['creator_id'] = $this->user_id;
$data['created_at'] = date("Y-m-d H:i:s");

$block_id = $this->input->post('block_id');

if (!empty($block_id)) {
$where['block_id'] = $this->input->post('block_id');
$block_id = $this->m_masterhs->update_data($where, $data, $this->model4);
$block_id = $this->m_masterhs->update_data($where, $data, "admin_mis.hs_block");
$result['status'] = true;
$result['message'] = 'Data berhasil disimpan.';
} else {
$block_id = $this->m_masterhs->insert_data($data, $this->model4);
$block_id = $this->m_masterhs->insert_data($data, "admin_mis.hs_block");
}
if ($block_id) {
$result['status'] = true;
$result['message'] = 'Data berhasil disimpan.';
}

echo json_encode($result);
}

public function edit_block()
{
$result['status'] = false;
$result['message'] = 'Data tidak ditemukan.';

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data['block_id'] = $this->input->post('block_id');


$result['status'] = true;
$result['message'] = 'Data ditemukan';
$edit_data = $this->m_masterhs->edit_data_masterblock($data, $this->model4)->row();
if (!empty($edit_data)) {
$result['data'] = (array) $edit_data;
}

echo json_encode($result);
}

public function delete_block()
{
$result['status'] = false;
$result['message'] = 'Data gagal dihapus.';

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data['block_id'] = $this->input->post('block_id');


$delete = $this->m_masterhs->delete_data($data, $this->model4);
$delete = $this->m_masterhs->delete_data($data, "admin_mis.hs_block");
if ($delete) {
$result['status'] = true;
$result['message'] = 'Data berhasil dihapus.';
}

echo json_encode($result);
}

// END BLOCK



// START INITIAL PETUGAS HS
public function get_phs()
{



$data = array();
$data_phs = $this->m_masterhs->get_all_masterphs($this->branch_id)->result();


if (!empty($data_phs)) {
$i = 0;
foreach ($data_phs as $key => $value) {
$data['data'][$i]['no'] = $i+1;
$data['data'][$i]['ptgshs_id'] = $value->ptgshs_id;
$data['data'][$i]['petugas_hs'] = $value->fullname;
$data['data'][$i]['abbr_hs'] = $value->abbr_hs;
$start_date = longdate_indo($value->start_date);
$data['data'][$i]['start_date'] = $start_date;
$data['data'][$i]['start_date_sort'] = strtotime($value->start_date);
$end_date =longdate_indo($value->end_date);
$data['data'][$i]['end_date'] = $end_date;
$data['data'][$i]['end_date_sort'] = strtotime($value->end_date);
$data['data'][$i]['branch_id'] = $value->branch_name;
$data['data'][$i]['creator_id'] = $value->namefull;
$created_at =  date('d-M-y H:i:s', strtotime($value->created_at) );
$data['data'][$i]['created_at'] = $created_at;$i++;
}
}
echo json_encode($data);
}

public function save_phs()
{
$result['status'] = false;
$result['message'] = 'Data gagal disimpan.';

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$id_notif = $this->input->post('id_notif');

$data['id_user'] = $this->input->post('id_user');
$data['abbr_hs'] = $this->input->post('abbr_hs');
$data['start_date'] = $this->input->post('start_date');
$data['end_date'] = $this->input->post('end_date');
$data['branch_id'] = $this->branch_id;
$data['creator_id'] = $this->user_id;
$data['created_at'] = date("Y-m-d H:i:s");

$phs_id = $this->input->post('ptgshs_id');
$id_user = $this->input->post('id_user');

if($data['branch_id']!=0 && $data['branch_id']!='0')
{

	if(!empty($id_user)){	
		$where = array('branch_id'=>$this->branch_id,'(start_date between "'.$this->input->post('start_date').'" and "'.$this->input->post('end_date').'" or end_date between "'.$this->input->post('start_date').'" and "'.$this->input->post('end_date').'")' =>null,'(id_user = '.$this->input->post('id_user').' or abbr_hs = "'.$this->input->post('abbr_hs').'")'=>null);
		
		if (!empty($phs_id)) $where['ptgshs_id <> '.$this->input->post('ptgshs_id')] = null;
		
		$cek = $this->m_masterhs->edit_data($where,$this->model1)->row(); //,'abbr_hs'=>$this->input->post('abbr_hs')
		
		if(isset($cek->ptgshs_id)){
			$result['status'] = false;
			$result['message'] = 'Data pada tanggal tersebut sudah ada.';
		}
		else{
			if (!empty($phs_id)){
				$where2['ptgshs_id'] = $this->input->post('ptgshs_id');
				$phs_id = $this->m_masterhs->update_data($where2, $data, $this->model1);
				$phs_id = $this->m_masterhs->update_data($where2, $data, "admin_mis.hs_initial_petugas_hs");
			}
			else{
				$phs_id = $this->m_masterhs->insert_data($data, $this->model1);
				$phs_id = $this->m_masterhs->insert_data($data, "admin_mis.hs_initial_petugas_hs");
			}
			$result['status'] = true;
			$result['message'] = 'Data berhasil disimpan.';
		}
	}
	else{
		$data['id_user'] = 0;
		$where = array('branch_id'=>$this->branch_id,'(start_date between "'.$this->input->post('start_date').'" and "'.$this->input->post('end_date').'" or end_date between "'.$this->input->post('start_date').'" and "'.$this->input->post('end_date').'")' =>null,'(abbr_hs = "'.$this->input->post('abbr_hs').'")'=>null);
		
		if (!empty($phs_id)) $where['ptgshs_id <> '.$this->input->post('ptgshs_id')] = null;
		
		$cek = $this->m_masterhs->edit_data($where,$this->model1)->row(); //,'abbr_hs'=>$this->input->post('abbr_hs')
		
		if(isset($cek->ptgshs_id)){
			$result['status'] = false;
			$result['message'] = 'Data pada tanggal tersebut sudah ada.';
		}
		else{
			if (!empty($phs_id)){
				$where2['ptgshs_id'] = $this->input->post('ptgshs_id');
				$phs_id = $this->m_masterhs->update_data($where2, $data, $this->model1);
				$phs_id = $this->m_masterhs->update_data($where2, $data, "admin_mis.hs_initial_petugas_hs");
			}
			else{
				$phs_id = $this->m_masterhs->insert_data($data, $this->model1);
				$phs_id = $this->m_masterhs->insert_data($data, "admin_mis.hs_initial_petugas_hs");
			}
					
			$result['status'] = true;
			$result['message'] = 'Data berhasil disimpan.';
		}
	}

	if($id_notif!=''){
		$cek_where['ptgshs_id'] = $this->input->post('ptgshs_id');
		//$cek_where['id_user <> 0'] = null;
		$cek = $this->m_masterhs->edit_data($cek_where,'hs_initial_petugas_hs')->row();
		if(isset($cek->id_user)){
			if($cek->id_user!=0){
				$where_notif["data like '%\"notif_type\":\"petugashs\"%'"] = null;
				$where_notif["data like '%\"id\":".$id_notif."%'"] = null;
				$this->m_masterhs->delete_data($where_notif, 'notifications');
				$this->m_masterhs->delete_data($where_notif, 'admin_mis.notifications');
			}
		}
	}

}
/*if ($phs_id) {
$result['status'] = true;
$result['message'] = 'Data berhasil disimpan.';
}*/

echo json_encode($result);
}

public function edit_phs()
{
$result['status'] = false;
$result['message'] = 'Data tidak ditemukan.';

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data['ptgshs_id'] = $this->input->post('ptgshs_id');


$result['status'] = true;
$result['message'] = 'Data ditemukan';
$edit_data = $this->m_masterhs->edit_data_masterphs($data, $this->model1)->row();
if (!empty($edit_data)) {
$result['data'] = (array) $edit_data;
}

echo json_encode($result);
}

public function delete_phs()
{
$result['status'] = false;
$result['message'] = 'Data gagal dihapus.';

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data['ptgshs_id'] = $this->input->post('ptgshs_id');


$delete = $this->m_masterhs->delete_data($data, $this->model1);
$delete = $this->m_masterhs->delete_data($data, "admin_mis.hs_initial_petugas_hs");

if ($delete) {
$result['status'] = true;
$result['message'] = 'Data berhasil dihapus.';
}

echo json_encode($result);
}

// END INITIAL PETUGAS HS



// START TIME
public function get_time()
{

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data = array();
$data_time = $this->m_masterhs->get_all_mastertime($this->branch_id)->result();
//$data_time = json_decode($this->curl->simple_get($this->API.'hstime?branch_id='.$this->branch_id));

if (!empty($data_time)) {
$i = 0;
foreach ($data_time as $key => $value) {
$data['data'][$i]['no'] = $i+1;
$data['data'][$i]['time_id'] = $value->time_id;
$data['data'][$i]['time_name'] = $value->time_name;
$data['data'][$i]['branch_id'] = $value->branch_name;
$data['data'][$i]['creator_id'] = $value->fullname;
$created_at = date('d-M-y H:i:s', strtotime($value->created_at) );
$data['data'][$i]['created_at'] = $created_at;
$i++;
}
}
echo json_encode($data);
}

public function save_time()
{
$result['status'] = false;
$result['message'] = 'Data gagal disimpan.';

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data['time_name'] = $this->input->post('time_name');
$data['branch_id'] = $this->branch_id;
$data['creator_id'] = $this->user_id;
$data['created_at'] = date("Y-m-d H:i:s");

$time_id = $this->input->post('time_id');

if (!empty($time_id)) {
$where['time_id'] = $this->input->post('time_id');
$time_id = $this->m_masterhs->update_data($where, $data, $this->model2);
$time_id = $this->m_masterhs->update_data($where, $data, "admin_mis.hs_tim");
$result['status'] = true;
$result['message'] = 'Data berhasil disimpan.';
} else {
$time_id = $this->m_masterhs->insert_data($data, $this->model2);
$time_id = $this->m_masterhs->insert_data($data, "admin_mis.hs_tim");
}
if ($time_id) {
$result['status'] = true;
$result['message'] = 'Data berhasil disimpan.';
}

echo json_encode($result);
}

public function edit_time()
{
$result['status'] = false;
$result['message'] = 'Data tidak ditemukan.';

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data['time_id'] = $this->input->post('time_id');


$result['status'] = true;
$result['message'] = 'Data ditemukan';
$edit_data = $this->m_masterhs->edit_data_mastertime($data, $this->model2)->row();
if (!empty($edit_data)) {
$result['data'] = (array) $edit_data;
}

echo json_encode($result);
}

public function delete_time()
{
$result['status'] = false;
$result['message'] = 'Data gagal dihapus.';

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data['time_id'] = $this->input->post('time_id');


$delete = $this->m_masterhs->delete_data($data, $this->model2);
$delete = $this->m_masterhs->delete_data($data, "admin_mis.hs_time");

if ($delete) {
$result['status'] = true;
$result['message'] = 'Data berhasil dihapus.';
}

echo json_encode($result);
}

// END TIME


function get_karyawan(){
	$kar =  $this->db->query("SELECT users.name,users.last_name FROM users  where status in ('TETAP','KONTRAK') ORDER BY name asc")->result();
	$data['code'] = 200;
	$data['data'] = $kar;

	echo json_encode($data);
}


// START FS
public function get_fs()
{



$data = array();
$data_fs = $this->m_fs->get_all_fs()->result();


if (!empty($data_fs)) {
$i = 0;
foreach ($data_fs as $key => $value) {
$data['data'][$i]['no'] = $i+1;
$data['data'][$i]['fs_id'] = $value->fs_id;
$data['data'][$i]['start_fs'] = $value->start_fs;
$data['data'][$i]['end_fs'] = $value->end_fs;
$data['data'][$i]['slot'] = $value->slot;
$data['data'][$i]['branch_id'] = $value->branch_name;
$data['data'][$i]['creator_id'] = $value->fullname;
$created_date = date('d-M-y H:i:s', strtotime($value->created_date) );
$data['data'][$i]['created_date'] = $created_date;
$i++;
}
}
echo json_encode($data);
}

public function save_fs()
{
$result['status'] = false;
$result['message'] = 'Data gagal disimpan.';

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data['start_fs'] = $this->input->post('start_fs');
$data['end_fs'] = $this->input->post('end_fs');
$data['slot'] = $this->input->post('slot');
$data['branch_id'] = $this->branch_id;
$data['creator_id'] = $this->user_id;
$data['created_at'] = date("Y-m-d H:i:s");

$fs_id = $this->input->post('fs_id');

if (!empty($fs_id)) {
$where['fs_id'] = $this->input->post('fs_id');
$fs_id = $this->m_fs->update_data($where, $data, $this->model5);
$fs_id = $this->m_fs->update_data($where, $data, "admin_mis.master_fs");
$result['status'] = true;
$result['message'] = 'Data berhasil disimpan.';
} else {
$fs_id = $this->m_fs->insert_data($data, $this->model5);
$fs_id = $this->m_fs->insert_data($data, "admin_mis.master_fs");
}
if ($fs_id) {
$result['status'] = true;
$result['message'] = 'Data berhasil disimpan.';
}

echo json_encode($result);
}

public function edit_fs()
{
$result['status'] = false;
$result['message'] = 'Data tidak ditemukan.';

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data['fs_id'] = $this->input->post('fs_id');


$result['status'] = true;
$result['message'] = 'Data ditemukan';
$edit_data = $this->m_fs->edit_data_fs($data, $this->model5)->row();
if (!empty($edit_data)) {
$result['data'] = (array) $edit_data;
}

echo json_encode($result);
}

public function delete_fs()
{
$result['status'] = false;
$result['message'] = 'Data gagal dihapus.';

if (!$this->input->is_ajax_request()) {
exit('No direct script access allowed');
}

$data['fs_id'] = $this->input->post('fs_id');


$delete = $this->m_fs->delete_data($data, $this->model5);
$delete = $this->m_fs->delete_data($data, "admin_mis.master_fs");
if ($delete) {
$result['status'] = true;
$result['message'] = 'Data berhasil dihapus.';
}

echo json_encode($result);
}

// END INITIAL PETUGAS HS


}
