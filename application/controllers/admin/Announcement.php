<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

class Announcement extends CI_Controller {
    public $model = 'users';
    function __construct(){
        parent::__construct();
        if ($this->session->userdata('status') != "login") {
            /*we set session to history if this is notif url*/
            $params = $_SERVER['QUERY_STRING'];
            if (!empty($params)) {
                $fullUrl = current_url().'?'.$params;
                $this->session->set_userdata('notif_url',$fullUrl);
            }else{
                $this->session->set_userdata('notif_url','');
            }
            redirect(base_url() . 'welcome?pesan=belumlogin');
        }
        
        $this->user_id = $this->session->userdata('id_user');
        $this->load->model('m_announcement', 'cmodel');
        $this->load->model('restapi/user_model');
        $this->user = $this->user_model->one_user(['where' => ['_.id_user' => $this->user_id]]);
        if(empty($this->user)) {
            echo 'User is not valid';
            exit();
        }
        // print_r($this->user);
        // exit();
        $this->branch_id = $this->user['branch_id'];
        $this->first_name = $this->session->userdata('name');
        $this->last_name = $this->session->userdata('last_name');
        $this->call_name = $this->session->userdata('call_name');
        $this->position_name = $this->session->userdata('position_name');
        $this->branch_name = $this->session->userdata('branch_name');
        ini_set('display_errors',0);
        // $cek_user = $this->db->query("select * from master_menu m left join role_menu_dept r on r.id_menu = m.id_menu left join role_menu_pos p on p.id_menu = m.id_menu left join role_menu_user u on u.id_menu = m.id_menu where u.id_user =  $this->user_id and m.url = 'admin/announcement' group by m.nama")->row();
        // if ($cek_user->id_menu < 1) {

        // echo "<script>
        // alert('Anda tidak mempunyai Akses');
        // window.location.href='dashboard';
        // </script>";
        // }
        // else {}
    }

    function index($call=''){
        // throw new \Exception(print_r('TEST',true));
         helper_log("akses", "Akses Menu Announcement");
        $data = [
            'model' => 'announcement',
            'mobile_menu' => $this->mysidebar->build_menu_mobile(),
            'sidebar_menu' => $this->mysidebar->asHtml([], true),
            'my_id' => $this->user_id
        ];
		
		if ($call=='js') {
			$this->load->view('admin/pengumuman/v_announcement', $data);
        }else{
			$this->load->view('admin/inc/v_header', $data);
			$this->load->view('admin/pengumuman/v_announcement', $data);   
			$this->load->view('admin/inc/v_footer', $data);    
        }
    }

    function my_announcements(){
        $args = [
            'where' => [
                //'_.deleted_at' => null
            ],
            //'order' => [ ['_.created_at', 'asc'] ]
        ];
        $args['year'] = intval($this->input->post('year'));
        $args['month'] = intval($this->input->post('month'));
        $args['id_user'] = $this->user_id;

        $sent = $this->cmodel->getSent($args);
        $received = $this->cmodel->getReceived($args);

        $r = [
            'sent' => $sent,
            'received' => $received
        ];
        echo json_encode($r);
    }

    function dt_inbox(){
        $args = [
            'year' => $this->input->post('year'),
            'month' => $this->input->post('month'),
            'day' => $this->input->post('day'),
            'id_user' => $this->user_id,
            'where' => [
                //'_.deleted_at' => null
            ]
        ];

        $r = $this->cmodel->dt_inbox($args);
        echo json_encode($r);
    }

    function dt_sent(){
        $args = [
            'year' => $this->input->post('year'),
            'month' => $this->input->post('month'),
            'day' => $this->input->post('day'),
            'id_user' => $this->user_id
        ];

        $r = $this->cmodel->dt_sent($args);
        echo json_encode($r);
    }

    function save(){
        $data = [
            'receivers' => $this->input->post('receivers'),
            'bagian' => $this->input->post('bagian'),
            'department' => $this->input->post('department'),
            'event_type' => $this->input->post('type'),
            'title' => $this->input->post('subject'),
            'start' => $this->input->post('start'),
            'end' => $this->input->post('end'),
            'note' => $this->input->post('notes'),
            'creator_id' => $this->user_id,
            'branch_id' => $this->branch_id,
			'repeat_type' => $this->input->post('repeat_type'),
            'time_repeat' => $this->input->post('time_repeat'),
			'tgl_multi' => $this->input->post('tgl_multi'),
			'id_announcement' => $this->input->post('id_announcement'),
        ];

        // print_r($data);
		$r = $this->cmodel->add_announcement($data);

        if($r['status'] == true) {
            $this->load->helper('file_uploader');
            start_upload([
                'mode' => 'single',
                'key' => 'file',
                'table_fk' => 'announcement',
                'id' => $r['id']
            ]);
        }

        echo json_encode($r);
    }

    function hapus(){
        $ids = $this->input->post('ids');
        $ids = json_decode($ids, true);
        if(is_array($ids) == false) {
            $ids = [$ids];
        }
        $items = $this->cmodel->many_announcement(['where_in' => ['_.id' => $ids]]);
        // print_r($items);
        $now = date('Y-m-d H:i:s');
        foreach ($items as $key => $item) {
            if($item['creator_id'] == $this->user_id) {
                $this->cmodel->simpleUpdate(['id' => $item['id']], ['deleted_at' => $now]);
            }
            else {
                $this->cmodel->removeReceiver(['id_user' => $this->user_id, 'id' => $item['id']]);
            }
        }

        $r = ['status' => true, 'message' => 'Delete success'];
        echo json_encode($r);
    }

    function get_user_read_old(){
        $id = $this->input->post('id');
        $r = $this->cmodel->getReceivers($id);
        echo json_encode($r);
    }
	
	function get_user_read(){
        $id = $this->input->post('id');
		$tgl = $this->input->post('tgl');
        $r = $this->cmodel->getReceivers($id,$tgl);
        echo json_encode($r);
    }

    function detail(){
        $id = $this->input->post('id');
        $r = $this->cmodel->one_announcement(['where' => ['_.id' => $id]]);
        if($r != null) {
            if($r['creator_id'] == $this->user_id) {} else {
                $this->cmodel->rcv_mark_read($r['id'],$this->user_id);
            }
            $r['receivers'] = $this->cmodel->getReceivers($r['id']);
            $r['attachments'] = $this->cmodel->getAttachments($r['id']);
        }
        echo json_encode($r);
    }
	
	public function get_notification() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data = array();
        $data['count'] = "";

        $data_notification = $this->cmodel->get_notification($this->user_id)->result();
        if (!empty($data_notification)) {
            foreach ($data_notification as $key => $value) {
                $data[$key]['id'] = $value->id;
                $data[$key]['from_id'] = $value->from_id;
				$data[$key]['photo_profile'] = $value->photo_profile;
				$data[$key]['creator_name'] = $value->creator_name;
				$data[$key]['read'] = $value->read;										 
                $data[$key]['to_id'] = $value->to_id;
                $data[$key]['description'] = $value->description;
                $data[$key]['data'] = json_decode($value->data);
                // $data[$key]['dailyreports_id'] = json_decode($value->data)->dailyreports_id;
                // $data[$key]['data'] = json_decode($value->data);
                $data[$key]['data'] = null;
                //$data[$key]['dailyreports_id'] = "";
                if (isset($value->data)) {
                    $data_ = json_decode($value->data);
                    $data[$key]['data'] = $data_;
                    //if (isset($data_->dailyreports_id)) {
                    //    $data[$key]['dailyreports_id'] = $data_->dailyreports_id;
                    //}
                }
                // $data[$key]['dailyreports_id'] = json_decode($value->data)->dailyreports_id;
                $time = date('d-M-y H:i', strtotime($value->time));
                $data[$key]['time'] = $time;
            }
            $data['count'] = $this->cmodel->get_notification($this->user_id)->num_rows();
			$data['noread'] = $this->cmodel->no_read_notification($this->user_id)->num_rows();

        }

        echo json_encode($data);
    }
	
	public function read_all_message() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        //$id = $this->input->post('id');
        $data = array(
            "read" => 1,
        );

        $this->m_mis->update_data(array(
            "data like '%id_announcement%'" => null,
			"cuti_flag" => 0,
            'to_id' => $this->user_id
        ) , $data, 'notifications');

        $this->m_mis->update_data(array(
            "data like '%id_announcement%'" => null,
			"cuti_flag" => 0,
            'to_id' => $this->user_id
        ) , $data, 'admin_mis.notifications');

        echo json_encode(array(
            "status" => true
        ));
    }

	/*public function get_notification()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data = array();
        $data['count'] = "";

        $data_notification = $this->cmodel->get_notification($this->user_id)->result();
        if (!empty($data_notification)) {
            foreach ($data_notification as $key => $value) {
                $data[$key]['id'] = $value->id;
                $data[$key]['from_id'] = $value->from_id;
                $data[$key]['to_id'] = $value->to_id;
                $data[$key]['description'] = $value->description;
                $data[$key]['data'] = json_decode($value->data);
                // $data[$key]['itreports_id'] = json_decode($value->data)->itreports_id;
                // $data[$key]['data'] = json_decode($value->data);
                $data[$key]['data'] = null;
                $data[$key]['itreports_id'] = "";
                if(isset($value->data)) {
                    $data_ = json_decode($value->data);
                    $data[$key]['data'] = $data_;
                    if(isset($data_->itreports_id)) {
                        $data[$key]['itreports_id'] = $data_->itreports_id;
                    }
                }
                // $data[$key]['itreports_id'] = json_decode($value->data)->itreports_id;
                $data[$key]['time'] = $value->time;
            }
            $data['count'] = $this->cmodel->get_notification($this->user_id)->num_rows();

        }

        echo json_encode($data);
    }*/

    public function set_notification($data_receiver, $replay=false, $other=null)
    {
        $idReceivers = [];
        $title = "New Announcement";
        if ($replay) {
            $title = 'Announcement Reply';
            $from_id = $this->user_id;
            $idReceivers[] = $to_id = $data_receiver['to_id'];
            $id_itreports = $dat['itreports_id'] = $data_receiver['id'];
            $decription = $this->session->userdata('name') . ' - '.$this->session->userdata('position_name').' ('.$this->session->userdata('branch_name').') has sent a new reply Announcement';

        }elseif (!empty($other)) {
            $from_id = $this->user_id;
            $idReceivers[] = $to_id = $data_receiver['id_receiver'];
            $dat['itreports_id'] = $data_receiver['id'];
            $decription = $this->session->userdata('name') . ' - '.$this->session->userdata('position_name').' ('.$this->session->userdata('branch_name').') '. $other;
        }else{
            $from_id = $this->user_id;
            // $idReceivers[] = $to_id = $data_receiver['id_receiver'];
            $idReceivers = $data_receiver['receivers'];
            //$dat['itreports_id'] = $data_receiver['id_itreports'];
            $decription = $this->session->userdata('name') . ' - '.$this->session->userdata('position_name').' ('.$this->session->userdata('branch_name').') has sent a new Announcement';
        }

        $this->load->library("Notify_lib");
        $nl = new Notify_lib();
        $nl->send(
            $title,                                             //title
            $decription,                                        //message
            $from_id,                                           //sender
            $idReceivers,                                       //list receiver
            ["id" => $data_receiver["id"]],        //attachment
            "announcement",                                      //type notifikasi
            true                                                //kirim ke onesignal
        );
    }

    public function update_notification()
    {
         if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $id = $this->input->post("id");

        $data['read'] = 1;

        $this->m_mis->update_data(array('id' => $id), $data, 'notifications');
        $this->m_mis->update_data(array('id' => $id), $data, 'admin_mis.notifications');

        echo json_encode(array("status" => true));
    }

}