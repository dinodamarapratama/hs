<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

include APPPATH.'third_party/PHPExcel/Classes/PHPExcel.php';
/**
 *
 */
class Homeservice extends CI_Controller
{

    public $model = 'homeservice';



    public function __construct()
    {
        parent::__construct();
        // cek login
        if ($this->session->userdata('status') != "login") {

            redirect(base_url() . 'welcome?pesan=belumlogin');
        }
        $this->load->library('upload');
        $this->load->helper('tgl_indo');
        $this->user_id = $this->session->userdata('id_user');
        $this->first_name = $this->session->userdata('name');
        $this->last_name = $this->session->userdata('last_name');
        $this->call_name = $this->session->userdata('call_name');
        $this->position_name = $this->session->userdata('position_name');
        $this->branch_name = $this->session->userdata('branch_name');
        $this->branch_id = $this->session->userdata('selected_branch');
        if (empty($this->branch_id)) {
            $this->branch_id = $this->session->userdata('branch_id');
        }
        ini_set('display_errors',0);
		$cek_user = $this->db->query("select * from master_menu m left join role_menu_dept r on r.id_menu = m.id_menu left join role_menu_pos p on p.id_menu = m.id_menu left join role_menu_user u on u.id_menu = m.id_menu where u.id_user =  $this->user_id and m.url = 'admin/homeservice' group by m.nama")->row();
		if ($cek_user->id_menu < 1) {

		echo "<script>
		alert('Anda tidak mempunyai Akses');
		window.location.href='dashboard';
		</script>";
		}
		else {}
    }

    public function index($call = '')
    {   
        $data = [
            'mobile_menu' => $this->mysidebar->build_menu_mobile(),
            'sidebar_menu' => $this->mysidebar->asHtml([], true)
        ];
        $data['model'] = $this->model;
        if (!empty($call)) {
            if ($call != 'js') {
                $id = $call;
                $this->session->set_userdata('selected_branch', $id);
                $this->branch_id = $this->session->userdata('branch_id');
            }
        }
         helper_log("akses", "Akses Menu Home Service");

        $data['master_branch'] = $this->m_masterusers->get_all_relations('branch')->result();
        $data['hs_time'] = $this->m_hs->get_branch_hs_time($this->branch_id)->result();
        $data['hs_payment'] = $this->m_hs->get_data('hs_payment')->result();
        $data['branch'] = $this->m_hs->get_data('branch')->result();
        $data['branch_id'] = $this->session->userdata('branch_id');
        $data['hs_initial_petugas'] = $this->m_hs->get_data_hs_initial_petugas($this->branch_id)->result();
        $data['hs_initial_petugas_hs'] = $this->m_hs->get_data_hs_initial_petugas_hs($this->branch_id, date('Y-m-d'))->result();
		$data['users'] = $this->m_masterusers->get_all_relations('users')->result();


        //throw new \Exception(print_r($data['hs_initial_petugas_hs'],true));
        $data['selected_branch'] = $this->session->userdata('selected_branch');;

        if ($call == 'js') {
			$data['easy'] = 1;
            $this->load->view('admin/homeservice/v_hs', $data);
        }else{
            $this->load->view('admin/inc/v_header', $data);
            $this->load->view('admin/homeservice/v_hs', $data);
            $this->load->view('admin/inc/v_footer', $data);    
        }

    }


    public function get_hs($date='',$branch='')
    {
       if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $date = empty($date) ? date('Y-m-d') : $date;
        $data = array();
		
		if($branch!='') $this->branch_id = $branch;
		
        $data_hs = $this->m_hs->get_all($date)->result();
        $data['hs_time'] = $this->m_hs->get_branch_hs_time($this->branch_id)->result();
        $data['hs_initial_petugas_hs'] = $this->m_hs->get_data_hs_initial_petugas_hs($this->branch_id, $date)->result();

        if (!empty($data_hs)) {
            $i = 0;
            foreach ($data_hs as $key => $value) {
                $data['data'][$i]['id'] = $value->id;
                $data['data'][$i]['no'] = $i+1;
                $data['data'][$i]['date'] = $value->date;
                $data['data'][$i]['pid'] = $value->pid;
                $time = substr($value->time_name, 0,8);
                $data['data'][$i]['time_name'] = $time;
                $data['data'][$i]['petugas_id'] = $value->petugas_id;
                $data['data'][$i]['time_id'] = $value->time_id;
                $data['data'][$i]['ptgshs_id'] = $value->ptgshs_id;
                $data['data'][$i]['nama'] = $value->namacaps;
                $data['data'][$i]['alamat'] = $value->alamat;
                $data['data'][$i]['no_telp'] = $value->no_telp;
                $data['data'][$i]['dokter'] = $value->dokter;
                $data['data'][$i]['jumlah_pasien'] = $value->jumlah_pasien;
                $data['data'][$i]['pemeriksaan'] = $value->pemeriksaan;
                $data['data'][$i]['pay_name'] = $value->pay_name;
                $data['data'][$i]['nama_petugas'] = $value->nama_petugas;
                $data['data'][$i]['nama_petugas_hs'] = $value->nama_petugas_hs;
                $data['data'][$i]['ttd_pasien'] = $value->ttd_pasien;
                $data['data'][$i]['branch'] = $value->branch_name;
                $data['data'][$i]['status'] = $value->status;
                $data['data'][$i]['reason'] = $value->reason;
                $data['data'][$i]['time_selesai'] = $value->time_selesai;
                $data['data'][$i]['branch_id'] = $value->branch_id;
                $data['data'][$i]['creator'] = $value->name_creator;
                $data['data'][$i]['created_date'] = $value->created_date;
                $data['data'][$i]['catatan'] = $value->catatan;

                $i++;
            }
        }
        $data['branch_id'] = $this->branch_id;
        echo json_encode($data);
    }
	
	public function get_hs_timeline($date='',$branch='')
    {
    //    if (!$this->input->is_ajax_request()) {
    //         exit('No direct script access allowed');
    //     }
        $date = empty($date) ? date('Y-m-d') : $date;
        $data = array();
		
		if($branch!='') $this->branch_id = $branch;
		
        $data_hs = $this->m_hs->get_all($date)->result();
        $data['hs_time'] = $this->m_hs->get_branch_hs_time($this->branch_id)->result();
        
		$abbr = $this->m_masterhs->get_all_masterabbr($this->branch_id)->result();
		$temp = array();
		if (!empty($abbr)){
			foreach($abbr as $val){
				$temp[$val->abbr_name]['abbr_hs'] = $val->abbr_name;
				$temp[$val->abbr_name]['branch_id'] = '';
				$temp[$val->abbr_name]['call_name'] = '';
				$temp[$val->abbr_name]['created_at'] = '';
				$temp[$val->abbr_name]['creator_id'] = '';
				$temp[$val->abbr_name]['end_date'] = '';
				$temp[$val->abbr_name]['id_user'] = '';
				$temp[$val->abbr_name]['name'] = '';
				$temp[$val->abbr_name]['ptgshs_id'] = '';
				$temp[$val->abbr_name]['start_date'] = '';
			}
		}
		
		$data['hs_initial_petugas_hs'] = $this->m_hs->get_data_hs_initial_petugas_hs($this->branch_id, $date)->result();
		
		if (!empty($data['hs_initial_petugas_hs'])){
			foreach($data['hs_initial_petugas_hs'] as $val){
				$temp[$val->abbr_hs]['abbr_hs'] = $val->abbr_hs;
				$temp[$val->abbr_hs]['branch_id'] = $val->branch_id;
				$temp[$val->abbr_hs]['call_name'] = $val->call_name;
				$temp[$val->abbr_hs]['created_at'] = $val->created_at;
				$temp[$val->abbr_hs]['creator_id'] = $val->creator_id;
				$temp[$val->abbr_hs]['end_date'] = $val->end_date;
				$temp[$val->abbr_hs]['id_user'] = $val->id_user;
				$temp[$val->abbr_hs]['name'] = $val->name;
				$temp[$val->abbr_hs]['ptgshs_id'] = $val->ptgshs_id;
				$temp[$val->abbr_hs]['start_date'] = $val->start_date;
			}
		}
		$ii = 0;
		foreach($temp as $k=>$v){
			$temp[$ii] = $temp[$k];
			unset($temp[$k]);
			$ii++;
		}
		
		$data['hs_initial_petugas_hs'] = $temp;
		
        if (!empty($data_hs)) {
            $i = 0;
			$temp_nama = '';
			$temp_count = 1;
			$temp_id = [];
			$temp_time_id = [];
			$temp_date = '';
			$temp_ptgshs_id = '';

			// stay
			// echo json_encode($temp_nama);
            
			foreach ($data_hs as $key => $value) {
                $time = substr($value->time_name, 0,8);
				$temp_time_id[]=$value->time_id;
				// if($value->namacaps==$temp_nama && $i!=0)
				if($value->namacaps==$temp_nama && $i!=0 && $value->ptgshs_id==$temp_ptgshs_id)
				{	
					$temp_count++;
					$k = $i-1;
					$value->time_id = $data['data'][$k]['time_id'];
					$time = $data['data'][$k]['time_name'];
					$i = $k;
					unset($data['data'][$k]);
					
					foreach($data['data'] as $y=>$v){
						if($v['nama'] == $temp_nama)
						{
							$aa = explode(",",$data['data'][$y]['multi_time_id']);
							foreach($aa as $c){
								$temp_time_id[] = $c;
							}
							$temp_time_id = array_unique($temp_time_id);
							$data['data'][$y]['multi_time_id'] = implode(",",$temp_time_id);
							
							
							$bb = explode(",",$data['data'][$y]['multi_id']);
							foreach($bb as $c){
								$temp_id[] = $c;
							}
							$temp_id[]=$value->id;
							$temp_id = array_unique($temp_id);
							$data['data'][$y]['multi_id'] = implode(",",$temp_id);
						}
					}
					
					/*foreach($data['data'] as $k=>$v){
						if($v['nama'] == $temp_nama)
						{
							$value->time_id = $data['data'][$k]['time_id'];
							$time = $data['data'][$k]['time_name'];
							$i = $k;
							unset($data['data'][$k]);
						}
					}*/
				}
				else {
					$temp_count=1;
					$temp_id = [];
					$temp_time_id = [];
					$temp_time_id[]=$value->time_id;
				}
				$temp_id[]=$value->id;
				
				$temp_nama = $value->namacaps;
				$temp_date = $value->date;
				$temp_ptgshs_id = $value->ptgshs_id;
				
				$data['data'][$i]['id'] = $value->id;
                $data['data'][$i]['no'] = $i+1;
                $data['data'][$i]['date'] = $value->date;
                $data['data'][$i]['pid'] = $value->pid;
                
                $data['data'][$i]['time_name'] = $time;
                $data['data'][$i]['petugas_id'] = $value->petugas_id;
                $data['data'][$i]['time_id'] = $value->time_id;
                $data['data'][$i]['ptgshs_id'] = $value->ptgshs_id;
                $data['data'][$i]['nama'] = $value->namacaps;
                $data['data'][$i]['alamat'] = $value->alamat;
                $data['data'][$i]['no_telp'] = $value->no_telp;
                $data['data'][$i]['dokter'] = $value->dokter;
                $data['data'][$i]['jumlah_pasien'] = $value->jumlah_pasien;
                $data['data'][$i]['pemeriksaan'] = $value->pemeriksaan;
                $data['data'][$i]['pay_name'] = $value->pay_name;
                $data['data'][$i]['nama_petugas'] = $value->nama_petugas;
                $data['data'][$i]['nama_petugas_hs'] = $value->nama_petugas_hs;
                $data['data'][$i]['ttd_pasien'] = $value->ttd_pasien;
                $data['data'][$i]['branch'] = $value->branch_name;
                $data['data'][$i]['status'] = $value->status;
                $data['data'][$i]['reason'] = $value->reason;
                $data['data'][$i]['time_selesai'] = $value->time_selesai;
                $data['data'][$i]['branch_id'] = $value->branch_id;
                $data['data'][$i]['creator'] = $value->name_creator;
                $data['data'][$i]['created_date'] = $value->created_date;
                $data['data'][$i]['catatan'] = $value->catatan;
				$data['data'][$i]['booked'] = $value->booked;
				$data['data'][$i]['deleted'] = $value->deleted;
				
				
				$data['data'][$i]['colspan'] = $temp_count;
				$data['data'][$i]['multi_id'] = implode(",",$temp_id);
				$data['data'][$i]['multi_time_id'] = implode(",",$temp_time_id);
				$data['data'][$i]['time_name_selesai'] = substr($value->time_name, 0,8);

                $i++;
            }
        }
        $data['branch_id'] = $this->branch_id;
        echo json_encode($data);
    }
	
    public function save_hs()
    {
        $result['status'] = false;
        $result['message'] = 'Data gagal disimpan.';
        $data['date'] = $this->input->post('date');
        $data['pid'] = $this->input->post('pid');
        $data['time_id'] = $this->input->post('time_id');
        $data['nama'] = $this->input->post('nama');
        $data['alamat'] = $this->input->post('alamat');
        $data['no_telp'] = $this->input->post('no_telp');
        $data['dokter'] = $this->input->post('dokter');
        $data['jumlah_pasien'] = $this->input->post('jumlah_pasien');
        $data['pemeriksaan'] = $this->input->post('pemeriksaan');
        $data['status'] = $this->input->post('status');
        $data['pay_id'] = $this->input->post('pay_id');
        $data['petugas_id'] = $this->user_id; //$this->input->post('petugas_id');
        $data['ptgshs_id'] = $this->input->post('ptgshs_id');
        $data['catatan'] = $this->input->post('catatan');
        $data['ttd_pasien'] = $this->input->post('ttd_pasien');
        $data['branch_id'] = $this->input->post('branch_id'); //$this->branch_id;
        $data['creator_id'] = $this->user_id;
        $data['created_date'] = date('Y-m-d H:i:s');


		// ganti petugas HS
		$petugas_pengganti_hs = $this->input->post('petugas_pengganti_hs');
		$data_petugas_pengganti = array(
            "id_user" => $petugas_pengganti_hs,
        );

		if(!empty($petugas_pengganti_hs)){
			$where['ptgshs_id'] = $data['ptgshs_id'];
			$this->m_hs->update_data($where, $data_petugas_pengganti, 'hs_initial_petugas_hs');
			$this->m_hs->update_data($where, $data_petugas_pengganti, 'admin_mis.hs_initial_petugas_hs');
		
		}

		// tambahin notif 

        $id_hs = $this->input->post('id');
		$id_multi = explode(",",$this->input->post('id_multi'));
		if($data['ptgshs_id']==''){
			$datenow = date("Y-m-d H:i:s");
			// if($this->m_hs->get_where(array('abbr_hs' => $this->input->post('abbr'), "'".$this->input->post('date')."'".' between start_date and end_date' => null ,'branch_id'=>$this->input->post('branch_id')), 'hs_initial_petugas_hs')->num_rows() == 0) 
			// {
			// 	$datax['id_user'] = 0;
			// 	$datax['abbr_hs'] = $this->input->post('abbr');
			// 	$datax['start_date'] = $this->input->post('date');
			// 	$datax['end_date'] = $this->input->post('date');
			// 	$datax['branch_id'] = $this->input->post('branch_id');
			// 	$datax['creator_id'] = $this->user_id;
			// 	$datax['created_at'] = $datenow;
			// 	$data['ptgshs_id'] = $this->m_masterhs->insert_data($datax, 'hs_initial_petugas_hs');
			// 	// $data['ptgshs_id'] = $this->m_masterhs->insert_data($datax, 'admin_mis.hs_initial_petugas_hs');
			// }
			// else{
			// 	$val = $this->m_hs->get_where(array('abbr_hs' => $this->input->post('abbr'), "'".$this->input->post('date')."'".' between start_date and end_date' => null,'branch_id'=>$this->input->post('branch_id')), 'hs_initial_petugas_hs')->row();
			// 	$data['ptgshs_id'] = $val->ptgshs_id;
			// }
			
			//NOTIF BRANCH MANAGER
			
			$this->load->model('restapi/user_model');

            $user_kepala_cabang = $this->user_model->many_user([
                'where_in' => [
                    'pos.id_position' => [7],       /* position branch manager */
                ],
                'where_not_in' => [
                    '_.id_user' => [62]  /* exclude dr edi */
                ],
                'where' => ['_.STATUS !=' => 'KELUAR','_.branch_id' => $this->input->post('branch_id')]
            ]);
			
			$idReceivers = [];
            foreach ($user_kepala_cabang as $key => $user) {
                $idReceivers[] = $user['id_user'];
            }
			//$idReceivers[] = 47;
			$this->load->library('Notify_lib');
			$ntf = new Notify_lib();
			$ntf->send(
				'New Home Service',
				'Mohon isi petugas hs yang terlewatkan dengan klik ini',
				$this->user_id,
				$idReceivers,
				['id' => $data['ptgshs_id'],'date' => $datenow,'branch_id' => $this->input->post('branch_id')],
				'petugashs',
				true
			);
			
			
			
		}
        if(!empty($id_hs)){
            // stay
			// if(count($id_multi)>0){
			// 	foreach($id_multi as $v){
			// 		$where['id'] = $v;
			// 		$this->m_hs->delete_data($where, $this->model);
			// 		// $this->m_hs->delete_data($where, 'admin_mis.homeservice');
			// 	}
				
			// }
			for ($x = $data['time_id']; $x <= $this->input->post('time_id_sampai'); $x++) {
				if($this->m_hs->get_where(array('time_id' => $x, 'ptgshs_id' => $data['ptgshs_id'], 'date' => $data['date'], 'status <> "Cancel"' => null ), $this->model)->num_rows() == 0) {
					$data['time_id'] = $x;
					// $id_hs = $this->m_hs->insert_data($data, $this->model);
					// $id_hs = $this->m_hs->insert_data($data, 'admin_mis.homeservice');
				}
			}
			
			$data_ptgs = $this->m_hs->edit_data(array('ptgshs_id' => $data['ptgshs_id']),'hs_initial_petugas_hs')->row();
			$data_time1 = $this->m_hs->edit_data(array('time_id' => $this->input->post('time_id')),'hs_time')->row();
			$jam = $data_time1->time_name;
			if($this->input->post('time_id_sampai')!=''){
				$data_time2 = $this->m_hs->edit_data(array('time_id' => $this->input->post('time_id_sampai')),'hs_time')->row();
				$jam = $data_time1->time_name.' '.($data_time1->time_name!=$data_time2->time_name?' - '.$data_time2->time_name:"");
			}

			if($data_ptgs->id_user!=0){
				$this->load->model('restapi/user_model');
				$sender = $this->user_model->one_user(['where' => ['_.id_user' => $data_ptgs->id_user]]);
				$idReceivers[] = $sender['id_user'];
				if($sender == null) {
					// echo json_encode(['status' => false, 'message' => 'Invalid Sender 1']);
					// exit();
				}
				else{
					$this->load->library('Notify_lib');
					$ntf = new Notify_lib();
					$ntf->send(
							'New Home Service',
							$sender['name'] . ' - '.$sender['position_name'].' ('.$sender['branch_name'].') anda mendapatkan tugas atas nama pasien '.$data['nama'].' dan jam '.$jam,
							$this->user_id,
							$idReceivers,
							['id' => $id_hs,'date' => $data['date'],'branch_id' => $data['branch_id']],
							'homeservice',
							true
						);
				}
			}
			
			$where['id'] = $this->input->post('id');
            if($this->m_hs->get_where(array('id' => $this->input->post('id'), 'ptgshs_id' => "> ''" ), $this->model)->num_rows() > 0) {
                $this->m_hs->delete_data($where, $data, $this->model);
				$this->m_hs->delete_data($where, $data, "admin_mis.homeservice");
                if(!empty($this->input->post('time_id_sampai')) && $this->input->post('time_id_sampai') > $this->input->post('time_id')) {
                    for ($x = $data['time_id']; $x <= $this->input->post('time_id_sampai'); $x++) {
                        if($this->m_hs->get_where(array('time_id' => $x, 'ptgshs_id' => $data['ptgshs_id'], 'date' => $data['date'] ), $this->model)->num_rows() == 0) {
                            $data['time_id'] = $x;
                            $id_hs = $this->m_hs->insert_data($data, $this->model);
							$id_hs = $this->m_hs->insert_data($data, "admin_mis.homeservice");
                        }
                    }

                } else {
                    $id_hs = $this->m_hs->insert_data($data, $this->model);
					$id_hs = $this->m_hs->insert_data($data, "admin_mis.homeservice");
                }
            } else {
                $id_hs = $this->m_hs->update_data($where, $data, $this->model);
				$id_hs = $this->m_hs->update_data($where, $data, "admin_mis.homeservice");
                                    for ($x = $data['time_id']; $x <= $this->input->post('time_id_sampai'); $x++) {
                        if($this->m_hs->get_where(array('time_id' => $x, 'ptgshs_id' => $data['ptgshs_id'], 'date' => $data['date'] ), $this->model)->num_rows() == 0) {
                            $data['time_id'] = $x;
                            $id_hs = $this->m_hs->insert_data($data, $this->model);
							$id_hs = $this->m_hs->insert_data($data, "admin_mis.homeservice");
                        }
                    }
                $result['status'] = true;
                $result['message'] = 'Data berhasil disimpan.';
            }

			$del = '%"id_hs":"hs_'.$this->input->post('id').'"%';
			$delete_notif = $this->db->query("delete from notifications where data LIKE '$del'");
			$delete_notif = $this->db->query("delete from admin_mis.notifications where data LIKE '$del'");

        }
		else{
			if(!empty($this->input->post('time_id_sampai')) && $this->input->post('time_id_sampai') > $this->input->post('time_id')) {
				for ($x = $data['time_id']; $x <= $this->input->post('time_id_sampai'); $x++) {
					if($this->m_hs->get_where(array('time_id' => $x, 'ptgshs_id' => $data['ptgshs_id'], 'date' => $data['date'], 'status <> "Cancel"' => null ), $this->model)->num_rows() == 0) {
						$data['time_id'] = $x;
            			$id_hs = $this->m_hs->insert_data($data, $this->model);
						$id_hs = $this->m_hs->insert_data($data, 'admin_mis.homeservice');
					}
				}

			} else {
            	$id_hs = $this->m_hs->insert_data($data, $this->model);
				$id_hs = $this->m_hs->insert_data($data, 'admin_mis.homeservice');
			}
			
			
			$data_ptgs = $this->m_hs->edit_data(array('ptgshs_id' => $data['ptgshs_id']),'hs_initial_petugas_hs')->row();
			$data_time1 = $this->m_hs->edit_data(array('time_id' => $this->input->post('time_id')),'hs_time')->row();
			
			$jam = $data_time1->time_name;
			if($this->input->post('time_id_sampai')!=''){
				$data_time2 = $this->m_hs->edit_data(array('time_id' => $this->input->post('time_id_sampai')),'hs_time')->row();
				$jam = $data_time1->time_name.' '.($data_time1->time_name!=$data_time2->time_name?' - '.$data_time2->time_name:"");
			}
			
			//$data_time2 = $this->m_hs->edit_data(array('time_id' => $this->input->post('time_id_sampai')),'hs_time')->row();
			//$jam = $data_time1->time_name.' '.($data_time1->time_name!=$data_time2->time_name?' - '.$data_time2->time_name:"");
			
			if($data_ptgs->id_user!=0){
				$this->load->model('restapi/user_model');
				$sender = null;
				if(isset($data_ptgs->id_user)) $sender = $this->user_model->one_user(['where' => ['_.id_user' => $data_ptgs->id_user]]);
				$idReceivers[] = $sender['id_user'];
				if($sender == null) {
					echo json_encode(['status' => false, 'message' => 'Invalid Sender 2']);
					exit();
				}
				else{
					$this->load->library('Notify_lib');
					$ntf = new Notify_lib();
					$ntf->send(
							'New Home Service',
							$sender['name'] . ' - '.$sender['position_name'].' ('.$sender['branch_name'].') anda mendapatkan tugas atas nama pasien '.$data['nama'].' dan jam '.$jam,
							$this->user_id,
							$idReceivers,
							['id' => $id_hs,'date' => $data['date'],'branch_id' => $data['branch_id']],
							'homeservice',
							true
						);
				}
			}


			$del = '%"id_hs":"hs_'.$this->input->post('id').'"%';
			$delete_notif = $this->db->query("delete from notifications where data LIKE '$del'");
			$delete_notif = $this->db->query("delete from admin_mis.notifications where data LIKE '$del'");
        }
        if($id_hs){
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        }

        echo json_encode($result);
    }



	public function book_hs()
    {
        $result['status'] = false;
        $result['message'] = 'Data gagal disimpan.';
		$data['id'] = $this->input->post('nama');
        $data['date'] = $this->input->post('date');
        $data['pid'] = $this->input->post('pid');
        $data['time_id'] = $this->input->post('time_id');
        $data['nama'] = $this->input->post('nama');
        $data['alamat'] = $this->input->post('alamat');
        $data['no_telp'] = $this->input->post('no_telp');
        $data['dokter'] = $this->input->post('dokter');
        $data['jumlah_pasien'] = $this->input->post('jumlah_pasien');
        $data['pemeriksaan'] = $this->input->post('pemeriksaan');
        $data['status'] = $this->input->post('status');
        $data['pay_id'] = $this->input->post('pay_id');
        $data['petugas_id'] = $this->user_id; //$this->input->post('petugas_id');
        $data['ptgshs_id'] = $this->input->post('ptgshs_id');
        $data['catatan'] = $this->input->post('catatan');
        $data['ttd_pasien'] = $this->input->post('ttd_pasien');
        $data['branch_id'] = $this->input->post('branch_id'); //$this->branch_id;
        $data['creator_id'] = $this->user_id;
        $data['created_date'] = date('Y-m-d H:i:s');
		$data['booked'] = $this->input->post('booked');

		// stay

        $id_hs = $this->input->post('id');
		$id_multi = explode(",",$this->input->post('id_multi'));
		if($data['ptgshs_id']==''){
			$datenow = date("Y-m-d H:i:s");
			// stay
			// if($this->m_hs->get_where(array('abbr_hs' => $this->input->post('abbr'), "'".$this->input->post('date')."'".' between start_date and end_date' => null ,'branch_id'=>$this->input->post('branch_id')), 'hs_initial_petugas_hs')->num_rows() == 0) 
			// if($this->m_hs->get_where(array('abbr_hs' => $this->input->post('abbr'), 'start_date' => $this->input->post('date') ,'branch_id'=>$this->input->post('branch_id')), 'hs_initial_petugas_hs')->num_rows() == 0) 
			$abbr_ = $this->input->post('abbr');
			$date_ = $this->input->post('date');
			$br_ = $this->input->post('branch_id');
			$cek_hs_initial_petugas_hs = $this->db->query("select * from hs_initial_petugas_hs where abbr_hs = '$abbr_'  and start_date = '$date_' and branch_id = '$br_'")->row();
			if ($cek_hs_initial_petugas_hs == 0)
			{
				$datax['id_user'] = 0;
				$datax['abbr_hs'] = $this->input->post('abbr');
				$datax['start_date'] = $this->input->post('date');
				$datax['end_date'] = $this->input->post('date');
				$datax['branch_id'] = $this->input->post('branch_id');
				$datax['creator_id'] = $this->user_id;
				$datax['created_at'] = $datenow;
				$datax['ptgshs_id'] = $this->input->post('nama');
				$data['ptgshs_id'] = $this->m_masterhs->insert_data($datax, 'hs_initial_petugas_hs');
				$data['ptgshs_id'] = $this->m_masterhs->insert_data($datax, 'admin_mis.hs_initial_petugas_hs');
			}
			else{
				// $val = $this->m_hs->get_where(array('abbr_hs' => $this->input->post('abbr'), "'".$this->input->post('date')."'".' between start_date and end_date' => null,'branch_id'=>$this->input->post('branch_id')), 'hs_initial_petugas_hs')->row();
				$val = $this->m_hs->get_where(array('abbr_hs' => $this->input->post('abbr'), 'start_date' => $this->input->post('date') ,'branch_id'=>$this->input->post('branch_id')), 'hs_initial_petugas_hs')->row();
				$data['ptgshs_id'] = $val->ptgshs_id;
			}
			
			//NOTIF BRANCH MANAGER
			
			$this->load->model('restapi/user_model');

            $user_kepala_cabang = $this->user_model->many_user([
                'where_in' => [
                    'pos.id_position' => [7],       /* position branch manager */
                ],
                'where_not_in' => [
                    '_.id_user' => [62]  /* exclude dr edi */
                ],
                'where' => ['_.STATUS !=' => 'KELUAR','_.branch_id' => $this->input->post('branch_id')]
            ]);
			
			$idReceivers = [];
            foreach ($user_kepala_cabang as $key => $user) {
                $idReceivers[] = $user['id_user'];
            }
			//$idReceivers[] = 47;
			$this->load->library('Notify_lib');
			$ntf = new Notify_lib();
			$ntf->send(
				'New Home Service',
				'Mohon isi petugas hs yang terlewatkan dengan klik ini',
				$this->user_id,
				$idReceivers,
				['id' => $data['ptgshs_id'],'date' => $datenow,'branch_id' => $this->input->post('branch_id')],
				'petugashs',
				true
			);
			
			
			
		}
        if(!empty($id_hs)){
            
			if(count($id_multi)>0){
				foreach($id_multi as $v){
					$where['id'] = $v;
					$this->m_hs->delete_data($where, $this->model);
					$this->m_hs->delete_data($where, "admin_mis.homeservice");
				}
				
			}
			for ($x = $data['time_id']; $x <= $this->input->post('time_id_sampai'); $x++) {
				if($this->m_hs->get_where(array('time_id' => $x, 'ptgshs_id' => $data['ptgshs_id'], 'date' => $data['date'], 'status <> "Cancel"' => null ), $this->model)->num_rows() == 0) {
					$data['time_id'] = $x;
					$id_hs = $this->m_hs->insert_data($data, $this->model);
					$id_hs = $this->m_hs->insert_data($data, 'admin_mis.homeservice');
				}
			}
			
			$data_ptgs = $this->m_hs->edit_data(array('ptgshs_id' => $data['ptgshs_id']),'hs_initial_petugas_hs')->row();
			$data_time1 = $this->m_hs->edit_data(array('time_id' => $this->input->post('time_id')),'hs_time')->row();
			$jam = $data_time1->time_name;
			if($this->input->post('time_id_sampai')!=''){
				$data_time2 = $this->m_hs->edit_data(array('time_id' => $this->input->post('time_id_sampai')),'hs_time')->row();
				$jam = $data_time1->time_name.' '.($data_time1->time_name!=$data_time2->time_name?' - '.$data_time2->time_name:"");
			}

			// stay
			if($data_ptgs->id_user!=0){
				$this->load->model('restapi/user_model');
				$sender = $this->user_model->one_user(['where' => ['_.id_user' => $data_ptgs->id_user]]);
				$idReceivers[] = $sender['id_user'];
				if($sender == null) {
					echo json_encode(['status' => false, 'message' => 'Invalid Sender 3']);
					exit();
				}
				else{
					$this->load->library('Notify_lib');
					$ntf = new Notify_lib();
					$ntf->send(
							'New Home Service',
							$sender['name'] . ' - '.$sender['position_name'].' ('.$sender['branch_name'].') anda mendapatkan tugas atas nama pasien '.$data['nama'].' dan jam '.$jam,
							$this->user_id,
							$idReceivers,
							['id' => $id_hs,'date' => $data['date'],'branch_id' => $data['branch_id']],
							'homeservice',
							true
						);
				}
			}
			
			/*$where['id'] = $this->input->post('id');
            if($this->m_hs->get_where(array('id' => $this->input->post('id'), 'ptgshs_id' => "> ''" ), $this->model)->num_rows() > 0) {
                $this->m_hs->delete_data($where, $data, $this->model);
                if(!empty($this->input->post('time_id_sampai')) && $this->input->post('time_id_sampai') > $this->input->post('time_id')) {
                    for ($x = $data['time_id']; $x <= $this->input->post('time_id_sampai'); $x++) {
                        if($this->m_hs->get_where(array('time_id' => $x, 'ptgshs_id' => $data['ptgshs_id'], 'date' => $data['date'] ), $this->model)->num_rows() == 0) {
                            $data['time_id'] = $x;
                            $id_hs = $this->m_hs->insert_data($data, $this->model);
                        }
                    }

                } else {
                    $id_hs = $this->m_hs->insert_data($data, $this->model);
                }
            } else {
                $id_hs = $this->m_hs->update_data($where, $data, $this->model);
                                    for ($x = $data['time_id']; $x <= $this->input->post('time_id_sampai'); $x++) {
                        if($this->m_hs->get_where(array('time_id' => $x, 'ptgshs_id' => $data['ptgshs_id'], 'date' => $data['date'] ), $this->model)->num_rows() == 0) {
                            $data['time_id'] = $x;
                            $id_hs = $this->m_hs->insert_data($data, $this->model);
                        }
                    }
                $result['status'] = true;
                $result['message'] = 'Data berhasil disimpan.';
            }*/

        }
		else{
			if(!empty($this->input->post('time_id_sampai')) && $this->input->post('time_id_sampai') > $this->input->post('time_id')) {
				for ($x = $data['time_id']; $x <= $this->input->post('time_id_sampai'); $x++) {
					if($this->m_hs->get_where(array('time_id' => $x, 'ptgshs_id' => $data['ptgshs_id'], 'date' => $data['date'], 'status <> "Cancel"' => null ), $this->model)->num_rows() == 0) {
						$data['time_id'] = $x;
            			$id_hs = $this->m_hs->insert_data($data, $this->model);
						$id_hs = $this->m_hs->insert_data($data, 'admin_mis.homeservice');
					}
				}

			} else {
            	$id_hs = $this->m_hs->insert_data($data, $this->model);
				$id_hs = $this->m_hs->insert_data($data, 'admin_mis.homeservice');
			}
			
			
			$data_ptgs = $this->m_hs->edit_data(array('ptgshs_id' => $data['ptgshs_id']),'hs_initial_petugas_hs')->row();
			$data_time1 = $this->m_hs->edit_data(array('time_id' => $this->input->post('time_id')),'hs_time')->row();
			
			$jam = $data_time1->time_name;
			if($this->input->post('time_id_sampai')!=''){
				$data_time2 = $this->m_hs->edit_data(array('time_id' => $this->input->post('time_id_sampai')),'hs_time')->row();
				$jam = $data_time1->time_name.' '.($data_time1->time_name!=$data_time2->time_name?' - '.$data_time2->time_name:"");
			}
			
			//$data_time2 = $this->m_hs->edit_data(array('time_id' => $this->input->post('time_id_sampai')),'hs_time')->row();
			//$jam = $data_time1->time_name.' '.($data_time1->time_name!=$data_time2->time_name?' - '.$data_time2->time_name:"");
			
			if($data_ptgs->id_user!=0){
				$this->load->model('restapi/user_model');
				$sender = null;
				if(isset($data_ptgs->id_user)) $sender = $this->user_model->one_user(['where' => ['_.id_user' => $data_ptgs->id_user]]);
				$idReceivers[] = $sender['id_user'];
				if($sender == null) {
					// echo json_encode(['status' => false, 'message' => 'Invalid Sender 4']);
					// exit();
				}
				else{
					// stay

					// $this->load->library('Notify_lib');
					// $ntf = new Notify_lib();
					// $ntf->send(
					// 		'New Home Service',
					// 		$sender['name'] . ' - '.$sender['position_name'].' ('.$sender['branch_name'].') anda mendapatkan tugas atas nama pasien '.$data['nama'].' dan jam '.$jam,
					// 		$this->user_id,
					// 		$idReceivers,
					// 		['id' => $id_hs,'date' => $data['date'],'branch_id' => $data['branch_id']],
					// 		'homeservice',
					// 		true
					// 	);
				}
			}else{
				// $this->load->model('restapi/user_model');
				// $sender = null;
				// $sender = $this->user_model->one_user(['where' => ['_.id_user' => $this->user_id]]);
				// $idReceivers[] = $sender['id_user'];
				// if($sender == null) {
				// 	echo json_encode(['status' => false, 'message' => 'Invalid Sender']);
				// 	exit();
				// }
				// else{
				// 	$this->load->library('Notify_lib');
				// 	$ntf = new Notify_lib();
				// 	$ntf->send(
				// 			'New Home Service',
				// 			$sender['name'] . ' - '.$sender['position_name'].' ('.$sender['branch_name'].') anda perlu mengisi data pasien ',
				// 			$this->user_id,
				// 			$idReceivers,
				// 			['id' => $id_hs,'date' => $data['date'],'branch_id' => $data['branch_id']],
				// 			'homeservice',
				// 			true
				// 		);
				// }
			}

			$timestamp = strtotime($this->input->post('date'));


			$this->load->model('restapi/user_model');
			$sender = null;
			$sender = $this->user_model->one_user(['where' => ['_.id_user' => $this->user_id]]);
			$idReceivers[] = $sender['id_user'];
			if($sender == null) {
				echo json_encode(['status' => false, 'message' => 'Invalid Sender 5']);
				exit();
			}
			else{
				$this->load->library('Notify_lib');
				$ntf = new Notify_lib();
				$ntf->send(
						'New Home Service',
						$sender['name'] . ' - '.$sender['position_name'].' ('.$sender['branch_name'].') anda perlu mengisi data pasien pada tanggal '. date("d-m-Y",$timestamp) . ' dan jam ' . $jam,
						$this->user_id,
						$idReceivers,
						['id' => $id_hs, 'id_hs' => "hs_".$id_hs,'date' => $data['date'],'branch_id' => $data['branch_id']],
						'bookinghs',
						true
					);
			}


        }
        if($id_hs){
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        }

        echo json_encode($result);
    }

    public function edit_hs()
    {
        $result['status'] = false;
        $result['message'] = 'Data tidak ditemukan.';

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['id'] = $this->input->post('id');


        $result['status'] = true;
        $result['message'] = 'Data ditemukan';
        $edit_data = $this->m_hs->get_by_id($this->input->post('id'))->row();
        if(!empty($edit_data)){
            $result['data'] = (array) $edit_data;

        }

        echo json_encode($result);
    }
	
	public function edit_hs_multi()
    {
        $result['status'] = false;
        $result['message'] = 'Data tidak ditemukan.';

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['id'] = $this->input->post('id');
		
		$explode = explode(",",$this->input->post('id'));
		$time_id_sampai = '';
		$id_multi = [];
		$time_id_multi = [];
		$time_name_multi = [];
		if(count($explode)>0)
		{
			$id = end($explode);
			$edit_data = $this->m_hs->get_by_id($id)->row();
			$time_id_sampai = $edit_data->time_id;
			
			foreach($explode as $v){
				$edit_data = $this->m_hs->get_by_id($v)->row();
				$id_multi[] = $edit_data->id;
				$time_id_multi[] = $edit_data->time_id;
				$time_name_multi[] = $edit_data->time_name;
			}
		}
        $result['status'] = true;
        $result['message'] = 'Data ditemukan';
        $edit_data = $this->m_hs->get_by_id($this->input->post('id'))->row();
        if(!empty($edit_data)){
            $result['data'] = (array) $edit_data;
			$result['data']['time_id_sampai'] = ((count($explode)>0)?$time_id_sampai:"");
			$result['data']['id_multi'] = ((count($explode)>0)?implode(",",$id_multi):"");
			$result['data']['time_id_multi'] = ((count($explode)>0)?implode(",",$time_id_multi):"");
			$result['data']['time_name_multi'] = ((count($explode)>0)?implode(",",$time_name_multi):"");
        }

        echo json_encode($result);
    }


    public function setSelesai()
    {
        $result['status'] = false;
        $result['message'] = 'Data gagal disimpan.';

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['status'] = $this->input->post('status');
        $data['time_selesai'] = $this->input->post('time_selesai');

        $id_hs = $this->input->post('id');
		$id_multi = explode(",",$this->input->post('id'));


        if (!empty($id_hs)) {
			
			if(count($id_multi)>0){
				foreach($id_multi as $v){
					$where['id'] = $v;
					$id_hs = $this->m_hs->update_data($where, $data, $this->model);
					$id_hs = $this->m_hs->update_data($where, $data, 'admin_mis.homeservice');
				}
				
			}
			else{
				$where['id'] = $this->input->post('id');
				$id_hs = $this->m_hs->update_data($where, $data, $this->model);
				$id_hs = $this->m_hs->update_data($where, $data, 'admin_mis.homeservice');
			}
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        } else {
            $id_hs = $this->m_hs->insert_data($data, $this->model);
			$id_hs = $this->m_hs->insert_data($data, 'admin_mis.homeservice');
			
		}
        if ($id_hs) {
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        }

        echo json_encode($result);
    }

    public function get_available_time() {
        $result['status'] = false;
        $result['message'] = 'Data tidak ditemukan.';

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $date = $this->input->post('date');
        $ptgshs_id = $this->input->post('ptgshs_id');


        $result['status'] = true;
        $result['message'] = 'Data ditemukan';
        $edit_data = $this->m_hs->get_available_time($date, $ptgshs_id, $this->branch_id)->result();
        if(!empty($edit_data)){
            $result['data'] = (array) $edit_data;

        }

        echo json_encode($result);
    }

    public function delete_hs()
    {
        $result['status'] = false;
        $result['message'] = 'Data Berhasil di cancel';

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['status'] = 'Cancel';
        $data['reason'] = $this->input->post('reason');
		$data['deleted'] = 1;
        
		$id_hs = false;
		$id_multi = explode(",",$this->input->post('id'));
		if(count($id_multi)>0){
			foreach($id_multi as $v){
				$where['id'] = $v;
				$id_hs = $this->m_hs->update_data($where, $data, $this->model);
				$id_hs = $this->m_hs->update_data($where, $data, 'admin_mis.homeservice');
			}
			
		}
		else{
			$where['id'] = $this->input->post('id');
			$id_hs = $this->m_hs->update_data($where, $data, $this->model);
			$id_hs = $this->m_hs->update_data($where, $data, 'admin_mis.homeservice');
		}
        if($id_hs){
            $result['status'] = true;
            $result['message'] = 'Home service di cancel.';
        }

        echo json_encode($result);
    }


    public function delete_booking()
    {
        $result['status'] = false;
        $result['message'] = 'Data Berhasil di cancel';

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        // $data['status'] = 'Cancel';
        // $data['reason'] = $this->input->post('reason');
		$data['deleted'] = 1;
        
		$id_hs = false;
		$id_multi = explode(",",$this->input->post('id'));
		if(count($id_multi)>0){
			foreach($id_multi as $v){
				$where['id'] = $v;
				$id_hs = $this->m_hs->update_data($where, $data, $this->model);
				$id_hs = $this->m_hs->update_data($where, $data, "admin_mis.homeservice");
				$where_['id'] = $v;

				$del = '%"id_hs":"hs_'.$this->input->post('id').'"%';
				$delete_notif = $this->db->query("delete from notifications where data LIKE '$del'");
				$delete_notif = $this->db->query("delete from admin_mis.notifications where data LIKE '$del'");
			}
			
		}
		else{
			$where['id'] = $this->input->post('id');
			$id_hs = $this->m_hs->update_data($where, $data, $this->model);
			$id_hs = $this->m_hs->update_data($where, $data, "admin_mis.homeservice");

			$del = '%"id_hs":"hs_'.$this->input->post('id').'"%';
			$delete_notif = $this->db->query("delete from notifications where data LIKE '$del'");
			$delete_notif = $this->db->query("delete from admin_mis.notifications where data LIKE '$del'");
		}
        if($id_hs){
            $result['status'] = true;
            $result['message'] = 'Home service di cancel.';
        }

        echo json_encode($result);
    }


public function get_lokasi()
{



$data = array();
$data_abbr = $this->m_masterhs->get_all_masterabbr($this->branch_id)->result();

if (!empty($data_abbr)) {
$i = 0;
foreach ($data_abbr as $key => $value) {
$data['data'][$i]['no'] = $i+1;
$data['data'][$i]['abbr_name'] = $value->abbr_name;
$data['data'][$i]['lokasi'] = $value->lokasi;

$i++;
}
}
echo json_encode($data);
}

public function exportv2()
{
	$request = $this->input->post();
	$cabang = $request['cabang'];
	$cabangname = $request['cabang_name'];
	$dari = $request['dari'];
	$sampai = $request['sampai'];

	$hs = $this->m_hs->getDataHomeServiceForExportV2($cabang, $dari, $sampai)->result();

	$time = $this->m_hs->getTimeHomeService($cabang, $dari, $sampai)->result();
	$totalwaktu = json_encode(count($time));

	$spreadsheet = new PHPExcel(); // instantiate Spreadsheet
	$sheet = $spreadsheet->setActiveSheetIndex(0);
	$spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true);


	$spreadsheet->getActiveSheet()->getStyle('B1')->getFont()->setBold( true );
	$spreadsheet->getActiveSheet()->getStyle('I1')->getFont()->setBold( true );
	$spreadsheet->getActiveSheet()->getStyle('I2')->getFont()->setBold( true );
	$spreadsheet->getActiveSheet()->getStyle('A4')->getFont()->setBold( true );
	$spreadsheet->getActiveSheet()->getStyle('B4')->getFont()->setBold( true );
	$spreadsheet->getActiveSheet()->getStyle('C4')->getFont()->setBold( true );

	$kolom = 'D';
	$x = 4;
	$no = 1;
	$sheet->setCellValue('B1', 'DATA HOME SERVICE');
	$sheet->setCellValue('I1', 'Cabang:');
	$sheet->setCellValue('J1', $cabangname);
	$sheet->setCellValue('I2', 'Tanggal:');
	$sheet->setCellValue('J2', $dari.' s.d. ' .$sampai);
	$sheet->setCellValue('A4', 'No.');
	$sheet->setCellValue('B4', 'Tanggal');
	$sheet->setCellValue('C4', 'Petugas');
	for($i=0; $i<$totalwaktu; $i++){
		$sheet->setCellValue($kolom.$x, $time[$i]->time_name);
		$spreadsheet->getActiveSheet()->getStyle($kolom.$x)->getFont()->setBold(true);
		$kolom++;
	}
	$sheet->setCellValue($kolom.$x, 'Total Pasien');
	$spreadsheet->getActiveSheet()->getStyle($kolom.$x)->getFont()->setBold(true);
	$kolom++;
	$sheet->setCellValue($kolom.$x, 'Total Lokasi');
	$spreadsheet->getActiveSheet()->getStyle($kolom.$x)->getFont()->setBold(true);
	$kolom++;
	$sheet->setCellValue($kolom.$x, 'Jam Tiba Di Lab');
	$spreadsheet->getActiveSheet()->getStyle($kolom.$x)->getFont()->setBold(true);
	$kolom++;
	$sheet->setCellValue($kolom.$x, 'Catatan');
	$spreadsheet->getActiveSheet()->getStyle($kolom.$x)->getFont()->setBold(true);
	$kolom++;
	$sheet->setCellValue($kolom.$x, 'Reason');
	$spreadsheet->getActiveSheet()->getStyle($kolom.$x)->getFont()->setBold(true);

	$dataKolom = 'A';
	$dataBaris = 5;
	$temp = "";
	$tempw = "";
	$tempn = "";
	$test = 0;
	$ganti = false;
	foreach($hs as $hs){
		$dom = false;
		if (($temp != $hs->abbr_hs) || ($tempw != $hs->date)){
			$tempc = [];
			$tempr = [];
			$jangkrik = 0;
			$jangkrik2 = 0;
			$sheet->setCellValue($dataKolom.$dataBaris, $no);
			$dataKolom++;
			$sheet->setCellValue($dataKolom.$dataBaris, $hs->date);
			$dataKolom++;
			$sheet->setCellValue($dataKolom.$dataBaris, $hs->abbr_hs);
			$dataKolom++;
			$temp = $hs->abbr_hs;
			$tempw = $hs->date;
			$test = 0;
			$dom = true;
		}else{
			$dataKolom++;
			$dataKolom++;
			$dataKolom++;
			$test = 1;
		}

		for($i=0; $i<$totalwaktu; $i++){
			if($time[$i]->time_name == $hs->time_name) {
				$sheet->setCellValue($dataKolom.($dataBaris-$test), $hs->nama == $tempn ? $hs->jumlah_pasien.' | PP' : $hs->jumlah_pasien);
				$jangkrik += $hs->jumlah_pasien;
				$jangkrik2 = $jangkrik2+1;
				$tempn = $hs->nama;
				if ($hs->catatan != "") {
					array_push($tempc, $time[$i]->time_name." = ".$hs->catatan);
				}
				if ($hs->reason != "") {
					array_push($tempr, $time[$i]->time_name." = ".$hs->reason);
				}
			}
			$dataKolom++;
		}

		if ($dom) {
			$ganti = true;
			$dataBaris++;
			$no++;
		}
		if($ganti){
			$sheet->setCellValue($dataKolom.($dataBaris-1), $jangkrik);
			$dataKolom++;
			$sheet->setCellValue($dataKolom.($dataBaris-1), $jangkrik2);
			$dataKolom++;
			// Ini jam tiba di lab

			$sheet->setCellValue($dataKolom.($dataBaris-1), $hs->time_selesai);

			$dataKolom++;
			if ($hs->catatan != "") {
				$sheet->setCellValue($dataKolom.($dataBaris-1), implode(', ',$tempc));
			}
			$dataKolom++;
			if ($hs->reason != "") {
				$sheet->setCellValue($dataKolom.($dataBaris-1), implode(', ',$tempr));
			}
			// $tempc = [];
			// for($h=0;$h<=count($tempc);$h++){
			//   unset($tempc[$h]);
			// }
		}
		$dataKolom = 'A';
	}

	// Set judul file excel nya
	$spreadsheet->getActiveSheet(0)->setTitle("Data_Home_Service");

  $u_agent = $_SERVER['HTTP_USER_AGENT'];
      $platform = 'Unknown';

      //Get the platform
      if (preg_match('/linux/i', $u_agent)) {
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Data_Home_Service.xlsx"'); // Set nama file excel nya
        $write = PHPExcel_IOFactory::createWriter($spreadsheet, 'Excel2007');
      } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
          header('Content-Type: application/vnd.ms-excel');
          header('Content-Disposition: attachment; filename="Data_Home_Service.xls"'); // Set nama file excel nya
          header('Cache-Control: max-age=0');
          $write = PHPExcel_IOFactory::createWriter($spreadsheet, 'Excel5');
      } elseif (preg_match('/windows|win32/i', $u_agent)) {
          header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
          header('Content-Disposition: attachment; filename="Data_Home_Service.xlsx"'); // Set nama file excel nya
          $write = PHPExcel_IOFactory::createWriter($spreadsheet, 'Excel2007');
      }
	$write->save('php://output');
}


public function get_notification() {
	if (!$this->input->is_ajax_request()) {
		exit('No direct script access allowed');
	}

	$data = array();
	$data['count'] = "";

	$data_notification = $this->m_mis->get_notification($this->user_id)->result();
	if (!empty($data_notification)) {
		foreach ($data_notification as $key => $value) {
			$data[$key]['id'] = $value->id;
			$data[$key]['from_id'] = $value->from_id;
			$data[$key]['photo_profile'] = $value->photo_profile;
			$data[$key]['creator_name'] = $value->creator_name;
			$data[$key]['read'] = $value->read;										 
			$data[$key]['to_id'] = $value->to_id;
			$data[$key]['description'] = $value->description;
			$data[$key]['data'] = json_decode($value->data);
			// $data[$key]['dailyreports_id'] = json_decode($value->data)->dailyreports_id;
			// $data[$key]['data'] = json_decode($value->data);
			$data[$key]['data'] = null;
			$data[$key]['dailyreports_id'] = "";
			if (isset($value->data)) {
				$data_ = json_decode($value->data);
				$data[$key]['data'] = $data_;
				if (isset($data_->dailyreports_id)) {
					$data[$key]['dailyreports_id'] = $data_->dailyreports_id;
				}
			}
			// $data[$key]['dailyreports_id'] = json_decode($value->data)->dailyreports_id;
			$time = date('d-M-y H:i', strtotime($value->time));
			$data[$key]['time'] = $time;
		}
		$data['count'] = $this->m_mis->get_notification($this->user_id)->num_rows();
		$data['noread'] = $this->m_mis->no_read_notification($this->user_id)->num_rows();																		

	}

	echo json_encode($data);
}

public function update_notification() {
	if (!$this->input->is_ajax_request()) {
		exit('No direct script access allowed');
	}

	$id = $this->input->post("id");

	$data['read'] = 1;

	$this->m_mis->update_data(array(
		'id' => $id
	) , $data, "notifications");

	$this->m_mis->update_data(array(
		'id' => $id
	) , $data, "mis.notifications");

	echo json_encode(array(
		"status" => true
	));
}


public function read_all_notif() {
	if (!$this->input->is_ajax_request()) {
		exit('No direct script access allowed');
	}

	//$id = $this->input->post('id');
	$data = array(
		"read" => 1,
	);

	$this->m_mis->update_data(array(
		"data not like '%doc_id%'" => null,
		"data not like '%id_announcement%'" => null,
		"cuti_flag" => 0,
		'to_id' => $this->user_id
	) , $data, "notifications");

	$this->m_mis->update_data(array(
		"data not like '%doc_id%'" => null,
		"data not like '%id_announcement%'" => null,
		"cuti_flag" => 0,
		'to_id' => $this->user_id
	) , $data, "mis.notifications");

	echo json_encode(array(
		"status" => true
	));
}

public function get_notificationAdd() {
	if (!$this->input->is_ajax_request()) {
		exit('No direct script access allowed');
	}

	$data = array();
	$data['count'] = "";

	$data_notification = $this->m_mis->get_notificationAdd($this->user_id)->result();
	if (!empty($data_notification)) {
		foreach ($data_notification as $key => $value) {
			$data[$key]['id'] = $value->id;
			$data[$key]['from_id'] = $value->from_id;
			$data[$key]['photo_profile'] = $value->photo_profile;
			$data[$key]['creator_name'] = $value->creator_name;
			$data[$key]['read'] = $value->read;										 
			$data[$key]['to_id'] = $value->to_id;
			$data[$key]['description'] = $value->description;
			$data[$key]['data'] = json_decode($value->data);
			// $data[$key]['dailyreports_id'] = json_decode($value->data)->dailyreports_id;
			// $data[$key]['data'] = json_decode($value->data);
			$data[$key]['data'] = null;
			$data[$key]['dailyreports_id'] = "";
			if (isset($value->data)) {
				$data_ = json_decode($value->data);
				$data[$key]['data'] = $data_;
				if (isset($data_->dailyreports_id)) {
					$data[$key]['dailyreports_id'] = $data_->dailyreports_id;
				}
			}
			// $data[$key]['dailyreports_id'] = json_decode($value->data)->dailyreports_id;
			$time = date('d-M-y H:i', strtotime($value->time));
			$data[$key]['time'] = $time;
		}
		$data['count'] = $this->m_mis->get_notificationAdd($this->user_id)->num_rows();
		$data['noread'] = $this->m_mis->no_read_notificationAdd($this->user_id)->num_rows();																		

	}

	echo json_encode($data);
}


}
