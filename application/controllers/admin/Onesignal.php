<?php

class Onesignal extends CI_Controller {

	function __construct(){
		parent::__construct();
		//cek login
        if ($this->session->userdata('status') != "login") {
            redirect(base_url() . 'welcome?pesan=belumlogin');
        }
        $this->user_id = $this->session->userdata('id_user');
        $this->first_name = $this->session->userdata('name');
        $this->last_name = $this->session->userdata('last_name');
        $this->position_name = $this->session->userdata('position_name');
        $this->branch_name = $this->session->userdata('branch_name');
	}

	function save(){
		$id = $this->input->post('id');
		$this->load->model('restapi/user_model');
		$r = $this->user_model->store_onesignal($this->user_id, $id, []);
		echo json_encode($r);
	}
}