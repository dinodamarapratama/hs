<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');
use Illuminate\Http\Request;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');


class Hr extends CI_Controller{
	public $model = 'users';
	public $model2 = 'bagian';
	public $model3 = 'position';
	
	public function __construct(){
        parent::__construct();
		//cek login
        $this->restapikey= $this->config->config['restapikey'];
        if ($this->session->userdata('status') != "login" && $this->input->post('auth_key') != $this->restapikey) {
            redirect(base_url() . 'welcome?pesan=belumlogin');
        }

		$this->user_id = $this->session->userdata('id_user');
		$this->id_position = $this->session->userdata('id_position');
		$this->id_department = $this->session->userdata('id_department');
		$this->branch_id = $this->session->userdata('branch_id');
		$this->first_name = $this->session->userdata('name');
		$this->last_name = $this->session->userdata('last_name');
		$this->position_name = $this->session->userdata('position_name');
		$this->branch_name = $this->session->userdata('branch_name');
		$this->load->library('upload');
		$this->load->library('misc');
		$this->load->model('m_profile');
		$this->load->model('m_cuti');
		$this->load->model('M_masterusers');

        ini_set('display_errors', 0);
        if (empty($this->input->post('auth_key'))) {
            $cek_user = $this->db->query("select * from master_menu m left join role_menu_dept r on r.id_menu = m.id_menu left join role_menu_pos p on p.id_menu = m.id_menu left join role_menu_user u on u.id_menu = m.id_menu where u.id_user =  $this->user_id and m.url = 'admin/hr' group by m.nama")->row();
            if ($cek_user->id_menu < 1) {
    
            echo "<script>
            alert('Anda tidak mempunyai Akses');
            window.location.href='dashboard';
            </script>";
            }
            else {}   
        } else {}


	}

	

	public function index($call = ''){

	

		$data = [
			'mobile_menu' => $this->mysidebar->build_menu_mobile(),
			'sidebar_menu' => $this->mysidebar->asHtml([], true)
		];
		
		helper_log("akses", "Akses Menu HR System");
		$data['model'] = $this->model;
		$data['model2'] = $this->model2;
		$data['model3'] = $this->model3;
		$data['master_depart'] = $this->M_masterusers->get_all_relations('departments')->result();
		$data['master_position'] = $this->M_masterusers->get_all_relations('position')->result();
		$data['master_bagian'] = $this->M_masterusers->get_all_relations('bagian')->result();
		$data['master_branch'] = $this->M_masterusers->get_all_relations('branch')->result();
		$data['propinsi'] = $this->m_profile->get_all_relations('m_ipropinsi')->result();
		$data['users'] = $this->db->query("SELECT users.name,users.last_name,id_user FROM users  where status in ('TETAP','KONTRAK') ORDER BY name asc")->result();

		/*count update data cuti karyawan*/
		//$this->updateCuti();

		// $data['FingerID'] = $this->getFingerId();
		
		$data['job_setting'] = $this->m_general->get_data_like('job','name','param_setting')->result();
		if ($call=='js') {
			$this->load->view('admin/hr/view_hr', $data);
		}else{
			$this->load->view('admin/inc/v_header', $data);
			$this->load->view('admin/hr/view_hr', $data);
			$this->load->view('admin/inc/v_footer', $data);    
		}

		
	}


	public function loadtab(){
		$data = [
			'mobile_menu' => $this->mysidebar->build_menu_mobile(),
			'sidebar_menu' => $this->mysidebar->asHtml([], true)
		];
		$data['model'] = $this->model;
		$data['master_depart'] = $this->M_masterusers->get_all_relations('departments')->result();
		$data['master_position'] = $this->M_masterusers->get_all_relations('position')->result();
		$data['master_bagian'] = $this->M_masterusers->get_all_relations('bagian')->result();
		$data['master_branch'] = $this->M_masterusers->get_all_relations('branch')->result();
		$data['propinsi'] = $this->m_profile->get_all_relations('m_ipropinsi')->result();
		$data['users'] = $this->db->query("SELECT users.name,users.last_name FROM users  where status in ('TETAP','KONTRAK') ORDER BY name asc")->result();

		/*count update data cuti karyawan*/
		//$this->updateCuti();

		// $data['FingerID'] = $this->getFingerId();
		
		$data['job_setting'] = $this->m_general->get_data_like('job','name','param_setting')->result();
		
		$id = $this->input->get('id');
		$this->load->view("admin/hr/".$id, $data);
	}

	public function idcard(){
		$data = [
			'mobile_menu' => $this->mysidebar->build_menu_mobile(),
			'sidebar_menu' => $this->mysidebar->asHtml([], true)
		];
		$data['model'] = $this->model;
		
		$this->load->view('admin/hr/v_idcard', $data);        
	}

	public function get_name_finger($idFinger=0){
		$data = $this->M_masterusers->getFingerIdName()->result();
		
		$d = [];
		foreach ($data as $key => $value) {
			$d[$value->FINGERID]=$value->name;
		}

		if ($idFinger>0) {
			return $d[$idFinger];
		}else{
			return $d;
		}
	}

	public function get_lastname_finger($idFinger=0){
		$data = $this->M_masterusers->getFingerIdLastName()->result();
		
		$d = [];
		foreach ($data as $key => $value) {
			$d[$value->FINGERID]=$value->last_name;
		}

		if ($idFinger>0) {
			return $d[$idFinger];
		}else{
			return $d;
		}
	}

	public function get_posisi_finger($idFinger=0){
		$data = $this->M_masterusers->getFingerIdPosisi()->result();
		
		$d = [];
		foreach ($data as $key => $value) {
			$d[$value->FINGERID]=$value->position_name;
		}

		if ($idFinger>0) {
			return $d[$idFinger];
		}else{
			return $d;
		}
	}

	public function get_finger(){
		if (!$this->input->is_ajax_request() && $direct==0) {
			exit('No direct script access allowed');
		}

		ini_set('memory_limit', '8048M');
		$data = array();
		
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$offset = empty($start) ? 0 : $start;
		$rows = empty($length) ? 10 : $length;
		$conds = array();
		$conds2 = array();
		
		$order_param = $this->input->post('order');
		if (!empty($order_param)) {
			$order_param = $order_param[0];
			$order_param['column'] = $order_param['column'];	
			
			if ($order_param['column']=='1') {
				$order_param['column'] = 'e.UserID';
			}
			else if ($order_param['column']=='2') {
				$order_param['column'] = '(select name from users u where e.UserID = u.FINGERID)';
			}	
			else if ($order_param['column']=='3') {
				$order_param['column'] = '(select position_name from v_user u where e.UserID = u.FINGERID)';
			}	
			else if ($order_param['column']=='4') {
				$order_param['column'] = 'e.DateLog';
			}	
			else if ($order_param['column']=='5') {
				$order_param['column'] = 'jam_datang';
			}	
			else if ($order_param['column']=='6') {
				$order_param['column'] = 'jam_pulang';
			}	
			else if ($order_param['column']=='7') {
				$order_param['column'] = '(select TimeLog from EventLog where UserID = e.UserID and DateLog=e.DateLog and FKMode=3 order by id asc limit 1)';
			}	
			else if ($order_param['column']=='8') {
				$order_param['column'] = '(select TimeLog from EventLog where UserID = e.UserID and DateLog=e.DateLog and FKMode=3 order by id desc limit 1)';
			}
			else if ($order_param['column']=='9') {
				$order_param['column'] = '(select branch_id from users u where e.UserID = u.FINGERID limit 1)';
			}
			else if ($order_param['column']=='10') {
				$order_param['column'] = '(select `ed`.`Cabang` from EventLogDevice where `ed`.`DeviceID` = `e`.`DeviceID`)';
			}			
		}
		
		
		$searchx = $this->input->post('search');
		$search = '';
		if (!empty($searchx)) {
			$search = $searchx['value'];
		}
		
		$tgl1 = $this->input->post('tgl1');
		$tgl2 = $this->input->post('tgl2');
		$cabang = $this->input->post('cabang');
		$nama = $this->input->post('name');
		
		$user_name = $this->get_name_finger();
		$last_name = $this->get_lastname_finger();
		$position_name = $this->get_posisi_finger();

		if (!empty($tgl1)) {
			$conds['DateLog >='] = $tgl1;	
			$conds2['DateLog >='] = $tgl1;	
		}

		if (!empty($tgl2)) {
			$conds['DateLog <='] = $tgl2;		
			$conds2['DateLog <='] = $tgl2;			
		}

		if (!empty($cabang)) {
			$conds['Cabang'] = $cabang;		
			//$conds2['branch_name'] = $cabang;
			$conds2['(select branch_id from users u where e.UserID = u.FINGERID limit 1) = '.$cabang] = null;
		}
		
		if (!empty($nama)) {
			if ($nama != 'Semua'){
				$conds['name'] = $nama;		
				//$conds2['name'] = $nama;
				$conds2[$nama.' in (select id_user from users u where e.UserID = u.FINGERID)'] = null;
			}	
		}

		if (empty($this->session->userdata('fingerRecords'))) {
			$count = $this->db->query('select count(id)as jml from EventLog')->result();
			$sess = [
				'fingerRecords' => $count[0]->jml
			];
			$this->session->set_userdata($sess);
		}

		$data_finger = $this->M_masterusers->get_all_finger($conds2,$rows,$offset,0,$order_param,$search)->result();
		$data_finger_count = $this->M_masterusers->get_all_finger_count($conds2,$search)->num_rows();
		
		if (count($conds2) > 0) {
			// $totalData = count($data);
			$totalData = $data_finger_count;
		}
		
		// throw new \Exception(print_r($data_finger,true));
		// print_r($data_finger);
		// die();
		
		$totalData = $this->session->userdata('fingerRecords');
		if (count($conds2) > 1) {
			$totalData = count($data_finger);
		}

		if (!empty($data_finger)) {
			$i = 0;
			foreach ($data_finger as $key => $value) {
				$namaKaryawan = "";
				if (isset($user_name[$value->FingerId])){
					if(isset($last_name[$value->FingerId])){
						$namaKaryawan = $user_name[$value->FingerId] . ' ' . $last_name[$value->FingerId];
					}else{
						$namaKaryawan = $user_name[$value->FingerId];
					}
				}
				
				$data['data'][$i]['no'] = $i+1;
				$data['data'][$i]['FingerId'] = $value->FingerId;
				$data['data'][$i]['nama'] = $namaKaryawan;
				// $data['data'][$i]['nama'] = isset($user_name[$value->FingerId])?$user_name[$value->FingerId]:'';
				// $data['data'][$i]['nama_belakang'] = isset($last_name[$value->FingerId])?$last_name[$value->FingerId]:'';
				$data['data'][$i]['position_name'] = isset($position_name[$value->FingerId])?$position_name[$value->FingerId]:'';
				$date = date('d-M-y', strtotime($value->DateLog) );
				$data['data'][$i]['Date'] = $date;
				$data['data'][$i]['jam_datang'] = $value->jam_datang;
				$data['data'][$i]['jam_pulang'] = $value->jam_pulang;
				
				$data['data'][$i]['jam_istirahat'] = $value->jam_istirahat;
				$data['data'][$i]['jam_masuk_istirahat'] = $value->jam_masuk_istirahat;
				
				if($value->jam_istirahat == $value->jam_masuk_istirahat)
					$data['data'][$i]['jam_masuk_istirahat'] = 'tidak ada data';
			
				$data['data'][$i]['Cabang'] = $value->Cabang;
				$data['data'][$i]['Cabangf'] = $value->Cabangf;

				$i++;
			}

			
			// $data['sql'] = $this->db->last_query();
		}
		
		$data['draw'] = isset($_REQUEST['draw'])?$_REQUEST['draw']:1;
		$data['recordsTotal'] = $totalData;
		$data['recordsFiltered'] = $data_finger_count;
		
		echo json_encode($data);
	}

	function getFingerId($direct=0){
		/*get next FINGER ID*/
		$last_fingerID = $this->M_masterusers->getLastFingerID()[0]['FINGERID'];
		$cur_fingerID = $last_fingerID+1;
		$c = strlen($cur_fingerID);
		$print_FID = '';
		
		for ($i=0; $i < (5-$c); $i++) { 
			$print_FID .= '0';
		}

		$print_FID = $print_FID.''.$cur_fingerID;
		$data['FingerID'] = $print_FID;

		if ($direct==0) {
			echo json_encode($data);
		}else{
			return $print_FID;
		}
	}

	function load_data($id_user=0,$direct=0,$from='hr'){
		/*
			- update 2-9-20
			- add order kolom
			- add mapper column
		*/
		// if (!$this->input->is_ajax_request() && $direct==0) {
		// 	exit('No direct script access allowed');
		// }


		$column_mapper = [
			3 => 'NIP',
			'name',
			'department_name',
			'bagian_name',
			'name_position',
			'branch_name',
			'TGLMASUK',
			'STATUS',
			'masa_kerja',
			'TMTLAHIR',
			'TGLLAHIR',
			'usia',
			'TGLTETAP',
			'TGLKELUAR',
			'AWALKONTRAK',
			'AKHIRKONTRAK',
			'GENDER',
			'PENDIDIKAN',
			'AGAMA',
			'GOLDARAH',
			'STATUSKAWIN',
			'ALAMAT',
			'KODEPOS',
			'TELPONRUMAH',
			'TELPONHP',
			'NONPWP',
			'NOKTP',
			'NOJAMSOSTEK',
			'NOBPJSKES',
			'BUMIDA',
			'AVPENSION',
			'NOREK',
			'FINGERID',
		];
		$column_mapper2 = [
			3=> 'name',
			'last_name',
			'branch_name',
			'username',
			'password',
			'call_name',
			'inisial',
			'TMTLAHIR',
			'TGLLAHIR',
			'GENDER',
			'TELPONRUMAH',
			'TELPONHP',
			'PENDIDIKAN',
			'AGAMA',
			'GOLDARAH',
			'TINGGI',
			'BERAT',
			'ALAMAT',
			'PROPINSI',
			'KABUPATEN',
			'KECAMATAN',
			'KELDESA',
			'RTRW',
			'KODEPOS',
			'ALAMAT_DOMO',
			'PROPINSI_DOMO',
			'KABUPATEN_DOMO',
			'KECAMATAN_DOMO',
			'KELDESA_DOMO',
			'RTRW_DOMO',
			'KODEPOS_DOMO',
			'email',
			'NOKTP',
			'PERUSAHAAN',
			'NONPWP',
			'TIPEBAYAR',
			'BANK',
			'STATUSKAWIN',
			'STATUS',
			'FINGERID',
			'JMLANAK',
			'NOJAMSOSTEK',
			'BUMIDA',
			'NOBPJSKES',
			'TGLMASUK',
			'TGLTETAP',
			'TGLKELUAR',
			'AWALKONTRAK',
			'AKHIRKONTRAK',
			'CUTI',
			'cuti_keseluruhan',
			'masa_kerja'
		];

		$order_param = $this->input->post('order');
		if (!empty($order_param)) {
			$order_param = $order_param[0];
			if ($from=='bm') {
				$order_param['column'] = $column_mapper2[$order_param['column']];
			}else{
				$order_param['column'] = $column_mapper[$order_param['column']];	
			}
			/*conds*/
			if ($order_param['column']=='usia') {
				$order_param['column'] = 'TGLLAHIR';
				// $order_param['dir'] = (isset($order_param['dir'])&&$order_param['dir']=='asc')?'desc':'asc';
			}

			if ($order_param['column']=='masa_kerja') {
				$order_param['column'] = 'TGLMASUK';
				// $order_param['dir'] = (isset($order_param['dir'])&&$order_param['dir']=='asc')?'desc':'asc';	
			}	
		}

		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$offset = empty($start) ? 0 : $start;
		$rows = empty($length) ? 10 : $length;

		$param = [];
		if ($id_user) {
			$param['id_user'] = $id_user;
			// $FingerID = $this->getFingerId(1);
		}
		
		$search = $this->input->post('search')['value'];
		$status = $this->input->post('status');
		$branch = $this->input->post('branch');
		$department = $this->input->post('department');
		$position = $this->input->post('id_position');
		
		if (!empty($branch)) {
			$param['users.branch_id'] = $branch;
		}else{
			// $param['users.branch_id'] = $this->branch_id;
			if ($id_user) {
				unset($param['users.branch_id']);
			}
		}
		if (!empty($department)) {
			$param['users.id_department'] = $department;
		}else{
			// $param['users.id_department'] = $this->department;
			if ($id_user) {
				unset($param['users.id_department']);
			}
		}
		if (!empty($search)) {
			/*assume search input is for name only*/
			$param['or_param']['users.name like '] = '%'.$search.'%';
			$param['or_param']['users.last_name like '] = '%'.$search.'%';
			
			$param['or_param']['REPLACE(concat(lower(COALESCE(users.name,"")),lower(COALESCE(users.last_name,"")))," ","") like '] = '%'.str_replace(' ','',strtolower($search)).'%';
			
			
			$param['or_param']['users.NONPWP like '] = '%'.$search.'%';
			$param['or_param']['users.NOKTP like '] = '%'.$search.'%';
			$param['or_param']['users.FINGERID like '] = '%'.$search.'%';
			$param['or_param']['departments.name like '] = '%'.$search.'%';
			$param['or_param']['bagian.name like '] = '%'.$search.'%';
			$param['or_param']['branch.branch_name like '] = '%'.$search.'%';
			$param['or_param']['users.NIP like '] = '%'.$search.'%';
			$param['or_param']['users.STATUS like '] = '%'.$search.'%';
			$param['or_param']['users.TMTLAHIR like '] = '%'.$search.'%';
			$param['or_param']['users.GENDER like '] = '%'.$search.'%';
			$param['or_param']['users.PENDIDIKAN like '] = '%'.$search.'%';
			$param['or_param']['users.AGAMA like '] = '%'.$search.'%';
			$param['or_param']['users.GOLDARAH like '] = '%'.$search.'%';
			$param['or_param']['users.STATUSKAWIN like '] = '%'.$search.'%';
			$param['or_param']['users.ALAMAT like '] = '%'.$search.'%';
			$param['or_param']['users.KODEPOS like '] = '%'.$search.'%';
			$param['or_param']['users.TELPONRUMAH like '] = '%'.$search.'%';
			$param['or_param']['users.TELPONHP like '] = '%'.$search.'%';
			$param['or_param']['users.NOJAMSOSTEK like '] = '%'.$search.'%';
			$param['or_param']['users.NOBPJSKES like '] = '%'.$search.'%';
			$param['or_param']['users.BUMIDA like '] = '%'.$search.'%';
			$param['or_param']['users.NOREK like '] = '%'.$search.'%';
			
			if (strpos($search, '-') !== false) {
				$param['or_param']['DATE_FORMAT(users.TGLMASUK, "%m-%d-%y") like '] = '%'.$search.'%';
				$param['or_param']['DATE_FORMAT(users.AKHIRKONTRAK, "%m-%d-%y") like '] = '%'.$search.'%';
				$param['or_param']['DATE_FORMAT(users.AWALKONTRAK, "%m-%d-%y") like '] = '%'.$search.'%';
				$param['or_param']['DATE_FORMAT(users.TGLKELUAR, "%m-%d-%y") like '] = '%'.$search.'%';
				$param['or_param']['DATE_FORMAT(users.TGLTETAP, "%m-%d-%y") like '] = '%'.$search.'%';
				$param['or_param']['DATE_FORMAT(users.TGLLAHIR, "%m-%d-%y") like '] = '%'.$search.'%';
			}
			/*add behavior*/
			/*remove branch if filter active*/
			unset($param['users.branch_id']);
		}					  
		if (!empty($status)) {
			$param['STATUS'] = $status ;
		}else{
			//$param['STATUS !='] = 'KELUAR' ;
		}

		if (!empty($position)) {
			$param['users.id_position'] = $position;
		}

		if (!$this->session->userdata('karyawanRecords')) {
			$sess = [
				'karyawanRecords' => count($this->M_masterusers->load_data($param,$rows,$offset,1,$order_param)),
				'karyawanRecords_2' => count($this->M_masterusers->load_data_count($param))
			];
			$this->session->set_userdata($sess);
		}
        $all_ = $this->input->post("all_");
		if ($all_) {
			$data = $this->m_masterusers->load_data($param,$rows,$offset,1,$order_param);
		}else{
			$data = $this->m_masterusers->load_data($param,$rows,$offset,$direct,$order_param);
		}
		// $data = $this->m_masterusers->load_data($param,$rows,$offset,$direct,$order_param);
        $data2 = $this->m_masterusers->load_data_count($param);
        $totalData = $this->session->userdata('karyawanRecords_2');
        if (count($param) > 0) {
            // $totalData = count($data);
            $totalData = count($data2);
        }
        // throw new \Exception(print_r($totalData,true));;
		
		$newData = [];
		$i = 0;
		
		if (empty($data)) {
			$newData['data'] = [];
		}

		foreach ($data as $key => $value) {
			$newData['data'][$i]['no'] = $i+1;
			$newData['data'][$i]['id_user'] = $value['id_user'];
			$newData['data'][$i]['name'] = $value['name'];
			$newData['data'][$i]['branch_name'] = $value['branch_name'];
			$newData['data'][$i]['last_name'] = $value['last_name'];
			$newData['data'][$i]['username'] = $value['username'];
			$newData['data'][$i]['password'] = $value['password'];
			$newData['data'][$i]['call_name'] = $value['call_name'];
			$newData['data'][$i]['inisial'] = $value['inisial'];
			$newData['data'][$i]['GENDER'] = $value['GENDER'];
			$newData['data'][$i]['TMTLAHIR'] = $value['TMTLAHIR'];
			$newData['data'][$i]['TGLLAHIR'] = $value['TGLLAHIR'];

			$birthdate = new DateTime(date("Y-m-d",  strtotime(implode('-', array_reverse(explode('/', $value['TGLLAHIR']))))));
			// $today= new DateTime(date("Y-m-d",  strtotime(implode('-', array_reverse(explode('/', $value['TGLLAHIR']))))));           
			$today= new DateTime();           
			$age = $birthdate->diff($today)->y;
			$newData['data'][$i]['usia'] = $age;

			$newData['data'][$i]['TELPONRUMAH'] = $value['TELPONRUMAH'];
			$newData['data'][$i]['TELPONHP'] = $value['TELPONHP'];
			$newData['data'][$i]['ALAMAT_DOMO'] = $value['ALAMAT_DOMO'];
			$newData['data'][$i]['RTRW_DOMO'] = $value['RTRW_DOMO'];
			
			if (!empty($value['KELDESA_DOMO']) && intval($value['KELDESA_DOMO'])) {
				$param = ['id_kelurahan' => $value['KELDESA_DOMO']];
				$field = ['id_kelurahan','nama_kelurahan'];
				$res = $this->M_masterusers->getListByID('m_ikelurahan',$param,$field);
				$id_kel_domo = $value['KELDESA_DOMO'];
				$value['KELDESA_DOMO']=isset($res[0])?$res[0]['nama_kelurahan']:'';
			}

			if (!empty($value['KELDESA']) && intval($value['KELDESA'])) {
				$param = ['id_kelurahan' => $value['KELDESA']];
				$field = ['id_kelurahan','nama_kelurahan'];
				$res = $this->M_masterusers->getListByID('m_ikelurahan',$param,$field);
				$res = isset($res[0])?$res[0]['nama_kelurahan']:'';
				$id_kel = $value['KELDESA'];
				$value['KELDESA'] = $res;
			}

			if (!empty($value['KECAMATAN']) && intval($value['KECAMATAN'])) {
				$param = ['id_kecamatan' => $value['KECAMATAN']];
				$field = ['id_kecamatan','nama_kecamatan'];
				$res = $this->M_masterusers->getListByID('m_ikecamatan',$param,$field);
				$res = isset($res[0])?$res[0]['nama_kecamatan']:'';
				$id_kec = $value['KECAMATAN'];
				$value['KECAMATAN'] = $res;
			}

			if (!empty($value['KECAMATAN_DOMO']) && intval($value['KECAMATAN_DOMO'])) {
				$param = ['id_kecamatan' => $value['KECAMATAN_DOMO']];
				$field = ['id_kecamatan','nama_kecamatan'];
				$res = $this->M_masterusers->getListByID('m_ikecamatan',$param,$field);
				$res = isset($res[0])?$res[0]['nama_kecamatan']:'';
				$id_kec_domo = $value['KECAMATAN_DOMO'];
				$value['KECAMATAN_DOMO'] = $res;
			}

			if (!empty($value['KABUPATEN']) && intval($value['KABUPATEN'])) {
				$param = ['id_kabkota' => $value['KABUPATEN']];
				$field = ['id_kabkota','nama_kabkota'];
				$res = $this->M_masterusers->getListByID('m_ikabkota',$param,$field);
				$res = isset($res[0])?$res[0]['nama_kabkota']:'';
				$id_kab = $value['KABUPATEN'];
				$value['KABUPATEN'] = $res;
			}

			if (!empty($value['KABUPATEN_DOMO']) && intval($value['KABUPATEN_DOMO'])) {
				$param = ['id_kabkota' => $value['KABUPATEN_DOMO']];
				$field = ['id_kabkota','nama_kabkota'];
				$res = $this->M_masterusers->getListByID('m_ikabkota',$param,$field);
				$res = isset($res[0])?$res[0]['nama_kabkota']:'';
				$id_kab_domo = $value['KABUPATEN_DOMO'];
				$value['KABUPATEN_DOMO'] = $res;
			}

			if (!empty($value['PROPINSI']) && intval($value['PROPINSI'])) {
				$param = ['id_propinsi' => $value['PROPINSI']];
				$field = ['id_propinsi','nama_propinsi'];
				$res = $this->M_masterusers->getListByID('m_ipropinsi',$param,$field);
				$res = isset($res[0])?$res[0]['nama_propinsi']:'';
				$id_prop = $value['PROPINSI'];
				$value['PROPINSI'] = $res;
			}

			if (!empty($value['PROPINSI_DOMO']) && intval($value['PROPINSI_DOMO'])) {
				$param = ['id_propinsi' => $value['PROPINSI_DOMO']];
				$field = ['id_propinsi','nama_propinsi'];
				$res = $this->M_masterusers->getListByID('m_ipropinsi',$param,$field);
				$res = isset($res[0])?$res[0]['nama_propinsi']:'';
				$id_prop_domo = $value['PROPINSI_DOMO'];
				$value['PROPINSI_DOMO'] = $res;
			}
			
			$newData['data'][$i]['KELDESA_DOMO'] = $value['KELDESA_DOMO'];
			$newData['data'][$i]['KECAMATAN_DOMO'] = $value['KECAMATAN_DOMO'];
			$newData['data'][$i]['KABUPATEN_DOMO'] = $value['KABUPATEN_DOMO'];
			$newData['data'][$i]['PROPINSI_DOMO'] = $value['PROPINSI_DOMO'];
			$newData['data'][$i]['KODEPOS_DOMO'] = $value['KODEPOS_DOMO'];
			$newData['data'][$i]['ALAMAT'] = $value['ALAMAT'];
			$newData['data'][$i]['RTRW'] = $value['RTRW'];
			$newData['data'][$i]['KELDESA'] = $value['KELDESA'];
			$newData['data'][$i]['KECAMATAN'] = $value['KECAMATAN'];
			$newData['data'][$i]['KABUPATEN'] = $value['KABUPATEN'];
			$newData['data'][$i]['PROPINSI'] = $value['PROPINSI'];
			$newData['data'][$i]['KODEPOS'] = $value['KODEPOS'];

			$newData['data'][$i]['TGLMASUK'] = $value['TGLMASUK'];
			$newData['data'][$i]['TGLKELUAR'] = $value['TGLKELUAR'];
			$newData['data'][$i]['TGLTETAP'] = $value['TGLTETAP'];
			
			$newData['data'][$i]['AWALKONTRAK'] = date("Y-m-d", strtotime($value['AWALKONTRAK']));
			$newData['data'][$i]['AKHIRKONTRAK'] = date("Y-m-d", strtotime($value['AKHIRKONTRAK']));

			/*filter if null or format not fix*/
			$yAwal = substr($newData['data'][$i]['AWALKONTRAK'],0,4);
			$yAkhir = substr($newData['data'][$i]['AKHIRKONTRAK'],0,4); 
			if (intval($yAwal) < 1983) {
				/*
					kosongkan jika tanggal set default on db or null
					1983 : est of biomedika		
				*/
				$newData['data'][$i]['AWALKONTRAK'] = '';
			}
			if (intval($yAkhir) < 1983) {
				$newData['data'][$i]['AKHIRKONTRAK'] = '';
			}
			
			$newData['data'][$i]['branch_id'] = $value['branch_id'];
			$newData['data'][$i]['id_position'] = $value['id_position'];
			$newData['data'][$i]['name_position'] = $value['name_position'];
			$newData['data'][$i]['id_bagian'] = $value['id_bagian'];
			$newData['data'][$i]['bagian_name'] = $value['bagian_name'];
			$newData['data'][$i]['id_department'] = $value['id_department'];
			$newData['data'][$i]['department_name'] = $value['department_name'];
			$newData['data'][$i]['image'] = $value['image'];
			$newData['data'][$i]['email'] = $value['email'];
			$newData['data'][$i]['FINGERID'] = $value['FINGERID'];
			// if ($id_user) {
			// 	$newData['data'][$i]['FINGERID'] = $FingerID;
			// }
			$newData['data'][$i]['NIP'] = $value['NIP'];
			$newData['data'][$i]['NONPWP'] = $value['NONPWP'];
			$newData['data'][$i]['NOKTP'] = $value['NOKTP'];
			$newData['data'][$i]['TIPEBAYAR'] = $value['TIPEBAYAR'];
			$newData['data'][$i]['BANK'] = $value['BANK'];
			$newData['data'][$i]['NOREK'] = $value['NOREK'];
			$newData['data'][$i]['STATUSKAWIN'] = $value['STATUSKAWIN'];
			$newData['data'][$i]['STATUS'] = $value['STATUS'];
			$newData['data'][$i]['JMLANAK'] = $value['JMLANAK'];
			$newData['data'][$i]['NOJAMSOSTEK'] = $value['NOJAMSOSTEK'];
			$newData['data'][$i]['TGLJAMSOSTEK'] = $value['TGLJAMSOSTEK'];
			$newData['data'][$i]['BUMIDA'] = $value['BUMIDA'];
			$newData['data'][$i]['AVPENSION'] = $value['AVPENSION'];
			$newData['data'][$i]['AVKELAS'] = $value['AVKELAS'];
			$newData['data'][$i]['CUTI'] = $value['CUTI'];
			$newData['data'][$i]['cuti_keseluruhan'] = $value['cuti_keseluruhan'];
			$newData['data'][$i]['masabakti'] = $value['masabakti'];
			$newData['data'][$i]['masabakti_keseluruhan'] = $value['masabakti_keseluruhan'];
			$newData['data'][$i]['BERAT'] = $value['BERAT'];
			$newData['data'][$i]['TINGGI'] = $value['TINGGI'];
			$newData['data'][$i]['GOLDARAH'] = $value['GOLDARAH'];
			$newData['data'][$i]['PENDIDIKAN'] = $value['PENDIDIKAN'];
			$newData['data'][$i]['AGAMA'] = $value['AGAMA'];
			$newData['data'][$i]['NOBPJSKES'] = $value['NOBPJSKES'];
			$newData['data'][$i]['PERUSAHAAN'] = $value['PERUSAHAAN'];
			/*get id for selected pro,kab,kec,kel*/
			$newData['data'][$i]['ID_KELDESA_DOMO'] = isset($id_kel_domo)?$id_kel_domo:0;
			$newData['data'][$i]['ID_KELDESA'] = isset($id_kel)?$id_kel:0;
			$newData['data'][$i]['ID_KECAMATAN'] = isset($id_kec)?$id_kec:0;
			$newData['data'][$i]['ID_KECAMATAN_DOMO'] = isset($id_kec_domo)?$id_kec_domo:0;
			$newData['data'][$i]['ID_KABUPATEN'] = isset($id_kab)?$id_kab:0;
			$newData['data'][$i]['ID_KABUPATEN_DOMO'] = isset($id_kab_domo)?$id_kab_domo:0;
			$newData['data'][$i]['ID_PROPINSI'] = isset($id_prop)?$id_prop:0;
			$newData['data'][$i]['ID_PROPINSI_DOMO'] = isset($id_prop_domo)?$id_prop_domo:0;
			/**/
			/* add masa kerja */
			$now = date('Y-m-d');
			$tgl_masuk = $value['TGLMASUK'];
			
			if ($tgl_masuk == '0000-00-00' || $tgl_masuk == '0000-00-00 00:00:00' || $tgl_masuk == null || empty($tgl_masuk)) {
				$caption = 0;
			}else{
				$resTgl = $this->misc->selisih_waktu($now,$tgl_masuk);
				$caption = $resTgl['tahun'].' tahun, '. $resTgl['bulan'].' bulan, '.$resTgl['hari'].' hari';
			}
			$newData['data'][$i]['masa_kerja'] = $caption;
			/**/
			$newData['data'][$i]['lampiran_str'] = $value['lampiran_str'];
			$newData['data'][$i]['lampiran_mcu'] = $value['lampiran_mcu'];
			$newData['data'][$i]['lampiran_skk'] = $value['lampiran_skk'];

			$newData['data'][$i]['cabang_pajak'] = $value['cabang_pajak'];
			$newData['data'][$i]['tanggal_pemindahan'] = $value['tanggal_pemindahan'];
			
			$i++;
		}

		$newData['draw'] = isset($_REQUEST['draw'])?$_REQUEST['draw']:1;
		$newData['recordsTotal'] = $totalData;
		$newData['recordsFiltered'] = $totalData;
		
		if ($direct) {
			return json_encode($newData);
		}else{
			echo json_encode($newData);	
		}
	}

	function load_data_pelamar($id_applicant=0,$direct=0){
		if (!$this->input->is_ajax_request() && $direct==0) {
			exit('No direct script access allowed');
		}

		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$id_applicant = $this->input->post('id_applicant');
		$offset = empty($start) ? 0 : $start;
		$rows = empty($length) ? 10 : $length;

		$param = [];
		if ($id_applicant!=0) {
			$param['id_applicant'] = $id_applicant;
			//$FingerID = $this->getFingerId(1);
		}
		
		$search = $this->input->post('search')['value'];
		$status = $this->input->post('status_applicant');
		// $branch = $this->input->post('branch');
		if (!empty($search)) {
			/*assume search input is for name only*/
			$param['name like '] = '%'.$search.'%';
		}

		// if (!empty($branch)) {
		// 	$param['users.branch_id'] = $branch;
		// }else{
		// 	$param['users.branch_id'] = $this->branch_id;
		// 	if ($id_applicant) {
		// 		unset($param['users.branch_id']);
		// 	}
		// }
		
		if (!empty($status)) {
			if ($status != '1') $param['status_applicant'] = $status ;
		}else{
			//$param['status_applicant'] = '1' ;
		}
			
		if (!$this->session->userdata('pelamarRecords')) {
			$sess = [
				'pelamarRecords' => count($this->m_general->load_data($param,$rows,$offset,1,'applicant','id_applicant'))
			];
			$this->session->set_userdata($sess);
		}
		$data = $this->m_general->load_data($param,$rows,$offset,$direct,'applicant','id_applicant');
		$totalData = $this->session->userdata('pelamarRecords');
		if (count($param) > 0) {
			$totalData = count($data);
		}
		
		$newData = [];
		
		//$newData['data'][0][]='';
		if(count($data)>0){
			$i = 0;
			foreach ($data as $key => $value) {
				$newData['data'][$i]['no'] = $i+1;
				$newData['data'][$i]['id_applicant'] = $value['id_applicant'];
				
				/*
				if (!empty($value['KELDESA_DOMO']) && intval($value['KELDESA_DOMO'])) {
					$param = ['id_kelurahan' => $value['KELDESA_DOMO']];
					$field = ['id_kelurahan','nama_kelurahan'];
					$res = $this->M_masterusers->getListByID('m_ikelurahan',$param,$field);
					$value['KELDESA_DOMO']=isset($res[0])?$res[0]['nama_kelurahan']:'';
				}

				if (!empty($value['KELDESA']) && intval($value['KELDESA'])) {
					$param = ['id_kelurahan' => $value['KELDESA']];
					$field = ['id_kelurahan','nama_kelurahan'];
					$res = $this->M_masterusers->getListByID('m_ikelurahan',$param,$field);
					$res = isset($res[0])?$res[0]['nama_kelurahan']:'';
					$value['KELDESA'] = $res;
				}

				if (!empty($value['KECAMATAN']) && intval($value['KECAMATAN'])) {
					$param = ['id_kecamatan' => $value['KECAMATAN']];
					$field = ['id_kecamatan','nama_kecamatan'];
					$res = $this->M_masterusers->getListByID('m_ikecamatan',$param,$field);
					$res = isset($res[0])?$res[0]['nama_kecamatan']:'';
					$value['KECAMATAN'] = $res;
				}

				if (!empty($value['KECAMATAN_DOMO']) && intval($value['KECAMATAN_DOMO'])) {
					$param = ['id_kecamatan' => $value['KECAMATAN_DOMO']];
					$field = ['id_kecamatan','nama_kecamatan'];
					$res = $this->M_masterusers->getListByID('m_ikecamatan',$param,$field);
					$res = isset($res[0])?$res[0]['nama_kecamatan']:'';
					$value['KECAMATAN_DOMO'] = $res;
				}

				if (!empty($value['KABUPATEN']) && intval($value['KABUPATEN'])) {
					$param = ['id_kabkota' => $value['KABUPATEN']];
					$field = ['id_kabkota','nama_kabkota'];
					$res = $this->M_masterusers->getListByID('m_ikabkota',$param,$field);
					$res = isset($res[0])?$res[0]['nama_kabkota']:'';
					$value['KABUPATEN'] = $res;
				}

				if (!empty($value['KABUPATEN_DOMO']) && intval($value['KABUPATEN_DOMO'])) {
					$param = ['id_kabkota' => $value['KABUPATEN_DOMO']];
					$field = ['id_kabkota','nama_kabkota'];
					$res = $this->M_masterusers->getListByID('m_ikabkota',$param,$field);
					$res = isset($res[0])?$res[0]['nama_kabkota']:'';
					$value['KABUPATEN_DOMO'] = $res;
				}

				if (!empty($value['PROPINSI']) && intval($value['PROPINSI'])) {
					$param = ['id_propinsi' => $value['PROPINSI']];
					$field = ['id_propinsi','nama_propinsi'];
					$res = $this->M_masterusers->getListByID('m_ipropinsi',$param,$field);
					$res = isset($res[0])?$res[0]['nama_propinsi']:'';
					$value['PROPINSI'] = $res;
				}

				if (!empty($value['PROPINSI_DOMO']) && intval($value['PROPINSI_DOMO'])) {
					$param = ['id_propinsi' => $value['PROPINSI_DOMO']];
					$field = ['id_propinsi','nama_propinsi'];
					$res = $this->M_masterusers->getListByID('m_ipropinsi',$param,$field);
					$res = isset($res[0])?$res[0]['nama_propinsi']:'';
					$value['PROPINSI_DOMO'] = $res;
				}
				*/
				$newData['data'][$i]['nik'] = $value['nik'];
				$newData['data'][$i]['name'] = $value['name'];
				$newData['data'][$i]['GENDER'] = $value['GENDER'];
				$newData['data'][$i]['created_at'] = $value['created_at'];
				$newData['data'][$i]['status_applicant'] = $value['status_applicant'];
				$newData['data'][$i]['is_user'] = $value['is_user'];
				$newData['data'][$i]['TMTLAHIR'] = $value['TMTLAHIR'];
				$newData['data'][$i]['TGLLAHIR'] = $value['TGLLAHIR'];
				$newData['data'][$i]['ALAMAT'] = $value['ALAMAT'];
				$newData['data'][$i]['RTRW'] = $value['RTRW'];
				$newData['data'][$i]['PROPINSI'] = $value['PROPINSI'];
				$newData['data'][$i]['KABUPATEN'] = $value['KABUPATEN'];
				$newData['data'][$i]['KECAMATAN'] = $value['KECAMATAN'];
				$newData['data'][$i]['KELDESA'] = $value['KELDESA'];
				
				$newData['data'][$i]['last_name'] = $value['last_name'];
				$newData['data'][$i]['PENDIDIKAN'] = $value['PENDIDIKAN'];
				
				$newData['data'][$i]['GENDER'] = $value['GENDER'];
				$newData['data'][$i]['GOLDARAH'] = $value['GOLDARAH'];
				$newData['data'][$i]['TINGGI'] = $value['TINGGI'];
				$newData['data'][$i]['BERAT'] = $value['BERAT'];
				
				$newData['data'][$i]['STATUSRUMAH'] = $value['STATUSRUMAH'];
				$newData['data'][$i]['TELPONRUMAH'] = $value['TELPONRUMAH'];
				$newData['data'][$i]['TELPONHP'] = $value['TELPONHP'];
				$newData['data'][$i]['STATUS'] = $value['STATUS'];
				$newData['data'][$i]['JMLANAK'] = $value['JMLANAK'];
				$newData['data'][$i]['AGAMA'] = $value['AGAMA'];
				$newData['data'][$i]['KENDARAAN'] = $value['KENDARAAN'];
				
				
				
				$newData['data'][$i]['RIWAYAT'] = $value['RIWAYAT'];
				$newData['data'][$i]['AYAH_NAMA'] = $value['AYAH_NAMA'];
				$newData['data'][$i]['AYAH_ALAMAT'] = $value['AYAH_ALAMAT'];
				$newData['data'][$i]['AYAH_PEND'] = $value['AYAH_PEND'];
				$newData['data'][$i]['AYAH_PEKERJAAN'] = $value['AYAH_PEKERJAAN'];
				$newData['data'][$i]['IBU_NAMA'] = $value['IBU_NAMA'];
				$newData['data'][$i]['IBU_ALAMAT'] = $value['IBU_ALAMAT'];
				$newData['data'][$i]['IBU_PEND'] = $value['IBU_PEND'];
				$newData['data'][$i]['IBU_PEKERJAAN'] = $value['IBU_PEKERJAAN'];
				$newData['data'][$i]['ANAK1_NAMA'] = $value['ANAK1_NAMA'];
				$newData['data'][$i]['ANAK1_ALAMAT'] = $value['ANAK1_ALAMAT'];
				$newData['data'][$i]['ANAK1_PEND'] = $value['ANAK1_PEND'];
				$newData['data'][$i]['ANAK1_PEKERJAAN'] = $value['ANAK1_PEKERJAAN'];
				$newData['data'][$i]['ANAK2_NAMA'] = $value['ANAK2_NAMA'];
				$newData['data'][$i]['ANAK2_ALAMAT'] = $value['ANAK2_ALAMAT'];
				$newData['data'][$i]['ANAK2_PEND'] = $value['ANAK2_PEND'];
				$newData['data'][$i]['ANAK2_PEKERJAAN'] = $value['ANAK2_PEKERJAAN'];
				$newData['data'][$i]['ANAK3_NAMA'] = $value['ANAK3_NAMA'];
				$newData['data'][$i]['ANAK3_ALAMAT'] = $value['ANAK3_ALAMAT'];
				$newData['data'][$i]['ANAK3_PEND'] = $value['ANAK3_PEND'];
				$newData['data'][$i]['ANAK3_PEKERJAAN'] = $value['ANAK3_PEKERJAAN'];
				$newData['data'][$i]['ANAK4_NAMA'] = $value['ANAK4_NAMA'];
				$newData['data'][$i]['ANAK4_ALAMAT'] = $value['ANAK4_ALAMAT'];
				$newData['data'][$i]['ANAK4_PEND'] = $value['ANAK4_PEND'];
				$newData['data'][$i]['ANAK4_PEKERJAAN'] = $value['ANAK4_PEKERJAAN'];
				$newData['data'][$i]['PEND1'] = $value['PEND1'];
				$newData['data'][$i]['PEND1_TEMPAT'] = $value['PEND1_TEMPAT'];
				$newData['data'][$i]['PEND1_JURUSAN'] = $value['PEND1_JURUSAN'];
				$newData['data'][$i]['PEND1_TAHUN'] = $value['PEND1_TAHUN'];
				$newData['data'][$i]['PEND2'] = $value['PEND2'];
				$newData['data'][$i]['PEND2_TEMPAT'] = $value['PEND2_TEMPAT'];
				$newData['data'][$i]['PEND2_JURUSAN'] = $value['PEND2_JURUSAN'];
				$newData['data'][$i]['PEND2_TAHUN'] = $value['PEND2_TAHUN'];
				$newData['data'][$i]['PEND3'] = $value['PEND3'];
				$newData['data'][$i]['PEND3_TEMPAT'] = $value['PEND3_TEMPAT'];
				$newData['data'][$i]['PEND3_JURUSAN'] = $value['PEND3_JURUSAN'];
				$newData['data'][$i]['PEND3_TAHUN'] = $value['PEND3_TAHUN'];
				$newData['data'][$i]['PEND4'] = $value['PEND4'];
				$newData['data'][$i]['PEND4_TEMPAT'] = $value['PEND4_TEMPAT'];
				$newData['data'][$i]['PEND4_JURUSAN'] = $value['PEND4_JURUSAN'];
				$newData['data'][$i]['PEND4_TAHUN'] = $value['PEND4_TAHUN'];
				$newData['data'][$i]['PEND5'] = $value['PEND5'];
				$newData['data'][$i]['PEND5_TEMPAT'] = $value['PEND5_TEMPAT'];
				$newData['data'][$i]['PEND5_JURUSAN'] = $value['PEND5_JURUSAN'];
				$newData['data'][$i]['PEND5_TAHUN'] = $value['PEND5_TAHUN'];
				$newData['data'][$i]['PENDNON1'] = $value['PENDNON1'];
				$newData['data'][$i]['PENDNON1_TEMPAT'] = $value['PENDNON1_TEMPAT'];
				$newData['data'][$i]['PENDNON1_MATERI'] = $value['PENDNON1_MATERI'];
				$newData['data'][$i]['PENDNON1_TAHUN'] = $value['PENDNON1_TAHUN'];
				$newData['data'][$i]['PENDNON2'] = $value['PENDNON2'];
				$newData['data'][$i]['PENDNON2_TEMPAT'] = $value['PENDNON2_TEMPAT'];
				$newData['data'][$i]['PENDNON2_MATERI'] = $value['PENDNON2_MATERI'];
				$newData['data'][$i]['PENDNON2_TAHUN'] = $value['PENDNON2_TAHUN'];
				$newData['data'][$i]['PENDNON3'] = $value['PENDNON3'];
				$newData['data'][$i]['PENDNON3_TEMPAT'] = $value['PENDNON3_TEMPAT'];
				$newData['data'][$i]['PENDNON3_MATERI'] = $value['PENDNON3_MATERI'];
				$newData['data'][$i]['PENDNON3_TAHUN'] = $value['PENDNON3_TAHUN'];
				$newData['data'][$i]['PENDNON4'] = $value['PENDNON4'];
				$newData['data'][$i]['PENDNON4_TEMPAT'] = $value['PENDNON4_TEMPAT'];
				$newData['data'][$i]['PENDNON4_MATERI'] = $value['PENDNON4_MATERI'];
				$newData['data'][$i]['PENDNON4_TAHUN'] = $value['PENDNON4_TAHUN'];
				$newData['data'][$i]['KERJA1_NAMA'] = $value['KERJA1_NAMA'];
				$newData['data'][$i]['KERJA1_TAHUN'] = $value['KERJA1_TAHUN'];
				$newData['data'][$i]['KERJA1_JABATAN'] = $value['KERJA1_JABATAN'];
				$newData['data'][$i]['KERJA1_GAJI'] = $value['KERJA1_GAJI'];
				$newData['data'][$i]['KERJA1_ALASAN'] = $value['KERJA1_ALASAN'];
				$newData['data'][$i]['KERJA2_NAMA'] = $value['KERJA2_NAMA'];
				$newData['data'][$i]['KERJA2_TAHUN'] = $value['KERJA2_TAHUN'];
				$newData['data'][$i]['KERJA2_JABATAN'] = $value['KERJA2_JABATAN'];
				$newData['data'][$i]['KERJA2_GAJI'] = $value['KERJA2_GAJI'];
				$newData['data'][$i]['KERJA2_ALASAN'] = $value['KERJA2_ALASAN'];
				$newData['data'][$i]['KERJA3_NAMA'] = $value['KERJA3_NAMA'];
				$newData['data'][$i]['KERJA3_TAHUN'] = $value['KERJA3_TAHUN'];
				$newData['data'][$i]['KERJA3_JABATAN'] = $value['KERJA3_JABATAN'];
				$newData['data'][$i]['KERJA3_GAJI'] = $value['KERJA3_GAJI'];
				$newData['data'][$i]['KERJA3_ALASAN'] = $value['KERJA3_ALASAN'];
				$newData['data'][$i]['JOB_DESC'] = $value['JOB_DESC'];
				$newData['data'][$i]['HARAPAN_GAJI'] = $value['HARAPAN_GAJI'];
				$newData['data'][$i]['DARURAT_NAMA'] = $value['DARURAT_NAMA'];
				$newData['data'][$i]['DARURAT_ALAMAT'] = $value['DARURAT_ALAMAT'];
				$newData['data'][$i]['DARURAT_TELP'] = $value['DARURAT_TELP'];
				$newData['data'][$i]['BERSEDIA_DITEMPAT'] = $value['BERSEDIA_DITEMPAT'];
				$newData['data'][$i]['TGL_SIAP_KERJA'] = $value['TGL_SIAP_KERJA'];
				$newData['data'][$i]['PERSETUJUAN'] = $value['created_at'];
				$newData['data'][$i]['POSITION'] = $value['POSITION'];
				$newData['data'][$i]['PER_LVL_COMP'] = $value['PER_LVL_COMP'];
				$newData['data'][$i]['PER_LVL_ENG'] = $value['PER_LVL_ENG'];
				$newData['data'][$i]['PER_LVL_MAN'] = $value['PER_LVL_MAN'];
				$newData['data'][$i]['PER_LVL_PHL'] = $value['PER_LVL_PHL'];
				$newData['data'][$i]['PER_LVL_EKG'] = $value['PER_LVL_EKG'];
				$newData['data'][$i]['PER_LVL_TEN'] = $value['PER_LVL_TEN'];
				$newData['data'][$i]['PER_LVL_SPI'] = $value['PER_LVL_SPI'];
				$newData['data'][$i]['PER_LVL_AUD'] = $value['PER_LVL_AUD'];
				$newData['data'][$i]['PER_LVL_PAP'] = $value['PER_LVL_PAP'];
				$newData['data'][$i]['PER_ADA_STR'] = $value['PER_ADA_STR'];
				$newData['data'][$i]['PER_ADA_BCLS'] = $value['PER_ADA_BCLS'];
				$newData['data'][$i]['ANA_LVL_COMP'] = $value['ANA_LVL_COMP'];
				$newData['data'][$i]['ANA_LVL_ENG'] = $value['ANA_LVL_ENG'];
				$newData['data'][$i]['ANA_LVL_MAN'] = $value['ANA_LVL_MAN'];
				$newData['data'][$i]['ANA_LVL_DAR'] = $value['ANA_LVL_DAR'];
				$newData['data'][$i]['ANA_LVL_MANUAL'] = $value['ANA_LVL_MANUAL'];
				$newData['data'][$i]['ANA_LVL_PEM'] = $value['ANA_LVL_PEM'];
				$newData['data'][$i]['ANA_LVL_KOR'] = $value['ANA_LVL_KOR'];
				$newData['data'][$i]['ANA_LVL_SEN'] = $value['ANA_LVL_SEN'];
				$newData['data'][$i]['ANA_LVL_MIC'] = $value['ANA_LVL_MIC'];
				$newData['data'][$i]['ANA_LVL_TIM'] = $value['ANA_LVL_TIM'];
				$newData['data'][$i]['ANA_ADA_PHR'] = $value['ANA_ADA_PHR'];
				$newData['data'][$i]['ANA_ADA_STR'] = $value['ANA_ADA_STR'];
				$newData['data'][$i]['sosmed'] = $value['sosmed'];
				$newData['data'][$i]['qbca'] = $value['qbca'];
				$newData['data'][$i]['qskck'] = $value['qskck'];
				
				/* add masa kerja */
				/*$now = date('Y-m-d');
				$tgl_masuk = $value['TGLMASUK'];
				
				if ($tgl_masuk == '0000-00-00' || $tgl_masuk == '0000-00-00 00:00:00' || $tgl_masuk == null || empty($tgl_masuk)) {
					$caption = 0;
				}else{
					$resTgl = $this->misc->selisih_waktu($now,$tgl_masuk);
					$caption = $resTgl['tahun'].' tahun, '. $resTgl['bulan'].' bulan, '.$resTgl['hari'].' hari';
				}
				$newData['data'][$i]['masa_kerja'] = $caption;*/
				/**/
				
				$i++;
			}

			$newData['draw'] = isset($_REQUEST['draw'])?$_REQUEST['draw']:1;
			$newData['recordsTotal'] = $totalData;
			$newData['recordsFiltered'] = $totalData;
			
			if ($direct) {
				return json_encode($newData);
			}else{
				echo json_encode($newData);	
			}
		}
		else
			echo json_encode(array('data'=>''));
	}

	function cek_status($id_applicant=0){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$data = $this->m_general->load_data(array('id_applicant'=>$id_applicant),0,10,1,'applicant','id_applicant');
		

		$newData = [];
		$i = 0;
		foreach ($data as $key => $value) {
			$newData['data'][$i]['no'] = $i+1;
			$newData['data'][$i]['id_applicant'] = $value['id_applicant'];
			$newData['data'][$i]['name'] = $value['name'];
			$newData['data'][$i]['created_at'] = $value['created_at'];
			$newData['data'][$i]['status_applicant'] = $value['status_applicant'];
			
			$newData['data'][$i]['date_applied'] = $value['date_applied'];
			$newData['data'][$i]['date_tes_kepribadian'] = $value['date_tes_kepribadian'];
			$newData['data'][$i]['date_wawancara_hrd'] = $value['date_wawancara_hrd'];
			$newData['data'][$i]['date_wawancara_user'] = $value['date_wawancara_user'];
			$newData['data'][$i]['date_psikotes'] = $value['date_psikotes'];
			$newData['data'][$i]['date_tes_kesehatan'] = $value['date_tes_kesehatan'];
			$newData['data'][$i]['date_kontrak'] = $value['date_kontrak'];
			$newData['data'][$i]['reason_applied'] = $value['reason_applied'];
			$newData['data'][$i]['reason_tes_kepribadian'] = $value['reason_tes_kepribadian'];
			$newData['data'][$i]['reason_wawancara_hrd'] = $value['reason_wawancara_hrd'];
			$newData['data'][$i]['reason_wawancara_user'] = $value['reason_wawancara_user'];
			$newData['data'][$i]['reason_psikotes'] = $value['reason_psikotes'];
			$newData['data'][$i]['reason_tes_kesehatan'] = $value['reason_tes_kesehatan'];
			$newData['data'][$i]['reason_kontrak'] = $value['reason_kontrak'];
			$newData['data'][$i]['file_applied'] = $value['file_applied'];
			$newData['data'][$i]['file_tes_kepribadian'] = $value['file_tes_kepribadian'];
			$newData['data'][$i]['file_tes_kepribadian2'] = $value['file_tes_kepribadian2'];
			$newData['data'][$i]['file_wawancara_hrd'] = $value['file_wawancara_hrd'];
			$newData['data'][$i]['file_wawancara_user'] = $value['file_wawancara_user'];
			$newData['data'][$i]['file_psikotes'] = $value['file_psikotes'];
			$newData['data'][$i]['file_tes_kesehatan'] = $value['file_tes_kesehatan'];
			$newData['data'][$i]['file_kontrak'] = $value['file_kontrak'];
			
			$i++;
		}

		//if ($direct) {
			//return json_encode($newData);
		//}else{
			echo json_encode($newData);	
		//}
	}

	function insert(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$edit=0;
		if (!empty($this->input->post('id_user'))) {
			$edit = 1;
		}

		$data = $this->input->post();
		$salt = 'Bio Medika';
		$password = '123456';
		$def_passwd = sha1($salt . $password);
		$username = explode(' ', strtolower($this->input->post('name')));
		$username = $username[0];
		if (!$edit) $data['username'] = $username;
		$data['password'] = $def_passwd;
		// $data['branch_id'] = $this->branch_id;
		$data['creator_id'] = $this->user_id;

		/*prevent unset data field (select)*/
		!isset($data['id_bagian'])?$data['id_bagian']='':'';
		!isset($data['GENDER'])?$data['GENDER']='':'';
		!isset($data['PROPINSI'])?$data['PROPINSI']='':'';
		!isset($data['KABUPATEN'])?$data['KABUPATEN']='':'';
		!isset($data['KECAMATAN'])?$data['KECAMATAN']='':'';
		!isset($data['KELDESA'])?$data['KELDESA']='':'';
		!isset($data['PROPINSI_DOMO'])?$data['PROPINSI_DOMO']='':'';
		!isset($data['KABUPATEN_DOMO'])?$data['KABUPATEN_DOMO']='':'';
		!isset($data['KECAMATAN_DOMO'])?$data['KECAMATAN_DOMO']='':'';
		!isset($data['KELDESA_DOMO'])?$data['KELDESA_DOMO']='':'';
		!isset($data['BANK'])?$data['BANK']='':'';
		!isset($data['STATUSKAWIN'])?$data['STATUSKAWIN']='':'';
		!isset($data['BUMIDA'])?$data['BUMIDA']='':'';
		!isset($data['branch_id'])?$data['branch_id']='':'';
		!isset($data['id_position'])?$data['id_position']='':'';
		!isset($data['id_department'])?$data['id_department']='':'';
		!isset($data['TIPEBAYAR'])?$data['TIPEBAYAR']='':'';
		!isset($data['STATUS'])?$data['STATUS']='':'';
		!isset($data['AVPENSION'])?$data['AVPENSION']='':'';
		!isset($data['AVKELAS'])?$data['AVKELAS']='':'';

		!isset($data['cabang_pajak'])?$data['cabang_pajak']='':'';
		!isset($data['tanggal_pemindahan'])?$data['tanggal_pemindahan']='':'';
		/**/

		

		/*uploading image*/
		$name_upload = $username;
		$config['upload_path']          = 'uploads/avatar/';
		$config['allowed_types']        = 'jpg|jpeg|png';
		$config['file_name'] = $name_upload;

		$size_image = $_FILES['foto']['size'];
		if ($size_image > 0) {
			$this->upload->initialize($config);
			@chmod(FCPATH.'uploads/avatar/',0777);
			$uploaded = $this->upload->do_upload('foto');
			if (!$uploaded) {
				$response['code'] = 304;
				$response['msg'] = $this->upload->error_msg[0];
				// break;
			}else{
				$data['image_uploaded'] = $this->upload->data();
				$response['code'] = 200;
				$response['upload_msg'] = 'Avatar berhasil diupload!';
				$data['image'] = $name_upload.'.'.$this->upload->data('image_type');
				unset($data['image_uploaded']);
				unset($data['initial']);
			}

			@chmod(FCPATH.'uploads/avatar/',0644);
			/**/
		}else{
			unset($data['image']);
		}

		if (isset($_FILES['lampiran_str']['name']) && ($_FILES['lampiran_str']['name'] !== '')) {
			//for path need permission - chmod -R 777
			$config['upload_path'] = "uploads/attachments/karyawan/";
			$config['allowed_types'] = '*';
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			$up = $this->upload->do_upload('lampiran_str');
			if ($up) {
				$upload = $this->upload->data();
				$data['lampiran_str'] = $config['upload_path'] . $upload['file_name'];
			}
		}

		if (isset($_FILES['lampiran_mcu']['name']) && ($_FILES['lampiran_mcu']['name'] !== '')) {
			//for path need permission - chmod -R 777
			$config['upload_path'] = "uploads/attachments/karyawan/";
			$config['allowed_types'] = '*';
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			$up = $this->upload->do_upload('lampiran_mcu');
			if ($up) {
				$upload = $this->upload->data();
				$data['lampiran_mcu'] = $config['upload_path'] . $upload['file_name'];
			}
		}

		if (isset($_FILES['lampiran_skk']['name']) && ($_FILES['lampiran_skk']['name'] !== '')) {
			//for path need permission - chmod -R 777
			$config['upload_path'] = "uploads/attachments/karyawan/";
			$config['allowed_types'] = '*';
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			$up = $this->upload->do_upload('lampiran_skk');
			if ($up) {
				$upload = $this->upload->data();
				$data['lampiran_skk'] = $config['upload_path'] . $upload['file_name'];
			}
		}

		$data['FINGERID'] = intval($data['FINGERID']);

		$vf = [];
		$valid_form = [];
		$res_valid_form = [];
		
		foreach ($data as $key => $value) {
			$resp = $this->field_validation($key,$value);
			$vf[] = $resp['status'];
			$valid_form[$key] = $value;
			$res_valid_form[$key] = $resp['msg'];
		}

		if (empty($vf) || in_array(0,$vf)) {
			$result['status'] = false;
				$result['message'] = 'Data belum lengkap. silakan di ulangi!';
				$result['form_res'] = $res_valid_form;
		}else{
			$arr = array("TGLLAHIR", "TGLMASUK", "TGLTETAP", "AWALKONTRAK", "AKHIRKONTRAK", "TGLKELUAR");
			foreach ($data as $key => $value) {
				if (in_array($key, $arr) && $value!='')
					$data[$key] = date("Y-m-d", strtotime(str_replace('/', '-', $value)) );
			}
			
			if ($edit) {
				/*edit*/
				unset($data['password']);
				unset($data['last_status']);
				// unset($data['NIP']);
				// unset($data['FINGERID']);	
				$id_user = $data['id_user'];
				
				$this->M_masterusers->update($data,$id_user);
			}else{
				/*save*/
				$this->M_masterusers->insert($data);
			}
			
			
			$result['status'] = true;
			$result['message'] = 'Data berhasil disimpan.';
				
		}
		echo json_encode($result);
	}

	function update($foto=0,$injctID=0){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$field = $this->input->post('field');
		$valData = $this->input->post('value');
		$id_user = $this->input->post('id_user');
		$cekValid = $this->field_validation($field,$valData);

		if ($foto) {
				//proses upload gambar
				$old_pict = $this->input->post('old_pict');
				$config['upload_path'] = './uploads/avatar/';
				$config['allowed_types'] = '*';
				$config['max_size'] = '*';
				$config['file_name'] = 'image'.time();
				$this->upload->initialize($config);

				$this->upload->do_upload('image');
				@unlink('./assets/upload/',$old_pict);
				$image = $this->upload->data();
		}

		//kondisi reset request cuti
		if ($data['last_status'] != $data['STATUS']){
			$param = [];
			$param['deleted'] = 0;
			$param['in_param'] = [
				'cuti.id_user' => $this->input->post('id_user')
			];

			$data_cuti = $this->m_cuti->get_data_cuti($param,0,0,1);
	
			if (!empty($data_cuti)) {
				foreach ($data_cuti as $key => $value) {
	
					$update_data = [
						'request_cuti' => 0,
						'last_request' => $value['request_cuti']
					];
	
				$this->m_cuti->update_cuti($update_data,['id_cuti'=>$value['id_cuti']]);

				}
			}


			//$data['CUTI'] = 0;
			//$data['cuti_keseluruhan'] = 0;

		}
		
		if ($cekValid['status']) {
			$data = [
				$field => $valData
			];

			if ($foto) {
				unset($data);
				$id_user = $injctID;
				$data['image'] = $image['file_name'];
			}
			
			$this->M_masterusers->update($data,$id_user);

			$result['status'] = true;
			$result['message'] = 'Data berhasil diupdate.';
		}else{
			$result['status'] = false;
			$result['message'] = 'Data gagal diupdate.';
			$res_valid_form[$field.'-'.$id_user] = $cekValid['msg'];
			$result['form_res'] = $res_valid_form;
		}
		
		echo json_encode($result);
	}

	function update_str($foto=0,$injctID=0){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$field = $this->input->post('field');
		$valData = $this->input->post('value');
		$id_user = $this->input->post('id_user');
		$cekValid = $this->field_validation($field,$valData);

		if ($str) {
				//proses upload gambar
				$old_pict = $this->input->post('old_pict');
				$config['upload_path'] = './uploads/str/';
				$config['allowed_types'] = '*';
				$config['max_size'] = '*';
				$config['file_name'] = 'image'.time();
				$this->upload->initialize($config);

				$this->upload->do_upload('image');
				@unlink('./assets/upload/',$old_pict);
				$image = $this->upload->data();
		}
		
		if ($cekValid['status']) {
			$data = [
				$field => $valData
			];

			if ($foto) {
				unset($data);
				$id_user = $injctID;
				$data['image'] = $image['file_name'];
			}
			
			$this->M_masterusers->update($data,$id_user);

			$result['status'] = true;
			$result['message'] = 'Data berhasil diupdate.';
		}else{
			$result['status'] = false;
			$result['message'] = 'Data gagal diupdate.';
			$res_valid_form[$field.'-'.$id_user] = $cekValid['msg'];
			$result['form_res'] = $res_valid_form;
		}
		echo json_encode($result);
	}

	function delete(){
		$result['status'] = false;
		$result['message'] = 'Data gagal dihapus.';

		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		
		$del = $this->M_masterusers->delete($this->input->post('id_user'));

		if ($del) {
			$result['status'] = true;
			$result['message'] = 'Data berhasil dihapus.';
		}
		
		echo json_encode($result);
	}

	function field_validation($field,$value){
        switch ($field) {
			/*case 'name':
			case 'last_name':
			case 'username':
			case 'password':
			case 'call_name':
			case 'inisial':
			case 'GENDER':
			case 'ALAMAT':
			case 'RTRW':
			case 'KELDESA':
			case 'ALAMAT_DOMO':
			case 'RTRW_DOMO':
			case 'TMTLAHIR':
			case 'TIPEBAYAR':
			case 'BANK':
			case 'STATUSKAWIN':
			case 'STATUS':
			case 'BUMIDA':
			case 'AVPENSION':
			case 'PENDIDIKAN':
			case 'GOLDARAH':
                # text & notEmpty
                $ret = [
                        'status' => 1,
                        'msg' => 'ok'
                    ];
                if (empty($value) || !is_string($value)) {
                    $ret = [
                        'status' => 0,
                        'msg' => 'inputan bukan string atau kosong!'
                    ];
                }
                return $ret;
                break;

            case 'TGLMASUK':
            case 'TGLJAMSOSTEK':
            case 'TGLLAHIR':
                # date & notEmpty
                $ret = [
                        'status' => 1,
                        'msg' => 'ok'
                    ];
                if (empty($value) || !$this->misc->checkIsAValidDate($value)) {
                    $ret = [
                        'status' => 0,
                        'msg' => 'inputan tgl tidak valid atau kosong!'
                    ];
                }
                return $ret;
                break;
            
          	case 'KODEPOS':
			case 'KODEPOS_DOMO':
			case 'FINGERID':
			case 'NIP':
			case 'NONPWP':
			case 'NOKTP':
			case 'NOREK':
			case 'TELPONHP':
			case 'TELPONRUMAH':
			case 'JMLANAK':
			// case 'NOJAMSOSTEK':
			case 'branch_id':
			case 'id_position':
			case 'id_bagian':
			case 'id_department':
			case 'CUTI':
			// case 'AVKELAS':
			case 'KECAMATAN':
			case 'KABUPATEN':
			case 'PROPINSI':
			case 'KELDESA_DOMO':
			case 'KECAMATAN_DOMO':
			case 'KABUPATEN_DOMO':
			case 'PROPINSI_DOMO':
			case 'TINGGI':
			case 'BERAT':
                # number & notEmpty
                $ret = [
                        'status' => 1,
                        'msg' => 'ok'
                    ];
                if (empty($value) || !is_numeric($value)) {
                    $ret = [
                        'status' => 0,
                        'msg' => 'inputan bukan angka atau kosong!'
                    ];
                }
                return $ret;
                break;
            
            case 'email':
            	# string email
            	$ret = [
                        'status' => 1,
                        'msg' => 'ok'
                    ];
            	$patternCheck = filter_var($value, FILTER_VALIDATE_EMAIL);
            	if (!$patternCheck) {
                    $ret = [
                        'status' => 0,
                        'msg' => 'email tidak valid atau kosong!'
                    ];
                }
                return $ret;
				break;*/
            default:
                $ret = [
                    'status' => 1,
                    'msg' => 'ok'
                ];
                return $ret;
                break;
        }
    }

    public function exportExcel(){
    	$data = $this->load_data(0,1);
    	$extract = json_decode($data,true);
    	unset($data);
    	$data['rowData'] = $extract;
    	$this->load->view('admin/hr/v_hr_excel', $data);
    }

	public function exportExcelCI(){
   // 	$data['karyawan'] = $this->db->query("SELECT NIP,name,last_name,cuti,COUNT(i.id_izin) as total_izin,TGLMASUK,STATUS,
	//	STR_TO_DATE(TGLMASUK, '%Y-%m-%d') as TGLMASUK,
    //	STR_TO_DATE(NOW(), '%Y-%m-%d') AS tanggal_sekarang,
    //	TIMESTAMPDIFF(YEAR, TGLMASUK, NOW()) AS selisih_tahun,
    //	TIMESTAMPDIFF(MONTH, TGLMASUK, NOW()) AS selisih_bulan
	//	 FROM users u left join izin i on i.id_user = u.id_user group by u.id_user")->result();
    //	$extract = json_decode($data,true);
    //	unset($data);
    //	$data['rowData'] = $extract;
	//	print_r($data);
	//	die();

	  	$tgl_masuk = $this->db->query("SELECT NIP,name,last_name,cuti,COUNT(i.id_izin) as total_izin,TGLMASUK,STATUS
		   FROM users u left join izin i on i.id_user = u.id_user group by u.id_user")->result();
		$i=0;
	
		foreach ($tgl_masuk as $tk){
		$now = date('Y-m-d');
		$data['data'][$i]['NIP'] = $tk->NIP;
		$data['data'][$i]['name'] = $tk->name;
		$data['data'][$i]['last_name'] = $tk->last_name;
		$data['data'][$i]['cuti'] = $tk->cuti;
		$data['data'][$i]['total_izin'] = $tk->total_izin;
		$data['data'][$i]['TGLMASUK'] = $tk->TGLMASUK;
		$data['data'][$i]['STATUS'] = $tk->STATUS;
		$data['data'][$i]['tgl_masuk'] = $this->misc->selisih_waktu($now,$tk->TGLMASUK);
		$i++;
		
		}
		$json = json_decode(json_encode($data));
	//	print_r($json);
	//	die();
    	$this->load->view('admin/hr/v_hr_excel_CI', $json);
    }

    public function exportExcelApp(){
    	$data = $this->load_data_pelamar(0,1);
    	$extract = json_decode($data,true);
    	unset($data);
    	$data['rowData'] = $extract;
    	$this->load->view('admin/hr/v_applicant_excel', $data);
    }

    public function excel_absen(){
ini_set('memory_limit', '-1');
    	ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); 
	   
		$conds = array();
		$conds2 = array();
		$startDate = $this->input->post('startDate');
		if($startDate == null){
			$startDate = date("Y-m-d");
			$conds['DateLog >='] = $startDate;
			$conds2['DateLog >='] = $startDate;
		}else{
			$conds['DateLog >='] = $startDate;
			$conds2['DateLog >='] = $startDate;
		}
		$endDate = $this->input->post('endDate');
		if($endDate == null){
			$endDate = date("Y-m-d");
			$conds['DateLog <='] = $endDate;
			$conds2['DateLog <='] = $endDate;
		}else{
			$conds['DateLog <='] = $endDate;
			$conds2['DateLog <='] = $endDate;
		}
		/*$cabang = $this->input->post('search_cab');
		if ($cabang != null) {
			$conds['Cabang'] = $cabang;		
			$conds2['branch_name'] = $cabang;		
		}
		$nama = $this->input->post('search_kar');
		if (!empty($nama)) {
			if ($nama != 'Semua'){
				$conds['name'] = $nama;		
				$conds2['name'] = $nama;
			}	
		}*/
		
		$cabang = $this->input->post('search_cab');
		if ($cabang != null) {
			$conds['Cabang'] = $cabang;		
			//$conds2['branch_name'] = $cabang;
			$conds2['(select branch_id from users u where e.UserID = u.FINGERID limit 1) = '.$cabang] = null;
		}
		$nama = $this->input->post('search_kar');
		if (!empty($nama)) {
			if ($nama != 'Semua'){
				$conds['name'] = $nama;		
				//$conds2['name'] = $nama;
				
				$karyawan =  $this->db->query("select FINGERID
					from users a
					where id_user='$nama'
				")->first_row();
				
				$conds2['e.UserID in ('.$karyawan->FINGERID.')'] = null;
			}	
		}
		
		$user_name = $this->get_name_finger();
		$last_name = $this->get_lastname_finger();
		$position_name = $this->get_posisi_finger();

		$data_absen = $this->M_masterusers->get_all_finger_absen($conds2)->result();
		if (!empty($data_absen)) {
			$i = 0;
			foreach ($data_absen as $key => $value) {
				$dataAbsen['data'][$i]['no'] = $i+1;
				$dataAbsen['data'][$i]['finger_id'] = $value->FingerId;
				$dataAbsen['data'][$i]['nama'] = isset($user_name[$value->FingerId])?$user_name[$value->FingerId]:'';
				$dataAbsen['data'][$i]['nama_belakang'] = isset($last_name[$value->FingerId])?$last_name[$value->FingerId]:'';
				$dataAbsen['data'][$i]['position_name'] = isset($position_name[$value->FingerId])?$position_name[$value->FingerId]:'';
				$date = date('d-M-y', strtotime($value->DateLog) );
				$dataAbsen['data'][$i]['tanggal'] = $date;
				$dataAbsen['data'][$i]['jam_datang'] = $value->jam_datang;
				$dataAbsen['data'][$i]['jam_pulang'] = $value->jam_pulang;
				
				$dataAbsen['data'][$i]['jam_istirahat'] = $value->jam_istirahat;
				$dataAbsen['data'][$i]['jam_masuk_istirahat'] = $value->jam_masuk_istirahat;
				
				if($value->jam_istirahat == $value->jam_masuk_istirahat)
					$dataAbsen['data'][$i]['jam_masuk_istirahat'] = 'tidak ada data';
				
				$dataAbsen['data'][$i]['cabang'] = $value->Cabang;
				$dataAbsen['data'][$i]['cabang_finger'] = $value->cabang_finger;
				$i++;
			}
			$json_absen = json_decode(json_encode($dataAbsen));
			$data['absen'] = $json_absen->data;
			$jumlahHari = [];
			foreach ($json_absen->data as $hari) {
				if (isset($hari->finger_id)) {
					$jumlah = $hari->finger_id;
					if (!isset($jumlahHari[$jumlah])) {
						$jumlahHari[$jumlah] = 0;
					}
					$jumlahHari[$jumlah]++;
				}
			}

			$data_finger= $this->M_masterusers->dateRangeFinger($conds2)->result();
			if (!empty($data_finger)) {
				$i = 0;
				foreach ($data_finger as $key => $value) {
					$dataFinger['data'][$i]['no'] = $i+1;
					$dataFinger['data'][$i]['nip'] = $value->nip;
					$dataFinger['data'][$i]['finger_id'] = $value->finger_id;
					$dataFinger['data'][$i]['keterangan_hrd'] = "";
					$dataFinger['data'][$i]['keterangan_cabang'] = "";
					$dataFinger['data'][$i]['nama'] = isset($value->name)?$value->name:'';
					$dataFinger['data'][$i]['nama_belakang'] = isset($value->last_name)?$value->last_name:'';
					$dataFinger['data'][$i]['jumlah_hari'] = isset($jumlahHari[$value->finger_id])?$jumlahHari[$value->finger_id]:0;
					$dataFinger['data'][$i]['cabang_finger'] = $value->cabang_finger;
					$dataFinger['data'][$i]['user_cabang'] = $value->user_cabang;
					$i++;
				}
			}
			$json_finger = json_decode(json_encode($dataFinger));
			$data['finger'] = $json_finger->data;

		}else{
			$data['absen'] = "";
			$data['finger'] = "";
		}		

		$data_absen_sun = $this->M_masterusers->get_all_finger_absen_sun($conds2)->result();
		if (!empty($data_absen_sun)) {
			$i = 0;
			foreach ($data_absen_sun as $key => $value) {
				$dataAbsenSun['data'][$i]['no'] = $i+1;
				$dataAbsenSun['data'][$i]['finger_id'] = $value->FingerId;
				$dataAbsenSun['data'][$i]['nama'] = isset($user_name[$value->FingerId])?$user_name[$value->FingerId]:'';
				$dataAbsenSun['data'][$i]['nama_belakang'] = isset($last_name[$value->FingerId])?$last_name[$value->FingerId]:'';
				$dataAbsenSun['data'][$i]['position_name'] = isset($position_name[$value->FingerId])?$position_name[$value->FingerId]:'';
				$date = date('d-M-y', strtotime($value->DateLog) );
				$dataAbsenSun['data'][$i]['tanggal'] = $date;
				$dataAbsenSun['data'][$i]['jam_datang'] = $value->jam_datang;
				$dataAbsenSun['data'][$i]['jam_pulang'] = $value->jam_pulang;
				
				$dataAbsenSun['data'][$i]['jam_istirahat'] = $value->jam_istirahat;
				$dataAbsenSun['data'][$i]['jam_masuk_istirahat'] = $value->jam_masuk_istirahat;
				$dataAbsenSun['data'][$i]['cabang'] = $value->Cabang;
				$dataAbsenSun['data'][$i]['cabang_finger'] = $value->cabang_finger;
				
				if($value->jam_istirahat == $value->jam_masuk_istirahat)
					$dataAbsenSun['data'][$i]['jam_masuk_istirahat'] = 'tidak ada data';
				
				
				$i++;
			}
			$json_absen_sun = json_decode(json_encode($dataAbsenSun));
			$data['absen_sun'] = $json_absen_sun->data;
			$jumlahHariMinggu = [];
			foreach ($json_absen_sun->data as $hariMinggu) {
				if (isset($hariMinggu->finger_id)) {
					$jumlahMinggu = $hariMinggu->finger_id;
					if (!isset($jumlahHariMinggu[$jumlahMinggu])) {
						$jumlahHariMinggu[$jumlahMinggu] = 0;
					}
					$jumlahHariMinggu[$jumlahMinggu]++;
				}
			}
			
			$data_finger_sun= $this->M_masterusers->dateRangeFingerSunday($conds2)->result();
			if (!empty($data_finger_sun)) {
				$i = 0;
				foreach ($data_finger_sun as $key => $value) {
					$dataFingerSun['data'][$i]['no'] = $i+1;
					$dataFingerSun['data'][$i]['nip'] = $value->nip;
					$dataFingerSun['data'][$i]['finger_id'] = $value->finger_id;
					$dataFingerSun['data'][$i]['keterangan_hrd'] = "";
					$dataFingerSun['data'][$i]['keterangan_cabang'] = "";
					$dataFingerSun['data'][$i]['nama'] = isset($value->name)?$value->name:'';
					$dataFingerSun['data'][$i]['nama_belakang'] = isset($value->last_name)?$value->last_name:'';
					$dataFingerSun['data'][$i]['jumlah_hari'] = isset($jumlahHariMinggu[$value->finger_id])?$jumlahHariMinggu[$value->finger_id]:0;
					$dataFingerSun['data'][$i]['cabang_finger'] = $value->cabang_finger;
					$dataFingerSun['data'][$i]['user_cabang'] = $value->user_cabang;
					$i++;
				}
			}
			$json_finger_sun = json_decode(json_encode($dataFingerSun));
			$data['finger_sun'] = $json_finger_sun->data;
		}else{
			$data['absen_sun'] = "";
			$data['finger_sun'] = "";
		}


//		print_r($data);
//		die();

		// throw new \Exception(print_r($data,true));
		// print_r($data['finger']);
		//die();
		$this->load->view('admin/hr/excel_absen', $data);
	}

	public function dateRange(){
		$date = $this->misc->dateRange('2019-11-1','2019-11-10');
		
	}

	public function updateCutiTetap(){
		$param = [];
		$param['in_param'] = [
			'STATUS' => [
				'TETAP',
			]
		];
		$data = $this->M_masterusers->load_data($param,0,0,1);
		foreach ($data as $key => $value) {
			/* count masa kerja */
			$now = date('Y-m-d');
			$tgl_masuk = $value['TGLMASUK'];
			$user_id = $value['id_user'];
			$masa_tahun = 0;
			$masa_bulan = 0;
			
			if ($tgl_masuk == '0000-00-00' || $tgl_masuk == '0000-00-00 00:00:00' || $tgl_masuk == null || empty($tgl_masuk)) {
				$masa_tahun = 0;
			}else{
				$resTgl = $this->misc->selisih_waktu($now,$tgl_masuk);
				$masa_tahun = $resTgl['tahun'];
				if ($masa_tahun < 1) {
					$masa_bulan = $resTgl['bulan'];
				}
			}

			/*6 bulan = 6 hari; 1 tahun = 12 hari; >1 tahun = 14 hari*/
			$jatah_cuti = 0;
			/*if ($masa_tahun < 1) {
				if ($masa_bulan >= 6) {
					$jatah_cuti = 6;
				}
			}elseif ($masa_tahun >= 1 && $value['STATUS'] == 'KONTRAK') {
				$jatah_cuti = 12;
			}elseif ($masa_tahun > 2) {
				$jatah_cuti = 14;
			}*/
			
			if ($value['STATUS'] == 'TETAP') {
				$jatah_cuti = 14;
			}

			$data_parse = [
				'CUTI' => $jatah_cuti,
				'last_reset_cuti' => date('Y-m-d')			
			];
			
			$this->M_masterusers->update($data_parse,$user_id);
		}
	}
	
	public function updateCutiKontrak(){
		$param = [];
		$param['in_param'] = [
			'STATUS' => [
				'KONTRAK'
			]
		];
		//$param['((year(last_reset_cuti)<>'.date('Y').') or last_reset_cuti is null)'] = null;
		$param['(TIMESTAMPDIFF(MONTH, TGLMASUK, "'.date('Y-m-d').'")=6 or TIMESTAMPDIFF(MONTH, TGLMASUK, "'.date('Y-m-d').'")=12) and (TIMESTAMPDIFF(MONTH, last_reset_cuti, "'.date('Y-m-d').'")>=1 or last_reset_cuti is null)'] = null;
		$data = $this->M_masterusers->load_data($param,0,0,1);
		foreach ($data as $key => $value) {
			/* count masa kerja */
			$now = date('Y-m-d');
			$tgl_masuk = $value['TGLMASUK'];
			$user_id = $value['id_user'];
			$masa_tahun = 0;
			$masa_bulan = 0;
			
			if ($tgl_masuk == '0000-00-00' || $tgl_masuk == '0000-00-00 00:00:00' || $tgl_masuk == null || empty($tgl_masuk)) {
				$masa_tahun = 0;
			}else{
				$resTgl = $this->misc->selisih_waktu($now,$tgl_masuk);
				$masa_tahun = $resTgl['tahun'];
				if ($masa_tahun < 1) {
					$masa_bulan = $resTgl['bulan'];
				}
			}

			/*6 bulan = 6 hari; 1 tahun = 12 hari; >1 tahun = 14 hari*/
			$jatah_cuti = 0;
			/*if ($masa_tahun < 1) {
				if ($masa_bulan >= 6) {
					$jatah_cuti = 6;
				}
			}elseif ($masa_tahun >= 1 && $value['STATUS'] == 'KONTRAK') {
				$jatah_cuti = 12;
			}elseif ($masa_tahun > 2) {
				$jatah_cuti = 14;
			}*/
			
			if ($masa_tahun < 1) {
				if ($masa_bulan >= 6 && $value['STATUS'] == 'KONTRAK') {
					$jatah_cuti = 6;
				}
			}elseif ($masa_tahun >= 1 && $value['STATUS'] == 'KONTRAK') {
				$jatah_cuti = 12;
			}

			$data_parse = [
				'CUTI' => $jatah_cuti,
				'last_reset_cuti' => date('Y-m-d')			
			];
			
			$this->M_masterusers->update($data_parse,$user_id);
		}
	}

	public function getTimeUpdate(){
		$tgl_skrg = date('d');
		$bln_skrg = date('m');
		$ret = [
				'status' => false,
				'msg' => ''
			];
		/*periode update = 1 januai tiap tahunnya */
		if ($tgl_skrg == '01' && $bln_skrg == '01') {
			//update
			$this->updateCutiTetap();
			$ret = [
				'status' => true,
				'msg' => 'data cuti sudah diupdate!'
			];
		}
		//else{
			$this->updateCutiKontrak();
			$ret = [
				'status' => true,
				'msg' => 'data cuti sudah diupdate!'
			];
		//}

		echo json_encode($ret);
	}

	function insertApp(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$edit=0;
		if (!empty($this->input->post('id_applicant'))) {
			$edit = 1;
		}

		$data = $this->input->post();
		$data['creator_id'] = $this->user_id;

		/*prevent unset data field (select)*/
		!isset($data['POSITION'])?$data['POSITION']='':'';
		
		!isset($data['GENDER'])?$data['GENDER']='':'';
		!isset($data['PENDIDIKAN'])?$data['PENDIDIKAN']='':'';
		!isset($data['PROPINSI'])?$data['PROPINSI']='':'';
		!isset($data['KABUPATEN'])?$data['KABUPATEN']='':'';
		!isset($data['KECAMATAN'])?$data['KECAMATAN']='':'';
		!isset($data['KELDESA'])?$data['KELDESA']='':'';
		
		!isset($data['STATUSRUMAH'])?$data['STATUSRUMAH']='':'';
		!isset($data['STATUS'])?$data['STATUS']='':'';
		!isset($data['AGAMA'])?$data['AGAMA']='':'';
		!isset($data['BERSEDIA_DITEMPAT'])?$data['BERSEDIA_DITEMPAT']='':'';
		!isset($data['ANA_LVL_COMP'])?$data['ANA_LVL_COMP']='':'';
		!isset($data['ANA_LVL_ENG'])?$data['ANA_LVL_ENG']='':'';
		!isset($data['ANA_LVL_MAN'])?$data['ANA_LVL_MAN']='':'';
		!isset($data['ANA_LVL_DAR'])?$data['ANA_LVL_DAR']='':'';
		!isset($data['ANA_LVL_MANUAL'])?$data['ANA_LVL_MANUAL']='':'';
		!isset($data['ANA_LVL_PEM'])?$data['ANA_LVL_PEM']='':'';
		!isset($data['ANA_LVL_KOR'])?$data['ANA_LVL_KOR']='':'';
		!isset($data['ANA_LVL_SEN'])?$data['ANA_LVL_SEN']='':'';
		!isset($data['ANA_LVL_MIC'])?$data['ANA_LVL_MIC']='':'';
		!isset($data['ANA_LVL_TIM'])?$data['ANA_LVL_TIM']='':'';
		!isset($data['ANA_ADA_PHR'])?$data['ANA_ADA_PHR']='':'';
		!isset($data['ANA_ADA_STR'])?$data['ANA_ADA_STR']='':'';
		!isset($data['PER_LVL_COMP'])?$data['PER_LVL_COMP']='':'';
		!isset($data['PER_LVL_ENG'])?$data['PER_LVL_ENG']='':'';
		!isset($data['PER_LVL_MAN'])?$data['PER_LVL_MAN']='':'';
		!isset($data['PER_LVL_PHL'])?$data['PER_LVL_PHL']='':'';
		!isset($data['PER_LVL_EKG'])?$data['PER_LVL_EKG']='':'';
		!isset($data['PER_LVL_TEN'])?$data['PER_LVL_TEN']='':'';
		!isset($data['PER_LVL_SPI'])?$data['PER_LVL_SPI']='':'';
		!isset($data['PER_LVL_AUD'])?$data['PER_LVL_AUD']='':'';
		!isset($data['PER_LVL_PAP'])?$data['PER_LVL_PAP']='':'';
		!isset($data['PER_ADA_STR'])?$data['PER_ADA_STR']='':'';
		!isset($data['PER_ADA_BCLS'])?$data['PER_ADA_BCLS']='':'';
		/**/

		//$data['FINGERID'] = intval($data['FINGERID']);

		$vf = [];
		$valid_form = [];
		$res_valid_form = [];
		
		foreach ($data as $key => $value) {
			$resp = $this->field_validation($key,$value);
			$vf[] = $resp['status'];
			$valid_form[$key] = $value;
			$res_valid_form[$key] = $resp['msg'];
		}

		if (empty($vf) || in_array(0,$vf)) {
			$result['status'] = false;
				$result['message'] = 'Data belum lengkap. silakan di ulangi!';
				$result['form_res'] = $res_valid_form;
		}else{
			$arr = array("TGLLAHIR", "TGL_SIAP_KERJA");
			foreach ($data as $key => $value) {
				if (in_array($key, $arr) && $value!='')
					$data[$key] = date("Y-m-d", strtotime(str_replace('/', '-', $value)) );
			}

			/*
				lampiran_str
				lampiran_mcu
			*/
			/*analis*/
			if (isset($_FILES['ana_lampiran_str']['name']) && ($_FILES['ana_lampiran_str']['name'] !== '')) {
				//for path need permission - chmod -R 777
				$config['upload_path'] = "./uploads/attachments/rekrutmen/";
				$config['allowed_types'] = '*';
				$config['encrypt_name'] = true;
				$this->upload->initialize($config);
				if ($this->upload->do_upload('ana_lampiran_str')) {
					$upload = $this->upload->data();
					$data['lampiran_str'] = $config['upload_path'] . $upload['file_name'];
				}
			}

			if (isset($_FILES['ana_lampiran_mcu']['name']) && ($_FILES['ana_lampiran_mcu']['name'] !== '')) {
				//for path need permission - chmod -R 777
				$config['upload_path'] = "./uploads/attachments/rekrutmen/";
				$config['allowed_types'] = '*';
				$config['encrypt_name'] = true;
				$this->upload->initialize($config);
				if ($this->upload->do_upload('ana_lampiran_mcu')) {
					$upload = $this->upload->data();
					$data['lampiran_mcu'] = $config['upload_path'] . $upload['file_name'];
				}
			}


			/*perawat*/
			if (isset($_FILES['per_lampiran_str']['name']) && ($_FILES['per_lampiran_str']['name'] !== '')) {
				//for path need permission - chmod -R 777
				$config['upload_path'] = "./uploads/attachments/rekrutmen/";
				$config['allowed_types'] = '*';
				$config['encrypt_name'] = true;
				$this->upload->initialize($config);
				if ($this->upload->do_upload('per_lampiran_str')) {
					$upload = $this->upload->data();
					$data['lampiran_str'] = $config['upload_path'] . $upload['file_name'];
				}
			}

			if (isset($_FILES['per_lampiran_mcu']['name']) && ($_FILES['per_lampiran_mcu']['name'] !== '')) {
				//for path need permission - chmod -R 777
				$config['upload_path'] = "./uploads/attachments/rekrutmen/";
				$config['allowed_types'] = '*';
				$config['encrypt_name'] = true;
				$this->upload->initialize($config);
				if ($this->upload->do_upload('per_lampiran_mcu')) {
					$upload = $this->upload->data();
					$data['lampiran_mcu'] = $config['upload_path'] . $upload['file_name'];
				}
			}
			
			if ($edit) {
				/*edit*/
				$id_applicant = array('id_applicant'=>$data['id_applicant']);
				$this->m_general->update_data($id_applicant,$data,'applicant');
				$this->m_general->update_data($id_applicant,$data,'admin_mis.applicant');
			}else{
				/*save*/
				$this->m_general->insert_data($data,'applicant');
				$this->m_general->insert_data($data,'admin_mis.applicant');
			}
			
			
			$result['status'] = true;
			$result['message'] = 'Data berhasil disimpan.';
				
		}
		echo json_encode($result);
	}

	function insertJob(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$edit = 1;

		$data = $this->input->post();
		$data['updateby'] = $this->user_id;

		/*prevent unset data field (select)*/
		!isset($data['active'])?$data['active']='':'';
		$vf = [];
		$valid_form = [];
		$res_valid_form = [];
		
		foreach ($data as $key => $value) {
			$resp = $this->field_validation($key,$value);
			$vf[] = $resp['status'];
			$valid_form[$key] = $value;
			$res_valid_form[$key] = $resp['msg'];
		}

		if (empty($vf) || in_array(0,$vf)) {
			$result['status'] = false;
				$result['message'] = 'Data belum lengkap. silakan di ulangi!';
				$result['form_res'] = $res_valid_form;
		}else{
			if ($edit) {
				/*edit*/
				$id_job = array('name'=>'job');
				$this->m_general->update_data($id_job,$data,'param_setting');
				$this->m_general->update_data($id_job,$data,'admin_mis.param_setting');
			}else{
				/*save*/
				$this->m_general->insert_data($data,'param_setting');
				$this->m_general->insert_data($data,'admin_mis.param_setting');
			}
			
			
			$result['status'] = true;
			$result['message'] = 'Data berhasil disimpan.';
				
		}
		echo json_encode($result);
	}

	function changeStatus(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		$data = array();
		$datax = $this->m_general->load_data(array('id_applicant'=>$this->input->post('id_applicant')),0,10,1,'applicant','id_applicant');
		
		$statusid = isset($datax[0]['status_applicant'])?$datax[0]['status_applicant']:0;
		$status = '';
		$result =  false;
		//print_r($data);
		if($statusid!=0){
				$datax = $this->m_general->load_data(array('status_seq'=>$statusid),0,10,1,'master_applicant_status','status_seq');
				$status = $datax[0]['status_name'];

				$edit = 1;
				$ismove = $this->input->post('islanjut');
				if($ismove==1)
					$statusid = $statusid+1;	
				
				
				$data['lastupdateby'] = $this->user_id;
				$data['status_applicant'] = ($statusid==8)?7:$statusid;
				
				if($statusid==8)
					$data['is_user'] = 1;
				
				$data['reason_'.$status] = $this->input->post('reason');
				$data['id_applicant']= $this->input->post('id_applicant');
				$data['date_'.$status]= date('Y-m-d');
				
				
				if (isset($_FILES['files']['name']) && ($_FILES['files']['name'] !== '')) {
					//for path need permission - chmod -R 777
					$config['upload_path'] = "./uploads/attachments/rekrutmen/";
					$config['allowed_types'] = '*';
					$config['encrypt_name'] = true;
					$this->upload->initialize($config);

					$count_file = count($_FILES['files']['name']);

					for ($i = 0; $i < $count_file; $i++) {
						$_FILES['file']['name'] = $_FILES['files']['name'][$i];
						$_FILES['file']['type'] = $_FILES['files']['type'][$i];
						$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
						$_FILES['file']['error'] = $_FILES['files']['error'][$i];
						$_FILES['file']['size'] = $_FILES['files']['size'][$i];

						if ($this->upload->do_upload('files')) {

							$datax = array('upload_data' => $this->upload->data());
							
							$data['file_'.$status] = $config['upload_path'] . $datax['upload_data']['file_name'];
							
							//$id_job = array('id_applicant'=>$data['id_applicant']);
							//$this->m_general->update_data($id_job,$data,'applicant');
						} else {
							//echo json_encode(array('status' => true, 'message' => 'Data berhasil disimpan'));
						}
					}
				}
				
				
				if (isset($_FILES['files2']['name']) && ($_FILES['files2']['name'] !== '')) {
					//for path need permission - chmod -R 777
					$config['upload_path'] = "./uploads/attachments/rekrutmen/";
					$config['allowed_types'] = '*';
					$config['encrypt_name'] = true;
					$this->upload->initialize($config);

					$count_file = count($_FILES['files2']['name']);

					for ($i = 0; $i < $count_file; $i++) {
						$_FILES['file']['name'] = $_FILES['files2']['name'][$i];
						$_FILES['file']['type'] = $_FILES['files2']['type'][$i];
						$_FILES['file']['tmp_name'] = $_FILES['files2']['tmp_name'][$i];
						$_FILES['file']['error'] = $_FILES['files2']['error'][$i];
						$_FILES['file']['size'] = $_FILES['files2']['size'][$i];

						if ($this->upload->do_upload('files2')) {

							$datax = array('upload_data' => $this->upload->data());
							
							$data['file_'.$status.'2'] = $config['upload_path'] . $datax['upload_data']['file_name'];
							
							//$id_job = array('id_applicant'=>$data['id_applicant']);
							//$this->m_general->update_data($id_job,$data,'applicant');
						} else {
							//echo json_encode(array('status' => true, 'message' => 'Data berhasil disimpan'));
						}
					}
				}
				
				
				$vf = [];
				$valid_form = [];
				$res_valid_form = [];
				
				
				
				foreach ($data as $key => $value) {
					$resp = $this->field_validation($key,$value);
					$vf[] = $resp['status'];
					$valid_form[$key] = $value;
					$res_valid_form[$key] = $resp['msg'];
				}

				if (empty($vf) || in_array(0,$vf)) {
					$result['status'] = false;
						$result['message'] = 'Data belum lengkap. silakan di ulangi!';
						$result['form_res'] = $res_valid_form;
				}else{
					if ($edit) {
						
						$id_job = array('id_applicant'=>$data['id_applicant']);
						$this->m_general->update_data($id_job,$data,'applicant');
						$this->m_general->update_data($id_job,$data,'admin_mis.applicant');
					}else{
						
						$this->m_general->insert_data($data,'param_setting');
						$this->m_general->insert_data($data,'admin_mis.param_setting');
					}
					$result['status'] = true;
					$result['message'] = 'Data berhasil disimpan.';
					//INSERT KARYAWAN
					if($statusid==7){
						
						$temp = $this->m_general->load_data(array('id_applicant'=>$data['id_applicant']),0,10,1,'applicant','id_applicant');
						$kar = $temp[0];
						
						
						$salt = 'Bio Medika';
						$password = '123456';
						$def_passwd = sha1($salt . $password);
						$username = explode(' ', strtolower($kar['name']));
						$username = $username[0];
						$datatemp['username'] = $username;
						$datatemp['password'] = $def_passwd;
						// $data['branch_id'] = $this->branch_id;
						$datatemp['creator_id'] = $this->user_id;
						$datatemp['name'] = $kar['name'];
						$datatemp['TMTLAHIR'] = $kar['TMTLAHIR'];
						$datatemp['TGLLAHIR'] = $kar['TGLLAHIR'];
						
						$datatemp['last_name'] = $kar['last_name'];
						$datatemp['PENDIDIKAN'] = $kar['PENDIDIKAN'];
						$datatemp['TINGGI'] = $kar['TINGGI'];
						$datatemp['BERAT'] = $kar['BERAT'];
						$datatemp['GOLDARAH'] = $kar['GOLDARAH'];
						$datatemp['GENDER'] = $kar['GENDER'];
						$datatemp['KABUPATEN'] = $kar['KABUPATEN'];
						$datatemp['KECAMATAN'] = $kar['KECAMATAN'];
						$datatemp['KELDESA'] = $kar['KELDESA'];
						$datatemp['PROPINSI'] = $kar['PROPINSI'];
						$datatemp['RTRW'] = $kar['RTRW'];
						
						$datatemp['STATUSKAWIN'] = $kar['STATUS'];
						$datatemp['ALAMAT'] = $kar['ALAMAT'];
						$datatemp['JMLANAK'] = $kar['JMLANAK'];
						$datatemp['AGAMA'] = $kar['AGAMA'];
						$datatemp['TELPONRUMAH'] = $kar['TELPONRUMAH'];
						$datatemp['TELPONHP'] = $kar['TELPONHP'];
						$datatemp['STATUS'] = 'TETAP';
						$datatemp['branch_id'] = '16';
						
						$id = $this->m_general->insert_data($datatemp,'users');
						$id = $this->m_general->insert_data($datatemp,'admin_mis.users');
						$result['message'] = 'insertkaryawan_'.$id;
						
						
						$datax['is_user'] = $id;
						$id_job = array('id_applicant'=>$data['id_applicant']);
						$this->m_general->update_data($id_job,$datax,'applicant');
						$this->m_general->update_data($id_job,$datax,'admin_mis.applicant');
						
					}
					
					
					
						
				}
		
			//}
		}
		
		echo json_encode($result);
	}

	function deleteApp(){
		$result['status'] = false;
		$result['message'] = 'Data gagal dihapus.';

		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}
		
		$param['id_applicant'] = $this->input->post('id_applicant');
		$del = $this->m_mis->delete($param,'applicant');
		$del = $this->m_mis->delete($param,'admin_mis.applicant');

		if ($del) {
			$result['status'] = true;
			$result['message'] = 'Data berhasil dihapus.';
		}
		echo json_encode($result);
	}

	public function set_user_read() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$id = $this->input->post('id');
		$this->load->library("Notify_lib");
		$nl = new Notify_lib();
		//print_r($this->user_id. "karyawan". $id);
		$nl->mark_read_2($id);


		echo json_encode(array(
			"status" => true
		));
	}

	public function updateStatusKaryawan(){
		$param = [];
		$param['in_param'] = [
			'STATUS' => [
				'KONTRAK',
			]
		];
		$data = $this->M_masterusers->load_data($param,0,0,1);
		$updated = 0;
		foreach ($data as $key => $value) {
			/* count masa kerja */
			$now = date('Y-m-d');
			$tgl_masuk = $value['TGLMASUK'];
			$user_id = $value['id_user'];
			$masa_tahun = 0;
			$masa_bulan = 0;
			
			if ($tgl_masuk == '0000-00-00' || $tgl_masuk == '0000-00-00 00:00:00' || $tgl_masuk == null || empty($tgl_masuk)) {
				$masa_tahun = 0;
			}else{
				$resTgl = $this->misc->selisih_waktu($now,$tgl_masuk);
				$masa_tahun = $resTgl['tahun'];
				if ($masa_tahun < 1) {
					$masa_bulan = $resTgl['bulan'];
				}
			}

			if ($masa_tahun > 2) {
				$data_parse = [
					'STATUS' => 'TETAP'	
				];
				$this->M_masterusers->update($data_parse,$user_id);
				$updated = $updated+1; 
			}
			
			
		}
		echo json_encode(['msg' => 'done','updated' => $updated]);
	}

	public function edit_bagian(){
        $result['status'] = false;
        $result['message'] = 'Data tidak ditemukan.';

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['id'] = $this->input->post('id');


        $result['status'] = true;
        $result['message'] = 'Data ditemukan';
        $edit_data = $this->m_mis->edit_data($data, $this->model2)->row();
        if(!empty($edit_data)){
            $result['data'] = (array) $edit_data;
        }

        echo json_encode($result);
    }

    public function save_bagian(){
        $result['status'] = false;
        $result['message'] = 'Data berhasil disimpan.';

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['id_department'] = $this->input->post('id_department');
        $data['name'] = $this->input->post('name');

        $id = $this->input->post('id'); 

        if(!empty($id)){
            $where['id'] = $this->input->post('id');
            $id = $this->m_mis->update_data($where, $data, $this->model2);
			$id = $this->m_mis->update_data($where, $data, "admin_mis.bagian");
        }else{
            $id = $this->m_mis->insert_data($data, $this->model2);
			$id = $this->m_mis->insert_data($data, "admin_mis.bagian");
        }
        if($id){
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        }

        echo json_encode($result);
    }

    public function get_all_bagian(){
         if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data = array();
        $data_bagian = $this->db->query('select dpt.name as department_name,bg.* from bagian bg,departments dpt where bg.id_department = dpt.id_department')->result();

        if (!empty($data_bagian)) {
            $i = 0;
            foreach ($data_bagian as $key => $value) {
                $data['data'][$i]['no'] = $i+1;
                $data['data'][$i]['id'] = $value->id;
                $data['data'][$i]['department_name'] = $value->department_name;
                $data['data'][$i]['bagian_name'] = $value->name;
                $i++;
            }
        }
        echo json_encode($data);
    }

    public function delete_bagian(){
        $result['status'] = false;
        $result['message'] = 'Data gagal dihapus.';

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['id'] = $this->input->post('id');


        $delete = $this->m_mis->delete_data($data, $this->model2);
		$delete = $this->m_mis->delete_data($data, "admin_mis.bagian");

        if($delete){
            $result['status'] = true;
            $result['message'] = 'Data berhasil dihapus.';
        }

        echo json_encode($result);
    }

    public function edit_pos(){
        $result['status'] = false;
        $result['message'] = 'Data tidak ditemukan.';

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['id_position'] = $this->input->post('id_position');


        $result['status'] = true;
        $result['message'] = 'Data ditemukan';
        $edit_data = $this->m_mis->edit_data($data, $this->model3)->row();
        if(!empty($edit_data)){
            $result['data'] = (array) $edit_data;
        }

        echo json_encode($result);
    }

    public function save_pos(){
        $result['status'] = false;
        $result['message'] = 'Data berhasil disimpan.';

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['name_position'] = $this->input->post('name_position');

        $id_position = $this->input->post('id_position'); 

        if(!empty($id_position)){
            $where['id_position'] = $this->input->post('id_position');
            $id_position = $this->m_mis->update_data($where, $data, $this->model3);
			$id_position = $this->m_mis->update_data($where, $data, "admin_mis.position");
        }else{
            $id_position = $this->m_mis->insert_data($data, $this->model3);
			$id_position = $this->m_mis->insert_data($data, "admin_mis.position");
        }
        if($id_position){
            $result['status'] = true;
            $result['message'] = 'Data berhasil disimpan.';
        }

        echo json_encode($result);
    }

    public function get_all_pos(){
         if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data = array();
        $data_pos = $this->m_mis->get_data('position')->result();

        if (!empty($data_pos)) {
            $i = 0;
            foreach ($data_pos as $key => $value) {
                $data['data'][$i]['no'] = $i+1;
                $data['data'][$i]['id_position'] = $value->id_position;
                $data['data'][$i]['name_position'] = $value->name_position;
                $i++;
            }
        }
        echo json_encode($data);
    }

    public function delete_pos(){
        $result['status'] = false;
        $result['message'] = 'Data gagal dihapus.';

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['id_position'] = $this->input->post('id_position');


        $delete = $this->m_mis->delete_data($data, $this->model3);
		$delete = $this->m_mis->delete_data($data, "admin_mis.position");

        if($delete){
            $result['status'] = true;
            $result['message'] = 'Data berhasil dihapus.';
        }

        echo json_encode($result);
    }

     public function get_all_branch()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        

        $data = array();
        $data_branch = $this->m_mis->get_data('branch')->result();

        if (!empty($data_branch)) {
            $i = 0;
            foreach ($data_branch as $key => $value) {
                $data['data'][$i]['no'] = $i+1;
                $data['data'][$i]['branch_id'] = $value->branch_id;
                $data['data'][$i]['branch_name'] = $value->branch_name;
                $data['data'][$i]['branch_code'] = $value->branch_code;
                $i++;
            }
        }
        echo json_encode($data);
    }


	public function gantiCabang($selected_id)
    {
        $branch_id = $this->input->post("branch_id");
		$update_data = [
			'branch_id' => $branch_id
		];

		$r = $this->M_masterusers->update($update_data,$selected_id);

        echo json_encode($r);
    }

	


}


