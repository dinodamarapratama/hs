<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

class Dailyreports extends CI_Controller {
    public $model = 'dailyreports';
    public $modelDetail = 'dailyreports_receiver';
    public $modelDetail1 = 'dailyreports_attachment';
    public $modelDetail2 = 'dailyreports_reply';
    public $modelDetail3 = 'notifications';

    public function __construct() {
        parent::__construct();
        //cek login
        if ($this->session->userdata('status') != "login") {
            /*we set session to history if this is notif url*/
            $params = $_SERVER['QUERY_STRING'];
            if (!empty($params)) {
                $fullUrl = current_url().'?'.$params;
                $this->session->set_userdata('notif_url',$fullUrl);
            }else{
                $this->session->set_userdata('notif_url','');
            }
            redirect(base_url() . 'welcome?pesan=belumlogin');
        }
        
        $this->load->library('upload');
        $this->load->helper('tgl_indo');
        $this->user_id = $this->session->userdata('id_user');
        $this->id_bagian = $this->session->userdata('id_bagian');
        $this->id_position = $this->session->userdata('id_position');
        $this->first_name = $this->session->userdata('name');
        $this->last_name = $this->session->userdata('last_name');
        $this->call_name = $this->session->userdata('call_name');
        $this->position_name = $this->session->userdata('position_name');
        $this->branch_name = $this->session->userdata('branch_name');
        $this->branch_id = $this->session->userdata('branch_id');

        

        ini_set('display_errors',0);
        $cek_user = $this->db->query("select * from master_menu m left join role_menu_dept r on r.id_menu = m.id_menu left join role_menu_pos p on p.id_menu = m.id_menu left join role_menu_user u on u.id_menu = m.id_menu where u.id_user =  $this->user_id and m.url = 'admin/dailyreports' group by m.nama")->row();
        if ($cek_user->id_menu < 1) {

        echo "<script>
        alert('Anda tidak mempunyai Akses');
        window.location.href='dashboard';
        </script>";
        }
        else {}
        
    }

    public function index($call='') {
        helper_log("akses", "Akses Menu Daily Report");
        set_time_limit(10*60);
        $search = $this->input->get();
        $data = [
            'mobile_menu' => $this->mysidebar->build_menu_mobile(),
            'sidebar_menu' => $this->mysidebar->asHtml([], true),
            // 'list_department' => $this->m_master_data->many_department(),
            'list_bagian' => $this->m_master_data->many_bagian(),
            'list_position' => $this->m_master_data->many_position(),
            'list_department' => $this->m_master_data->many_department(),
            'show_absensi' => true,
            'show_support' => false,
            'show_daily' => true,
            'show_close' => true
        ];
        $dept = $this->session->userdata('position_name');
        if(empty($dept)) {
            $dept = '';
        }
        if(in_array($this->user_id, [48,62,30,68,81,67,55,37,57]) ) {
            $data['show_absensi'] = true;
        }
        // if(in_array($this->id_bagian, [19,7,1,20])) {
        //     $data['show_support'] = true;
        // }
        /** tampilkan field daily report untuk branch manager, direktur utama, dan ass. general manager */
        if(in_array($this->id_position, [7, 1, 20,9,4,13,19])) {
            $data['show_daily'] = true;
        }

        if(in_array($this->user_id, [48])) {
            $data['show_daily'] = true;
        }

        if(in_array($this->id_position, [7, 5])) {
            $data['show_close'] = true;
        }
        // var_dump($dept);
        // var_dump($data);
        // exit();

        $this->load->model('restapi/user_model');
		
		// fungsi peniadaan filter dokter PJ freelance yang tidak sesuai cabang
		$blockUsers = [];
		$users = $this->user_model->many_user(['where_in' => ['_.id_position' => 13, '_.STATUS' => 'FREELANCE' ]]);
		foreach ($users as $key => $usr) {
			$input   = $usr['many_branch'];
			$numbers = str_split("',");
			$output  = str_replace($numbers,' ',$input);
			$numArray = explode(" ", $output);
			$final = (array_values(array_filter($numArray)));
			if (in_array($this->branch_id, $final) == false){
				$blockUsers[] = $usr['id_user'];
			}
		}

		$data['users_drpj'] = $this->user_model->many_user([
			'where_not_in' => ['_.id_user' => $blockUsers],
		]);

        
        $data['received'] = $this->view_received(true, $search);
        $data['sent'] = $this->view_send(true, $search);
        $data['archived'] = $this->view_archived(true, $search);
        $data['draft'] = $this->view_draft(true, $search);
        $data['position'] = $this->m_mis->get_data('position')->result_array();

        $data['model'] = $this->model;
        $data['modelDetail'] = $this->modelDetail;
        $data['modelDetail1'] = $this->modelDetail1;

        $this->load->model('M_masterdepartment');
        $data['departments'] = $this->M_masterdepartment->get_data('departments')->result();


        // print_r($data['received']);
        // exit();
        if ($call=='js') {
           
			$this->load->view('admin/dailyreports/v_daily', $data);
        }else{

            $user = $this->m_mis->get_karyawan(['id_user' => $this->session->userdata('id_user')],0,0,1);
            $this->status_karyawan = $user[0]['STATUS'];
			
            $this->load->view('admin/inc/v_header', $data);
			$this->load->view('admin/dailyreports/v_daily', $data);   
			$this->load->view('admin/inc/v_footer', $data);    
        }
        
    }

    public function index_js() {
        set_time_limit(10*60);
        $search = $this->input->get();
        $data = [
            //'mobile_menu' => $this->mysidebar->build_menu_mobile(),
            //'sidebar_menu' => $this->mysidebar->asHtml([], true),
            // 'list_department' => $this->m_master_data->many_department(),
            'list_bagian' => $this->m_master_data->many_bagian(),
            'list_position' => $this->m_master_data->many_position(),
            'list_department' => $this->m_master_data->many_department(),
            'show_absensi' => true,
            'show_support' => false,
            'show_daily' => true,
            'show_close' => true
        ];
        $dept = $this->session->userdata('position_name');
        if(empty($dept)) {
            $dept = '';
        }
        if(in_array($this->user_id, [48,62,30,68,81,67,55,37,57]) ) {
            $data['show_absensi'] = true;
        }
        // if(in_array($this->id_bagian, [19,7,1,20])) {
        //     $data['show_support'] = true;
        // }
        /** tampilkan field daily report untuk branch manager, direktur utama, dan ass. general manager */
        if(in_array($this->id_position, [7, 1, 20,9,4,13,19])) {
            $data['show_daily'] = true;
        }

        if(in_array($this->user_id, [48])) {
            $data['show_daily'] = true;
        }

        if(in_array($this->id_position, [7, 5])) {
            $data['show_close'] = true;
        }
        // var_dump($dept);
        // var_dump($data);
        // exit();
        $data['received'] = $this->view_received(true, $search);
        $data['sent'] = $this->view_send(true, $search);
        $data['archived'] = $this->view_archived(true, $search);
        $data['draft'] = $this->view_draft(true, $search);
        $data['position'] = $this->m_mis->get_data('position')->result_array();

        $data['model'] = $this->model;
        $data['modelDetail'] = $this->modelDetail;
        $data['modelDetail1'] = $this->modelDetail1;

        $this->load->model('M_masterdepartment');
        $data['departments'] = $this->M_masterdepartment->get_data('departments')->result();


        // print_r($data['received']);
        // exit();

        //$this->load->view('admin/inc/v_header', $data);
        //$this->load->view('admin/dailyreports/v_daily', $data);
		$this->load->view('admin/dailyreports/v_daily', $data);
        //$this->load->view('admin/inc/v_footer', $data);
    }

    public function send() {


        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data = $this->input->post($this->model);
        $id = $this->input->post('id_dailyreports');

        // $receiver_id = $data['to_id'];
        $receiver_id = $this->input->post('to_id');
		
		// $receiver_id = $data['to_id'];
		unset($data['to_id']);
		
        if(is_array($receiver_id) == false) {
            $receiver_id = explode(",", $receiver_id);
        }
        // print_r($receiver_id);
        // exit();
        foreach ($receiver_id as $key => $rcv) {
            if(empty($rcv) == true || $rcv == null) {
                unset($receiver_id[$key]);
            }
        }

        $receiver_filterAbsensi = $receiver_id;

        $pos = $this->input->post('position');
        if(is_array($pos) == false) {
            $pos = [ $pos ];
        }
        $userPos = [];
        $this->load->model('restapi/user_model');
        if(empty($pos) == false) {
            $users = $this->user_model->many_user([
                'where_in' => ['pos.id_position' => $pos],
                'where' => ['_.STATUS !=' => 'KELUAR']
            ]);
            foreach ($users as $key => $usr) {
                $userPos[] = $usr['id_user'];
                $receiver_id[] = $usr['id_user'];
            }
        }

        $dpt = $this->input->post('department');
        if(is_array($dpt) == false) {
            $dpt = [ $dpt ];
        }
        $userDpt = [];
        $this->load->model('restapi/user_model');
        if(empty($dpt) == false) {
            $users = $this->user_model->many_user([
                'where_in' => ['dep.id_department' => $dpt],
                'where' => ['_.STATUS !=' => 'KELUAR']
            ]);
            foreach ($users as $key => $usr) {
                $userDpt[] = $usr['id_user'];
                $receiver_id[] = $usr['id_user'];
            }
        }

        $bagian = $this->input->post('bagian');
        if(is_array($bagian) == false) {
            $bagian = [ $bagian ];
        }


        $userBagian = [];
        $this->load->model('restapi/user_model');
        if(empty($bagian) == false) {
            $users = $this->user_model->many_user([
                'where_in' => ['bg.id' => $bagian],
                'where' => ['_.STATUS !=' => 'KELUAR']
            ]);
            foreach ($users as $key => $usr) {
                $userBagian[] = $usr['id_user'];
                $receiver_id[] = $usr['id_user'];
            }
        }
        // $dept = $this->input->post('department');
        // if (is_array($dept) == false) {
        //     $dept = [$dept];
        // }
        // $userDept = [];
        // $this->load->model('restapi/user_model');
        // if (empty($dept) == false) {
        //     $users = $this->user_model->many_user(['where_in' => ['dep.id_department' => $dept]]);
        //     foreach ($users as $key => $user) {
        //         $userDept[] = $user['id_user'];
        //     }
        // }

        $temp = [];
        foreach ($receiver_id as $key => $user_id) {
            if(in_array($user_id, $temp)) {
                continue;
            }
            $temp[] = $user_id;
        }
        $receiver_id = $temp;

        // if(empty($receiver_id) == true && empty($userDept) == true) {
        /*
            batal menyimpan data baru ketika receiver kosong dan bukan draft
        */
        if(empty($receiver_id) == true && empty($id) == true && in_array($data['id_draft'], ['1']) == false) {
            echo json_encode(['rcv' => $receiver_id, 'rcv_bagian' => $userBagian, 'bagian' => $bagian]);
            exit();
        }
        // var_dump($receiver_id);
        // // var_dump($userDept);
        // var_dump($userBagian);
        // exit();

        $data['from_id'] = $this->user_id;
        $sender = $this->user_model->one_user(['where' => ['_.id_user' => $data['from_id']]]);
        if($sender == null) {
            echo json_encode(['status' => false, 'message' => 'Invalid Sender']);
            exit();
        }
        $data['is_daily'] = 1;
        $receiver_absensi = [];

        if ($data['absensi_des1'] == 'on') {
            $data['absensi_des'] = htmlentities($data['absensi_des']);
            $data['is_absensi'] = 1;

            /* tambah user pos 24 ke dalam receiver */
            // $users = $this->user_model->many_user(['where_in' => ['_.id_user' => [48,62,30,68,81,67,55,37,57]]]);
            // foreach ($users as $key => $usr) {
            //     $receiver_id[] = $usr['id_user'];
            //     $receiver_absensi[] = $usr['id_user'];
            // }

            // mode HRD saja
            // $users = $this->user_model->many_user(['where_in' => ['_.id_bagian' => [22]]]);
            // foreach ($users as $key => $usr) {
            //     $receiver_id[] = $usr['id_user'];
            //     $receiver_absensi[] = $usr['id_user'];
            // }



            $users = $this->user_model->many_user(['where_in' => ['_.id_user' => [48,62,68,81,67,55,37,57]]]);
            foreach ($users as $key => $usr) {
                $receiver_id[] = $usr['id_user'];
                $receiver_absensi[] = $usr['id_user'];
            }
            
        }
        else {
            $data['absensi_des'] = $data['absensi_des1'];
        }
        if ($data['reg_des2'] == 'on') {
            $data['reg_des'] = htmlentities($data['reg_des']);
        }
        else {
            $data['reg_des'] = $data['reg_des2'];
        }
        if ($data['sampling_des3'] == 'on') {
            $data['sampling_des'] = htmlentities($data['sampling_des']);
        }
        else {
            $data['sampling_des'] = $data['sampling_des3'];
        }

        if ($data['qc_des4'] == 'on') {
            $data['qc_des'] = htmlentities($data['qc_des']);
        }
        else {
            $data['qc_des'] = $data['qc_des4'];
        }
        if ($data['alat_des5'] == 'on') {
            $data['alat_des'] = htmlentities($data['alat_des']);
        }
        else {
            $data['alat_des'] = $data['alat_des5'];
        }
        if ($data['janji_des6'] == 'on') {
            $data['janji_des'] = htmlentities($data['janji_des']);
        }
        else {
            $data['janji_des'] = $data['janji_des6'];
        }
        if ($data['lain_des7'] == 'on') {
            $data['lain_des'] = htmlentities($data['lain_des']);
        }
        else {
            $data['lain_des'] = $data['lain_des7'];
        }

        if ($data['pelayanan_des8'] == 'on') {
            $data['pelayanan_des'] = htmlentities($data['pelayanan_des']);
        }
        else {
            $data['pelayanan_des'] = $data['pelayanan_des8'];
        }



        if($data['reg_des'] == '2' && $data['sampling_des'] == '3' && $data['qc_des'] == '4'
            && $data['alat_des'] == '5' && $data['janji_des'] == '6' && $data['lain_des'] == '7'
            && $data['pelayanan_des'] == '8' && ($data['absensi_des'] != '1')) {
            $data['is_daily'] = 0;
        }

        $data['last_update'] = date('Y-m-d H:i:s');
        $data['is_view'] = 1;

        unset($data['to_id']);
        unset($data['absensi_des1']);
        unset($data['reg_des2']);
        unset($data['sampling_des3']);
        unset($data['qc_des4']);
        unset($data['alat_des5']);
        unset($data['janji_des6']);
        unset($data['lain_des7']);
        unset($data['pelayanan_des8']);

        // unset($data['it_des']);

        $result = [];

        if (!empty($id)) {
            $this->m_mis->update_data(array(
                'id' => $id
            ) , $data, $this->model);
            /*update data*/
            $last_receivers = [];
            $last_receivers = $this->m_mis->get_user_read($id)->result();
            
            if (!empty($last_receivers)) {
                /*formating*/
                $tmp_receivers = [];
                foreach ($last_receivers as $key => $value) {
                    $tmp_receivers[$value->id_receiver] = $value->id_receiver;
                }
                unset($last_receivers);
                $last_receivers = $tmp_receivers;
            }


            if (!empty($receiver_id)) {
                $to_id = $receiver_id;

                $count_send = $to_id;
                foreach ($count_send as $key => $value) {

                    $data_receiver = ['is_closed' => '0', ];

                    /*insert new or just update*/
                    $cek_old = in_array($value,$last_receivers);
                    if ($cek_old) {
                        /*update*/
                        $status = $this->m_mis->update_data(array(
                            'id_dailyreports' => $id,
                            'id_receiver' => $value
                        ) , $data_receiver, $this->modelDetail);

                    }else{
                        /*insert new*/
                        /*create new receivers record*/
                        $id_dailyreports = $id;
                        $data_receiver = [
                            'id_dailyreports' => $id_dailyreports,
                            'id_receiver' => $value,
                        ];
                        
                        $status = $this->m_mis->insert_data($data_receiver, $this->modelDetail);
                        $data_receiver['receivers'][] = $value;
                        /**/
                    }



                }

                // stay
                /*add notify*/
                $this->load->library('Notify_lib');
                $ntf = new Notify_lib();
                $ntf->send(
                        'New Daily Report',
                        $sender['name'] . ' - '.$sender['position_name'].' ('.$sender['branch_name'].') has sent new Daily Report',
                        $sender['id_user'],
                        $data_receiver['receivers'],
                        ['id' => $id_dailyreports],
                        'dailyreport',
                        true
                    );
                if($data['absensi_des'] != '1') {
                    $ntf->send(
                        'New Absensi Report',
                        $sender['name'] . ' - '.$sender['position_name'].' ('.$sender['branch_name'].') has sent new Absensi Report',
                        $sender['id_user'],
                        $receiver_absensi,
                        ['id' => $id_dailyreports],
                        'dailyreport',
                        false
                    );
                }



                if(in_array($data['absensi_des'], ['1']) == false) {
                    $this->load->model('m_absensi');
                    $data_absensi = [
                        'id_dailyreports' => $id_dailyreports,
                        'from_id' => $this->user_id,
                        'absensi_des' => $data['absensi_des'],
                        'send_notif' => false
                    ];
                    $r = $this->m_absensi->save(
                        $data_absensi,
                        [48], // user receiver
                        [], // dept receiver
                        [], // pos receiver
                        [22], // bagian
                        '__file__'
                    );
                }
                /**/
                echo json_encode(array(
                    "status" => true,
                    'message' => 'Data Berhasil dikirim'
                ));
            }
            else {
                echo json_encode(array(
                    "status" => true,
                    'message' => 'Data Berhasil diubah'
                ));
            }

        }
        elseif ($data['id_draft'] == '1') {

            $id_dailyreports = $this->m_mis->insert_data($data, $this->model);
            $data_receiver = ['id_dailyreports' => $id_dailyreports];

            if (!empty($receiver_id)) {
                // $to_id = substr(trim($receiver_id) , 0, -1);

                // $count_send = explode(',', $to_id);
                // foreach ($userDept as $key => $id_user) {
                //     $count_send[] = $id_user;
                // }
                $list_added = [];

                foreach ($receiver_id as $key => $value) {
                    if (empty($value)) {
                        continue;
                    }
                    if (in_array($value, $list_added)) {
                        continue;
                    }
                    if($value == $this->user_id) {
                        continue;
                    }
                    $list_added[] = $value;

                    $data_receiver = ['id_dailyreports' => $id_dailyreports, 'id_receiver' => $value, 'is_closed' => 1 ];
                    $status = $this->m_mis->insert_data($data_receiver, $this->modelDetail);
                }
            }
            echo json_encode(array(
                "status" => true,
                'message' => 'Data Masuk Draft'
            ));

        }
        else {
           
            // send
            // stay
            $id_dailyreports = $this->m_mis->insert_data($data, $this->model);
            $data_receiver = ['id_dailyreports' => $id_dailyreports];
            $IT['id_dailyreports'] = $id_dailyreports;
            $list_added = [];
            if (!empty($receiver_id)) {
                foreach ($receiver_id as $key => $value) {
                    if (empty(trim($value))) {
                        continue;
                    }
                    if (in_array($value, $list_added)) {
                        continue;
                    }
                    if ($value == $this->user_id) {
                        continue;
                    }
                    $list_added[] = $value;

                    $data_receiver = ['id_dailyreports' => $id_dailyreports, 'id_receiver' => $value, ];
                    $status = $this->m_mis->insert_data($data_receiver, $this->modelDetail);
                    // echo "save receiver";
                    // print_r($data_receiver);
                }
                $result['receiver'] = $list_added;

                // fungsi peniadaan notifikasi absensi khusus dokter PJ freelance

                $filterUsers = [];
                $users = $this->user_model->many_user(['where_in' => ['_.id_user' => $result['receiver']]]);
                foreach ($users as $key => $usr) {
                    $status = $usr['STATUS'];
                    $id_position = $usr['id_position'];
                    if ($status != "FREELANCE" && $id_position != 13){
                        $filterUsers[] = $usr['id_user'];
                    }elseif($status != "FREELANCE" && $id_position == 13){
                        $filterUsers_[] = $usr['id_user'];
                    }elseif($status == "FREELANCE" && $id_position == 13 && (($data['absensi_des'] == "1") == true)){
                        $filterUsers_[] = $usr['id_user'];
                    }else{}
                }

                // fungsi peniadaan notifikasi absensi ke receivers to_id
                $filterUsersFinal = [];
                foreach ($filterUsers as $key => $usr) {
                    if (!in_array($usr, $receiver_filterAbsensi)) {
                        $filterUsersFinal[] = $usr;
                    }
                }

                // print_r($filterUsersFinal);
                // exit();
                // print_r($result['receiver']);
                // print_r("--------");
                // print_r($filterUsers);

                // if(isset($data['is_it']) == true && $data['is_it'] == '1') {
                //     $this->load->model('restapi/generalreports_model');
                //     $this->generalreports_model->save($IT, [], [
                //         26, /* kirim ke departemen GA */
                //         24, /* kirim ke departemen IT */
                //     ]);
                // }

                $this->load->library('Notify_lib');
                $ntf = new Notify_lib();
                /* kirim notif ke penerima hanya jika ada yg no */
                if ($data['reg_des'] !== '2' || $data['sampling_des'] !== '3'
                    || $data['qc_des'] !== '4' || $data['alat_des'] !== '5' || $data['janji_des'] !== '6'
                    || $data['lain_des'] !== '7' || $data['pelayanan_des'] !== '8') {
                    $data_receiver['receivers'] = $list_added;
                    // $this->set_notification($data_receiver);
                    $ntf->send(
                        'New Daily Report',
                        $sender['name'] . ' - '.$sender['position_name'].' ('.$sender['branch_name'].') has sent new Daily Report',
                        $sender['id_user'],
                        $result['receiver'],
                        ['id' => $id_dailyreports],
                        'dailyreport',
                        true
                    );

                } else {
                    // $ntf->send(
                    //     'New Daily Report',
                    //     $sender['name'] . ' - '.$sender['position_name'].' ('.$sender['branch_name'].') has sent new Daily Report',
                    //     $sender['id_user'],
                    //     $result['receiver'],
                    //     ['id' => $id_dailyreports],
                    //     'dailyreport',
                    //     true
                    // );
                }

                // if($data['absensi_des'] != '1') {
                    // $ntf->send(
                    //     'New Absensi Report',
                    //     $sender['name'] . ' - '.$sender['position_name'].' ('.$sender['branch_name'].') has sent new Absensi Report',
                    //     $sender['id_user'],
                    //     $receiver_absensi,
                    //     ['id' => $id_dailyreports],
                    //     'dailyreport',
                    //     false
                    // );
                // }

                // if(in_array($data['absensi_des'], ['1']) == false) {
                //     $this->load->model('m_absensi');
                //     $data_absensi = [
                //         'id_dailyreports' => $id_dailyreports,
                //         'from_id' => $this->user_id,
                //         'absensi_des' => $data['absensi_des'],
                //         'send_notif' => false
                //     ];
                //     $r = $this->m_absensi->save(
                //         $data_absensi,
                //         [48], // user receiver
                //         [], // dept receiver
                //         [], // pos receiver
                //         [22], // bagian
                //         '__file__'
                //     );
                // }

                if($data['absensi_des'] != '1') {
                    $this->load->model('m_absensi');
                    $data_absensi = [
                        'id_dailyreports' => $id_dailyreports,
                        'from_id' => $this->user_id,
                        'absensi_des' => $data['absensi_des'],
                        'send_notif' => true
                    ];
                    $r = $this->m_absensi->save(
                        $data_absensi,
                        $filterUsersFinal, // user receiver
                        [], // dept receiver
                        [], // pos receiver
                        [22], // bagian
                        '__file__'
                    );
                }

            }

            echo json_encode(array(
                "status" => true,
                'message' => 'Data Berhasil dikirim',
                'r' => $result
            ));
        }

        if (isset($_FILES['files']['name']) && ($_FILES['files']['name'] !== '')) {
            //for path need permission - chmod -R 777
            $config['upload_path'] = "uploads/attachments/dailyreport/";
            $config['allowed_types'] = '*';
            $config['encrypt_name'] = true;
            $this->upload->initialize($config);

            $count_file = count($_FILES['files']['name']);

            for ($i = 0;$i < $count_file;$i++) {
                $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                $_FILES['file']['size'] = $_FILES['files']['size'][$i];

                if ($this->upload->do_upload('file')) {

                    $data = array(
                        'upload_data' => $this->upload->data()
                    );
                    $data_attachment = ['dailyreport_id' => $id_dailyreports, 'title' => $data['upload_data']['file_name'], 'description' => '', 'path' => $config['upload_path'] . $data['upload_data']['file_name'], 'filetype' => $data['upload_data']['file_type'], 'filesize' => $data['upload_data']['file_size'], 'type' => 0, 'name' => $_FILES['files']['name'][$i], 'creator_id' => $this->user_id ];
                    $this->m_mis->insert_data($data_attachment, $this->modelDetail1);
                }
                else {
                    echo json_encode(array(
                        'status' => 'false',
                        'message' => $this->upload->display_errors()
                    ));
                }
            }
        }

        if (isset($_FILES['filesJ']['name']) && ($_FILES['filesJ']['name'] !== '')) {
            //for path need permission - chmod -R 777
            $config['upload_path'] = "uploads/attachments/dailyreport/";
            $config['allowed_types'] = '*';
            $config['encrypt_name'] = true;
            $this->upload->initialize($config);

            $count_file = count($_FILES['filesJ']['name']);

            for ($i = 0;$i < $count_file;$i++) {
                $_FILES['file']['name'] = $_FILES['filesJ']['name'][$i];
                $_FILES['file']['type'] = $_FILES['filesJ']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['filesJ']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['filesJ']['error'][$i];
                $_FILES['file']['size'] = $_FILES['filesJ']['size'][$i];

                if ($this->upload->do_upload('file')) {

                    $data = array(
                        'upload_data' => $this->upload->data()
                    );
                    $data_attachment = ['dailyreport_id' => $id_dailyreports, 'title' => $data['upload_data']['file_name'], 'description' => '', 'path' => $config['upload_path'] . $data['upload_data']['file_name'], 'filetype' => $data['upload_data']['file_type'], 'filesize' => $data['upload_data']['file_size'], 'type' => 1, 'name' => $_FILES['filesJ']['name'][$i], 'creator_id' => $this->user_id ];
                    $this->m_mis->insert_data($data_attachment, $this->modelDetail1);
                }
                else {
                    echo json_encode(array(
                        'status' => 'false',
                        'message' => $this->upload->display_errors()
                    ));
                }
            }
        }


    }
	
    public function view_received($view = false, $search = array()) {
        $data = array();
		$data_ = array();

        $position = isset($search['position']) ? $search['position'] : "";
        $search = isset($search['search']) ? $search['search'] : "";

        $id_dailyreports = $this->input->post("id");
		
		$rowperpage = 10;
		$rowno = $this->input->post('rowno')<>null?$this->input->post('rowno'):0;
		$view = $this->input->post('view')<>null?false:$view;
		$search = $this->input->post('search')<>null?$this->input->post('search'):$search;
		
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
        }
		$args = [];
		$args['limit'] = $rowperpage;
		$args['offset'] = $rowno;
		
		$allcount = $this->m_mis->total_data_received($this->user_id, $id_dailyreports, $position, $search, $args)->num_rows();		
        $data_receiver = $this->m_mis->get_data_received($this->user_id, $id_dailyreports, $position, $search, $args)->result();

        if (!empty($data_receiver)) {
			$i=0;
            foreach ($data_receiver as $key => $value) {
                $data_[$i]['id'] = $value->id;
                $data_[$i]['from'] = $value->call_name;
                $data_[$i]['from_id'] = $value->from_id;
                $data_[$i]['position'] = $value->name_position;
                $data_[$i]['branch'] = $value->branch_name;
                $data_[$i]['photo_profile'] = $value->image;
                $data_[$i]['sent_to'] = $this->session->userdata('call_name');
                //$date = date('d-M-y', strtotime($value->date));
				
				$date = shortdate_indo($value->date);
				
                $data_[$i]['date'] = $date;
                $data_[$i]['subject'] = $value->title;
                $data_[$i]['absensi_des'] = ($value->absensi_des == '1') ? "Lancar" : $value->absensi_des;
                $data_[$i]['reg_des'] = ($value->reg_des == '2') ? "Lancar" : $value->reg_des;
                $data_[$i]['sampling_des'] = ($value->sampling_des == '3') ? "Lancar" : $value->sampling_des;
                $data_[$i]['qc_des'] = ($value->qc_des == '4') ? "Lancar" : $value->qc_des;
                $data_[$i]['alat_des'] = ($value->alat_des == '5') ? "Lancar" : $value->alat_des;
                $data_[$i]['janji_des'] = ($value->janji_des == '6') ? "Lancar" : $value->janji_des;
                $data_[$i]['lain_des'] = ($value->lain_des == '7') ? "Lancar" : $value->lain_des;
                $data_[$i]['pelayanan_des'] = ($value->pelayanan_des == '8') ? "Lancar" : $value->pelayanan_des;
                $data_[$i]['kategori'] = $value->kategori;
                $data_[$i]['status'] = $value->status;
                $data_[$i]['count_send'] = $this->m_mis->get_sent_count($value->id);
                $data_[$i]['count_read'] = $this->m_mis->get_read_count($value->id);
                $data_[$i]['read'] = $value->read;
                $data_[$i]['file_attachment'] = $this->m_mis->edit_data(array(
                    'dailyreport_id' => $value->id
                ) , 'dailyreports_attachment')->result();
				$i++;
            }
			
			$this->load->helper('url');
			$this->load->library('pagination');
			
			$config['base_url'] = base_url().'admin/dailyreports/view_received';
			$config['use_page_numbers'] = TRUE;
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			$config['total_rows'] = $allcount;
			$config['per_page'] = $rowperpage;
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$data['result'] = $data_;
			$data['row'] = $rowno;
		}

        if ($view) {
            return $data;
        }
        else {

            echo json_encode($data);
        }
    }

    public function view_send($view = false, $search = array()) {
        $data = array();
		$data_ = array();

        $position = isset($search['position']) ? $search['position'] : "";
        $search = isset($search['search']) ? $search['search'] : "";

        $id_dailyreports = $this->input->post("id");
		
		$rowperpage = 10;
		$rowno = $this->input->post('rowno')<>null?$this->input->post('rowno'):0;
		$view = $this->input->post('view')<>null?false:$view;
		$search = $this->input->post('search')<>null?$this->input->post('search'):$search;
		
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
        }
		$args = [];
		$args['limit'] = $rowperpage;
		$args['offset'] = $rowno;
		
		$allcount = $this->m_mis->total_data_sent($this->user_id, $id_dailyreports, $position, $search, $args)->num_rows();		
        $data_send = $this->m_mis->get_data_sent($this->user_id, $id_dailyreports, $position, $search, $args)->result();

        if (!empty($data_send)) {
			$i=0;  
			foreach ($data_send as $key => $value) {
                $data_[$i]['id'] = $value->id;
                $data_[$i]['from'] = $this->session->userdata('call_name');
                $data_[$i]['position'] = $value->name_position;
                $data_[$i]['branch'] = $value->branch_name;
                $data_[$i]['photo_profile'] = $value->image;
                
				$data_all_send = $this->m_mis->get_all_sent_to($value->id)->result();
				if (!empty($data_all_send)){
					$j=0;           
					foreach($data_all_send as $v){
						$data_[$i]['sent_to'][$j] = $v;
						$j++;
					}
				}
				//$data_[$i]['sent_to'][$key] = $this->m_mis->get_sent_to($value->id_receiver)->row();
				//$date = date('d-M-y', strtotime($value->date));
				//$date = date('d-M', strtotime($value->date) );
                $date = shortdate_indo($value->date);
                $data_[$i]['date'] = $date;
                $data_[$i]['subject'] = $value->title;
                $data_[$i]['absensi_des'] = ($value->absensi_des == '1') ? "Lancar" : $value->absensi_des;
                $data_[$i]['reg_des'] = ($value->reg_des == '2') ? "Lancar" : $value->reg_des;
                $data_[$i]['sampling_des'] = ($value->sampling_des == '3') ? "Lancar" : $value->sampling_des;
                $data_[$i]['qc_des'] = ($value->qc_des == '4') ? "Lancar" : $value->qc_des;
                $data_[$i]['alat_des'] = ($value->alat_des == '5') ? "Lancar" : $value->alat_des;
                $data_[$i]['janji_des'] = ($value->janji_des == '6') ? "Lancar" : $value->janji_des;
                $data_[$i]['lain_des'] = ($value->lain_des == '7') ? "Lancar" : $value->lain_des;
                $data_[$i]['pelayanan_des'] = ($value->pelayanan_des == '8') ? "Lancar" : $value->pelayanan_des;
                $data_[$i]['kategori'] = $value->kategori;
                $data_[$i]['status'] = $value->status;
                $data_[$i]['count_send'] = $this->m_mis->get_sent_count($value->id);
                $data_[$i]['count_read'] = $this->m_mis->get_read_count($value->id);
                $data_[$i]['file_attachment'] = $this->m_mis->edit_data(array(
                    'dailyreport_id' => $value->id
                ) , 'dailyreports_attachment')->result();
				$i++;
				
            }
			
			$this->load->helper('url');
			$this->load->library('pagination');
			
			$config['base_url'] = base_url().'admin/dailyreports/view_send';
			$config['use_page_numbers'] = TRUE;
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			$config['total_rows'] = $allcount;
			$config['per_page'] = $rowperpage;
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$data['result'] = $data_;
			$data['row'] = $rowno;
        }

		//print_r($data['result']);


        if ($view) {
            return $data;
        }
        else {

            if (!$this->input->is_ajax_request()) {
                exit('No direct script access allowed');
            }

            echo json_encode($data);
        }

    }

    public function view_archived($view = false, $search = array()) {
        $data = array();
		$data_ = array();

        $position = isset($search['position']) ? $search['position'] : "";
        $search = isset($search['search']) ? $search['search'] : "";

        $id_dailyreports = $this->input->post("id");
		
		$rowperpage = 10;
		$rowno = $this->input->post('rowno')<>null?$this->input->post('rowno'):0;
		$view = $this->input->post('view')<>null?false:$view;
		$search = $this->input->post('search')<>null?$this->input->post('search'):$search;
		
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
        }
		$args = [];
		$args['limit'] = $rowperpage;
		$args['offset'] = $rowno;
		
		$allcount = $this->m_mis->total_data_archived($this->user_id, $id_dailyreports, $position, $search, $args)->num_rows();	
        $data_send = $this->m_mis->get_data_archived($this->user_id, $id_dailyreports, $position, $search, $args)->result();

        if (!empty($data_send)) {
			$i=0;  
			foreach ($data_send as $key => $value) {
                $data_[$i]['id'] = $value->id;
                $data_[$i]['from'] = $value->call_name;
                $data_[$i]['position'] = $value->name_position;
                $data_[$i]['branch'] = $value->branch_name;
                $data_[$i]['photo_profile'] = $value->image;
				
				$data_all_send = $this->m_mis->get_all_sent_to($value->id)->result();
				if (!empty($data_all_send)){
					$j=0;           
					foreach($data_all_send as $v){
						$data_[$i]['sent_to'][$j] = $v;
						$j++;
					}
				}
				
				
                //$data_[$i]['sent_to'][$key] = $this->m_mis->get_sent_to($value->id_receiver)->row();
                //$date = date('d-M-y', strtotime($value->date));
				//$date = date('d-M', strtotime($value->date) );
                $date = shortdate_indo($value->date);
                $data_[$i]['date'] = $date;
                $data_[$i]['subject'] = $value->title;
                $data_[$i]['absensi_des'] = ($value->absensi_des == '1') ? "Lancar" : $value->absensi_des;
                $data_[$i]['reg_des'] = ($value->reg_des == '2') ? "Lancar" : $value->reg_des;
                $data_[$i]['sampling_des'] = ($value->sampling_des == '3') ? "Lancar" : $value->sampling_des;
                $data_[$i]['qc_des'] = ($value->qc_des == '4') ? "Lancar" : $value->qc_des;
                $data_[$i]['alat_des'] = ($value->alat_des == '5') ? "Lancar" : $value->alat_des;
                $data_[$i]['janji_des'] = ($value->janji_des == '6') ? "Lancar" : $value->janji_des;
                $data_[$i]['lain_des'] = ($value->lain_des == '7') ? "Lancar" : $value->lain_des;
                $data_[$i]['pelayanan_des'] = ($value->pelayanan_des == '8') ? "Lancar" : $value->pelayanan_des;
                $data_[$i]['kategori'] = $value->kategori;
                $data_[$i]['status'] = $value->status;
                $data_[$i]['count_send'] = $this->m_mis->get_sent_count($value->id);
                $data_[$i]['count_read'] = $this->m_mis->get_read_count($value->id);
                $data_[$i]['file_attachment'] = $this->m_mis->edit_data(array(
                    'dailyreport_id' => $value->id
                ) , 'dailyreports_attachment')->result();
				$i++;
            }
			
			$this->load->helper('url');
			$this->load->library('pagination');
			
			$config['base_url'] = base_url().'admin/dailyreports/view_archived';
			$config['use_page_numbers'] = TRUE;
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			$config['total_rows'] = $allcount;
			$config['per_page'] = $rowperpage;
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$data['result'] = $data_;
			$data['row'] = $rowno;
		}

        if ($view) {
            return $data;
        }
        else {

            if (!$this->input->is_ajax_request()) {
                exit('No direct script access allowed');
            }
            echo json_encode($data);
        }
    }

    public function view_draft($view = false, $search = array()) {
        $data = array();
		$data_ = array();
		
        $position = isset($search['position']) ? $search['position'] : "";
        $search = isset($search['search']) ? $search['search'] : "";

        $id_dailyreports = $this->input->post("id");
		
		
		$rowperpage = 10;
		$rowno = $this->input->post('rowno')<>null?$this->input->post('rowno'):0;
		$view = $this->input->post('view')<>null?false:$view;
		$search = $this->input->post('search')<>null?$this->input->post('search'):$search;
		
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
        }
		$args = [];
		$args['limit'] = $rowperpage;
		$args['offset'] = $rowno;
		
		$allcount = $this->m_mis->total_data_draft($this->user_id, $id_dailyreports, $position, $search, $args)->num_rows();	
        $data_send = $this->m_mis->get_data_draft($this->user_id, $id_dailyreports, $position, $search, $args);

        if (!empty($data_send)) {
			$i=0;
			foreach ($data_send as $key => $value) {
                $data_[$i]['id'] = $value->id;
                $data_[$i]['from'] = $this->session->userdata('call_name');
                $data_[$i]['position'] = $value->name_position;
                $data_[$i]['branch'] = $value->branch_name;
                $data_[$i]['photo_profile'] = $value->image;
                
				$data_all_send = $this->m_mis->get_all_sent_to($value->id)->result();
				if (!empty($data_all_send)){
					$j=0;           
					foreach($data_all_send as $v){
						$data_[$i]['sent_to'][$j] = $v;
						$j++;
					}
				}
				
				//$data_[$i]['sent_to'][$key] = !empty($this->m_mis->get_sent_to($value->id_receiver)->row()) ? $this->m_mis->get_sent_to($value->id_receiver)->row() : "";
                //$date = date('d-M-y', strtotime($value->date));
				//$date = date('d-M', strtotime($value->date) );
                $date = shortdate_indo($value->date);
                $data_[$i]['date'] = $date;
                $data_[$i]['subject'] = $value->title;
                $data_[$i]['absensi_des'] = ($value->absensi_des == '1') ? "Lancar" : $value->absensi_des;
                $data_[$i]['reg_des'] = ($value->reg_des == '2') ? "Lancar" : $value->reg_des;
                $data_[$i]['sampling_des'] = ($value->sampling_des == '3') ? "Lancar" : $value->sampling_des;
                $data_[$i]['qc_des'] = ($value->qc_des == '4') ? "Lancar" : $value->qc_des;
                $data_[$i]['alat_des'] = ($value->alat_des == '5') ? "Lancar" : $value->alat_des;
                $data_[$i]['janji_des'] = ($value->janji_des == '6') ? "Lancar" : $value->janji_des;
                $data_[$i]['lain_des'] = ($value->lain_des == '7') ? "Lancar" : $value->lain_des;
                $data_[$i]['pelayanan_des'] = ($value->pelayanan_des == '8') ? "Lancar" : $value->pelayanan_des;
                $data_[$i]['kategori'] = $value->kategori;
                $data_[$i]['status'] = $value->status;
                $data_[$i]['jam_buka'] = $value->jam_buka;
                $data_[$i]['count_send'] = $this->m_mis->get_sent_count($value->id);
                $data_[$i]['count_read'] = $this->m_mis->get_read_count($value->id);
                $data_[$i]['file_attachment'] = $this->m_mis->edit_data(array(
                    'dailyreport_id' => $value->id
                ) , 'dailyreports_attachment')->result();
				$i++;
            }

            // stay
            //  view jam buka

			
			$this->load->helper('url');
			$this->load->library('pagination');
			
			$config['base_url'] = base_url().'admin/dailyreports/view_draft';
			$config['use_page_numbers'] = TRUE;
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			$config['total_rows'] = $allcount;
			$config['per_page'] = $rowperpage;
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$data['result'] = $data_;
			$data['row'] = $rowno;
        }

        if ($view) {
            return $data;
        }
        else {
            if (!$this->input->is_ajax_request()) {
                exit('No direct script access allowed');
            }

            echo json_encode($data);
        }
    }

    public function delete_sent_draft() {
        $id = $this->input->post('id');
        $param = $this->input->post('param');
        if ($param == 'draft') {
            $data['id_draft'] = 0; // hilangkan dari draft list
        }else{
            $data['is_view'] = 2; // hilangkan dari sent list
        }

        $table = $this->model;

        foreach ($id as $value) {
            $where = array(
                'id' => $value
            );
            $this->m_mis->update_data($where, $data, $table);
        }

        echo json_encode(array(
            "status" => true,
            'message' => 'Data Berhasil dihapus'
        ));
    }

    public function delete_data_replay() {
        $id = $this->input->post('id');

        $table = $this->modelDetail2;
		$where = array(
			'id' => $id
		);
		$id_user = $this->user_id;
		$this->load->model('restapi/dailyreport_model');
		echo json_encode($this->dailyreport_model->hapus_reply($id,$id_user));
    }

    public function delete_received() {
        $id = $this->input->post('id');
        $data['is_closed'] = 1;
        $table = $this->modelDetail;

        foreach ($id as $value) {
            $where = array(
                'id_dailyreports' => $value,
                'id_receiver' => $this->user_id
            );
            $this->m_mis->update_data($where, $data, $table);
        }

        echo json_encode(array(
            "status" => true,
            'message' => 'Data Berhasil dihapus'
        ));
    }

    public function set_user_read() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $id = $this->input->post('id');
        $data = array(
            "is_view" => 1,
        );

        $this->m_mis->update_data(array(
            'id_dailyreports' => $id,
            'id_receiver' => $this->user_id
        ) , $data, $this->modelDetail);

        // $this->m_mis->update_data(array('data LIKE '=>'%'.$id.'%','to_id'=>$this->user_id), array('read'=>1), $this->modelDetail3);
        /* tandai notifikasi sudah dibaca untuk dailyreport ini */
        $this->load->library("Notify_lib");
        $nl = new Notify_lib();
        $nl->mark_read($this->user_id, "dailyreport", $id);

        if ($this->check_dailyreport_lancar($id)) {

            $where = array(
                "id_dailyreports" => $id,
            );

            $count_received = $this->m_mis->edit_data($where, $this->modelDetail)->num_rows();

            $where_view = array(
                "is_view" => 1,
                "id_dailyreports" => $id,
            );

            $count_read = $this->m_mis->edit_data($where_view, $this->modelDetail)->num_rows();

            $is_closed = $count_received - $count_read;

            if ($is_closed == 0) {
                $this->m_mis->update_data(array(
                    'id' => $id
                ) , array(
                    'status' => 'Close'
                ) , $this->model);
            }
        }

        echo json_encode(array(
            "status" => true
        ));
    }

    public function get_user_read() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data = array();

        $id = $this->input->post('id');
        $user_read = $this->m_mis->get_user_read($id)->result();
        if (!empty($user_read)) {
            foreach ($user_read as $key => $value) {
                $data[$key]['call_name'] = $value->call_name;
                $data[$key]['name_position'] = $value->name_position;
                $data[$key]['id_receiver'] = $value->id_receiver;
                $data[$key]['is_view'] = $value->is_view;
            }
        }

        echo json_encode($data);

    }

    public function replay() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
		$id_dailyreports = $this->input->post('id_dailyreports_replay');
		$reply_id = $this->input->post('id_replay');
		$id_user = $this->user_id;
		$message_reply = htmlentities($this->input->post("message_reply"));
		
		if($reply_id!=''){
			$data = array(
				"user_id" => $id_user,
				"id" => $id_dailyreports,
				"reply_id" => $reply_id,
				"message" => $message_reply,
			);

			$this->load->model('restapi/dailyreport_model');
			$data = $this->dailyreport_model->reply($data);
			$data['id'] = $id_dailyreports;
			echo json_encode($data);
		}
		else{
			

			
			
			// $message_reply = $this->input->post("message_reply");
			$data = array(
				"id_dailyreports" => $id_dailyreports,
				"sender" => $id_user,
				"message_reply" => $message_reply,
				"date_added" => date("Y-m-d H:i:s"),
			);

			$this->load->model('restapi/dailyreport_model');
			$data = $this->dailyreport_model->reply($data);
			$data['id'] = $id_dailyreports;
			echo json_encode($data);
			
			// $saved = $this->m_mis->insert_data($data, $this->modelDetail2);
			// $this->db->where(['id' => $id_dailyreports])->update($this->model, ['is_view' => 0, 'last_update' => date('Y-m-d H:i:s') ]);

			// $data_receiver = array();
			// $model = $this->m_mis->get_to_id($id_dailyreports, $id_user)->row();
			// if (!empty($model)) {
			//     $data_receiver['to_id'] = $model->from_id;
			//     $data_receiver['id_dailyreports'] = $model->id_dailyreports;

			//     $this->set_notification($data_receiver, $replay = true);
			// }
			// else {
			//     $model = $this->m_mis->get_from_id($id_dailyreports)->result();
			//     $list_receiver = [];
			//     foreach ($model as $key => $value) {
			//         $data_receiver['to_id'] = $value->id_receiver;
			//         $data_receiver['id_dailyreports'] = $value->id_dailyreports;
			//         $list_receiver[] = $value->id_receiver;
			//     }

			//     $data_receiver['list_receiver'] = $list_receiver;

			//     $this->set_notification($data_receiver, $replay = true);
			// }

			/*echo json_encode(array(
				"status" => true,
				'message' => 'Reply Berhasil dikirim',
				// 'id' => $data_receiver['id_dailyreports']
			));*/
		}
    }

    public function get_user_replay() {
        

        $data = array();

        $id_dailyreports = $this->input->post("id");
        $data_send = $this->m_mis->get_user_replay($id_dailyreports)->result();

        if (!empty($data_send)) {
            foreach ($data_send as $key => $value) {
                $data[$key]['call_name'] = $value->call_name;
                $data[$key]['branch_name'] = $value->branch_name;
                $data[$key]['name_position'] = $value->name_position;
                $data[$key]['message_reply'] = $value->message_reply;
                $date = date('d-M-y H:i:s', strtotime($value->date_added));
                $data[$key]['datedate_added'] = $date;
                $data[$key]['photo_profile'] = $value->image;
				
				$data[$key]['is_edit'] = ($value->sender==$this->session->userdata('id_user')?1:0);
				$data[$key]['id'] = $value->id;

            }
        }

        echo json_encode($data);

    }

    public function get_notification() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data = array();
        $data['count'] = "";

        $data_notification = $this->m_mis->get_notification($this->user_id)->result();
        if (!empty($data_notification)) {
            foreach ($data_notification as $key => $value) {
                $data[$key]['id'] = $value->id;
                $data[$key]['from_id'] = $value->from_id;
				$data[$key]['photo_profile'] = $value->photo_profile;
				$data[$key]['creator_name'] = $value->creator_name;
				$data[$key]['read'] = $value->read;										 
                $data[$key]['to_id'] = $value->to_id;
                $data[$key]['description'] = $value->description;
                $data[$key]['data'] = json_decode($value->data);
                // $data[$key]['dailyreports_id'] = json_decode($value->data)->dailyreports_id;
                // $data[$key]['data'] = json_decode($value->data);
                $data[$key]['data'] = null;
                $data[$key]['dailyreports_id'] = "";
                if (isset($value->data)) {
                    $data_ = json_decode($value->data);
                    $data[$key]['data'] = $data_;
                    if (isset($data_->dailyreports_id)) {
                        $data[$key]['dailyreports_id'] = $data_->dailyreports_id;
                    }
                }
                // $data[$key]['dailyreports_id'] = json_decode($value->data)->dailyreports_id;
                $time = date('d-M-y H:i', strtotime($value->time));
                $data[$key]['time'] = $time;
            }
            $data['count'] = $this->m_mis->get_notification($this->user_id)->num_rows();
			$data['noread'] = $this->m_mis->no_read_notification($this->user_id)->num_rows();																		

        }

        echo json_encode($data);
    }

    public function get_notificationAdd() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data = array();
        $data['count'] = "";

        $data_notification = $this->m_mis->get_notificationAdd($this->user_id)->result();
        if (!empty($data_notification)) {
            foreach ($data_notification as $key => $value) {
                $data[$key]['id'] = $value->id;
                $data[$key]['from_id'] = $value->from_id;
				$data[$key]['photo_profile'] = $value->photo_profile;
				$data[$key]['creator_name'] = $value->creator_name;
				$data[$key]['read'] = $value->read;										 
                $data[$key]['to_id'] = $value->to_id;
                $data[$key]['description'] = $value->description;
                $data[$key]['data'] = json_decode($value->data);
                // $data[$key]['dailyreports_id'] = json_decode($value->data)->dailyreports_id;
                // $data[$key]['data'] = json_decode($value->data);
                $data[$key]['data'] = null;
                $data[$key]['dailyreports_id'] = "";
                if (isset($value->data)) {
                    $data_ = json_decode($value->data);
                    $data[$key]['data'] = $data_;
                    if (isset($data_->dailyreports_id)) {
                        $data[$key]['dailyreports_id'] = $data_->dailyreports_id;
                    }
                }
                // $data[$key]['dailyreports_id'] = json_decode($value->data)->dailyreports_id;
                $time = date('d-M-y H:i', strtotime($value->time));
                $data[$key]['time'] = $time;
            }
            $data['count'] = $this->m_mis->get_notificationAdd($this->user_id)->num_rows();
			$data['noread'] = $this->m_mis->no_read_notificationAdd($this->user_id)->num_rows();																		

        }

        echo json_encode($data);
    }
    
	public function read_all_notif() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        //$id = $this->input->post('id');
        $data = array(
            "read" => 1,
        );

        $this->m_mis->update_data(array(
            "data not like '%doc_id%'" => null,
			"data not like '%id_announcement%'" => null,
			"cuti_flag" => 0,
            'to_id' => $this->user_id
        ) , $data, $this->modelDetail3);

        $this->m_mis->update_data(array(
            "data not like '%doc_id%'" => null,
			"data not like '%id_announcement%'" => null,
			"cuti_flag" => 0,
            'to_id' => $this->user_id
        ) , $data, "admin_mis.notifications");


        echo json_encode(array(
            "status" => true
        ));
    }							   

    public function set_notification($data_receiver, $replay = false, $other = null) {
        $idReceivers = [];
        $title = "New Daily Report";
        if ($replay) {
            $title = 'Daily Report Reply';
            $from_id = $this->user_id;
            $idReceivers[] = $to_id = $data_receiver['to_id'];
            $id_dailyreport = $dat['dailyreports_id'] = $data_receiver['id_dailyreports'];
            $decription = $this->session->userdata('call_name') . ' - '.$this->session->userdata('position_name').' ('.$this->session->userdata('branch_name').') has sent a new reply Daily Report';

            /* beri notif juga kepada tujuan dailyreport ini */
            $this->load->model("restapi/dailyreport_model", "m_dailyreport");
            $listReceiver = $this->m_dailyreport->getReceivers($id_dailyreport);
            foreach ($listReceiver as $key => $rc) {
                /* penerima ini adalah yg sedang membalas, maka skip */
                if ($rc["id_user"] == $this->user_id) {
                    continue;
                }

                $idReceivers[] = $rc["id_user"];
            }
        }
        elseif (!empty($other)) {
            $from_id = $this->user_id;
            $idReceivers[] = $to_id = $data_receiver['id_receiver'];
            $dat['dailyreports_id'] = $data_receiver['id_dailyreports'];
            $decription = $this->session->userdata('call_name') . ' - '.$this->session->userdata('position_name').' ('.$this->session->userdata('branch_name').') '. $other;
        }
        else {
            $from_id = $this->user_id;
            // $idReceivers[] = $to_id = $data_receiver['id_receiver'];
            $dat['dailyreports_id'] = $data_receiver['id_dailyreports'];
            $idReceivers = $data_receiver['receivers'];
            $decription = $this->session->userdata('call_name') . ' - '.$this->session->userdata('position_name').' ('.$this->session->userdata('branch_name').') has sent a new Daily Report';
        }

        // $data = array(
        //     'from_id' => $from_id,
        //     'to_id' => $to_id,
        //     'description' => $decription,
        //     'data' => json_encode($dat),
        //     'read' => '0',
        //     'time' => date('Y-m-d H:i:s'),
        // );
        // $this->m_mis->insert_data($data, $this->modelDetail3);
        $this->load->library("Notify_lib");
        $nl = new Notify_lib();
        $nl->send($title, //title
        $decription, //message
        $from_id, //sender
        $idReceivers, //list receiver
        ["id" => $data_receiver["id_dailyreports"]], //attachment
        "dailyreport", //type notifikasi
        true
        //kirim ke onesignal
        );
    }

    public function update_notification() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $id = $this->input->post("id");

        $data['read'] = 1;

        $this->m_mis->update_data(array(
            'id' => $id
        ) , $data, $this->modelDetail3);

        $this->m_mis->update_data(array(
            'id' => $id
        ) , $data, "admin_mis.notifications");

        echo json_encode(array(
            "status" => true
        ));
    }

    public function check_dailyreport_lancar($id) {
        $data = false;

        $where = array(
            'id' => $id,
            'absensi_des' => '1',
            'reg_des' => '2',
            'sampling_des' => '3',
            'qc_des' => '4',
            'alat_des' => '5',
            'janji_des' => '6',
            'lain_des' => '7',
            'pelayanan_des' => '8'

        );
        $table = $this->model;

        $check = $this->m_mis->edit_data($where, $table)->row();

        if ($check) {
            $data = true;
        }

        return $data;
    }

//    public function get_data_autocomplete() {
//        if (!$this->input->is_ajax_request()) {
//            exit('No direct script access allowed');
//        }

//        $data = array();
//        $autocomplete = $this->input->get('autocomplete');
//        if ($autocomplete == 'search_user') {
//            $query = $this->input->get('search');
//            $where = $query['name'];
//           $column = 'name';
//            $table = 'users';

//           $datas = $this->m_mis->get_data_like($where, $column, $table)->result();
//            if (count($datas) > 0) {
//                foreach ($datas as $key => $obj) {
//                    $data[$key]['id'] = $obj->id_user;
//                    $data[$key]['value'] = $obj->name;
//                    $data[$key]['label'] = $obj->name;
//                }
//            }
//            echo json_encode($data);
//        }
//    }

 //   function detail_modal() {
 //       $id = $this->input->get_post("id");
 //       $this->load->model("restapi/dailyreport_model");
 //       $data = $this->dailyreport_model->detail($id);
 //       $this->load->view("admin/v_dailyreport_detail_modal", ["data" => $data]);
 //   }

    function detail(){
        $id = $this->input->get_post('id');
        $this->load->model('restapi/dailyreport_model');
        $report = $this->dailyreport_model->detail($id);
        if($report != null) {
            $report->show_absensi = false;
			
			
            $report->show_support = true;
            $report->show_daily = true;
            // if($report->from_id == $this->user_id) {
            //     $report->show_absensi = true;
            //     $report->show_support = true;
            //     $report->show_daily = true;
            // }
            // else {
            //     $report->show_daily = false;
            //     $report->show_absensi = false;
            //     $report->show_support = false;

            if(in_array($this->id_bagian, [22]) ) {
                $report->show_absensi = true;
                $report->show_support = false;
                $report->show_daily = false;
            }
            if(in_array($this->user_id, [48])) {
                $report->show_absensi = true;
                $report->show_support = true;
                $report->show_daily = true;
            }
            //     if(in_array($this->id_bagian, [19])) {
            //         $report->show_support = true;
            //     }
            //     /** tampilkan field daily report untuk branch manager, direktur utama, dan ass. general manager */
            //     if(in_array($this->id_position, [7, 1, 20])) {
            //         $report->show_daily = true;
            //     }
            // }
        }
        if(in_array($this->user_id, [62,30,55,37,57,70])) {
            $report->show_absensi = true;
            $report->show_support = true;
            $report->show_daily = true;
        }

        if(in_array($this->id_position, [7]) ) {
            $report->show_absensi = true;
            $report->show_support = true;
            $report->show_daily = true;
        }

		
		if($report->from_id == $this->session->userdata('id_user') || strtoupper($report->status)=="CLOSE")
			$report->show_absensi = true;
		
        echo json_encode($report);
    }

    function update_status() {
        $id_dailyreport = $this->input->post('id_dailyreport');
        $status = $this->input->post("status");
        $status = strtoupper($status);
        if (in_array($status, ['Open', 'Close']) == false) {
            $status = 'Close';
        }

        $target = $this->db->where(["id" => $id_dailyreport])->get($this->model)->row();
        if ($target == null) {
            echo json_encode(['status' => false, 'message' => 'data tidak ditemukan']);
            exit();
        }

        $this->db->where(["id" => $id_dailyreport])->update($this->model, ['status' => $status]);

        echo json_encode(['status' => true]);
    }

    public function export_excel()
    {
        $this->load->model('restapi/user_model');
        $this->load->model('');

        $this->form_validation->set_rules('branch', 'Branch', 'required');
        $this->form_validation->set_rules('start_date', 'Start Date', 'required');
        $this->form_validation->set_rules('end_date', 'End Date', 'required');

        $branch = $this->input->post('branch');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $idPositions = '7';
        $user_kepala_cabang = $this->user_model->many_user([
            'where_in' => [
                'b1.branch_id' => $branch,   /* user yg berada di dep opt */
                'pos.id_position' => $idPositions /* position branch manager */
            ],
            'where' => ['_.STATUS !=' => 'KELUAR']
        ]);

        $ids = [];
        foreach ($user_kepala_cabang as $key => $user) {
            $ids[] = $user['id_user'];
        }

        if (empty($ids) == false) {
            /* get report yg telah dibuat hari ini oleh user kepala cabang */
            $this->load->model('restapi/dailyreport_model');
            $dr = $this->dailyreport_model->many([
                'where_in' => [
                    '_.from_id' => $ids
                ],
                'where' => [
                    'date >=' => $start_date,
                    'date <=' => $end_date
                ]
            ]);

            $ids2 = $ids;
            $date = [];
            /* exclude kepala cabang yg telah membuat report */
            while ((strtotime($start_date) <= strtotime($end_date))) {
                $start_date = date("Y-m-d", strtotime($start_date));
                $date[] = $start_date;
                $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
            }

            foreach ($dr as $key => $report) {
                if (in_array($report->date, $date) == 1) {
                    unset($date[array_search($report->date, $date)]);
                }
            }
            /* id user di dalam ids2 belum membuat daily report hari ini */
            /* get user yg belum membuat report */

            $tanggal_belum_report = [];
            foreach ($date as $d) {
                foreach ($user_kepala_cabang as $key => $user) {
                    if (in_array($user['id_user'], $ids2)) {
                        $tanggal_belum_report[] = array_merge($user, ["date" => $d]);
                    }
                }
            }
            $data['belum_reports'] = $tanggal_belum_report;
        }


        $this->load->view('admin/dailyreports/v_dailyreport_export_excel', $data);
    }


}
