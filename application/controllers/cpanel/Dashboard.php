<?php
defined('BASEPATH') Or exit('No Direct Script Access Allowed');


/**
*
*/
class Dashboard extends CI_Controller{

	function __construct(){
	parent::__construct();
		//cek login
	if ($this->session->userdata('status') != "login" ){

		redirect(base_url().'welcome?pesan=belumlogin');
	}
 }

 function index(){

 	$this->load->view('cpanel/inc/main');
 	$this->load->view('cpanel/v_dashboard');
	$this->load->view('cpanel/inc/sidebar');
 	}

 	function logout(){
 		$this->session->sess_destroy();
 		redirect(base_url().'welcome?pesan=logout');
 	}


  }
