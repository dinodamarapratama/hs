<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //cek login
        if ($this->session->userdata('status') != "login") {

            redirect(base_url() . 'welcome?pesan=belumlogin');
        }
        $this->load->library('upload');
        $this->user_id = $this->session->userdata('id_user');
    }

    public function index()
    {

        $data['users'] = $this->db->query("SELECT * from users inner join position on users.id_position=position.id_position")->result();
        $data['position'] = $this->m_mis->get_data('position')->result();
        $this->load->view('cpanel/inc/main');
        $this->load->view('cpanel/v_user',$data);
        $this->load->view('cpanel/inc/sidebar');

    }


    function add()
    {

        $image = $this->input->post('image');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('id_position', 'Position', 'required');
        $this->form_validation->set_rules('gender', 'Gender', 'required');
        $this->form_validation->set_rules('dob', 'DOB', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('contact', 'Contact', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('joining_date', 'Joining Date', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('level', 'Level', 'required');
        if($this->form_validation->run()==FALSE){
            $this->session->set_flashdata('error',"Data Gagal Di Tambahkan");
            redirect('cpanel/user');
        }else{

           //konfigurasi upload gambar
            $config['upload_path'] = './uploads/avatar/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = '2048';
            $config['file_name'] = 'image'.time();
            $this->upload->initialize($config);
            if($this->upload->do_upload('gambar')){
            $image=$this->upload->data();
            $data=array(

                'name'=>$_POST['name'],
                'username'=>$_POST['username'],
                'password'=>sha1($_POST['password']),
                'id_position'=>$_POST['id_position'],
                'image' => $image['file_name'],
                'gender'=>$_POST['gender'],
                'dob'=>$_POST['dob'],
                'email'=>$_POST['email'],
                'contact'=>$_POST['contact'],
                'address'=>$_POST['address'],
                'joining_date'=>$_POST['joining_date'],
                'status'=>$_POST['status'],
                'level'=>$_POST['level']
            );
        }
            $this->m_mis->insert_data($data,'users');
            $this->session->set_flashdata('sukses',"Data Berhasil Disimpan");
            redirect('cpanel/user');

        }
    }



    function edit(){

        $data['position'] = $this->m_mis->get_data('position')->result();
        $data['users'] = $this->m_mis->get_data('users')->result();
        $id = $this->input->post('id_user');
        $old_pict =  $this->input->post('old_pict');
        $this->form_validation->set_rules('id_user', 'ID user', 'required');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('id_position', 'Position', 'required');
        $this->form_validation->set_rules('gender', 'Gender', 'required');
        $this->form_validation->set_rules('dob', 'DOB', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('contact', 'Contact', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('joining_date', 'Joining Date', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('level', 'Level', 'required');
            $config['upload_path'] = './uploads/avatar/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = '2048';
            $config['file_name'] = 'image'.time();
            $this->upload->initialize($config);
            $data = array(
             'name'=>$_POST['name'],
                'username'=>$_POST['username'],
                'password'=>sha1($_POST['password']),
                'id_position'=>$_POST['id_position'],
                'gender'=>$_POST['gender'],
                'dob'=>$_POST['dob'],
                'email'=>$_POST['email'],
                'contact'=>$_POST['contact'],
                'address'=>$_POST['address'],
                'joining_date'=>$_POST['joining_date'],
                'status'=>$_POST['status'],
                'level'=>$_POST['level']
                
            );

            @unlink('./assets/upload/',$old_pict);

            if($this->upload->do_upload('gambar')){
                //proses upload gambar
                $image = $this->upload->data();
                unlink('./uploads/avatar/'.$this->input->post('old_pict',TRUE));
                $data['image'] = $image['file_name'];
                $this->db->where('id_user', $_POST['id_user']);
            }else{
                $this->db->where('id_user', $_POST['id_user']);
            } 

            $this->db->update('users',$data);
            $this->session->set_flashdata('sukses',"Data Berhasil Di ubah");
            redirect(base_url().'cpanel/user');
        }
    }


     function delete($id)

    {

        if($id==""){

            $this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");

            redirect('cpanel/user');

        }else{

            $this->db->where('id_user', $id);

            $this->db->delete('users');

            $this->session->set_flashdata('sukses',"Data Berhasil Dihapus");

            redirect('cpanel/user');

        }

    }
