<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_data extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('restapi/user_model', 'muser');
        $this->load->model('m_master_data');

        $this->load->library('rest_client');

        /**/
        $this->validationAuth();
    }


	function data_branch(){

		$start = !empty($this->input->post('start'))?$this->input->post('start'):'';
		$length = !empty($this->input->post('length'))?$this->input->post('length'):'';
		$search = !empty($this->input->post('search'))?$this->input->post('search'):'';

		$args = [
			'order' => [ ['_.branch_name', 'asc'] ]
		];
	    
		if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
			$args['limit'] = [intval($start), intval($length)];
		}
		if(empty($search) == false) {
			$args['like'] = [
				[
					'cols' => ['branch_name'],
					'search' => $search
				]
			];
		}
		
		$r = $this->m_master_data->many_branch($args,['_.branch_id','_.branch_name','_.branch_code']);
		echo json_encode($r);
	}

	function data_department() {

		$start = !empty($this->input->post('start'))?$this->input->post('start'):'';
		$length = !empty($this->input->post('length'))?$this->input->post('length'):'';
		$search = !empty($this->input->post('search'))?$this->input->post('search'):'';

		$args = [
			'order' => [ ['_.name', 'asc'] ]
		];
        
		if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
			$args['limit'] = [intval($start), intval($length)];
		}
		if(empty($search) == false) {
			$args['like'] = [
				[
					'cols' => ['_.name'],
					'search' => $search
				]
			];
		}
		$r = $this->m_master_data->many_department($args,['_.id_department','_.name']);
		echo json_encode($r);
	}

	function data_bagian(){

		$start = !empty($this->input->post('start'))?$this->input->post('start'):'';
		$length = !empty($this->input->post('length'))?$this->input->post('length'):'';
		$search = !empty($this->input->post('search'))?$this->input->post('search'):'';

		$args = [
			'order' => [ ['_.name', 'asc'] ]
		];
        
		if($start != null && strlen($start) > 0 && $length != null && strlen($length) > 0) {
			$args['limit'] = [intval($start), intval($length)];
		}
		if(empty($search) == false) {
			$args['like'] = [
				[
					'cols' => ['_.name'],
					'search' => $search
				]
			];
		}
		$r = $this->m_master_data->many_bagian($args,['_.id','_.id_department','_.name']);
		echo json_encode($r);
	}


	public function validationAuth(){
        /*
            - check if token valid, compare with login users
            - konsep JWT auth yg kita pakai masih yang sederhana (user&pass), belum menggunakan validasi time token
        */
        $payload = $this->rest_client->validateToken();
        $username = $payload->userId;
        $salt = 'Bio Medika';
        $password = $payload->userPass;
        /*cek login here*/

        $login = $this->muser->login_verify($username,$password);
        if ($login != FALSE) {
            return true;
        }else{
            $key = $this->rest_client->responseCode('ATHENTICATION_FAIL');
            $this->rest_client->responseError($key,'Credential login tidak terdaftar!');
        }
    }

}