<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Appdokter extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        // header('Content-Type: application/json');
        $this->load->model('m_appdokter', 'mapp_shift');
        $this->load->model('restapi/user_model', 'muser');
        $this->load->library('rest_client');

        $this->validationAuth();
    }

    public function validationAuth(){
        /*
            - check if token valid, compare with login users
            - konsep JWT auth yg kita pakai masih yang sederhana (user&pass), belum menggunakan validasi time token
        */
        $payload = $this->rest_client->validateToken();
        $username = $payload->userId;
        $salt = 'Bio Medika';
        $password = $payload->userPass;
        /*cek login here*/

        $login = $this->muser->login_verify($username,$password);
        if ($login != FALSE) {
            return true;
        }else{
            $key = $this->rest_client->responseCode('ATHENTICATION_FAIL');
            $this->rest_client->responseError($key,'Credential login tidak terdaftar!');
        }
    }

    function getSlot(){
    	$parseData = $this->input->post();
    	$id_shift_detail = $this->input->post('schedule_id');
    	// $dd=$this->mapp_shift->get_where(['id'=>$id_shift_detail],'appdokter_shift_detail')->result();
        $config = [
          'fields' => ['appdokter_shift_detail.*','appdokter_shift.branch_id','appdokter_shift.id_dokter','appdokter_shift.nm_dktr_dpn','appdokter_shift.nm_dktr_blkg'], /*leave empty if select all field*/ 
          'relation' => [
            'appdokter_shift_detail' => [
              'table' => 'appdokter_shift_detail',
              'key' => 'appdokter_shift.id=appdokter_shift_detail.id_shift'
            ]
          ],
          'where' => [
            'appdokter_shift_detail.id' => $id_shift_detail
          ],
          'master' => 'appdokter_shift'
        ];
        $dd = $this->mapp_shift->get_related_data($config)->result();
    	$rslt = ['code'=>404,'msg'=>'no record'];
    	if (!empty($dd)) {
    		$rslt = ['code'=>200,'msg'=>'success'];
    		$rslt['data'] = json_decode(json_encode($dd),true); 
    	}

    	echo json_encode($rslt);
    }

    function getAvaildokter(){

    }

    public function getAvailSchedule(){
    	/*
    		*param:
    		- dokter 
    		- branch
    		- day/tgl
    	*/
    	$parseData = $this->input->post();
    	$config = [
	      'fields' => ['appdokter_shift_detail.*','appdokter_shift.branch_id','appdokter_shift.id_dokter','appdokter_shift.nm_dktr_dpn','appdokter_shift.nm_dktr_blkg'], /*leave empty if select all field*/ 
	      'relation' => [
	        'appdokter_shift_detail' => [
	          'table' => 'appdokter_shift_detail',
	          'key' => 'appdokter_shift.id=appdokter_shift_detail.id_shift'
	        ],
	        'branch' => [
	          'table' => 'branch b',
	          'key' => 'b.branch_id=appdokter_shift.branch_id'
	        ]
	      ],
	      'where' => [
	        // 'id_dokter' => $parseData['shift_dokter'],
	        'b.branch_id' => $parseData['shift_cabang'],
	        'tanggal' => $parseData['shift_tanggal'],
	      ],
	      'order' => [
	        'field' => 'tanggal',
	        'dir' => 'asc'
	      ],
	      'master' => 'appdokter_shift'
	    ];
	    $r = $this->mapp_shift->get_related_data($config)->result();
	    $rslt = ['code'=>404,'msg'=>'no record'];
    	if (!empty($r)) {
    		$rslt = ['code'=>200,'msg'=>'success'];
    		$rslt['data'] = json_decode(json_encode($r),true); 
    	}

    	echo json_encode($rslt);
    }
}