<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('restapi/user_model', 'muser');
        $this->load->library('rest_client');
    }

    public function getToken(){
        $username = $this->input->post('username');
        $salt = 'Bio Medika';
        $password = sha1($salt . $this->input->post('password'));
        $getToken = $this->rest_client->generateToken($username,$password);

        echo json_encode([
            'token' => 'JWT '.$getToken
        ]);
    }

    public function get_data()
    {
        $this->validationAuth();
        
        /*gunakan schema datatable*/
        $search = $this->input->post('search');
        $order = $this->input->post('order');
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $draw = $this->input->post('draw');
        
        $offset = empty($start) ? 0 : $start;
        $rows = empty($length) ? 10 : $length;

        $records = [];
        $searchable_field = [
            'username' => '_.username', 
            'nama_depan' => '_.name',
            'nama_belakang' => '_.last_name',
            'nip' => '_.NIP',
            'bagian' => 'bg.name',
            'posisi' => 'pos.name_position',
            'department' => 'dep.name'
        ]; /*nip/nik, bagian, jabatan, username*/
        $like = []; /*
            ['cols' => 'nama field', 'search' => 'param'],[]
        */
        $urutkan = [['_.name','ASC']]; /* ['nama field' => 'Direction(ASC/DESC)'] */

        if (isset($search['value']) && !empty($search)) {
            foreach ($searchable_field as $key => $field) {
                $like[1]['cols'][] = $field;
                $like[1]['search'] = $search['value'];       
            }   
        }

        if (!empty($order)) {
            $urutkan = [];
            foreach ($order as $key => $value) {
                $urutkan[] = [
                    $searchable_field[$value['column']],$value['dir']
                ];
            }
        }

        $select_field = [];
        foreach ($searchable_field as $key => $v) {
            $aliasing = $v;
            if ($v == 'bg.name') {
                $aliasing = $v.' as bagian_name';
            }
            if ($v == 'dep.name') {
                $aliasing = $v.' as department_name';
            }
            $select_field[] = $aliasing;
        }

        if (!empty($like)) {
            $users = $this->muser->get_users([
                'like' => $like,
                'order' => $urutkan
            ],$select_field);
            $totalData = count($users);
        }else{
            /*all data user */
            $users = $this->muser->get_users([
                'order' => $urutkan
            ],$select_field);
            $totalData = count($users);
        }

        
        $records['draw'] = $draw;
        $records['recordsTotal'] = $totalData;
        $records['recordsFiltered'] = $totalData;
        $records['data'] = $users;

        echo json_encode($records);
    }

    public function validationAuth(){
        /*
            - check if token valid, compare with login users
            - konsep JWT auth yg kita pakai masih yang sederhana (user&pass), belum menggunakan validasi time token
        */
        $payload = $this->rest_client->validateToken();
        $username = $payload->userId;
        $salt = 'Bio Medika';
        $password = $payload->userPass;
        /*cek login here*/

        $login = $this->muser->login_verify($username,$password);
        if ($login != FALSE) {
            return true;
        }else{
            $key = $this->rest_client->responseCode('ATHENTICATION_FAIL');
            $this->rest_client->responseError($key,'Credential login tidak terdaftar!');
        }
    }

    public function validationLogin(){
        /*digunakan untuk pengecekan login biomedika APP*/
        $payload = $this->rest_client->validateToken();
        $username = $payload->userId;
        $salt = 'Bio Medika';
        $password = $payload->userPass;
        /*cek login here*/

        $login = $this->muser->login_verify1($username,$password);
        if ($login != FALSE) {
            // return true;
            $key = $this->rest_client->responseCode('SUCCESS_RESPONSE');
            $this->rest_client->responseResult($key,[
                'msg' => 'anda berhasil login',
                'data' => $login
            ]);
        }else{
            $key = $this->rest_client->responseCode('ATHENTICATION_FAIL');
            $this->rest_client->responseError($key,'Credential login tidak terdaftar!');
        }
    }
}