<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class Maintanceexternal extends CI_Controller
{

    public $model = 'homeservice';

    public function __construct()
    {
        parent::__construct();
        $this->session->set_userdata('status',"login");
        $this->branch_id = $this->session->userdata('selected_branch');
        if (empty($this->branch_id)) {
            $this->branch_id = $this->session->userdata('branch_id');
			if (empty($this->branch_id)) $this->branch_id = $this->input->get('branch_id');
        }
    }

     

    public function index($id = '')
    {
        $data['model'] = $this->model;
        if (!empty($id)) {
            $this->session->set_userdata('selected_branch', $id);
            $this->branch_id = $id;
        }
      
        $data['jadwal'] = $this->db->query("select * from maintain_ext me join master_qc_alat alat on alat.id_alat = me.id_alat")->result();
        

        $this->load->view('v_maintance_ext', $data);
    }
    
    
}
