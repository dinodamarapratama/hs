<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

/**
 *
 */
class Homeservice extends CI_Controller
{

    public $model = 'homeservice';

    public function __construct()
    {
        parent::__construct();
        $this->session->set_userdata('status',"login");
        $this->branch_id = $this->session->userdata('selected_branch');
        if (empty($this->branch_id)) {
            $this->branch_id = $this->session->userdata('branch_id');
			if (empty($this->branch_id)) $this->branch_id = $this->input->get('branch_id');
        }
    }

     public function get_available_time() {
        $result['status'] = false;
        $result['message'] = 'Data tidak ditemukan.';

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $date = $this->input->post('date');
        $ptgshs_id = $this->input->post('ptgshs_id');


        $result['status'] = true;
        $result['message'] = 'Data ditemukan';
        $edit_data = $this->m_hs->get_available_time($date, $ptgshs_id, $this->branch_id)->result();
        if(!empty($edit_data)){
            $result['data'] = (array) $edit_data;

        }

        echo json_encode($result);
    }
	
    public function get_hs($date='')
    {
       if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $date = empty($date) ? date('Y-m-d') : $date; 
        $data = array();
        $data_hs = $this->m_hs->get_all($date)->result();
        //$data['hs_time'] = $this->m_hs->get_data('hs_time')->result();
		$data['hs_time'] = $this->m_hs->get_branch_hs_time($this->branch_id)->result();
        $data['hs_initial_petugas_hs'] = $this->m_hs->get_data_hs_initial_petugas_hs($this->branch_id, $date)->result();

        if (!empty($data_hs)) {
            $i = 0;
            foreach ($data_hs as $key => $value) {
                $data['data'][$i]['id'] = $value->id;
                $data['data'][$i]['no'] = $i+1;
                $data['data'][$i]['date'] = $value->date;
                $data['data'][$i]['pid'] = $value->pid;
                $time = substr($value->time_name, 0,8);
                $data['data'][$i]['time_name'] = $time;
                $data['data'][$i]['petugas_id'] = $value->petugas_id;
                $data['data'][$i]['time_id'] = $value->time_id;
                $data['data'][$i]['ptgshs_id'] = $value->ptgshs_id;
                $data['data'][$i]['nama'] = $value->namacaps;
                $data['data'][$i]['alamat'] = $value->alamat;
                $data['data'][$i]['no_telp'] = $value->no_telp;
                $data['data'][$i]['dokter'] = $value->dokter;
                $data['data'][$i]['jumlah_pasien'] = $value->jumlah_pasien;
                $data['data'][$i]['pemeriksaan'] = $value->pemeriksaan;
                $data['data'][$i]['pay_name'] = $value->pay_name;
                $data['data'][$i]['nama_petugas'] = $value->nama_petugas;
                $data['data'][$i]['nama_petugas_hs'] = $value->nama_petugas_hs;
                $data['data'][$i]['ttd_pasien'] = $value->ttd_pasien;
                $data['data'][$i]['branch'] = $value->branch_name;
                $data['data'][$i]['status'] = $value->status;
                $data['data'][$i]['reason'] = $value->reason;
				$data['data'][$i]['catatan'] = $value->catatan;
                $data['data'][$i]['time_selesai'] = $value->time_selesai;
                $data['data'][$i]['creator'] = $value->name_creator;
                $data['data'][$i]['created_date'] = $value->created_date;
                
                $i++;
            }
        }
        echo json_encode($data);
    }
	
	public function get_hs_timeline($date='')
    {
       if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $date = empty($date) ? date('Y-m-d') : $date; 
        $data = array();
        $data_hs = $this->m_hs->get_all($date)->result();
        //$data['hs_time'] = $this->m_hs->get_data('hs_time')->result();
		$data['hs_time'] = $this->m_hs->get_branch_hs_time($this->branch_id)->result();
		
		//print_r($this->branch_id);
		//return;
		
		$abbr = $this->m_hs->get_all_masterabbr($this->branch_id)->result();
		$temp = array();
		if (!empty($abbr)){
			foreach($abbr as $val){
				$temp[$val->abbr_name]['abbr_hs'] = $val->abbr_name;
				$temp[$val->abbr_name]['branch_id'] = '';
				$temp[$val->abbr_name]['call_name'] = '';
				$temp[$val->abbr_name]['created_at'] = '';
				$temp[$val->abbr_name]['creator_id'] = '';
				$temp[$val->abbr_name]['end_date'] = '';
				$temp[$val->abbr_name]['id_user'] = '';
				$temp[$val->abbr_name]['name'] = '';
				$temp[$val->abbr_name]['ptgshs_id'] = '';
				$temp[$val->abbr_name]['start_date'] = '';
			}
		}
		
		
        $data['hs_initial_petugas_hs'] = $this->m_hs->get_data_hs_initial_petugas_hs($this->branch_id, $date)->result();
		
		if (!empty($data['hs_initial_petugas_hs'])){
			foreach($data['hs_initial_petugas_hs'] as $val){
				$temp[$val->abbr_hs]['abbr_hs'] = $val->abbr_hs;
				$temp[$val->abbr_hs]['branch_id'] = $val->branch_id;
				$temp[$val->abbr_hs]['call_name'] = $val->call_name;
				$temp[$val->abbr_hs]['created_at'] = $val->created_at;
				$temp[$val->abbr_hs]['creator_id'] = $val->creator_id;
				$temp[$val->abbr_hs]['end_date'] = $val->end_date;
				$temp[$val->abbr_hs]['id_user'] = $val->id_user;
				$temp[$val->abbr_hs]['name'] = $val->name;
				$temp[$val->abbr_hs]['ptgshs_id'] = $val->ptgshs_id;
				$temp[$val->abbr_hs]['start_date'] = $val->start_date;
			}
		}
		$ii = 0;
		foreach($temp as $k=>$v){
			$temp[$ii] = $temp[$k];
			unset($temp[$k]);
			$ii++;
		}
		
		$data['hs_initial_petugas_hs'] = $temp;
		
        if (!empty($data_hs)) {
            $i = 0;
			$temp_nama = '';
			$temp_count = 1;
			$temp_id = [];
			$temp_time_id = [];
            foreach ($data_hs as $key => $value) {
				
				$time = substr($value->time_name, 0,8);
				$temp_time_id[]=$value->time_id;
				if($value->namacaps==$temp_nama && $i!=0)
				{
					$temp_count++;
					foreach($data['data'] as $k=>$v){
						if($v['nama'] == $temp_nama)
						{
							$value->time_id = $data['data'][$k]['time_id'];
							$time = $data['data'][$k]['time_name'];
							$i = $k;
							unset($data['data'][$k]);
						}
					}
				}
				else {
					$temp_count=1;
					$temp_id = [];
					$temp_time_id = [];
					$temp_time_id[]=$value->time_id;
				}
				$temp_id[]=$value->id;
				
				$temp_nama = $value->namacaps;
				
                $data['data'][$i]['id'] = $value->id;
                $data['data'][$i]['no'] = $i+1;
                $data['data'][$i]['date'] = $value->date;
                $data['data'][$i]['pid'] = $value->pid;
                //$time = substr($value->time_name, 0,8);
                $data['data'][$i]['time_name'] = $time;
                $data['data'][$i]['petugas_id'] = $value->petugas_id;
                $data['data'][$i]['time_id'] = $value->time_id;
                $data['data'][$i]['ptgshs_id'] = $value->ptgshs_id;
                $data['data'][$i]['nama'] = $value->namacaps;
                $data['data'][$i]['alamat'] = $value->alamat;
                $data['data'][$i]['no_telp'] = $value->no_telp;
                $data['data'][$i]['dokter'] = $value->dokter;
                $data['data'][$i]['jumlah_pasien'] = $value->jumlah_pasien;
                $data['data'][$i]['pemeriksaan'] = $value->pemeriksaan;
                $data['data'][$i]['pay_name'] = $value->pay_name;
                $data['data'][$i]['nama_petugas'] = $value->nama_petugas;
                $data['data'][$i]['nama_petugas_hs'] = $value->nama_petugas_hs;
                $data['data'][$i]['ttd_pasien'] = $value->ttd_pasien;
                $data['data'][$i]['branch'] = $value->branch_name;
                $data['data'][$i]['status'] = $value->status;
                $data['data'][$i]['reason'] = $value->reason;
				$data['data'][$i]['catatan'] = $value->catatan;
                $data['data'][$i]['time_selesai'] = $value->time_selesai;
                $data['data'][$i]['creator'] = $value->name_creator;
                $data['data'][$i]['created_date'] = $value->created_date;
				$data['data'][$i]['booked'] = $value->booked;
				$data['data'][$i]['deleted'] = $value->deleted;
                
				$data['data'][$i]['colspan'] = $temp_count;
				$data['data'][$i]['multi_id'] = implode(",",$temp_id);
				$data['data'][$i]['multi_time_id'] = implode(",",$temp_time_id);
				$data['data'][$i]['time_name_selesai'] = substr($value->time_name, 0,8);
				
                $i++;
            }
        }
        echo json_encode($data);
    }
	

    public function index($id = '')
    {
        $data['model'] = $this->model;
        if (!empty($id)) {
            $this->session->set_userdata('selected_branch', $id);
            $this->branch_id = $id;
        }
        $data['master_branch'] = $this->m_masterusers->get_all_relations('branch')->result();
        $data['hs_time'] = $this->m_hs->get_data('hs_time')->result();
        $data['hs_payment'] = $this->m_hs->get_data('hs_payment')->result();
        $data['branch'] = $this->m_hs->get_data('branch')->result();
        $data['hs_initial_petugas'] = $this->m_hs->get_data_hs_initial_petugas($this->branch_id)->result();
        $data['hs_initial_petugas_hs'] = $this->m_hs->get_data_hs_initial_petugas_hs($this->branch_id, date('Y-m-d'))->result();
        $data['selected_branch'] = $this->session->userdata('selected_branch');;

        $this->load->view('v_hs', $data);
    }
    
    public function autoblock() {
        $hsblock= $this->m_hs->get_data('hs_block')->result();
        foreach ($hsblock as $data ) {
            if($data->hari_id == date('w')) {

                $ptgs = $this->m_hs->get_where(array('ptgshs_id'=>$data->ptgs_id), 'hs_initial_petugas_hs')->result();
                $id_user = $ptgs[0]->id_user;
                $ptgshs = $this->m_hs->get_hs_initial_petugas_hs_by_id_user($id_user, date('Y-m-d'))->result()[0];
                $data->ptgs_id = $ptgshs->ptgshs_id;

                $jam = explode(",",$data->time_id);
                foreach ($jam as $time_id ) {
					if($this->m_hs->get_where(array('time_id' => $time_id, 'ptgshs_id' => $data->ptgs_id, 'date' => date('Y-m-d') ), $this->model)->num_rows() == 0) {
						$this->m_hs->insert_data(["date" => date('Y-m-d'), "nama" => $data->nama, "time_id"=> $time_id, "ptgshs_id"=> $data->ptgs_id, "alamat" => "Autoblock",  "pay_id" => 1, "created_date" => date('Y-m-d H:i:s') , "branch_id"=>$data->branch_id, "creator_id" => 47, "status"=> "Selesai", "petugas_id" => 47], 'homeservice');
						var_dump($data);
					}
                }
            }
        }
    }
}
