<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');
use Illuminate\Http\Request;
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');

/**
*
*/
class Welcome extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->library(['Notify_lib']);
		$this->load->library('recaptcha');
		$this->load->helper('tgl_indo');

	}

	public function index(){

		$user_id = $this->session->userdata("id_user");
		$level = $this->session->userdata("level");

		$recaptcha = new Recaptcha();
		$box = $recaptcha->create_box();
		// var_dump($user_id);
		// exit();
		if($user_id == null) {
			$this->load->view('v_login',['recaptcha' => $box]);
		} else {
			if ($level == "1") {
				header("location:" . base_url() . "cpanel/dashboard");
			} else {
				header("location:" . base_url() . "admin/homeservice");
			}
		}
	}

	public function getKabupaten($idPropinsi=0,$id=0){
		$id_propinsi = ($idPropinsi==0)?$this->input->post('id_propinsi'):$idPropinsi;
		$param = ['id_propinsi' => $id_propinsi];
		if ($id>0) {
			$param=['id_kabkota' => $id];
		}
		$data = $this->m_profile->get_list_select('m_ikabkota',$param);
		if (!$this->input->is_ajax_request()) {
			return $data;
		}else{
			$new_format=[];
			foreach ($data as $key => $value) {
				$new_format[$key+1]['id'] = $value->id_kabkota;
				$new_format[$key+1]['text'] = $value->nama_kabkota;
			}
			$new_format[0]['id']=0;
			$new_format[0]['text']='Pilih';
			asort($new_format);
			echo json_encode($new_format);
		}	
	}
	
	public function getKecamatan($idKabkota=0,$id=0){
		$id_kabkota = ($idKabkota==0)?$this->input->post('id_kabkota'):$idKabkota;
		$param = ['id_kabkota' => $id_kabkota];
		if ($id>0) {
			$param=['id_kecamatan' => $id];
		}
	
		$data = $this->m_profile->get_list_select('m_ikecamatan',$param);
		if (!$this->input->is_ajax_request()) {
			return $data;
		}else{
			$new_format=[];
			foreach ($data as $key => $value) {
				$new_format[$key+1]['id'] = $value->id_kecamatan;
				$new_format[$key+1]['text'] = $value->nama_kecamatan;
			}
			$new_format[0]['id']=0;
			$new_format[0]['text']='Pilih';
			asort($new_format);
			echo json_encode($new_format);
		}	
	}
	
	public function getKelurahan($idKecamatan=0,$id=0){
		$id_kecamatan = ($idKecamatan==0)?$this->input->post('id_kecamatan'):$idKecamatan;
		$param = ['id_kecamatan' => $id_kecamatan];
		if ($id>0) {
			$param=['id_kelurahan' => $id];
		}
		$data = $this->m_profile->get_list_select('m_ikelurahan',$param);
		
	
		if (!$this->input->is_ajax_request()) {
			return $data;
		}else{
			$new_format=[];
			foreach ($data as $key => $value) {
				$new_format[$key+1]['id'] = $value->id_kelurahan;
				$new_format[$key+1]['text'] = $value->nama_kelurahan;
			}
			$new_format[0]['id']=0;
			$new_format[0]['text']='Pilih';
			asort($new_format);
			echo json_encode($new_format);
		}	
	}
	
	public function getPropinsi(){
		$data = $this->m_profile->get_list_select('m_ipropinsi',[]);
		if (!$this->input->is_ajax_request()) {
			return $data;
		}else{
			$new_format=[];
			foreach ($data as $key => $value) {
				$new_format[$key+1]['id'] = $value->id_propinsi;
				$new_format[$key+1]['text'] = $value->nama_propinsi;
			}
			$new_format[0]['id']=0;
			$new_format[0]['text']='Pilih';
			asort($new_format);
			echo json_encode($new_format);
		}	
	}


	public function changeLogin(){

	$nip = $this->input->post('NIP');
	$baru = $this->input->post('password_new');

	
	$this->form_validation->set_rules('password_new', 'Password Baru', 'required|min_length[6]|trim|required');
//	$this->form_validation->set_rules('password_new','Password Baru','trim|callback_valid_password');

	if($this->form_validation->run()==FALSE){
	$this->session->set_flashdata('error',"Data Gagal Di Tambahkan");
	redirect('');
	}else{

	$where = array('NIP' => $nip);
	$data = $this->m_mis->edit_data($where,'users');
	$d = $this->m_mis->edit_data($where,'users')->row();
	$cek = $data->num_rows();

	if($cek > 0){

	$data = array(
		'NIP' => $nip,
		'password'=>sha1('Bio Medika'.$baru)
	);

	$where = array('NIP' => $nip);
	$this->m_mis->update_data($where,$data,'users');
	$this->session->set_flashdata('sukses',"Password berhasil di ubah");
	redirect(base_url().'');
	
	}


	else{
	$this->session->set_flashdata('valid_password','NIP tidak di temukan');
	redirect(base_url().'');
	
	}


	


	}

	

}


	public function checkSession(){
		$user_login = $this->session->userdata("id_user");
		$ret = [
			'session' => 'continue',
		];
		if (empty($user_login)) {
			$ret = [
				'session' => 'end'
			];
		}
		echo json_encode($ret);
	}

	public function login(){

		if($this->input->post('action') === 'submit')
		{
			/*
			 Check if the reCAPTCHA was solved
			 You can pass arguments to the `is_valid` method,
			 but it should work fine without any.
			 Check the "Validating the reCAPTCHA" section for more details
			*/
			$is_valid =$recaptcha->is_valid();

			if($is_valid['success'])
			{
				echo "reCAPTCHA solved";
			}
			else
			{
				echo "reCAPTCHA not solved/an error occured";
			}
		}

		$salt = 'Bio Medika';
		$password = '445566';

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		//$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|trim|required');
		if ($this->form_validation->run() != false) {
		// $where = array('name=$username OR NIP=$username OR username=$username', 'password' => sha1($salt . $password));
		// $data = $this->m_mis->edit_data($where, 'users');
		// $d = $this->m_mis->edit_data($where, 'users')->row();
		$this->load->model('restapi/user_model');
		$b = $this->user_model->signin($username, $password);
		$c = json_decode($b);
		$d = $c->user;
		if (empty($d) == false) {
			// print_r($d->user->id_user);
			// exit();

			$where = array('id_position' => $d->id_position);
			$e = $this->m_mis->edit_data($where, 'position')->row();

			$bagian = $this->db->where(['id' => $d->id_bagian])->get('bagian')->row();
			//var_dump($bagian);

			// var_dump($e);
			$f = null;
			if(empty($bagian) == false) {
				$f = $this->db->where(['id_department' => $bagian->id_department])->get('departments')->row();
			}
			else if(empty($e) == false) {
				$where = array('id_department' => $e->id_department);
				$f = $this->m_mis->edit_data($where, 'departments')->row();
			}
			
			$g = null;
			if(empty($f) == false) {
				$where = array('branch_id' => $d->branch_id);
				$g = $this->m_mis->edit_data($where, 'branch')->row();
			}
			
			$h = null;
			if(empty($h) == false) {
				$where = array('id' => $d->id);
				$h = $this->m_mis->edit_data($where, 'bagian')->row();
			}
			

			// $cek = $data->num_rows();


			$session = array(
				'id_user' => $d->id_user,
				'name' => $d->name,
				'last_name' => $d->last_name,
				'call_name' => $d->call_name,							 
				'username' => $d->username,
				'email' => $d->email,
				'level' => $d->level,
				'id_position' => $d->id_position,
				'position' => empty($e) ? '' : $e->name_position,
				'department' => empty($f) ? '' : $f->name,
				'id_department' => empty($f) ? '' : $f->id_department,
				'branch_id' => empty($g) ? '' : $g->branch_id,
				'branch_name' => empty($g) ? '' : $g->branch_name,
				'image' => $d->image,
				'status' => 'login',
				'last_login' => $d->last_login,
				'position_name' => $d->position,
				'bagian_name' => $d->bagian_name,
				'id_bagian' => $d->id_bagian
			);
			//print_r($session);
			// exit();
			$this->session->set_userdata($session);
			helper_log("login", "Login");
			if ($session["level"] == "1") {
				$rsp = [
					'code' => 200,
					'msg' => 'login berhasil !',
					'redirect' => base_url() . "admin/homeservice"
				];
				// header("location:" . base_url() . "cpanel/dashboard");
			} else {
				// header("location:" . base_url() . "admin/homeservice");
				$rsp = [
					'code' => 200,
					'msg' => 'login berhasil !',
					'redirect' => base_url() . "admin/homeservice"
				];
			}

			// if ($session["id_position"] == "3") {
			// 	// header("location:" . base_url() . "admin/qc");
			// 	$rsp = [
			// 		'code' => 200,
			// 		'msg' => 'login berhasil !',
			// 		'redirect' => base_url() . "admin/qc"
			// 	];
			// } else {
			// 	// header("location:" . base_url() . "admin/homeservice");
			// 	$rsp = [
			// 		'code' => 200,
			// 		'msg' => 'login berhasil !',
			// 		'redirect' => base_url() . "admin/homeservice"
			// 	];
			// }


			} else {
				// $this->session->set_flashdata('alert', 'Login gagal ! Username dan password salah.');
				// redirect(base_url());
				$rsp = [
					'code' => 304,
					'msg' => 'login gagal ! Username dan password salah.',
					'redirect' => base_url()
				];
			}
		} else {
			// $this->session->set_flashdata('alert', 'Anda Belum mengisi username atau password');
			// $this->load->view('v_login');
			$rsp = [
				'code' => 304,
				'msg' => 'Anda Belum mengisi username atau password anda kurang dari 6 karakter',
				'redirect' => base_url()
			];
		}

		echo json_encode($rsp);
		exit();
	}


	public function job(){
		$data['job_setting'] = $this->m_general->get_data_like('job','name','param_setting')->row();
		$this->load->model('m_profile');
		$data['propinsi'] = $this->m_profile->get_all_relations('m_ipropinsi')->result();

		if(isset($data['job_setting']->active)){
			if($data['job_setting']->active=="0")
			{
				redirect('');
			}
			else if($data['job_setting']->active=="1"){
				$user_id = $this->session->userdata("id_user");
				$level = $this->session->userdata("level");
				// var_dump($user_id);
				// exit();
				//if($user_id == null) {
				$this->load->view('job',$data);
				/*} else {
				if ($level == "1") {
				header("location:" . base_url() . "cpanel/dashboard");
				} else {
				header("location:" . base_url() . "admin/homeservice");
				}
				}*/
			}
		}
	}

	public function fs(){
		$this->session->set_userdata('status',"login");
		$this->load->model('m_branch');
		$data['propinsi'] = $this->m_profile->get_all_relations('m_ipropinsi')->result();
		$data['branch'] = $this->m_branch->get_data('branch')->result();
		$data['time'] = $this->m_fs->get_data('master_fs')->result();


		//CEK TGL MERAH
		$date = array();
		/*$array = json_decode(file_get_contents("https://raw.githubusercontent.com/guangrei/Json-Indonesia-holidays/master/calendar.json"),true);
		//print_r($array);
		foreach($array as $v=>$g){
			$year = substr($v,0,4);
			$month = substr($v,4,2);
			$day = substr($v,6,2);
			if(isset($g['deskripsi'])) $date[] = json_encode(array($year,$month,$day,$g['deskripsi']));
			//print_r($g['deskripsi']);
		}*/
		
		$start_date = date('Y').'-01-01';
		$end_date = date('Y').'-12-31';
		
		$ch = curl_init(); 
		
		$endpoint = 'https://www.googleapis.com/calendar/v3/calendars/en.indonesian%23holiday%40group.v.calendar.google.com/events';
		$params = array('key' => 'AIzaSyDcnW6WejpTOCffshGDDb4neIrXVUA1EAE',
						'timeMin' => $start_date.'T00:00:00+07:00',
						'timeMax' => $end_date.'T00:00:00+07:00',
						'singleEvents' => 'true',
						'maxResults' => '9999');
								
		$url = $endpoint . '?' . http_build_query($params);
		//echo $url;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);      
		$arr = json_decode($output);
		
		foreach($arr->items as $val){
			$v = $val->start->date;
			$year = substr($v,0,4);
			$month = substr($v,5,2);
			$day = substr($v,8,2);
			$date[] = json_encode(array($year,$month,$day,$val->summary));
		}
		
		
		
		
		
		$data['date'] = ($date);
		//print_r($date);
		//CEK TGL MERAH

		$this->load->view('formulir_screening',$data);

	}

function insertFs(){
	if (!$this->input->is_ajax_request()) {
        exit('No direct script access allowed');
    }
	

	
    $edit=0;
	if (!empty($this->input->post('id'))) {
		$edit = 1;
	}

	$data = $this->input->post();
	//$data['creator_id'] = (isset($this->user_id))?$this->user_id:'';

	/*prevent unset data field (select)*/
	!isset($data['branch_id'])?$data['branch_id']='':'';
	!isset($data['jam_tersedia'])?$data['jam_tersedia']='':'';
	
	!isset($data['id_propinsi'])?$data['id_propinsi']='':'';
	!isset($data['id_kabupaten'])?$data['id_kabupaten']='':'';
	!isset($data['id_kecamatan'])?$data['id_kecamatan']='':'';
	!isset($data['id_desa'])?$data['id_desa']='':'';
	
	
	
	$vf = [];
	$valid_form = [];
    $res_valid_form = [];
    
	foreach ($data as $key => $value) {
		$resp = $this->field_validation($key,$value);
        $vf[] = $resp['status'];
        $valid_form[$key] = $value;
        $res_valid_form[$key] = $resp['msg'];
	}

	if (empty($vf) || in_array(0,$vf)) {
		$result['status'] = false;
            $result['message'] = 'Data belum lengkap. silakan di ulangi!';
            $result['form_res'] = $res_valid_form;
	}else{
		if ($edit) {
			/*edit*/
			$id = array('id'=>$data['id']);
			$this->m_general->update_data($id,$data,'fs');
		}else{
			/*save*/
			$config = [
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'protocol'  => 'smtp',
            'smtp_host' => 'mail.biomedika.co.id',
            'smtp_user' => 'bookingform@biomedika.co.id',  // Email gmail
            'smtp_pass'   => 'biomedika2020',  // Password gmail
            'smtp_crypto' => 'ssl',
            'smtp_port'   => 465,
            'crlf'    => "\r\n",
            'newline' => "\r\n"
        ];

       // Load library email dan konfigurasinya
        $this->load->library('email', $config);
         // Email dan nama pengirim
        $this->email->from('bookingform@biomedika.co.id', 'No-reply');
        // Email penerima
        $this->email->to('rahmat@bio-medika.com'); // Ganti dengan email tujuan
        $this->email->attach('https://masrud.com/content/images/20181215150137-codeigniter-smtp-gmail.png');
         // Subject email
        $this->email->subject('Kirim Email dengan SMTP Gmail CodeIgniter');
         // Isi email
        $this->email->message("Ini adalah contoh email yang dikirim menggunakan SMTP Gmail pada CodeIgniter.<br><br> Klik <strong><a href='https://masrud.com/post/kirim-email-dengan-smtp-gmail' target='_blank' rel='noopener'>disini</a></strong> untuk melihat tutorialnya.");
        $this->email->send();
		$this->m_general->insert_data($data,'fs');
		}
		
		
		$result['status'] = true;
	    $result['message'] = 'Data berhasil disimpan.';
	    	
	}
	echo json_encode($result);
}

public function cekSlot(){
	if (!$this->input->is_ajax_request()) {
		exit('No direct script access allowed');
	}
	
	$propinsi = $this->input->post('propinsi');
	$kabupaten = $this->input->post('kabupaten');
	$kecamatan = $this->input->post('kecamatan');
	$desa = $this->input->post('desa');
	$branch = $this->input->post('branch');
	$tgl_periksa = $this->input->post('tgl_periksa');
	$jam_tersedia = $this->input->post('jam_tersedia');
	
	
	//CEK TGL MERAH
	/*
	$value = date("Ymd", strtotime($tgl_periksa));
	$array = json_decode(file_get_contents("https://raw.githubusercontent.com/guangrei/Json-Indonesia-holidays/master/calendar.json"),true);
	$flag_merah = "";	
	if(isset($array[$value]))
		$flag_merah = "tanggal merah ".$array[$value]["deskripsi"]; //check tanggal merah berdasarkan libur nasional	
	elseif(date("D",strtotime($value))==="Sun")
		$flag_merah = "tanggal merah hari minggu"; //check tanggal merah berdasarkan hari minggu
	*/
	//CEK TGL MERAH
	
	//if($flag_merah==""){
		$data = array('fs_id'=>$jam_tersedia);
			
		$slot_master = $this->m_fs->edit_data($data,'master_fs')->result();	
		$slot = isset($slot_master[0]->slot)?$slot_master[0]->slot:0;
		
		$data = array(
			//'id_propinsi'=>$propinsi,
			//'id_kabupaten'=>$kabupaten,
			//'id_kecamatan'=>$kecamatan,
			//'id_desa'=>$desa,
			'branch_id'=>$branch,
			'tgl_periksa'=>$tgl_periksa,
			'jam_tersedia'=>$jam_tersedia
			);
			
		$slot_fs = $this->m_fs->edit_data($data,'fs')->result();	
		$slotfs = count($slot_fs);
		
		echo json_encode(array('slot'=>$slot-$slotfs,'msg'=>''));
	//}
	//else
	//	echo json_encode(array('slot'=>0,'msg'=>$flag_merah));
}

	
function insertApp()
{
	if (!$this->input->is_ajax_request()) {
        exit('No direct script access allowed');
    }
	
	$data['job_setting'] = $this->m_general->get_data_like('job','name','param_setting')->row();

	if(isset($data['job_setting']->active)){
		if($data['job_setting']->active=="0")
		{
			redirect('');
		}
	}
	
    $edit=0;
	if (!empty($this->input->post('id_applicant'))) {
		$edit = 1;
	}

	$data = $this->input->post();
	$data['creator_id'] = (isset($this->user_id))?$this->user_id:'';

	/*prevent unset data field (select)*/
	!isset($data['POSITION'])?$data['POSITION']='':'';
	
	!isset($data['GENDER'])?$data['GENDER']='':'';
	!isset($data['PENDIDIKAN'])?$data['PENDIDIKAN']='':'';
	!isset($data['PROPINSI'])?$data['PROPINSI']='':'';
	!isset($data['KABUPATEN'])?$data['KABUPATEN']='':'';
	!isset($data['KECAMATAN'])?$data['KECAMATAN']='':'';
	!isset($data['KELDESA'])?$data['KELDESA']='':'';
	
	!isset($data['STATUSRUMAH'])?$data['STATUSRUMAH']='':'';
	!isset($data['STATUS'])?$data['STATUS']='':'';
	!isset($data['AGAMA'])?$data['AGAMA']='':'';
	!isset($data['BERSEDIA_DITEMPAT'])?$data['BERSEDIA_DITEMPAT']='':'';
	!isset($data['ANA_LVL_COMP'])?$data['ANA_LVL_COMP']='':'';
	!isset($data['ANA_LVL_ENG'])?$data['ANA_LVL_ENG']='':'';
	!isset($data['ANA_LVL_MAN'])?$data['ANA_LVL_MAN']='':'';
	!isset($data['ANA_LVL_DAR'])?$data['ANA_LVL_DAR']='':'';
	!isset($data['ANA_LVL_MANUAL'])?$data['ANA_LVL_MANUAL']='':'';
	!isset($data['ANA_LVL_PEM'])?$data['ANA_LVL_PEM']='':'';
	!isset($data['ANA_LVL_KOR'])?$data['ANA_LVL_KOR']='':'';
	!isset($data['ANA_LVL_SEN'])?$data['ANA_LVL_SEN']='':'';
	!isset($data['ANA_LVL_MIC'])?$data['ANA_LVL_MIC']='':'';
	!isset($data['ANA_LVL_TIM'])?$data['ANA_LVL_TIM']='':'';
	!isset($data['ANA_ADA_PHR'])?$data['ANA_ADA_PHR']='':'';
	!isset($data['ANA_ADA_STR'])?$data['ANA_ADA_STR']='':'';
	!isset($data['PER_LVL_COMP'])?$data['PER_LVL_COMP']='':'';
	!isset($data['PER_LVL_ENG'])?$data['PER_LVL_ENG']='':'';
	!isset($data['PER_LVL_MAN'])?$data['PER_LVL_MAN']='':'';
	!isset($data['PER_LVL_PHL'])?$data['PER_LVL_PHL']='':'';
	!isset($data['PER_LVL_EKG'])?$data['PER_LVL_EKG']='':'';
	!isset($data['PER_LVL_TEN'])?$data['PER_LVL_TEN']='':'';
	!isset($data['PER_LVL_SPI'])?$data['PER_LVL_SPI']='':'';
	!isset($data['PER_LVL_AUD'])?$data['PER_LVL_AUD']='':'';
	!isset($data['PER_LVL_PAP'])?$data['PER_LVL_PAP']='':'';
	!isset($data['PER_ADA_STR'])?$data['PER_ADA_STR']='':'';
	!isset($data['PER_ADA_BCLS'])?$data['PER_ADA_BCLS']='':'';
	/**/

	//$data['FINGERID'] = intval($data['FINGERID']);

	$vf = [];
	$valid_form = [];
    $res_valid_form = [];
    
	foreach ($data as $key => $value) {
		$resp = $this->field_validation($key,$value);
        $vf[] = $resp['status'];
        $valid_form[$key] = $value;
        $res_valid_form[$key] = $resp['msg'];
	}

	if (empty($vf) || in_array(0,$vf)) {
		$result['status'] = false;
            $result['message'] = 'Data belum lengkap. silakan di ulangi!';
            $result['form_res'] = $res_valid_form;
	}else{
		if ($edit) {
			/*edit*/
			$id_applicant = array('id_applicant'=>$data['id_applicant']);
			$this->m_general->update_data($id_applicant,$data,'applicant');
		}else{
			/*save*/
			$this->m_general->insert_data($data,'applicant');
		}
		
		
		$result['status'] = true;
	    $result['message'] = 'Data berhasil disimpan.';
	    	
	}
	echo json_encode($result);
}

function field_validation($field,$value){
	switch ($field) {
		/*case 'name':
		case 'last_name':
		case 'username':
		case 'password':
		case 'call_name':
		case 'inisial':
		case 'GENDER':
		case 'ALAMAT':
		case 'RTRW':
		case 'KELDESA':
		case 'ALAMAT_DOMO':
		case 'RTRW_DOMO':
		case 'TMTLAHIR':
		case 'TIPEBAYAR':
		case 'BANK':
		case 'STATUSKAWIN':
		case 'STATUS':
		case 'BUMIDA':
		case 'AVPENSION':
		case 'PENDIDIKAN':
		case 'GOLDARAH':
			# text & notEmpty
			$ret = [
					'status' => 1,
					'msg' => 'ok'
				];
			if (empty($value) || !is_string($value)) {
				$ret = [
					'status' => 0,
					'msg' => 'inputan bukan string atau kosong!'
				];
			}
			return $ret;
			break;

		case 'TGLMASUK':
		case 'TGLJAMSOSTEK':
		case 'TGLLAHIR':
			# date & notEmpty
			$ret = [
					'status' => 1,
					'msg' => 'ok'
				];
			if (empty($value) || !$this->misc->checkIsAValidDate($value)) {
				$ret = [
					'status' => 0,
					'msg' => 'inputan tgl tidak valid atau kosong!'
				];
			}
			return $ret;
			break;
		
		case 'KODEPOS':
		case 'KODEPOS_DOMO':
		case 'FINGERID':
		case 'NIP':
		case 'NONPWP':
		case 'NOKTP':
		case 'NOREK':
		case 'TELPONHP':
		case 'TELPONRUMAH':
		case 'JMLANAK':
		// case 'NOJAMSOSTEK':
		case 'branch_id':
		case 'id_position':
		case 'id_bagian':
		case 'id_department':
		case 'CUTI':
		// case 'AVKELAS':
		case 'KECAMATAN':
		case 'KABUPATEN':
		case 'PROPINSI':
		case 'KELDESA_DOMO':
		case 'KECAMATAN_DOMO':
		case 'KABUPATEN_DOMO':
		case 'PROPINSI_DOMO':
		case 'TINGGI':
		case 'BERAT':
			# number & notEmpty
			$ret = [
					'status' => 1,
					'msg' => 'ok'
				];
			if (empty($value) || !is_numeric($value)) {
				$ret = [
					'status' => 0,
					'msg' => 'inputan bukan angka atau kosong!'
				];
			}
			return $ret;
			break;
		
		case 'email':
			# string email
			$ret = [
					'status' => 1,
					'msg' => 'ok'
				];
			$patternCheck = filter_var($value, FILTER_VALIDATE_EMAIL);
			if (!$patternCheck) {
				$ret = [
					'status' => 0,
					'msg' => 'email tidak valid atau kosong!'
				];
			}
			return $ret;
			break;
*/
		default:
			$ret = [
					'status' => 1,
					'msg' => 'ok'
				];
			return $ret;
			break;
	}
}


	public function getTimeUpdate(){
		$tgl_skrg = date('d');
		$bln_skrg = date('m');
		$ret = [
				'status' => false,
				'msg' => ''
			];
		/*periode update = 1 januai tiap tahunnya */
		if ($tgl_skrg == '01' && $bln_skrg == '01') {
			//update
			$this->updateCutiTetap();
			$ret = [
				'status' => true,
				'msg' => 'data cuti sudah diupdate!'
			];
		}
		//else{
			$this->updateCutiKontrak();
			$ret = [
				'status' => true,
				'msg' => 'data cuti sudah diupdate!'
			];
		//}

		echo json_encode($ret);
	}

	
	public function updateCutiTetap(){
		$param = [];
		$param['in_param'] = [
			'STATUS' => [
				'TETAP',
			]
		];
		$this->load->library('misc');
		$data = $this->m_masterusers->load_data($param,0,0,1);
		foreach ($data as $key => $value) {
			/* count masa kerja */
			$now = date('Y-m-d');
			$tgl_masuk = $value['TGLMASUK'];
			$user_id = $value['id_user'];
			$masa_tahun = 0;
			$masa_bulan = 0;
			
			if ($tgl_masuk == '0000-00-00' || $tgl_masuk == '0000-00-00 00:00:00' || $tgl_masuk == null || empty($tgl_masuk)) {
				$masa_tahun = 0;
			}else{
				$resTgl = $this->misc->selisih_waktu($now,$tgl_masuk);
				$masa_tahun = $resTgl['tahun'];
				if ($masa_tahun < 1) {
					$masa_bulan = $resTgl['bulan'];
				}
			}

			/*6 bulan = 6 hari; 1 tahun = 12 hari; >1 tahun = 14 hari*/
			$jatah_cuti = 0;
			/*if ($masa_tahun < 1) {
				if ($masa_bulan >= 6) {
					$jatah_cuti = 6;
				}
			}elseif ($masa_tahun >= 1 && $value['STATUS'] == 'KONTRAK') {
				$jatah_cuti = 12;
			}elseif ($masa_tahun > 2) {
				$jatah_cuti = 14;
			}*/
			
			if ($value['STATUS'] == 'TETAP') {
				$jatah_cuti = 14;
			}

			$data_parse = [
				'CUTI' => $jatah_cuti,
				'last_reset_cuti' => date('Y-m-d')			
			];
			
			$this->m_masterusers->update($data_parse,$user_id);
		}
	}
	
	public function updateCutiKontrak(){
		$param = [];
		$param['in_param'] = [
			'STATUS' => [
				'KONTRAK'
			]
		];
		$this->load->library('misc');
		//$param['((year(last_reset_cuti)<>'.date('Y').') or last_reset_cuti is null)'] = null;
		$param['(TIMESTAMPDIFF(MONTH, TGLMASUK, "'.date('Y-m-d').'")=6 or TIMESTAMPDIFF(MONTH, TGLMASUK, "'.date('Y-m-d').'")=12) and (TIMESTAMPDIFF(MONTH, last_reset_cuti, "'.date('Y-m-d').'")>=1 or last_reset_cuti is null)'] = null;
		$data = $this->m_masterusers->load_data($param,0,0,1);
		foreach ($data as $key => $value) {
			/* count masa kerja */
			$now = date('Y-m-d');
			$tgl_masuk = $value['TGLMASUK'];
			$user_id = $value['id_user'];
			$masa_tahun = 0;
			$masa_bulan = 0;
			
			if ($tgl_masuk == '0000-00-00' || $tgl_masuk == '0000-00-00 00:00:00' || $tgl_masuk == null || empty($tgl_masuk)) {
				$masa_tahun = 0;
			}else{
				$resTgl = $this->misc->selisih_waktu($now,$tgl_masuk);
				$masa_tahun = $resTgl['tahun'];
				if ($masa_tahun < 1) {
					$masa_bulan = $resTgl['bulan'];
				}
			}

			/*6 bulan = 6 hari; 1 tahun = 12 hari; >1 tahun = 14 hari*/
			$jatah_cuti = 0;
			/*if ($masa_tahun < 1) {
				if ($masa_bulan >= 6) {
					$jatah_cuti = 6;
				}
			}elseif ($masa_tahun >= 1 && $value['STATUS'] == 'KONTRAK') {
				$jatah_cuti = 12;
			}elseif ($masa_tahun > 2) {
				$jatah_cuti = 14;
			}*/
			
			if ($masa_tahun < 1) {
				if ($masa_bulan >= 6 && $value['STATUS'] == 'KONTRAK') {
					$jatah_cuti = 6;
				}
			}elseif ($masa_tahun >= 1 && $value['STATUS'] == 'KONTRAK') {
				$jatah_cuti = 12;
			}

			$data_parse = [
				'CUTI' => $jatah_cuti,
				'last_reset_cuti' => date('Y-m-d')			
			];
			
			$this->m_masterusers->update($data_parse,$user_id);
		}
	}
	
	public function updateCutiMasaBakti(){
		$param = [];
		$param['in_param'] = [
			'STATUS' => [
				'TETAP'
			]
		];
		$this->load->library('misc');
		$param['(TIMESTAMPDIFF(YEAR, TGLMASUK, "'.date('Y-m-d').'")=6 or TIMESTAMPDIFF(YEAR, TGLMASUK, "'.date('Y-m-d').'")=11) and (TIMESTAMPDIFF(YEAR, last_reset_masabakti, "'.date('Y-m-d').'")>=1 or last_reset_masabakti is null)'] = null;
		$data = $this->m_masterusers->load_data($param,0,0,1);
		foreach ($data as $key => $value) {
			/* count masa kerja */
			$now = date('Y-m-d');
			$tgl_masuk = $value['TGLMASUK'];
			$user_id = $value['id_user'];
			$masa_tahun = 0;
			//$masa_bulan = 0;
			
			if ($tgl_masuk == '0000-00-00' || $tgl_masuk == '0000-00-00 00:00:00' || $tgl_masuk == null || empty($tgl_masuk)) {
				$masa_tahun = 0;
			}else{
				$resTgl = $this->misc->selisih_waktu($now,$tgl_masuk);
				$masa_tahun = $resTgl['tahun'];
				//if ($masa_tahun < 1) {
				//	$masa_bulan = $resTgl['bulan'];
				//}
			}

			/*6 bulan = 6 hari; 1 tahun = 12 hari; >1 tahun = 14 hari*/
			$jatah_cuti = 0;
			
			if ($masa_tahun == 6 && $value['STATUS'] == 'TETAP') {
				$jatah_cuti = 6;
			}elseif ($masa_tahun == 11 && $value['STATUS'] == 'TETAP') {
				$jatah_cuti = 12;
			}

			$data_parse = [
				'masabakti_keseluruhan' => $jatah_cuti,
				'masabakti' => $jatah_cuti,
				'last_reset_masabakti' => date('Y-m-d')			
			];
			
			$this->m_masterusers->update($data_parse,$user_id);
		}
		
		$paramx['(TIMESTAMPDIFF(YEAR, TGLMASUK, "'.date('Y-m-d').'")=7 or TIMESTAMPDIFF(YEAR, TGLMASUK, "'.date('Y-m-d').'")=12) and (TIMESTAMPDIFF(YEAR, last_reset_masabakti, "'.date('Y-m-d').'")>=1)'] = null;
		$datax = $this->m_masterusers->load_data($paramx,0,0,1);
		foreach ($datax as $key => $value) {
			//print_r($value);
			
			$user_id = $value['id_user'];
			
			//RESET CUTI
			$jatah_cuti = 0;
			$data_parse = [
				'masabakti_keseluruhan' => $jatah_cuti,
				'masabakti' => $jatah_cuti,
				'last_reset_masabakti' => date('Y-m-d')			
			];
			
			$this->m_masterusers->update($data_parse,$user_id);
		}
		
	}
	
	public function pengumuman(){
		$cek_hari_nasional = $this->get_google_calender();
		$cek_flag=1;
		if(isset($cek_hari_nasional[0])){
			foreach($cek_hari_nasional as $val){
				if($val['date'] == date('Y-m-d')) 
					$cek_flag=0;
			}
		}
		
		if($cek_flag){
			$date_now = date('d-m-y');
			//between 5 menit now
			$ann = $this->db->query("select * from announcements where 
			deleted_at is null and 
			time_repeat<>'00:00' and 
			(curdate() between start and end or tgl_multi like '%".$date_now."%') and 
			(CAST(time_repeat AS TIME)< CAST(NOW() AS TIME) and ADDTIME(CAST(time_repeat AS TIME),'600') > CAST(NOW() AS TIME))
			")->result();
			
			$receivers = [];
			
			
					
			$all_user = $this->db->query("select id_user from users")->result();
			foreach ($all_user as $key => $usr) {
				$receivers[] = $usr->id_user;
			}
			
			//print_r($receivers);
			$this->load->model('M_announcement');
			
			
			if(isset($ann[0])){
				//print_r($ann);
				foreach($ann as $val){
					$flag = true;
					
					if($val->tgl_multi==''){
						$startdate = date( 'Y-m-d',strtotime($val->start));
						$datenow = date('Y-m-d');				
						$x = strtotime($datenow)-strtotime($startdate);
						$x = floor($x/3600/24); //get days 
						
						if($val->repeat_type=="2")
						{
							if( $x%2 != 0) {$flag = false;}
						}
						else if($val->repeat_type=="3")
						{
							if( $x%7 != 0) {$flag = false;}
						}
						else if($val->repeat_type=="4")
						{
							if( $x%30 != 0) {$flag = false;}
						}
						else if($val->repeat_type=="5")
						{
							if( $x%365 != 0) {$flag = false;}
						}
					}
					//if($flag)
					//echo "kirim";
					
					if($flag){
						$cek = $this->db->query('select * from notifications where 
						data like \'%id_announcement":"'.$val->id.'"%\' and DATE(time)=curdate() limit 1
						')->result();
						//print_r($val->creator_id);
						if(!isset($cek[0]))
						{
							$this->load->model('m_receiver');
							$this->m_receiver->add_receiver($val->id, 'announcement', $receivers);
							
							$data = $this->M_announcement->one_announcement(['where' => ['_.id' => $val->id]]);
							
							$data['attachments'] = $this->M_announcement->getAttachments($val->id);
							
							$this->load->model('restapi/user_model');
							$sender = $this->user_model->one_user(['where' => ['_.id_user' => 1]]);
							$sender_name = '';
							$position_name = '';
							$branch_name = '';
							if($sender != null) {
								$sender_name = $sender['name'];
								$position_name = $sender['position_name'];
								$branch_name = $sender['branch_name'];
							}
							
							$filter = str_replace('<p>&nbsp;</p>','',$data['note']);
							$filter = str_replace('<p>','',$filter);
							$filter = str_replace('</p>','',$filter);
							
							$this->load->library('Notify_lib');
							$nl = new Notify_lib();
							$nl->send(
								'New Announcement',
								//' 
		//'.strip_tags($filter),
								strip_tags($filter),
								//$data['creator_name'] . ' has created new announcement',
								//$sender_name . ' - '.$position_name.' ('.$branch_name.') has created new announcement',
								1,
								$receivers,
								$data,
								'announcement',
								true
							);
						}
					}
				}
			}
			
			
		
		}
	}


	
	public function document(){
		$date_now = date('d-m-y');
		$ann = $this->db->query("select * from master_doc where tgl_document = DATE_ADD(curdate(), INTERVAL 1 YEAR)")->result();
		
		$receivers = [];
		
		//print_r($receivers);
		$this->load->model('M_announcement');
		
		
		if(isset($ann[0])){
			foreach($ann as $val){
				$all_user = $this->db->query("select email from users where id_position in (19) and id_bagian=".$val->bagian_id)->result();
				foreach ($all_user as $key => $usr) {
					$receivers[] = $usr->email;
				}
				
				if($val->bagian_id=="19")
					$receivers[] = "rahmat@bio-medika.com";
									
					
				//$this->load->library('kirim_email');
				
				$mailBody = '<h1>Reminder</h1>';
				$mailBody .= '<h3>Apakah Dokumen ini masih dipakai? </h2>';
				$mailBody .= '<table style="border:solid 1px;">
							<tr><td>No Doc</td><td>Nama Doc</td></tr>
						';	
				$mailBody .= '<tr><td>'.$val->no_doc.'</td>';
				$mailBody .= '<td>'.$val->nama_doc.'</td></tr>';
				$mailBody .='</table> <br /> <a href="'.base_url().'admin/master_doc?reminder_id='.$val->doc_id.'">Link dokumen</a>';	
				
				foreach ($receivers as $address) {
				
					$send_data = [
						'pengirim' => 'MIS Biomedika',
						'penerima' => $address,
						'subject' => 'MIS | Reminder Dokumen',
						'content' => $mailBody
					];

					$this->kirim_email->send_email($send_data);
					//print_r($send_data);
				}
			
			}
		}
	}
	
	public function insert_suhu(){
		$this->insert_suhu_karyawan();
		$this->insert_suhu_pasien();
	}
	
	public function insert_suhu_pasien(){
		$date_now = date('d-m-y');
		
		$mis_suhu = $this->db->query("select group_concat(QUOTE(user_id)) as val from suhu where user_type=0 and DATE(recog_time) = DATE(CURDATE())")->row();
		
		$otherdb = $this->load->database('third', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
		
		$temp = '';
		if(isset($mis_suhu->val)){
			$temp = "and a.user_id not in (".$mis_suhu->val.")";
		}
		
		$data = $otherdb->query("select a.*,COUNT(a.user_id) from face_record_stranger a where DATE(a.recog_time) = DATE(CURDATE()) ".$temp." group by a.user_id HAVING a.body_temperature >= 37.3 and count(a.user_id)>=2")->result();
		
		
		foreach ($data as $key => $usr) {
			$data_save['user_type'] = 0;
			$data_save['user_id'] =  $usr->user_id;
			$data_save['user_number'] =  $usr->id;
			$data_save['user_name'] =  $usr->sn;
			$data_save['user_photo'] =  $usr->recog_photo;
			$data_save['recog_photo'] =  $usr->recog_photo;
			$data_save['recog_time'] =  $usr->recog_time;
			$data_save['body_temperature'] =  $usr->body_temperature;
			
			$id = $this->m_general->insert_data($data_save,'suhu');
		}
		
		
	}
	
	public function insert_suhu_karyawan(){
		$date_now = date('d-m-y');
		
		$mis_suhu = $this->db->query("select user_number as val from suhu where user_type=1 order by id desc")->row();
		$seq = 0;
		if(isset($mis_suhu->val)) $seq = $mis_suhu->val;
		
		$otherdb = $this->load->database('third', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
		
		$data = $otherdb->query("select a.*,(select id_name from face_user where id=a.user_id) name from face_record a where a.id>".$seq)->result();
		
		foreach ($data as $key => $usr) {
			$data_save['user_type'] = 1;
			$data_save['user_id'] =  $usr->user_id;
			$data_save['user_number'] =  $usr->id;
			$data_save['user_name'] =  $usr->name;
			$data_save['user_photo'] =  $usr->recog_photo;
			$data_save['recog_photo'] =  $usr->recog_photo;
			$data_save['recog_time'] =  $usr->recog_time;
			$data_save['body_temperature'] =  $usr->body_temperature;			
			
			$id = $this->m_general->insert_data($data_save,'suhu');
		}
		
		//SEND NOTIF HR			
		$mis_suhu = $this->db->query("select group_concat(QUOTE(user_id)) as val from suhu where user_type=1 and DATE(recog_time) = DATE(CURDATE()) and is_notif=1")->row();		
		
		$temp = '';
		if(isset($mis_suhu->val)){
			$temp = "and a.user_id not in (".$mis_suhu->val.")";
		}
		
		$data = $this->db->query("select a.* from suhu a where DATE(a.recog_time) = DATE(CURDATE()) ".$temp." group by a.user_id HAVING a.body_temperature >= 37.3 and count(a.user_id)>=2")->result();
		//print_r($data);
		if(is_array($data)){
			$receivers = [];
			
			$this->load->model('restapi/user_model');
			$sender = $this->user_model->one_user(['where' => ['_.id_user' => 0]]);
			$sender_name = '';
			$position_name = '';
			$branch_name = '';
			if($sender != null) {
				$sender_name = $sender['name'];
				$position_name = $sender['position_name'];
				$branch_name = $sender['branch_name'];
			}
			
			$users = $this->user_model->many_user(['where_in' => ['bg.id' => 22]]); //HR
			foreach ($users as $key => $user) {
				$receivers[] = $user['id_user'];
			}
			
			foreach ($data as $key => $usr) {
				$data_save['user_type'] = 1;
				$data_save['user_id'] =  $usr->user_id;
				$data_save['user_number'] =  $usr->user_number;
				$data_save['user_name'] =  $usr->user_name;
				$data_save['user_photo'] =  $usr->user_photo;
				$data_save['recog_photo'] =  $usr->recog_photo;
				$data_save['recog_time'] =  $usr->recog_time;
				$data_save['body_temperature'] =  $usr->body_temperature;
				
				$where_id = array('user_id'=>$usr->user_id,'DATE(recog_time) = DATE(CURDATE())'=>null);
				$this->m_general->update_data($where_id,array('is_notif'=>1),'suhu');
				
				$data_parse = $data_save;
				$data_parse['id'] = $usr->id;
				$data_parse['notif_type'] = "suhu";
				
				$this->load->library('Notify_lib');
				$nl = new Notify_lib();
				$nl->send(
					'Suhu Panas Karyawan',
					$sender_name . ' has created new announcement',
					1,
					$receivers,
					$data_parse,
					'suhu',
					true
				);
				
			}
			//SEND NOTIF
		}
	}
	
	public function insert_suhu_karyawan_old(){
		$date_now = date('d-m-y');
		
		$mis_suhu = $this->db->query("select group_concat(QUOTE(user_id)) as val from suhu where user_type=1 and DATE(recog_time) = DATE(CURDATE())")->row();
		
		$otherdb = $this->load->database('third', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
		
		$temp = '';
		if(isset($mis_suhu->val)){
			$temp = "and a.user_id not in (".$mis_suhu->val.")";
		}
		
		$data = $otherdb->query("select a.*,(select id_name from face_user where id=a.user_id) name,COUNT(a.user_id) from face_record a where DATE(a.recog_time) = DATE(CURDATE()) ".$temp." group by a.user_id HAVING a.body_temperature >= 37.3 and count(a.user_id)>=2")->result();
		
		
		$receivers = [];
		
		$this->load->model('restapi/user_model');
		$sender = $this->user_model->one_user(['where' => ['_.id_user' => 0]]);
		$sender_name = '';
		$position_name = '';
		$branch_name = '';
		if($sender != null) {
			$sender_name = $sender['name'];
			$position_name = $sender['position_name'];
			$branch_name = $sender['branch_name'];
		}
		
		foreach ($data as $key => $usr) {
			$data_save['user_type'] = 1;
			$data_save['user_id'] =  $usr->user_id;
			$data_save['user_number'] =  $usr->id;
			$data_save['user_name'] =  $usr->name;
			$data_save['user_photo'] =  $usr->recog_photo;
			$data_save['recog_photo'] =  $usr->recog_photo;
			$data_save['recog_time'] =  $usr->recog_time;
			$data_save['body_temperature'] =  $usr->body_temperature;
			
			$id = $this->m_general->insert_data($data_save,'suhu');
			
			
			$data_parse = $data_save;
			$data_parse['id'] = $id;
			$data_parse['notif_type'] = "suhu";
			//$receivers[] = $usr->id_user;
			
			
			
			//SEND NOTIF HR			
				
				$users = $this->user_model->many_user(['where_in' => ['bg.id' => 22]]); //HR
				foreach ($users as $key => $user) {
					$receivers[] = $user['id_user'];
				}
				
				//$receivers[] = 47;
				
				//$this->load->model('m_receiver');
				//$this->m_receiver->add_receiver($val->id, 'announcement', $receivers);
				
				//$data = $this->M_announcement->one_announcement(['where' => ['_.id' => $val->id]]);
				
				
				
				//$filter = str_replace('<p>&nbsp;</p>','',$data_save['user_name'].' '.$data_save['body_temperature']);
				//$filter = str_replace('<p>','',$filter);
				//$filter = str_replace('</p>','',$filter);
				
				$this->load->library('Notify_lib');
				$nl = new Notify_lib();
				$nl->send(
					'Suhu Panas Karyawan',
					$sender_name . ' has created new announcement',
					0,
					$receivers,
					$data_parse,
					'suhu',
					true
				);
			//SEND NOTIF
		}
		
		
	}
	
	public function check_suhu(){
		$date_now = date('d-m-y');
		// <> 1 menit
		$data = $this->db->query("select * from suhu where DATE(recog_time) = DATE(CURDATE()) and user_type=0 and 
		(CAST(NOW() AS TIME) > SUBTIME(CAST(recog_time AS TIME),'120') and CAST(NOW() AS TIME) < ADDTIME(CAST(recog_time AS TIME),'120')) order by id asc
		")->result();
		
		echo json_encode($data);
	}
	
	public function tes(){
		/*
		$date_now = date('d-m-y');
		
		$mis_suhu = $this->db->query("select group_concat(QUOTE(user_id)) as val from suhu where DATE(recog_time) = DATE(CURDATE())")->row();
		
		$otherdb = $this->load->database('third', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
		
		$temp = '';
		if(isset($mis_suhu->val)){
			$temp = "and a.user_id not in (".$mis_suhu->val.")";
		}
		
		$data = $otherdb->query("select a.*,COUNT(a.user_id) from face_record_stranger a where DATE(a.recog_time) = DATE(CURDATE()) ".$temp." group by a.user_id HAVING a.body_temperature >= 36.3 and count(a.user_id)>=2")->result();
		*/
		
		$otherdb = $this->load->database('third', TRUE);
		$data = $otherdb->query("select a.*,(select id_name from face_user where id=a.user_id) name,COUNT(a.user_id) from face_record a group by a.user_id HAVING a.body_temperature >= 35 and count(a.user_id)>=2")->result();//where DATE(a.recog_time) = DATE(CURDATE())
		//$data = $otherdb->query("select * from face_user limit 20")->result();
		
		echo json_encode($data);
	}

	public function get_suhu(){
		$this->load->model('m_clockin');
        /*alih fungsi untuk dilakukan cronjob external*/
        $ptgl = date('Y-m-d');

        $source = [
            'query_select' => 'sh.*,u.name,u.last_name,u.id_user',
            'master_table' => 'suhu sh',
            'relations' => [
                'users u' => [
                    'conditions' => 'sh.user_id = u.eintell_id',
                    'join_type' => 'inner'
                ]
            ],
        ];
        $param=[
            'sh.recog_time like '=> '%'.$ptgl.'%',
        ];
        $get_data = $this->m_clockin->get_all_data($param,$source)->result();
        if (!empty($get_data)) {
            /*add the process here*/
            /*update clockin*/
            foreach ($get_data as $key => $value) {
                $tglStore = explode(' ',$value->recog_time);
                $tgl = $tglStore[0];
                $userID = $value->id_user;
                $suhu_id = $value->id;
                $where_update = [
                    'id_user' => $userID,
                    'tgl_absen' => $ptgl
                ];
                $data_update = [
                    'suhu' => $value->body_temperature,
                    'suhu_id' => $suhu_id
                ];
                $where_update['suhu_id'] = 0;
                $dd = $this->m_clockin->get_data('clockin_user',$where_update)->result();
                
                unset($where_update['suhu_id']);
                $x = $this->m_clockin->update_data($where_update, $data_update, 'clockin_user');
                $ic_id = isset($dd[0]->id)?$dd[0]->id:0;
                if ($ic_id > 0) {
                	$this->notifSuhu($ic_id,$value->name,$userID);
                }
            }
        }

        echo "well done.";
    }
	
	public function reset_card_status(){
        /*diaktifkan setiap ganti hari*/
        $this->load->model('m_clockin');
        $now = date('Y-m-d');
        $last = $this->m_clockin->get_data('clockin_user',[
                'tgl_absen <'=>$now,
                'card_status not like' => '%expired%'
            ])->result();
        foreach ($last as $key => $value) {
            $setID = $value->id;
            $this->m_clockin->update_data(['id'=>$setID],['card_status'=>'expired'],'clockin_user');
        }
        echo "done";
    }

    public function notifSuhu($clockin_id = '', $nama='', $id_user){
        /*hanya dikirim ke user ybs*/
        $idReceivers = [];
        $idReceivers[] = $id_user;
        $msg = 'Anda sudah melakukan pengecekan suhu, silahkan lakukan verifikasi Clock In.';
        $nl = new Notify_lib();
        $nl->send('Verifikasi Clock In', //title
            $msg, //message
            $id_user, //sender set default to rahmat
            $idReceivers, //list receiver
            ['clockin_id' => $clockin_id],
            "clockin_suhu", //type notifikasi
            true // kirim ke onesignal
        );

        return true;
    }

    public function repeatWeeklyShiftDokter(){
    	/*function automatic create shift dokter*/
    	// cari detail shift dengan hari setelah hari ini
    	// dengan repeat_weekly = on(1)
    	$this->load->model('m_appdokter');
    	$this->load->library('misc');
	 		$dt = $this->m_appdokter->get_where(['repeat_weekly'=>1],'appdokter_shift_detail')->result();
	 		$copyDt = $listOfWeek = [];
	 		foreach ($dt as $key => $v) {
	 			$tgl = $v->tanggal;
	 			$nwd = $this->getNextWeek($tgl);
	 			$copyDt[$key] = $v;
	 			$copyDt[$key]->tanggal = $nwd; 
	 			/*get weeks of the date*/
	 			$listOfWeek[$nwd] = $this->getWeekDate($nwd);

	 		}

	 		$mList = [];
	 		foreach ($listOfWeek as $key => $value) {
	 			$mList = array_merge($mList,$value);
	 		}

	 		$savedData = [];
	 		$listHari = $this->misc->dayEnToId();
	 		$copyDt = json_decode(json_encode($copyDt),true);
	 		foreach ($mList as $key => $tgl) {
	 			foreach ($copyDt as $x => $v) {
	 				if ($v['tanggal'] == $tgl) {
	 					$savedData[$key][$x] = $v;
	 				}else{
	 					$savedData[$key][$x] = [];
	 				}
	 			}
	 		}

	 		$dtShift = [];
	 		$idShift = 0;
	 		$lastShiftDetailId = [];
	 		foreach ($savedData as $key => $value) {
	 			/*ready to store*/
	 			foreach ($value as $k => $v) {
	 				if (!empty($v)) {
	 					unset($v['id']);
	 					$idShift = $v['id_shift'];
	 					$cek = $this->m_appdokter->get_where($v,'appdokter_shift_detail')->result();
	 					if (empty($cek)) {
	 						$v['id_shift_copy']=$idShift;
	 						$sv = $this->m_appdokter->insert_data($v, 'appdokter_shift_detail');
	 						$lastShiftDetailId[] = $sv;
	 					}
	 					unset($mList[$key]);
	 				}
	 			}
	 		}
	 		
	 		/*sisa hari*/
	 		$gkstmp = '';
	 		foreach ($mList as $hari => $tgl) {
	 			$dt = [
					'id_shift' => $idShift,
					'tanggal' => $tgl,
					'hari' => $hari,
					'jam' => '-',
					// 'group_keystamp' => date('YmdHis'),
					'repeat_weekly' => 0,
					'id_shift_copy' => $idShift
	 			];
	 			$cek = $this->m_appdokter->get_where($dt,'appdokter_shift_detail')->result(); 
	 			if (empty($cek)) {
	 				if (empty($gkstmp)) {
	 					$gkstmp = date('YmdHis');	
	 				}
	 				$dt['group_keystamp'] = $gkstmp;
	 				$sv = $this->m_appdokter->insert_data($dt, 'appdokter_shift_detail');	
	 				$lastShiftDetailId[] = $sv;
	 			}
	 			
	 		}
	 		/**/
	 		/**/
	 		$dtShift = $this->m_appdokter->get_where(['id'=>$idShift],'appdokter_shift')->result();
	 		$dtShift = json_decode(json_encode($dtShift),true);
	 		$dtShift = $dtShift[0];
	 		$dtShift['date_create'] = $this->getNextWeek($dtShift['date_create']);
	 		$dtShift['group_keystamp'] = date_format(date_create($dtShift['date_create']),'YmdHis');
	 		unset($dtShift['id']);
	 		$shiftIdNew = $this->m_appdokter->insert_data($dtShift, 'appdokter_shift');	
	 		/**/
	 		/*update id*/
	 		foreach ($lastShiftDetailId as $key => $id) {
	 			$this->m_appdokter->update_data(['id'=>$id], ['id_shift'=>$shiftIdNew], 'appdokter_shift_detail');
	 		}
	 		/**/
    }

    function getNextWeek($tgl){
	    $next_date = new DateTime($tgl); /*Y-m-d*/
	    $next_date->modify('+7 day');
	    $nextWeekDate =  $next_date->format('Y-m-d');

	    return $nextWeekDate;
	  }

		function getWeekDate($parseTgl=''){
      $tgl = $this->input->post('tgl');
      if (empty($tgl) && !empty($parseTgl)) {
        $tgl = $parseTgl;
      }else{
        echo "fail"; exit();
      }
      // $tgl = '2021-01-06';
      $day = date_format(date_create($tgl),'D');
      $listHari = $this->misc->dayEnToId();
      $key = $this->misc->weekIdKeys();

      $selected = $listHari[$day];
      $keySelect = $key[$selected];

      $current_date = new DateTime($tgl);
      $date_before = [];
      $date_after = [];

      /*before*/
      for ($i=1; $i < ($keySelect); $i++) { 
        $current_date->modify('-1 day');
        $add_date =  $current_date->format('Y-m-d');
        $d = date_format(date_create($add_date),'D');
        $hr = $listHari[$d];
        $date_before[$hr] = $add_date;
      }

      /*before*/
      $current_date = new DateTime($tgl);
      for ($i=1; $i < (8-$keySelect); $i++) { 
        $current_date->modify('+1 day');
        $add_date =  $current_date->format('Y-m-d');
        $d = date_format(date_create($add_date),'D');
        $hr = $listHari[$d];
        $date_after[$hr] = $add_date;
      }

      $cur_date = [];
      $d = date_format(date_create($tgl),'D');
      $hr = $listHari[$d];
      $cur_date[$hr] = $tgl;

      $list_week_date = array_merge($date_before,$cur_date,$date_after);
      asort($list_week_date);

      // $rslt = [
      //   'current_day' => $hr,
      //   'current_date' => $tgl,
      //   'list_date' => $list_week_date
      // ];

      return $list_week_date;
    }

	public function get_google_calender($start_date = '',$end_date = ''){
		$start_date = ($start_date == ''?$this->input->get('start_date'):$start_date);
		$end_date = ($end_date == ''?$this->input->get('end_date'):$end_date);
		
		if($start_date=='') $start_date = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( date('Y-m-d')) ) ));
		else $start_date = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $start_date) ) ));
		
		if($end_date=='') $end_date = date('Y-m-d',(strtotime ( '+1 day' , strtotime ( date('Y-m-d')) ) ));
		else $end_date = date('Y-m-d',(strtotime ( '+1 day' , strtotime ( $end_date) ) ));
		
		$ch = curl_init(); 
		
		$endpoint = 'https://www.googleapis.com/calendar/v3/calendars/en.indonesian%23holiday%40group.v.calendar.google.com/events';
		$params = array('key' => 'AIzaSyDcnW6WejpTOCffshGDDb4neIrXVUA1EAE',
						'timeMin' => $start_date.'T00:00:00+07:00',
						'timeMax' => $end_date.'T00:00:00+07:00',
						'singleEvents' => 'true',
						'maxResults' => '9999');
								
		$url = $endpoint . '?' . http_build_query($params);
		//echo $url;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);      
		$arr = json_decode($output);
		$list_date = array();
		foreach($arr->items as $val){
			$list_date[] = array('date'=>$val->start->date,'name'=>$val->summary);
		}
		return $list_date;
	}
	
	public function get_lastday_cuti_melahirkan(){
		$start_date = $this->input->get('start_date');
		$end_date = date('Y-m-d',(strtotime ( '+5 month' , strtotime ( $start_date) ) ));
		
		$list_hari_nasional = $this->get_google_calender($start_date,$end_date);
		
		$list_holiday = array();
		foreach($list_hari_nasional as $v){
			$list_holiday[] = $v['date'];
		}
		
		//print_r($list_holiday);
		
		$x = 1;
		$count_date = $start_date;
		for($i=0;$i<90;$i++){
			$newTime = strtotime('+ '.$x.' days',strtotime($start_date)); 
			//echo $x.' - '.($i+1).' - '.date('Y-m-d', $newTime).'-'.date('N', ($newTime));
			if(date('N', $newTime) == 7)
			{
				$i--;
			}
			else if(in_array(date('Y-m-d', $newTime), $list_holiday))
			{
				$i--;
			}
			else $count_date = date('Y-m-d', $newTime);
			
			$x++;
		}
		//echo $count_date;
		
		echo $count_date;
	}


	public function reminder_checklist_kedoya(){
		$today = date('Y-m-d');
		$receivers = ['47','30','57','62','66'];
		$kedoya = $this->db->query("select count(c.id) as total,c.* from intern_checklist_input c where c.branch_id='2' and c.tanggal = '$today' " )->row();
		
				if($kedoya->total < 1){
					
				$data_parse['id'] = '1';
				$data_parse['notif_type'] = "checklist";
				
				$this->load->library('Notify_lib');
				$nl = new Notify_lib();
				$nl->send(
					'New Checklist Reminder',
					'Cabang kedoya tidak melakukan Checklist Alat',
					1,
					$receivers,
					$data_parse,
					'checklist',
					true
				);
							
							
							
		}
	}

	public function reminder_str(){
		$today = date('Y-m-d');
		$remind = $today ;
		
		$data = $this->db->query("select u.tanggal_str,u.NIP,u.id_user,u.name,u.last_name from users u")->result();

		foreach ($data as $d) {

			//	print_r($d->tanggal_str);
		//		$hitung = date_diff($today, $d->tanggal_str);
				$nama = $d->name.' '.$d->last_name;
				$receivers = ['47','859','67','68','81',$d->id_user];

				$now = date("n");
				$date_expire = date('n', strtotime($d->tanggal_str));
				$hitung = $date_expire - $now;


			//	print_r($date_expire);
			//	die();
				
				if ($hitung == '3') {

						$data_parse['id_user'] = $d->id_user;
						$this->load->library('Notify_lib');
						$nl = new Notify_lib();
						$nl->send(
							'Reminder STR atas nama '.$nama,
							'Mohon segera lakukan perpanjangan dokumen STR atas nama '.$nama.'<br><br> Tanggal STR terdaftar : '.longdate_indo($d->tanggal_str),
							1,
							$receivers,
							$data_parse,
							'str',
							true
						);

					echo('kirim reminder'); 
				}
				else {

					echo('<br>tidak ada data yg cocok<br>');
				}

			//	echo ' <br>Reminder Jalan';

		}
		
		
	}

	public function reminder_sik(){
		$today = date('Y-m-d');
		$remind = $today ;
		
		$data = $this->db->query("select u.tanggal_sik,u.NIP,u.id_user,u.name,u.last_name from users u where YEAR(u.tanggal_sik) = YEAR(CURRENT_DATE())" )->result();

		foreach ($data as $d) {
			$receivers = ['47','859','67','68','81',$d->id_user];
			//	print_r($d->tanggal_str);
		//		$hitung = date_diff($today, $d->tanggal_str);
				$nama = $d->name.' '.$d->last_name;


				
				$now = date("n");
				$date_expire = date('n', strtotime($d->tanggal_sik));
				$hitung = $date_expire - $now;
			//	$hitung = $date->diff($now)->format("%m");
			//	print_r($date_expire - $now);
			//	die();
				if ($hitung == '3' && $hitung=='-3') {

				
						$data_parse['id_user'] = $d->id_user;
						
						$this->load->library('Notify_lib');
						$nl = new Notify_lib();
						$nl->send(
							'Reminder SIK atas nama '.$nama,
							'Mohon segera lakukan perpanjangan dokumen SIK atas nama '.$nama.'<br><br> Tanggal SIK terdaftar : '.longdate_indo($d->tanggal_sik),
							1,
							$receivers,
							$data_parse,
							'sik',
							true
						);

					echo('kirim reminder'); 
				}
				else {

					echo('tidak ada data yg cocok');
				}

				echo 'Reminder Jalan';

		}

		
	}

	}

			


	

