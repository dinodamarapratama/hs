<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kirim_email {

	protected $username = 'mis@biomedika.co.id';
	protected $password = 'biomedika2020';
	protected $email_sender = 'mis@biomedika.co.id';
	protected $email_server = 'mail.biomedika.co.id'; /*ex gmail : tls://smtp.gmail.com*/

	public function __construct(){
		/* initialize */
        require_once APPPATH."third_party/kirim_email/src/PHPMailer.php";
        require_once APPPATH."third_party/kirim_email/src/Exception.php";
        require_once APPPATH."third_party/kirim_email/src/OAuth.php";
        require_once APPPATH."third_party/kirim_email/src/POP3.php";
        require_once APPPATH."third_party/kirim_email/src/SMTP.php";
    }

	function send_email($data=[],$type='default'){
		/*base*/
		/**/
        if ($type=='default') {
        	$this->simple_email($data);
        }else{
        	$this->attachment_email($data);
        }
	}


	function simple_email($cfg=[]){
		$mail = new PHPMailer\PHPMailer\PHPMailer();

        //Enable SMTP debugging.
        // $mail->SMTPDebug = 3;
        $mail->isSMTP();
        $mail->Host = $this->email_server; //host mail server
        $mail->SMTPAuth = true;
        $mail->Username = $this->username;   //nama-email smtp
        $mail->Password = $this->password;   //password email smtp
        $mail->SMTPSecure = "ssl";
        $mail->Port = 465;/*gmail*/

        $mail->From = $this->email_sender; //email pengirim
        $mail->FromName = $cfg['pengirim']; //nama pengirim

        $mail->addAddress($cfg['penerima']); //email penerima

        $mail->isHTML(true);

        $mail->Subject = $cfg['subject']; //subject
        $mail->Body    = $cfg['content']; //isi email
        $mail->AltBody = "MIS Biomedika Mailer System"; //body email

        if(!$mail->send()){
        	// echo "Mailer Error: " . $mail->ErrorInfo;
        	return $mail->ErrorInfo;
        }
        else{
        	// echo "Message has been sent successfully";
        	return true;
        }
	}

	function attachment_email(){

	}

}