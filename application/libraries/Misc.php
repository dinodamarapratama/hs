<?php

class Misc {
 
    function dateFormat($tgl){
        /* convert format dmY ke Ymd dan sebaliknya */
        $tanggal = explode('-',$tgl);
        if (count($tanggal)>1) {
            $new_tgl = $tanggal[2].'-'.$tanggal[1].'-'.$tanggal[0];
        }else{
            $new_tgl = '';
        }
        return $new_tgl;
    }

    function dateFormatIdEn($tgl){
        /* convert format mdY ke dmY dan sebaliknya */
        $tanggal = explode('-',$tgl);
        if (count($tanggal)>1) {
            $new_tgl = $tanggal[1].'-'.$tanggal[0].'-'.$tanggal[2];
        }else{
            $new_tgl = '';
        }
        return $new_tgl;
    }

    function checkIsAValidDate($myDateString){
	    return (bool)strtotime($myDateString);
	}

	function selisih_waktu($akhir,$awal){
        // Declare and define two dates 
        $date1 = strtotime($awal);  
        $date2 = strtotime($akhir);  
          
        // Formulate the Difference between two dates 
        $diff = abs($date2 - $date1);  
          
          
        // To get the year divide the resultant date into 
        // total seconds in a year (365*60*60*24) 
        $years = floor($diff / (365*60*60*24));  
          
          
        // To get the month, subtract it with years and 
        // divide the resultant date into 
        // total seconds in a month (30*60*60*24) 
        $months = floor(($diff - $years * 365*60*60*24) 
                                       / (30*60*60*24));  
          
          
        // To get the day, subtract it with years and  
        // months and divide the resultant date into 
        // total seconds in a days (60*60*24) 
        $days = floor(($diff - $years * 365*60*60*24 -  
                     $months*30*60*60*24)/ (60*60*24)); 
          
          
        // To get the hour, subtract it with years,  
        // months & seconds and divide the resultant 
        // date into total seconds in a hours (60*60) 
        $hours = floor(($diff - $years * 365*60*60*24  
               - $months*30*60*60*24 - $days*60*60*24) 
                                           / (60*60));  
          
          
        // To get the minutes, subtract it with years, 
        // months, seconds and hours and divide the  
        // resultant date into total seconds i.e. 60 
        $minutes = floor(($diff - $years * 365*60*60*24  
                 - $months*30*60*60*24 - $days*60*60*24  
                                  - $hours*60*60)/ 60);

        $seconds = floor(($diff - $years * 365*60*60*24  
         - $months*30*60*60*24 - $days*60*60*24 
                - $hours*60*60 - $minutes*60));  

        $result = [
            'tahun' => $years,
            'bulan' => $months,
            'hari' => $days,
            'jam' => ($hours<10)?'0'.$hours:$hours,
            'menit' => ($minutes<10)?'0'.$minutes:$hours,
            'detik' => ($seconds<10)?'0'.$seconds:$seconds,
            'awal' => $awal,
            'akhir' => $akhir
        ];

        return $result;  
    }

    public function weekDays(){
        $hari = [
            'senin','selasa','rabu','kamis','jumat','sabtu','minggu'
        ];

        return $hari;
    }

    public function weekIdKeys(){
        $key = [
            'senin' => 1,
            'selasa' => 2,
            'rabu' => 3,
            'kamis' => 4,
            'jumat' => 5,
            'sabtu' => 6,
            'minggu' => 7
        ];
        return $key;
    }

    public function dayIdToEn(){
        $day = [
            'senin' => 'Mon',
            'selasa' => 'Tue',
            'rabu' => 'Wed',
            'kamis' => 'Thu',
            'jumat' => 'Fri',
            'sabtu' => 'Sat',
            'minggu' => 'Sun'
        ];
        return $day;
    }

    public function dayEnToId(){
        $hari = [
            'Mon' => 'senin',
            'Tue' => 'selasa',
            'Wed' => 'rabu',
            'Thu' => 'kamis',
            'Fri' => 'jumat',
            'Sat' => 'sabtu',
            'Sun' => 'minggu'
        ];
        return $hari;
    }

    public function dateRange($date1='',$date2=''){
        
        $period = new DatePeriod(
             new DateTime($date1),
             new DateInterval('P1D'),
             new DateTime($date2)
        );

        $list = [];
        $hari = $this->dayEnToId();
        foreach ($period as $key => $value) {
            $list[] = [
                'tgl' => $value->format('Y-m-d'),
                'hari' => $hari[$value->format('D')]
            ];
        }
        $list[] = [
            'tgl' => $date2,
            'hari' => $hari[date_format(date_create($date2),'D')]
        ];

        return $list;
    }

    function getBulan($key=0){
        $bulan = [
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        ];
        
        return !empty($key)?$bulan[$key]:$bulan;
    }

    function getTahun($year=''){
        if (empty($year)) {
            $year = date('Y');
        }
        /* 5 before & after */
        $list_tahun[$year] = $year;
        for ($i=1; $i <= 5; $i++) { 
            $y = intval($year);
            $list_tahun[$y-$i] = $y-$i;
            $list_tahun[$i+$y] = $i+$y;
        }

        asort($list_tahun);

        return $list_tahun;
    }

    function weekOfMonth($date) {
        $firstOfMonth = date("Y-m-01", strtotime($date));
        return intval(date("W", strtotime($date))) - intval(date("W", strtotime($firstOfMonth)));
    }

    function triwulanOfMonth($date){
        $triwulan = [
            1 => [1,2,3],
            2 => [4,5,6],
            3 => [7,8,9],
            4 => [10,11,12]
        ];
        $month = intval(substr($date,5,2));
        $result = 0;
        foreach ($triwulan as $key => $value) {
            $cek = array_search($month, $value);
            if (!empty($cek)) {
                $result = $key;
            }
        }
        return $result;
    }

    function semesterOfMonth($date){
        $triwulan = [
            1 => [1,2,3,4,5,6],
            2 => [7,8,9,10,11,12]
        ];
        $month = intval(substr($date,5,2));
        $result = 0;
        foreach ($triwulan as $key => $value) {
            $cek = array_search($month, $value);
            if (!empty($cek)) {
                $result = $key;
            }
        }
        return $result;
    }

    function getOfficeHour($start = 8, $end = 17, $menit_interval = 0){
        /* assume office hours 8-17 */
        /* interval = 00 -> 60 menit */
        $sub_loop = 1;
        switch ($menit_interval) {
            case '15':
                $sub_loop = 4;
                break;
            case '30':
                $sub_loop = 2;
                break;
            default:
                $sub_loop = 1;
                break;
        }

        $time = [];
        for ($i=$start; $i <= $end ; $i++) { 
            for ($k=$sub_loop; $k >= 1 ; $k--) { 
                $jam = '0'.$i.':';
                $menit = $menit_interval*$k;
                $menit = ($menit==60)?'00':strval($menit);
                $jam = ($i>=10)?$jam = $i.':':$jam;
                $caption = $jam.''.$menit;
                $time[] = $caption;
                if ($jam == $end) {
                    break;
                }
            }
            
        }

        return $time;
    }

    function modify_date($type='+1 months',$tanggal=''/*Y-m-d*/){
        $new_date = false;
        switch ($type) {
            case '+1 months':
            case '+2 months':
            case '+3 months':
            case '+4 months':
            case '+5 months':
            case '+6 months':
            case '+7 months':
            case '+8 months':
            case '+9 months':
            case '+10 months':
            case '+11 months':
            case '+12 months':
                $date = new DateTime($tanggal);
                $date->modify($type);
                $new_date =  $date->format('Y-m-d');
                break;
            default:
                # code...
                break;
        }
        return $new_date;
    }
}