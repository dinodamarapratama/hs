<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* Rest_client.php
* @created on : 20 Nov 2020
* @author [Aby Salim] <abysalim007@gmail.com>
* [artcodingteam.com]
**/
class Rest_client{

	public function __construct() {
		$core =& get_instance();
		$this->core = $core;
		$this->core->load->library('controllerlist');
		$this->core->load->library('jwt');
		$this->security_salt = 'Bio Medika';
	}

	public function responseCode($param){
		$rsp = [
			/*error code*/
			'REQUEST_METHOD_NOT_VALID' => 100,
			'REQUEST_CONTENTTYPE_NOT_VALID' => 101,
			'REQUEST_NOT_VALID' => 102,
			'VALIDATE_PARAMETER_REQUIRED' => 103,
			'VALIDATE_PARAMETER_DATATYPE' => 104,
			'API_NAME_REQUIRED' => 105,
			'API_PARAM_REQUIRED' => 106,
			'API_DOEST_NOT_EXIST' => 107,
			'INVALID_USER_PASS' => 108,
			'USER_NOT_ACTIVE' => 109,
			'SUCCESS_RESPONSE' => 200,
			/*server error*/
			'JWT_PROCESSING_ERROR' => 300,
			'ATHORIZATION_HEADER_NOT_FOUND' => 301,
			'ACCESS_TOKEN_ERRORS' => 302,
			'ATHENTICATION_FAIL' => 302,
			'BAD_REQUEST_URL' => 404,
			'NOT_FOUND' => 404
		];

		return $rsp[$param];
	}

	public function responseError($code, $message) {
		header("content-type: application/json");
		$errorMsg = json_encode(['error' => ['status'=>$code, 'message'=>$message]]);
		echo $errorMsg; die;
	}

	public function responseResult($code, $data) {
		header("content-type: application/json");
		$response = json_encode(['response' => ['status' => $code, "result" => $data]]);
		echo $response; die;
	}

	public function generateToken($user,$pass){
		$paylod = [
			'loginTime' => time(),
			'expTime' => time() + (1*24*60*60), /*one day*/
			'userId' => $user,
			'userPass' => $pass
		];
		$token = $this->core->jwt->encode($paylod, $this->security_salt);
		return $token;
	}

	public function getAuthHeader(){
	    $headers = null;
	    if (isset($_SERVER['Authorization'])) {
	        $headers = trim($_SERVER["Authorization"]);
	    }
	    else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
	        $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
	    } elseif (function_exists('apache_request_headers')) {
	        $requestHeaders = apache_request_headers();
	        // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
	        $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
	        if (isset($requestHeaders['Authorization'])) {
	            $headers = trim($requestHeaders['Authorization']);
	        }
	    }
	    return $headers;
	}

	public function getJwtToken() {
		/*hanya pengecekan token*/
	    $headers = $this->getAuthHeader();
	    if (!empty($headers)) {
	        if (preg_match('/JWT\s(\S+)/', $headers, $matches)) {
	            return $matches[1];
	        }
	    }
	    $this->responseError( $this->responseCode('ATHORIZATION_HEADER_NOT_FOUND'), 'Access Token Not found');
	}

	public function validateToken(){
		$token = $this->getJwtToken();
		$payload = $this->core->jwt->decode($token, $this->security_salt, ['HS256']);
		/*payload = obj{
			'userId',
			'userPass'
			'loginTime',
			'expTime'
		}*/
		return $payload;
	}

	public function validateEndPoint($params=[]){
		$get_module = $this->core->controllerlist->getControllers($params['path']);
		$endPoint = $this->sanitizeMethod($get_module);
		if (in_array($params['url'],$endPoint)) {
			if(in_array($_SERVER['REQUEST_METHOD'], explode(',',$params['allowed_method']))){
				return true;
			}else{
				$this->responseError( $this->responseCode('REQUEST_METHOD_NOT_VALID'),"Jenis request method tidak diijinkan.");
			}
			
		}else{
			$this->responseError( $this->responseCode('BAD_REQUEST_URL'),"URL yang anda tuju tidak ditemukan.");
		}
		
	}

	public function sanitizeMethod($methods=[]){
		$rm = [
			'__get','debug'
		];
		$new_list = [];
		foreach ($methods as $key => $v) {
			foreach ($v as $key => $method) {
				if(!in_array($method, $rm)){
					$new_list[] = $method;
				}
			}
			
		}
		return $new_list;
	}

	public function validateParams($fieldName, $value, $dataType, $required = true) {
		if($required == true && empty($value) == true) {
			$this->responseError( $this->responseCode('VALIDATE_PARAMETER_REQUIRED'), $fieldName . " parameter is required.");
		}

		switch ($dataType) {
			case BOOLEAN:
				if(!is_bool($value)) {
					$this->responseError( $this->responseCode('VALIDATE_PARAMETER_DATATYPE'), "Datatype is not valid for " . $fieldName . '. It should be boolean.');
				}
				break;
			case INTEGER:
				if(!is_numeric($value)) {
					$this->responseError( $this->responseCode('VALIDATE_PARAMETER_DATATYPE'), "Datatype is not valid for " . $fieldName . '. It should be numeric.');
				}
				break;

			case STRING:
				if(!is_string($value)) {
					$this->responseError( $this->responseCode('VALIDATE_PARAMETER_DATATYPE'), "Datatype is not valid for " . $fieldName . '. It should be string.');
				}
				break;
			
			default:
				$this->responseError( $this->responseCode('VALIDATE_PARAMETER_DATATYPE'), "Datatype is not valid for " . $fieldName);
				break;
		}

		return $value;

	}


}
