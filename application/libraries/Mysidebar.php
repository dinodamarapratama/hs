<?php

class Mysidebar {
    private $ci;
    private $id_user;

    
    function __construct(){
        $this->ci =& get_instance();
        $this->id_user = $this->ci->session->userdata('id_user');
    }

    function myMenu(){
        $this->ci->load->model('m_master_menu');
        $myMenus = $this->ci->m_master_menu->getMyMenu($this->id_user);
        return $myMenus;
    }

    // ++
    function build_menu_mobile(){
        $mobile_menu = $this->mobile_menu();

        return $mobile_menu;
    }

    function mobile_menu(){
        $menu_utama_mobile = $this->ParentMenu(); 
        
        $menu_sub_mobile = $this->ChildMenu(); 

        $menu_utama_mobile = json_decode(json_encode($menu_utama_mobile), true);
        $menu_sub_mobile = json_decode(json_encode($menu_sub_mobile), true);

        $RelationMenu = array();
        foreach ($menu_utama_mobile as $key => $value) {
            $RelationMenu[$value['id_menu']] = $value;
            foreach ($menu_sub_mobile as $k => $v) {
                if ($value['id_menu'] == $v['parent']) {
                    $RelationMenu[$value['id_menu']]['childs'][$v['id_menu']] = $v; 
                }
            }
        }

        return $RelationMenu;
    }
    

    function ParentMenu(){
        $this->ci->load->model('m_master_menu');
        $ParentMenu = $this->ci->m_master_menu->getParentMenu($this->id_user);
        return $ParentMenu;
    }
    function ChildMenu(){
        $this->ci->load->model('m_master_menu');
        $ChildMenu = $this->ci->m_master_menu->getChildMenu($this->id_user);
        return $ChildMenu;
    }
    // ++

    function asHtml($menus, $reload) {
        // return $this->asHtml_v1($menus, $reload);
        //return $this->asHtml_v2($menus, $reload);
		return $this->asHtml_v3($menus, $reload);
        // exit();
    }

    function asHtml_v2($menus, $reload) {
        if($reload == true || empty($menus) == true) {
            $menus = $this->myMenu();
        }

        $res_atas = '';
        $res_bawah = '';
        $res = [];
        foreach ($menus as $key => $menu) {
            if($menu->show_in_web == '1') {} else {
                continue;
            }

            if($menu->parent == '0') {
                $r = $this->rec_create_menu_v2($menu, $menus);
                // $res_atas .= $r['atas'];
                // $res_bawah .= $r['bawah'];
                foreach ($r as $key => $val) {
                    if(array_key_exists($key, $res) == false) {
                        $res[$key] = '';
                    }
                    $res[$key] .= $val;
                }
            }
        }
        // return $res_atas . $res_bawah;
        // print_r($res);
        $res_string = '';
        foreach ($res as $key => $val) {
            if($key == '0') {
                $res_string .= '<ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">' . $val  . '</ul>';
            }
            else if($key == '1') {
                $res_string .= '<div class="tab-content custom-menu-content">' . $val . '</div>';
            }
        }

        return $res_string;
    }
	
	function asHtml_v3($menus, $reload) {
        if($reload == true || empty($menus) == true) {
            $menus = $this->myMenu();
        }

        $res_atas = '';
        $res_bawah = '';
        $res = [];
        foreach ($menus as $key => $menu) {
            if($menu->show_in_web == '1') {} else {
                continue;
            }

            if($menu->parent == '0') {
                $r = $this->rec_create_menu_v3($menu, $menus);
                // $res_atas .= $r['atas'];
                // $res_bawah .= $r['bawah'];
                foreach ($r as $key => $val) {
                    if(array_key_exists($key, $res) == false) {
                        $res[$key] = '';
                    }
                    $res[$key] .= $val;
                }
            }
        }
        // return $res_atas . $res_bawah;
         //print_r($res);
        $res_string = '';
        foreach ($res as $key => $val) {
            if($key == '0') {
                // stay
                $res_string .= '<ul class="horizontalMenu-list">' . $val  . '</ul>';
            }
            else if($key == '1') {
                //$res_string .= '<div class="horizontal-megamenu clearfix">' . $val . '</div>';
            }
        }

        return $res_string;
    }

    function rec_create_menu_v2($current, $menus, $level = 0) {
        $level = intval($level);
        $current_url = $this->ci->uri->uri_string();
        $toggle = $current_url == $current->url;

        $hasChildren = false;
        $children = [];

        foreach ($menus as $key => $menu) {
            if($menu->show_in_web == '1') {} else {
                continue;
            }

            if($menu->parent == $current->id_menu){
                $hasChildren = true;
                $children[] = $menu;
            }
        }

        // $res_bawah = '';
        $res = [];

        $icon_text = isset($current->icon_text) ? $current->icon_text : '';
        $icon_class = isset($current->icon_class) ? $current->icon_class : '';


        $url = base_url() . $current->url;
        if($hasChildren == true) {
            $url = 'nav-' . mt_rand();

            foreach ($children as $key => $child) {
                $r = $this->rec_create_menu_v2($child, $menus, $level + 1);
                // echo $level;
                // print_r($r);
                // $atas = $r['res_atas'];
                // $bawah = $r['res_bawah'];
                foreach ($r as $key2 => $val) {
                    if(array_key_exists($key2, $res) == false) {
                        $res[$key2] = '';
                    }
                    // if($key2 == '1') {
                    //     // $res[$key2] .= "<div id=\"$url\" class=\"tab-pane in notika-tab-menu-bg animated flipInX\"><ul class=\"notika-main-menu-dropdown\">$val</ul></div>";
                    //     $res[$key2] .= "<div id=\"$url\" class=\"tab-pane in notika-tab-menu-bg animated flipInX\"><ul class=\"notika-main-menu-dropdown\">$val</ul></div>";
                    // }
                    // else {
                    $res[$key2] .= $val;
                    // }
                    
                }
            }

            foreach ($res as $key => $val) {
                if($key == '1') {
                    $res[$key] = "<div id=\"$url\" class=\"tab-pane in notika-tab-menu-bg animated flipInX\"><ul class=\"notika-main-menu-dropdown\">$val</ul></div>";
                }
            }
        }
        $active = $toggle ? " class='active' " : "";
        if($hasChildren == true){
            $url = '#' . $url;
            $res[$level] = "<li ".$active.">
                                <a href=\"$url\" data-toggle=\"tab\">
                                    <i class=\"$icon_class\">$icon_text</i> $current->nama 
                                </a>
                            </li>";
        }
        else {
            $res[$level] = "<li ".$active.">
                                <a href=\"$url\">
                                    <i class=\"$icon_class\">$icon_text</i> $current->nama 
                                </a>
                            </li>";
        }
        
        

        // return [
        //     'res_atas' => $res_atas,
        //     'res_bawah' => $res_bawah
        // ];
        return $res;
    }

	function rec_create_menu_v3($current, $menus, $level = 0) {
        $level = intval($level);
        $current_url = $this->ci->uri->uri_string();
        $toggle = $current_url == $current->url;

        $hasChildren = false;
        $children = [];

        foreach ($menus as $key => $menu) {
            if($menu->show_in_web == '1') {} else {
                continue;
            }

            if($menu->parent == $current->id_menu){
                $hasChildren = true;
                $children[] = $menu;
            }
        }

        // $res_bawah = '';
        $res = [];

        $icon_text = isset($current->icon_text) ? $current->icon_text : '';
        $icon_class = isset($current->icon_class) ? $current->icon_class : '';


        $url = base_url() . $current->url;
        if($hasChildren == true) {
            $url = 'nav-' . mt_rand();

            foreach ($children as $key => $child) {
                $r = $this->rec_create_menu_v2($child, $menus, $level + 1);
                // echo $level;
                // print_r($r);
                // $atas = $r['res_atas'];
                // $bawah = $r['res_bawah'];
                foreach ($r as $key2 => $val) {
                    if(array_key_exists($key2, $res) == false) {
                        $res[$key2] = '';
                    }
                    // if($key2 == '1') {
                    //     // $res[$key2] .= "<div id=\"$url\" class=\"tab-pane in notika-tab-menu-bg animated flipInX\"><ul class=\"notika-main-menu-dropdown\">$val</ul></div>";
                    //     $res[$key2] .= "<div id=\"$url\" class=\"tab-pane in notika-tab-menu-bg animated flipInX\"><ul class=\"notika-main-menu-dropdown\">$val</ul></div>";
                    // }
                    // else {
                    $res[$key2] .= $val;
                    // }
                    
                }
            }

            foreach ($res as $key => $val) {
                if($key == '1') {
                    //$res[$key] = "<div id=\"$url\" class=\"tab-pane in notika-tab-menu-bg animated flipInX\"><ul class=\"notika-main-menu-dropdown\">$val</ul></div>";
					$res[$key] = "<ul class=\"sub-menu\">$val</ul>";
                }
            }
        }
        $active = $toggle ? " class='active' " : "";
        if($hasChildren == true){
            $url = '#' . $url;
            $res[$level] = "<li aria-haspopup='true' ".$active.">
                                <span class='horizontalMenu-click'><i class='horizontalMenu-arrow fe fe-chevron-down'></i></span>
								<a href=\"#\" class=\"sub-icon\">
                                    <i class=\"$icon_class\">$icon_text</i> $current->nama 
									<i class='fe fe-chevron-down horizontal-icon'></i>
                                </a>
								".$res['1']."
                            </li>";
        }
        else {
            $res[$level] = "<li aria-haspopup='true' ".$active.">
                                <a href=\"$url\">
                                    <i class=\"$icon_class\">$icon_text</i> $current->nama 
                                </a>
                            </li>";
        }
        
        

        // return [
        //     'res_atas' => $res_atas,
        //     'res_bawah' => $res_bawah
        // ];
        return $res;
    }

    function asHtml_v1($menus, $reload){
        if($reload == true || empty($menus)) {
            $menus = $this->myMenu();
        }
        // print_r($menus);
        // exit();
        $res = '';
        foreach ($menus as $key => $menu) {
            if($menu->show_in_web == '1') {} else {
                continue;
            }
            if($menu->parent == '0'){
                $r = $this->rec_create_menu_v1($menu, $menus);
                $res .= $r['html'];
            }            
        }
        // echo $res;
        // exit();
        return $res;
    }

    function rec_create_menu_v1($current, $all) {
        $current_url = $this->ci->uri->uri_string();
        $toggle = $current_url == $current->url;

        $hasChildren = false;
        $children = [];
        foreach ($all as $key => $menu) {
            if($menu->show_in_web == '1') {} else {
                continue;
            }
            if($menu->parent == $current->id_menu) {
                $hasChildren = true;
                $children[] = $menu;
            }
        }

        $res = '';
        $icon_text = isset($current->icon_text) ? $current->icon_text : '';
        $icon_class = isset($current->icon_class) ? $current->icon_class : 'material-icons';
        if($hasChildren == false) {
            $res = '<li class="' . ( $toggle ? 'active' : '' ) . '">
                        <a href="' . base_url() . $current->url  . '">
                            <i class="' . $icon_class . '">' . $icon_text . '</i>
                            <span>' . $current->nama . '</span>
                        </a>
                    </li>';
        } else {
            $children_html = '';
            $toggle_parent = false;
            foreach ($children as $key => $menu) {
                $r = $this->rec_create_menu_v1($menu, $all);
                $children_html .= $r['html'];
                $toggle_parent = $toggle_parent || $r['toggle'];
            }
            $res = '<li class="' . ( $toggle_parent ? 'active' : '' ) . '">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="' . $icon_class . '">' . $icon_text . '</i>
                            <span>' . $current->nama . '</span>
                        </a>
                        <ul class="ml-menu">' . $children_html . '</ul>
                    </li>';
        }
        // return $res;
        return [
            'toggle' => $toggle,
            'html' => $res
        ];
    }
}