<?php

class Notify_lib {

    protected $ci = null;

    function __construct(){
        $this->ci =& get_instance();
    }

    function send($title, $msg, $sender_id, $receiver_ids, $data, $notif_type, $send_onesignal = false) {
        $result = [];
        /** insert ke table notifications */
        $notif_data = $data;
        $notif_data['notif_type'] = $notif_type;
        $template_key = "onesignal_template_id";
        $web_url = '';
        $cuti_flag = 0;
		$cronjob_flag = 0;
		$cron_link = "https://mis.biomedika.co.id/";

        if($notif_type == "dailyreport") {
            $notif_data["dailyreports_id"] = $notif_data["dailyreport_id"] = empty($data["id"]) ? "" : $data["id"];
            $template_key = "onesignal_dailyreport_template_id";
            $web_url = 'admin/dailyreports?notif=' . $data['id'];
        }
        else if($notif_type == 'dailyreport_reply'){
            $notif_data["dailyreports_id"] = $notif_data["dailyreport_id"] = empty($data["id"]) ? "" : $data["id"];
            $template_key = "onesignal_dailyreport_template_id";
            $web_url = 'admin/dailyreports?notif=' . $data['id'];
        }  else if($notif_type == "qcreport") {
            $notif_data["qcreports_id"] = $notif_data["qcreport_id"] = empty($data["id"]) ? "" : $data["id"];
            $web_url = 'admin/qcreports?notif=' . $data['id'];
        } else if($notif_type == 'qcreport_reply'){
            $notif_data["qcreports_id"] = $notif_data["qcreport_id"] = empty($data["id"]) ? "" : $data["id"];
            $web_url = 'admin/qcreports?notif=' . $data['id'];
        }
        else if($notif_type == "complaint") {
            $notif_data["complaint_id"] = empty($data["id"]) ? "" : $data["id"];
            $template_key = "onesignal_complaint_template_id";
            $web_url = 'admin/complaint?notif=' . $data['id'];
        }
        else if($notif_type == 'generalreports'){
            $notif_data["generalreports_id"] = empty($data["id"]) ? "" : $data["id"];
            $web_url = 'admin/generalreports?notif=' . $data['id'];
        }
        else if($notif_type == 'monthlyreports'){
            $notif_data["monthlyreports_id"] = empty($data["id"]) ? "" : $data["id"];
            $web_url = 'admin/monthlyreports?notif=' . $data['id'];
        }
        else if($notif_type == 'techreports'){
            $notif_data["techreports_id"] = empty($data["id"]) ? "" : $data["id"];
            $web_url = 'admin/techreports?notif=' . $data['id'];
        }
        else if($notif_type == 'itreports'){
            $notif_data["itreports_id"] = empty($data["id"]) ? "" : $data["id"];
            $web_url = 'admin/itreports?notif=' . $data['id'];
        }
		else if($notif_type == 'reqdoc'){
            $notif_data["doc_id"] = empty($data["id"]) ? "" : $data["id"];
            $web_url = 'admin/doc?notif=' . $data['id'];
        }
       else if($notif_type == 'qc_result') {
           $notif_data['qc_result_id'] = empty($data['id']) ? '' : $data['id'];
           $web_url = 'admin/qc?notif=' . $data['id'];
       }
       else if($notif_type == 'qc_base_result') {
           $notif_data['qc_result_id'] = empty($data['id']) ? '' : $data['id'];
           $web_url = 'admin/qc?notif=' . $data['id'];
       }
       else if($notif_type == 'reqcuti') {
           $notif_data['id_cuti'] = empty($data['id']) ? '' : $data['id'];
           $web_url = 'admin/reqcuti?notif=' . $data['id'];
           $cuti_flag = 1;
       }
	   else if($notif_type == 'reqizin') {
           $notif_data['id_izin'] = empty($data['id']) ? '' : $data['id'];
           $web_url = 'admin/reqizin?notif=' . $data['id'];
           $cuti_flag = 1;
       }
	    else if($notif_type == 'karyawan1year') {
           $notif_data['id'] = empty($data['id']) ? '' : $data['id'];
           $web_url = 'admin/hr?id=' . $data['id'];
           $cuti_flag = 1;
       }
	   else if($notif_type == 'karyawan2year') {
           $notif_data['id'] = empty($data['id']) ? '' : $data['id'];
           $web_url = 'admin/hr?id=' . $data['id'];
           $cuti_flag = 1;
       }
       else if($notif_type == 'announcement') {
           $notif_data['id_announcement'] = empty($data['id']) ? '' : $data['id'];
           $web_url = 'admin/announcement?notif=' . $data['id'];
		      $cronjob_flag = 1;
       }
      elseif ($notif_type == 'checklist') {
           $notif_data['id_intern_checklist'] = empty($data['id']) ? '' : $data['id'];
           $web_url = 'admin/intern_checklist?notif=' . $data['id'];
           
       }
       elseif ($notif_type == 'str') {
        $notif_data['id_user'] = empty($data['id_user']) ? '' : $data['id_user'];
        $cuti_flag = 1;
        $web_url = 'admin/hr_data?id=' . $data['id_user'];
        
    }
    elseif ($notif_type == 'sik') {
        $notif_data['id_user'] = empty($data['id_user']) ? '' : $data['id_user'];
        $cuti_flag = 1;
        $web_url = 'admin/hr_data?id=' . $data['id_user'];
        
    }
       elseif ($notif_type == 'clockin' || $notif_type == 'clockin_suhu') {
           $notif_data['clockin_id'] = empty($data['clockin_id']) ? '' : $data['clockin_id'];
       }
       elseif ($notif_type == 'overtime') {
           $notif_data['sp_id'] = empty($data['sp_id']) ? '' : $data['sp_id'];
           $notif_data['sp_receiver_id'] = empty($data['sp_receiver_id']) ? '' : $data['sp_receiver_id'];
           $web_url = 'admin/overtime?notif=' . $data['sp_id'];
           $cuti_flag = 1;
       }
       elseif ($notif_type == 'finance') {
        $notif_data['sp_id'] = empty($data['sp_id']) ? '' : $data['sp_id'];
        $notif_data['sp_receiver_id'] = empty($data['sp_receiver_id']) ? '' : $data['sp_receiver_id'];
        $web_url = 'admin/reqovertime?notif=' . $data['sp_id'];
        $cuti_flag = 1;
      }
      elseif ($notif_type == 'generalhousekeepingreports') {
        $notif_data["generalhousekeepingreports_id"] = empty($data["id"]) ? "" : $data["id"];
        $web_url = 'admin/generalhousekeepingreports?notif=' . $data['id'];
      } elseif ($notif_type == 'bookinghs') {
        $notif_data["id"] = empty($data["id"]) ? "" : $data["id"];
        $web_url = 'admin/homeservice?notif=' . $data['id'].'&date='.$data['date'].'&branch_id='.$data['branch_id'].'&notif_type='.'bookinghs';
      }
       else {
           if(empty($data['id']) == false) {
               $notif_data['id'] = $data['id'];
           }
       }

        if(is_array($receiver_ids) == false) {
            $receiver_ids = [];
        }

        $added = [];
        foreach($receiver_ids as $receiver_id) {
            if(in_array($receiver_id, $added)) {
                continue;
            }
            if(empty($receiver_id)) {
                continue;
            }

            $ext_id_receiver = explode('hrd_',$receiver_id);
            $read = 0;
            if (count($ext_id_receiver) > 1) {
                $receiver_id = str_replace('hrd_','',$receiver_id);
                $read = 1;
                $notif_data['hrd'] = 1;
            }else{
                $notif_data['hrd'] = 0;
            }
            $added[] = $receiver_id;

            $r = $this->ci->db->insert("notifications", [
                "from_id" => $sender_id,
                "to_id" => $receiver_id,
                "description" => $msg,
                "data" => json_encode($notif_data),
                "time" => date("Y-m-d H:i:s"),
                "read" => $read,
                "cuti_flag" => $cuti_flag
            ]);

            $r = $this->ci->db->insert("admin_mis.notifications", [
                "from_id" => $sender_id,
                "to_id" => $receiver_id,
                "description" => $msg,
                "data" => json_encode($notif_data),
                "time" => date("Y-m-d H:i:s"),
                "read" => $read,
                "cuti_flag" => $cuti_flag
            ]);

            if ($notif_type == 'reqcuti') {
                $id_cuti = empty($data['id']) ? '' : $data['id'];
                $id_notif = $this->ci->db->insert_id();
                $this->ci->db->insert("cuti_notif", ['id_cuti'=>$id_cuti,'id_notif'=>$id_notif]);
                $this->ci->db->insert("admin_mis.cuti_notif", ['id_cuti'=>$id_cuti,'id_notif'=>$id_notif]);
            }
        }

        /** kirim notif ke server onesignal, untuk push notification */
        $onesignals = [];
        if(count($receiver_ids) > 0) {
            $onesignals = $this->ci->db->from("onesignal_ids")
                ->where_in("user_id", $receiver_ids)
                ->get()->result_array();
        }

        $player_id = [];
        foreach($onesignals as $os) {
            if($os["player_id"] == "null" || $os["player_id"] == null) continue;
            $player_id[] = $os["player_id"];
        }
		
        if(count($player_id) > 0 && $send_onesignal == true) {
        // if(count($player_id) > 0 && $send_onesignal == false) {
            $heading = [
                "en" => $title
            ];

            $content = [
                "en" => $msg
            ];

            // $body__ = $data;
            $body__ = $notif_data;
            $body__["notif_type"] = $notif_type;

            $body_ = [
                // 'template_id' => $this->ci->config->item($template_key),
				// 'app_id' => "1d187d53-37dd-4631-a552-3ef3e9ec0d3f",
                // 'app_id' => $this->ci->config->item('onesignal_app_id_web_dev'),
                'app_id' => $this->ci->config->item('onesignal_app_id'),
				// 'include_player_ids' => ['cc266acf-e70e-45da-922c-b154e1f45bd7'],
                'include_player_ids' => $player_id,
				'data' => $body__,
                'web_url' => (($cronjob_flag==0)?base_url() . $web_url:$cron_link . $web_url),
                'url' => null,
				'headings' => $heading,
				'contents' => $content,
                'android_channel_id' => 'b0b0cf76-853a-4ac4-a217-d3a459f2ab3f', // dino
                // 'android_channel_id' => '042a2bdf-cd80-4949-a247-0971a9180029', // rahmat
                'ios_sound' => 'mis_notif.wav'
            ];;
            $body_ = json_encode($body_);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body_);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

            $response = curl_exec($ch);
            curl_close($ch);

            // var_dump($response);
            $result[] = $response;
            // print_r($response);

            // $r = json_decode($response, true);
            // if(empty($r) == false) {
            //     if(array_key_exists('errors', $r)) {
            //         $errors = $r['errors'];
            //         if(array_key_exists('invalid_player_ids', $errors)) {
            //             $invalid_ids = $errors['invalid_player_ids'];
            //             foreach ($invalid_ids as $key => $player_id) {
            //                 $this->ci->db->where(['player_id' => $player_id])->delete('onesignal_ids');
            //                 $this->ci->db->where(['player_id' => $player_id])->delete('admin_mis.onesignal_ids');
            //             }
            //         }
            //     }
            // }
        }

        return $result;
    }

    function mark_read($to_id, $type, $id) {
        // echo "marking read for receiver $to_id with type of $type and id data is $id ";
        $r = $this->ci->db->from("notifications")
            ->where([
                "to_id" => $to_id
            ])
            ->group_start()
                ->or_where(["read" => null])
                ->or_where(["read" => 0])
            ->group_end()
            ->get()->result_array();

        // echo "list of notification: ";
        // print_r($r);

        foreach ($r as $key => $row) {
            $data = json_decode($row["data"], true);
            if(is_array($data) == false){
                $data = [];
            }
            if($type == "complaint") {
                if(isset($data["complaint_id"]) && $data["complaint_id"] == $id) {
                    $this->mark_read_2($row["id"]);
                    // break;
                }
            } else if($type == "dailyreport") {
                if(isset($data["dailyreports_id"]) && $data["dailyreports_id"] == $id) {
                    $this->mark_read_2($row["id"]);
                    // break;
                } else if(isset($data["dailyreport_id"]) && $data["dailyreport_id"] == $id) {
                    $this->mark_read_2($row["id"]);
                    // break;
                }
            }else if($type == "qcreport") {
                if(isset($data["qcreports_id"]) && $data["qcreports_id"] == $id) {
                    $this->mark_read_2($row["id"]);
                    // break;
                } else if(isset($data["qcreport_id"]) && $data["qcreport_id"] == $id) {
                    $this->mark_read_2($row["id"]);
                    // break;
                }
            } else if($type == "generalreports") {
                if(isset($data["generalreports_id"]) && $data["generalreports_id"] == $id) {
                    $this->mark_read_2($row["id"]);
                }
            } else if($type == "monthlyreports") {
                if(isset($data["monthlyreports_id"]) && $data["monthlyreports_id"] == $id) {
                    $this->mark_read_2($row["id"]);
                }
            }
            else if($type == "techreports") {
                if(isset($data["techreports_id"]) && $data["techreports_id"] == $id) {
                    $this->mark_read_2($row["id"]);
                }
            }
            else if($type == "itreports") {
                if(isset($data["itreports_id"]) && $data["itreports_id"] == $id) {
                    $this->mark_read_2($row["id"]);
                }
            }
           else if($type == 'qc_result') {
               if(isset($data['id']) && $data['id'] == $id) {
                   $this->mark_read_2($row['id']);
               }
           }
           else if($type == 'qc_base_result') {
               if(isset($data['id']) && $data['id'] == $id) {
                   $this->mark_read_2($row['id']);
               }
           }
		      else if($type == 'announcements') {
               if(isset($data['id']) && $data['id'] == $id) {
                   $this->mark_read_2($row['id']);
               }
           }
           else if($type == 'clockin' || $type == 'clockin_suhu') {
               if(isset($data['clockin_id']) && $data['clockin_id'] == $id) {
                   $this->mark_read_2($row['id']);
               }
           }
        }
    }

    function mark_read_2($id) {
        $this->ci->db->where([
                "id" => $id
            ])->update("notifications", ["read" => 1]);
        $this->ci->db->where([
                "id" => $id
            ])->update("admin_mis.notifications", ["read" => 1]);
        
    }


    function mark_all_read($to_id) {
        $this->ci->db->where([
                "to_id" => $to_id
            ])->update("notifications", ["read" => 1]);
        $this->ci->db->where([
                "to_id" => $to_id
            ])->update("admin_mis.notifications", ["read" => 1]);
    }

    function deleteNotifV1($params,$receiver_id){
      /*data*/
      /**
        - param format = [
          [flag => value],
        ]
      */
      $query = 'select * from `notifications` ';
      $query .= 'where `to_id`='.$receiver_id;

      $pLike = [];
      foreach ($params as $key => $obj) {
        if (is_string($obj['value'])) {
          $obj['value'] = '"'.$obj['value'].'"';
        }
        $line = '"'.$obj['flag'].'":'.$obj['value'];
        $query .= ' AND `data` like \'%'.$line.'%\'';
      }
      $exe = $this->ci->db->query($query);
      $d = $exe->result();
      /*deleting data*/
      $qdel = 'delete from `notifications` where `id`='.(!empty($d)?$d[0]->id:0);
      $exe = $this->ci->db->query($qdel);
      $qdel = 'delete from `admin_mis.notifications` where `id`='.(!empty($d)?$d[0]->id:0);
      $exe = $this->ci->db->query($qdel);
      return [
        'sql' => $query,
        'data' => $qdel
      ];
      // return true;
    }
}
