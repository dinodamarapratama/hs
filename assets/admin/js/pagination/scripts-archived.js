"use strict";

var numberOfItemsArchived = $("#archived .list-group").length; // Get total number of the items that should be paginated
var limitPerPageArchived = 15; // Limit of items per each archived
$("#archived .list-group:gt(" + (limitPerPageArchived - 1) + ")").hide(); // Hide all items over archived limits (e.g., 5th item, 6th item, etc.)
var totalPagesArchived = Math.ceil(numberOfItemsArchived / limitPerPageArchived); // Get number of archiveds
$(".pagination-archived").append(
  "<li class='current-archived active'><a href='javascript:void(0)'>" +
    1 +
    "</a></li>"
); // Add first archived marker

// Loop to insert archived number for each sets of items equal to archived limit (e.g., limit of 4 with 20 total items = insert 5 archiveds)
for (var i = 2; i <= totalPagesArchived; i++) {
  $(".pagination-archived").append(
    "<li class='current-archived'><a href='javascript:void(0)'>" +
      i +
      "</a></li>"
  ); // Insert archived number into pagination-archived tabs
}

// Add next button after all the archived numbers
$(".pagination-archived").append(
  "<li id='next-archived'><a href='javascript:void(0)' aria-label=Next><span aria-hidden=true>Next</span></a></li>"
);

// Function that displays new items based on archived number that was clicked
$(".pagination-archived li.current-archived").on("click", function() {
  // Check if archived number that was clicked on is the current archived that is being displayed
  if ($(this).hasClass("active")) {
    return false; // Return false (i.e., nothing to do, since user clicked on the archived number that is already being displayed)
  } else {
    var currentPageArchived = $(this).index(); // Get the current archived number
    $(".pagination-archived li").removeClass("active"); // Remove the 'active' class status from the archived that is currently being displayed
    $(this).addClass("active"); // Add the 'active' class status to the archived that was clicked on
    $("#archived .list-group").hide(); // Hide all items in loop, this case, all the list groups
    var grandTotalArchived = limitPerPageArchived * currentPageArchived; // Get the total number of items up to the archived number that was clicked on

    // Loop through total items, selecting a new set of items based on archived number
    for (var i = grandTotalArchived - limitPerPageArchived; i < grandTotalArchived; i++) {
      $("#archived .list-group:eq(" + i + ")").show(); // Show items from the new archived that was selected
    }
  }
});

// Function to navigate to the next archived when users click on the next-archived id (next archived button)
$("#next-archived").on("click", function() {
  var currentPageArchived = $(".pagination-archived li.active").index(); // Identify the current active archived
  // Check to make sure that navigating to the next archived will not exceed the total number of archiveds
  if (currentPageArchived === totalPagesArchived) {
    return false; // Return false (i.e., cannot navigate any further, since it would exceed the maximum number of archiveds)
  } else {
    currentPageArchived++; // Increment the archived by one
    $(".pagination-archived li").removeClass("active"); // Remove the 'active' class status from the current archived
    $("#archived .list-group").hide(); // Hide all items in the pagination-archived loop
    var grandTotalArchived = limitPerPageArchived * currentPageArchived; // Get the total number of items up to the archived that was selected

    // Loop through total items, selecting a new set of items based on archived number
    for (var i = grandTotalArchived - limitPerPageArchived; i < grandTotalArchived; i++) {
      $("#archived .list-group:eq(" + i + ")").show(); // Show items from the new archived that was selected
    }

    $(".pagination-archived li.current-archived:eq(" + (currentPageArchived - 1) + ")").addClass(
      "active"
    ); // Make new archived number the 'active' archived
  }
});

// Function to navigate to the previous archived when users click on the previous-archived id (previous archived button)
$("#previous-archived").on("click", function() {
  var currentPageArchived = $(".pagination-archived li.active").index(); // Identify the current active archived
  // Check to make sure that users is not on archived 1 and attempting to navigating to a previous archived
  if (currentPageArchived === 1) {
    return false; // Return false (i.e., cannot navigate to a previous archived because the current archived is archived 1)
  } else {
    currentPageArchived--; // Decrement archived by one
    $(".pagination-archived li").removeClass("active"); // Remove the 'activate' status class from the previous active archived number
    $("#archived .list-group").hide(); // Hide all items in the pagination-archived loop
    var grandTotalArchived = limitPerPageArchived * currentPageArchived; // Get the total number of items up to the archived that was selected

    // Loop through total items, selecting a new set of items based on archived number
    for (var i = grandTotalArchived - limitPerPageArchived; i < grandTotalArchived; i++) {
      $("#archived .list-group:eq(" + i + ")").show(); // Show items from the new archived that was selected
    }

    $(".pagination-archived li.current-archived:eq(" + (currentPageArchived - 1) + ")").addClass(
      "active"
    ); // Make new archived number the 'active' archived
  }
});

