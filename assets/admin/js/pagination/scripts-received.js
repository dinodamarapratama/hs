"use strict";

var numberOfItemsReceived = $("#received .list-group").length; // Get total number of the items that should be paginated
var limitPerPageReceived = 15; // Limit of items per each received
$("#received .list-group:gt(" + (limitPerPageReceived - 1) + ")").hide(); // Hide all items over received limits (e.g., 5th item, 6th item, etc.)
var totalPagesReceived = Math.ceil(5); // Get number of receiveds
$(".pagination-received").append(
  "<li class='current-received active'><a href='javascript:void(0)'>" +
    1 +
    "</a></li>"
); // Add first received marker

// Loop to insert received number for each sets of items equal to received limit (e.g., limit of 4 with 20 total items = insert 5 receiveds)
for (var i = 2; i <= totalPagesReceived; i++) {
  $(".pagination-received").append(
    "<li class='current-received'><a href='javascript:void(0)'>" +
      i +
      "</a></li>"
  ); // Insert received number into pagination-received tabs
}

// Add next button after all the received numbers
$(".pagination-received").append(
  "<li id='next-received'><a href='javascript:void(0)' aria-label=Next><span aria-hidden=true>Next</span></a></li>"
);

// Function that displays new items based on received number that was clicked
$(".pagination-received li.current-received").on("click", function() {
  // Check if received number that was clicked on is the current received that is being displayed
  if ($(this).hasClass("active")) {
    return false; // Return false (i.e., nothing to do, since user clicked on the received number that is already being displayed)
  } else {
    var currentPageReceived = $(this).index(); // Get the current received number
    $(".pagination-received li").removeClass("active"); // Remove the 'active' class status from the received that is currently being displayed
    $(this).addClass("active"); // Add the 'active' class status to the received that was clicked on
    $("#received .list-group").hide(); // Hide all items in loop, this case, all the list groups
    var grandTotalReceived = limitPerPageReceived * currentPageReceived; // Get the total number of items up to the received number that was clicked on

    // Loop through total items, selecting a new set of items based on received number
    for (var i = grandTotalReceived - limitPerPageReceived; i < grandTotalReceived; i++) {
      $("#received .list-group:eq(" + i + ")").show(); // Show items from the new received that was selected
    }
  }
});

// Function to navigate to the next received when users click on the next-received id (next received button)
$("#next-received").on("click", function() {
  var currentPageReceived = $(".pagination-received li.active").index(); // Identify the current active received
  // Check to make sure that navigating to the next received will not exceed the total number of receiveds
  if (currentPageReceived === totalPagesReceived) {
    return false; // Return false (i.e., cannot navigate any further, since it would exceed the maximum number of receiveds)
  } else {
    currentPageReceived++; // Increment the received by one
    $(".pagination-received li").removeClass("active"); // Remove the 'active' class status from the current received
    $("#received .list-group").hide(); // Hide all items in the pagination-received loop
    var grandTotalReceived = limitPerPageReceived * currentPageReceived; // Get the total number of items up to the received that was selected

    // Loop through total items, selecting a new set of items based on received number
    for (var i = grandTotalReceived - limitPerPageReceived; i < grandTotalReceived; i++) {
      $("#received .list-group:eq(" + i + ")").show(); // Show items from the new received that was selected
    }

    $(".pagination-received li.current-received:eq(" + (currentPageReceived - 1) + ")").addClass(
      "active"
    ); // Make new received number the 'active' received
  }
});

// Function to navigate to the previous received when users click on the previous-received id (previous received button)
$("#previous-received").on("click", function() {
  var currentPageReceived = $(".pagination-received li.active").index(); // Identify the current active received
  // Check to make sure that users is not on received 1 and attempting to navigating to a previous received
  if (currentPageReceived === 1) {
    return false; // Return false (i.e., cannot navigate to a previous received because the current received is received 1)
  } else {
    currentPageReceived--; // Decrement received by one
    $(".pagination-received li").removeClass("active"); // Remove the 'activate' status class from the previous active received number
    $("#received .list-group").hide(); // Hide all items in the pagination-received loop
    var grandTotalReceived = limitPerPageReceived * currentPageReceived; // Get the total number of items up to the received that was selected

    // Loop through total items, selecting a new set of items based on received number
    for (var i = grandTotalReceived - limitPerPageReceived; i < grandTotalReceived; i++) {
      $("#received .list-group:eq(" + i + ")").show(); // Show items from the new received that was selected
    }

    $(".pagination-received li.current-received:eq(" + (currentPageReceived - 1) + ")").addClass(
      "active"
    ); // Make new received number the 'active' received
  }
});

