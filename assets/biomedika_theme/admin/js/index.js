$(function() {
	
	
	/* Chartjs (#project-budget) opened */
	var canvas = document.getElementById("project-budget");
		var areaData = {
			labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug"],
			datasets: [{
			data: [0, 150, 65, 160, 70, 130, 70, 120],
			backgroundColor: [
				'rgba(251, 73, 102,.5)'
			],
			borderColor: [
				'rgba(251, 73, 102,.8)'
			],
			borderWidth: 2,
			fill: 'origin',
			label: "total budget"
		},
        {
            data: [50, 90, 210, 90, 150, 75, 200, 70],
            backgroundColor: [
              'rgba(56, 88, 249,.65)'
            ],
            borderColor: [
              'rgba(56, 88, 249,.7)'
            ],
            borderWidth: 2,
            fill: 'origin',
            label: "amount"
          }
        ]
      };
      var areaOptions = {
        responsive: true,
        maintainAspectRatio: true,
        scales: {
			xAxes: [{
				display: true,
				ticks: {
					display: true,
					fontColor: "#8e9cad",
					fontSize: "13",
				},
				gridLines: {
					display: true,
					drawBorder: false,
					color: 'rgb(193, 184, 184,0.2)',
					zeroLineColor: '#000'
				}
			}],
			yAxes: [{
            display: false,
            ticks: {
				display: true,
				autoSkip: false,
				maxRotation: 0,
				stepSize: 15,
				min: 0,
				max: 250
            }
          }]
        },
        legend: {
          display: false
        },
        tooltips: {
          enabled: true
        },
        elements: {
          line: {
            tension: .3
          },
          point: {
            radius: 0
          }
        }
      }
   
	
	/* Chartjs (#project-budget) closed */
	
	
	/* sparkeline chart */
	
	$('#sparkel1').sparkline('html', {
		lineColor: '#3858f9',
		lineWidth: 1.5,
		spotColor: false,
		minSpotColor: false,
		maxSpotColor: false,
		highlightSpotColor: null,
		highlightLineColor: null,
		fillColor: 'rgba(40, 92, 247, 0.09) ',
		chartRangeMin: 0,
		chartRangeMax: 10,
		width: '100%',
		height: 30,
		disableTooltips: true
	});
	$('#sparkel2').sparkline('html', {
		lineColor: '#f09819',
		lineWidth: 1.5,
		spotColor: false,
		minSpotColor: false,
		maxSpotColor: false,
		highlightSpotColor: null,
		highlightLineColor: null,
		fillColor: 'rgba(240, 152, 25, 0.09) ',
		chartRangeMin: 0,
		chartRangeMax: 10,
		width: '100%',
		height: 30,
		disableTooltips: true
	});
	$('#sparkel3').sparkline('html', {
		lineColor: '#3cba92',
		lineWidth: 1.5,
		spotColor: false,
		minSpotColor: false,
		maxSpotColor: false,
		highlightSpotColor: null,
		highlightLineColor: null,
		fillColor: 'rgba(60, 186, 146, 0.09) ',
		chartRangeMin: 0,
		chartRangeMax: 10,
		width: '100%',
		height: 30,
		disableTooltips: true
	});
	
	/* Chartjs (#chartDonut) */
	
	
  
	/* Chartjs (#chartDonut) closed */

	/* p-scroll */
	
   
   // Header-fixed
	$(window).on("scroll", function() {
		if($(window).scrollTop() > 50) {
			$(".main-header").addClass("header-sticky");
		} else {
			//remove the background property so it comes transparent again (defined in your css)
		   $(".main-header").removeClass("header-sticky");
		}
	});

});