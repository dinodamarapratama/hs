﻿var today2 = new Date();
var currentMonth2 = today2.getMonth();
var currentYear2 = today2.getFullYear();
var selectYear2 = document.getElementById("yearCal_izin");
var selectMonth2 = document.getElementById("monthCal_izin");

var months2 = [];
var selectedDates2 = [];
var years2 = [];

// parameters to be set for the datepicker to run accordingly
var minYear2 = 2017;
var maxYear2 = 2037;
var startMonth2 = 0;
var endMonth2 = 11;
var highlightToday2 = true;
var dateSeparator2 = ', ';

// constants that would be used in the script
const dictionaryMonth2 =
    [
        ["Jan", 0],
        ["Feb", 1],
        ["Mar", 2],
        ["Apr", 3],
        ["May", 4],
        ["Jun", 5],
        ["Jul", 6],
        ["Aug", 7],
        ["Sep", 8],
        ["Oct", 9],
        ["Nov", 10],
        ["Dec", 11]
    ];

//this class will add a background to the selected date
const highlightClass2 = 'highlight';

$(document).ready(function (e) {
    today2 = new Date();
    currentMonth2 = today2.getMonth();
    currentYear2 = today2.getFullYear();
    selectYear2 = document.getElementById("yearCal_izin");
    selectMonth2 = document.getElementById("monthCal_izin");
    loadControl2(currentMonth2, currentYear2);
});

function nextCal2() {
    currentYear2 = currentMonth2 === 11 ? currentYear2 + 1 : currentYear2;
    currentMonth2 = currentMonth2 + 1 % 12;
	console.log(currentMonth2+currentYear2);
    loadControl2(currentMonth2, currentYear2);
}

function previousCal2() {
    currentYear2 = currentMonth2 === 0 ? currentYear2 - 1 : currentYear2;
    currentMonth2 = currentMonth2 === 0 ? 11 : currentMonth2 - 1;
    loadControl2(currentMonth2, currentYear2);
}

function change2() {
    currentYear2 = parseInt(selectYear2.value);
    currentMonth2 = parseInt(selectMonth2.value);
    loadControl2(currentMonth2, currentYear2);
}


function loadControl2(month, year) {
    
    addMonths2(month);
    addYears2(year);
    // console.log(year);
    // console.log(month);

    let firstDay = (new Date(year, month)).getDay();

     // body of the calendar
    var tbl = document.querySelector("#calendarBody2");
    // clearing all previous cells
    tbl.innerHTML = "";


    var monthAndYear_izin = document.getElementById("monthAndYear_izin");
    // filing data about month and in the page via DOM.
    monthAndYear_izin.innerHTML = months2[month] + " " + year;


    selectYear2.value = year;
    selectMonth2.value = month;
    
    // creating the date cells here
    let date = 1;

    // add the selected dates here to preselect
    //selectedDates2.push((month + 1).toString() + '/' + date.toString() + '/' + year.toString());

    // there will be maximum 6 rows for any month
    for (let rowIterator = 0; rowIterator < 6; rowIterator++) {

        // creates a new table row and adds it to the table body
        let row = document.createElement("tr");

        //creating individual cells, filing them up with data.
        for (let cellIterated = 0; cellIterated < 7 && date <= daysInMonth2(month, year); cellIterated++) {

            // create a table data cell
            cell = document.createElement("td");
            let textNode = "";

            // check if this is the valid date for the month
            if (rowIterator !== 0 || cellIterated >= firstDay) {
                cell.id = (month + 1).toString() + '/' + date.toString() + '/' + year.toString();
                cell.class = "clickable";
                textNode = date;

                // this means that highlightToday2 is set to true and the date being iterated it today2s date,
                // in such a scenario we will give it a background color
                if (highlightToday2
                    && date === today2.getDate() && year === today2.getFullYear() && month === today2.getMonth()) {
                    cell.classList.add("today-color");
                }

                // set the previous dates to be selected
                // if the selectedDates2 array has the dates, it means they were selected earlier. 
                // add the background to it.
                if (selectedDates2.indexOf((month + 1).toString() + '/' + date.toString() + '/' + year.toString()) >= 0) {
                    cell.classList.add(highlightClass2);
                }

                date++;
            }

            cellText = document.createTextNode(textNode);
            cell.appendChild(cellText);
            row.appendChild(cell);
        }

        tbl.appendChild(row); // appending each row into calendar body.
    }

    // this adds the button panel at the bottom of the calendar
    addButtonPanel2(tbl);

    // function when the date cells are clicked
    $("#calendarBody2 tr td").click(function (e) {
        var id = $(this).attr('id');
        // check the if cell clicked has a date
        // those with an id, have the date
        if (typeof id !== typeof undefined) {
            var classes = $(this).attr('class');
            if (typeof classes === typeof undefined || !classes.includes(highlightClass2)) {
                var selectedDate = new Date(id);
                //selectedDates2.push((selectedDate.getMonth() + 1).toString() + '/' + selectedDate.getDate().toString() + '/' + selectedDate.getFullYear());
				selectedDates2.push( zeroPad(selectedDate.getDate().toString(),2) + '-' + zeroPad((selectedDate.getMonth() + 1).toString(),2) + '-' + selectedDate.getFullYear().toString().substr(-2));
            }
            else {
                var index = selectedDates2.indexOf(id);
                if (index > -1) {
                    selectedDates2.splice(index, 1);
                }
            }

            $(this).toggleClass(highlightClass2);
        }

        // sort the selected dates array based on the latest date first
        var sortedArray = selectedDates2.sort((a, b) => {
            return new Date(a) - new Date(b);
        });

        // update the selectedValues text input
        document.getElementById('selectedValues_izin').value = datesToString2(sortedArray);
        hitungTglIzin();
    });


    var $search = $('#selectedValues_izin');
    var $dropBox = $('#parent2');
    
    $search.on('blur', function (event) {
        //$dropBox.hide();
    }).on('focus', function () {
        $dropBox.show();
    });
}


// check how many days in a month code from https://dzone.com/articles/determining-number-days-month
function daysInMonth2(iMonth, iYear) {
    return 32 - new Date(iYear, iMonth, 32).getDate();
}

// adds the months2 to the dropdown
function addMonths2(selectedMonth) {
    var select = document.getElementById("monthCal_izin");

    if (months2.length > 0) {
        return;
    }

    for (var month = startMonth2; month <= endMonth2; month++) {
        var monthInstance = dictionaryMonth2[month];
        months2.push(monthInstance[0]);
        select.options[select.options.length] = new Option(monthInstance[0], monthInstance[1], parseInt(monthInstance[1]) === parseInt(selectedMonth));
    }
}

// adds the years2 to the selection dropdown
// by default it is from 1990 to 2030
function addYears2(selectedYear) {

    if (years2.length > 0) {
        return;
    }

    var select = document.getElementById("yearCal_izin");

    for (var year = minYear2; year <= maxYear2; year++) {
        years2.push(year);
        select.options[select.options.length] = new Option(year, year, parseInt(year) === parseInt(selectedYear));
    }
}

resetCalendar2 = function resetCalendar2() {
    // reset all the selected dates
    selectedDates2 = [];
    $('#calendarBody2 tr').each(function () {
        $(this).find('td').each(function () {
            // $(this) will be the current cell
            $(this).removeClass(highlightClass2);
        });
    });
};

function datesToString2(dates) {
    return dates.join(dateSeparator2);
}

function endSelection2() {
    $('#parent2').hide();
    $('#selectedValues_izin').trigger('change2');
}


// to add the button panel at the bottom of the calendar
function addButtonPanel2(tbl) {
    // after we have looped for all the days and the calendar is complete,
    // we will add a panel that will show the buttons, reset and done
    let row = document.createElement("tr");
    row.className = 'buttonPanel';
    cell = document.createElement("td");
    cell.colSpan = 7;
    var parent2Div = document.createElement("div");
    parent2Div.classList.add('row');
    parent2Div.classList.add('buttonPanel-row');
    

    var div = document.createElement("div");
    div.className = 'col-md-6';
    var resetButton = document.createElement("a");
    resetButton.className = 'btn btn-default';
    resetButton.value = 'Reset';
    resetButton.onclick = function () { resetCalendar2(); };
    var resetButtonText = document.createTextNode("Reset");
    resetButton.appendChild(resetButtonText);

    div.appendChild(resetButton);
    parent2Div.appendChild(div);
   

    var div2 = document.createElement("div");
    div2.className = 'col-md-6';
    var doneButton = document.createElement("a");
    doneButton.className = 'btn btn-default';
    doneButton.value = 'Done';
    doneButton.onclick = function () { endSelection2(); };
    var doneButtonText = document.createTextNode("Done");
    doneButton.appendChild(doneButtonText);

    div2.appendChild(doneButton);
    parent2Div.appendChild(div2);

    cell.appendChild(parent2Div);
    row.appendChild(cell);
    // appending each row into calendar body.
    tbl.appendChild(row);
}
function zeroPad(num, places) {
  var zero = places - num.toString().length + 1;
  return Array(+(zero > 0 && zero)).join("0") + num;
}
