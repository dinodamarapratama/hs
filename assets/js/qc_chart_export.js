var list_chart_instances = {};
var list_page = {};
var pageSizeExport = 35; //15
var dataQc = null;
var myColors = {};
var list_detail_hasil = {};

function drawLine(ctx, x0, y0, x1, y1) {
    ctx.beginPath();
    ctx.strokeStyle = '#000000';
    ctx.moveTo(x0,y0);
    ctx.lineTo(x1,y1);
    ctx.stroke();
    // console.log('draw line ', x0, y0, x1, y1);
}

function dateFormating(limiter='-',$tanggal){
    /*
        - dmY -> Ymd
        - Ymd -> dmY
    */
    var date = $tanggal.split(limiter);
    return date[2]+limiter+date[1]+limiter+date[0];
}

$(document).ready(function(){
    function drawBackground(chart){
        // console.log("chart before init", chart);
        var ctx = chart.chart.ctx;
        var canvas = chart.chart.canvas;
        var chartArea = chart.chartArea;
        var marginTop = chartArea.top + 5;
        var marginLeft = chartArea.left;
        var width = canvas.width - marginLeft;
        // var height = canvas.height - marginTop;
        var height = chartArea.bottom - marginTop;

        var areaHeight = height / 6;

        // ctx.strokeStyle = "#FF0000";
        // ctx.fillStyle = "#FF0000";
        ctx.strokeStyle = "#f26e6e";
        ctx.fillStyle = "#f26e6e";

        ctx.fillRect(marginLeft,0*areaHeight + marginTop, width, areaHeight);
        ctx.fillRect(marginLeft,5*areaHeight + marginTop, width, areaHeight);

        ctx.strokeStyle = "#FFFF66";
        ctx.fillStyle = "#FFFF66";

        ctx.fillRect(marginLeft,1*areaHeight + marginTop, width, areaHeight);
        ctx.fillRect(marginLeft,4*areaHeight + marginTop, width, areaHeight);

        /* hijau */
        // ctx.strokeStyle = "#00FF7F";
        ctx.strokeStyle = "#86e786";
        // ctx.fillStyle = "#00FF7F";
        ctx.fillStyle = "#86e786";

        ctx.fillRect(marginLeft,2*areaHeight + marginTop, width, areaHeight);
        ctx.fillRect(marginLeft,3*areaHeight + marginTop, width, areaHeight);
        
        
        ctx.stroke();
    }

    function drawHasilOut(chart) {
        if(chart == undefined || chart == null) {
            console.log('chart is null', chart);
            return;
        }
        var _chart = chart.chart;
        var datasetMeta = chart.getDatasetMeta(0);
        var data1 = chart.data.datasets[0].data;
        var data2 = chart.data.datasets[0].data2;

        var chartLeft = chart.chartArea.left;
        if(_chart == undefined || _chart == null) {
            console.log('_chart is null', _chart);
            // debugger;
            return;
        }
        var ctx = _chart.ctx;
        var width = _chart.width;

        if(ctx == undefined || ctx == null) {
            console.log('ctx is null', ctx);
            return;
        }
        var data = chart.data;
        if(data == undefined || data == null) {
            console.log('chart data is null', data);
            return;
        }
        var datasets = data.datasets;
        if(datasets == undefined || datasets == null) {
            console.log('datasets is null', datasets);
            return;
        }
        var dataset = datasets[0];
        if(dataset == undefined || dataset == null) {
            console.log('dataset is null', dataset);
            return;
        }
        var _data = dataset.data;
        if(_data == undefined || _data == null) {
            console.log('_data is null', _data);
            return;
        }
        // debugger;
        // console.log('_data', _data, 'meta', dMeta, 'data2', data2);
        for(var i = 0, i2 = data2.length; i < i2; i++) {
            var _d = data2[i];
            // console.log('_d',_d);
            if(_d == null) {
                continue;
            }
            if(_d < -3 || _d > 3) {
                var dMeta = datasetMeta.data[i];
                var chartWidth = width - 30;
                var f = (chartWidth / 29) - 5;
                var _long = f;
                var atas = 7;
                var bawah = 220;
                var y = atas;
                if(_d < -3) {
                    y = bawah;
                }
                // var x = (i * f) + 48;
                drawLine(ctx, dMeta._model.x - (i > 0 ? f : 0), y, dMeta._model.x + f, y);

                if(i - 1 >= 0) {
                    var _dP = data2[i-1];
                    var dmP = datasetMeta.data[i-1];
                    if(_dP > 3) {
                        drawLine(ctx, dMeta._model.x - f, y, dmP._model.x + f, atas);
                    } 
                    else if(_dP < -3) {
                        drawLine(ctx, dMeta._model.x - f, y, dmP._model.x + f, bawah);
                    }
                    else {
                        drawLine(ctx, dMeta._model.x - f, y, dmP._model.x, dmP._model.y);
                    }
                }
                
                if(i+1 < i2) {
                    var _dN = data2[i+1];
                    if(_dN <= 3 && _dN >= -3) {
                        var dmN = datasetMeta.data[i + 1];
                        drawLine(ctx, dMeta._model.x + f, y, dmN._model.x, dmN._model.y);
                    }
                }
            }
        }
        
    }
    Chart.plugins.register({
        // plugin implementation
        beforeDraw: drawBackground,
        // beforeUpdate: drawBackground,
        afterDraw: function(chart, easing){
            // console.log('after draw', chart);
            drawHasilOut(chart);
        }
    });
})

function qc_tooltip(args){
    console.log("qc_tooltip", args);
}

function showQcChartExport(node) {
	//$("#modal_chartjs").modal("show");
    myColors = {};
    pageInfo = {};
    //node = JSON.parse(node);
    var id_qc_input = node.id_qc_input;
    var id_hasil = node.id_hasil;
	var month = node.month;
	var year = node.year;

    var container = $("#graf");
    container.html("");
    
    // console.log("qc input", id_qc_input, " hasil", id_hasil);
    // start create canvas
    
    var createChart = function(data){
    	// console.log("create chart",data);
        var input = data.input || {};
    	var level = data.level;
    	var labels = data.labels;
    	var arr = data.arr;
        // var list_hasil = data.list_hasil;

        list_chart_instances[ level.id_level ] = null;
        list_page[ level.id_level ] = 0;

        if(input.mean == undefined) {
            input.mean = 0;
        }
        if(input.sd == undefined) {
            input.sd = 0;
        }
    	var id = "canvas_" + level.id_level + "_" + Math.random();
        var dateStart = "";
        var dateEnd = "";
        if(labels && labels.length > 0) {
            dateStart = labels[0];
            dateEnd = labels[labels.length - 1];
        }
        // var totalHasil = 0;
        var nHasil = arr.length;
        // for(var i = 0, i2 = arr.length; i < i2; i++) {
        //     nHasil++;
        //     totalHasil += Number(arr[i]);
        // }

        // var meanHasil = 0;
        // if( nHasil > 0 ){
        //     meanHasil = totalHasil / nHasil;  
        // } 

        // var rentangBawah = ( meanHasil - ( 2 * input.sd ) ).toFixed(5);
        // var rentangAtas =  ( meanHasil + ( 2 * input.sd ) ).toFixed(5);

        var rentangBawah = ( Number(input.mean) - ( 2 * input.sd ) ).toFixed(5);
        var rentangAtas =  ( Number(input.mean) + ( 2 * input.sd ) ).toFixed(5);
        // debugger;

        var _infoLevel = `<div class="col-sm-12" style="padding-left: 0px; padding-right: 0px; width: 100%;">
            <table class="table_chart_info" style="font-size: 10pt; border: 1px solid rgba(0,0,0,0.1); margin-top: 30px; width: 100%">
                <tr>
                    <td style="width: 40%">LEVEL: <strong>${level.level_name}</strong</td>
                    <td style="width: 60%">N: <strong>${nHasil}</strong></td>
                </tr>
                <tr>
                    <td>MEAN: <strong>${input.mean}</strong></td>
                    <td>SD: <strong>${input.sd}</strong></td>
                </tr>
                <tr>
                    <td colspan="">CONTROL RANGE</td>
                    <td>: <strong>${rentangBawah} - ${rentangAtas}</strong></td>
                </tr>
                <tr>
                    <td colspan="2"><br/></td>
                </tr>
                <tr>
                    <td>COMPARISON MEAN</td>
                    <td>:</td>
                </tr>
                <tr>
                    <td>COMPARISON SD</td>
                    <td>:</td>
                </tr>
                <tr>
                    <td>VISIBLE DATE RANGE</td>
                    <td>: <strong>${dateFormating('-',dateStart)} to ${dateFormating('-',dateEnd)}</strong></td>
                </tr>
            </table>
        </div>`;
        var _infoLegend =`<div class="col-sm-12" style="padding-left: 0px; padding-right: 0px; width: 100%;">
            <table class="table_chart_info" style="font-size: 10pt; border: 1px solid rgba(0,0,0,0.1); margin-top: 30px; width: 100%">
                <tr>
                    <td style="width: 40%">Titik yang berada pada posisi lebih dari 2D atau beda di bawah -2D</td>
                    <td style="width: 60%"><i class="fas fa-dot-circle" style="font-size:16;color:red"></i></td>
                </tr>
                <tr>
                    <td style="width: 40%">Titik yang mengulang dari nilai sebelumnya</td>
                    <td style="width: 60%"><i class="fas fa-dot-circle" style="font-size:16;color:black"></i></td>
                </tr>
            </table>
        </div>`
    	var _html =   `<div class="row" style="padding-left: 0px; padding-right: 0px; margin-left: 25px;" id="row_chart_${level.id_level}">
            <div class="col-md-11" style="padding-left: 0px; padding-right: 0px;">
                ${_infoLevel}
            </div>
            <div class="col-md-11" style="padding-left: 0px; padding-right: 0px;">
                ${_infoLegend}
            </div>
            <div class="col-md-11" style="padding-left: 0px; padding-right: 0px;margin-top:20px;">
                <div class="col-md-11" style="height:350px;">
                    <canvas id="${id}" style=""></canvas>
                    <canvas id="${id}" style=""></canvas>
                </div>
            </div>
    	</div>`;
    	container.append(_html);
		//console.log('WWWW');
        setTimeout(function(){
            var ctx = document.getElementById(id).getContext("2d");
            // var points = [];
            // var _labels = [];
            // var arrLength = arr.length;
            // var labelsLength = labels.length;
            // var bgColor = [];
            // var colorNormal = 'rgb(0,0,0)';
            // var colorRed = 'rgb(255,0,0)';
            // for(var i = 0; i < pageSizeExport; i++) {
            //     if(i >= arrLength || i >= labelsLength ) {
            //         break;
            //     }
            //     points.push( arr[i] );
            //     _labels.push( labels[i] );

            //     // if(arr[i] > 3 || arr[i] < -3) {
            //     //     colorNormal = colorRed;
            //     // }

            //     bgColor.push( colorNormal );
            // }
            var bg = [];
            // var ds = createDatasetsSingleLine(level.level_name, labels, arr, 0, Math.max(labels.length - pageSizeExport, 0), pageSizeExport);
            var ds = createDatasetsSingleLine(level.id_level, labels, arr, 0, Math.max(labels.length - pageSizeExport, 0), pageSizeExport);
            // console.log('first page', ds);
            var targetLabel = '#row_chart_' + level.level_name + ' #label-page';
            $(targetLabel).html((1 + ds.start) + ' - ' + (ds.start + ds.length));
            list_page[level.id_level] = ds.start;
            // debugger;
            // var oldDs = [{
            //     label: level.level_name,
            //     // data: arr,
            //     data: points,
            //     data2: points,
            //     fill: false,
            //     pointRadius: 5,
            //     pointHoverRadius: 10,
            //     lineTension: 0,
            //     backgroundColor: 'rgb(0,0,0)',
            //     pointBackgroundColor: bgColor,
            //     borderColor: 'rgba(0,0,0,0.8)',
            //     borderWidth: '1px'
            // }];
            // console.log('datasets', ds);

            list_chart_instances[ level.id_level ] = new Chart(ctx, {
                type: "line",
                data: {
                    // labels: labels,
                    // labels: _labels,
                    labels: ds.label,
                    datasets: [{
                        label: level.level_name,
                        data: ds.ds,
                        data2: ds.ds2,
                        data3: ds.ds3,
                        fill: false,
                        pointRadius: 3,
                        pointHoverRadius: 5,
                        lineTension: 0,
                        backgroundColor: 'rgb(0,0,0)',
                        pointBackgroundColor: ds.color,
                        borderColor: 'rgba(0,0,0,0.8)',
                        borderWidth: '1px'
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        display: false,
                    },
                    tooltips: {
                        mode: 'x-axis',
                        enabled: false,
                        callbacks: {
                            title: function(a,b){
                                // console.log("tooltip title", a , b);
                                // return "Hasil QC:";
                                var labels = b.labels;
                                var index = a[0].index;
                                var today = new Date(labels[index]);
                                var dd = today.getDate();
                                var mm = today.getMonth()+1; 
                                var yyyy = today.getFullYear();
                                var tanggal = dd+'-'+mm+'-'+yyyy;
                                return tanggal + ' :';
                            },
                            label: function(a,b){
                                // console.log("tooltip label", a , b);
                                // var labels = b.labels;
                                // var index = a.index;
                                // return "Tgl " + b.labels[a.index] + ": " + a.yLabel;
                                var ds = b.datasets[0];
                                var data3 = ds.data3;
                                var data2 = ds.data2;
                                var index = a.index;
                                return "Hasil: " + data3[index] + "<br/>SD: " + data2[index];
                            },
                            afterLabel: function(a,b){
                                // console.log("tooltip after label", a , b);
                                if(a.yLabel < -3 || a.yLabel > 3) {
                                    return 'Warning';
                                }
                                
                                // var index = a.index;
                                // var data = b.datasets[0].data;
                                // for(var i = 0, i2 = data.length; i < i2; i++) {
                                //     if( i > index){
                                //         break;
                                //     }
                                //     // if(data[i] > 3 || data[i] < -3) {
                                //     //     return "Should Reject?";
                                //     // }
                                // }
                                return "";
                            },
                            // beforeLabel: function(a,b){
                            //     return b.datasets[0].data[a.index];
                            // }
                        },
                        custom: function(tooltipModel) {
                            // console.log("tooltip model", tooltipModel);
                            // Tooltip Element
                            var tooltipEl = document.getElementById('chartjs-tooltip');

                            // Create element on first render
                            if (!tooltipEl) {
                                tooltipEl = document.createElement('div');
                                tooltipEl.id = 'chartjs-tooltip';
                                tooltipEl.innerHTML = '<table></table>';
                                tooltipEl.style.margin = '5px';
                                tooltipEl.style.backgroundColor = 'black';
                                document.body.appendChild(tooltipEl);
                            }

                            // Hide if no tooltip
                            if (tooltipModel.opacity === 0) {
                                tooltipEl.style.opacity = 0;
                                return;
                            }

                            // Set caret Position
                            tooltipEl.classList.remove('above', 'below', 'no-transform');
                            if (tooltipModel.yAlign) {
                                tooltipEl.classList.add(tooltipModel.yAlign);
                            } else {
                                tooltipEl.classList.add('no-transform');
                            }

                            function getBody(bodyItem) {
                                // console.log("get body tooltip", bodyItem);
                                return bodyItem.lines;
                            }

                            // Set Text
                            // debugger;
                            if (tooltipModel.body) {
                                var titleLines = tooltipModel.title || [];
                                var bodyLines = tooltipModel.body.map(getBody);
                                var afterLines = tooltipModel.body[0].after;
                                // console.log("body---", bodyLines, afterLines);

                                var innerHtml = '<thead>';

                                titleLines.forEach(function(title) {
                                    innerHtml += '<tr><th style="background-color: black; color: white">' + title + '</th></tr>';
                                });
                                innerHtml += '</thead><tbody>';

                                bodyLines.forEach(function(body, i) {
                                    // console.log("body item", body, i);
                                    var colors = tooltipModel.labelColors[i] || {};
                                    var style = 'background:' + (colors.backgroundColor || '#000000');
                                    style += '; border-color:' + colors.borderColor;
                                    style += '; border-width: 2px; color: white;';
                                    // var span = '<span style="' + style + '"></span>';
                                    // innerHtml += '<tr><td>' + span + body + '</td></tr>';
                                    innerHtml += '<tr><td style="'+style+'">' + body + '</td></tr>';
                                });
                                afterLines.forEach(function(body, i) {
                                    // console.log("body item", body, i);
                                    var colors = tooltipModel.labelColors[i] || {};
                                    var style = 'background:' + (colors.backgroundColor || '#FF0000');
                                    style += '; border-color:' + colors.borderColor;
                                    style += '; border-width: 2px; color: white;';
                                    // var span = '<span style="' + style + '"></span>';
                                    // innerHtml += '<tr><td>' + span + body + '</td></tr>';
                                    innerHtml += '<tr><td style="'+style+'">' + body + '</td></tr>';
                                });
                                innerHtml += '</tbody>';

                                var tableRoot = tooltipEl.querySelector('table');
                                tableRoot.innerHTML = innerHtml;
                            }

                            // `this` will be the overall tooltip
                            var position = this._chart.canvas.getBoundingClientRect();

                            // Display, position, and set styles for font
                            tooltipEl.style.opacity = 1;
                            tooltipEl.style.position = 'absolute';
                            tooltipEl.style.left = 50 + position.left + window.pageXOffset + (tooltipModel.caretX || 0) + 'px';
                            tooltipEl.style.top = position.top + window.pageYOffset + (tooltipModel.caretY || 0) + 'px';
                            // debugger;
                            tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
                            tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
                            tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
                            tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
                            tooltipEl.style.pointerEvents = 'none';
                            tooltipEl.style.zIndex = 9999999;
                        }
                    },
                    scales: {
                        yAxes: [{
                            gridLines: { color: "#333", lineWidth: 2 },
                            ticks: {
                                max: 3,
                                min: -3,
                                stepValue: 1,
                                callback: function(a,b,c){
                                    // console.log("yaxes tick",a,b,c);
                                    return a+"SD";
                                }
                            }
                        }],
                        xAxes: [{
                            ticks: {
                                setpSize: 1,
                                fixedStepSize: 1,
                                max: 35,
                                callback: function(label,index,labels){
                                    // console.log("xAxes tick", a, index, c, d);
                                    // return "" + (b+1);
                                    // return d.substr(5,4);
                                    return label.substr(8);
                                    // return 'tes' + a;
                                }
                            },
                        }]
                    }
                },
            });
            // console.log('chart', list_chart_instances[ level.id_level ]);
			
			console.log('A1');
			
			
			
        }, 0);
        // }, 300);
    	// $("#" + id).css({height: '400px'});
		
		
		
		
		
		
		
		
    }
    /* end create canvas */

    reqQcDetailExport({id_qc_input: id_qc_input, id_hasil: id_hasil, month: month, year: year}, function(data){
    	// console.log("qc detail", data);
    	var list_level = data.levels;
        var qc_input = data.qc_input;
    	var labels = data.labels;
        var detail_hasil = data.list_detail_hasil;
        var list_detail = {};
        if(qc_input != undefined && qc_input != null) {
            $('#modal_chartjs #test-name').html('<span style="font-weight: 100;">Test: </span>' + qc_input.test_name);
            $('#modal_chartjs #lot-number').html('<span style="font-weight: 100;">Lot Number:</span> ' + qc_input.lot_control_name + '');
            $('#modal_chartjs #branch-name').html('<span style="font-weight: 100;">Branch:</span> ' + qc_input.branch_name + '');
            if( qc_input.detail != undefined && qc_input.detail != null ) {
                // list_detail = data.qc_input.detail;
                var details = qc_input.detail;
                for(var key in details) {
                    var d = details[key];
                    list_detail[ d.id_level ] = d;
                }
            }
        }
        
        // console.log("list detail input", list_detail);
    	for(var i = 0, i2 = list_level.length; i < i2; i++){
    		var level = list_level[i];
    		var id_level = parseInt(level.id_level);
    		var dataset = [];
    		var charts = data.charts;
            // console.log('charts', charts);
    		$.each(charts, function(id_level2, arr) {
    			if(id_level == id_level2) {
    				dataset = arr;
    			}
    		});
            var list_hasil = [];
            $.each(detail_hasil, function(index, data){
                // console.log('detail hasil, index', index, 'data', data);
                if( Number( data.id_level ) == id_level ) {
                    list_hasil.push( data.hasil );
                }
            });
            list_detail_hasil[ id_level ] = list_hasil;
            var detail = {};
            for(var j =0, j2 = list_detail.length; j < j2; j++) {
                if(parseInt(list_detail[j].id_level) == id_level) {
                    detail = list_detail[j]
                    break;
                }
            }
    		createChart({
    			level: level,
    			arr: dataset,
    			labels: labels[ id_level ],
                // detail: detail
                input: list_detail[ id_level ],
                // list_hasil: list_hasil,
    		});
    	}
		
		setTimeout(function(){
			console.log('B1');
			$("#graf").css("width", "100%");
			$("#graf").css("height", "100%");
			//
			
			// window.open('<?= base_url() ?>admin/qc/qc_pdf/'+row.id_hasil,'_blank');  
			
			// $("#graf2").css("display", "block");
			//var nodex = document.getElementById('graf').getElementsByClassName("row");
			
			//console.log(nodex);
			$('#graf div[id^="row_chart_"]').each(function(i, obj) {
				//console.log('XXX');
				
				var lev_id = $(obj).attr('id');
				lev_id = lev_id.substr(lev_id.length - 1);
				domtoimage.toPng(obj)
				.then(function (dataUrl) {
					//console.log(dataUrl);
					//var img = new Image();
					//img.src = dataUrl;
					//document.body.appendChild(img);

					$.ajax({
						url: node.url+"admin/qc/upload_graf_multi/"+node.id_hasil,
						method: "post",
						data: {
							img:dataUrl,
							level:lev_id
						},
						success: function(response) {
							console.log('Status : ',response.status);
						},
						error: function(a,b,c) {
							//swal.showInputError(c);
						}
					});
				})
				.catch(function (error) {
					//console.error('oops, something went wrong!', error);
				});
				//window.open('<?= base_url() ?>admin/qc/qc_pdf/'+row.id_qc_input,'_blank'); 
				
			});
			
			//setTimeout(function(){
				$("#graf").css("width", "650px");
				$("#graf").css("height", "569px");
				// $("#graf2").css("height", "0");
				$eui('#id_hasil').val(node.id_hasil);
				//window.open('<?= base_url() ?>admin/qc/qc_pdf/'+row.id_hasil,'_blank');  
				window.open(node.url+'admin/qc/qc_pdf_multi/'+node.id_hasil,'_blank'); 
			//}, 4000);
		}, 5000);
    });
	
	
	return container;
}

function createDatasetsSingleLine(levelName, labels, points, page, start, length) {
    if(page == undefined || page  == null) {
        page = 0;
    }
    if(start == undefined || start == null) {
        start = 0;
    }
    if(length == undefined || length == null) {
        length = pageSizeExport;
    }

    var indexStart = start;
    // console.log("indexStart : ",indexStart);
    var indexStop = indexStart + length;
    // console.log("indexStop : ",indexStop);
    // console.log("create chart1",myColors[levelName]);
    if(myColors[levelName] == undefined) {
        myColors[levelName] = createColors(levelName, labels);
    }

    var colors = myColors[levelName];
    var details = list_detail_hasil[ levelName ];

    var dLength = Math.min( labels.length, points.length, colors.length );
    // console.log("dLength : ",dLength);
    var result = {
        label: [],
        ds: [],
        ds2: [],
        ds3: [],
        color: [],
        start: start,
        length: length,
        page: page
    };

    for(var i = indexStart; i < indexStop; i++) {
        // console.log("for-dLength : ",i);
        if(i >= dLength) {
            // break;
            result.label.push('');
            result.ds.push(null);
            result.ds2.push(null);
            result.color.push('');
            continue;
        }
        var label = labels[i];
        var point = points[i];
        var point_1 = points[i-1]; 

        result.label.push(label);
        if(point > 3 || point < -3) {
            result.ds.push(null);
        }
        else {
            result.ds.push(point);
        }
        
        result.ds2.push(point);
        // result.color.push(colors[i]);
        // if(point > 1 || point < -1) {
        //     result.color.push('rgb(255, 255, 0)');
        // }else if(point > 2 || point < -2){
        //     result.color.push('rgb(255, 0, 0)');
        // }else if(point_1 === point){
        //     result.color.push('rgb(255, 255, 255)');
        // }else {
        //     result.color.push(colors[i]);
        // }
        if(point > 2 || point < -2){
            result.color.push('rgb(255, 0, 0)');
        }
        else if(point_1 === point){
            result.color.push('rgb(255, 255, 255)');
        }else {
            result.color.push(colors[i]);
        }
        result.length = i - indexStart + 1;
        result.ds3.push(Number(details[i]));
    }
    return result;
}

function createColors(labelName, labels) {
    var added = [];
    var colors = [];
    for(var i = 0, i2 = labels.length; i < i2; i++) {
        var label = labels[i];
        if(added.indexOf(label) >= 0) {
            colors.push('rgb(255,255,255)');
        } else {
            added.push(label);
            colors.push('rgb(0,0,0)');
        }
    }
    return colors;
}

function createDatasetsMultiLine(levelName, labels, points) {
    /* tentukan jumlah maksimal input data dalam suatu hari */
    var kemunculan = {};
    var kemunculanMaksimal = 1;
    // console.log('labels', labels);
    for(var i = 0, i2 = labels.length; i < i2; i++) {
        var label = labels[i];
        if(kemunculan[label] == undefined || kemunculan[label] == null) {
            kemunculan[label] = 1;
        } else {
            kemunculan[label]++;
            if(kemunculan[label] > kemunculanMaksimal) {
                kemunculanMaksimal = kemunculan[label];
            }
        }
    }
    // console.log('kemunculan', kemunculanMaksimal, kemunculan);
    var result = [];
    for(var i = 0; i < kemunculanMaksimal; i++) {
        var color = 'rgb(0,0,0)';
        if(i>0){
            color = 'rgb(255,255,255)';
        }
        var ds = {
                    label: levelName,
                    data: [],
                    fill: false,
                    pointRadius: 5,
                    pointHoverRadius: 10,
                    lineTension: 0,
                    backgroundColor: color,
                    pointBackgroundColor: color,
                    borderColor: 'rgba(0,0,0,0.8)',
                    borderWidth: '1px'
                };
        result.push(ds);
    }
    // var countLabel = labels.length;
    // var countPoint = points.length;
    // var count = Math.min(countLabel, countPoint);
    var pointsCopy = points.slice();
    var labelsCopy = labels.slice();

    var distinctLabel = [];
    for(var i = 0, i2 = labelsCopy.length; i < i2; i ++) {
        var label = labelsCopy[i];
        if(distinctLabel.indexOf(label) < 0) {
            distinctLabel.push(label);
        }
    }
    // debugger;
    var processed = [];
    for(var i = 0; i < kemunculanMaksimal; i++) {
        for(var j = 0, j2 = distinctLabel.length; j < j2; j++) {
            var label = distinctLabel[j];

            var targetIndex = -1;
            var pointResult = 0;
            var minCount = Math.min( pointsCopy.length, labelsCopy.length );
            for(var k = 0; k < minCount; k++) {
                var tLabel = labelsCopy[k];
                if(tLabel == label) {
                    targetIndex = k;
                    pointResult = pointsCopy[k];
                    pointsCopy.splice(k, 1);
                    labelsCopy.splice(k, 1);
                    break;
                }
            }
            if(targetIndex < 0) {
                result[i].data.push( NaN );
            } else {
                result[i].data.push( pointResult );
            }
        }
    }
    // for(var i = 0; i < count; i ++) {
    //     var label = labels[i];
    //     var point = points[i];

    //     if(kemunculan[label] == 1){
    //         for(var j = 0; j < kemunculanMaksimal; j++) {
    //             result[j].data.push(point);
    //         }
    //     } else {
    //         var isiMinimal = 999;
    //         var indexMinimal = 0;
    //         for(var j = 0, j2 = result.length; j < j2; j++){
    //             var dLenth = result[j].data.length;
    //             if(isiMinimal > dLenth) {
    //                 isiMinimal = dLenth;
    //                 indexMinimal = j;
    //             }
    //         }

    //         result[indexMinimal].data.push( point );
    //     }
        
    // }
    return {
        labels: distinctLabel,
        ds: result
    };
}

function reqQcDetailExport(data, callback) {
	$.ajax({
		method: "post",
		url: base_url + "admin/qc/chart_js_export",
		data: data,
		success: function(resp) {
            dataQc = JSON.parse(resp);
			callback(dataQc);
		}
	});
}

function click_chart_shift_right(node) {
    node = $(node);
    var id_level = node.attr("id-level");
    var p = chart_right(id_level, 1);

    // node.parent().find("#label-page").html( parseInt(p) + 1 );
    if(p != undefined && p != null){
        updatePageInfo(node, p.start, p.start + p.length);
    }
    // debugger;
}

function click_chart_shift_left(node) {
    node = $(node);
    var id_level = node.attr("id-level");

    var p = chart_left(id_level, 1);
    // node.parent().find("#label-page").html( parseInt(p) + 1 );
    if(p != undefined && p != null){
        updatePageInfo(node, p.start, p.start + p.length);
    }
   
    // debugger;
}

function click_chart_prev_page(node) {
    node = $(node);
    var id_level = node.attr("id-level");

    var p = chart_left(id_level, pageSizeExport);
    // node.parent().find("#label-page").html( parseInt(p) + 1 );
    if(p != undefined && p != null){
        updatePageInfo(node, p.start, p.start + p.length);
    }
}

function click_chart_next_page(node) {
    node = $(node);
    var id_level = node.attr("id-level");
    var p = chart_right(id_level, pageSizeExport);

    // node.parent().find("#label-page").html( parseInt(p) + 1 );
    if(p != undefined && p != null){
        updatePageInfo(node, p.start, p.start + p.length);
    }
}

function updatePageInfo(node, start, end) {
    node.parent().find("#label-page").html( (start + 1) + ' - ' + end );
}

function chart_right(id_level, inc) {
    // debugger;
    if(dataQc == undefined || dataQc == null ) {
        console.log("data qc is not valid");
        return;
    }
    // debugger;
    var chart = list_chart_instances[ id_level ];
    if ( chart == undefined || chart == null ) {
        console.log("chart is not valid", chart);
        return;
    }
    // debugger;
    var page = list_page[ id_level ];
    // debugger;
    if(page == undefined || page == null ) {
        console.log("page is not valid", page);
        // return;
        page = 0;
    }

    // if(page + inc > )
    // debugger;
    
    var _labels = dataQc.labels[ id_level ];
    var _chart = dataQc.charts[ id_level ];
    if(_labels.length < page + inc + pageSizeExport) {
        console.log('page maximum', page);
        // return;
        page = Math.max(0, _labels.length - pageSizeExport);
        list_page[ id_level ] = page;
    } 
    else {
        list_page[ id_level ] = page = page + inc;
    } 
    
    // debugger;
    var ds = updateChartData(chart, page, _labels, _chart, page, pageSizeExport, id_level);

    // return page;
    // debugger;
    return ds;
}

function chart_left(id_level, dec) {
    // debugger;
    if(dataQc == undefined || dataQc == null ) {
        console.log("data qc is not valid");
        return;
    }
    // debugger;
    var chart = list_chart_instances[ id_level ];
    if ( chart == undefined || chart == null ) {
        console.log("chart is not valid", chart);
        return;
    }
    // debugger;
    var page = list_page[ id_level ];
    if(page == undefined || page == null ) {
        console.log("page is not valid", page);
        // return;
        page = 0;
    }
    // debugger;
    if(page - dec < 0) {
        console.log("page is minimum", page);
        // return ;
        list_page[ id_level ] = page = 0;
    } 
    else {
        list_page[ id_level ] = page = page - dec;
    }
    // list_page[ id_level ] = page = page - 1;
    
    var _labels = dataQc.labels[ id_level ];
    var _chart = dataQc.charts[ id_level ];

    var ds = updateChartData(chart, page, _labels, _chart, page, pageSizeExport, id_level);

    // return page;
    // debugger;
    return ds;
}

function updateChartData(chart, page, _labels, _chart, start, length, id_level){
    // var _labelLength = _labels.length;
    // var _chartLength = _chart.length;

    // var indexStart = page * pageSizeExport;
    // var indexStop = indexStart + pageSizeExport;
    // // console.log("display chart for page ", indexStart, indexStop);

    // var __chart = [];
    // var __label = [];
    // var __color = [];
    // var colorBlue = 'rgb(0, 0, 255)';
    // var colorNormal = 'rgb(0,0,0)';
    // if(indexStart>= pageSizeExport) {
    //     colorNormal = colorBlue;
    // }
    // // for(var i = 0; i < indexStart; i++) {
    // //     if(i >= _chartLength ){
    // //         break;
    // //     }
    // //     if(_chart[i] > 3 || _chart[i] < -3)  {
    // //         colorNormal = colorBlue;
    // //         // debugger;
    // //     }

    // // }
    // // debugger;
    // for(var i = indexStart; i < indexStop; i++ ) {
    //     if( i >= _labelLength || i >= _chartLength ) {
    //         break;
    //     }
    //     __chart.push( _chart[ i ] );
    //     __label.push( _labels[ i ] );

    //     // if(_chart[i] > 3 || _chart[i] < -3)  {
    //     //     colorNormal = colorBlue;
    //     //     // debugger;
    //     // }
    //     __color.push(colorNormal);
    // }
    // debugger;
    // chart.data.labels = __label;
    var ds = createDatasetsSingleLine(id_level, _labels, _chart, page, start, length);
    // console.log('pagination datasets', ds);
    chart.data.labels = ds.label;
    chart.data.datasets.forEach((d) => {
        // debugger;
        // d.data = __chart;
        d.data = ds.ds;
        d.data2 = ds.ds2;
        d.data3 = ds.ds3;
        // d.pointBackgroundColor = __color;
        d.pointBackgroundColor = ds.color;
        // console.log(d);
    });

    chart.update();

    // debugger;
    return ds;

    // var targetLabel = '#row_chart_' + level.level_name + ' #label-page';
    // $(targetLabel).html((1 + ds.start) + ' - ' + (ds.start + ds.length));
}
