"use strict";

var numberOfItemsDraft = $("#draft .list-group").length; // Get total number of the items that should be paginated
var limitPerPageDraft = 15; // Limit of items per each draft
$("#draft .list-group:gt(" + (limitPerPageDraft - 1) + ")").hide(); // Hide all items over draft limits (e.g., 5th item, 6th item, etc.)
var totalPagesDraft = Math.ceil(numberOfItemsDraft / limitPerPageDraft); // Get number of drafts
$(".pagination-draft").append(
  "<li class='current-draft active'><a href='javascript:void(0)'>" +
    1 +
    "</a></li>"
); // Add first draft marker

// Loop to insert draft number for each sets of items equal to draft limit (e.g., limit of 4 with 20 total items = insert 5 drafts)
for (var i = 2; i <= totalPagesDraft; i++) {
  $(".pagination-draft").append(
    "<li class='current-draft'><a href='javascript:void(0)'>" +
      i +
      "</a></li>"
  ); // Insert draft number into pagination-draft tabs
}

// Add next button after all the draft numbers
$(".pagination-draft").append(
  "<li id='next-draft'><a href='javascript:void(0)' aria-label=Next><span aria-hidden=true>Next</span></a></li>"
);

// Function that displays new items based on draft number that was clicked
$(".pagination-draft li.current-draft").on("click", function() {
  // Check if draft number that was clicked on is the current draft that is being displayed
  if ($(this).hasClass("active")) {
    return false; // Return false (i.e., nothing to do, since user clicked on the draft number that is already being displayed)
  } else {
    var currentPageDraft = $(this).index(); // Get the current draft number
    $(".pagination-draft li").removeClass("active"); // Remove the 'active' class status from the draft that is currently being displayed
    $(this).addClass("active"); // Add the 'active' class status to the draft that was clicked on
    $("#draft .list-group").hide(); // Hide all items in loop, this case, all the list groups
    var grandTotalDraft = limitPerPageDraft * currentPageDraft; // Get the total number of items up to the draft number that was clicked on

    // Loop through total items, selecting a new set of items based on draft number
    for (var i = grandTotalDraft - limitPerPageDraft; i < grandTotalDraft; i++) {
      $("#draft .list-group:eq(" + i + ")").show(); // Show items from the new draft that was selected
    }
  }
});

// Function to navigate to the next draft when users click on the next-draft id (next draft button)
$("#next-draft").on("click", function() {
  var currentPageDraft = $(".pagination-draft li.active").index(); // Identify the current active draft
  // Check to make sure that navigating to the next draft will not exceed the total number of drafts
  if (currentPageDraft === totalPagesDraft) {
    return false; // Return false (i.e., cannot navigate any further, since it would exceed the maximum number of drafts)
  } else {
    currentPageDraft++; // Increment the draft by one
    $(".pagination-draft li").removeClass("active"); // Remove the 'active' class status from the current draft
    $("#draft .list-group").hide(); // Hide all items in the pagination-draft loop
    var grandTotalDraft = limitPerPageDraft * currentPageDraft; // Get the total number of items up to the draft that was selected

    // Loop through total items, selecting a new set of items based on draft number
    for (var i = grandTotalDraft - limitPerPageDraft; i < grandTotalDraft; i++) {
      $("#draft .list-group:eq(" + i + ")").show(); // Show items from the new draft that was selected
    }

    $(".pagination-draft li.current-draft:eq(" + (currentPageDraft - 1) + ")").addClass(
      "active"
    ); // Make new draft number the 'active' draft
  }
});

// Function to navigate to the previous draft when users click on the previous-draft id (previous draft button)
$("#previous-draft").on("click", function() {
  var currentPageDraft = $(".pagination-draft li.active").index(); // Identify the current active draft
  // Check to make sure that users is not on draft 1 and attempting to navigating to a previous draft
  if (currentPageDraft === 1) {
    return false; // Return false (i.e., cannot navigate to a previous draft because the current draft is draft 1)
  } else {
    currentPageDraft--; // Decrement draft by one
    $(".pagination-draft li").removeClass("active"); // Remove the 'activate' status class from the previous active draft number
    $("#draft .list-group").hide(); // Hide all items in the pagination-draft loop
    var grandTotalDraft = limitPerPageDraft * currentPageDraft; // Get the total number of items up to the draft that was selected

    // Loop through total items, selecting a new set of items based on draft number
    for (var i = grandTotalDraft - limitPerPageDraft; i < grandTotalDraft; i++) {
      $("#draft .list-group:eq(" + i + ")").show(); // Show items from the new draft that was selected
    }

    $(".pagination-draft li.current-draft:eq(" + (currentPageDraft - 1) + ")").addClass(
      "active"
    ); // Make new draft number the 'active' draft
  }
});

