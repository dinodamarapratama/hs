"use strict";

var numberOfItemsSent = $("#sent .list-group").length; // Get total number of the items that should be paginated
var limitPerPageSent = 15; // Limit of items per each sent
$("#sent .list-group:gt(" + (limitPerPageSent - 1) + ")").hide(); // Hide all items over sent limits (e.g., 5th item, 6th item, etc.)
var totalPagesSent = Math.ceil(numberOfItemsSent / limitPerPageSent); // Get number of sents
$(".pagination-sent").append(
  "<li class='current-sent active'><a href='javascript:void(0)'>" +
    1 +
    "</a></li>"
); // Add first sent marker

// Loop to insert sent number for each sets of items equal to sent limit (e.g., limit of 4 with 20 total items = insert 5 sents)
for (var i = 2; i <= totalPagesSent; i++) {
  $(".pagination-sent").append(
    "<li class='current-sent'><a href='javascript:void(0)'>" +
      i +
      "</a></li>"
  ); // Insert sent number into pagination-sent tabs
}

// Add next button after all the sent numbers
$(".pagination-sent").append(
  "<li id='next-sent'><a href='javascript:void(0)' aria-label=Next><span aria-hidden=true>Next</span></a></li>"
);

// Function that displays new items based on sent number that was clicked
$(".pagination-sent li.current-sent").on("click", function() {
  // Check if sent number that was clicked on is the current sent that is being displayed
  if ($(this).hasClass("active")) {
    return false; // Return false (i.e., nothing to do, since user clicked on the sent number that is already being displayed)
  } else {
    var currentPageSent = $(this).index(); // Get the current sent number
    $(".pagination-sent li").removeClass("active"); // Remove the 'active' class status from the sent that is currently being displayed
    $(this).addClass("active"); // Add the 'active' class status to the sent that was clicked on
    $("#sent .list-group").hide(); // Hide all items in loop, this case, all the list groups
    var grandTotalSent = limitPerPageSent * currentPageSent; // Get the total number of items up to the sent number that was clicked on

    // Loop through total items, selecting a new set of items based on sent number
    for (var i = grandTotalSent - limitPerPageSent; i < grandTotalSent; i++) {
      $("#sent .list-group:eq(" + i + ")").show(); // Show items from the new sent that was selected
    }
  }
});

// Function to navigate to the next sent when users click on the next-sent id (next sent button)
$("#next-sent").on("click", function() {
  var currentPageSent = $(".pagination-sent li.active").index(); // Identify the current active sent
  // Check to make sure that navigating to the next sent will not exceed the total number of sents
  if (currentPageSent === totalPagesSent) {
    return false; // Return false (i.e., cannot navigate any further, since it would exceed the maximum number of sents)
  } else {
    currentPageSent++; // Increment the sent by one
    $(".pagination-sent li").removeClass("active"); // Remove the 'active' class status from the current sent
    $("#sent .list-group").hide(); // Hide all items in the pagination-sent loop
    var grandTotalSent = limitPerPageSent * currentPageSent; // Get the total number of items up to the sent that was selected

    // Loop through total items, selecting a new set of items based on sent number
    for (var i = grandTotalSent - limitPerPageSent; i < grandTotalSent; i++) {
      $("#sent .list-group:eq(" + i + ")").show(); // Show items from the new sent that was selected
    }

    $(".pagination-sent li.current-sent:eq(" + (currentPageSent - 1) + ")").addClass(
      "active"
    ); // Make new sent number the 'active' sent
  }
});

// Function to navigate to the previous sent when users click on the previous-sent id (previous sent button)
$("#previous-sent").on("click", function() {
  var currentPageSent = $(".pagination-sent li.active").index(); // Identify the current active sent
  // Check to make sure that users is not on sent 1 and attempting to navigating to a previous sent
  if (currentPageSent === 1) {
    return false; // Return false (i.e., cannot navigate to a previous sent because the current sent is sent 1)
  } else {
    currentPageSent--; // Decrement sent by one
    $(".pagination-sent li").removeClass("active"); // Remove the 'activate' status class from the previous active sent number
    $("#sent .list-group").hide(); // Hide all items in the pagination-sent loop
    var grandTotalSent = limitPerPageSent * currentPageSent; // Get the total number of items up to the sent that was selected

    // Loop through total items, selecting a new set of items based on sent number
    for (var i = grandTotalSent - limitPerPageSent; i < grandTotalSent; i++) {
      $("#sent .list-group:eq(" + i + ")").show(); // Show items from the new sent that was selected
    }

    $(".pagination-sent li.current-sent:eq(" + (currentPageSent - 1) + ")").addClass(
      "active"
    ); // Make new sent number the 'active' sent
  }
});

