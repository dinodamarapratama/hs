-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2022 at 02:21 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hs`
--

-- --------------------------------------------------------

--
-- Table structure for table `bagian`
--

CREATE TABLE `bagian` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_department` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `branch_id` int(11) NOT NULL,
  `branch_name` varchar(64) NOT NULL,
  `branch_code` varchar(2) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `creator_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `branch_time`
--

CREATE TABLE `branch_time` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `time_ids` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id_department` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `homeservice`
--

CREATE TABLE `homeservice` (
  `id` int(11) UNSIGNED NOT NULL,
  `date` date DEFAULT NULL,
  `pid` varchar(100) DEFAULT NULL,
  `time_id` int(11) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alamat` text,
  `no_telp` varchar(100) DEFAULT NULL,
  `dokter` varchar(100) DEFAULT NULL,
  `jumlah_pasien` int(11) DEFAULT NULL,
  `pemeriksaan` varchar(100) DEFAULT NULL,
  `pay_id` int(11) NOT NULL DEFAULT '1',
  `petugas_id` int(11) DEFAULT NULL,
  `ptgshs_id` int(11) DEFAULT NULL,
  `ttd_pasien` varchar(100) DEFAULT NULL,
  `time_selesai` varchar(10) NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `reason` varchar(150) DEFAULT NULL,
  `catatan` text,
  `booked` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hs_abbr`
--

CREATE TABLE `hs_abbr` (
  `abbr_id` int(11) UNSIGNED NOT NULL,
  `abbr_name` varchar(100) DEFAULT NULL,
  `lokasi` longtext,
  `branch_id` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hs_block`
--

CREATE TABLE `hs_block` (
  `block_id` int(11) UNSIGNED NOT NULL,
  `ptgs_id` int(11) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `hari_id` int(11) DEFAULT NULL,
  `time_id` varchar(11) DEFAULT NULL,
  `time_name` text,
  `branch_id` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hs_initial_petugas_hs`
--

CREATE TABLE `hs_initial_petugas_hs` (
  `ptgshs_id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `abbr_hs` varchar(100) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `file_out` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hs_payment`
--

CREATE TABLE `hs_payment` (
  `pay_id` int(11) NOT NULL,
  `pay_name` varchar(50) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hs_time`
--

CREATE TABLE `hs_time` (
  `time_id` int(11) NOT NULL,
  `time_name` time NOT NULL,
  `branch_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intern_checklist`
--

CREATE TABLE `intern_checklist` (
  `id` int(11) NOT NULL,
  `id_intern_alat` int(11) NOT NULL,
  `periode` enum('harian','mingguan','bulanan','as_needed','tri_month','six_month','as_needed') NOT NULL,
  `checklist_name` varchar(100) NOT NULL DEFAULT '',
  `creator` int(11) DEFAULT NULL,
  `note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_fs`
--

CREATE TABLE `master_fs` (
  `fs_id` int(11) UNSIGNED NOT NULL,
  `start_fs` time DEFAULT NULL,
  `end_fs` time DEFAULT NULL,
  `slot` varchar(100) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_intern_alat`
--

CREATE TABLE `master_intern_alat` (
  `id` int(11) NOT NULL,
  `id_alat` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `creator` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `serial_number` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_menu`
--

CREATE TABLE `master_menu` (
  `id_menu` int(11) NOT NULL,
  `urutan` int(11) NOT NULL DEFAULT '1',
  `nama` varchar(100) NOT NULL,
  `url` varchar(200) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `icon_class` varchar(20) DEFAULT NULL,
  `icon_text` varchar(20) DEFAULT NULL,
  `mobile_route` varchar(100) DEFAULT NULL,
  `show_mobile_dashboard` int(11) NOT NULL DEFAULT '1',
  `show_mobile_drawer` int(11) NOT NULL DEFAULT '1',
  `mobile_icon` varchar(200) NOT NULL,
  `show_in_web` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_qc_alat`
--

CREATE TABLE `master_qc_alat` (
  `id_alat` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `alat_name` varchar(100) NOT NULL,
  `qr_path` varchar(225) NOT NULL,
  `group_id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_ikabkota`
--

CREATE TABLE `m_ikabkota` (
  `id_kabkota` int(5) NOT NULL,
  `nama_kabkota` varchar(50) NOT NULL,
  `id_propinsi` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_ikecamatan`
--

CREATE TABLE `m_ikecamatan` (
  `id_kecamatan` int(5) NOT NULL,
  `nama_kecamatan` varchar(50) NOT NULL,
  `id_kabkota` int(5) NOT NULL,
  `id_propinsi` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_ikelurahan`
--

CREATE TABLE `m_ikelurahan` (
  `id_kelurahan` int(5) NOT NULL,
  `nama_kelurahan` varchar(50) NOT NULL,
  `id_kecamatan` int(5) NOT NULL,
  `id_kabkota` int(5) NOT NULL,
  `id_propinsi` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_ipropinsi`
--

CREATE TABLE `m_ipropinsi` (
  `id_propinsi` int(5) NOT NULL,
  `nama_propinsi` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(20) NOT NULL,
  `from_id` int(11) DEFAULT NULL,
  `to_id` int(11) DEFAULT NULL,
  `description` text,
  `data` text,
  `read` int(11) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `cuti_flag` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `onesignal_ids`
--

CREATE TABLE `onesignal_ids` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `player_id` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE `position` (
  `id_position` int(11) NOT NULL,
  `id_department` int(11) NOT NULL,
  `name_position` varchar(200) NOT NULL DEFAULT '',
  `creator_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role_menu_dept`
--

CREATE TABLE `role_menu_dept` (
  `id` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_dept` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role_menu_pos`
--

CREATE TABLE `role_menu_pos` (
  `id` int(11) NOT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `id_pos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role_menu_user`
--

CREATE TABLE `role_menu_user` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_log`
--

CREATE TABLE `tabel_log` (
  `log_id` int(11) NOT NULL,
  `log_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `log_user` varchar(255) DEFAULT NULL,
  `log_tipe` int(11) DEFAULT NULL,
  `log_desc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `CUTI` varchar(100) DEFAULT NULL,
  `cuti_keseluruhan` int(11) NOT NULL DEFAULT '0',
  `masabakti` int(11) NOT NULL DEFAULT '0',
  `last_reset_masabakti` date NOT NULL,
  `masabakti_keseluruhan` int(11) NOT NULL DEFAULT '0',
  `lampiran_str` text NOT NULL,
  `lampiran_mcu` text NOT NULL,
  `lampiran_skk` text NOT NULL,
  `last_reset_cuti` date DEFAULT NULL,
  `NIP` varchar(25) NOT NULL,
  `FINGERID` varchar(25) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `many_branch` varchar(100) DEFAULT NULL,
  `image` text,
  `id_position` int(11) DEFAULT NULL,
  `id_bagian` int(11) DEFAULT NULL,
  `id_department` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `inisial` varchar(100) DEFAULT NULL,
  `call_name` varchar(100) DEFAULT NULL,
  `password` text NOT NULL,
  `TGLMASUK` date DEFAULT NULL,
  `TGLKELUAR` date DEFAULT NULL,
  `TGLTETAP` date DEFAULT NULL,
  `TMTLAHIR` varchar(255) DEFAULT NULL,
  `TGLLAHIR` date DEFAULT NULL,
  `GENDER` varchar(6) DEFAULT NULL,
  `GOLDARAH` varchar(3) DEFAULT NULL,
  `KODEUNIT` int(25) DEFAULT NULL,
  `KODEJAB` varchar(4) DEFAULT NULL,
  `KODEFUNGSI` varchar(4) DEFAULT NULL,
  `ALAMAT` varchar(500) DEFAULT NULL,
  `RTRW` varchar(100) DEFAULT NULL,
  `KELDESA` varchar(500) DEFAULT NULL,
  `PROPINSI` int(150) DEFAULT NULL,
  `KECAMATAN` varchar(150) DEFAULT NULL,
  `KABUPATEN` int(150) DEFAULT NULL,
  `KODEPOS` int(5) DEFAULT NULL,
  `TELPONRUMAH` varchar(250) DEFAULT NULL,
  `TELPONHP` varchar(250) DEFAULT NULL,
  `NONPWP` varchar(150) DEFAULT NULL,
  `NOKTP` varchar(150) DEFAULT NULL,
  `AGAMA` varchar(150) DEFAULT NULL,
  `STATUS` varchar(50) DEFAULT NULL,
  `PIN` varchar(100) DEFAULT NULL,
  `MUSTCHANGE` bit(64) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `JMLANAK` int(150) DEFAULT NULL,
  `STATUSKAWIN` varchar(25) DEFAULT NULL,
  `PENDIDIKAN` varchar(5) DEFAULT NULL,
  `AWALKONTRAK` datetime DEFAULT NULL,
  `AKHIRKONTRAK` datetime DEFAULT NULL,
  `AVKELAS` int(11) DEFAULT NULL,
  `BUMIDA` varchar(30) DEFAULT NULL,
  `AVPENSION` bit(64) DEFAULT NULL,
  `TIPEBAYAR` varchar(25) DEFAULT NULL,
  `BANK` varchar(25) DEFAULT NULL,
  `NOREK` varchar(50) DEFAULT NULL,
  `NOJAMSOSTEK` varchar(25) DEFAULT NULL,
  `TGLJAMSOSTEK` datetime DEFAULT NULL,
  `NIPLAMA` varchar(25) DEFAULT NULL,
  `TINGGI` decimal(18,2) DEFAULT NULL,
  `BERAT` decimal(18,2) DEFAULT NULL,
  `APR` int(150) DEFAULT NULL,
  `TKES` int(150) DEFAULT NULL,
  `SPK` int(150) DEFAULT NULL,
  `PERUSAHAAN` varchar(100) DEFAULT NULL,
  `NOBPJSKES` varchar(25) DEFAULT NULL,
  `NOBUMIDA` varchar(25) DEFAULT NULL,
  `ALAMAT_DOMO` varchar(500) DEFAULT NULL,
  `RTRW_DOMO` varchar(100) DEFAULT NULL,
  `KELDESA_DOMO` varchar(500) DEFAULT NULL,
  `PROPINSI_DOMO` varchar(100) DEFAULT NULL,
  `KECAMATAN_DOMO` varchar(100) DEFAULT NULL,
  `KABUPATEN_DOMO` varchar(100) DEFAULT NULL,
  `KODEPOS_DOMO` varchar(100) DEFAULT NULL,
  `TELPONRUMAH_DOMO` varchar(100) DEFAULT NULL,
  `TELPONHP_DOMO` varchar(100) DEFAULT NULL,
  `eintell_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `creator_id` int(11) DEFAULT NULL,
  `LastUpdateDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login` timestamp NULL DEFAULT NULL,
  `tanggal_str` date NOT NULL,
  `tanggal_sik` date NOT NULL,
  `tanggal_vaksin1` date NOT NULL,
  `tanggal_vaksin2` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_user`
-- (See below for the actual view)
--
CREATE TABLE `v_user` (
`id_user` int(11)
,`name` varchar(150)
,`username` varchar(100)
,`FINGERID` varchar(25)
,`image` text
,`id_department` int(11) unsigned
,`position_name` varchar(200)
,`department_name` varchar(255)
,`id_position` int(11)
,`branch_name` varchar(64)
,`bagian_name` varchar(100)
,`id_bagian` int(11)
);

-- --------------------------------------------------------

--
-- Structure for view `v_user`
--
DROP TABLE IF EXISTS `v_user`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_user`  AS  select `usr`.`id_user` AS `id_user`,`usr`.`name` AS `name`,`usr`.`username` AS `username`,`usr`.`FINGERID` AS `FINGERID`,`usr`.`image` AS `image`,`dept`.`id_department` AS `id_department`,`pos`.`name_position` AS `position_name`,`dept`.`name` AS `department_name`,`pos`.`id_position` AS `id_position`,`b`.`branch_name` AS `branch_name`,`bg`.`name` AS `bagian_name`,`usr`.`id_bagian` AS `id_bagian` from ((((`mis`.`users` `usr` left join `mis`.`bagian` `bg` on((`bg`.`id` = `usr`.`id_bagian`))) left join `mis`.`position` `pos` on((`pos`.`id_position` = `usr`.`id_position`))) left join `mis`.`departments` `dept` on(((`dept`.`id_department` = `pos`.`id_department`) or (`dept`.`id_department` = `bg`.`id_department`)))) left join `mis`.`branch` `b` on((`b`.`branch_id` = `usr`.`branch_id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bagian`
--
ALTER TABLE `bagian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_department` (`id_department`),
  ADD KEY `creator_id` (`creator_id`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`branch_id`),
  ADD KEY `creator_id` (`creator_id`);

--
-- Indexes for table `branch_time`
--
ALTER TABLE `branch_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id_department`),
  ADD KEY `creator_id` (`creator_id`);

--
-- Indexes for table `homeservice`
--
ALTER TABLE `homeservice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `time_id` (`time_id`),
  ADD KEY `pay_id` (`pay_id`),
  ADD KEY `petugas_id` (`petugas_id`),
  ADD KEY `ptgshs_id` (`ptgshs_id`),
  ADD KEY `creator_id` (`creator_id`);

--
-- Indexes for table `hs_abbr`
--
ALTER TABLE `hs_abbr`
  ADD PRIMARY KEY (`abbr_id`),
  ADD KEY `branch_id` (`branch_id`),
  ADD KEY `creator_id` (`creator_id`);

--
-- Indexes for table `hs_block`
--
ALTER TABLE `hs_block`
  ADD PRIMARY KEY (`block_id`),
  ADD KEY `ptgs_id` (`ptgs_id`),
  ADD KEY `hari_id` (`hari_id`),
  ADD KEY `time_id` (`time_id`),
  ADD KEY `branch_id` (`branch_id`),
  ADD KEY `creator_id` (`creator_id`);

--
-- Indexes for table `hs_initial_petugas_hs`
--
ALTER TABLE `hs_initial_petugas_hs`
  ADD PRIMARY KEY (`ptgshs_id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `branch_id` (`branch_id`),
  ADD KEY `creator_id` (`creator_id`);

--
-- Indexes for table `hs_payment`
--
ALTER TABLE `hs_payment`
  ADD PRIMARY KEY (`pay_id`),
  ADD KEY `creator_id` (`creator_id`);

--
-- Indexes for table `hs_time`
--
ALTER TABLE `hs_time`
  ADD PRIMARY KEY (`time_id`),
  ADD KEY `branch_id` (`branch_id`),
  ADD KEY `creator_id` (`creator_id`);

--
-- Indexes for table `intern_checklist`
--
ALTER TABLE `intern_checklist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_fs`
--
ALTER TABLE `master_fs`
  ADD PRIMARY KEY (`fs_id`),
  ADD KEY `branch_id` (`branch_id`),
  ADD KEY `creator_id` (`creator_id`);

--
-- Indexes for table `master_intern_alat`
--
ALTER TABLE `master_intern_alat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_alat` (`id_alat`),
  ADD KEY `branch_id` (`branch_id`),
  ADD KEY `creator` (`creator`);

--
-- Indexes for table `master_menu`
--
ALTER TABLE `master_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `master_qc_alat`
--
ALTER TABLE `master_qc_alat`
  ADD PRIMARY KEY (`id_alat`),
  ADD KEY `creator_id` (`creator_id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `branch_id` (`branch_id`);

--
-- Indexes for table `m_ikabkota`
--
ALTER TABLE `m_ikabkota`
  ADD PRIMARY KEY (`id_kabkota`),
  ADD KEY `id_propinsi` (`id_propinsi`);

--
-- Indexes for table `m_ikecamatan`
--
ALTER TABLE `m_ikecamatan`
  ADD PRIMARY KEY (`id_kecamatan`),
  ADD KEY `id_kabkota` (`id_kabkota`),
  ADD KEY `id_propinsi` (`id_propinsi`);

--
-- Indexes for table `m_ikelurahan`
--
ALTER TABLE `m_ikelurahan`
  ADD PRIMARY KEY (`id_kelurahan`),
  ADD KEY `id_kecamatan` (`id_kecamatan`),
  ADD KEY `id_kabkota` (`id_kabkota`),
  ADD KEY `id_propinsi` (`id_propinsi`);

--
-- Indexes for table `m_ipropinsi`
--
ALTER TABLE `m_ipropinsi`
  ADD PRIMARY KEY (`id_propinsi`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from_id` (`from_id`),
  ADD KEY `to_id` (`to_id`);

--
-- Indexes for table `onesignal_ids`
--
ALTER TABLE `onesignal_ids`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`id_position`),
  ADD KEY `id_department` (`id_department`);

--
-- Indexes for table `role_menu_dept`
--
ALTER TABLE `role_menu_dept`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_menu_pos`
--
ALTER TABLE `role_menu_pos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_menu_user`
--
ALTER TABLE `role_menu_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_log`
--
ALTER TABLE `tabel_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`,`NIP`),
  ADD KEY `branch_id` (`branch_id`),
  ADD KEY `FINGERID` (`FINGERID`),
  ADD KEY `many_branch` (`many_branch`),
  ADD KEY `id_position` (`id_position`),
  ADD KEY `id_bagian` (`id_bagian`),
  ADD KEY `id_department` (`id_department`),
  ADD KEY `creator_id` (`creator_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bagian`
--
ALTER TABLE `bagian`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `branch_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `branch_time`
--
ALTER TABLE `branch_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id_department` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `homeservice`
--
ALTER TABLE `homeservice`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hs_abbr`
--
ALTER TABLE `hs_abbr`
  MODIFY `abbr_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hs_block`
--
ALTER TABLE `hs_block`
  MODIFY `block_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hs_initial_petugas_hs`
--
ALTER TABLE `hs_initial_petugas_hs`
  MODIFY `ptgshs_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hs_payment`
--
ALTER TABLE `hs_payment`
  MODIFY `pay_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hs_time`
--
ALTER TABLE `hs_time`
  MODIFY `time_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `intern_checklist`
--
ALTER TABLE `intern_checklist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `master_fs`
--
ALTER TABLE `master_fs`
  MODIFY `fs_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `master_intern_alat`
--
ALTER TABLE `master_intern_alat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `master_menu`
--
ALTER TABLE `master_menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `master_qc_alat`
--
ALTER TABLE `master_qc_alat`
  MODIFY `id_alat` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_ikabkota`
--
ALTER TABLE `m_ikabkota`
  MODIFY `id_kabkota` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_ikecamatan`
--
ALTER TABLE `m_ikecamatan`
  MODIFY `id_kecamatan` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_ikelurahan`
--
ALTER TABLE `m_ikelurahan`
  MODIFY `id_kelurahan` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_ipropinsi`
--
ALTER TABLE `m_ipropinsi`
  MODIFY `id_propinsi` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `onesignal_ids`
--
ALTER TABLE `onesignal_ids`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `position`
--
ALTER TABLE `position`
  MODIFY `id_position` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role_menu_dept`
--
ALTER TABLE `role_menu_dept`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role_menu_pos`
--
ALTER TABLE `role_menu_pos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role_menu_user`
--
ALTER TABLE `role_menu_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tabel_log`
--
ALTER TABLE `tabel_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
